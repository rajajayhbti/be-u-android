package com.beusalons.android.Model.UserProject;

/**
 * Created by Ajay on 2/8/2018.
 */

public class ProjectData_post {
    private String userId;
    private  int page;
    private String artistId;
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }
}
