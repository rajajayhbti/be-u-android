package com.beusalons.android.Model.Login;

import java.io.Serializable;

/**
 * Created by myMachine on 11/1/2016.
 */

public class Login_ResponseData implements Serializable {

    private String userId;
    private String name;
    private String phoneNumber;
    private String emailId;
    private String accessToken;
    private Integer phoneVerification;
    private String profilePic;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Integer getPhoneVerification() {
        return phoneVerification;
    }

    public void setPhoneVerification(Integer phoneVerification) {
        this.phoneVerification = phoneVerification;
    }

    public String getProfilePic() {
        return profilePic;
    }


    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }



}


