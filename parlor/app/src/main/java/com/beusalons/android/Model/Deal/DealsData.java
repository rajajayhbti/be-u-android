package com.beusalons.android.Model.Deal;

import java.io.Serializable;

/**
 * Created by Ajay on 1/8/2017.
 */

public class DealsData implements Serializable{

    private int dealId;
    private int serviceCode;

    public DealsData() {
    }

    public int getDealId() {
        return dealId;
    }

    public void setDealId(int dealId) {
        this.dealId = dealId;
    }

    public int getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(int serviceCode) {
        this.serviceCode = serviceCode;
    }
}
