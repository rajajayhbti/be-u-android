package com.beusalons.android.Fragment.UpdateAppScreen;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.support.v4.app.FragmentManager;
import android.widget.TextView;

import com.beusalons.android.Model.UpdateAppResponse;
import com.beusalons.android.R;
import com.google.gson.Gson;
import com.rd.PageIndicatorView;
import com.synnapps.carouselview.PageIndicator;

/**
 * Created by myMachine on 19-Feb-18.
 */

public class UpdateAppScreenFragment extends android.support.v4.app.DialogFragment {

    public static final String DATA= "com.beusalons.fragment.updateappscreenfrag";
    private UpdateAppResponse data= null;
    private TextView txt_title, txt_description;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);          //no action bar space
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        RelativeLayout relative_= (RelativeLayout)inflater.
                inflate(R.layout.fragment_update_screen, container, false);

        Bundle bundle= getArguments();
        if(bundle!=null &&
                bundle.containsKey(DATA))
            data= new Gson().fromJson(bundle.getString(DATA), UpdateAppResponse.class);
        else
            dismiss();

        ViewPager pager= relative_.findViewById(R.id.view_pager);
        ImageAdapter adapter= new ImageAdapter(getChildFragmentManager());
        pager.setAdapter(adapter);

        if(data!=null){
            pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    txt_title.setText(data.getData().get(position).getHeading());
                    txt_description.setText(data.getData().get(position).getDescription());
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            PageIndicatorView page_indicator= relative_.findViewById(R.id.page_indicator);
            page_indicator.setRadius(4);
            page_indicator.setPadding(8);
            page_indicator.setViewPager(pager);
            txt_title= relative_.findViewById(R.id.txt_title);
            txt_description= relative_.findViewById(R.id.txt_description);

            ImageView img_cancel= relative_.findViewById(R.id.img_cancel);
            img_cancel.setAlpha(.7f);
            img_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                }
            });
        }

        return relative_;
    }

    private class ImageAdapter extends FragmentPagerAdapter{

        private ImageAdapter(FragmentManager fm){
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            if(position==0){

                txt_title.setText(data.getData().get(position).getHeading());
                txt_description.setText(data.getData().get(position).getDescription());
            }
            return ImageFragment.newInstance(data.getData().get(position).getImage());
        }

        @Override
        public int getCount() {
            if(data!=null &&
                    data.getData()!=null &&
                    data.getData().size()>0)
                return data.getData().size();
            return 0;
        }
    }


}
