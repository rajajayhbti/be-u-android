package com.beusalons.android.Fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beusalons.android.Adapter.PastAppointmentsListAdapter;
import com.beusalons.android.Model.Appointments.PastAppointmentsPost;
import com.beusalons.android.Model.Appointments.PastAppointmentsResponse;
import com.beusalons.android.Model.Appointments.PastData;
import com.beusalons.android.Model.Appointments.Services;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class PastAppointments_Fragment extends Fragment {


    public PastAppointments_Fragment() {
        // Required empty public constructor
    }

    private static final String TAG= PastAppointments_Fragment.class.getSimpleName();

    private Retrofit retrofit;
    private List<PastData> details= new ArrayList<>();



    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view= inflater.inflate(R.layout.fragment_previous__appointment, container, false);

        recyclerView= (RecyclerView)view.findViewById(R.id.rcyView_prAppt);
        layoutManager= new LinearLayoutManager(getActivity());

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        adapter= new PastAppointmentsListAdapter(details, getContext());
        recyclerView.setAdapter(adapter);

        //progress dialog
//        progress= DialogSpinner.progressDialog(getActivity(), "Loading..");
//        progress.show();


        fetchData();


        return view;
    }

    public void fetchData(){

        SharedPreferences preferences= getActivity().getSharedPreferences("userDetails", Context.MODE_PRIVATE);
        final PastAppointmentsPost pastAppointmentsPost= new PastAppointmentsPost();

        if(preferences!=null){

            pastAppointmentsPost.setUserId(preferences.getString("userId", null));
            pastAppointmentsPost.setAccessToken(preferences.getString("accessToken", null));
        }

        retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<PastAppointmentsResponse> call= apiInterface.pastAppts(pastAppointmentsPost);
        call.enqueue(new Callback<PastAppointmentsResponse>() {
            @Override
            public void onResponse(Call<PastAppointmentsResponse> call, Response<PastAppointmentsResponse> response) {

                Log.i(TAG, "I'm in onResponse");
                PastAppointmentsResponse appointmentsResponse= response.body();

                if(appointmentsResponse!=null) {

                    //2017-01-31T12:40:39.538Z
                    SimpleDateFormat source= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");            //data aisa ata hai

                    SimpleDateFormat destDate= new SimpleDateFormat("dd/MM/yy");
                    SimpleDateFormat destTime= new SimpleDateFormat("hh:mm a");

                    for(int i=0; i<appointmentsResponse.getData().size();i++){

                        PastData data= new PastData();
                        data.setParlorName(appointmentsResponse.getData().get(i).getParlorName());
                        data.setParlorAddress(appointmentsResponse.getData().get(i).getParlorAddress());
                        data.setAppointmentId(appointmentsResponse.getData().get(i).getAppointmentId());
                        data.setOpeningTime(appointmentsResponse.getData().get(i).getOpeningTime());
                        data.setClosingTime(appointmentsResponse.getData().get(i).getClosingTime());

                        Log.i("i'mdateh", "value in start at: "+ appointmentsResponse.getData().get(i).getStartsAt());

                        //date
                        String stringDate= appointmentsResponse.getData().get(i).getStartsAt();
                        Date date = null;
                        String formattedDate ="", formattedTime="";

                        try {
                            Date timestamp = source.parse(stringDate);

                            Calendar cal= Calendar.getInstance();       //creating calendar instance
                            cal.setTime(timestamp);
                            cal.add(Calendar.HOUR_OF_DAY, 5);
                            cal.add(Calendar.MINUTE, 30);

                            timestamp= cal.getTime();

                            formattedDate= destDate.format(timestamp);
                            formattedTime= destTime.format(timestamp);


                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        int service_size= appointmentsResponse.getData().get(i).getServices().size();

                        List<Services> services_list= new ArrayList<>();
                        for(int j=0; j<service_size;j++){

                            Services services= new Services();
                            services.setName(appointmentsResponse.getData().get(i).getServices().get(j).getName());
                            services.setPrice(appointmentsResponse.getData().get(i).getServices().get(j).getPrice());
                            services_list.add(services);
                        }
                        data.setServices(services_list);

                        Log.i(TAG, "data in date: "+ formattedDate+ " data in time: "+ formattedTime);
                        data.setDate(formattedDate);
                        data.setTime(formattedTime);
                        data.setSubtotal(appointmentsResponse.getData().get(i).getSubtotal());
                        data.setDiscount(appointmentsResponse.getData().get(i).getDiscount());
                        data.setTax(appointmentsResponse.getData().get(i).getTax());
                        data.setPayableAmount(appointmentsResponse.getData().get(i).getPayableAmount());
                        data.setParlorId(appointmentsResponse.getData().get(i).getParlorId());
                        data.setStartsAt(formatDateTime(appointmentsResponse.getData().get(i).getStartsAt()));

                        details.add(data);
                    }
                    adapter.notifyDataSetChanged();
                }
//                progress.dismiss();

            }

            @Override
            public void onFailure(Call<PastAppointmentsResponse> call, Throwable t) {
//                progress.dismiss();
                Log.i(TAG, "I'm in onFailure"+ t.getCause());
            }
        });
    }

    private String formatDateTime(String str_date){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        Date date_format=null;
        try {
            date_format = sdf.parse(str_date);
            Calendar cal= Calendar.getInstance();       //creating calendar instance
            cal.setTime(date_format);
            cal.add(Calendar.HOUR_OF_DAY, 5);
            cal.add(Calendar.MINUTE, 30);

            date_format= cal.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("MMM dd yyyy, EEEE KK:mm aa").format(date_format);
    }

}
