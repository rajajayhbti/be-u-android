package com.beusalons.android.Model.SubscriptionHistory;

/**
 * Created by Ajay on 2/27/2018.
 */

public class Appointment {

    private String date;
    private Integer payableAmount;
    private Integer loyalitySubscription;
    private String parlorName;
    private String parlorAddress2;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(Integer payableAmount) {
        this.payableAmount = payableAmount;
    }

    public Integer getLoyalitySubscription() {
        return loyalitySubscription;
    }

    public void setLoyalitySubscription(Integer loyalitySubscription) {
        this.loyalitySubscription = loyalitySubscription;
    }

    public String getParlorName() {
        return parlorName;
    }

    public void setParlorName(String parlorName) {
        this.parlorName = parlorName;
    }

    public String getParlorAddress2() {
        return parlorAddress2;
    }

    public void setParlorAddress2(String parlorAddress2) {
        this.parlorAddress2 = parlorAddress2;
    }
}
