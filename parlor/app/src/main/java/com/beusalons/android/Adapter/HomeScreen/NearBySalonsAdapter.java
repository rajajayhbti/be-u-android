package com.beusalons.android.Adapter.HomeScreen;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Event.CartNotificationEvent;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.HomeFragmentModel.NearBySalon;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.R;
import com.beusalons.android.SalonPageActivity;
import com.bumptech.glide.Glide;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.snappydb.DB;
import com.snappydb.DBFactory;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashish salons on 3/2/2017.
 */

public class NearBySalonsAdapter extends RecyclerView.Adapter<NearBySalonsAdapter.ViewHolder>{
    private Activity activity;
    private List<NearBySalon> popularSalons;
    AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;

    public NearBySalonsAdapter(Activity mcontext,List<NearBySalon> mpopularSalons){
        popularSalons=new ArrayList<NearBySalon>();
        this.popularSalons=mpopularSalons;
        this.activity=mcontext;
        if (activity!=null){
            logger = AppEventsLogger.newLogger(activity);
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_salon_home,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.txtViewTitle.setText(popularSalons.get(position).getName());

        if (popularSalons.get(position).getParlorType() == 0)
            holder.img_.setImageResource(R.drawable.ic_premium_badge);
        else if (popularSalons.get(position).getParlorType() == 1)
            holder.img_.setImageResource(R.drawable.ic_standard_badge);
        else if (popularSalons.get(position).getParlorType() == 2)
            holder.img_.setImageResource(R.drawable.ic_budget_badge);

        if (popularSalons.get(position).getFavourite()){
            holder.imgFave.setVisibility(View.VISIBLE);
        }else{
            holder.imgFave.setVisibility(View.GONE);
        }

        if(position==1 || position== 4){
//            ll_popular_salons

            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) holder.llParent.getLayoutParams();
            params.rightMargin= 12;
            params.leftMargin = 12;

        }


        holder.txtViewAddress2.setText(popularSalons.get(position).getAddress2());
        holder.distance.setText(popularSalons.get(position).getDistance() + " km");
       /* if (dist>9.0){


            holder.distance.setTextColor(context.getResources().getColor(R.color.light_yellow));
        }else{
            holder.distance.setTextColor(context.getResources().getColor(R.color.colorGreen));
        }*/
        Glide.with(activity).load(popularSalons.get(position).getImage()).into(holder.imgSalons);
/*        Glide.with(activity)
                .load(popularSalons.get(position).getImage())
                .centerCrop()
                .placeholder(R.drawable.master_salon)
                .crossFade()
                .into(holder.imgSalons);*/

        holder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(activity, SalonPageActivity.class);
//                intent.putExtra("parlorId",popularSalons.get(position).getParlorId());
//                activity.startActivity(intent);

                boolean show_dialog= false;
                UserCart saved_cart = null;
                //opening cart, checking cart type equal to service type, and parlor id are same, if not same show dialog
                try {

                    DB snappyDB = DBFactory.open(activity);
                    //db mai jo saved cart hai
                    if (snappyDB.exists(AppConstant.USER_CART_DB)) {

                        saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
                    }
                    snappyDB.close();

                    if(saved_cart!=null){

                        if(saved_cart.getCartType()!=null &&
                                saved_cart.getCartType().equalsIgnoreCase(AppConstant.SERVICE_TYPE)){

                            if(saved_cart.getParlorId().equalsIgnoreCase(popularSalons.get(position).getParlorId()))
                                show_dialog= false;
                            else{

                                if(saved_cart.getServicesList().size()>0)
                                    show_dialog= true;
                                else
                                    show_dialog= false;
                            }
                        }else{
                            show_dialog= false;
                            try {

                                DB snappyDB_ = DBFactory.open(activity);
                                snappyDB_.destroy();
                                snappyDB_.close();

                                EventBus.getDefault().post(new CartNotificationEvent());
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                        }
                    }else
                        show_dialog= false;

                }catch (Exception e){
                    e.printStackTrace();
                }

                if(show_dialog){

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle("Replace cart item?");
                    builder.setMessage("Your cart contains services from "+ saved_cart.getParlorName()+". Do you " +
                            "wish to discard the selection and add services from "+ popularSalons.get(position).getName()+"?");
                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, int id) {

                            try {

                                DB snappyDB = DBFactory.open(activity);
                                snappyDB.destroy();
                                snappyDB.close();

                                EventBus.getDefault().post(new CartNotificationEvent());
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            Intent intent=new Intent(activity, SalonPageActivity.class);
                            intent.putExtra("parlorId",popularSalons.get(position).getParlorId());
                            activity.startActivity(intent);

                        }
                    });
                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.dismiss();
                        }
                    }).show();

                }else{
                    logNearbySalonsHomeButtonEvent();
                    logNearbySalonsHomeButtonFireBaseEvent();
                    Intent intent=new Intent(activity, SalonPageActivity.class);
                    intent.putExtra("parlorId",popularSalons.get(position).getParlorId());
                    activity.startActivity(intent);
                }

            }
        });

    }

    public void setData(ArrayList<NearBySalon> nearBySalon){
        this.popularSalons=nearBySalon;
        notifyDataSetChanged();

    }
    public void logNearbySalonsHomeButtonEvent () {
        Log.e("nearBy","fine");
       logger.logEvent( AppConstant.NearbySalonsHomeButton);
    }
    public  void logNearbySalonsHomeButtonFireBaseEvent(){
        Log.e("nearByfire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.NearbySalonsHomeButton,bundle);
    }

    //comment by Ajay 3 march 17
//    public void setData(ArrayList<NearBySalon> nearBySalons) {
//        this.popularSalons = nearBySalons;
//        notifyDataSetChanged();
//    }

    @Override
    public int getItemCount() {
        return popularSalons.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private  ImageView imgFave;
        private  TextView distance;
        LinearLayout llParent;
        TextView txtViewTitle;
        TextView txtViewAddress2;
        ImageView imgSalons, img_;
        public ViewHolder(View itemView) {
            super(itemView);

            txtViewTitle=(TextView)itemView.findViewById(R.id.txt_salon_title);
            txtViewAddress2=(TextView)itemView.findViewById(R.id.txtView_address2);
            imgSalons=(ImageView)itemView.findViewById(R.id.imgSalons);
            llParent= (LinearLayout) itemView.findViewById(R.id.ll_popular_salons);
            distance= (TextView) itemView.findViewById(R.id.distance_salon);
            imgFave=(ImageView)itemView.findViewById(R.id.img_fav_salon);
            img_= (ImageView) itemView.findViewById(R.id.img_);
        }
    }
}
