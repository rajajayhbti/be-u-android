package com.beusalons.android.Model.Loyalty;

import java.util.List;

/**
 * Created by myMachine on 5/5/2017.
 */

public class Tnc {

    public String title;
    public List<String> points = null;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getPoints() {
        return points;
    }

    public void setPoints(List<String> points) {
        this.points = points;
    }
}
