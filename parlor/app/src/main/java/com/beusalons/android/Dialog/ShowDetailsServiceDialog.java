package com.beusalons.android.Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.R;

/**
 * Created by Ajay on 7/6/2017.
 */

public class ShowDetailsServiceDialog {



    final Dialog dialog;

    private TextView txtViewHeader,txtDiscription;
    LinearLayout linearLayoutManager,llServicePopup;

    private  TextView txtViewService;

    public ShowDetailsServiceDialog(final Activity activity, String DealName, String DealDiscriptiom){

        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_service_details);
        txtViewHeader=(TextView)dialog.findViewById(R.id.xt_show_detail);
        txtDiscription=(TextView)dialog.findViewById(R.id.txt_deal_discription);
        linearLayoutManager= (LinearLayout) dialog.findViewById(R.id.ll_show_deal_dialogue);
        llServicePopup=(LinearLayout)dialog.findViewById(R.id.ll_service_popup);
        txtDiscription.setText(DealDiscriptiom);
        txtViewHeader.setText(DealName);

        linearLayoutManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        llServicePopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
