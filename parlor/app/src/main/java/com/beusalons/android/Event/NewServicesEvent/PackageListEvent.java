package com.beusalons.android.Event.NewServicesEvent;

import com.beusalons.android.Model.UserCart.UserServices;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 9/19/2017.
 */

public class PackageListEvent {

    private String name;
    private boolean isAlter;
    private List<UserServices> list= new ArrayList<>();

    public PackageListEvent(List<UserServices> list, String name, boolean isAlter){
        this.name= name;
        this.isAlter= isAlter;
        this.list= list;
    }

    public String getName() {
        return name;
    }

    public boolean isAlter() {
        return isAlter;
    }

    public void setList(List<UserServices> list) {
        this.list = list;
    }

    public List<UserServices> getList() {
        return list;
    }

}
