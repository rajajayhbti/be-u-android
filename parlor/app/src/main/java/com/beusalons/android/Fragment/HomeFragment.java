package com.beusalons.android.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.beusalons.android.Adapter.NewHomeScreen.HomeAdapterVertical;
import com.beusalons.android.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ashish Sharma on 1/2/2018.
 */

public class HomeFragment extends Fragment {
    @BindView(R.id.rec_home)
    RecyclerView recHome;
    @BindView(R.id.ll_header_salon_type)
    LinearLayout topHeader;
  private   HomeAdapterVertical adapterVertical;
    LinearLayoutManager manager;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_new_home_page, container, false);
        ButterKnife.bind(this,view);

         manager=new LinearLayoutManager(getActivity());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recHome.setLayoutManager(manager);
        adapterVertical = new HomeAdapterVertical();
        recHome.setAdapter(adapterVertical);


        recHome.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                  int  position = manager.findFirstVisibleItemPosition();
                  if (position>2){
                      topHeader.setVisibility(View.VISIBLE);
                  }else  topHeader.setVisibility(View.GONE);
                 }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayout linearLayout = (LinearLayout) recyclerView.findChildViewUnder(dx, dy);
//                TextView textView = (TextView) recyclerView.findChildViewUnder(dx, dy);
                if (linearLayout != null && linearLayout.getTag() != null) {
                    int tag = Integer.parseInt(linearLayout.getTag().toString());
                    if (tag >= 0 && tag < 0) {
                        topHeader.setVisibility(View.GONE);
                      //  textOpernCloseCount.setText(openCount + " Restaurent are taking orders in "+Preferences.getString(Preferences.POST_CODE));
                    } else {
                        topHeader.setVisibility(View.VISIBLE);
                     //   textOpernCloseCount.setText(closeCounmt + " Restaurents are not taking orders");
                    }
                }
            }
        });
        return view;
    }


}
