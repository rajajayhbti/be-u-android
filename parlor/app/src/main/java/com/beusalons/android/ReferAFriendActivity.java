package com.beusalons.android;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.beusalons.android.Fragment.SelectEmployeeFragment;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;


public class ReferAFriendActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer_afriend);
        //new ToolbarHelper(getSupportActionBar(), getLayoutInflater(), true, this);
        setToolBar();

        TextView txt_tap_to_share= (TextView) findViewById(R.id.txt_tap_to_share);
        TextView txt_code= (TextView)findViewById(R.id.txt_refer_code);

        Log.i("notificka", "value : "+ BeuSalonsSharedPrefrence.getReferCode());

//        txtViewEarn.setText(fromHtml(getString(R.string.earn_text)));
//        txtViewShare.setText(fromHtml(getString(R.string.share_txt)));
        txt_code.setText(BeuSalonsSharedPrefrence.getReferCode());
        txt_tap_to_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent sendIntent= new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");


//                String text_content= "Get A Free Haircut (On Min Bill Value Of ₹100) + " +
//                        "1000 Freebie Points On Downloading T  he Be U Salons App And Get 100% " +
//                        "Cashback On 1st Digital Payment Via Be U Salons App. To Avail The Same, Use Code "+
//                        BeuSalonsSharedPrefrence.getReferCode()+" To Sign Up. "+"onelink.to/bf45qf";

                //have to set title and content
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Refer To Grab The Best!");
                sendIntent.putExtra(Intent.EXTRA_TEXT ,BeuSalonsSharedPrefrence.getNormalMessage());
                startActivity(Intent.createChooser(sendIntent, "Share code:"));
            }
        });



    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void setToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null){

            getSupportActionBar().setTitle(getResources().getString(R.string.refer_a_friend));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }






    }


}
