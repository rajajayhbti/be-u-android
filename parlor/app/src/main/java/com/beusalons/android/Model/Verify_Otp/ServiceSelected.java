package com.beusalons.android.Model.Verify_Otp;

import java.util.List;

/**
 * Created by Ajay on 12/20/2016.
 */

public class ServiceSelected {
    private  String titleName;

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    private String serviceId;
    private List<String> priceId;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public List<String> getPriceId() {
        return priceId;
    }

    public void setPriceId(List<String> priceId) {
        this.priceId = priceId;
    }
}
