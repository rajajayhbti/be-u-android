package com.beusalons.android.Fragment.DealFragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.beusalons.android.Event.NewDealEvent.DealComboEvent;
import com.beusalons.android.Fragment.DialogFragmentServices;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.DealsServices.DealService;
import com.beusalons.android.Model.UserCart.PackageService;
import com.beusalons.android.Model.newServiceDeals.NewCombo.Selector;
import com.beusalons.android.R;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.michael.easydialog.EasyDialog;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 6/16/2017.
 */

public class ComboBottomSheet extends BottomSheetDialogFragment {


    private DealService dealService;

    private List<PackageService> package_services_list= new ArrayList<>();

    private List<Integer> service_index= new ArrayList<>();
    private List<Integer> brand_index= new ArrayList<>();
    private List<Integer> product_index= new ArrayList<>();

    private List<Boolean> has_brand= new ArrayList<>();
    private List<Boolean> has_product= new ArrayList<>();
    AppEventsLogger logger;

    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle= getArguments();
        if(bundle!=null && bundle.containsKey("combo_service")){

            dealService= new Gson().fromJson(bundle.getString("combo_service"), DealService.class);
        }else{
            dismiss();
        }
        logger = AppEventsLogger.newLogger(getActivity());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_services_specific_bottomsheet_, container, false);

        final ViewAnimator view_animator= (ViewAnimator)view.findViewById(R.id.view_animator);

        final TextView txt_customize, txt_select, txt_done_apply, txt_cancel,
                txt_name_, txt_description_;
        final LinearLayout linear_done, linear_, linear_items, linear_price, linear_cancel;        //linear_ and linear_item se khelenge

        linear_= (LinearLayout)view.findViewById(R.id.linear_);
        linear_items= (LinearLayout)view.findViewById(R.id.linear_items);
        linear_done= (LinearLayout)view.findViewById(R.id.linear_done);     //yehi apply bhi hai
        linear_cancel= (LinearLayout)view.findViewById(R.id.linear_cancel);


        txt_name_= (TextView)view.findViewById(R.id.txt_name_);
        txt_description_= (TextView)view.findViewById(R.id.txt_description_);

        txt_name_.setText(dealService.getName());
        txt_description_.setText(dealService.getShortDescription());

        txt_customize= (TextView)view.findViewById(R.id.txt_customize);
        txt_select= (TextView)view.findViewById(R.id.txt_select);

        txt_customize.setText("Customize Your Package");
        txt_select.setText("Please Select");

        txt_done_apply= (TextView)view.findViewById(R.id.txt_done_apply);
        txt_cancel= (TextView)view.findViewById(R.id.txt_cancel);


        linear_price= view.findViewById(R.id.linear_price);
        if(dealService.getStartAt()!=null &&
                !dealService.getStartAt().equalsIgnoreCase("") &&
                !dealService.getStartAt().equalsIgnoreCase("0")){
            linear_price.setVisibility(View.VISIBLE);

            TextView txt_price= view.findViewById(R.id.txt_price);
            TextView txt_save_per= view.findViewById(R.id.txt_save_per);
            TextView txt_what= view.findViewById(R.id.txt_what);

            txt_what.setText("Lowest Price: ");
            txt_price.setText(AppConstant.CURRENCY+dealService.getStartAt());

            String txt= "<font color='#58595b'>Saving Upto </font>"+"<font color='#3e780a'>"+dealService.getSave()+"%</font>";
            txt_save_per.setText(fromHtml(txt));
            txt_save_per.setTypeface(null, Typeface.ITALIC);

        }else{
            linear_price.setVisibility(View.GONE);
        }


        linear_.removeAllViews();
        for(int i=0;i<dealService.getSelectors().size();i++){

            final int selector_index= i;

            final PackageService package_service= new PackageService();

//            View view_= LayoutInflater.from(getActivity()).
//                    inflate(R.layout.fragment_services_specific_bottomsheet_package_combo, null, false);

            boolean isService;

//            final TextView txt_service, txt_brand, txt_product, txt_service_name;
//            final LinearLayout linear_service, linear_brand, linear_product,view_service,view_brand,
//                    linear_brand_, linear_product_;

//            linear_service= (LinearLayout)view_.findViewById(R.id.linear_service);
//            linear_brand= (LinearLayout)view_.findViewById(R.id.linear_brand);
//            linear_product= (LinearLayout)view_.findViewById(R.id.linear_product);
//
//            linear_brand_= (LinearLayout)view_.findViewById(R.id.linear_brand_);
//            linear_product_= (LinearLayout)view_.findViewById(R.id.linear_product_);
//
//
//            txt_service= (TextView)view_.findViewById(R.id.txt_service);
//            txt_brand= (TextView)view_.findViewById(R.id.txt_brand);
//            txt_product= (TextView)view_.findViewById(R.id.txt_product);
//            txt_service_name= (TextView)view_.findViewById(R.id.txt_service_name);
//
//
//            view_service=(LinearLayout) view_.findViewById(R.id.view_service);
//            view_brand=(LinearLayout)view_.findViewById(R.id.view_brand);
//
//            txt_service_name.setText(dealService.getSelectors().get(selector_index).getServiceTitle());

            //service-------------------------------------------
            service_index.add(selector_index, 0);
            brand_index.add(selector_index, 0);     //--------------------------------
            product_index.add(selector_index, 0);           //--------------------------------


            dealService.getSelectors().get(i).getServices().get(0).setCheck(true);
//            txt_service.setText(dealService.getSelectors().get(selector_index).getServices().
//                    get(0).getName());

            package_service.setService_code(dealService.getSelectors().get(selector_index)
                    .getServices().get(0).getServiceCode());
            package_service.setService_id(dealService.getSelectors().get(selector_index).getServices().
                    get(0).getServiceId());
            package_service.setService_name(dealService.getSelectors().get(selector_index).getServices().
                    get(0).getName());


            //whether to show service or not
            if(dealService.getSelectors().get(i).getServices().size()>1){      //size greater than 1 then show

                isService= true;
//                linear_service.setVisibility(View.VISIBLE);
            }else{

                isService= false;
//                linear_service.setVisibility(View.GONE);
            }

            if(dealService.getSelectors().get(i).getServices().get(0).getBrands()==null ||
                    dealService.getSelectors().get(i).getServices().get(0).getBrands().size()==0 ){

                has_brand.add(false);
                has_product.add(false);
//                linear_brand.setVisibility(View.GONE);
//                linear_product.setVisibility(View.GONE);

            }else{


                //brand ka stuff-------------------------------------------------
                has_brand.add(true);
//                view_service.setPadding(0,10,195,0);
//                linear_brand.setVisibility(View.VISIBLE);

                dealService.getSelectors().get(selector_index).getServices().get(0).getBrands().get(0).setCheck(true);
//                txt_brand.setText(dealService.getSelectors().get(selector_index).getServices().
//                        get(0).getBrands().get(0).getBrandName());

                package_service.setBrand_id(dealService.getSelectors().get(selector_index).getServices().
                        get(0).getBrands().get(0).getBrandId());
                package_service.setBrand_name(dealService.getSelectors().get(selector_index).getServices().
                        get(0).getBrands().get(0).getBrandName());

//                if(dealService.getSelectors().get(selector_index).getServices().get(0).getBrands().size()>1){
//                    linear_brand_.setVisibility(View.VISIBLE);
//                    linear_brand.setEnabled(true);
//                }else{
//                    linear_brand_.setVisibility(View.GONE);
//                    linear_brand.setEnabled(false);
//                }


                if(dealService.getSelectors().get(selector_index).getServices().
                        get(0).getBrands().get(0).getProducts()==null ||
                        dealService.getSelectors().get(selector_index).getServices().
                                get(0).getBrands().get(0).getProducts().size()==0){

//                    linear_product.setVisibility(View.GONE);
                    has_product.add(false);

                }else{

                    has_product.add(true);
//                    linear_product.setVisibility(View.VISIBLE);
//                    view_brand.setPadding(0,10,195,0);
                    package_service.setProduct_id(dealService.getSelectors().get(selector_index).getServices().
                            get(0).getBrands().get(0).getProducts().get(0).getProductId());
                    package_service.setProduct_name(dealService.getSelectors().get(selector_index).getServices().
                            get(0).getBrands().get(0).getProducts().get(0).getProductName());

                    dealService.getSelectors().get(selector_index).getServices().get(0).getBrands().get(0).
                            getProducts().get(0).setCheck(true);
//                    txt_product.setText(dealService.getSelectors().get(selector_index).getServices().
//                            get(0).getBrands().get(0).getProducts().get(0).getProductName());


//                    if(dealService.getSelectors().get(selector_index).getServices()
//                            .get(0).getBrands().get(0).getProducts().size()>1){
//
//                        linear_product_.setVisibility(View.VISIBLE);
//                        linear_product.setEnabled(true);
//
//                    }else{
//
//                        linear_product_.setVisibility(View.GONE);
//                        linear_product.setEnabled(false);
//                    }
                }
            }
            package_services_list.add(package_service);

            //--------------------------------------------------++++++++++++++++++++++++++++++++++++++++++
//            linear_service.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    //view animator type hai yeh
//                    view_animator.setInAnimation(getActivity(), R.anim.slide_from_right);
//
//                    txt_customize.setText("Customize Your Service");
//                    txt_select.setText("Please Select Any One Option");
//
//                    txt_cancel.setText("Cancel");
//                    txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cancel_));
//
//                    logDealBottomSheetChangeEvent();
//                    logDealBottomSheetChangeFirebaseEvent();
//                    linear_cancel.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            view_animator.setInAnimation(getActivity(), R.anim.slide_from_left);
//
//                            txt_cancel.setText("View Cart");
//                            txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.alter_));
//
//
//                            txt_customize.setText("Customize Your Package");
//                            txt_select.setText("Please Select");
//
//                            linear_cancel.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    logDealBottomSheetCancelEvent();
//                                    logDealBottomSheetCancelFirBaseEvent();
//
//                                    addServices(true);
//
//
//                                }
//                            });
//
//                            txt_done_apply.setText(getResources().getString(R.string.done));
//                            linear_done.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    addServices(false);
//                                    logDealBottomSheetDoneEvent();
//                                    logDealBottomSheetDoneFireBaseEvent();
//                                    dismiss();
//                                }
//                            });
//
//
//                            txt_service.setText(dealService.getSelectors().get(selector_index).getServices()
//                                    .get(service_index.get(selector_index)).getName());
//
//                            if(dealService.getSelectors().get(selector_index).getServices()
//                                    .get(service_index.get(selector_index)).getBrands()!=null &&
//                                    dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().size()>0){
//                                has_brand.set(selector_index, true);
//
//                                if(dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(0).getProducts()!=null &&
//                                        dealService.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(0).getProducts().size()>0){
//
//                                    has_product.set(selector_index, true);
//                                }else{
//                                    has_product.set(selector_index, false);
//                                }
//
//                            }else{
//                                has_brand.set(selector_index, false);
//                                has_product.set(selector_index, false);
//                            }
//
//
//                            if(has_brand.get(selector_index)){
//                                //brand ka case handle kar raha hoon-----------------------------------------------
//                                for(int a=0;a<dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().size();a++){
//                                    dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(a).setCheck(false);
//                                }
//                                dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(0).setCheck(true);
//                                txt_brand.setText(dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(0).getBrandName());
//
//                                brand_index.set(selector_index, 0);
//
//                                package_service.setBrand_name(dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(0).getBrandName());
//                                package_service.setBrand_id(dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(0).getBrandId());
//                                package_services_list.set(selector_index, package_service);
//
//
//                                if(has_product.get(selector_index)){
//                                    //product ka case handle kara hai-----------------------------------------
//                                    for(int b=0;b<dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().size();b++){
//
//                                        dealService.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().get(b).setCheck(false);
//                                    }
//                                    dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(0).setCheck(true);
//                                    txt_product.setText(dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands()
//                                            .get(brand_index.get(selector_index)).getProducts().get(0).getProductName());
//
//                                    product_index.set(selector_index, 0);
//
//                                    package_service.setProduct_name(dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(0).getProductName());
//                                    package_service.setProduct_id(dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(0).getProductId());
//                                }
//
//                                package_services_list.set(selector_index, package_service);
//                            }
//
//                            view_animator.showPrevious();
//                        }
//                    });
//
//                    txt_done_apply.setText(getResources().getString(R.string.apply));
//                    linear_done.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            view_animator.setInAnimation(getActivity(), R.anim.slide_from_left);
//
//                            txt_cancel.setText("View Cart");
//                            txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.alter_));
//
//                            txt_customize.setText("Customize Your Package");
//                            txt_select.setText("Please Select");
//
//                            txt_done_apply.setText(getResources().getString(R.string.done));
//
//
//                            txt_service.setText(dealService.getSelectors().get(selector_index).getServices()
//                                    .get(service_index.get(selector_index)).getName());
//
//                            linear_done.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    logDealBottomSheetDoneEvent();
//                                    logDealBottomSheetDoneFireBaseEvent();
//                                    addServices(false);
//                                    dismiss();
//                                }
//                            });
//                            linear_cancel.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    logDealBottomSheetCancelEvent();
//                                    logDealBottomSheetCancelFirBaseEvent();
//
//                                    addServices(true);
//
//                                    dismiss();
//                                }
//                            });
//
//                            if(dealService.getSelectors().get(selector_index).getServices()
//                                    .get(service_index.get(selector_index)).getBrands()!=null &&
//                                    dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().size()>0){
//                                has_brand.set(selector_index, true);
//
//                                if(dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(0).getProducts()!=null &&
//                                        dealService.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(0).getProducts().size()>0){
//
//                                    has_product.set(selector_index, true);
//                                }else{
//                                    has_product.set(selector_index, false);
//                                }
//
//                            }else{
//                                has_brand.set(selector_index, false);
//                                has_product.set(selector_index, false);
//                            }
//
//
//                            if(has_brand.get(selector_index)){
//                                //brand ka case handle kar raha hoon-----------------------------------------------
//                                for(int a=0;a<dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().size();a++){
//                                    dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(a).setCheck(false);
//                                }
//                                dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(0).setCheck(true);
//                                txt_brand.setText(dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(0).getBrandName());
//
//                                brand_index.set(selector_index, 0);
//
//                                package_service.setBrand_name(dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(0).getBrandName());
//                                package_service.setBrand_id(dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(0).getBrandId());
//                                package_services_list.set(selector_index, package_service);
//
//
//                                if(has_product.get(selector_index)){
//                                    //product ka case handle kara hai-----------------------------------------
//                                    for(int b=0;b<dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().size();b++){
//
//                                        dealService.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().get(b).setCheck(false);
//                                    }
//                                    dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(0).setCheck(true);
//                                    txt_product.setText(dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands()
//                                            .get(brand_index.get(selector_index)).getProducts().get(0).getProductName());
//
//                                    product_index.set(selector_index, 0);
//
//                                    package_service.setProduct_name(dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(0).getProductName());
//                                    package_service.setProduct_id(dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(0).getProductId());
//                                }
//
//                                package_services_list.set(selector_index, package_service);
//                            }
//
//                            view_animator.showPrevious();
//                        }
//                    });
//
//                    linear_items.removeAllViews();                  //removing the views first...no duplication
//
//                    LinearLayout customize_= (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.edit_service, null);
//                    TextView txt_customize_= (TextView)customize_.findViewById(R.id.txt_customize_);
//                    txt_customize_.setText("Service");
//                    linear_items.addView(customize_);
//
//                    final List<RadioButton> list_radio_= new ArrayList<>();
//
//                    for(int j=0;j<dealService.getSelectors().get(selector_index).getServices().size();j++){
//
//                        final int index= j;
//                        View view_= LayoutInflater.from(getActivity()).
//                                inflate(R.layout.bottomsheet_items, null, false);
//
//                        LinearLayout linear_click= (LinearLayout)view_.findViewById(R.id.linear_click);
//                        TextView txt_name= (TextView)view_.findViewById(R.id.txt_name);
//                        TextView txt_price= (TextView)view_.findViewById(R.id.txt_price);
//
//                        txt_name.setText(dealService.getSelectors().get(selector_index).getServices().get(j).getName());
//
//                        final RadioButton radio_= (RadioButton)view_.findViewById(R.id.radio_);
//                        radio_.setClickable(false);
//                        list_radio_.add(radio_);            //adding the radio button in radio list
//
//                        if(dealService.getSelectors().get(selector_index).getServices().get(j).isCheck()){
//
//                            radio_.setChecked(true);
//                            list_radio_.get(j).setChecked(true);
//                        }else{
//
//                            radio_.setChecked(false);
//                            list_radio_.get(j).setChecked(false);
//                        }
//
//
//                        linear_click.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//
//                                if(list_radio_.get(index).isChecked()){
//
//                                    list_radio_.get(index).setChecked(false);
//                                }else{
//
//                                    list_radio_.get(index).setChecked(true);
//                                }
//
//                                //updating view
//                                int list_radio_size=0;
//                                for(int k=0;k<list_radio_.size();k++){
//
//                                    if(k==index && list_radio_.get(index).isChecked()){     //conditions yaha likh
//
//                                        service_index.set(selector_index, index);
//
//                                        dealService.getSelectors().get(selector_index).getServices().get(index).setCheck(true);
//
//                                        package_service.setService_name(dealService.getSelectors().get(selector_index).getServices().
//                                                get(index).getName());
//                                        package_service.setService_id(dealService.getSelectors().get(selector_index).getServices().
//                                                get(index).getServiceId());
//                                        package_service.setService_code(dealService.getSelectors()
//                                                .get(selector_index).getServices().get(index).getServiceCode());
//                                        package_services_list.set( selector_index,package_service);
//
//                                    }else{              //baki sare un check
//
//                                        list_radio_size++;
//                                        list_radio_.get(k).setChecked(false);
//                                        dealService.getSelectors().get(selector_index).getServices().get(k).setCheck(false);
//                                    }
//                                }
//
//                                //agar koi bhi select nai kara toh current wale ko select karao
//                                if(list_radio_size==list_radio_.size()){                            //aur conditions yaha likh
//
//                                    service_index.set(selector_index, index);
//
//                                    //json types saving to cart
//                                    package_service.setService_name(dealService.getSelectors().get(selector_index).getServices().
//                                            get(index).getName());
//                                    package_service.setService_id(dealService.getSelectors().get(selector_index).getServices().
//                                            get(index).getServiceId());
//                                    package_service.setService_code(dealService.getSelectors()
//                                            .get(selector_index).getServices().get(index).getServiceCode());
//                                    package_services_list.set( selector_index,package_service);
//
//                                    dealService.getSelectors().get(selector_index).getServices().get(index).setCheck(true);
//                                    list_radio_.get(index).setChecked(true);
//                                }
//                            }
//                        });
//
//                        linear_items.addView(view_);
//                    }
//
//                    view_animator.showNext();
//                }
//            });
//
//            linear_brand.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    //view animator type hai yeh
//                    view_animator.setInAnimation(getActivity(), R.anim.slide_from_right);
//                    logDealBottomSheetChangeEvent();
//                    logDealBottomSheetChangeFirebaseEvent();
//
//                    txt_cancel.setText("Cancel");
//                    txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cancel_));
//
//                    txt_customize.setText("Customize Your Service");
//                    txt_select.setText("Please Select Any One Option");
//
//                    txt_done_apply.setText(getResources().getString(R.string.apply));
//
//                    linear_cancel.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            view_animator.setInAnimation(getActivity(), R.anim.slide_from_left);
//
//                            txt_customize.setText("Customize Your Package");
//                            txt_select.setText("Please Select");
//
//                            txt_cancel.setText("View Cart");
//                            txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.alter_));
//
//                            linear_cancel.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    logDealBottomSheetCancelEvent();
//                                    logDealBottomSheetCancelFirBaseEvent();
//
//                                    addServices(true);
//
//
//                                    dismiss();
//                                }
//                            });
//
//                            txt_done_apply.setText(getResources().getString(R.string.done));
//                            linear_done.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    addServices(false);
//                                    logDealBottomSheetDoneEvent();
//                                    logDealBottomSheetDoneFireBaseEvent();
//                                    dismiss();
//                                }
//                            });
//
//                            txt_brand.setText(dealService.getSelectors().get(selector_index).getServices()
//                                    .get(service_index.get(selector_index)).getBrands()
//                                    .get(brand_index.get(selector_index)).getBrandName());
//
//                            if(dealService.getSelectors().get(selector_index).getServices()
//                                    .get(service_index.get(selector_index)).getBrands().
//                                            get(brand_index.get(selector_index)).getProducts()!=null &&
//                                    dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).
//                                            getBrands().get(brand_index.get(selector_index)).getProducts().size()>0){
//
//                                has_product.set(selector_index, true);
//                            }else{
//                                has_product.set(selector_index, false);
//                            }
//
//
//                            if(has_product.get(selector_index)){
//
//                                //product ka case handle kara hai
//                                for(int b=0;b<dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().size();b++){
//
//                                    dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().get(b).setCheck(false);
//                                }
//                                dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().get(0).setCheck(true);
//                                txt_product.setText(dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands()
//                                        .get(brand_index.get(selector_index)).getProducts().get(0).getProductName());
//
//                                product_index.set(selector_index, 0);
//                                package_service.setProduct_name(dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(0).getProductName());
//                                package_service.setProduct_id(dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(0).getProductId());
//                                package_services_list.set(selector_index, package_service);
//
//
//                                if(dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().size()>1){
//
//                                    linear_product_.setVisibility(View.VISIBLE);
//                                    linear_product.setEnabled(true);
//                                }else{
//
//                                    linear_product_.setVisibility(View.GONE);
//                                    linear_product.setEnabled(false);
//                                }
//                            }
//
//                            view_animator.showPrevious();
//
//                        }
//                    });
//
//                    linear_done.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//
//                            view_animator.setInAnimation(getActivity(), R.anim.slide_from_left);
//
//
//                            txt_customize.setText("Customize Your Package");
//                            txt_select.setText("Please Select");
//
//                            txt_done_apply.setText(getResources().getString(R.string.done));
//
//                            txt_brand.setText(dealService.getSelectors().get(selector_index).getServices()
//                                    .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getBrandName());
//
//                            linear_done.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    logDealBottomSheetDoneEvent();
//                                    logDealBottomSheetDoneFireBaseEvent();
//                                    addServices(false);
//                                    dismiss();
//                                }
//                            });
//
//                            txt_cancel.setText("View Cart");
//                            txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.alter_));
//
//                            linear_cancel.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    logDealBottomSheetCancelEvent();
//                                    logDealBottomSheetCancelFirBaseEvent();
//
//                                    addServices(true);
//
//
//                                    dismiss();
//                                }
//                            });
//
//                            if(dealService.getSelectors().get(selector_index).getServices()
//                                    .get(service_index.get(selector_index)).getBrands().
//                                            get(brand_index.get(selector_index)).getProducts()!=null &&
//                                    dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).
//                                            getBrands().get(brand_index.get(selector_index)).getProducts().size()>0){
//
//                                has_product.set(selector_index, true);
//                            }else{
//                                has_product.set(selector_index, false);
//                            }
//
//
//                            if(has_product.get(selector_index)){
//
//                                //product ka case handle kara hai
//                                for(int b=0;b<dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().size();b++){
//
//                                    dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().get(b).setCheck(false);
//                                }
//                                dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().get(0).setCheck(true);
//                                txt_product.setText(dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands()
//                                        .get(brand_index.get(selector_index)).getProducts().get(0).getProductName());
//
//                                product_index.set(selector_index, 0);
//                                package_service.setProduct_name(dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(0).getProductName());
//                                package_service.setProduct_id(dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(0).getProductId());
//                                package_services_list.set(selector_index, package_service);
//
//
//                                if(dealService.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().size()>1){
//
//                                    linear_product_.setVisibility(View.VISIBLE);
//                                    linear_product.setEnabled(true);
//                                }else{
//
//                                    linear_product_.setVisibility(View.GONE);
//                                    linear_product.setEnabled(false);
//                                }
//                            }
//
//                            view_animator.showPrevious();
//                        }
//                    });
//
//                    linear_items.removeAllViews();                  //removing the views first...no duplication
//
//                    LinearLayout customize_= (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.edit_service, null);
//                    TextView txt_customize_= (TextView)customize_.findViewById(R.id.txt_customize_);
//                    txt_customize_.setText("Brand");
//                    linear_items.addView(customize_);
//
//                    final List<RadioButton> list_radio_= new ArrayList<>();
//
//                    for(int j=0;j<dealService.getSelectors().get(selector_index).getServices().
//                            get(service_index.get(selector_index)).getBrands().size();j++){
//
//                        final int index= j;
//
//                        View view_= LayoutInflater.from(getActivity()).
//                                inflate(R.layout.bottomsheet_items, null, false);
//
//                        LinearLayout linear_click= (LinearLayout)view_.findViewById(R.id.linear_click);
//                        TextView txt_name= (TextView)view_.findViewById(R.id.txt_name);
//                        TextView txt_price= (TextView)view_.findViewById(R.id.txt_price);
//
//                        txt_name.setText(dealService.getSelectors().get(selector_index).getServices().
//                                get(service_index.get(selector_index)).getBrands().get(j).getBrandName());
//
//
//                        final RadioButton radio_= (RadioButton)view_.findViewById(R.id.radio_);
//                        radio_.setClickable(false);
//                        list_radio_.add(radio_);            //adding the radio button in radio list
//
//                        if(dealService.getSelectors().get(selector_index).getServices().get(service_index.get(selector_index))
//                                .getBrands().get(j).isCheck()){
//
//                            radio_.setChecked(true);
//                            list_radio_.get(j).setChecked(true);
//                        }else{
//
//                            radio_.setChecked(false);
//                            list_radio_.get(j).setChecked(false);
//                        }
//
//                        linear_click.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//
//                                if(list_radio_.get(index).isChecked()){
//
//                                    list_radio_.get(index).setChecked(false);
//                                }else{
//
//                                    list_radio_.get(index).setChecked(true);
//                                }
//
//                                //updating view
//                                int list_radio_size=0;
//                                for(int k=0;k<list_radio_.size();k++){
//
//                                    if(k==index && list_radio_.get(index).isChecked()){     //conditions yaha likh
//
//                                        brand_index.set(selector_index, index);
//                                        dealService.getSelectors().get(selector_index).getServices().
//                                                get(service_index.get(selector_index)).getBrands().get(index).setCheck(true);
//
//                                        package_service.setBrand_id(dealService.getSelectors().get(selector_index).getServices().
//                                                get(service_index.get(selector_index)).getBrands().get(index).getBrandId());
//                                        package_service.setBrand_name(dealService.getSelectors().get(selector_index).getServices().
//                                                get(service_index.get(selector_index)).getBrands().get(index).getBrandName());
//                                        package_services_list.set(selector_index, package_service);
//                                    }else{              //baki sare un check
//
//                                        list_radio_size++;
//                                        list_radio_.get(k).setChecked(false);
//                                        dealService.getSelectors().get(selector_index).getServices().
//                                                get(service_index.get(selector_index)).getBrands().get(k).setCheck(false);
//                                    }
//                                }
//
//                                //agar koi bhi select nai kara toh current wale ko select karao
//                                if(list_radio_size==list_radio_.size()){                            //aur conditions yaha likh
//
//                                    brand_index.set(selector_index, index);
//                                    dealService.getSelectors().get(selector_index).getServices().
//                                            get(service_index.get(selector_index)).getBrands().get(index).setCheck(true);
//                                    list_radio_.get(index).setChecked(true);
//
//                                    package_service.setBrand_id(dealService.getSelectors().get(selector_index).getServices().
//                                            get(service_index.get(selector_index)).getBrands().get(index).getBrandId());
//                                    package_service.setBrand_name(dealService.getSelectors().get(selector_index).getServices().
//                                            get(service_index.get(selector_index)).getBrands().get(index).getBrandName());
//                                    package_services_list.set(selector_index, package_service);
//
//
//                                }
//                            }
//                        });
//
//                        linear_items.addView(view_);
//                    }
//
//                    view_animator.showNext();
//                }
//            });
//
//            //products ka click hai yeh
//            linear_product.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    view_animator.setInAnimation(getActivity(), R.anim.slide_from_right);
//
//                    txt_cancel.setText("Cancel");
//                    txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cancel_));
//
//                    txt_customize.setText("Customize Your Service");
//                    txt_select.setText("Please Select Any One Option");
//
//                    txt_done_apply.setText(getResources().getString(R.string.apply));
//                    logDealBottomSheetChangeEvent();
//                    logDealBottomSheetChangeFirebaseEvent();
//                    linear_cancel.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            view_animator.setInAnimation(getActivity(), R.anim.slide_from_left);
//
//                            txt_customize.setText("Customize Your Package");
//                            txt_select.setText("Please Select");
//
//                            txt_cancel.setText("View Cart");
//                            txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.alter_));
//
//                            linear_cancel.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    logDealBottomSheetCancelFirBaseEvent();
//                                    logDealBottomSheetCancelEvent();
//
//                                    addServices(true);
//
//                                    dismiss();
//                                }
//                            });
//
//                            txt_done_apply.setText(getResources().getString(R.string.done));
//                            linear_done.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    logDealBottomSheetDoneEvent();
//                                    logDealBottomSheetDoneFireBaseEvent();
//                                    addServices(false);
//                                    dismiss();
//                                }
//                            });
//
//                            txt_product.setText(dealService.getSelectors().get(selector_index).getServices()
//                                    .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                    .getProducts().get(product_index.get(selector_index)).getProductName());
//
//                            view_animator.showPrevious();
//                        }
//                    });
//
//                    linear_done.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            view_animator.setInAnimation(getActivity(), R.anim.slide_from_left);
//
//                            txt_customize.setText("Customize Your Package");
//                            txt_select.setText("Please Select");
//
//                            txt_done_apply.setText(getResources().getString(R.string.done));
//
//                            txt_product.setText(dealService.getSelectors().get(selector_index).getServices()
//                                    .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                    .getProducts().get(product_index.get(selector_index)).getProductName());
//
//                            linear_done.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    logDealBottomSheetDoneEvent();
//                                    logDealBottomSheetDoneFireBaseEvent();
//                                    addServices(false);
//                                    dismiss();
//                                }
//                            });
//
//                            txt_cancel.setText("View Cart");
//                            txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.alter_));
//
//                            linear_cancel.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    logDealBottomSheetCancelFirBaseEvent();
//                                    logDealBottomSheetCancelEvent();
//
//                                    addServices(true);
//
//
//                                    dismiss();
//                                }
//                            });
//
//                            view_animator.showPrevious();
//                        }
//                    });
//
//                    linear_items.removeAllViews();                  //removing the views first...no duplication
//
//                    LinearLayout customize_= (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.edit_service, null);
//                    TextView txt_customize_= (TextView)customize_.findViewById(R.id.txt_customize_);
//                    txt_customize_.setText("Product");
//                    linear_items.addView(customize_);
//
//                    final List<RadioButton> list_radio_= new ArrayList<>();
//
//                    for(int j=0;j<dealService.getSelectors().get(selector_index).getServices().
//                            get(service_index.get(selector_index)).getBrands()
//                            .get(brand_index.get(selector_index)).getProducts().size();j++){
//                        final int index= j;
//
//                        View view_= LayoutInflater.from(getActivity()).
//                                inflate(R.layout.bottomsheet_items, null, false);
//
//                        LinearLayout linear_click= (LinearLayout)view_.findViewById(R.id.linear_click);
//                        TextView txt_name= (TextView)view_.findViewById(R.id.txt_name);
//                        TextView txt_price= (TextView)view_.findViewById(R.id.txt_price);
//
//                        txt_name.setText(dealService.getSelectors().get(selector_index).getServices().
//                                get(service_index.get(selector_index)).
//                                getBrands().get(brand_index.get(selector_index)).getProducts().get(index).getProductName());
//
//
//                        final RadioButton radio_= (RadioButton)view_.findViewById(R.id.radio_);
//                        radio_.setClickable(false);
//                        list_radio_.add(radio_);            //adding the radio button in radio list
//
//                        if(dealService.getSelectors().get(selector_index).getServices().get(service_index.get(selector_index))
//                                .getBrands().get(brand_index.get(selector_index)).getProducts().get(j).isCheck()){
//
//                            radio_.setChecked(true);
//                            list_radio_.get(j).setChecked(true);
//                        }else{
//
//                            radio_.setChecked(false);
//                            list_radio_.get(j).setChecked(false);
//                        }
//
//                        linear_click.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//
//                                if(list_radio_.get(index).isChecked()){
//
//                                    list_radio_.get(index).setChecked(false);
//                                }else{
//
//                                    list_radio_.get(index).setChecked(true);
//                                }
//
//                                //updating view
//                                int list_radio_size=0;
//                                for(int k=0;k<list_radio_.size();k++){
//
//                                    if(k==index && list_radio_.get(index).isChecked()){     //conditions yaha likh
//
//                                        product_index.set(selector_index, index);
//                                        dealService.getSelectors().get(selector_index).getServices().get(service_index.get(selector_index))
//                                                .getBrands().get(brand_index.get(selector_index)).getProducts().get(index).setCheck(true);
//
//                                        package_service.setProduct_name(dealService.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(index).getProductName());
//                                        package_service.setProduct_id(dealService.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(index).getProductId());
//                                        package_services_list.set(selector_index, package_service);
//
//                                    }else{              //baki sare un check
//
//                                        list_radio_size++;
//                                        list_radio_.get(k).setChecked(false);
//                                        dealService.getSelectors().get(selector_index).getServices().get(service_index.get(selector_index))
//                                                .getBrands().get(brand_index.get(selector_index)).getProducts().get(k).setCheck(false);
//                                    }
//                                }
//
//                                //agar koi bhi select nai kara toh current wale ko select karao
//                                if(list_radio_size==list_radio_.size()){                            //aur conditions yaha likh
//
//                                    product_index.set(selector_index, index);
//                                    list_radio_.get(index).setChecked(true);
//                                    dealService.getSelectors().get(selector_index).getServices().get(service_index.get(selector_index))
//                                            .getBrands().get(brand_index.get(selector_index)).getProducts().get(index).setCheck(true);
//
//
//                                    package_service.setProduct_name(dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(index).getProductName());
//                                    package_service.setProduct_id(dealService.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(index).getProductId());
//                                    package_services_list.set(selector_index, package_service);
//
//                                }
//                            }
//                        });
//
//                        linear_items.addView(view_);
//                    }
//
//                    view_animator.showNext();
//                }
//            });
//
//            if(!dealService.getSelectors().get(i).isShowToUser() ||
//                    (!isService && !has_brand.get(selector_index) && !has_product.get(selector_index))){

////                View view_txt= LayoutInflater.from(getActivity()).
//                        inflate(R.layout.combo_newcombo_txt, null, false);

            LinearLayout linear_service= (LinearLayout) LayoutInflater.from(view.getContext()).
                    inflate(R.layout.bottom_sheet_services, null, false);
            TextView txt_name= linear_service.findViewById(R.id.txt_name);
            final TextView txt_des= linear_service.findViewById(R.id.txt_description);
            TextView txt_menu_price_= linear_service.findViewById(R.id.txt_menu_price);
            TextView txt_save_per_= linear_service.findViewById(R.id.txt_save_per);
            TextView txt_price_= linear_service.findViewById(R.id.txt_price);
            LinearLayout linear_change= linear_service.findViewById(R.id.linear_change);
            LinearLayout linear_show= linear_service.findViewById(R.id.linear_show);

//            RelativeLayout relative_price= linear_service.findViewById(R.id.relative_price);
//            relative_price.setVisibility(View.GONE);

//            LinearLayout linear_des= linear_service.findViewById(R.id.linear_des);

            txt_name.setText(dealService.getSelectors().get(selector_index).getServiceTitle());

            //description ka stuff
            String brand_name= package_service.getBrand_name()==null ||
                    package_service.getBrand_name().equalsIgnoreCase("")?"":package_service.getBrand_name();
            String product_name= package_service.getProduct_name()==null ||
                    package_service.getProduct_name().equalsIgnoreCase("")?"":package_service.getProduct_name();

            if(brand_name.equalsIgnoreCase("") &&
                    product_name.equalsIgnoreCase("")){

//                linear_des.setVisibility(View.GONE);
                txt_des.setText("");
            }
            else{

                if(!product_name.equalsIgnoreCase(""))
                    brand_name= brand_name + " - ";

//                linear_des.setVisibility(View.VISIBLE);
                txt_des.setText(brand_name+ product_name);
            }

            if(!dealService.getSelectors().get(i).isShowToUser() ||
                    (!isService && !has_brand.get(selector_index) && !has_product.get(selector_index))){

                linear_show.setVisibility(View.GONE);
            }else{

                linear_show.setVisibility(View.VISIBLE);
                linear_change.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        logPackageBottomSheetChangeEvent(dealService.getSelectors().get(selector_index).getServiceTitle());

                        package_service.setName(dealService.getSelectors().get(selector_index).getServiceTitle());
                        package_service.setDescription(dealService.getSelectors().get(selector_index).getDescription());

                        DialogFragmentServices fragment= new DialogFragmentServices();
                        Bundle bundle= new Bundle();
                        bundle.putString(DialogFragmentServices.SERVICE_DATA,
                                new Gson().toJson(dealService.getSelectors().get(selector_index), Selector.class));
                        bundle.putString(DialogFragmentServices.DATA,
                                new Gson().toJson(package_service, PackageService.class));
                        bundle.putString("isService","false");
                        fragment.setArguments(bundle);
                        fragment.show(getActivity().getFragmentManager(),
                                DialogFragmentServices.DIALOG);
                        fragment.setListener(new DialogFragmentServices.FragmentListener() {
                            @Override
                            public void onClick(PackageService packageService) {
                                Log.i("humkahahai", "int the gharh");
                                package_services_list.set(selector_index, packageService);

                                String brand_name= packageService.getBrand_name()==null ||
                                        packageService.getBrand_name().equalsIgnoreCase("")?"":packageService.getBrand_name();
                                String product_name= packageService.getProduct_name()==null ||
                                        packageService.getProduct_name().equalsIgnoreCase("")?"":packageService.getProduct_name();

                                if(brand_name.equalsIgnoreCase("") &&
                                        product_name.equalsIgnoreCase(""))
                                    txt_des.setText("");
                                else
                                if(!product_name.equalsIgnoreCase(""))
                                    brand_name= brand_name + " - ";

                                txt_des.setText(brand_name+ product_name);


                            }
                        });
                    }
                });
            }



            linear_.addView(linear_service);
            //show sirf service name :D



//            }else
//                linear_.addView(view_);

        }


        linear_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logDealBottomSheetDoneEvent();
                logDealBottomSheetDoneFireBaseEvent();
                logPackageBottomSheetAddEvent(dealService.getName());
                addServices(false);
                dismiss();
            }
        });

        linear_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logDealBottomSheetCancelFirBaseEvent();
                logDealBottomSheetCancelEvent();
                logPackageBottomSheetViewCartEvent(dealService.getName());

                addServices(true);


                dismiss();
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                setToolTip(txt_cancel);
            }
        }, 500);

        return view;

    }


    private void setToolTip(TextView txt_cancel){
        View view = getActivity().getLayoutInflater().inflate(R.layout.tooltip_bottomsheet, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT );
        view.setLayoutParams(params);
        new EasyDialog(getActivity())
                // .setLayoutResourceId(R.layout.layout_tip_content_horizontal)//layout resource id
                .setLayout(view)
                .setBackgroundColor(getActivity().getResources().getColor(R.color.tooltip_bg))
                // .setLocation(new location[])//point in screen
                .setLocationByAttachedView(txt_cancel)
                .setGravity(EasyDialog.GRAVITY_BOTTOM)
                .setAnimationTranslationShow(EasyDialog.DIRECTION_X, 1000, -600, 100, -50, 50, 0)
                .setAnimationAlphaShow(100, 0.3f, 1.0f)
                .setAnimationTranslationDismiss(EasyDialog.DIRECTION_X, 500, -50, 800)
                .setAnimationAlphaDismiss(200, 1.0f, 0.0f)
                .setTouchOutsideDismiss(true)
                .setOutsideColor(getActivity().getResources().getColor(android.R.color.transparent))
                .setMatchParent(true)
                .setMarginLeftAndRight(36, 36)
                .show();
    }


    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logDealBottomSheetChangeEvent () {
        Log.e("changeDeal","fine");
        logger.logEvent(AppConstant.DealBottomSheetChange);
    }
    public void logPackageBottomSheetAddEvent (String packageName) {
        Log.e("logPackageBttmAddEvent","fine"+packageName);

        Bundle params = new Bundle();
        params.putString(AppConstant.PackageName, packageName);
        logger.logEvent(AppConstant.PackageBottomSheetAdd, params);
    }
    public void logPackageBottomSheetViewCartEvent (String packageName) {
        Log.e("logPackageViewCart","fine"+packageName);

        Bundle params = new Bundle();
        params.putString(AppConstant.PackageName, packageName);
        logger.logEvent(AppConstant.PackageBottomSheetViewCart, params);
    }


    public void logDealBottomSheetDoneEvent () {
        Log.e("doneDeal","fine");

        logger.logEvent(AppConstant.DealBottomSheetDone);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logDealBottomSheetCancelEvent () {
        Log.e("cancelDeal","fine");

        logger.logEvent(AppConstant.DealBottomSheetCancel);
    }

    public void logDealBottomSheetCancelFirBaseEvent () {
        Log.e("cancelDealfirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.DealBottomSheetCancel,bundle);
    }

    public void logDealBottomSheetDoneFireBaseEvent () {
        Log.e("doneDealFireBase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.DealBottomSheetDone,bundle);
    }
    public void logDealBottomSheetChangeFirebaseEvent () {
        Log.e("changeDealFireBase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.DealBottomSheetChange,bundle);
    }

//    private void saveToCart(){

    private void addServices(boolean isAlter){

        DealComboEvent event= new DealComboEvent();

        event.setService_name(dealService.getName());

        event.setService_deal_id(""+dealService.getParlorDealId());             //isme string hai
        event.setDealId(dealService.getDealId());                       //isme ha like 140, 156..    yeh important hai

        event.setType(dealService.getDealType());
        event.setParlorTypes(dealService.getParlorTypes());

        String primary_key="";
        for(int i=0;i<package_services_list.size();i++){

            int service_code= package_services_list.get(i).getService_code();
            String service_id= package_services_list.get(i).getService_id();
            String brand_id= package_services_list.get(i).getBrand_id()==null?
                    "":package_services_list.get(i).getBrand_id();
            String product_id=package_services_list.get(i).getProduct_id()==null?
                    "":package_services_list.get(i).getProduct_id();
            primary_key+= ""+service_code+ service_id+ brand_id+product_id;

            Log.i("primary_key", "value in: "+ service_code+
                    "  " + service_id+  " "+ brand_id+  " "+ product_id);
        }
        event.setPrimary_key(primary_key);
        Log.i("primary_key", "values: "+primary_key);

        event.setDescription(dealService.getDescription());
        event.setPackage_services_list(package_services_list);

        EventBus.getDefault().post(event);

    }

//    private void addServices(boolean isAlter){
//
//        List<UserServices> list= new ArrayList<>();
//        for(int i=0;i<package_services_list.size();i++){
//
//            int service_code= package_services_list.get(i).getService_code();
//            String service_id= package_services_list.get(i).getService_id();
//            String brand_id= package_services_list.get(i).getBrand_id()==null?
//                    "":package_services_list.get(i).getBrand_id();
//            String product_id=package_services_list.get(i).getProduct_id()==null?
//                    "":package_services_list.get(i).getProduct_id();
//
//            String brand_name= package_services_list.get(i).getBrand_name()==null || package_services_list.get(i).getBrand_name()
//                    .equalsIgnoreCase("")?"":package_services_list.get(i).getBrand_name();
//            String product_name= package_services_list.get(i).getProduct_name()==null|| package_services_list.get(i).getProduct_name()
//                    .equalsIgnoreCase("")?"":package_services_list.get(i).getProduct_name();
//
//            String primary_key= ""+service_code+ service_id+ brand_id+product_id;
//
//            Log.i("primary_keyey", "value in: "+ service_code+
//                    "  " + service_id+  " "+ brand_id+  " "+ product_id);
//
//            UserServices service= new UserServices();
//            service.setName(package_services_list.get(i).getService_name());
//
//            service.setService_code(service_code);
//            service.setPrice_id(service_code);
//            service.setService_id(package_services_list.get(i).getService_id());
//
//            service.setService_deal_id(dealService.getParlorDealId());
//            service.setDealId(dealService.getDealId());
//
//            service.setType("newCombo");
//
//            service.setBrand_id(package_services_list.get(i).getBrand_id());
//            service.setProduct_id(package_services_list.get(i).getProduct_id());
//
//            service.setDescription(brand_name+" "+product_name);
//            service.setPrimary_key(primary_key);
//
//            list.add(service);
//        }
//
//        EventBus.getDefault().post(new PackageListEvent(list, dealService.getName(), isAlter));
//
//        dismiss();
//    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }
    public void logPackageBottomSheetChangeEvent (String serviceName) {
        Log.e("logPackageBottomSChange","fine"+serviceName);

        Bundle params = new Bundle();
        params.putString(AppConstant.ServiceName, serviceName);
        logger.logEvent(AppConstant.PackageBottomSheetChange, params);
    }
}
