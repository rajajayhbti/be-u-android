package com.beusalons.android.Model.SalonHome.salonDepartments;

import com.beusalons.android.Model.SalonHome.Image;
import com.beusalons.android.Model.SalonHome.Membership;
import com.beusalons.android.Model.SalonHome.Subscription;
import com.beusalons.android.Model.SalonHome.WelcomeOffer;
import com.beusalons.android.Model.newServiceDeals.Service;

import java.util.List;

/**
 * Created by Ashish Sharma on 2/15/2018.
 */

public class SalonDeparttmentsData {
    private String name;
    private String parlorId;

    private Subscription subscriptions;

    private List<Service> departments = null;

    float tax;

    private WelcomeOffer welcomeOffer;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }

    public Subscription getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(Subscription subscriptions) {
        this.subscriptions = subscriptions;
    }

    public List<Service> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Service> departments) {
        this.departments = departments;
    }

    public float getTax() {
        return tax;
    }

    public void setTax(float tax) {
        this.tax = tax;
    }

    public WelcomeOffer getWelcomeOffer() {
        return welcomeOffer;
    }

    public void setWelcomeOffer(WelcomeOffer welcomeOffer) {
        this.welcomeOffer = welcomeOffer;
    }
}
