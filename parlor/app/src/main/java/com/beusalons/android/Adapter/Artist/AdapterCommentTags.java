package com.beusalons.android.Adapter.Artist;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beusalons.android.Model.ArtistProfile.CreativeField;
import com.beusalons.android.R;

import java.util.List;


/**
 * Created by Ajay on 2/1/2018.
 */

public class AdapterCommentTags extends RecyclerView.Adapter<AdapterCommentTags.ViewHolder> {

    private List<CreativeField> list;
   private boolean isHome=false;
    public AdapterCommentTags(List<CreativeField> list, boolean isHome){
        this.list= list;
        this.isHome=isHome;
    }

    public void setList(List<CreativeField> list){
        this.list= list;
        notifyDataSetChanged();
    }




    @Override
    public AdapterCommentTags.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LinearLayout linear= (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_creative_field_row, parent, false);
        return new AdapterCommentTags.ViewHolder(linear);
    }

    @Override
    public void onBindViewHolder(AdapterCommentTags.ViewHolder holder, int position) {

        final LinearLayout linear_= holder.linear;
        final int index= position;
        final TextView txt_char= linear_.findViewById(R.id.txt_char);
        final TextView txt_name= linear_.findViewById(R.id.txt_name);
        final RelativeLayout relative_= linear_.findViewById(R.id.relative_);
        final TextView txt_more= linear_.findViewById(R.id.txt_more);

        if (isHome){
            if (index<2){
                txt_char.setText(""+list.get(index).getCollectionName().charAt(0));

                txt_name.setText(list.get(index).getCollectionName());

                relative_.setBackgroundResource(R.drawable.shape_txt_primary);
                txt_name.setTextColor(ContextCompat.getColor(linear_.getContext(), R.color.colorPrimaryText));
                Log.e("vis fromhome","home");
                if (index==1){
                    txt_more.setVisibility(View.VISIBLE);
                }

            }else{
                relative_.setVisibility(View.GONE);
                Log.e("gone fromhome","home");

            }
        }else{
            txt_char.setText(""+list.get(index).getCollectionName().charAt(0));

            txt_name.setText(list.get(index).getCollectionName());

            relative_.setBackgroundResource(R.drawable.shape_txt_primary);
            txt_name.setTextColor(ContextCompat.getColor(linear_.getContext(), R.color.colorPrimaryText));
            Log.e("not fromhome","home");

        }


        if (position%5==0){
            txt_char.setBackgroundResource(R.drawable.shape_circle_primary);
        }else if (position%5==1){
            txt_char.setBackgroundResource(R.drawable.creative_drawble_one);

        }else if (position%5==2){
            txt_char.setBackgroundResource(R.drawable.creative_drawble_two);

        }else if (position%5==3){
            txt_char.setBackgroundResource(R.drawable.creative_drawble_three);

        }else if (position%5==4){
            txt_char.setBackgroundResource(R.drawable.shape_circle_primary);

        }

    }




    @Override
    public int getItemCount() {
        if(list!=null &&
                list.size()>0)
            return list.size();
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private LinearLayout linear;
        public ViewHolder(LinearLayout itemView) {
            super(itemView);
            linear= itemView;
        }
    }
}
