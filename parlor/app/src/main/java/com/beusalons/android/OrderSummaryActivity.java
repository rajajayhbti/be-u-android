package com.beusalons.android;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.beusalons.android.Adapter.OrderSummaryListAdapter;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.Appointments.PastData;
import com.beusalons.android.Model.Appointments.Services;
import com.beusalons.android.Model.Appointments.UpcomingData;
import com.beusalons.android.Utility.Utility;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class OrderSummaryActivity extends AppCompatActivity {

    public static final String BOOKED_APPOINTMENT= "booked_appointment";
    public static final String COMPLETED_APPOINTMENT= "completed_appointment";


    private TextView txt_total, txt_discount, txt_tax, txt_grand_total, txt_time,txtViewActionBarName;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    List<Services> services_list= new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_summary);
        setToolBar();

        txt_total=(TextView)findViewById(R.id.txt_order_summary_total);
        txt_discount=(TextView)findViewById(R.id.txt_order_summary_discount);
        txt_tax=(TextView)findViewById(R.id.txt_order_summary_tax);
        txt_grand_total=(TextView)findViewById(R.id.txt_order_summary_grand_total);
        txt_time= (TextView)findViewById(R.id.txt_order_summary_time);

        recyclerView= (RecyclerView)findViewById(R.id.rcy_order_summary);
        layoutManager= new LinearLayoutManager(OrderSummaryActivity.this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setHasFixedSize(true);

        Bundle bundle= getIntent().getExtras();
        PastData pastData= new PastData();
        UpcomingData upcomingData= new UpcomingData();
        if(bundle!=null &&
                bundle.containsKey(COMPLETED_APPOINTMENT)){

            pastData= new Gson().fromJson(bundle.getString(COMPLETED_APPOINTMENT), PastData.class);

            txt_total.setText(AppConstant.CURRENCY+ String.valueOf(pastData.getSubtotal()));
            txt_discount.setText(AppConstant.CURRENCY+String.valueOf(pastData.getDiscount()));
            txt_tax.setText(AppConstant.CURRENCY+String.valueOf(pastData.getTax()));
            txt_grand_total.setText(AppConstant.CURRENCY+String.valueOf(pastData.getPayableAmount()));
            txt_time.setText(pastData.getStartsAt());

            services_list= pastData.getServices();
            adapter= new OrderSummaryListAdapter(services_list);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }else{

            upcomingData= new Gson().fromJson(bundle.getString(BOOKED_APPOINTMENT), UpcomingData.class);

            txt_total.setText(AppConstant.CURRENCY+ String.valueOf(upcomingData.getSubtotal()));
            txt_discount.setText(AppConstant.CURRENCY+String.valueOf(upcomingData.getDiscount()));
            txt_tax.setText(AppConstant.CURRENCY+String.valueOf(upcomingData.getTax()));
            txt_grand_total.setText(AppConstant.CURRENCY+String.valueOf(upcomingData.getPayableAmount()));
            txt_time.setText(upcomingData.getStartsAt());
            Log.i("txt", "value: "+ upcomingData.getStartsAt());

            services_list= upcomingData.getServices();
            adapter= new OrderSummaryListAdapter(services_list);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }


    }
    private void setToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(getResources().getString(R.string.txt_order_summary_static_order));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);

        }


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
