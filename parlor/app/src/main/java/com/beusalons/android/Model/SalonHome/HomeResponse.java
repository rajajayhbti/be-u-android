package com.beusalons.android.Model.SalonHome;

/**
 * Created by myMachine on 5/26/2017.
 */

public class HomeResponse {

    private Boolean success;
    private Data data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
