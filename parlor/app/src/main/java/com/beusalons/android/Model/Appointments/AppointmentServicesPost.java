package com.beusalons.android.Model.Appointments;

import com.beusalons.android.Model.Appointments.CeateAppointMent.PackageService_;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 12/12/2016.
 */

public class AppointmentServicesPost {

    private int serviceCode;
    private int code;                         //price id i.e. price code
    private String serviceId;                   //deal ya service id

    private String type;                    // this is service string or deal name string or just deal in case of free service offer
    private Integer quantity;
    private int addition;                   //prices addition
    private int typeIndex;

    private String brandId;
    private String productId;
    private List<PackageService_> services;

    private Boolean frequencyUsed;              // this variable is for offer, if user have selected the free service offer

    public List<PackageService_> getServices() {
        return services;
    }

    public void setServices(List<PackageService_> services) {
        this.services = services;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Boolean getFrequencyUsed() {
        return frequencyUsed;
    }

    public void setFrequencyUsed(Boolean frequencyUsed) {
        this.frequencyUsed = frequencyUsed;
    }

    public int getTypeIndex() {
        return typeIndex;
    }

    public void setTypeIndex(int typeIndex) {
        this.typeIndex = typeIndex;
    }

    public int getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(int serviceCode) {
        this.serviceCode = serviceCode;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public int getAddition() {
        return addition;
    }

    public void setAddition(int addition) {
        this.addition = addition;
    }


}
