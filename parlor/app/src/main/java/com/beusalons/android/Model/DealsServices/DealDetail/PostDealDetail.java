package com.beusalons.android.Model.DealsServices.DealDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashish Sharma on 6/16/2017.
 */

public class PostDealDetail {

    private int dealId;
    private int quantity;
    private List<ServicesList> services;


    public int getDealId() {
        return dealId;
    }

    public void setDealId(int dealId) {
        this.dealId = dealId;
    }

    public List<ServicesList> getServices() {
        return services;
    }

    public void setServices(List<ServicesList> services) {
        this.services = services;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
