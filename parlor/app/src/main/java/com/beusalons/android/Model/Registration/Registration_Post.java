package com.beusalons.android.Model.Registration;

import java.io.Serializable;

/**
 * Created by myMachine on 10/31/2016.
 */

public class Registration_Post implements Serializable {

    private String name;
    private String emailId;
    private String phoneNumber;
    private String password;
    private String accessToken;
    private String profilePic;
    private String gender;
    private int mobile;                         //send 1 in case of app
    private int socialLoginType;
    private String referCode;

    public String getReferCode() {
        return referCode;
    }

    public void setReferCode(String referCode) {
        this.referCode = referCode;
    }

    public int getSocialLoginType() {
        return socialLoginType;
    }

    public void setSocialLoginType(int socialLoginType) {
        this.socialLoginType = socialLoginType;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getMobile() {
        return mobile;
    }

    public void setMobile(int mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
