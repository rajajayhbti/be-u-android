package com.beusalons.android.Model.ArtistProfile;

import java.util.List;

/**
 * Created by Ajay on 1/31/2018.
 */

public class ArtistFollowed {
    private String _id;
    private String createdAt;
    private String updatedAt;
    private String firstName;
    private String password;
    private String phoneNumber;
    private String facebookId;
    private Integer __v;
    private Integer realOtp;
    private Object emailId;
    private String portfolioDescription;
    private Integer myLikedPost;
    private List<Object> likedPosts = null;
    private List<Object> portfolioCollection = null;
    private Integer portfolioLikes;
    private Integer portfolioRating;
    private List<String> portfolioPosition;

    private String portfolioProfile;
    private Integer portfolioPosts;
    private Object iosVersion;
    private Boolean isProfessional;
    private Object androidVersion;
    private Integer mobile;
    private String address;
    private String gender;
    private String lastName;
    private double longitude;
    private Integer registerLongitude;
    private double latitude;
    private Integer registerLatitude;
    private List<Object> restrictPortfolio = null;
    private Integer phoneVerification;
    private String profilePic;
    private String accesstoken;
    private Integer firstTimeVerified;
    private Boolean isRegistered;
    private Integer credits;
    private Object firebaseIdIOS;
    private List<String> followingArtist = null;
    private List<String> followingCollection = null;
    private Object firebaseId;
    private Object googleId;

    private int totalPosts;

    public int getTotalPosts() {
        return totalPosts;
    }

    public void setTotalPosts(int totalPosts) {
        this.totalPosts = totalPosts;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public Integer get__v() {
        return __v;
    }

    public void set__v(Integer __v) {
        this.__v = __v;
    }

    public Integer getRealOtp() {
        return realOtp;
    }

    public void setRealOtp(Integer realOtp) {
        this.realOtp = realOtp;
    }

    public Object getEmailId() {
        return emailId;
    }

    public void setEmailId(Object emailId) {
        this.emailId = emailId;
    }

    public String getPortfolioDescription() {
        return portfolioDescription;
    }

    public void setPortfolioDescription(String portfolioDescription) {
        this.portfolioDescription = portfolioDescription;
    }

    public Integer getMyLikedPost() {
        return myLikedPost;
    }

    public void setMyLikedPost(Integer myLikedPost) {
        this.myLikedPost = myLikedPost;
    }

    public List<Object> getLikedPosts() {
        return likedPosts;
    }

    public void setLikedPosts(List<Object> likedPosts) {
        this.likedPosts = likedPosts;
    }

    public List<Object> getPortfolioCollection() {
        return portfolioCollection;
    }

    public void setPortfolioCollection(List<Object> portfolioCollection) {
        this.portfolioCollection = portfolioCollection;
    }

    public Integer getPortfolioLikes() {
        return portfolioLikes;
    }

    public void setPortfolioLikes(Integer portfolioLikes) {
        this.portfolioLikes = portfolioLikes;
    }

    public Integer getPortfolioRating() {
        return portfolioRating;
    }

    public void setPortfolioRating(Integer portfolioRating) {
        this.portfolioRating = portfolioRating;
    }

    public List<String> getPortfolioPosition() {
        return portfolioPosition;
    }

    public void setPortfolioPosition(List<String> portfolioPosition) {
        this.portfolioPosition = portfolioPosition;
    }

    public String getPortfolioProfile() {
        return portfolioProfile;
    }

    public void setPortfolioProfile(String portfolioProfile) {
        this.portfolioProfile = portfolioProfile;
    }

    public Integer getPortfolioPosts() {
        return portfolioPosts;
    }

    public void setPortfolioPosts(Integer portfolioPosts) {
        this.portfolioPosts = portfolioPosts;
    }

    public Object getIosVersion() {
        return iosVersion;
    }

    public void setIosVersion(Object iosVersion) {
        this.iosVersion = iosVersion;
    }

    public Boolean getProfessional() {
        return isProfessional;
    }

    public void setProfessional(Boolean professional) {
        isProfessional = professional;
    }

    public Object getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(Object androidVersion) {
        this.androidVersion = androidVersion;
    }

    public Integer getMobile() {
        return mobile;
    }

    public void setMobile(Integer mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }



    public Integer getRegisterLongitude() {
        return registerLongitude;
    }

    public void setRegisterLongitude(Integer registerLongitude) {
        this.registerLongitude = registerLongitude;
    }



    public Integer getRegisterLatitude() {
        return registerLatitude;
    }

    public void setRegisterLatitude(Integer registerLatitude) {
        this.registerLatitude = registerLatitude;
    }

    public List<Object> getRestrictPortfolio() {
        return restrictPortfolio;
    }

    public void setRestrictPortfolio(List<Object> restrictPortfolio) {
        this.restrictPortfolio = restrictPortfolio;
    }

    public Integer getPhoneVerification() {
        return phoneVerification;
    }

    public void setPhoneVerification(Integer phoneVerification) {
        this.phoneVerification = phoneVerification;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getAccesstoken() {
        return accesstoken;
    }

    public void setAccesstoken(String accesstoken) {
        this.accesstoken = accesstoken;
    }

    public Integer getFirstTimeVerified() {
        return firstTimeVerified;
    }

    public void setFirstTimeVerified(Integer firstTimeVerified) {
        this.firstTimeVerified = firstTimeVerified;
    }

    public Boolean getRegistered() {
        return isRegistered;
    }

    public void setRegistered(Boolean registered) {
        isRegistered = registered;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public Object getFirebaseIdIOS() {
        return firebaseIdIOS;
    }

    public void setFirebaseIdIOS(Object firebaseIdIOS) {
        this.firebaseIdIOS = firebaseIdIOS;
    }

    public List<String> getFollowingArtist() {
        return followingArtist;
    }

    public void setFollowingArtist(List<String> followingArtist) {
        this.followingArtist = followingArtist;
    }

    public List<String> getFollowingCollection() {
        return followingCollection;
    }

    public void setFollowingCollection(List<String> followingCollection) {
        this.followingCollection = followingCollection;
    }

    public Object getFirebaseId() {
        return firebaseId;
    }

    public void setFirebaseId(Object firebaseId) {
        this.firebaseId = firebaseId;
    }

    public Object getGoogleId() {
        return googleId;
    }

    public void setGoogleId(Object googleId) {
        this.googleId = googleId;
    }
}
