package com.beusalons.android.Model.HomePage;

import com.beusalons.android.Model.HomeFragmentModel.DiscountRules;

import java.util.List;

/**
 * Created by myMachine on 22-Jan-18.
 */

public class ApiResponse {

    private Boolean success;
    private Data data;

    public class Data{

        private List<HomeDatum> homeData = null;
        private Scorecard scorecard;
        private Banner banner;
        private String serverTime;
        private List<DiscountRules> discountRules=null;

        public List<DiscountRules> getDiscountRules() {
            return discountRules;
        }

        public void setDiscountRules(List<DiscountRules> discountRules) {
            this.discountRules = discountRules;
        }

        public String getServerTime() {
            return serverTime;
        }

        public void setServerTime(String serverTime) {
            this.serverTime = serverTime;
        }

        public List<HomeDatum> getHomeData() {
            return homeData;
        }

        public void setHomeData(List<HomeDatum> homeData) {
            this.homeData = homeData;
        }

        public Scorecard getScorecard() {
            return scorecard;
        }

        public void setScorecard(Scorecard scorecard) {
            this.scorecard = scorecard;
        }

        public Banner getBanner() {
            return banner;
        }

        public void setBanner(Banner banner) {
            this.banner = banner;
        }
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
