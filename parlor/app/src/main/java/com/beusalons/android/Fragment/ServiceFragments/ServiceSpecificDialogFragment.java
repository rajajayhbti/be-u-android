package com.beusalons.android.Fragment.ServiceFragments;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Adapter.NewServiceDeals.BrandsListAdapter;
import com.beusalons.android.Adapter.NewServiceDeals.LengthListAdapter;
import com.beusalons.android.Adapter.NewServiceDeals.PackageListAdapter;
import com.beusalons.android.Adapter.NewServiceDeals.ProductsListAdapter;
import com.beusalons.android.Adapter.NewServiceDeals.UpgradeListAdapter;
import com.beusalons.android.BookingSummaryActivity;
import com.beusalons.android.Event.NewServicesEvent.AddCartEvent;
import com.beusalons.android.Event.NewServicesEvent.AddServiceEvent;
import com.beusalons.android.Event.NewServicesEvent.BrandEvent;
import com.beusalons.android.Event.NewServicesEvent.EditPackageListEvent;
import com.beusalons.android.Event.NewServicesEvent.ProductEvent;
import com.beusalons.android.Event.NewServicesEvent.SingleBrandProductEvent;
import com.beusalons.android.Event.NewServicesEvent.UpgradeEvent_;
import com.beusalons.android.Fragment.ServiceDesciptionFragment;
import com.beusalons.android.Fragment.UserCartFragment;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.AddServiceUserCart.AddService_response;
import com.beusalons.android.Model.AddServiceUserCart.UserCart_post;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Service;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Task.MultipleServicesTask;
import com.beusalons.android.Task.UserCartTask;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by myMachine on 5/23/2017.
 */

public class ServiceSpecificDialogFragment extends DialogFragment {

    public static final String HOME_RESPONSE= "home_response";

    private CoordinatorLayout coordinator_;
    private BrandsListAdapter brands_adapter;       //brand list adapter
    private ProductsListAdapter products_adapter;            //products list adapter
    private LengthListAdapter type_adapter;                  //types adapter


    private Service service;
    private UserCart user_cart;
//    private Slabs slabs;


    private TextView txt_product_name ,txtView_header_upgrade,txtView_header_package;
    private TextView txt_no_thanks;

    private boolean hasBrand, hasProducts, hasTypes;

    private boolean add_service= false;             //add service-. when no brand & produce &&
    private boolean has_service= false, add_= false;           //add_ -> when one brand & product

    View view_for_lenght, viewForBrand;
    AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;

    private String home_response;

    private LinearLayout linear_header_show_details;
    private View view_package_line, view_upgrade_border;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);          //no action bar space
        service= new Service();

        View view = inflater.inflate(R.layout.fragment_services_specific_dialog, container);

        coordinator_= (CoordinatorLayout)view.findViewById(R.id.coordinator_);
        logger = AppEventsLogger.newLogger(getActivity());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());

        LinearLayout linear_cart, linear_no_thanks, linear_bottom;

        view_package_line= view.findViewById(R.id.view_package_line);
        view_upgrade_border= view.findViewById(R.id.view_upgrade_border);

        linear_bottom= (LinearLayout)view.findViewById(R.id.linear_bottom);
        linear_cart= (LinearLayout)view.findViewById(R.id.linear_cart);
        linear_no_thanks= (LinearLayout)view.findViewById(R.id.linear_no_thanks);
        txt_no_thanks= (TextView)view.findViewById(R.id.txt_no_thanks);
        linear_header_show_details=view.findViewById(R.id.linear_header_show_details);
        Bundle bundle= getArguments();
        if(bundle!=null && bundle.containsKey("service")){

            service= new Gson().fromJson(bundle.getString("service"), Service.class);
            user_cart= new Gson().fromJson(bundle.getString("user_cart"), UserCart.class);
//            slabs= new Gson().fromJson(bundle.getString("slabs"), Slabs.class);
            add_service= bundle.getBoolean("add_service", false);
            add_= bundle.getBoolean("add_", false);

            home_response= bundle.getString(HOME_RESPONSE, "");

            //lets say no brand, product types tab yaha service add karta hoon
//            if(add_service){
//
//                Log.i("editservice", "in the add service");
//                linear_bottom.setVisibility(View.VISIBLE);
//                linear_no_thanks.setVisibility(View.VISIBLE);
//                linear_cart.setVisibility(View.GONE);
//
//                txt_no_thanks.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//
//                        Log.i("buttonclic", "single service pe");
//
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                addService();
//                            }
//                        }, 250);
//
//                        dismiss();
//
//                    }
//                });
//
//            }else{
//
//                Log.i("editservice", "in the else pe hoon add service");
//                linear_bottom.setVisibility(View.GONE);
//            }
        }

        TextView txt_name= view.findViewById(R.id.txt_name);
        TextView txt_info= view.findViewById(R.id.txt_info);
        TextView txt_description= view.findViewById(R.id.txt_description);
        TextView txt_price= view.findViewById(R.id.txt_price);
        TextView txt_menu_price= view.findViewById(R.id.txt_menu_price);
        TextView txt_save_per= view.findViewById(R.id.txt_save_per);
        LinearLayout linear_share= view.findViewById(R.id.linear_share);
        LinearLayout linear_price= view.findViewById(R.id.linear_price);

        txt_name.setText(service.getName());
        if(service.getDescription()!=null &&
                !service.getDescription().equalsIgnoreCase("")){
            txt_description.setVisibility(View.VISIBLE);
            if (service.getDescription().length()>45)
            txt_description.setText(service.getDescription().substring(0,45)+"...");
            linear_header_show_details.setVisibility(View.VISIBLE);
        }else {
            linear_header_show_details.setVisibility(View.GONE);

            txt_description.setVisibility(View.GONE);

        }
           txt_info.setVisibility(View.GONE);


           linear_header_show_details.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   ServiceDesciptionFragment fragment= new ServiceDesciptionFragment();
                   Bundle bundle= new Bundle();
                   bundle.putString(ServiceDesciptionFragment.NAME, service.getName());
                   bundle.putString(ServiceDesciptionFragment.DESCRIPTION, service.getDescription());
                   bundle.putString(ServiceDesciptionFragment.TIME, service.getEstimatedTime());
                   fragment.setArguments(bundle);
                   fragment.show(getActivity().getFragmentManager(),
                           ServiceDesciptionFragment.THIS_FRAGMENT);
               }
           });


        if(service.getDealPrice()!=0){
            txt_menu_price.setVisibility(View.VISIBLE);
            txt_save_per.setVisibility(View.VISIBLE);
            int price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                    service.getDealPrice());
            int menu_price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                    service.getMenuPrice());

            txt_price.setText(AppConstant.CURRENCY+ price_);
            txt_menu_price.setText(AppConstant.CURRENCY+ menu_price_);
            txt_menu_price.setPaintFlags(txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            txt_save_per.setText(AppConstant.SAVE+" "+
                    (int) (100 -((price_*100)/
                            menu_price_))+"%");
        }else{
            txt_menu_price.setVisibility(View.GONE);
            txt_save_per.setVisibility(View.GONE);

            int price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                    service.getPrice());

            txt_price.setText(AppConstant.CURRENCY+price_);
        }


        LinearLayout linear_save_more= (LinearLayout)view.findViewById(R.id.linear_save_more);
        linear_save_more.setVisibility(View.GONE);

        txtView_header_package=(TextView)view.findViewById(R.id.txtView_header_package);
        txtView_header_upgrade=(TextView)view.findViewById(R.id.txtView_header_upgrade);
        LinearLayout linear_back= (LinearLayout)view.findViewById(R.id.linear_back);
        TextView txt_service_name= (TextView)view.findViewById(R.id.txt_service_name);

        linear_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        //header ka naam ab static hai
        txt_service_name.setText("Service Details");


        txtView_header_package.setText("You Have Chosen "+service.getName());
        txtView_header_upgrade.setText("You Have Chosen "+service.getName());

        TextView txt_checkout, txt_continue;
        txt_checkout= (TextView)view.findViewById(R.id.txt_checkout);
        txt_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logEditServiceCheckOutEvent();
                logEditServiceCheckOutFireBaseEvent();
                Intent intent= new Intent(getActivity(), BookingSummaryActivity.class);
                intent.putExtra("user_cart" ,new Gson().toJson(user_cart, UserCart.class));
                intent.putExtra(BookingSummaryActivity.HOME_RESPONSE, home_response);
                startActivity(intent);
            }
        });

        view_for_lenght=(View)view.findViewById(R.id.view_for_lenght);
        viewForBrand=(View)view.findViewById(R.id.view_for_brand);
        txt_continue= (TextView)view.findViewById(R.id.txt_continue);
        txt_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                logEditServiceContinueShopingEvent();
                logEditServiceContinueShopingFireBaseEvent();
            }
        });

        //brands
        LinearLayout linear_brand= (LinearLayout)view.findViewById(R.id.linear_brand);
        TextView txt_brand_name= (TextView)view.findViewById(R.id.txt_brand_name);
        RecyclerView rec_brand= (RecyclerView)view.findViewById(R.id.rec_brand);

        //products
        LinearLayout linear_products= (LinearLayout)view.findViewById(R.id.linear_products);
        txt_product_name= (TextView)view.findViewById(R.id.txt_product_name);
        RecyclerView rec_products= (RecyclerView)view.findViewById(R.id.rec_products);

        //length
        LinearLayout linear_length= (LinearLayout)view.findViewById(R.id.linear_length);
        TextView txt_length_name= (TextView)view.findViewById(R.id.txt_length_name);
        RecyclerView rec_length= (RecyclerView)view.findViewById(R.id.rec_length);

        //upgrade
        LinearLayout linear_upgrade= (LinearLayout)view.findViewById(R.id.linear_upgrade);
        TextView txt_upgrade_name= (TextView)view.findViewById(R.id.txt_upgrade_name);
        RecyclerView rec_upgrade= (RecyclerView)view.findViewById(R.id.rec_upgrade);

        //package
        LinearLayout linear_package= (LinearLayout)view.findViewById(R.id.linear_package);
        TextView txt_package_name= (TextView)view.findViewById(R.id.txt_package_name);
        RecyclerView rec_package= (RecyclerView)view.findViewById(R.id.rec_package);


        //checking if type hai kya
        hasTypes= false;
        if(service.getPrices()!=null &&
                service.getPrices().size()>0 &&
                service.getPrices().get(0).getAdditions()!=null &&
                service.getPrices().get(0).getAdditions().size()>0){

            hasTypes= true;
            has_service= true;
        }

        //brand ka
        hasBrand= false;
        if( service.getPrices()!=null &&
                service.getPrices().size()>0 &&
                service.getPrices().get(0).getBrand()!=null &&
                service.getPrices().get(0).getBrand().getBrands().size()>0){

            has_service= true;
            hasBrand= true;

            hasProducts= false;
            if(service.getPrices().get(0).getBrand().getBrands().get(0).getProducts()!=null &&
                    service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().size()>0){

                hasProducts= true;
                has_service= true;
            }

            Log.i("dialogStuff", "i'm in the brand loop now");
            linear_brand.setVisibility(View.VISIBLE);
            txt_brand_name.setText(AppConstant.CHOOSE_YOUR+ service.getPrices().get(0).getBrand().getTitle());

            rec_brand.setHasFixedSize(true);
            GridLayoutManager layoutManager= new GridLayoutManager(getActivity(), 2);
            rec_brand.setLayoutManager(layoutManager);

            int size= service.getPrices().get(0).getBrand().getBrands().size();
            for(int i=0;i<size;i++){

                service.getPrices().get(0).getBrand().getBrands().get(i).setService_name(service.getName());
                service.getPrices().get(0).getBrand().getBrands().get(i).setService_code(service.getServiceCode());
                service.getPrices().get(0).getBrand().getBrands().get(i).setPrice_id(
                        service.getPrices().get(0).getPriceId());


                if(service.getPrices().get(0).getBrand().getBrands().get(i).getDealRatio()!=null){

                    service.getPrices().get(0).getBrand().getBrands().get(i).setService_deal_id(
                            service.getDealId());
                    service.getPrices().get(0).getBrand().getBrands().get(i).setPrice((int)
                            ((double)service.getPrices().get(0).getBrand().getBrands().get(i).getDealRatio()*service.getDealPrice()));
                    service.getPrices().get(0).getBrand().getBrands().get(i).setMenu_price(service.getMenuPrice());
                    service.getPrices().get(0).getBrand().getBrands().get(i).setSave_per((int)((service.getPrices().get(0).getBrand().getBrands().get(i)
                            .getDealRatio() * service.getDealPrice()*100)/service.getMenuPrice()));

                    service.getPrices().get(0).getBrand().getBrands().get(i)
                            .setType(service.getDealType());

                }else{      //integer set kara

                    service.getPrices().get(0).getBrand().getBrands().get(i).setService_deal_id(
                            service.getServiceId());
                    service.getPrices().get(0).getBrand().getBrands().get(i).setPrice((int)
                            ((double)service.getPrices().get(0).getBrand().getBrands().get(i).getRatio()
                                    *service.getPrices().get(0).getPrice()));
                    service.getPrices().get(0).getBrand().getBrands().get(i)
                            .setType("service");
                }
            }
            service.getPrices().get(0).getBrand().getBrands().get(0).setCheck(true);

            //firebase and facebook default log event
            logEditServiceBrandChangeEvent( service.getPrices().get(0).getBrand().getBrands().get(0).getName());
            logEditServiceBrandChangeFireBaseEvent(service.getPrices().get(0).getBrand().getBrands().get(0).getName());
            brands_adapter= new BrandsListAdapter(getActivity(),
                    service.getPrices().get(0).getBrand().getBrands(), hasProducts, hasTypes);
            rec_brand.setAdapter(brands_adapter);

        }else{

            linear_brand.setVisibility(View.GONE);
        }

        //brand pe products ka hai yeh---- by deault brand 0 index ka products show kar raha hoon aur title bhi dikha raha hoon
        if(service.getPrices()!=null &&
                service.getPrices().size()>0 &&
                service.getPrices().get(0).getBrand()!=null &&
                service.getPrices().get(0).getBrand().getBrands().size()>0 &&
                service.getPrices().get(0).getBrand().getBrands().get(0).getProducts()!=null &&
                service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().size()>0){


            Log.i("dialogStuff", "i'm in the products loop now");
            linear_products.setVisibility(View.VISIBLE);
            txt_product_name.setText(AppConstant.CHOOSE_YOUR+
                    service.getPrices().get(0).getBrand().getBrands().get(0).getProductTitle());

            rec_products.setHasFixedSize(true);

            GridLayoutManager layoutManager= new GridLayoutManager(getActivity(), 2);
            rec_products.setLayoutManager(layoutManager);

            int size= service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().size();
            for(int i=0; i<size; i++){

                //check ki deal lagi ke nai

                service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().get(i).setCheck(false);


                if(service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().get(i).getDealRatio()!=null){

                    service.getPrices().get(0).getBrand().getBrands().get(0)
                            .getProducts().get(i).setPrice( (int)((double)service.getPrices().get(0).getBrand().getBrands().get(0)
                            .getProducts().get(i).getDealRatio() * service.getDealPrice()));
                    service.getPrices().get(0).getBrand().getBrands().get(0)
                            .getProducts().get(i).setMenu_price(service.getMenuPrice());

                    service.getPrices().get(0).getBrand().getBrands().get(0)
                            .getProducts().get(i).setSave_per((int)(100-((service.getPrices().get(0).getBrand().getBrands().get(0)
                            .getProducts().get(i).getDealRatio() * service.getDealPrice()*100)/service.getMenuPrice())));

                    //deal apply hua ki nai ka code
                    service.getPrices().get(0).getBrand().getBrands().get(0)
                            .getProducts().get(i).setService_deal_id(service.getDealId());
                    service.getPrices().get(0).getBrand().getBrands().get(0).getProducts()
                            .get(i).setType(service.getDealType());

                }else{      //deal lagi hai

                    service.getPrices().get(0).getBrand().getBrands().get(0)
                            .getProducts().get(i).setPrice((int)((double)service.getPrices().get(0).getBrand().getBrands().get(0)
                            .getProducts().get(i).getRatio() *
                            service.getPrices().get(0).getPrice()));
                    service.getPrices().get(0).getBrand().getBrands().get(0)
                            .getProducts().get(i).setService_deal_id(service.getServiceId());
                    service.getPrices().get(0).getBrand().getBrands().get(0).getProducts()
                            .get(i).setType("service");
                }
            }
            // default event for product
            logEditServiceProductChangeEvent(service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().get(0).getName());
            logEditServiceProductChangeFireBaseEvent( service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().get(0).getName());

            service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().get(0).setCheck(true);
            products_adapter= new ProductsListAdapter(getActivity(),
                    service.getPrices().get(0).getBrand().getBrands().get(0).getProducts(), 0, hasTypes);
            rec_products.setAdapter(products_adapter);

        }else{
            linear_products.setVisibility(View.GONE);
        }

        //use for horizontal line divider visible
        if (!hasBrand && !hasProducts){
            viewForBrand.setVisibility(View.GONE);
        }

        //types ka
        if(hasTypes){

            Log.i("dialogStuff", "i'm in the length loop now");
            linear_length.setVisibility(View.VISIBLE);
            txt_length_name.setText(AppConstant.CHOOSE_YOUR+ service.getPrices().get(0).getAdditions().get(0).getName());
            rec_length.setHasFixedSize(true);

            LinearLayoutManager layoutManager= new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            rec_length.setLayoutManager(layoutManager);

            int size= service.getPrices().get(0).getAdditions().get(0).getTypes().size();

            for(int i=0;i<size;i++){

                //setting the service stuff
                service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setService_name(service.getName());
                service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setService_code(service.getServiceCode());
                service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setPrice_id(
                        service.getPrices().get(0).getPriceId());

                if(hasBrand && hasProducts){            //has brand and product

                    if(service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().get(0).getDealRatio()!=null){

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setPrice((int)
                                ((double)service.getPrices().get(0).getBrand().getBrands().get(0).getProducts()
                                        .get(0).getDealRatio() *service.getDealPrice()));
                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setMenu_price((int)service.getMenuPrice()
                                +service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).getAdditions());

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setService_deal_id(service.getDealId());
                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i)
                                .setType(service.getDealType());

                    }else{

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setPrice((int)
                                ((double) service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().get(0).getRatio()
                                        *(service.getPrices().get(0).getPrice()
                                        +service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).getAdditions())));

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setService_deal_id(service.getServiceId());
                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i)
                                .setType("service");
                    }

                }else if(hasBrand){      //has brands only

                    if(service.getPrices().get(0).getBrand().getBrands().get(0).getDealRatio()!=null){

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setPrice((int)
                                ((double)service.getPrices().get(0).getBrand().getBrands().get(0).getDealRatio()
                                        *service.getDealPrice()));
                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i)
                                .setMenu_price((int)service.getMenuPrice()+service.getPrices().get(0)
                                        .getAdditions().get(0).getTypes().get(i).getAdditions());

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setService_deal_id(service.getDealId());
                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i)
                                .setType(service.getDealType());

                    }else{

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setPrice((int)
                                ((double)service.getPrices().get(0).getBrand().getBrands().get(0)
                                        .getRatio()
                                        *(service.getPrices().get(0).getPrice())
                                        +service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).getAdditions()));

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setService_deal_id(service.getServiceId());
                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i)
                                .setType("service");
                    }

                }else{          //no brands and product ke case mai ispe jayega aur koi deal nai

                    if(service.getDealId()!=null){          //deal hai isme

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setPrice(
                                service.getDealPrice());
                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).
                                setMenu_price((int)service.getMenuPrice()+service.getPrices().get(0).getAdditions().
                                        get(0).getTypes().get(i).getAdditions());

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setService_deal_id(service.getDealId());
                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i)
                                .setType(service.getDealType());

                    }else{                          //no deal

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setPrice((int)
                                ((double)service.getPrices().get(0).getPrice()+
                                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).getAdditions()));

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setService_deal_id(service.getServiceId());
                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i)
                                .setType("service");
                    }
                }
            }

            service.getPrices().get(0).getAdditions().get(0).getTypes().get(0).setCheck(true);
            logEditServiceLengthChangeEvent( service.getPrices().get(0).getAdditions().get(0).getTypes().get(0).getName());
            logEditServiceLengthChangeFireBaseEvent(service.getPrices().get(0).getAdditions().get(0).getTypes().get(0).getName());
            type_adapter= new LengthListAdapter(getActivity(), service.getPrices().get(0).getAdditions().get(0));
            rec_length.setAdapter(type_adapter);
        }else{

            linear_length.setVisibility(View.GONE);
            view_for_lenght.setVisibility(View.GONE);
        }

        if(service.getUpgrades()!=null &&
                service.getUpgrades().size()>0){
            view_upgrade_border.setVisibility(View.VISIBLE);
            linear_upgrade.setVisibility(View.VISIBLE);
            linear_save_more.setVisibility(View.VISIBLE);
            Log.i("dialogstuff", "i'm in the upgrade");

            txt_upgrade_name.setText("Upgrades For You");

            rec_upgrade.setHasFixedSize(true);

            LinearLayoutManager layoutManager= new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            rec_upgrade.setLayoutManager(layoutManager);

            UpgradeListAdapter adapter= new UpgradeListAdapter(getActivity(),
                    service.getUpgrades(), getDialog(), service.getName());
            rec_upgrade.setAdapter(adapter);
        }else{
            view_upgrade_border.setVisibility(View.GONE);
            linear_upgrade.setVisibility(View.GONE);
        }


        if(service.getPackages()!=null &&
                service.getPackages().size()>0){
            view_package_line.setVisibility(View.VISIBLE);
            linear_package.setVisibility(View.VISIBLE);
            linear_save_more.setVisibility(View.VISIBLE);
            Log.i("dialogstuff", "i'm in the upgrade");

            txt_package_name.setText("PACKAGES");

            rec_package.setHasFixedSize(true);

            LinearLayoutManager layoutManager= new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            rec_package.setLayoutManager(layoutManager);

            PackageListAdapter adapter= new PackageListAdapter(getActivity(), service.getPackages(), /*slabs,*/ user_cart);
            rec_package.setAdapter(adapter);
        }else{
            view_package_line.setVisibility(View.GONE);
            linear_package.setVisibility(View.GONE);
        }


        final LinearLayout linear_service= view.findViewById(R.id.linear_service);
        if(has_service){

            linear_service.setVisibility(View.VISIBLE);
        }else{

            linear_service.setVisibility(View.GONE);
        }

        final TextView txt_add_to_cart= (TextView) view.findViewById(R.id.txt_add_to_cart);
        txt_add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Todo:price id save karna hai
                logEditServicePageOpenAddToCartEvent();
                logEditServicePageOpenAddToCartFireBaseEvent();

                if(add_){


                    EventBus.getDefault().post(new SingleBrandProductEvent(service));
                    dismiss();
                }else if(add_service){

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            addService();
                        }
                    }, 250);
                    dismiss();
                }else{
                    //yeh to boss hai

                    String service_name= hasTypes?type_adapter.getService_name():hasBrand?brands_adapter.getService_name():"";
                    String service_deal_id= hasTypes?type_adapter.getService_deal_id():hasProducts?products_adapter.getService_deal_id():
                            hasBrand?brands_adapter.getService_deal_id():"";
                    String type= hasTypes?type_adapter.getType():hasBrand?brands_adapter.getType():"";

                    int service_code= hasTypes?type_adapter.getService_code():hasBrand?brands_adapter.getService_code():0;
                    int price_id= hasTypes?type_adapter.getPrice_id():hasBrand?brands_adapter.getPrice_id():0;

                    int price= hasTypes? type_adapter.getPrice():hasProducts?products_adapter.getPrice():
                            hasBrand?brands_adapter.getPrice():0;
                    //price with tax
                    int price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*price);

                    int menu_price= hasTypes? type_adapter.getMenu_price():hasProducts?products_adapter.getMenu_price():
                            hasBrand?brands_adapter.getMenu_price():0;
                    //menu price with tax
                    int menu_price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*menu_price);

                    //brand stuff
                    String brand_name= hasBrand?brands_adapter.getBrandName():"";
                    String brand_id= hasBrand?brands_adapter.getbrandId():"";

                    //product stuff
                    String product_id= hasProducts? products_adapter.getProductId():"";
                    String product_name= hasProducts? products_adapter.getProductName():"";

                    //type stuff
                    String type_index= hasTypes?""+type_adapter.getType_index():"";
                    String type_name= hasTypes?type_adapter.getType_name():"";

                    UserServices services= new UserServices();

                    services.setName(service_name);

                    services.setService_code(service_code);
                    services.setPrice_id(price_id);

                    services.setService_deal_id(service_deal_id);               //service ya deal id hogi isme
                    services.setService_id(service.getServiceId());

                    services.setType(type);

                    services.setPrice(price_);
                    services.setMenu_price(menu_price_);

                    services.setDescription(brand_name + " "+product_name+ " "+type_name);

                    services.setBrand_name(brand_name);
                    services.setBrand_id(brand_id);
                    services.setProduct_id(product_id);
                    services.setProduct_name(product_name);
                    services.setType_name(type_name);
                    services.setType_index(hasTypes?type_adapter.getType_index():0);
                    services.setType_additions(hasTypes?type_adapter.getAdditions():0);

                    services.setPrimary_key(""+service_code+service_deal_id
                            +brand_id+product_id+type_index);

                    Log.i("intheadd", "value: "+ service_code + "  "+ service_deal_id+ "  "+ brand_id+ " "+
                            product_id+ "  "+ type_index);

                    new Thread(new UserCartTask(getActivity(), user_cart, services, false, false)).start();
                    logAddedToCartEvent(service_name,"INR",price_);

                    EventBus.getDefault().post(new AddCartEvent(service_name));

                    addServiceToCart(user_cart.getParlorId(),services.getService_code());
                    dismiss();
                }

                Log.i("yehprintnaihoga", "oh yes");


            }
        });

        if(add_ || add_service){

            Log.i("intheadd", "i'm in the add <------------------");

            linear_bottom.setVisibility(View.GONE);
            linear_no_thanks.setVisibility(View.GONE);
            linear_cart.setVisibility(View.GONE);

            linear_price.setVisibility(View.VISIBLE);
            linear_service.setVisibility(View.VISIBLE);
            linear_brand.setVisibility(View.GONE);
            linear_products.setVisibility(View.GONE);
            linear_length.setVisibility(View.GONE);
            view_for_lenght.setVisibility(View.GONE);


//            String str_info= "";
//            if(brands_adapter!=null &&
//                    brands_adapter.getBrandName()!=null &&
//                    !brands_adapter.getBrandName().equalsIgnoreCase("")){
//                str_info= brands_adapter.getBrandName()+" - ";
//            }
//
//            if(products_adapter!=null &&
//                    products_adapter.getProductName()!=null &&
//                    !products_adapter.getProductName().equalsIgnoreCase("")){
//                str_info= str_info+ products_adapter.getProductName();
//            }
//            if(!str_info.equalsIgnoreCase("")){
//                txt_info.setVisibility(View.VISIBLE);
//                txt_info.setText(str_info);
//            }else
//                txt_info.setVisibility(View.GONE);

//            linear_bottom.setVisibility(View.VISIBLE);
//
//            linear_service.setVisibility(View.GONE);
//            linear_no_thanks.setVisibility(View.VISIBLE);
//            linear_cart.setVisibility(View.GONE);
//            txt_no_thanks.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            Log.i("buttonclic", "event bus service pe");
//                            EventBus.getDefault().post(new SingleBrandProductEvent(service));
//                        }
//                    }, 250);
//
//                    dismiss();
//
//                }
//            });
        }else{
            linear_price.setVisibility(View.GONE);
        }

        return view;
    }

    //brand ka event hai products mai change karane ke liye
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBrandEvent(BrandEvent event) {

        Log.i("btnvalue", "i'm in the brand ke event pe: "+ event.getPosition());

        if(hasProducts){

            int size= service.getPrices().get(0).getBrand().getBrands().get(event.getPosition()).getProducts().size();
            for(int i=0; i<size; i++){

                //check ki deal lagi ke nai

                service.getPrices().get(0).getBrand().getBrands().get(event.getPosition()).getProducts().get(i).setCheck(false);


                if(service.getPrices().get(0).getBrand().getBrands().get(event.getPosition()).getProducts().get(i).getDealRatio()!=null){

                    service.getPrices().get(0).getBrand().getBrands().get(event.getPosition())
                            .getProducts().get(i).setPrice( (int)((double)service.getPrices().get(0).getBrand().getBrands().get(event.getPosition())
                            .getProducts().get(i).getDealRatio() * service.getDealPrice()));
                    service.getPrices().get(0).getBrand().getBrands().get(event.getPosition())
                            .getProducts().get(i).setMenu_price(service.getMenuPrice());

                    service.getPrices().get(0).getBrand().getBrands().get(event.getPosition())
                            .getProducts().get(i).setSave_per((int)(100-((service.getPrices().get(0).getBrand().getBrands().get(event.getPosition())
                            .getProducts().get(i).getDealRatio() * service.getDealPrice()*100)/service.getMenuPrice())));

                    //deal apply hua ki nai ka code
                    service.getPrices().get(0).getBrand().getBrands().get(event.getPosition())
                            .getProducts().get(i).setService_deal_id(service.getDealId());
                    service.getPrices().get(0).getBrand().getBrands().get(event.getPosition()).getProducts()
                            .get(i).setType(service.getDealType());

                }else{      //deal lagi hai

                    service.getPrices().get(0).getBrand().getBrands().get(event.getPosition())
                            .getProducts().get(i).setPrice((int)((double)service.getPrices().get(0).getBrand().getBrands().get(event.getPosition())
                            .getProducts().get(i).getRatio() *
                            service.getPrices().get(0).getPrice()));

                    service.getPrices().get(0).getBrand().getBrands().get(event.getPosition())
                            .getProducts().get(i).setService_deal_id(service.getServiceId());
                    service.getPrices().get(0).getBrand().getBrands().get(event.getPosition()).getProducts()
                            .get(i).setType("service");
                }
            }

            Log.i("btnvalue", "size of product: "+
                    service.getPrices().get(0).getBrand().getBrands().get(event.getPosition()).getProducts().size());

            service.getPrices().get(0).getBrand().getBrands().get(event.getPosition()).getProducts().get(0).setCheck(true);
            products_adapter.setProductList(service.getPrices().get(0).getBrand().getBrands().get(event.getPosition()).getProducts(),
                    event.getPosition());
        }

        if(hasTypes) {
            int size = service.getPrices().get(0).getAdditions().get(0).getTypes().size();

            for (int i = 0; i < size; i++) {
                service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setCheck(false);

                if (hasBrand && hasProducts) {            //has brand and product

                    if (service.getPrices().get(0).getBrand().getBrands().get(event.getPosition()).getProducts().get(0).getDealRatio() != null) {

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setPrice((int)
                                ((double)service.getPrices().get(0).getBrand().getBrands().get(event.getPosition()).getProducts()
                                        .get(0).getDealRatio() * service.getDealPrice()));
                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setMenu_price((int)service.getMenuPrice()+
                                service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).getAdditions());

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setService_deal_id(service.getDealId());
                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i)
                                .setType(service.getDealType());

                    } else {

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setPrice((int)
                                ((double)service.getPrices().get(0).getBrand().getBrands().get(event.getPosition()).getProducts().get(0).getRatio()
                                        * (service.getPrices().get(0).getPrice()
                                        + service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).getAdditions())));

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setService_deal_id(service.getServiceId());
                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i)
                                .setType("service");
                    }

                } else if (hasBrand) {      //has brands only

                    if (service.getPrices().get(0).getBrand().getBrands().get(event.getPosition()).getDealRatio() != null) {

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setPrice((int)
                                ((double)service.getPrices().get(0).getBrand().getBrands().get(event.getPosition()).getDealRatio()
                                        * service.getDealPrice()));
                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setMenu_price((int)service.getMenuPrice()+
                                service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).getAdditions());

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setService_deal_id(service.getDealId());
                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i)
                                .setType(service.getDealType());

                    } else {

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setPrice((int)
                                ((double)service.getPrices().get(0).getBrand().getBrands().get(event.getPosition())
                                        .getRatio()
                                        * (service.getPrices().get(0).getPrice())
                                        + service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).getAdditions()));

                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setService_deal_id(service.getServiceId());
                        service.getPrices().get(0).getAdditions().get(0).getTypes().get(i)
                                .setType("service");
                    }

                } else {          //no brands and product ke case mai ispe jayega aur koi deal nai

                    Log.i("ismekabhi", "so isme kabhi nahi jayega");
                }

                service.getPrices().get(0).getAdditions().get(0).getTypes().get(0).setCheck(true);
                type_adapter.setLengthList(service.getPrices().get(0).getAdditions().get(0));
            }
        }
    }

    //product ka event hai yeh
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onProductEvent(ProductEvent event) {

        if(hasTypes){

            int size= service.getPrices().get(0).getAdditions().get(0).getTypes().size();

            for(int i=0;i<size;i++){

                service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setCheck(false);


                if(service.getPrices().get(0).getBrand().getBrands().get(event.getBrand_index()).getProducts()
                        .get(event.getProduct_index()).getDealRatio()!=null){

                    service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setPrice((int)
                            ((double)service.getPrices().get(0).getBrand().getBrands().get(event.getBrand_index()).getProducts()
                                    .get(event.getProduct_index()).getDealRatio() *service.getDealPrice()));
                    service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setMenu_price((int)service.getMenuPrice()+
                            service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).getAdditions());

                    service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setService_deal_id(service.getDealId());
                    service.getPrices().get(0).getAdditions().get(0).getTypes().get(i)
                            .setType(service.getDealType());

                }else{

                    service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setPrice((int)
                            ((double)service.getPrices().get(0).getBrand().getBrands().get(event.getBrand_index())
                                    .getProducts().get(event.getProduct_index()).getRatio()
                                    *(service.getPrices().get(0).getPrice()
                                    +service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).getAdditions())));

                    service.getPrices().get(0).getAdditions().get(0).getTypes().get(i).setService_deal_id(service.getServiceId());
                    service.getPrices().get(0).getAdditions().get(0).getTypes().get(i)
                            .setType("service");
                }
            }

            service.getPrices().get(0).getAdditions().get(0).getTypes().get(0).setCheck(true);
            type_adapter.setLengthList(service.getPrices().get(0).getAdditions().get(0));
        }

    }

    //brand ka event hai products mai change karane ke liye
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpgradeEvent_(final UpgradeEvent_ event) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                UserServices services= new UserServices();
                services.setName(event.getService_name());

                services.setService_code(event.getService_code());

                services.setService_deal_id(event.getService_deal_id());               //service ya deal id hogi isme
                services.setService_id(event.getService_id());                      //service ka id, quantity ke liye

                services.setDealId(event.getDealId());

                services.setType(event.getType());

                //price & menuprice with tax
                int price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*event.getPrice());
                int menu_price_;
                if(event.getMenu_price()>0){
                    menu_price_ = (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*event.getMenu_price());
                }else
                    menu_price_=0;

                services.setPrice(price_);
                services.setMenu_price(menu_price_);

                services.setBrand_id(event.getBrand_id());
                services.setProduct_id(event.getProduct_id());

                services.setPackageServices(event.getPackage_services_list());

                services.setDescription(event.getDescription());
                services.setPrimary_key(event.getPrimary_key());

                Log.i("primary_key", "value to be sent to adapter: " + event.getService_name()+ "  "+ event.getService_code()
                        + "  "+event.getService_deal_id()
                        + "  "+ event.getType()+ "  "+event.getPrice()+ "  "+event.getMenu_price()+ "  "+
                        event.getDescription()+ "  "+ event.getPrimary_key());
                logAddedToCartEvent(services.getName(),"INR",services.getPrice());
                new Thread(new UserCartTask(getActivity(), user_cart, services, false, false)).start();
                addServiceToCart(user_cart.getParlorId(),service.getServiceCode());

                showSnackbar(event.getService_name());
            }
        }, 200);
    }

    private void addServiceToCart(String parlorId,int serviceCode ){
        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface= retrofit.create(ApiInterface.class);
        UserCart_post userCartPost=new UserCart_post();
        userCartPost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        userCartPost.setUserId(BeuSalonsSharedPrefrence.getUserId());
        userCartPost.setParlorId(parlorId);
        userCartPost.setServiceCode(serviceCode);
        userCartPost.setQuantity(1);
        Call<AddService_response> call=apiInterface.addServicetoCart(userCartPost);
        call.enqueue(new Callback<AddService_response>() {
            @Override
            public void onResponse(Call<AddService_response> call, Response<AddService_response> response) {
                if (response.isSuccessful()){
                    if (response.body().isSuccess()){
                        Log.e("stuff add service cart", "i'm retrofit getStatus true :(");

                    }else{
                        Log.e("stuff add service cart", "i'm retrofit getStatus false:(");

                    }
                }else{
                    Log.e("stuff add service cart", "i'm retrofit failure :(");

                }
            }

            @Override
            public void onFailure(Call<AddService_response> call, Throwable t) {
                Log.e("stuff add service cart", "i'm in failure: "+ t.getMessage()+ "   "+
                        t.getStackTrace()+ t.getCause()+ " "+ t.getLocalizedMessage());
            }
        });

    }

    //jab koi brand product types nai hai toh
    private void addService(){

        Log.i("addservicestuff", "i'm in the add service method");

        AddServiceEvent services= new AddServiceEvent();
        services.setName(service.getName());

        String service_deal_id= service.getDealId()==null?
                service.getServiceId():service.getDealId();
        services.setService_deal_id(service_deal_id);

        services.setService_id(service.getServiceId());

        //service ya deal id hogi isme
        services.setType(service.getDealId()==null?"service":service.getDealType());

        services.setService_code(service.getServiceCode());
        services.setPrice_id(service.getPrices().get(0).getPriceId());

        if(services.getType().equalsIgnoreCase("chooseOnePer")){

            int price= service.getMenuPrice()- (int) (((double)service.getDealPrice()/100)*service.getMenuPrice());
            services.setPrice(price);
        }else
            services.setPrice(service.getDealId()==null?service.getPrices().get(0).getPrice():service.getDealPrice());

        services.setMenu_price(service.getDealId()==null?
                0:service.getMenuPrice());

        services.setDescription(service.getDescription()==null?
                "":service.getDescription());

        String primary_key= ""+service.getServiceCode()+service_deal_id;
        services.setPrimary_key(primary_key);

        EventBus.getDefault().post(services);

    }

    public void showSnackbar(String service_name){
        Snackbar snackbar = Snackbar.make( coordinator_,
                service_name+" Service Added To Cart!", 1000);

        //setting the snackbar action button text size
        View view = snackbar.getView();
        TextView txt_action = (TextView) view.findViewById(android.support.design.R.id.snackbar_action);
        TextView txt_text = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txt_action.setTextSize(13);
        txt_action.setAllCaps(false);
        txt_text.setTextSize(13);
        snackbar.setActionTextColor(ContextCompat.getColor(getActivity(), R.color.snackbar));

//        if(add_service || add_){
//
//            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams)
//                    view.getLayoutParams();
//            params.setMargins(0, 0, 0, 90);
//            params.gravity = Gravity.BOTTOM;
//            view.setLayoutParams(params);
//        }

        snackbar.setAction("View Cart", new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UserCartFragment fragment= new UserCartFragment();
                Bundle bundle= new Bundle();
                bundle.putBoolean("has_data", true);
                fragment.setArguments(bundle);
                fragment.show(getActivity().getSupportFragmentManager(), "user_cart");
            }
        });
        snackbar.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(UserCart event) {

        Log.i("servicefragmentpe", "I'm here now you know where");

        txt_no_thanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void editPackageListEvent(final EditPackageListEvent event) {

        showSnackbar(event.getName());
        new Thread(new MultipleServicesTask(getActivity(), user_cart, event.getList())).start();

        Handler handler = new Handler();
        for (int i=0;i<event.getList().size();i++) {
            final int finalI = i;
            handler.postDelayed(new Runnable() {
                public void run() {

                    addServiceToCart(user_cart.getParlorId(), event.getList().get(finalI).getService_code());
                }

            }, 2000);

        }
        if(event.isAlter())
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    UserCartFragment fragment= new UserCartFragment();
                    Bundle bundle= new Bundle();
                    bundle.putBoolean("has_data", true);
                    fragment.setArguments(bundle);
                    if(getActivity()!=null)
                        fragment.show(getActivity().getSupportFragmentManager(), "user_cart");
                    else
                        Log.i("activitynull", "bhai kya ho raha hai yeh");
                }
            }, 400);
    }


    public void logAddedToCartEvent (String contentType, String currency, double price) {
        Log.e("prefine","add  type"+contentType+ " price"+price);

        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        logger.logEvent(AppEventsConstants.EVENT_NAME_ADDED_TO_CART, price, params);
    }


    public void logEditServicePageOpenAddToCartEvent () {
        Log.e("EditServiceAddToCart","fine");
        logger.logEvent(AppConstant.EditServicePageOpenAddToCart);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */

    public void logEditServiceCheckOutEvent () {
        Log.e("EditServiceCheckOut","fine");

        logger.logEvent(AppConstant.EditServiceCheckOut);
    }


    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logEditServiceContinueShopingEvent () {
        Log.e("EditServContiShoping","fine");

        logger.logEvent(AppConstant.EditServiceContinueShoping);
    }

    public void logEditServiceCheckOutFireBaseEvent () {
        Log.e("EditCheckOutfirebse","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.EditServiceCheckOut,bundle);
    }


    public void logEditServiceContinueShopingFireBaseEvent () {
        Log.e("EditServCoiShogfirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.EditServiceContinueShoping,bundle);
    }

    public void logEditServicePageOpenAddToCartFireBaseEvent () {
        Log.e("EditAddToCartfire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.EditServicePageOpenAddToCart,bundle);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {

            Window window = getDialog().getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFFFFF")));
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }catch (Exception e){

            e.printStackTrace();
        }

    }

    public void logEditServiceLengthChangeEvent (String name) {
        Log.e("EditServiceLengthChange","fine");
        Bundle params = new Bundle();
        params.putString("Name", name);
        logger.logEvent(AppConstant.EditServiceLengthChange, params);
    }

    public void logEditServiceLengthChangeFireBaseEvent (String name) {
        Log.e("EditServicefirebsLength","fine");
        Bundle params = new Bundle();
        params.putString("Name", name);
        mFirebaseAnalytics.logEvent(AppConstant.EditServiceLengthChange, params);
    }
    public void logEditServiceBrandChangeEvent (String name) {
        Log.e("EditServiceBrandChange","fine");
        Bundle params = new Bundle();
        params.putString("Name", name);
        logger.logEvent(AppConstant.EditServiceBrandChange, params);
    }

    public void logEditServiceBrandChangeFireBaseEvent (String name) {
        Log.e("EditServiceBrfireChange","fine");
        Bundle params = new Bundle();
        params.putString("Name", name);
        mFirebaseAnalytics.logEvent(AppConstant.EditServiceBrandChange, params);
    }
    public void logEditServiceProductChangeEvent (String name) {
        Log.e("EditSerProductChange","fine");
        Bundle params = new Bundle();
        params.putString("Name", name);
        logger.logEvent(AppConstant.EditServiceProductChange, params);
    }
    public void logEditServiceProductChangeFireBaseEvent (String name) {
        Log.e("EditProductFirebasech","fine");
        Bundle params = new Bundle();
        params.putString("Name", name);
        mFirebaseAnalytics.logEvent(AppConstant.EditServiceProductChange, params);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


}
