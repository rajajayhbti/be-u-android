package com.beusalons.android.Model.Appointments;

import java.io.Serializable;
import java.util.List;

/**
 * Created by myMachine on 11/28/2016.
 */

public class PastData {

    private Integer otherCharges;
    private Integer subtotal;
    private Integer discount;
    private Integer appointmentType;
    private Integer paymentMethod;
    private Integer membershipDiscount;
    private Double payableAmount;
    private String appointmentId;
    private Integer parlorAppointmentId;
    private String appointmentStatus;
    private Integer creditUsed;
    private Double tax;
    private String startsAt;
    private Integer estimatedTime;
    private Integer loyalityPoints;
    private List<Services> services = null;
    private List<Products> products = null;
    private String parlorName;
    private String parlorAddress;
    private PastReview review;
    private String parlorId;

    //my variables
    private String date;
    private String time;

    private String openingTime;
    private String closingTime;

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public String getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(String closingTime) {
        this.closingTime = closingTime;
    }

    public Integer getOtherCharges() {
        return otherCharges;
    }

    public void setOtherCharges(Integer otherCharges) {
        this.otherCharges = otherCharges;
    }

    public Integer getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Integer subtotal) {
        this.subtotal = subtotal;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(Integer appointmentType) {
        this.appointmentType = appointmentType;
    }

    public Integer getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Integer paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Integer getMembershipDiscount() {
        return membershipDiscount;
    }

    public void setMembershipDiscount(Integer membershipDiscount) {
        this.membershipDiscount = membershipDiscount;
    }

    public Double getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(Double payableAmount) {
        this.payableAmount = payableAmount;
    }

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public Integer getParlorAppointmentId() {
        return parlorAppointmentId;
    }

    public void setParlorAppointmentId(Integer parlorAppointmentId) {
        this.parlorAppointmentId = parlorAppointmentId;
    }

    public String getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(String appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }

    public Integer getCreditUsed() {
        return creditUsed;
    }

    public void setCreditUsed(Integer creditUsed) {
        this.creditUsed = creditUsed;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public String getStartsAt() {
        return startsAt;
    }

    public void setStartsAt(String startsAt) {
        this.startsAt = startsAt;
    }

    public Integer getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(Integer estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public Integer getLoyalityPoints() {
        return loyalityPoints;
    }

    public void setLoyalityPoints(Integer loyalityPoints) {
        this.loyalityPoints = loyalityPoints;
    }

    public List<Services> getServices() {
        return services;
    }

    public void setServices(List<Services> services) {
        this.services = services;
    }

    public List<Products> getProducts() {
        return products;
    }

    public void setProducts(List<Products> products) {
        this.products = products;
    }

    public String getParlorName() {
        return parlorName;
    }

    public void setParlorName(String parlorName) {
        this.parlorName = parlorName;
    }

    public String getParlorAddress() {
        return parlorAddress;
    }

    public void setParlorAddress(String parlorAddress) {
        this.parlorAddress = parlorAddress;
    }

    public PastReview getReview() {
        return review;
    }

    public void setReview(PastReview review) {
        this.review = review;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
