package com.beusalons.android.Model.DealsSalonList;

import java.util.List;

/**
 * Created by myMachine on 6/28/2017.
 */

public class Parlor {


    private String name;
    private String parlorId;
    private List<Image> images = null;
    private String gender;
    private String address1;
    private int parlorType;
    private String link;
    private String address2;
    private String closingTime;
    private String openingTime;
    private double rating;
    private int price;
    private Boolean favourite;
    private Boolean recent;
    private Object distance;
    //private List<Object> parlorservices = null;
    private List<ParlorServices> parlorservices = null;
    private List<Deal> deals = null;
    private boolean isSelected =false;

    private int menuPrice;
    private int dealPrice;


    public List<ParlorServices> getParlorservices() {
        return parlorservices;
    }

    public void setParlorservices(List<ParlorServices> parlorservices) {
        this.parlorservices = parlorservices;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public int getParlorType() {
        return parlorType;
    }

    public void setParlorType(int parlorType) {
        this.parlorType = parlorType;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(String closingTime) {
        this.closingTime = closingTime;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Boolean getFavourite() {
        return favourite;
    }

    public void setFavourite(Boolean favourite) {
        this.favourite = favourite;
    }

    public Boolean getRecent() {
        return recent;
    }

    public void setRecent(Boolean recent) {
        this.recent = recent;
    }

    public Object getDistance() {
        return distance;
    }

    public void setDistance(Object distance) {
        this.distance = distance;
    }

//    public List<Object> getParlorservices() {
//        return parlorservices;
//    }
//
//    public void setParlorservices(List<Object> parlorservices) {
//        this.parlorservices = parlorservices;
//    }

    public List<Deal> getDeals() {
        return deals;
    }

    public void setDeals(List<Deal> deals) {
        this.deals = deals;
    }

    public int getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(int menuPrice) {
        this.menuPrice = menuPrice;
    }

    public int getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(int dealPrice) {
        this.dealPrice = dealPrice;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
