package com.beusalons.android.Model.HomeFragmentModel;

/**
 * Created by Ashish Sharma on 3/15/2017.
 */

public class FreeBees {

    String title;
    String message;
    String image;
    String points;
    String description;
    String head;
    String newHead;
    String newPoints;
    String action;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNewHead() {
        return newHead;
    }

    public void setNewHead(String newHead) {
        this.newHead = newHead;
    }

    public String getNewPoints() {
        return newPoints;
    }

    public void setNewPoints(String newPoints) {
        this.newPoints = newPoints;
    }
}
