package com.beusalons.android.Model.Corporate.CorporateDetail;

/**
 * Created by myMachine on 8/3/2017.
 */

public class CorporateRegister {

    private String title;
    private String image;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
