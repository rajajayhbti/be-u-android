package com.beusalons.android;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.beusalons.android.Utility.Utility;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.beusalons.android.BuildConfig.VERSION_NAME;

public class AboutUs extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        setToolBar();
    }
    private ImageView img_beu;

    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){

            getSupportActionBar().setTitle(getResources().getString(R.string.about_us));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }

        img_beu= (ImageView)findViewById(R.id.img_beu);
        img_beu.setImageResource(R.drawable.beu_logo);
        img_beu.setColorFilter(ContextCompat.getColor(this,R.color.colorPrimary));

        TextView textView= (TextView) findViewById(R.id.txt_abt_version);
        textView.setText("v"+ VERSION_NAME);
        Log.i("versionName", "value in version: "+ VERSION_NAME);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
