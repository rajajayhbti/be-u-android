package com.beusalons.android.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Adapter.HomeScreenAdapter;
import com.beusalons.android.Event.FiltersEvent;
import com.beusalons.android.Event.HomeAddressChangedEvent;
import com.beusalons.android.Event.SortFilterEvent;
import com.beusalons.android.Helper.CheckConnection;
import com.beusalons.android.MainActivity;
import com.beusalons.android.Model.HomeFragmentModel.DiscountRules;
import com.beusalons.android.Model.HomePage.ApiResponse;
import com.beusalons.android.Model.HomePage.HomeDatum;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.PaginationScrollListener;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by myMachine on 22-Jan-18.
 */

public class NewHomePageFragment extends android.support.v4.app.Fragment {

    private RecyclerView recycler_;
    private HomeScreenAdapter adapter;
    private List<HomeDatum> list=null;

    //paginatino stuff
    private int PAGE=1;
    private boolean isLoading= false;

    //filter ka stuff
    private boolean isMale=false, isFemale= false, isUnisex= false;
    private boolean isPopularity= false, isPriceLow= false, isPriceHigh= false, isNearest= false;
    private boolean isRating_1= false, isRating_2= false, isRating_3= false, isRating_4= false;
    private boolean isPrice_red= false, isPrice_blue= false, isPrice_green= false, isPrice_all= false;
    private String filters_rating= null, filters_price= null, filters_gender= null, filters_sort= "0";
    private List<String> filters_brands;

    //connection ka stuff
    private ProgressBar progress_;
    private Button btn_retry;
    private  List<DiscountRules> discountRulesList=new ArrayList<>();

    //snackbar ke liye--- update/review wagera wagera

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view= inflater.inflate(R.layout.fragment_home_page, container, false);

        progress_= view.findViewById(R.id.progress_);
        btn_retry= view.findViewById(R.id.btn_retry);

        recycler_ = view.findViewById(R.id.recycler_);
        LinearLayoutManager manager= new LinearLayoutManager(view.getContext());
        recycler_.setLayoutManager(manager);
        recycler_.addOnScrollListener(new PaginationScrollListener(manager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){

                    Log.i("loadinghua", "im here now");

                    if(CheckConnection.isConnected(view.getContext())){
                        isLoading= true;
                        adapter.addProgress();
                        PAGE++;
                        fetchData();
                    }


                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
        list= new ArrayList<>();
        adapter= new HomeScreenAdapter(list);
        recycler_.setAdapter(adapter);

        btn_retry= view.findViewById(R.id.btn_retry);
        btn_retry.setVisibility(View.GONE);
        btn_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(CheckConnection.isConnected(v.getContext())){

                    try{

                        fetchData();
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }else{
                    Toast.makeText(v.getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });

        filters_brands= new ArrayList<>();

        return view;
    }



    private void fetchData(){

        Log.i("taggs", "kitni bar call hoga yeh: "+ filters_brands+ "   "+
                new Gson().toJson(filters_brands));

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        String brands= null;
        if(filters_brands.size()>0)
            brands= new Gson().toJson(filters_brands);

        Call<ApiResponse> call = apiInterface.getData(BeuSalonsSharedPrefrence.getLatitude(),
                BeuSalonsSharedPrefrence.getLongitude(), BeuSalonsSharedPrefrence.getUserId(), PAGE,
                filters_rating, filters_price, filters_gender, filters_sort, brands);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, final Response<ApiResponse> response) {

                if (response.isSuccessful()) {

                    if (response.body().getSuccess()) {
                        Log.i("homepage", "i'm in success");

                        recycler_.setVisibility(View.VISIBLE);
                        progress_.setVisibility(View.GONE);
                        btn_retry.setVisibility(View.GONE);


                        //carousel page home ka
                        if (response.body().getData().getScorecard() != null &&
                                response.body().getData().getScorecard().getList() != null &&
                                response.body().getData().getScorecard().getList().size() > 0) {

//                            HomeDatum data_search= new HomeDatum("search", 5);
//                            list.add(data_search);

                            list.clear();

                            HomeDatum data_score= new HomeDatum("freebies", 4);
                            data_score.setScorecard(response.body().getData().getScorecard());
                            list.add(data_score);
                        }

                        if (response.body().getData().getBanner() != null &&
                                response.body().getData().getBanner().getList() != null &&
                                response.body().getData().getBanner().getList().size() > 0) {

                            HomeDatum data_banner= new HomeDatum("firstAdBanner", 3);
                            data_banner.setList(response.body().getData().getBanner().getList());
                            list.add(data_banner);
                        }


                        if (response.body().getData().getHomeData() != null &&
                                response.body().getData().getHomeData().size() > 0) {


                            if (PAGE == 1) {

                                list.addAll(response.body().getData().getHomeData());
                                adapter.setList(list, response.body().getData().getServerTime());

                            } else {
                                adapter.removeProgress();
                                adapter.addData(response.body().getData().getHomeData());

                            }

                        }else if(response.body().getData().getHomeData()==null ||
                                response.body().getData().getHomeData().size()==0){

                            if(PAGE!=1)
                                adapter.removeProgress();
                        }


                        if (response.body().getData().getDiscountRules()!=null &&
                                response.body().getData().getDiscountRules().size()>0){
                            if (discountRulesList.size()>0){
                                discountRulesList.clear();
                            }
                            for (int i=0;i<response.body().getData().getDiscountRules().size();i++){
                                discountRulesList.add(response.body().getData().getDiscountRules().get(i));
                            }
                            BeuSalonsSharedPrefrence.clearDiscountData();
                            BeuSalonsSharedPrefrence.saveDiscount(getActivity(),discountRulesList);

                        }

                        isLoading= false;

                    } else {
                        Log.i("homepage", "i'm in the not success");

                        if(PAGE==1){

                            recycler_.setVisibility(View.GONE);
                            progress_.setVisibility(View.GONE);
                            btn_retry.setVisibility(View.VISIBLE);
                        }else{
                            isLoading= false;
                            adapter.removeProgress();
                        }

                    }

                } else {
                    Log.i("homepage", "i'm in else");
                    if(PAGE==1){

                        recycler_.setVisibility(View.GONE);
                        progress_.setVisibility(View.GONE);
                        btn_retry.setVisibility(View.VISIBLE);
                    }else{
                        isLoading= false;
                        adapter.removeProgress();
                    }
                }

            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                Log.i("homepage", "value: " + t.getCause() + " " + t.getStackTrace() + " " +
                        t.getMessage());
                if(PAGE==1){

                    recycler_.setVisibility(View.GONE);
                    progress_.setVisibility(View.GONE);
                    btn_retry.setVisibility(View.VISIBLE);
                }else{
                    isLoading= false;
                    adapter.removeProgress();
                    recycler_.setVisibility(View.GONE);
                    progress_.setVisibility(View.GONE);
                    btn_retry.setVisibility(View.VISIBLE);

                }
            }
        });


    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void filterEvent(SortFilterEvent event) {

        Log.i("filterevent", "sort: "+ event.getSort()+ " category: "+event.getCategory()+
                " rating: "+event.getRating()+ " brands: "+event.getBrands());

        if(event.getSort().equalsIgnoreCase("0")){
            filters_sort= "4";
        }else if(event.getSort().equalsIgnoreCase("1")){
            filters_sort= "0";
        }else if(event.getSort().equalsIgnoreCase("2")){
            filters_sort= "1";
        }else if(event.getSort().equalsIgnoreCase("3")){
            filters_sort= "2";
        }else if(event.getSort().equalsIgnoreCase("4")){
            filters_sort= "3";
        }


        filters_price= event.getCategory();     //price hai yeh

        if(event.getRating()!=null){
            if(event.getRating().equalsIgnoreCase("0")){
                filters_rating= "4";
            }else if(event.getRating().equalsIgnoreCase("1")){
                filters_rating= "3";
            }else if(event.getRating().equalsIgnoreCase("2")){
                filters_rating= "2";
            }else if(event.getRating().equalsIgnoreCase("3")){
                filters_rating= "1";
            }
        }

        filters_brands.addAll(event.getBrands());
        PAGE=1;
        fetchData();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFilterEvent3(FiltersEvent event) {

        isMale= event.getFilters_hashmap().get("isMale")==null?false:event.getFilters_hashmap().get("isMale");
        isFemale= event.getFilters_hashmap().get("isFemale")==null?false:event.getFilters_hashmap().get("isFemale");
        isUnisex= event.getFilters_hashmap().get("isUnisex")==null?false:event.getFilters_hashmap().get("isUnisex");

        isPopularity= event.getFilters_hashmap().get("isPopularity")==null?false:event.getFilters_hashmap().get("isPopularity");
        isPriceLow= event.getFilters_hashmap().get("isPriceLow")==null?false:event.getFilters_hashmap().get("isPriceLow");
        isPriceHigh= event.getFilters_hashmap().get("isPriceHigh")==null?false:event.getFilters_hashmap().get("isPriceHigh");
        isNearest= event.getFilters_hashmap().get("isNearest")==null?false:event.getFilters_hashmap().get("isNearest");

        isRating_1= event.getFilters_hashmap().get("isRating_1")==null?false:event.getFilters_hashmap().get("isRating_1");
        isRating_2= event.getFilters_hashmap().get("isRating_2")==null?false:event.getFilters_hashmap().get("isRating_2");
        isRating_3= event.getFilters_hashmap().get("isRating_3")==null?false:event.getFilters_hashmap().get("isRating_3");
        isRating_4= event.getFilters_hashmap().get("isRating_4")==null?false:event.getFilters_hashmap().get("isRating_4");

        isPrice_red= event.getFilters_hashmap().get("isPrice_red")==null?false:event.getFilters_hashmap().get("isPrice_red");
        isPrice_blue= event.getFilters_hashmap().get("isPrice_blue")==null?false:event.getFilters_hashmap().get("isPrice_blue");
        isPrice_green= event.getFilters_hashmap().get("isPrice_green")==null?false:event.getFilters_hashmap().get("isPrice_green");
        isPrice_all= event.getFilters_hashmap().get("isPrice_all")==null?false:event.getFilters_hashmap().get("isPrice_all");


        //--------------------------------------------sort------------------------------
        if(isPopularity){
            filters_sort="1";
        }else if(isPriceLow){
            filters_sort="2";
        }else if(isPriceHigh){
            filters_sort="3";
        }else if(isNearest){
            filters_sort="0";           //distance ka hai yeh
        }else{
            filters_sort=null;
        }

        //----------------------------------------------filters------------------------


        if(isRating_1){
            filters_rating="1";
        }else if(isRating_2){
            filters_rating="2";
        }else if(isRating_3){
            filters_rating="3";
        }else if(isRating_4){
            filters_rating="4";
        }else{
            filters_rating=null;
        }

        if(isPrice_red){
            filters_price= "0";
        }else if(isPrice_blue){
            filters_price= "1";
        }else if(isPrice_green){
            filters_price= "2";
        }else if(isPrice_all){
            filters_price= "3";
        }else{
            filters_price= null;
        }

        if(isMale){
            filters_gender="2";
        }else if (isFemale) {
            filters_gender="3";
        }else if(isUnisex){
            filters_gender="1";
        }else{
            filters_gender=null;
        }

        PAGE=1;
        fetchData();

        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isMale"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isFemale"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isUnisex"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isPopularity"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isPriceLow"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isPriceHigh"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isNearest"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isRating_1"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isRating_2"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isRating_3"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isRating_4"));
    }

    @Override
    public void onStart() {
        super.onStart();

        PAGE=1;
        if(list==null ||
                list.size()==0){
            if(getActivity()!=null){
                if(CheckConnection.isConnected(getActivity())){

                    fetchData();
                }else{

                    recycler_.setVisibility(View.GONE);
                    progress_.setVisibility(View.GONE);
                    btn_retry.setVisibility(View.VISIBLE);
//                    Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }

        }
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLocationChangeEvent(HomeAddressChangedEvent event) {

//        isMale=false; isFemale= false; isUnisex= false;
//        isPopularity= false; isPriceLow= false; isPriceHigh= false; isNearest= false;
//        isRating_1= false; isRating_2= false; isRating_3= false; isRating_4= false;
//        isPrice_red= false; isPrice_blue= false; isPrice_green= false; isPrice_all= false;
//        filters_rating= null; filters_price= null; filters_gender= null; filters_sort= "0";
        PAGE=1;
        if(getActivity()!=null){
            if(CheckConnection.isConnected(getActivity())){

                fetchData();
            }else{
                recycler_.setVisibility(View.GONE);
                progress_.setVisibility(View.GONE);
                btn_retry.setVisibility(View.VISIBLE);
//                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
            }

        }

    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
