package com.beusalons.android.Event;

import java.util.HashMap;

/**
 * Created by myMachine on 4/12/2017.
 */

public class FiltersEvent {

    private HashMap<String, Boolean> filters_hashmap= new HashMap<>();

    public FiltersEvent(){

    }

    public FiltersEvent(HashMap<String, Boolean> filters_hashmap){

        this.filters_hashmap= filters_hashmap;

    }

    public HashMap<String, Boolean> getFilters_hashmap() {
        return filters_hashmap;
    }
}
