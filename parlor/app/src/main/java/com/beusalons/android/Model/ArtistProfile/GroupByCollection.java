package com.beusalons.android.Model.ArtistProfile;

import java.util.List;

/**
 * Created by Ajay on 1/29/2018.
 */

public class GroupByCollection {
    private String name;
    private Boolean followingCollection;
    private String collecId;
    private List<Project_> projects = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getFollowingCollection() {
        return followingCollection;
    }

    public void setFollowingCollection(Boolean followingCollection) {
        this.followingCollection = followingCollection;
    }

    public String getCollecId() {
        return collecId;
    }

    public void setCollecId(String collecId) {
        this.collecId = collecId;
    }

    public List<Project_> getProjects() {
        return projects;
    }

    public void setProjects(List<Project_> projects) {
        this.projects = projects;
    }
}
