package com.beusalons.android.Fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.beusalons.android.Adapter.AdapterContactList;
import com.beusalons.android.Model.MymembershipDetails.ContactModel;
import com.beusalons.android.MyMembershipDetails;
import com.beusalons.android.R;
import com.beusalons.android.Utility.Utility;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Ajay on 9/16/2017.
 */

public class SelectContactDailog extends DialogFragment {
    RecyclerView recyclerViewContact;
    public static List<ContactModel> list=new LinkedList<>();
    public static List<ContactModel> contactSelectedList=new ArrayList<>();
    AdapterContactList adapterContactList;
    private EditText edtSearch;
    private Button btnCancel,btnSubmit;
    public  int totalUser=0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);          //no action bar space
        View view = inflater.inflate(R.layout.select_contact_dailog, container);
        Utility.getContactData(getActivity());
        recyclerViewContact=(RecyclerView)view.findViewById(R.id.recycler_view_contact);

        Log.i("listsize",""+list.size());

        Bundle args = getArguments();
        if (args != null )
            totalUser = args.getInt("totolUser");
        edtSearch=(EditText)view.findViewById(R.id.edt_search_name_or_number);
        btnCancel=(Button)view.findViewById(R.id.btnCancel);
        btnSubmit=(Button)view.findViewById(R.id.btnSubmit);
        adapterContactList = new AdapterContactList(getActivity(),list,totalUser);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewContact.setLayoutManager(mLayoutManager);
        recyclerViewContact.setAdapter(adapterContactList);
        inItView();
        return view;
    }



    private void inItView(){
        edtSearch.setCompoundDrawablesWithIntrinsicBounds(
                R.drawable.ic_add_search_icon, 0, 0, 0);


        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (s.toString().length() > 0) {
                    edtSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
                            0);
                } else {
                    // Assign your image again to the view, otherwise it will
                    // always be gone even if the text is 0 again.
                    edtSearch.setCompoundDrawablesWithIntrinsicBounds(
                            R.drawable.ic_add_search_icon, 0, 0, 0);
                }
                String text = edtSearch.getText().toString()
                        .toLowerCase(Locale.getDefault());
                adapterContactList.filter(text);

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedContactList();

                dismiss();
                MyMembershipDetails.isDismiss=true;
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactSelectedList.clear();
                totalUser=0;
               dismiss();
            }
        });

    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        Log.e("dismissdialog","dismiss");
        final Activity activity = getActivity();
        if (activity instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);
        }

    }

    private void selectedContactList(){

        if (contactSelectedList.size()>0){
            contactSelectedList.clear();
        }
        for (int i=0;i<list.size();i++){
              if (list.get(i).isSelected()){
                  contactSelectedList.add(list.get(i));
              }
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        list.clear();
    }
}
