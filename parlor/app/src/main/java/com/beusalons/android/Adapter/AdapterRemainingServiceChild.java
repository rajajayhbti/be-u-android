package com.beusalons.android.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Event.RemainingServicesEvent.RemainingServiceIndex;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.ServiceAvailable.Service;
import com.beusalons.android.R;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by Ajay on 10/5/2017.
 */

public class AdapterRemainingServiceChild  extends RecyclerView.Adapter<AdapterRemainingServiceChild.MyViewHolder>{

    private Activity activity;
    private List<Service> serviceArrayList;
    private int parlor_index;
   private boolean fromService;
    public AdapterRemainingServiceChild(Activity context,List<Service> list, int parlor_index,boolean fromService){

        this.activity=context;
        this.serviceArrayList=list;
        this.fromService=fromService;
        this.parlor_index= parlor_index;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view= LayoutInflater.from(activity).inflate(R.layout.row_remaining_service_child,parent,false);
        return new AdapterRemainingServiceChild.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position){

        if (fromService){
         holder.linearLayoutremaining.setVisibility(View.GONE);
        }
        final int remainingQuantity=serviceArrayList.get(position).getQuantity()-serviceArrayList.get(position).getMyQuantity();
        holder.linear_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("addmyservicetx","add");
                if (remainingQuantity>0){
                    EventBus.getDefault().post(new RemainingServiceIndex(parlor_index, position));

                }else{
                    Toast.makeText(activity,"No Remaining srvice",Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.txtViewAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("addmyservicetx","add");
                if (remainingQuantity>0){
                    EventBus.getDefault().post(new RemainingServiceIndex(parlor_index, position));

                }else{
                    Toast.makeText(activity,"No Remaining srvice",Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.txtPrice.setText(AppConstant.CURRENCY+(int)serviceArrayList.get(position).getPrice());
        holder.txtQuantityRemaining.setText("Remaining Quantity: "+remainingQuantity);
        if (serviceArrayList.get(position).getMyQuantity()>0){
            holder.linear_add_remove_one.setVisibility(View.VISIBLE);
            holder. linear_add_in.setVisibility(View.GONE);
            holder.txtViewQuantity.setText(""+serviceArrayList.get(position).getMyQuantity());
        }else{
            holder.linear_add_remove_one.setVisibility(View.GONE);
            holder. linear_add_in.setVisibility(View.VISIBLE);
        }


        final int index= position;


       holder.linear_add_one.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if (remainingQuantity>0){
                   EventBus.getDefault().post(new RemainingServiceIndex(parlor_index, position));

               }else{
                   Toast.makeText(activity,"No Remaining srvice",Toast.LENGTH_SHORT).show();
               }
           }
       });

        holder.linear_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        holder.txtName.setText(serviceArrayList.get(position).getName());
        holder.linearLayoutShowDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // new ShowDatesPopUp(activity,"","");
            }
        });
    }

    @Override
    public int getItemCount(){
        return serviceArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout linearLayoutShowDetails,linear_remove,linear_add,linear_add_one,linear_add_in,linear_add_remove_one,linearLayoutremaining;
        private TextView txtName,txtPrice,txtViewQuantity,txtViewAdd,txtQuantityRemaining;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtName=(TextView)itemView.findViewById(R.id.txt_name);
            txtPrice=(TextView)itemView.findViewById(R.id.txt_price);
            txtViewQuantity=(TextView)itemView.findViewById(R.id.txt_quantity);
            txtViewAdd=(TextView)itemView.findViewById(R.id.txtViewAdd);
            linearLayoutShowDetails=(LinearLayout)itemView.findViewById(R.id.linearLayout_ahow_details);
            linear_add=(LinearLayout)itemView.findViewById(R.id.linear_add);
            linear_add_one=(LinearLayout)itemView.findViewById(R.id.linear_add_one);
            linear_remove=(LinearLayout)itemView.findViewById(R.id.linear_remove);
            linear_add_in=(LinearLayout)itemView.findViewById(R.id.linear_add_in);
            linear_add_remove_one=(LinearLayout)itemView.findViewById(R.id.linear_add_remove_one);
            txtQuantityRemaining=(TextView)itemView.findViewById(R.id.txt_quantity_remaining);
            linearLayoutremaining=(LinearLayout)itemView.findViewById(R.id.linearLayoutremaining);
        }
    }
}