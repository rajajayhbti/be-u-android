package com.beusalons.android.Adapter.NewServiceDeals;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.newServiceDeals.Category;
import com.beusalons.android.R;
import com.beusalons.android.ServiceSpecificActivity;
import com.bumptech.glide.Glide;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by myMachine on 5/25/2017.
 */

public class DepartmentServicesAdapter extends RecyclerView.Adapter<DepartmentServicesAdapter.ViewHolder> {


    private Context context;
    private List<Category> list;
    private String gender, departmentId, department_name;
    private String home_membership;
    private UserCart userCart;
    AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;

    public DepartmentServicesAdapter(Context context, List<Category> list, String gender, UserCart userCart,
                                     String departmentId, String department_name, String home_membership){

        this.context= context;
        this.list= list;
        this.gender= gender;
        this.userCart= userCart;
        this.departmentId= departmentId;
        this.department_name= department_name;
        this.home_membership= home_membership;
        logger = AppEventsLogger.newLogger(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);


    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (list.size() > position) {
            final Category category = list.get(position);

            holder.txt_name.setText(category.getName());


            Glide.with(context)
                    .load(category.getImage())
//                .centerCrop()
                    .into(holder.img_);

            holder.linear_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    logServiceCategoryEvent(department_name,category.getName(),gender);
                    logServiceCategoryFireBaseEvent(department_name,category.getName(),gender);
                    Intent intent = new Intent(context, ServiceSpecificActivity.class);
                    intent.putExtra("department_id", departmentId);
                    intent.putExtra("department_name", department_name);
                    intent.putExtra("department_service", category.getName());
                    intent.putExtra("user_cart", new Gson().toJson(userCart, UserCart.class));
                    intent.putExtra("gender", gender);
                    intent.putExtra("membership", home_membership);
                    context.startActivity(intent);


                }
            });
        } else {

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            if (list.size() == 4) {
                if (position == 5) {
                    params.setMargins(-1, 1, 1, 1);
                    holder.linear_click.setLayoutParams(params);
                }
            } else if (list.size() == 1 && position == 2) {
                params.setMargins(-1, 1, 1, 1);
                holder.linear_click.setLayoutParams(params);
            } else if (list.size() == 7 && position == 7) {
                holder.linear_click.setLayoutParams(params);
            }


        }
    }



    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logServiceCategoryEvent (String depName,String name,String gender) {
        Log.e("category","fine"+depName+ "-"+name+"-"+gender);
        Bundle params = new Bundle();
        params.putString("Name",depName+ "-"+name+"-"+gender);
//        params.putString(AppConstant.DepName,depName);
//        params.putString(AppConstant.Gender,gender);
        logger.logEvent(AppConstant.ServiceCategory, params);
    }

    public void logServiceCategoryFireBaseEvent (String department_name,String name,String gender) {
        Log.e("categoryfirebase","fine"+department_name+"-"+name+"-"+gender);
        Bundle params = new Bundle();
        params.putString("Name", department_name+"-"+name+"-"+gender);
        mFirebaseAnalytics.logEvent(AppConstant.ServiceCategory, params);
    }
    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_name;
        private ImageView img_;
        private LinearLayout linear_click;


        public ViewHolder(View itemView) {
            super(itemView);
            txt_name= (TextView)itemView.findViewById(R.id.txt_name);
            img_= (ImageView)itemView.findViewById(R.id.img_);
            linear_click= (LinearLayout)itemView.findViewById(R.id.linear_click);

        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    @Override
    public int getItemCount() {
        if (list.size()==1 || list.size()==2){

            return 3;
        }else if (list.size()==4 || list.size()==5){
            return 6;
        }else if (list.size()==7 || list.size()==8){
            return 9;
        }else
        return list.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.services_department_services_grid, parent, false);
        return new ViewHolder(view);
    }

}
