package com.beusalons.android.Adapter.NewServiceDeals;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.beusalons.android.Event.NewServicesEvent.BrandEvent;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Brand_;
import com.beusalons.android.R;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 5/23/2017.
 */

public class BrandsListAdapter extends RecyclerView.Adapter<BrandsListAdapter.ViewHolder> {

    private Context context;
    private List<Brand_> list;
    private String brand_id, brand_name, service_name, service_deal_id, type;
    private int service_code, price_id, price, menu_price;
    private boolean hasProduct, hasTypes;
    AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;

    public BrandsListAdapter(Context context, List<Brand_> list, boolean hasProduct, boolean hasTypes){

        this.context= context;
        this.list= list;
        this.hasProduct= hasProduct;
        this.hasTypes= hasTypes;
        logger = AppEventsLogger.newLogger(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    public int getMenu_price(){
        return menu_price;
    }

    public String getbrandId(){
        return brand_id;
    }

    public String getBrandName(){
        return brand_name;
    }

    public String getService_name(){
        return service_name;
    }

    public int getService_code(){
        return service_code;
    }

    public int getPrice_id(){
        return price_id;
    }

    public int getPrice(){
        return price;
    }

    public String getService_deal_id(){
        return service_deal_id;
    }

    public String getType(){
        return type;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Brand_ brand_= list.get(position);
        holder.txt_brand_name.setText(brand_.getName());
        Log.i("btnvalue","value in service name: "+ brand_.getService_name()+ " " + brand_.getService_code());
        boolean isCheck;


        holder.txt_save_per.setVisibility(View.GONE);

        if(brand_.isCheck()){
            type= brand_.getType();
            service_name= brand_.getService_name();
            service_deal_id= brand_.getService_deal_id();
            service_code= brand_.getService_code();
            price_id= brand_.getPrice_id();
            price= brand_.getPrice();
            brand_id= brand_.getBrandId();
            brand_name= brand_.getName();
            menu_price= brand_.getMenu_price();

            isCheck= true;
            holder.radio_.setChecked(true);
        }else{
            isCheck= false;
            holder.radio_.setChecked(false);
        }

        if (position==list.size()-1){
            holder.viewDivider.setVisibility(View.GONE);
        }

       /* if(hasProduct || hasTypes){     //don't show price

            holder.txt_menu_price.setVisibility(View.GONE);

            holder.txt_price.setText(AppConstant.CURRENCY+(int)brand_.getPrice());

        }else{
*/

       //price with tax
        int price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*brand_.getPrice());
        holder.txt_price.setText(AppConstant.CURRENCY+(int)price_);

        if(brand_.getType().equalsIgnoreCase("service")){               //no deal so these both invisible

            holder.txt_menu_price.setVisibility(View.GONE);
        }else{

            holder.txt_menu_price.setVisibility(View.VISIBLE);
            holder.txt_menu_price.setTextColor(Color.parseColor("#808285"));

            //menu price with tax
            int menu_price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*brand_.getMenu_price());

            holder.txt_menu_price.setText(AppConstant.CURRENCY+menu_price_);
            holder.txt_menu_price.setPaintFlags(  holder.txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


        }

        final boolean setCheck= isCheck;
        holder.linear_radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(setCheck){

                    holder.radio_.setChecked(false);
                    list.get(position).setCheck(false);
                }else{

                    logEditServiceBrandChangeEvent(brand_.getName());
                    logEditServiceBrandChangeFireBaseEvent(brand_.getName());
                    Log.i("btnvalue", "i'm in the click radio ke: "+ position);
                    type= brand_.getType();
                    service_name= brand_.getService_name();
                    service_code= brand_.getService_code();
                    service_deal_id= brand_.getService_deal_id();
                    price_id= brand_.getPrice_id();
                    price= brand_.getPrice();
                    brand_id= brand_.getBrandId();
                    brand_name= brand_.getName();
                    menu_price= brand_.getMenu_price();

                    holder.radio_.setChecked(true);
                    list.get(position).setCheck(true);
                    EventBus.getDefault().post(new BrandEvent(position));

                }

                int selected_radio_= 0;
                for(int i=0;i<list.size();i++){

                    if(!list.get(i).isCheck()){

                        Log.i("i'mhere", "count: ++");
                        selected_radio_++;
                    }
                }

                Log.i("i'mhere", "selected size: "+ selected_radio_+ " " + list.size());
                if(selected_radio_== list.size()){

                    holder.radio_.setChecked(true);
                    list.get(position).setCheck(true);
                }
                updateView(position);
            }
        });


    }

    private void updateView(int pos){

        for(int i=0; i<list.size();i++){
            if(i!=pos){
                list.get(i).setCheck(false);
            }
        }
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        private RadioButton radio_;
        private TextView txt_price, txt_save_per, txt_menu_price, txt_brand_name;
        private LinearLayout linear_radio;
        private View viewDivider;

        public ViewHolder(View itemView) {

            super(itemView);
            viewDivider=(View)itemView.findViewById(R.id.view_for_divider);
            radio_= (RadioButton)itemView.findViewById(R.id.radio_);
            txt_price= (TextView)itemView.findViewById(R.id.txt_price);
            txt_save_per= (TextView)itemView.findViewById(R.id.txt_save_per);
            txt_menu_price= (TextView)itemView.findViewById(R.id.txt_menu_price);
            txt_brand_name= (TextView)itemView.findViewById(R.id.txt_brand_name);
            linear_radio= (LinearLayout)itemView.findViewById(R.id.linear_radio);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }
    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logEditServiceBrandChangeEvent (String name) {
        Log.e("EditServiceBrandChange","fine");
        Bundle params = new Bundle();
        params.putString("Name", name);
        logger.logEvent(AppConstant.EditServiceBrandChange, params);
    }
    public void logEditServiceBrandChangeFireBaseEvent (String name) {
        Log.e("EditServiceBrfireChange","fine");
        Bundle params = new Bundle();
        params.putString("Name", name);
        mFirebaseAnalytics.logEvent(AppConstant.EditServiceBrandChange, params);
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.fragment_services_specific_dialog_radio, parent, false);
        return new ViewHolder(view);
    }

}
