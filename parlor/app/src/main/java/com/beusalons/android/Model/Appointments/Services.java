package com.beusalons.android.Model.Appointments;

import java.util.List;

/**
 * Created by myMachine on 11/28/2016.
 */

public class Services {

    private String name;
    private double price;
    private List<Employees> employees = null;
    private int estimatedTime;
    private int additions;
    private int quantity;
    private int count;
    private int code;
    private double tax;
    private String dealId;
    private String type;
    private Boolean dealPriceUsed;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<Employees> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employees> employees) {
        this.employees = employees;
    }

    public int getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(int estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public int getAdditions() {
        return additions;
    }

    public void setAdditions(int additions) {
        this.additions = additions;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getDealPriceUsed() {
        return dealPriceUsed;
    }

    public void setDealPriceUsed(Boolean dealPriceUsed) {
        this.dealPriceUsed = dealPriceUsed;
    }
}
