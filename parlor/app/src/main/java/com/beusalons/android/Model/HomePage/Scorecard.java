package com.beusalons.android.Model.HomePage;

import java.util.List;

/**
 * Created by bhrigu on 1/24/2018.
 */

public class Scorecard {


    private String type;
    private String title;
    private List<List_> list = null;

    public class List_{

        private double loyalityPoints;
        private double coupons;
        private double freeServices;
        private double familyWallet;
        private double subscription;
        private String subscriptionExpiry;
        private String couponExpiry;
        private String freeServiceExpiry;
        private String freebieExpiry;

        public String getSubscriptionExpiry() {
            return subscriptionExpiry;
        }

        public void setSubscriptionExpiry(String subscriptionExpiry) {
            this.subscriptionExpiry = subscriptionExpiry;
        }

        public String getCouponExpiry() {
            return couponExpiry;
        }

        public void setCouponExpiry(String couponExpiry) {
            this.couponExpiry = couponExpiry;
        }

        public String getFreeServiceExpiry() {
            return freeServiceExpiry;
        }

        public void setFreeServiceExpiry(String freeServiceExpiry) {
            this.freeServiceExpiry = freeServiceExpiry;
        }

        public String getFreebieExpiry() {
            return freebieExpiry;
        }

        public void setFreebieExpiry(String freebieExpiry) {
            this.freebieExpiry = freebieExpiry;
        }

        public double getLoyalityPoints() {
            return loyalityPoints;
        }

        public void setLoyalityPoints(double loyalityPoints) {
            this.loyalityPoints = loyalityPoints;
        }

        public double getCoupons() {
            return coupons;
        }

        public void setCoupons(double coupons) {
            this.coupons = coupons;
        }

        public double getFreeServices() {
            return freeServices;
        }

        public void setFreeServices(double freeServices) {
            this.freeServices = freeServices;
        }

        public double getFamilyWallet() {
            return familyWallet;
        }

        public void setFamilyWallet(double familyWallet) {
            this.familyWallet = familyWallet;
        }

        public double getSubscription() {
            return subscription;
        }

        public void setSubscription(double subscription) {
            this.subscription = subscription;
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<List_> getList() {
        return list;
    }

    public void setList(List<List_> list) {
        this.list = list;
    }
}
