package com.beusalons.android.Model.Deal;

import java.io.Serializable;

/**
 * Created by Ajay on 1/20/2017.
 */

public class DealsImageUrl implements Serializable {


    private String imageUrl;

    public String getUrl() {
        return imageUrl;
    }

    public void setUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
