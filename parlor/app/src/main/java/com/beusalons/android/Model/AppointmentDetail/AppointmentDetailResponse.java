package com.beusalons.android.Model.AppointmentDetail;

/**
 * Created by myMachine on 12/13/2016.
 */

public class AppointmentDetailResponse {

    private boolean success;
    private String message;
    private AppointmentDetailDataResponse data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public AppointmentDetailDataResponse getData() {
        return data;
    }

    public void setData(AppointmentDetailDataResponse data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
