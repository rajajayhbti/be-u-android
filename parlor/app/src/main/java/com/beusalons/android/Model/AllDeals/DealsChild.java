package com.beusalons.android.Model.AllDeals;

import java.util.List;

/**
 * Created by myMachine on 4/28/2017.
 */

public class DealsChild {

    private String childName;

    private String name;
    private String gender;
    private int serviceCode;

    private String dealId;
    private int dealIdParlor;
    private String dealType;

    private double price;
    private double menuPrice;

    private String description;
    private int weekday;

    private int quanity;            //yeh to imp hai bhai

    private boolean hasManyChild;

    private List<DealsDialogChild> list;

    public List<DealsDialogChild> getList() {
        return list;
    }

    public void setList(List<DealsDialogChild> list) {
        this.list = list;
    }

    public boolean isHasManyChild() {
        return hasManyChild;
    }

    public void setHasManyChild(boolean hasManyChild) {
        this.hasManyChild = hasManyChild;
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    public int getQuanity() {
        return quanity;
    }

    public void setQuanity(int quanity) {
        this.quanity = quanity;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public int getDealIdParlor() {
        return dealIdParlor;
    }

    public void setDealIdParlor(int dealIdParlor) {
        this.dealIdParlor = dealIdParlor;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(double menuPrice) {
        this.menuPrice = menuPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getWeekday() {
        return weekday;
    }

    public void setWeekday(int weekday) {
        this.weekday = weekday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(int serviceCode) {
        this.serviceCode = serviceCode;
    }
}
