package com.beusalons.android.Event;

/**
 * Created by Ajay on 6/8/2017.
 */

public class DealsEvent {

    private String parlorId;
    private String departmentId;

    public DealsEvent(String parlorId, String departmentId){

        this.parlorId= parlorId;
        this.departmentId= departmentId;
    }


    public String getParlorId() {
        return parlorId;
    }

    public String getDepartmentId() {
        return departmentId;
    }
}
