package com.beusalons.android.Helper;

import android.content.Context;
import android.content.SharedPreferences;

import static com.beusalons.android.Helper.AppConstant.LOCATION_PREF;
import static com.beusalons.android.Helper.AppConstant.USER_PREF;

/**
 * Created by Robbin Singh on 03/11/2016.
 */

public class ConstantHelper {

    //
    private static String latitude, longitude, location, lastParlorId, lastParlorName;

    //User info
    private static String userName, userId, userAccessToken, userProfileImage,regId;

    public void createLocation(Context context, String latitude, String longitude, String locationName){
        context.getSharedPreferences(LOCATION_PREF, context.MODE_PRIVATE).edit()
                .putString("latitude", latitude).putString("longitude", longitude).putString("location", locationName).commit();
    }


    public void createUser(Context context, String userId, String accessToken, String name, String profileImage){
        context.getSharedPreferences(USER_PREF, context.MODE_PRIVATE).edit()
                .putString("name", name).putString("accessToken", accessToken).putString("userId", userId).putString("profileImage", profileImage).commit();
    }

    public void initLocation(Context context)
    {
        SharedPreferences preferences= context.getSharedPreferences(LOCATION_PREF,context.MODE_PRIVATE);
        this.latitude = preferences
                .getString("latitude", null);
        this.longitude = preferences
                .getString("longitude", null);
        this.location = preferences
                .getString("location", null);

    }

    public void initUser(Context context)
    {
        SharedPreferences preferences= context.getSharedPreferences(USER_PREF,context.MODE_PRIVATE);
        this.userId = preferences
                .getString("userId", null);
        this.userName = preferences
                .getString("name", null);
        this.userAccessToken = preferences
                .getString("accessToken", null);
        this.userProfileImage = preferences
                .getString("profileImage", null);
        this.regId=preferences
                .getString("regId",null);
    }

    public static String getLastParlorId() {
        return lastParlorId;
    }

    public static String getLatitude() {
        return latitude;
    }

    public static String getLongitude() {
        return longitude;
    }

    public static String getLastParlorName() {
        return lastParlorName;
    }

    public static String getLocation() {
        return location;
    }

    public static String getUserId() {
        return userId;
    }

    public static String getRegId() {
        return regId;
    }

    public static String getUserName() {
        return userName;
    }

    public static String getUserAccessToken() {
        return userAccessToken;
    }

    public static String getUserProfileImage() {
        return userProfileImage;
    }
}
