package com.beusalons.android.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.DealsData.DealsData;
import com.beusalons.android.Model.newServiceDeals.Department;
import com.beusalons.android.R;
import com.bumptech.glide.Glide;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

/**
 * Created by Ajay on 6/7/2017.
 */

public class DealsFragmentAdapter extends RecyclerView.Adapter<DealsFragmentAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Department> list;
    private String gender;
    AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;
    private DealsData dealData;
    public DealsFragmentAdapter(Context context, ArrayList<Department> list, String gender, DealsData dealsData){

        this.context= context;
        this.list= list;
        this.gender= gender;
        this.dealData=dealsData;
        logger = AppEventsLogger.newLogger(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    public void setData(ArrayList<Department> list, String gender,DealsData dealsData){

        this.list= list;
        this.gender= gender;
        this.dealData=dealsData;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final DealsFragmentAdapter.ViewHolder holder, final int position) {

        final Department department= list.get(position);

        holder.txt_name.setText(department.getName());
        holder.txt_info.setText(department.getDescription());




        if(department.isSelected()){
            holder.img_animation.setImageResource(R.drawable.ic_arrow_down);
            holder.rec_services.setVisibility(View.VISIBLE);
            if (position % 2==0){
                holder.LlContainer.setBackgroundColor(context.getResources().getColor(R.color.yellow_fedded));
            }else {
                holder.LlContainer.setBackgroundColor(context.getResources().getColor(R.color.blue_DARK));
            }

            GridLayoutManager layoutManager= new GridLayoutManager(context, 3);
            holder.rec_services.setLayoutManager(layoutManager);
            DepartmentDealsAdapter adapter= new DepartmentDealsAdapter(context, department.getCategories(),
                    gender,  department.getDepartmentId(), department.getName(), list,dealData);
            holder.rec_services.setAdapter(adapter);
        }else{

            holder.LlContainer.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.img_animation.setImageResource(R.drawable.ic_right);
            holder.rec_services.setVisibility(View.GONE);
        }

        final int pos= position;
        holder.linear_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(department.isSelected()){
                    holder.img_animation.setImageResource(R.drawable.ic_right);
                    holder.rec_services.setVisibility(View.GONE);
                    holder.LlContainer.setBackgroundColor(context.getResources().getColor(R.color.white));
                    department.setSelected(false);

                }else{

                    logDealDepartmentEvent(department.getName()+"- "+gender);
                    logDealDepartmentFireBaseEvent(department.getName()+"- "+gender);
                    if (position % 2==0){
                        holder.LlContainer.setBackgroundColor(context.getResources().getColor(R.color.yellow_fedded));

                    }else {
                        holder.LlContainer.setBackgroundColor(context.getResources().getColor(R.color.blue_DARK));
                    }
                    Log.e("in services","test");
                    holder.img_animation.setImageResource(R.drawable.ic_arrow_down);


                    holder.rec_services.setVisibility(View.VISIBLE);
                    GridLayoutManager layoutManager= new GridLayoutManager(context, 3);
                    holder.rec_services.setLayoutManager(layoutManager);

                    DepartmentDealsAdapter adapter= new DepartmentDealsAdapter(context, department.getCategories(),
                            gender,  department.getDepartmentId(), department.getName(),list,dealData);
                    holder.rec_services.setAdapter(adapter);
                    department.setSelected(true);
                    updateView(pos);
                }



            }
        });


        Glide.with(context)
                .load(department.getImage())
//                .centerCrop()
                .into(holder.img_);




    }


    public void updateView(int pos){
        for(int i=0;i<list.size();i++){
            if(i!=pos){
                list.get(i).setSelected(false);

            }
        }
        notifyDataSetChanged();
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logDealDepartmentEvent (String name) {
        Log.e("dealDepaertment","fine"+name);
        Bundle params = new Bundle();
        params.putString("Name", name);
        logger.logEvent(AppConstant.DealDepartment, params);
    }

    public void logDealDepartmentFireBaseEvent (String name) {
        Log.e("dealDepaertmentFireBase","fine"+name);
        Bundle params = new Bundle();
        params.putString("Name", name);
        mFirebaseAnalytics.logEvent(AppConstant.DealDepartment, params);
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_name, txt_info;
        private ImageView img_, img_animation;
        private RelativeLayout linear_title;
        LinearLayout LlContainer;
        private RecyclerView rec_services;

        public ViewHolder(View itemView) {
            super(itemView);

            txt_name= (TextView)itemView.findViewById(R.id.txt_name);
            txt_info= (TextView)itemView.findViewById(R.id.txt_info);
            img_= (ImageView) itemView.findViewById(R.id.img_);
            img_animation= (ImageView)itemView.findViewById(R.id.img_animation);
            linear_title= (RelativeLayout) itemView.findViewById(R.id.linear_title);
            LlContainer= (LinearLayout) itemView.findViewById(R.id.ll_container_category_list);
            rec_services= (RecyclerView) itemView.findViewById(R.id.rec_services);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    @Override
    public int getItemCount() {

        return list.size();

    }

    @Override
    public DealsFragmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.deals_department_list, parent, false);
        return new DealsFragmentAdapter.ViewHolder(view);
    }
}