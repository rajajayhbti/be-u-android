package com.beusalons.android.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


/**
 * Created by Robbin Singh on 10/11/2016.
 */

public class AdditionsModel {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("types")
    @Expose
       private  List<ServiceLengthModel> types;


//    public List<String> getTypes() {
//        return types;
//    }
//
//    public void setTypes(List<String> types) {
//        this.types = types;
//    }



    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    public List<ServiceLengthModel> getTypes() {
        return types;
    }

    public void setTypes(List<ServiceLengthModel> types) {
        this.types = types;
    }
}

