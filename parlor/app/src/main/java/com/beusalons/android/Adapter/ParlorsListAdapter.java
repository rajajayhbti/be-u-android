package com.beusalons.android.Adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beusalons.android.Event.CartNotificationEvent;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.ParlorModel;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.R;
import com.beusalons.android.SalonPageActivity;
import com.bumptech.glide.Glide;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.snappydb.DB;
import com.snappydb.DBFactory;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import static com.beusalons.android.Model.ParlorModel.PARLOR_TYPE;
import static com.beusalons.android.Model.ParlorModel.PROGRESS_TYPE;


/**
 * Created by Robbin Singh on 03/11/2016.
 */

public class ParlorsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ParlorModel> list;
    private Activity activity;
    private AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;

    public ParlorsListAdapter(List<ParlorModel> list, Activity activity) {
        this.list = list;
        this.activity = activity;
        logger = AppEventsLogger.newLogger(activity);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
    }

    public void setInitialData(List<ParlorModel> list){

        this.list=list;
        notifyDataSetChanged();
    }

    public void addData(List<ParlorModel> list){

        int size= this.list.size();
        Log.i("sizeing", "size of new: "+ list.size()+ " "+ "old: "+ this.list.size());
        this.list.addAll(list);             //add kara
        Log.i("sizeing", "size: "+ this.list.size());
        notifyItemRangeInserted(size, this.list.size());        //existing size, aur new size
    }

    public void addProgress(){

        if(list!=null && list.size()>0){

            list.add(new ParlorModel(1));
            notifyItemInserted(list.size()-1);
        }
    }

    public void removeProgress(){

        int size= list!=null && list.size()>0?list.size():0;

        if(size>0){

            list.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }


    public class ViewHolder1 extends RecyclerView.ViewHolder {

        private ImageView img_salon, img_type, img_fav;
        private TextView txt_name, txt_address, txt_cost, txt_rating, txt_distance,checked_in;
        private LinearLayout linear_click;

        public ViewHolder1(View view) {
            super(view);

            img_salon= (ImageView)view.findViewById(R.id.img_salon);
            img_type= (ImageView)view.findViewById(R.id.img_type);
            img_fav= (ImageView)view.findViewById(R.id.img_fav);
            txt_name= (TextView) view.findViewById(R.id.txt_name);
//            checked_in= (TextView) view.findViewById(R.id.checked_in);
            txt_address= (TextView)view.findViewById(R.id.txt_address);
            txt_cost= (TextView)view.findViewById(R.id.txt_cost);
            txt_rating= (TextView)view.findViewById(R.id.txt_rating);
            txt_distance= (TextView)view.findViewById(R.id.txt_distance);
            linear_click= (LinearLayout)view.findViewById(R.id.linear_click);
        }
    }

    public class ViewHolder2 extends RecyclerView.ViewHolder{

        private ProgressBar progress_;
        public ViewHolder2(View itemView) {
            super(itemView);
            progress_= (ProgressBar)itemView.findViewById(R.id.progress_);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        switch (viewType){
            case PARLOR_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.salon_list, parent, false);
                return new ViewHolder1(view);

            case PROGRESS_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_progress, parent, false);
                return new ViewHolder2(view);
        }
        return  null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        switch (list.get(position).getType()){

            case PARLOR_TYPE:

                ((ViewHolder1)holder).txt_name.setText(list.get(position).getName());
                ((ViewHolder1)holder).txt_address.setText(list.get(position).getAddress1()+ " "+list.get(position).getAddress2());
                Glide.with(activity)
                        .load(list.get(position).getImages().get(0).getAppImageUrl())
                        .into(((ViewHolder1)holder).img_salon);

                ((ViewHolder1)holder).txt_distance.setText(list.get(position).getDistance()+"Km");

                if(list.get(position).getParlorType()==0)
                    ((ViewHolder1)holder).img_type.setImageResource(R.drawable.ic_premium_badge);
                else if(list.get(position).getParlorType()==1)
                    ((ViewHolder1)holder).img_type.setImageResource(R.drawable.ic_standard_badge);
                else
                    ((ViewHolder1)holder).img_type.setImageResource(R.drawable.ic_budget_badge);

                ((ViewHolder1)holder).txt_rating.setText(""+list.get(position).getRating());

              /*  if (position==0 ||position==5){
                    ((ViewHolder1) holder).checked_in.setVisibility(View.VISIBLE);
                }else ((ViewHolder1) holder).checked_in.setVisibility(View.GONE);*/
                int price= list.get(position).getPrice();
                String cost="";
                if(price==1)
                    cost= "<font color='#3e780a'>₹</font>"+" "+"₹"+" "+"₹"+" "+"₹"+" "+"₹";
                else if(price==2)
                    cost= "<font color='#3e780a'>₹ ₹</font>"+" "+"₹"+" "+"₹"+" "+"₹";
                else if(price==3)
                    cost= "<font color='#3e780a'>₹ ₹ ₹</font>"+" "+"₹"+" "+"₹";
                else if(price==4)
                    cost= "<font color='#3e780a'>₹ ₹ ₹ ₹</font>"+" "+"₹";
                else
                    cost= "<font color='#3e780a'>₹ ₹ ₹ ₹ ₹</font>";
                ((ViewHolder1)holder).txt_cost.setText(fromHtml(cost));

                if(list.get(position).getFavourite())
                    ((ViewHolder1)holder).img_fav.setVisibility(View.VISIBLE);
                else
                    ((ViewHolder1)holder).img_fav.setVisibility(View.GONE);

                ((ViewHolder1)holder).linear_click.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        doStuff(position);
                    }
                });
                break;

            case PROGRESS_TYPE:

//                ((ViewHolder2)holder).progress_.getIndeterminateDrawable().setColorFilter(
//                        ContextCompat.getColor(activity, R.color.fontColor), PorterDuff.Mode.MULTIPLY);


                break;

        }
    }


    private void doStuff(final int position){

        boolean show_dialog= false;

        logSalonClickEvent();
        logSalonClickFireBaseEvent();
        logSalonListTileEvent(String.valueOf(position));
        //opening cart, checking cart type equal to service type, and parlor id are same, if not same show dialog
        UserCart saved_cart = null;
        try {

            DB snappyDB = DBFactory.open(activity);
            //db mai jo saved cart hai
            if (snappyDB.exists(AppConstant.USER_CART_DB)) {

                saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
            }
            snappyDB.close();

            if(saved_cart!=null){

                if(saved_cart.getCartType().equalsIgnoreCase(AppConstant.SERVICE_TYPE)){

                    if(saved_cart.getParlorId().equalsIgnoreCase(list.get(position).getParlorId()))
                        show_dialog= false;
                    else{

                        if(saved_cart.getServicesList().size()>0)
                            show_dialog= true;
                        else
                            show_dialog= false;
                    }
                }else
                    show_dialog= false;
            }else
                show_dialog= false;

        }catch (Exception e){
            e.printStackTrace();
        }

        if(show_dialog){

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle("Replace cart item?");
            builder.setMessage("Your cart contains services from "+ saved_cart.getParlorName()+". Do you " +
                    "wish to discard the selection and add services from "+ list.get(position).getName()+"?");
            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(final DialogInterface dialog, int id) {

                    try {

                        DB snappyDB = DBFactory.open(activity);
                        if(snappyDB.exists(AppConstant.USER_CART_DB)) {
                            Log.i("mainyahatukaha", " existing hai");
                            snappyDB.del(AppConstant.USER_CART_DB);
                        }
                        snappyDB.close();
                        EventBus.getDefault().post(new CartNotificationEvent());
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    Intent intent = new Intent(activity, SalonPageActivity.class);
                    intent.putExtra("parlorId", list.get(position).getParlorId());
                    activity.startActivity(intent);

                }
            });
            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    dialog.dismiss();
                }
            }).show();

        }else{

            Intent intent = new Intent(activity, SalonPageActivity.class);
            intent.putExtra("parlorId", list.get(position).getParlorId());
            activity.startActivity(intent);
        }
    }
    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logSalonListTileEvent (String index) {
        Log.e("SalonClickFirebase","fine"+index);

        Bundle params = new Bundle();
        params.putString("index", index);
        logger.logEvent(AppConstant.SalonListTile, params);
    }

    private void logSalonClickEvent () {
        Log.e("SalonClick","fine");
        logger.logEvent(AppConstant.SalonClick);
    }
    private void logSalonClickFireBaseEvent () {
        Log.e("SalonClickFirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.SalonClick,bundle);
    }

    @Override
    public int getItemCount() {

        if(list!=null && list.size()>0)
            return list.size();

        return 0;
    }

    @Override
    public int getItemViewType(int position) {

        if(list!=null && list.size()>0)
            return list.get(position).getType();
        return 0;
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }



}
