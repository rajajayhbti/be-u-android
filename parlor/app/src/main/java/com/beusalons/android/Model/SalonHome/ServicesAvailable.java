package com.beusalons.android.Model.SalonHome;

import com.beusalons.android.Model.ServiceAvailable.Service;

import java.util.List;

/**
 * Created by Ajay on 10/9/2017.
 */

public class ServicesAvailable {
    private String expiryDate;
    private List<Service> services = null;

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }
}
