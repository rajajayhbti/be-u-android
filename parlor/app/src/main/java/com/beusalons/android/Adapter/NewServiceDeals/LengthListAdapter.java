package com.beusalons.android.Adapter.NewServiceDeals;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Addition;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Type;
import com.beusalons.android.R;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.bumptech.glide.Glide;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by myMachine on 5/24/2017.
 */

public class LengthListAdapter extends RecyclerView.Adapter<LengthListAdapter.ViewHolder>{

    private Context context;
    private Addition addition;
    int size= 0;
    private String type_name, service_deal_id, service_name, type_;
    private int type_index, price, service_code, price_id, additions, menu_price;
    AppEventsLogger logger;

    private FirebaseAnalytics mFirebaseAnalytics;

    public LengthListAdapter(Context context, Addition addition){

        this.context= context;
        this.addition= addition;
        logger = AppEventsLogger.newLogger(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);


    }

    public int getMenu_price(){
        return menu_price;
    }

    public void setLengthList(Addition addition){

        this.addition= addition;
        notifyDataSetChanged();
    }

    public String getType_name(){
        return type_name;
    }

    public int getType_index(){
        return type_index;
    }

    public int getPrice(){
        return price;
    }

    public String getService_deal_id(){
        return service_deal_id;
    }

    public int getService_code(){
        return service_code;
    }

    public String getService_name(){
        return service_name;
    }

    public int getPrice_id(){
        return price_id;
    }

    public String getType(){
        return type_;
    }

    public int getAdditions(){
        return additions;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        // hide it

        final Type type= addition.getTypes().get(position);


        if(addition.getName().equalsIgnoreCase("Length")){

            String arr=type.getName().replace("(","-");
            String test []=arr.split("-");
            String name=test[1]+"\n"+"("+test[2];
            holder.txt_name.setText(name);


        }else{      //upstyling ya shampoo length

            holder.txt_name.setText(type.getName());
        }

        //price with tax
        int price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*type.getPrice());
        holder.txt_price.setText(AppConstant.CURRENCY+price_);

        if(type.getType()!=null && type.getType().equalsIgnoreCase("service")){     //no deal so gone

            holder.txt_menu_price.setVisibility(View.GONE);
            holder.txt_save_per.setVisibility(View.GONE);
        }else{
            holder.txt_menu_price.setVisibility(View.VISIBLE);
            holder.txt_save_per.setVisibility(View.VISIBLE);

            //menu price with tax
            int menu_price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*type.getMenu_price());
            holder.txt_menu_price.setText(AppConstant.CURRENCY+ menu_price_);
            holder.txt_menu_price.setTextColor(Color.parseColor("#808285"));
            holder.txt_menu_price.setPaintFlags(  holder.txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            holder.txt_save_per.setText(AppConstant.SAVE+" "+
                    (100 -((price_*100)/menu_price_))+"%");
            holder.txt_save_per.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
            holder.txt_save_per.setBackgroundResource(R.drawable.discount_seletor);
        }


        // use for vertical divider
        if (position==size-1){
            holder.viewDivider.setVisibility(View.GONE);
        }
        boolean isCheck= false;
        if(type.isCheck()){
            additions= type.getAdditions();
            type_= type.getType();
            type_name= type.getName();
            type_index= position;
            price= type.getPrice();
            service_deal_id= type.getService_deal_id();
            service_code= type.getService_code();
            service_name= type.getService_name();
            price_id= type.getPrice_id();
            menu_price= type.getMenu_price();

            holder.radio_.setChecked(true);
            isCheck= true;
        }else{
            holder.radio_.setChecked(false);
            isCheck= false;
        }
        final boolean check_= isCheck;


        holder.linear_radio_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(check_){

                    holder.radio_.setChecked(false);
                    addition.getTypes().get(position).setCheck(false);

                }else{

                    logEditServiceLengthChangeEvent(type.getName());
                    logEditServiceLengthChangeFireBaseEvent(type.getName());
                    additions= type.getAdditions();
                    type_= type.getType();
                    type_name= type.getName();
                    type_index= position;
                    price= type.getPrice();
                    service_deal_id= type.getService_deal_id();
                    service_code= type.getService_code();
                    service_name= type.getService_name();
                    price_id= type.getPrice_id();
                    menu_price= type.getMenu_price();

                    Log.i("valueinstufff", "values: "+ type.getMenu_price());

                    holder.radio_.setChecked(true);
                    addition.getTypes().get(position).setCheck(true);
                }

                int selected_radio_= 0;
                for(int i=0;i<addition.getTypes().size();i++){

                    if(!addition.getTypes().get(i).isCheck()){

                        Log.i("i'mhere", "count: ++");
                        selected_radio_++;
                    }
                }

                Log.i("i'mhere", "selected size: "+ selected_radio_+ " " + addition.getTypes().size());
                if(selected_radio_== addition.getTypes().size()){

                    holder.radio_.setChecked(true);
                    addition.getTypes().get(position).setCheck(true);
                }

                updateView(position);
            }
        });

        Glide.with(context)
                .load(type.getImageUrl())
//                .centerCrop()
                .into(holder.img_);


    }

    private void updateView(int pos){

        for(int i=0; i<addition.getTypes().size();i++){
            if(i!=pos){
                addition.getTypes().get(i).setCheck(false);
            }
        }
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {

        return addition.getTypes().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_name, txt_menu_price, txt_price, txt_save_per,txt_name_subtitle;
        private RadioButton radio_;
        private ImageView img_;
        private LinearLayout linear_radio_;
        private View viewDivider;
        public ViewHolder(View itemView) {
            super(itemView);
            viewDivider=(View)itemView.findViewById(R.id.view_for_divder_lenght);
            txt_name= (TextView)itemView.findViewById(R.id.txt_name);
            txt_menu_price= (TextView)itemView.findViewById(R.id.txt_menu_price);
            txt_price= (TextView)itemView.findViewById(R.id.txt_price);
            txt_save_per= (TextView)itemView.findViewById(R.id.txt_save_per);

            radio_= (RadioButton) itemView.findViewById(R.id.radio_);
            img_= (ImageView)itemView.findViewById(R.id.img_);
            linear_radio_= (LinearLayout)itemView.findViewById(R.id.linear_radio_);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logEditServiceLengthChangeEvent (String name) {
        Log.e("EditServiceLengthChange","fine");
        Bundle params = new Bundle();
        params.putString("Name", name);
        logger.logEvent(AppConstant.EditServiceLengthChange, params);
    }

    public void logEditServiceLengthChangeFireBaseEvent (String name) {
        Log.e("EditServicefirebsLength","fine");
        Bundle params = new Bundle();
        params.putString("Name", name);
        mFirebaseAnalytics.logEvent(AppConstant.EditServiceLengthChange, params);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.fragment_services_specific_dialog_length, parent, false);
        return new ViewHolder(view);
    }
}
