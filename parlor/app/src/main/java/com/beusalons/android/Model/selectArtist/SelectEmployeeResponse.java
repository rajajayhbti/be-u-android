package com.beusalons.android.Model.selectArtist;

import java.util.ArrayList;

/**
 * Created by Ashish Sharma on 12/29/2017.
 */

public class SelectEmployeeResponse {

    private boolean success;
    private ArtistData data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArtistData getData() {
        return data;
    }

    public void setData(ArtistData data) {
        this.data = data;
    }

}
