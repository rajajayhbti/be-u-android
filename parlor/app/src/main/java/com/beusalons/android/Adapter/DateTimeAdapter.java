package com.beusalons.android.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.BillSummaryActivity;
import com.beusalons.android.BookingSummaryActivity;
import com.beusalons.android.DateTimeActivity;
import com.beusalons.android.Event.CancelAppointment;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.AppointmentDetail.AppointmentDetailPost;
import com.beusalons.android.Model.AppointmentDetail.AppointmentDetailResponse;
import com.beusalons.android.Model.Appointments.AppointmentPost;
import com.beusalons.android.Model.Appointments.AppointmentServicesPost;
import com.beusalons.android.Model.Appointments.CeateAppointMent.PackageService_;
import com.beusalons.android.Model.DateTimeModel;
import com.beusalons.android.Model.RescheduleAppointmentPost;
import com.beusalons.android.Model.RescheduleAppointmentResponse;
import com.beusalons.android.Model.SalonHome.HomeResponse;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.SalonPageActivity;
import com.beusalons.android.SalonServicesActivity;
import com.beusalons.android.Service.FetchDepartmentsService;
import com.beusalons.android.ServiceSpecificActivity;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.snappydb.DB;
import com.snappydb.DBFactory;

import org.greenrobot.eventbus.EventBus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.beusalons.android.Model.DateTimeModel.IMAGE_TYPE;
import static com.beusalons.android.Model.DateTimeModel.TIME_TYPE;

/**
 * Created by myMachine on 11/21/2016.
 */

public class DateTimeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    Activity activity;

    List<DateTimeModel> details= new ArrayList<>();
    AppEventsLogger logger;
    private int selected_position  = 0;

    private String apptId, home_response;
    private boolean isReschedule;
    public String DepartmentId="";
    public String departmentName="";
    public int index=-1;
    public String gender="";
    private FirebaseAnalytics mFirebaseAnalytics;

    private boolean isReorder;

    // private static String appt_id;                  //appiontment from the notification
    public DateTimeAdapter(Activity activity, List<DateTimeModel> details,
                           String apptId, boolean isReschedule, String home_response, boolean isReorder){
        this.details= details;
        this.activity= activity;
        logger = AppEventsLogger.newLogger(activity);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);

        this.home_response= home_response;
        this.apptId= apptId;
        this.isReschedule= isReschedule;
        this.isReorder= isReorder;

    }
    public void setHomeData(String homedata){
        this.home_response=homedata;
    }
    public  DateTimeAdapter(Activity activity, List<DateTimeModel> details,
                            String apptId, boolean isReschedule, String home_response, String DepartmentId, String departmentName, int index, String gender){
        this.details= details;
        this.activity= activity;
        logger = AppEventsLogger.newLogger(activity);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);

        this.home_response= home_response;
        this.apptId= apptId;
        this.isReschedule= isReschedule;
        this.gender=gender;
        this.DepartmentId=DepartmentId;
        this.departmentName=departmentName;
        this.index=index;
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder{

        private ImageView view_image;
        private TextView txt_image;
        public ImageViewHolder(View itemView) {
            super(itemView);
            view_image= (ImageView)itemView.findViewById(R.id.view_time_slot_image);
            txt_image= (TextView)itemView.findViewById(R.id.txt_image);
        }
    }

    public class TimeViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_time;
        public TimeViewHolder(View itemView) {
            super(itemView);
            txt_time= (TextView)itemView.findViewById(R.id.txt_time_slot_time);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case IMAGE_TYPE:

                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_date_time_image, parent, false);
                return new ImageViewHolder(view);
            case TIME_TYPE:

                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_date_time_time, parent, false);
                return new TimeViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position){

        final DateTimeModel modal= details.get(position);
        if (modal != null) {
            switch (modal.getType()) {
                case IMAGE_TYPE:
                    ((ImageViewHolder)holder).txt_image.setText(modal.getTxt_image());
                    ((ImageViewHolder)holder).view_image.setBackgroundResource(modal.getImage_id());

                    break;
                case TIME_TYPE:

                    ((TimeViewHolder)holder).txt_time.setText(modal.getDisplay_time());

                    if(selected_position  == position){
                        // Here I am just highlighting the background
                        ((TimeViewHolder)holder).txt_time.setBackgroundResource(R.drawable.txt_background_primary);
                        ((TimeViewHolder)holder).txt_time.setTextColor(Color.parseColor("#ffffff"));
                    }else{
                        ((TimeViewHolder)holder).txt_time.setBackgroundResource(R.drawable.txt_border_line);
                        ((TimeViewHolder)holder).txt_time.setTextColor(Color.parseColor("#58595b"));
                    }

                    ((TimeViewHolder)holder).txt_time.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            HomeResponse homeResponse= new Gson().fromJson(home_response,
                                    HomeResponse.class);
//                            EventBus.getDefault().post(new DateTimeEvent());
                            // Updating old as well as new positions
                            String date = modal.getDate() + " " + modal.getTime();
                            DateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm");               //parsing this date into date
                            final DateFormat send_format = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss");           //the format i need
                            Date so_the_date = new Date();
                            try {

                                so_the_date = date_format.parse(date);
                                Calendar cal = Calendar.getInstance();
                                cal.setTime(so_the_date);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (homeResponse!=null &&
                                    homeResponse.getData()!=null &&
                                    homeResponse.getData().getDepartments()!=null &&
                                    homeResponse.getData().getDepartments().size()>0) {


                                logTimeSelctedEvent();
                                logTimeSelctedFireBaseEvent();
                                notifyItemChanged(selected_position);
                                selected_position = position;
                                notifyItemChanged(selected_position);

//                            int day_of_week=0;          // save the user selected date in this integer variable



                                UserCart saved_cart = null;
                                DB snappyDB = null;
                                try {
                                    snappyDB = DBFactory.open(activity);
                                    if (snappyDB.exists(AppConstant.USER_CART_DB))
                                        saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
                                    snappyDB.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                if (saved_cart == null && DepartmentId.length() == 0) {

                                    Intent intent = new Intent(activity, SalonServicesActivity.class);
                                    intent.putExtra("selected_date_time", send_format.format(so_the_date));
                                    intent.putExtra("home_response", home_response);
                                    activity.startActivity(intent);
                                } else if (DepartmentId.trim().length() > 0) {
                                    Intent intent = new Intent(activity, ServiceSpecificActivity.class);
                                    intent.putExtra("department_id", DepartmentId);
                                    intent.putExtra("department_name", departmentName);
                                    intent.putExtra("index", index);
                                    intent.putExtra("gender", gender);
                                    intent.putExtra("membership", home_response);
                                    homeResponse = new Gson().fromJson(home_response,
                                            HomeResponse.class);
                                    UserCart userCart = new UserCart();
                                    userCart.setCartType(AppConstant.SERVICE_TYPE);
                                    userCart.setParlorId(homeResponse.getData().getParlorId());
                                    userCart.setParlorName(homeResponse.getData().getName());
                                    userCart.setParlorType(homeResponse.getData().getParlorType());
                                    userCart.setGender(homeResponse.getData().getGender());
                                    userCart.setRating(homeResponse.getData().getRating());
                                    userCart.setOpeningTime(homeResponse.getData().getOpeningTime());
                                    userCart.setClosingTime(homeResponse.getData().getClosingTime());
                                    userCart.setAddress1(homeResponse.getData().getAddress1());
                                    userCart.setAddress2(homeResponse.getData().getAddress2());
                                    userCart.setDate_time(send_format.format(so_the_date));
                                    intent.putExtra("user_cart", new Gson().toJson(userCart, UserCart.class));
                                    activity.startActivity(intent);

                                } else {

                                    if (saved_cart.getServicesList() != null &&
                                            saved_cart.getServicesList().size() > 0) {
                                        boolean hasFreeService = false;
                                        for (int i = 0; i < saved_cart.getServicesList().size(); i++) {
                                            if (saved_cart.getServicesList().get(i).isFree_service()) {

                                                hasFreeService = true;
                                                break;
                                            }

                                        }

                                        if (hasFreeService) {

                                            Intent intent = new Intent(activity, SalonServicesActivity.class);
                                            intent.putExtra("selected_date_time", send_format.format(so_the_date));
                                            intent.putExtra("home_response", home_response);
                                            activity.startActivity(intent);

                                        } else {


                                            final Date date_ = so_the_date;
                                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                                            builder.setMessage("Do You Want To Add More Services In Your Cart?")
                                                    .setPositiveButton("Add More", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {


                                                            logAddMoreButtonClickedEvent();
                                                            logAddMoreButtonClickedFireBaseEvent();
                                                            Intent intent = new Intent(activity, SalonServicesActivity.class);
                                                            intent.putExtra("selected_date_time", send_format.format(date_));
                                                            intent.putExtra("home_response", home_response);
                                                            activity.startActivity(intent);
                                                            dialog.dismiss();

                                                        }
                                                    })
                                                    .setNegativeButton("Checkout ", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            logCheckoutButtonClickedEvent();
                                                            logCheckoutButtonClickedFireBaseEvent();
                                                            Intent intent = new Intent(activity, BookingSummaryActivity.class);

                                                            HomeResponse homeResponse = new Gson().fromJson(home_response,
                                                                    HomeResponse.class);
                                                            UserCart userCart = new UserCart();
                                                            userCart.setCartType(AppConstant.SERVICE_TYPE);
                                                            userCart.setParlorId(homeResponse.getData().getParlorId());
                                                            userCart.setParlorName(homeResponse.getData().getName());
                                                            userCart.setParlorType(homeResponse.getData().getParlorType());
                                                            userCart.setGender(homeResponse.getData().getGender());
                                                            userCart.setRating(homeResponse.getData().getRating());
                                                            userCart.setOpeningTime(homeResponse.getData().getOpeningTime());
                                                            userCart.setClosingTime(homeResponse.getData().getClosingTime());
                                                            userCart.setAddress1(homeResponse.getData().getAddress1());
                                                            userCart.setAddress2(homeResponse.getData().getAddress2());
                                                            userCart.setDate_time(send_format.format(date_));
                                                            intent.putExtra("user_cart", new Gson().toJson(userCart, UserCart.class));
                                                            intent.putExtra(BookingSummaryActivity.HOME_RESPONSE, home_response);
                                                            activity.startActivity(intent);
                                                        }
                                                    }).show();


                                        }

                                    } else {

                                        Intent intent = new Intent(activity, SalonServicesActivity.class);
                                        intent.putExtra("selected_date_time", send_format.format(so_the_date));
                                        intent.putExtra("home_response", home_response);
                                        activity.startActivity(intent);

                                    }

                                }


                            }else  if (isReorder) {


//                                Intent intent= new Intent(activity, BillSummaryActivity.class);
//                                intent.putExtra("apptId", apptId);
//                                intent.putExtra("time",send_format.format(so_the_date)+""+" GMT+0530 (India Standard Time)");
//                                activity.startActivity(intent);

//                                apptId, send_format.format(so_the_date)
                                fetchData(apptId, send_format.format(so_the_date) + " GMT+0530 (India Standard Time)");

                            } else if (isReschedule) {

                                reschedule(apptId, send_format.format(so_the_date));

                            }

                            else {
                                Toast.makeText(activity,"Fetching Data Wait for 5 Seconds.",Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(activity, FetchDepartmentsService.class);
                                i.putExtra("parlorId",homeResponse.getData().getParlorId() );
                                activity.startService(i);

                            }
                        }
                    });

                    break;
            }
        }
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logTimeSelctedEvent () {
        Log.e("time","fine");
        logger.logEvent(AppConstant.TimeSelcted);
    }

    public void logTimeSelctedFireBaseEvent () {
        Log.e("timeFireBse","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.TimeSelcted,bundle);
    }

    public void logAddMoreButtonClickedEvent () {
        Log.e("addmore","fine");
        logger.logEvent(AppConstant.AddMoreButtonClicked);
    }
    public void logAddMoreButtonClickedFireBaseEvent () {
        Log.e("addmorefirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.AddMoreButtonClicked,bundle);
    }
    public void logCheckoutButtonClickedEvent () {
        Log.e("checkout","fine");
        logger.logEvent(AppConstant.CheckoutButtonClicked);
    }
    public void logCheckoutButtonClickedFireBaseEvent () {
        Log.e("checkoutfirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.CheckoutButtonClicked,bundle);
    }


    public void fetchData(String appt_id, final String dateTime){

        final AppointmentDetailPost post = new AppointmentDetailPost();
        post.setUserId(BeuSalonsSharedPrefrence.getUserId());
        post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        post.setAppointmentId(appt_id);

//        appointmentDetailPost.setPaymentMethod(5);

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<AppointmentDetailResponse> call= apiInterface.apptDetail(post);
        call.enqueue(new Callback<AppointmentDetailResponse>() {
            @Override
            public void onResponse(Call<AppointmentDetailResponse> call, Response<AppointmentDetailResponse> response) {


                if(response.isSuccessful()){

                    if(response.body().isSuccess()){

                        Log.i(DateTimeActivity.TAG, "value in the response stuff: "+ response.body().getData().getAppointmentId()+ " latitude: "+
                                response.body().getData().getLatitude()+ "   "+
                                response.body().getData().isOnlinePaymentDiscountAvailable());


                        final AppointmentPost appointmentPost= new AppointmentPost();

                        appointmentPost.setUserId(BeuSalonsSharedPrefrence.getUserId());
                        appointmentPost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
                        appointmentPost.setLatitude(BeuSalonsSharedPrefrence.getLatitude());
                        appointmentPost.setLongitude(BeuSalonsSharedPrefrence.getLongitude());

//                        appointmentPost.setAppointmentId(response.body().getData().getAppointmentId());

                        appointmentPost.setDatetime(dateTime);
                        appointmentPost.setParlorId(response.body().getData().getParlorId());
                        appointmentPost.setMode(1);                 //mode 1 for android, 2 for ios
//                        appointmentPost.setPaymentMethod(5);

                        List<AppointmentServicesPost> servicesPosts= new ArrayList<>();
                        for(int i=0; i<response.body().getData().getServices().size();i++){

                            AppointmentServicesPost services_= new AppointmentServicesPost();


                            services_.setServiceCode(response.body().getData().getServices().get(i).getServiceCode());
                            services_.setCode(response.body().getData().getServices().get(i).getCode());

                            services_.setBrandId(response.body().getData().getServices().get(i).getBrandId());
                            services_.setProductId(response.body().getData().getServices().get(i).getProductId());
                            services_.setTypeIndex(response.body().getData().getServices().get(i).getTypeIndex());
                            services_.setAddition((int)response.body().getData().getServices().get(i).getAdditions());


                            String service_deal_id= response.body().getData().getServices().get(i).getDealId()==null?
                                    response.body().getData().getServices().get(i).getServiceId():
                                    response.body().getData().getServices().get(i).getDealId();
                            services_.setServiceId(service_deal_id);

                            //deal, service, dealprice wagera khatam ab
//                            if(cart.getServicesList().get(i).isFree_service()){
//                                services_.setType(cart.getServicesList().get(i).getType());
//                            }else if(cart.getServicesList().get(i).isRemainingService()){
//                                services_.setType("serviceAvailable");
//                            }else if(cart.getServicesList().get(i).isMyMembershipFreeService()){
//                                services_.setType("membership");
//                            }else if(cart.getServicesList().get(i).isSubscription())
//                                services_.setType("subscription");
//                            else
//                                services_.setType("newPackage");


                            services_.setType(response.body().getData().getServices().get(i).getType());
                            services_.setQuantity(response.body().getData().getServices().get(i).getQuantity());
//                            services_.setFrequencyUsed(cart.getServicesList().get(i).isFree_service());

                            servicesPosts.add(services_);
                        }


                        appointmentPost.setServices(servicesPosts);


                        Intent intent= new Intent(activity, BillSummaryActivity.class);
                        intent.putExtra("appointment_post", new Gson().toJson(appointmentPost));
                        activity.startActivity(intent);

                    }else {
                        //success : false hai toh
                        //// TODO: 3/25/2017 toast dikhao
                        Log.e(DateTimeActivity.TAG, "success fail pe aya hai");
                        activity.finish();
                    }
                }else{
                    //// TODO: 3/8/2017  handle karo isseh
                    Log.e(DateTimeActivity.TAG,"retrofit ke issuccessful fail pe aya hai");
                    activity.finish();
                }




            }


            @Override
            public void onFailure(Call<AppointmentDetailResponse> call, Throwable t) {

                Log.i(DateTimeActivity.TAG, "I'm in onFailure" + t.getMessage()+ "  "+ t.getCause()+ "  "+t.getStackTrace());
                Toast.makeText(activity, "Looks Like The Server Is Taking To Long To Respond\n" +
                        " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
//                if(data_retry<3){
//                    fetchData();
//                    data_retry++;
//                }else{
//                    Toast.makeText(OnlinePaymentActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
//                    finish();
//                }

            }
        });

    }









    public void reschedule(String apptId, String startsAt){

        RescheduleAppointmentPost post= new RescheduleAppointmentPost();
        post.setUserId(BeuSalonsSharedPrefrence.getUserId());
        post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        post.setAppointmentId(apptId);
        post.setStartAts(startsAt);

        Log.i("rescheudle", "value in stuff: "+ post.getUserId()+ "   "+ post.getAppointmentId()+ "  "+ post.getStartAts());

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<RescheduleAppointmentResponse> call = apiInterface.rescheduleAppt(post);
        call.enqueue(new Callback<RescheduleAppointmentResponse>() {
            @Override
            public void onResponse(Call<RescheduleAppointmentResponse> call, Response<RescheduleAppointmentResponse> response) {

                if(response.isSuccessful()){
                    if(response.body().isSuccess()){
                        Log.i("reschedule", " success mai hoon");

                        Toast.makeText(activity, "Appointment Rescheduled", Toast.LENGTH_SHORT).show();
                        EventBus.getDefault().post(new CancelAppointment());
                        activity.finish();
                    }else{

                        Log.i("reschedule", " success false mai hoon");
                        Toast.makeText(activity, "Looks Like The Server Is Taking To Long To Respond\n" +
                                " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
                        activity.finish();
                    }


                }else{
                    Log.i("reschedule", " retrofit false mai hoon");
                    Toast.makeText(activity, "Looks Like The Server Is Taking To Long To Respond\n" +
                            " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
                    activity.finish();
                }


            }

            @Override
            public void onFailure(Call<RescheduleAppointmentResponse> call, Throwable t) {
                Log.i("reschedule", " failure mai hoon");
                Toast.makeText(activity, "Looks Like The Server Is Taking To Long To Respond\n" +
                        " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
                activity.finish();
            }
        });

    }

    @Override
    public int getItemCount() {
        if (details == null)
            return 0;
        return details.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (details != null &&
                details.size()>0) {
            DateTimeModel object = details.get(position);
            if (object != null) {
                return object.getType();
            }
        }
        return 0;
    }


}
