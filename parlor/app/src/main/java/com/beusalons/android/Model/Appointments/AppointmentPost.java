package com.beusalons.android.Model.Appointments;

import java.io.Serializable;
import java.util.List;

/**
 * Created by myMachine on 12/12/2016.
 */

public class AppointmentPost  {

    private String userId;
    private String accessToken;
    private String datetime;
    private String parlorId;
    private int mode;
    private String comment;
    private String buyMembershipId;
    private int useMembershipCredits;
    private List<AppointmentServicesPost> services;

    //bill summary ka stuff hai yeh
    private Boolean useLoyalityPoints;
    private String appointmentId;
    private Integer useFreeThreading;
    private Integer paymentMethod;

    private String latitude;
    private String longitude;
    private String couponCodeId;
    private String couponCode;
    private String subscriptionReferralCode;
    private int buySubscriptionId;
    private boolean useSubscriptionCredits;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }

    public List<AppointmentServicesPost> getServices() {
        return services;
    }

    public void setServices(List<AppointmentServicesPost> services) {
        this.services = services;
    }


    public Boolean getUseLoyalityPoints() {
        return useLoyalityPoints;
    }

    public void setUseLoyalityPoints(Boolean useLoyalityPoints) {
        this.useLoyalityPoints = useLoyalityPoints;
    }

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public int getUseFreeThreading() {
        return useFreeThreading;
    }

    public void setUseFreeThreading(Integer useFreeThreading) {
        this.useFreeThreading = useFreeThreading;
    }

    public Integer getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Integer paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getBuyMembershipId() {
        return buyMembershipId;
    }

    public void setBuyMembershipId(String buyMembershipId) {
        this.buyMembershipId = buyMembershipId;
    }

    public int getUseMembershipCredits() {
        return useMembershipCredits;
    }

    public void setUseMembershipCredits(int useMembershipCredits) {
        this.useMembershipCredits = useMembershipCredits;
    }

    public String getCouponCodeId() {
        return couponCodeId;
    }

    public void setCouponCodeId(String couponCodeId) {
        this.couponCodeId = couponCodeId;
    }

    public int getSubscriptionId() {
        return buySubscriptionId;
    }

    public void setSubscriptionId(int subscriptionId) {
        this.buySubscriptionId = subscriptionId;
    }

    public boolean getUseSubscriptionCredits() {
        return useSubscriptionCredits;
    }

    public void setUseSubscriptionCredits(boolean useSubscriptionCredits) {
        this.useSubscriptionCredits = useSubscriptionCredits;
    }

    public String getSubscriptionReferralCode() {
        return subscriptionReferralCode;
    }

    public void setSubscriptionReferralCode(String subscriptionReferralCode) {
        this.subscriptionReferralCode = subscriptionReferralCode;
    }

    public int getBuySubscriptionId() {
        return buySubscriptionId;
    }

    public void setBuySubscriptionId(int buySubscriptionId) {
        this.buySubscriptionId = buySubscriptionId;
    }

    public boolean isUseSubscriptionCredits() {
        return useSubscriptionCredits;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }
}
