package com.beusalons.android.Event.NewServicesEvent;

import com.beusalons.android.Model.UserCart.UserServices;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 9/20/2017.
 */

public class EditPackageListEvent {

    private String name;
    private boolean isAlter;
    private List<UserServices> list= new ArrayList<>();

    public EditPackageListEvent(List<UserServices> list, String name, boolean isAlter){
        this.name= name;
        this.isAlter= isAlter;
        this.list= list;
    }

    public boolean isAlter() {
        return isAlter;
    }

    public String getName() {
        return name;
    }

    public List<UserServices> getList() {
        return list;
    }
}
