package com.beusalons.android.Model.DealsSalonList;

/**
 * Created by myMachine on 04-Dec-17.
 */

public class ServicePrice {

    private int serviceCode;
    private double price;
    private double menuPrice;

    public int getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(int serviceCode) {
        this.serviceCode = serviceCode;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(double menuPrice) {
        this.menuPrice = menuPrice;
    }
}
