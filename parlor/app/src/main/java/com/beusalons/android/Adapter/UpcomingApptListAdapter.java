package com.beusalons.android.Adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.DateTimeActivity;
import com.beusalons.android.Event.BookingSummaryPromoEvent;
import com.beusalons.android.Event.CancelAppointment;
import com.beusalons.android.Model.Appointments.CancelAppointMentsPost;
import com.beusalons.android.Model.Appointments.CancelAppointmentResponse;
import com.beusalons.android.Model.Appointments.UpcomingData;
import com.beusalons.android.Model.BillSummery.CouponAppliedResponse;
import com.beusalons.android.OnlinePaymentActivity;
import com.beusalons.android.OrderSummaryActivity;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by myMachine on 11/16/2016.
 */

public class UpcomingApptListAdapter extends RecyclerView.Adapter<UpcomingApptListAdapter.UpApptsViewHolder> {

    private List<UpcomingData> details;
    private Activity context;

    public UpcomingApptListAdapter(List<UpcomingData> details, Activity context){
        this.details= details;
        this.context= context;
    }


    public static class UpApptsViewHolder extends RecyclerView.ViewHolder{


        //booked salon details
        private TextView salonName, salonAddress, salonDate, salonTime;
        private Spinner spinner;
        private LinearLayout linear_direction, linear_details;
        private FrameLayout frame_spinner;


        public UpApptsViewHolder(View view) {
            super(view);

            salonName=(TextView)view.findViewById(R.id.txt_upAppt_salonName);
            salonAddress=(TextView)view.findViewById(R.id.txt_upAppt_salonAddress);
            salonDate=(TextView)view.findViewById(R.id.txt_upAppt_salonDate);
            salonTime=(TextView)view.findViewById(R.id.txt_upAppt_salonTime);

            spinner= (Spinner)view.findViewById(R.id.spinner_edit);

            linear_direction= (LinearLayout)view.findViewById(R.id.linear_direction);
            linear_details= (LinearLayout)view.findViewById(R.id.linear_details);
            frame_spinner= (FrameLayout)view.findViewById(R.id.frame_spinner);
        }
    }

    @Override
    public UpApptsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.upcoming_appointments_cardview, parent, false);
        return new UpApptsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final UpApptsViewHolder holder, int position) {

        final UpcomingData data= details.get(position);

        holder.salonName.setText(data.getParlorName());
        holder.salonAddress.setText(data.getParlorAddress());

        holder.salonDate.setText(data.getDate());
        holder.salonTime.setText(data.getTime());

//        if (data.getPaymentMethod()!=null && data.getPaymentMethod()== 5){
//            holder.linear_pay.setVisibility(View.INVISIBLE);
//        }else{
//            holder.linear_pay.setVisibility(View.VISIBLE);
//        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {

                    ((TextView)v.findViewById(android.R.id.text1))
                            .setText("");
                    ((TextView)v.findViewById(android.R.id.text1))
                            .setHint(getItem(getCount())); //"Hint to be displayed"
                }

                return v;
            }

            @Override
            public int getCount() {
                return super.getCount()-1;
                // you dont display last item. It is used as hint.
            }
        };

        adapter.setDropDownViewResource(R.layout.row_spinner_appt);

        adapter.add("Reschedule");
        adapter.add("Cancel");
        adapter.add("Edit"); //This is the text that will be displayed as hint.



        holder.spinner.setAdapter(adapter);
        holder.spinner.setSelection(adapter.getCount());
        holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                //0 position hai reschedule- 1 hai cancel
                Log.i("spinner", "value in position"+ parent.getItemAtPosition(position)+ " "+ position);
                if (position==0){//reschedule ka hai yeh

                    holder.spinner.setSelection(2);
                    Intent intent= new Intent(view.getContext(), DateTimeActivity.class);
                    Bundle bundle= new Bundle();
                    bundle.putString(DateTimeActivity.APPOINTMENT_ID, data.getAppointmentId());
                    bundle.putString(DateTimeActivity.CURRENT_TIME, data.getCurrentTime());  //"2017-11-10T03:45:00.000Z"
                    bundle.putString(DateTimeActivity.OPENING_TIME, data.getOpeningTime());
                    bundle.putString(DateTimeActivity.CLOSING_TIME, data.getClosingTime());
                    bundle.putInt(DateTimeActivity.DAY_CLOSED, 0);
                    intent.putExtras(bundle);
                    view.getContext().startActivity(intent);

                }else if(position==1){//cancellation ka hai yeh

//                    holder.spinner.setSelection(2);

                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setMessage("Are you sure, You want to cancel your appointment?");
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            CancelAppointMentsPost post=new CancelAppointMentsPost();
                            post.setAppointmentId(data.getAppointmentId());
                            post.setUserId(BeuSalonsSharedPrefrence.getUserId());
                            post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());

                            Log.i("cancellation", "value in appt id: "+ post.getAppointmentId());
                            cancelAppointMent(post);

                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();

                   holder.spinner.setSelection(2);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.i("spinner", "nothing selected");
            }
        });

        holder.frame_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.spinner.performClick();
            }
        });

        Log.i("tagged", "value in lat: "+ data.getParlorLatitude());

        holder.linear_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Uri uri= Uri.parse("http://maps.google.com/maps?q="+ data.getParlorLatitude()  +"," +
                        data.getParlorLongitude()+"("+ data.getParlorName() + ")&iwloc=A&hl=es");
                Intent intent= new Intent(Intent.ACTION_VIEW, uri);
                intent.setPackage("com.google.android.apps.maps");
                view.getContext().startActivity(intent);
            }
        });

//        holder.linear_pay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent  intent1= new Intent(v.getContext(), OnlinePaymentActivity.class);
//                intent1.putExtra("apptId",data.getAppointmentId());
//                v.getContext().startActivity(intent1);
//            }
//        });

        holder.linear_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle= new Bundle();
                bundle.putString(OrderSummaryActivity.BOOKED_APPOINTMENT, new Gson().toJson(data));
                Intent intent= new Intent(v.getContext(), OrderSummaryActivity.class);
                intent.putExtras(bundle);
                v.getContext().startActivity(intent);
            }
        });


    }



    private void cancelAppointMent(CancelAppointMentsPost applyPromoRequest){

        Retrofit retrofit1 = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit1.create(ApiInterface.class);
        Call<CancelAppointmentResponse> call= apiInterface.cancelAppointMent(applyPromoRequest);
        call.enqueue(new Callback<CancelAppointmentResponse>() {
            @Override
            public void onResponse(Call<CancelAppointmentResponse> call, Response<CancelAppointmentResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().isSuccess()){

                        Log.i("cancellation", "mai success pe hoon");
                        EventBus.getDefault().post(new CancelAppointment());
                        Toast.makeText(context,response.body().getData(),Toast.LENGTH_LONG).show();
                    }else{
                        Log.i("cancellation", "mai else pe hoon");

                        Toast.makeText(context,"Please Try Again",Toast.LENGTH_LONG).show();
                    }




                }else{

                    Log.i("cancellation", "mai else else pe hoon");
                    Toast.makeText(context,"Please Try Again",Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<CancelAppointmentResponse> call, Throwable t) {
                Log.i("cancellation", "mai failure pe hoon");
                Toast.makeText(context,"Please Try Again",Toast.LENGTH_LONG).show();
            }
        });

    }




    @Override
    public int getItemCount() {
        return details.size();
    }
}
