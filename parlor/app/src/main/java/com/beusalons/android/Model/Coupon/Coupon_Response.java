package com.beusalons.android.Model.Coupon;

/**
 * Created by Ajay on 12/5/2017.
 */

public class Coupon_Response {
    private Boolean success;
    private CouponData data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public CouponData getData() {
        return data;
    }

    public void setData(CouponData data) {
        this.data = data;
    }
}
