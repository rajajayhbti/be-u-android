package com.beusalons.android.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.beusalons.android.Fragment.CouponFragment;
import com.beusalons.android.Fragment.DealsFragment;
import com.beusalons.android.Fragment.HomeFragment;
import com.beusalons.android.Fragment.HomeFragmentNew;
import com.beusalons.android.Fragment.NewHomePageFragment;
import com.beusalons.android.Fragment.UserLoyalty;
import com.beusalons.android.Fragment.UserProfile;

/**
 * Created by Robbin Singh on 11/11/2016.
 */

public class HomePageAdapter extends FragmentPagerAdapter  {

    Context context;

    public HomePageAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context= context;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = null;
            if(i == 0){
                fragment = new NewHomePageFragment();
            }
            if(i == 1){
                fragment = new DealsFragment();
            }
            if(i == 2){
                fragment = new UserLoyalty();
            }
            if(i == 3){
               // fragment = new UserCartFragment();
                 fragment = new CouponFragment();

            }
            if(i == 4){
                fragment = new UserProfile();
            }
        return fragment;
    }

}
