package com.beusalons.android;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.beusalons.android.Adapter.SelectEmployeeAdapter;
import com.beusalons.android.Model.selectArtist.SelectEmployeePost;
import com.beusalons.android.Model.selectArtist.SelectEmployeeResponse;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Ashish Sharma on 12/29/2017.
 */

public class SelectStylistActivity extends AppCompatActivity {
@BindView(R.id.rec_employee)
    RecyclerView rec_employee;
    private Retrofit retrofit;
    String appointmentId;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_stylist);
        ButterKnife.bind(this);
        setToolBar();
        Bundle bundle= getIntent().getExtras();
        if(bundle!=null){
            appointmentId=bundle.getString("appointment_id");
        }
        getArtists( appointmentId);

        LinearLayoutManager manager=new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        rec_employee.setLayoutManager(manager);
    }


    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }
    }

    private void getArtists(String appointmentId){
        retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        final SelectEmployeePost request=new SelectEmployeePost();
        request.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        request.setUserId(BeuSalonsSharedPrefrence.getUserId());
        request.setAppointmentId(appointmentId);
        Call<SelectEmployeeResponse> call=apiInterface.getArtist(request);
        call.enqueue(new Callback<SelectEmployeeResponse>() {
            @Override
            public void onResponse(Call<SelectEmployeeResponse> call, Response<SelectEmployeeResponse> response) {
                if (response.body().isSuccess()){
                    SelectEmployeeAdapter adapter=new SelectEmployeeAdapter(SelectStylistActivity.this,response.body().getData().getServices());
                    rec_employee.setAdapter(adapter);
                    Log.e("dfdf",response.body().getData().getServices().size()+"");
                }


            }

            @Override
            public void onFailure(Call<SelectEmployeeResponse> call, Throwable t) {

            }
        });
    }


}
