package com.beusalons.android.Retrofit;

import com.beusalons.android.Model.ServiceCategoryModel;
import com.beusalons.android.Model.StatusModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robbin Singh on 11/11/2016.
 */
public class ServicesResponseModel  implements Serializable{


    private boolean success;
    private String message;

    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {
        @SerializedName("services")
        @Expose
        private List<ServiceCategoryModel> categories = new ArrayList<ServiceCategoryModel>();

        public List<ServiceCategoryModel> getCategories() {
            return categories;
        }

        public void setCategories(List<ServiceCategoryModel> categories) {
            this.categories = categories;
        }
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
