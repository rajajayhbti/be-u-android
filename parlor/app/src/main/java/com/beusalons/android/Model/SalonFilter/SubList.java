package com.beusalons.android.Model.SalonFilter;

/**
 * Created by myMachine on 22-Feb-18.
 */

public class SubList{

    private String name;
    private boolean isCheck;

    public SubList(String name, boolean isCheck){
        this.name= name;
        this.isCheck= isCheck;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}