package com.beusalons.android.Model.SubscriptionHistory;

import java.util.List;

/**
 * Created by Ajay on 2/27/2018.
 */

public class Month {

    private String month;
    private List<Appointment> appointments = null;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }
}
