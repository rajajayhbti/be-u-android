package com.beusalons.android.Model.Loyalty;

/**
 * Created by myMachine on 8/18/2017.
 */

public class UpgradePost {

    private String userId;
    private String accessToken;
    private String id;                  //bahar ka id
    private int upgradeId;              //upgrade id

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getUpgradeId() {
        return upgradeId;
    }

    public void setUpgradeId(int upgradeId) {
        this.upgradeId = upgradeId;
    }
}
