package com.beusalons.android.Model.AllDeals;

/**
 * Created by myMachine on 4/28/2017.
 */

public class DealType {

    public Integer price;
    public Object loyalityPoints;
    public Object frequencyFree;
    public Object frequencyRequired;
    public String name;

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Object getLoyalityPoints() {
        return loyalityPoints;
    }

    public void setLoyalityPoints(Object loyalityPoints) {
        this.loyalityPoints = loyalityPoints;
    }

    public Object getFrequencyFree() {
        return frequencyFree;
    }

    public void setFrequencyFree(Object frequencyFree) {
        this.frequencyFree = frequencyFree;
    }

    public Object getFrequencyRequired() {
        return frequencyRequired;
    }

    public void setFrequencyRequired(Object frequencyRequired) {
        this.frequencyRequired = frequencyRequired;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
