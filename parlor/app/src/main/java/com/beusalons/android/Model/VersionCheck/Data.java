package com.beusalons.android.Model.VersionCheck;

/**
 * Created by myMachine on 5/5/2017.
 */

public class Data{

    private boolean forcedUpdate;
    private boolean recommendedUpdate;


    public boolean isForcedUpdate() {
        return forcedUpdate;
    }

    public void setForcedUpdate(boolean forcedUpdate) {
        this.forcedUpdate = forcedUpdate;
    }

    public boolean isRecommendedUpdate() {
        return recommendedUpdate;
    }

    public void setRecommendedUpdate(boolean recommendedUpdate) {
        this.recommendedUpdate = recommendedUpdate;
    }


}