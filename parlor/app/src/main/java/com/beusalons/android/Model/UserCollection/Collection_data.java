package com.beusalons.android.Model.UserCollection;

import java.util.List;

/**
 * Created by Ajay on 2/8/2018.
 */

public class Collection_data {
    private String name;
    private List<Colllection_project> projects = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Colllection_project> getProjects() {
        return projects;
    }

    public void setProjects(List<Colllection_project> projects) {
        this.projects = projects;
    }
}
