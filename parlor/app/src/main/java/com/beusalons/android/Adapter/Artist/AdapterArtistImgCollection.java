package com.beusalons.android.Adapter.Artist;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.beusalons.android.Model.Profile.Project_;
import com.beusalons.android.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ajay on 2/21/2018.
 */

public class AdapterArtistImgCollection extends RecyclerView.Adapter<AdapterArtistImgCollection.MyViewHolder>{

    Activity activity;
    private List<Project_> project_list;
    public AdapterArtistImgCollection(Activity activity,List<Project_> list){
        this.activity=activity;
        this.project_list=list;
    }
    @Override
    public AdapterArtistImgCollection.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout linear_= (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_collection_img, parent,false);
        return new AdapterArtistImgCollection.MyViewHolder(linear_);
    }

    @Override
    public void onBindViewHolder(AdapterArtistImgCollection.MyViewHolder holder, int position) {

        final int pos=position;
        try{
            Glide.with(activity).load(project_list.get(pos).getImages().get(0))
                    .apply(new RequestOptions().placeholder(R.drawable.about_us_3))
                    .into(holder.img_collection);


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return project_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_collection)
        ImageView img_collection;
        @BindView(R.id.linear_project)
        LinearLayout linear_project;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
