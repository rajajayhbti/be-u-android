package com.beusalons.android.Model;

/**
 * Created by myMachine on 5/4/2017.
 */

public class RescheduleAppointmentResponse {

    private boolean success;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
