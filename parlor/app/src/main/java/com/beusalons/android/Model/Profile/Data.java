package com.beusalons.android.Model.Profile;

import com.beusalons.android.Model.ArtistProfile.ArtistFollowed;

import java.util.List;


/**
 * Created by myMachine on 10/27/2017.
 */

public class Data {





    private String id;
    private String userId;
    private List<Object> likedPosts = null;
    private Integer portfolioLikes;
    private Integer portfolioRating;
    private List<String> portfolioPosition;
    private String portfolioProfile;
    private Integer portfolioPosts;
    private String gender;
    private String lastName;
    private String profilePic;
    private String phoneNumber;
    private String emailId;
    private String firstName;
    private String portfolioDescription;
//    private List<Project> projects = null;
//    private List<CollectionWisePost> collectionWisePost = null;
    private String positionName;
    private List<ArtistFollowed> artistFollowed = null;
    private List<CollectionFollowed> collectionFollowed = null;
    private String address;



    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<Object> getLikedPosts() {
        return likedPosts;
    }

    public void setLikedPosts(List<Object> likedPosts) {
        this.likedPosts = likedPosts;
    }

    public Integer getPortfolioLikes() {
        return portfolioLikes;
    }

    public void setPortfolioLikes(Integer portfolioLikes) {
        this.portfolioLikes = portfolioLikes;
    }

    public Integer getPortfolioRating() {
        return portfolioRating;
    }

    public void setPortfolioRating(Integer portfolioRating) {
        this.portfolioRating = portfolioRating;
    }

    public List<String> getPortfolioPosition() {
        return portfolioPosition;
    }

    public void setPortfolioPosition(List<String> portfolioPosition) {
        this.portfolioPosition = portfolioPosition;
    }

    public String getPortfolioProfile() {
        return portfolioProfile;
    }

    public void setPortfolioProfile(String portfolioProfile) {
        this.portfolioProfile = portfolioProfile;
    }

    public Integer getPortfolioPosts() {
        return portfolioPosts;
    }

    public void setPortfolioPosts(Integer portfolioPosts) {
        this.portfolioPosts = portfolioPosts;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPortfolioDescription() {
        return portfolioDescription;
    }

    public void setPortfolioDescription(String portfolioDescription) {
        this.portfolioDescription = portfolioDescription;
    }

//    public List<Project> getProjects() {
//        return projects;
//    }
//
//    public void setProjects(List<Project> projects) {
//        this.projects = projects;
//    }
//
//    public List<CollectionWisePost> getCollectionWisePost() {
//        return collectionWisePost;
//    }
//
//    public void setCollectionWisePost(List<CollectionWisePost> collectionWisePost) {
//        this.collectionWisePost = collectionWisePost;
//    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public List<ArtistFollowed> getArtistFollowed() {
        return artistFollowed;
    }

    public void setArtistFollowed(List<ArtistFollowed> artistFollowed) {
        this.artistFollowed = artistFollowed;
    }

    public List<CollectionFollowed> getCollectionFollowed() {
        return collectionFollowed;
    }

    public void setCollectionFollowed(List<CollectionFollowed> collectionFollowed) {
        this.collectionFollowed = collectionFollowed;
    }


    //    public String getPortfolioDescription() {
//        return portfolioDescription;
//    }
//
//    public void setPortfolioDescription(String portfolioDescription) {
//        this.portfolioDescription = portfolioDescription;
//    }
//
//    public String get_id() {
//        return _id;
//    }
//
//    public void set_id(String _id) {
//        this._id = _id;
//    }
//
//    public String getFirstName() {
//        return firstName;
//    }
//
//    public void setFirstName(String firstName) {
//        this.firstName = firstName;
//    }
//
//    public String getPhoneNumber() {
//        return phoneNumber;
//    }
//
//    public void setPhoneNumber(String phoneNumber) {
//        this.phoneNumber = phoneNumber;
//    }
//
//    public List<Object> getLikedPosts() {
//        return likedPosts;
//    }
//
//    public void setLikedPosts(List<Object> likedPosts) {
//        this.likedPosts = likedPosts;
//    }
//
//    public List<Object> getPortfolioCollection() {
//        return portfolioCollection;
//    }
//
//    public void setPortfolioCollection(List<Object> portfolioCollection) {
//        this.portfolioCollection = portfolioCollection;
//    }
//
//    public int getPortfolioLikes() {
//        return portfolioLikes;
//    }
//
//    public void setPortfolioLikes(int portfolioLikes) {
//        this.portfolioLikes = portfolioLikes;
//    }
//
//    public double getPortfolioRating() {
//        return portfolioRating;
//    }
//
//    public void setPortfolioRating(double portfolioRating) {
//        this.portfolioRating = portfolioRating;
//    }
//
//    public String getPortfolioPosition() {
//        return portfolioPosition;
//    }
//
//    public void setPortfolioPosition(String portfolioPosition) {
//        this.portfolioPosition = portfolioPosition;
//    }
//
//    public String getPortfolioProfile() {
//        return portfolioProfile;
//    }
//
//    public void setPortfolioProfile(String portfolioProfile) {
//        this.portfolioProfile = portfolioProfile;
//    }
//
//    public int getPortfolioPosts() {
//        return portfolioPosts;
//    }
//
//    public void setPortfolioPosts(int portfolioPosts) {
//        this.portfolioPosts = portfolioPosts;
//    }
//
//    public String getGender() {
//        return gender;
//    }
//
//    public void setGender(String gender) {
//        this.gender = gender;
//    }
//
//    public String getLastName() {
//        return lastName;
//    }
//
//    public void setLastName(String lastName) {
//        this.lastName = lastName;
//    }
//
//    public String getProfilePic() {
//        return profilePic;
//    }
//
//    public void setProfilePic(String profilePic) {
//        this.profilePic = profilePic;
//    }
//
//    public String getEmailId() {
//        return emailId;
//    }
//
//    public void setEmailId(String emailId) {
//        this.emailId = emailId;
//    }
//
//    public List<Project> getProjects() {
//        return projects;
//    }
//
//    public void setProjects(List<Project> projects) {
//        this.projects = projects;
//    }
//
//    public String getDescription() {
//        return description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }
//
//    public String getLocation() {
//        return location;
//    }
//
//    public void setLocation(String location) {
//        this.location = location;
//    }
}
