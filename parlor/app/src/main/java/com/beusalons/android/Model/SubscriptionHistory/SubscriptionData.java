package com.beusalons.android.Model.SubscriptionHistory;

import java.util.List;

/**
 * Created by Ajay on 2/27/2018.
 */

public class SubscriptionData {




    private String _id;
    private String annualBalance;
    private String clientName;
    private List<Month> months = null;
    private String subscriptionType;


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getAnnualBalance() {
        return annualBalance;
    }

    public void setAnnualBalance(String annualBalance) {
        this.annualBalance = annualBalance;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public List<Month> getMonths() {
        return months;
    }

    public void setMonths(List<Month> months) {
        this.months = months;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }
}
