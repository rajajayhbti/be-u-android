package com.beusalons.android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Adapter.NotificationAdapter;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.AppointmentDetail.AppointmentDetailPost;
import com.beusalons.android.Model.AppointmentDetail.AppointmentDetailResponse;
import com.beusalons.android.Model.Notifications.NotiFicationResponse;
import com.beusalons.android.Model.Notifications.NotificationDetail;
import com.beusalons.android.Model.Notifications.NotificationPost;
import com.beusalons.android.Model.Notifications.Notifications;
import com.beusalons.android.Model.Reviews.WriteReviewPost;
import com.beusalons.android.Model.Reviews.WriteReviewResponse;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Task.SaveNotificationTask;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NotificationsActivity extends AppCompatActivity {
    @BindView(R.id.rec_notifications)
    RecyclerView recNotifications;
    private int retry;
    private  NotificationAdapter adapter;
    List<NotificationDetail> list_detail= new ArrayList<>();
    String userId;
    String accessToken;
    private int retry_submit=0;

    private String parlorId;

    private View relative, spinner;
    private TextView txt_no_noti;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        ButterKnife.bind(this);
        setToolBar();

        LinearLayoutManager layoutManager=new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        relative= findViewById(R.id.relative);
        spinner= findViewById(R.id.spinner);
        txt_no_noti= (TextView)findViewById(R.id.txt_notifications);
        txt_no_noti.setVisibility(View.GONE);


        recNotifications.setLayoutManager(layoutManager);
        adapter=new NotificationAdapter(this,list_detail,clickListener);
        recNotifications.setAdapter(adapter);
        userId= BeuSalonsSharedPrefrence.getUserId();
        accessToken=  BeuSalonsSharedPrefrence.getAccessToken();
        fetchData();
    }


    public View.OnClickListener clickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final NotificationDetail notiFicationData=(NotificationDetail) v.getTag();
            // Toast.makeText(NotificationsActivity.this,"test",Toast.LENGTH_LONG).show();
            notiFicationData.setSeen(true);
            new Thread(new SaveNotificationTask(NotificationsActivity.this, notiFicationData)).start();

            if (notiFicationData.getAction().equalsIgnoreCase("offer")){
                //  new Thread(new SaveNotificationTask(NotificationsActivity.this, notiFicationData)).start();
                Intent intent=new Intent(NotificationsActivity.this,MainActivity.class);
                intent.putExtra("type",1);
                startActivity(intent);
                finish();

            }else if(notiFicationData.getAction().equalsIgnoreCase("review")){


                Bundle bundle_= new Bundle();
                bundle_.putString(CustomerReviewActivity.PARLOR_NAME, "");
                bundle_.putString(CustomerReviewActivity.PARLOR_ID, "");
                bundle_.putString(CustomerReviewActivity.APPOINTMENT_ID, notiFicationData.getAppointmentId());
                Intent intent= new Intent(NotificationsActivity.this, CustomerReviewActivity.class);
                intent.putExtras(bundle_);
                startActivity(intent);

            } else if(notiFicationData.getAction().equalsIgnoreCase("profile")){

                onBackPressed();

            } else {

                Intent intent=new Intent(NotificationsActivity.this,MainActivity.class);
                intent.putExtra("type",3);
                startActivity(intent);
                finish();
            }
        }
    };



    private void fetchData() {

        spinner.setVisibility(View.VISIBLE);
        relative.setVisibility(View.GONE);

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface service = retrofit.create(ApiInterface.class);

        NotificationPost post = new NotificationPost();
        post.setUserId(userId);
        post.setAccessToken(accessToken);

        Call<NotiFicationResponse> call = service.getNotifications(post);

        call.enqueue(new Callback<NotiFicationResponse>() {

            @Override
            public void onResponse(Call<NotiFicationResponse> call, Response<NotiFicationResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().getSuccess()){

                        list_detail.clear();

                        list_detail= response.body().getData();
                        for(int j=0;j<list_detail.size();j++){

                            list_detail.get(j).setSeen(true);
                        }

                        Notifications notifications= new Notifications();



                        DB snappyDB = null;
                        try {
                            snappyDB = DBFactory.open(getApplicationContext());

                            if(notifications!=null)
                                notifications.getList().clear();
                            notifications.setList(list_detail);

                            snappyDB.put(AppConstant.NOTIFICATION_DB, notifications);
                            snappyDB.close();
                        } catch (SnappydbException e) {
                            e.printStackTrace();
                        }
                        adapter.setData(list_detail) ;

                        if (list_detail.size() == 0) {
                            txt_no_noti.setVisibility(View.VISIBLE);
//                            Toast.makeText(NotificationsActivity.this, "No record found", Toast.LENGTH_SHORT).show();
                        }else{
                            txt_no_noti.setVisibility(View.GONE);
                        }

                        relative.setVisibility(View.VISIBLE);
                        spinner.setVisibility(View.GONE);

                    }else{

//                        Toast.makeText(NotificationsActivity.this, "We are facing a technical issue", Toast.LENGTH_SHORT).show();
//                        finish();
                    }



                } else{
//                    Toast.makeText(NotificationsActivity.this, "We are facing a technical issue", Toast.LENGTH_SHORT).show();
//                    finish();

                }


            }

            @Override
            public void onFailure(Call<NotiFicationResponse> call, Throwable t) {
//                Log.e("asddas", t.getMessage());

                retry++;
                if(retry < 2){

                    fetchData();
                }else{
//                    Toast.makeText(NotificationsActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
//                    finish();

//                    retry = 0;
//                    ActivityHelper.hideProgress(progressWheel);
//                    progress.dismiss();
//                    retryButton.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void setToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(getResources().getString(R.string.notifications));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
