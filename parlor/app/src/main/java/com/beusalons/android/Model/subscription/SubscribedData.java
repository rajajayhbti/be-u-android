package com.beusalons.android.Model.subscription;

/**
 * Created by Ashish Sharma on 1/25/2018.
 */

public class SubscribedData {
    private String name;
    private String subscriptionType;
    private String validFrom;
    private String validTill;
    private String currentMonth;
    private String profilePic;
    private long annualBalance;
    private long monthlyBalance;
    String referMessage;
    private String subscriptionCount;
    private String heading;

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getSubscriptionCount() {
        return subscriptionCount;
    }

    public void setSubscriptionCount(String subscriptionCount) {
        this.subscriptionCount = subscriptionCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTill() {
        return validTill;
    }

    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }

    public String getCurrentMonth() {
        return currentMonth;
    }

    public void setCurrentMonth(String currentMonth) {
        this.currentMonth = currentMonth;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public long getAnnualBalance() {
        return annualBalance;
    }

    public void setAnnualBalance(long annualBalance) {
        this.annualBalance = annualBalance;
    }

    public long getMonthlyBalance() {
        return monthlyBalance;
    }

    public void setMonthlyBalance(long monthlyBalance) {
        this.monthlyBalance = monthlyBalance;
    }

    public String getReferMessage() {
        return referMessage;
    }

    public void setReferMessage(String referMessage) {
        this.referMessage = referMessage;
    }
}
