package com.beusalons.android.Model.Share;

/**
 * Created by Ashish Sharma on 12/19/2017.
 */

public class ShareDealService {


    private int dealId;
    private int index;

    public ShareDealService(int dealId, int index) {
        this.dealId = dealId;
        this.index = index;
    }


    public int getDealId() {
        return dealId;
    }

    public int getIndex() {
        return index;
    }


}
