package com.beusalons.android.Model.HomeFragmentModel;

/**
 * Created by Ajay on 3/21/2017.
 */

public class PackageDealType {


    private Integer price;

    private Object loyalityPoints;

    private Object frequencyFree;

    private Object frequencyRequired;

    private String name;

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Object getLoyalityPoints() {
        return loyalityPoints;
    }

    public void setLoyalityPoints(Object loyalityPoints) {
        this.loyalityPoints = loyalityPoints;
    }

    public Object getFrequencyFree() {
        return frequencyFree;
    }

    public void setFrequencyFree(Object frequencyFree) {
        this.frequencyFree = frequencyFree;
    }

    public Object getFrequencyRequired() {
        return frequencyRequired;
    }

    public void setFrequencyRequired(Object frequencyRequired) {
        this.frequencyRequired = frequencyRequired;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
