package com.beusalons.android.Adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.BookingSummaryActivity;
import com.beusalons.android.Event.MembershipEvent.Event;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.MemberShipCardAcitvity;
import com.beusalons.android.Model.MemberShip.Membership;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.R;
import com.beusalons.android.Task.UserCartTask;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.bumptech.glide.Glide;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.snappydb.DB;
import com.snappydb.DBFactory;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ajay on 5/22/2017.
 */

public class MemberShipAdapter extends RecyclerView.Adapter<MemberShipAdapter.MyViewHolder> {

    private Activity myActivity;
    private List<Membership> list;              //oh yeah!
    private AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;

    public MemberShipAdapter(Activity activity, List<Membership> list){

        this.myActivity=activity;
        this.list=list;
        logger = AppEventsLogger.newLogger(activity);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
    }

    public void updateList(List<Membership> list){

        this.list= list;
        notifyDataSetChanged();
    }


    @Override
    public MemberShipAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(myActivity).inflate(R.layout.row_membership,parent,false);
        return new MemberShipAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final Membership membership= list.get(position);

//        String strTxtView5 = " Save <font color=#d2232a><b>"+membership.getDealPercentage()+"%</b></font> on Deal Menu";
//        String strTxtView20 = " Save <font color=#d2232a><b>"+membership.getNormalPercentage()+"%</b></font> on Normal Menu";
//        String strMemberShip="<font color=#d2232a><b>"+membership.getTitle()+"</b></font>";
        final String nodata="Terms & Conditions <br/> Validity:- 1year or till amount lasts&#8226;five registered family members can avail this discount benifits &#8226;" +
                " Client can recharge this card with same amount &#8226; No discount valid in case service value goes beyond the available value balance &#8226;" +
                " This value in membership card can be redeemed across all Be U outlets &#8226; Valid for all 7 days in week &#8226;" +
                " All memberships needs to be registered with the Be U Salon by mobile number &#8226; Can'nt be clubbed with any other offer/discount &#8226; 100% upfront payment " +
                "&#8226; No refund/unused card value can not be exchanges for cash or reuse.";

//        holder.txtViewSave5.setText(fromHtml(strTxtView5));
//        holder.txtViewSave20.setText(fromHtml(strTxtView20));
//        holder.txtViewMemberShip.setText(fromHtml(strMemberShip));
//        holder.txtViewHeaderOne.setText(list.get(position).getDetails().get(0).getHead());
//        holder.txtViewHeaderTwo.setText(membership.getDetails().get(1).getHead());
//        holder.txt_line_1.setText(membership.getLine1());
//        holder.txt_line_2.setText(membership.getLine2());

        holder.txt_title.setText(fromHtml(membership.getTitle()));
        holder.txt_line_1.setText(fromHtml(membership.getLine1()));
        holder.txt_line_2.setText(fromHtml(membership.getLine2()));
        holder.txt_line_3.setText(fromHtml(membership.getLine3()));
        holder.txt_member.setText("*Applicable To "+membership.getNoOfMembersAllowed()+" Registered Family Wallet Member");

        if(membership.getValidity()!=null &&
                membership.getValidity().equalsIgnoreCase("1")){
            holder.txt_validity.setText("*Validity: "+membership.getValidity() +" Month");
        }else
            holder.txt_validity.setText("*Validity: "+membership.getValidity()+ " Months");


        holder.txt_count_1.setText(fromHtml(membership.getCountDown1()));
        holder.txt_count_2.setText(fromHtml(membership.getCountDown2()));
        if(position==0)
            holder.img_count.setBackgroundResource(R.drawable.ic_family_yellow);
        else if(position==1)
            holder.img_count.setBackgroundResource(R.drawable.ic_family_red);
        else if(position==2)
            holder.img_count.setBackgroundResource(R.drawable.ic_family_green);

        try{

            Glide.with(myActivity).load(membership.getCardImage())
                    .into(holder.imgViewMemberShipCard);
        }catch (Exception e){
            e.printStackTrace();
        }
//        for (int i=0;i<membership.getDetails().get(1).getValues().size();i++){
//            View view = myActivity.getLayoutInflater().inflate(R.layout.membership_inflate_row, null);
//            TextView title = (TextView) view.findViewById(R.id.txtView_title);
//            TextView value=(TextView)view.findViewById(R.id.txtView_value);
//            holder.linearLayout_inflate.removeView(view);
//
//            title.setText(membership.getDetails().get(1).getValues().get(i).getTitle());
//            value.setText(membership.getDetails().get(1).getValues().get(i).getValue());
//            holder.linearLayout_inflate.addView(view);
//        }
//
//        for (int i=0;i<membership.getDetails().get(0).getValues().size();i++){
//            View view = myActivity.getLayoutInflater().inflate(R.layout.membership_inflate_row, null);
//            TextView title = (TextView) view.findViewById(R.id.txtView_title);
//            TextView value=(TextView)view.findViewById(R.id.txtView_value);
//            holder.linearLayout_inflate_one.removeView(view);
//            title.setText(membership.getDetails().get(0).getValues().get(i).getTitle());
//            value.setText(membership.getDetails().get(0).getValues().get(i).getValue());
//            holder.linearLayout_inflate_one.addView(view);
//        }
//


//        if (!membership.isShowDetails()){
//
//            holder. LinearLayoutMembershipCard.setVisibility(View.GONE);
//            holder.linearLayout_color.setBackgroundDrawable(myActivity.getResources().getDrawable(R.drawable.linear_membership));
//            holder. cardViewMembership.setBackgroundColor(ContextCompat.getColor(myActivity, R.color.white));
//        }
//
        holder.addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            /*    if(BeuSalonsSharedPrefrence.getMembershipPoints()>0){

                    AlertDialog.Builder builder = new AlertDialog.Builder(myActivity);
                    builder.setMessage("You cannot buy more Membership until you utilize your existing Membership points("+
                                    BeuSalonsSharedPrefrence.getMembershipPoints()+").");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    }).show();
                }else{*/
                logMembershipBuyEvent(membership.getTitle());
                logMembershipBuyFireBaseEvent(membership.getTitle());
                addMembership(membership);
                //   }

            }
        });
//
//
//        holder. txtViewShowDetails.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (membership.isShowDetails()){
//                    holder. LinearLayoutMembershipCard.setVisibility(View.GONE);
//                    holder. cardViewMembership.setBackgroundColor(ContextCompat.getColor(myActivity, R.color.white));
//                    holder.linearLayout_color.setBackgroundDrawable( myActivity.getResources().getDrawable(R.drawable.linear_membership) );
//
//                    membership.setShowDetails(false);
//                }else{
//
//                    logMemberShipShowDetailEvent();
//                    logMemberShipShowDetailFireBaseEvent();
//                    holder.LinearLayoutMembershipCard.setVisibility(View.VISIBLE);
//                    holder.txtViewTermsConditions.setText(fromHtml(nodata));
//                    holder.cardViewMembership.setBackgroundColor(ContextCompat.getColor(myActivity, R.color.big_light_yellow));
//                    holder.linearLayout_color.setBackgroundDrawable( myActivity.getResources().getDrawable(R.drawable.add_back_expand) );
//
//                    membership.setShowDetails(true);
//                    // updateView(position);
//                }
//            }
//        });

    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logMemberShipShowDetailEvent () {
        Log.e("MemberShipShowDetail","fine");
        logger.logEvent(AppConstant.MemberShipShowDetail);
    }

    public void logMemberShipShowDetailFireBaseEvent () {
        Log.e("MemberDetailfirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.MemberShipShowDetail,bundle);
    }
    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logMembershipBuyEvent (String membershipType) {
        Log.e("MembershipBuy","fine");
        Bundle params = new Bundle();
        params.putString(AppConstant.MembershipType, membershipType);
        logger.logEvent(AppConstant.MembershipBuy, params);
    }

    public void logMembershipBuyFireBaseEvent (String membershipType) {
        Log.e("MembershipBuyfirebase","fine");
        Bundle params = new Bundle();
        params.putString(AppConstant.MembershipType, membershipType);
        mFirebaseAnalytics.logEvent(AppConstant.MembershipBuy, params);
    }

    private void addMembership(final Membership membership){

        try{

            DB snappyDB = DBFactory.open(myActivity);

            UserCart savedCart = null;               //db mai jo saved cart hai
            if (snappyDB.exists(AppConstant.USER_CART_DB)) {

                savedCart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
            }
            snappyDB.close();

            String saved_membership= null, saved_primary_key=null;
            int size= savedCart==null?0:savedCart.getServicesList().size();
            for(int i=0;i <size;i++){

                if(savedCart.getServicesList().get(i).isMembership()){

                    saved_membership= savedCart.getServicesList().get(i).getName();
                    saved_primary_key= savedCart.getServicesList().get(i).getPrimary_key();
                }
            }

            if(saved_membership==null || saved_primary_key==null){

                UserCart userCart= new UserCart();
                userCart.setCartType(AppConstant.OTHER_TYPE);

                UserServices userServices=new UserServices();
                userServices.setName(membership.getTitle());
                userServices.setPrimary_key(membership.getMembershipId());
                userServices.setMembership_id(membership.getMembershipId());
//                userServices.setPrice(Double.valueOf(membership.getDetails().get(0).
//                        getValues().get(3).getValue().
//                        substring(2,membership.getDetails().get(0).getValues().get(3).getValue().length())));
                userServices.setPrice(membership.getPrice());
                userServices.setMembership(true);

                new Thread(new UserCartTask(myActivity, userCart, userServices, false, false)).start();

            }else{

                final String primary_key= saved_primary_key==null?"": saved_primary_key;

                if(!primary_key.equalsIgnoreCase(membership.getMembershipId())){

                    AlertDialog.Builder builder = new AlertDialog.Builder(myActivity);
                    builder.setMessage(fromHtml("Do you wish to replace "+ saved_membership +" with "+ membership.getTitle()));
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, int id) {

                            UserCart userCart= new UserCart();
                            userCart.setCartType(AppConstant.OTHER_TYPE);

                            UserServices userServices=new UserServices();
                            userServices.setPrimary_key(primary_key);

                            new Thread(new UserCartTask(myActivity, userCart, userServices, true, false)).start();

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    UserCart userCart= new UserCart();
                                    userCart.setCartType(AppConstant.OTHER_TYPE);

                                    UserServices userServices=new UserServices();

                                    userServices.setName(membership.getTitle());

                                    userServices.setPrimary_key(membership.getMembershipId());
                                    userServices.setMembership_id(membership.getMembershipId());
//                                    userServices.setPrice(Double.valueOf(membership.getDetails().get(0).
//                                            getValues().get(3).getValue().
//                                            substring(2,membership.getDetails().get(0).getValues().get(3).getValue().length())));
                                    userServices.setPrice(membership.getPrice());

                                    userServices.setMembership(true);

                                    new Thread(new UserCartTask(myActivity, userCart, userServices, false, false)).start();

                                    dialog.dismiss();
                                }
                            }, 500);
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.dismiss();

                        }
                    }).show();


                }else{

                    EventBus.getDefault().post(new Event(saved_membership, true));
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
//        @BindView(R.id.txtView_save5)
//        TextView txtViewSave5;
        @BindView(R.id.txt_title)
        TextView txt_title;
//        @BindView(R.id.txtView_save20)
//        TextView txtViewSave20;
        @BindView(R.id.txtView_show_detail)
        TextView txtViewShowDetails;
        @BindView(R.id.txt_line_2)
        TextView txt_line_1;
        @BindView(R.id.txt_line_1)
        TextView txt_line_2;
        @BindView(R.id.txt_line_3)
        TextView txt_line_3;
        @BindView(R.id.linearLayout_membership_card)
        LinearLayout LinearLayoutMembershipCard;
        @BindView(R.id.txtView_terms_conditions)
        TextView txtViewTermsConditions;
        @BindView(R.id.cardView_membership)
        CardView cardViewMembership;
        @BindView(R.id.imgView_memberSHipCard)
        ImageView imgViewMemberShipCard;
        @BindView(R.id.txtViewHeaderTwo)
        TextView txtViewHeaderTwo;
        @BindView(R.id.txtViewHeaderOne)
        TextView txtViewHeaderOne;
        @BindView(R.id.linearLayout_inflate)
        LinearLayout linearLayout_inflate;
        @BindView(R.id.linearLayout_inflate_one)
        LinearLayout linearLayout_inflate_one;
//        @BindView(R.id.linearLayout_color)
//        LinearLayout linearLayout_color;
        @BindView(R.id.linearlayout_cart)
        LinearLayout linearlayoutCart;
        @BindView(R.id.addButton)
        Button addButton;
        @BindView(R.id.txt_validity)
        TextView txt_validity;
        @BindView(R.id.txt_member)
        TextView txt_member;
        @BindView(R.id.txt_count_1)
        TextView txt_count_1;
        @BindView(R.id.txt_count_2)
        TextView txt_count_2;
        @BindView(R.id.img_count)
        ImageView img_count;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);


        }
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

}
