package com.beusalons.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.beusalons.android.Dialog.CorporateSuccess;
import com.beusalons.android.Fragment.UserProfile;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.Corporate.CorporateDetail.CorporateDetailPost;
import com.beusalons.android.Model.Corporate.CorporateDetail.CorporateDetailResponse;
import com.beusalons.android.Model.Corporate.CorporateSendOtp.CorporateSendOtp;
import com.beusalons.android.Model.Corporate.CorporateSendOtp.CorporateSendResponse;
import com.beusalons.android.Model.Corporate.CorporateVerifyOtp.CorporateVerifyPost;
import com.beusalons.android.Model.Corporate.CorporateVerifyOtp.CorporateVerifyResponse;
import com.beusalons.android.Model.Loyalty.LoyaltyPointsRequest;
import com.beusalons.android.Model.Loyalty.LoyaltyPointsResponse;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.bumptech.glide.Glide;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CorporateLoginActivity extends AppCompatActivity {

    private ViewAnimator view_animator;

    private SearchableSpinner spinner_;
    private TextView txt_register, txt_next, txt_send_otp, txt_otp_email,
            txt_share_code, txt_code,txt_corporate_id, txt_1, txt_2, txt_3;
    private EditText etxt_, etxt_otp;
    private LinearLayout linear_dynamic, linear_share, linear_next, linear_refer, linear_t_n_c, linear_register;
    private ScrollView scroll_;

    private ArrayList<String> list_name, list_id;
    private CorporateDetailResponse corporateDetailResponse;

    private CoordinatorLayout coordinator_;
    private boolean update= false;
    private AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_corporate_login);
        setToolBar();
        logger = AppEventsLogger.newLogger(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        coordinator_= (CoordinatorLayout)findViewById(R.id.coordinator_);

        view_animator= (ViewAnimator)findViewById(R.id.view_animator);

        scroll_= (ScrollView)findViewById(R.id.scroll_);
        scroll_.setAlpha(0.2f);
        scroll_.setEnabled(false);

        spinner_= (SearchableSpinner)findViewById(R.id.spinner_);
        txt_register= (TextView) findViewById(R.id.txt_register);
        txt_next= (TextView)findViewById(R.id.txt_next);
        etxt_= (EditText)findViewById(R.id.etxt_);

        linear_dynamic= (LinearLayout)findViewById(R.id.linear_dynamic);
        linear_share= (LinearLayout)findViewById(R.id.linear_share);
        linear_next= (LinearLayout)findViewById(R.id.linear_next);
        linear_refer= (LinearLayout)findViewById(R.id.linear_refer);
        linear_t_n_c= (LinearLayout)findViewById(R.id.linear_t_n_c);
        linear_register= (LinearLayout)findViewById(R.id.linear_register);

        etxt_otp= (EditText)findViewById(R.id.etxt_otp);
        etxt_otp.setTransformationMethod(new NumericKeyBoardTransformationMethod());
        txt_send_otp= (TextView)findViewById(R.id.txt_send_otp);
        txt_otp_email= (TextView)findViewById(R.id.txt_otp_email);

        txt_share_code= (TextView)findViewById(R.id.txt_share_code);
        txt_code= (TextView)findViewById(R.id.txt_code);
        txt_corporate_id= (TextView)findViewById(R.id.txt_corporate_id);
        txt_1= (TextView)findViewById(R.id.txt_1);
        txt_2= (TextView)findViewById(R.id.txt_2);
        txt_3= (TextView)findViewById(R.id.txt_3);
        spinner_.setTitle("");

        txt_register.setEnabled(false);
        txt_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent= new Intent(CorporateLoginActivity.this, RegisterCorporateActivity.class);
                intent.putExtra(RegisterCorporateActivity.CORPORATE_RESPONSE,
                        new Gson().toJson(corporateDetailResponse, CorporateDetailResponse.class));
                startActivity(intent);
            }
        });

        txt_send_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOtp();
            }
        });

        linear_next.setBackgroundColor(ContextCompat.getColor(this, R.color.corporate_next));
        txt_next.setTextColor(ContextCompat.getColor(this, R.color.black));
        txt_next.setEnabled(false);

        txt_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{

                    String email= etxt_.getText().toString();

                    if(spinner_.getSelectedItemPosition() ==0)
                        Toast.makeText(CorporateLoginActivity.this, "Select Your Company", Toast.LENGTH_SHORT).show();
                    else if(email.length()<1)
                        Toast.makeText(CorporateLoginActivity.this, "Enter Email", Toast.LENGTH_SHORT).show();
                    else if(!email.endsWith(corporateDetailResponse.getData()
                            .getCompanies().get(spinner_.getSelectedItemPosition()-1).getDomain()))
                        Toast.makeText(CorporateLoginActivity.this, "Please Check Entered Email", Toast.LENGTH_SHORT).show();
                    else{

                        txt_otp_email.setText("The OTP Has Been Sent To Your Email ID: "+etxt_.getText().toString());
                        linear_next.setBackgroundColor(ContextCompat
                                .getColor(CorporateLoginActivity.this, R.color.corporate_next));
                        txt_next.setTextColor(ContextCompat.getColor(CorporateLoginActivity.this, R.color.black));
                        txt_next.setEnabled(false);

                        view_animator.setInAnimation(CorporateLoginActivity.this, R.anim.slide_from_right);
                        view_animator.showNext();
                        sendOtp();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        txt_share_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                logTapToShareCodeEvent();
                logTapToShareCodeFireBaseEvent();
                Intent sendIntent= new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");

                sendIntent.putExtra(Intent.EXTRA_TEXT , BeuSalonsSharedPrefrence.getCorporateReferCode());
                startActivity(Intent.createChooser(sendIntent, "Share code:"));
            }
        });

        if(BeuSalonsSharedPrefrence.getVerifyCorporate()){

            String txt= "\u2022 "+ BeuSalonsSharedPrefrence.getCorporateId();
            if(BeuSalonsSharedPrefrence.getCorporateId()!=null)
                txt_corporate_id.setText(txt);
            else
                txt_corporate_id.setVisibility(View.GONE);

            if(getSupportActionBar()!=null)
                getSupportActionBar().setTitle("Corporate Referrals");
            txt_code.setText(BeuSalonsSharedPrefrence.getReferCode());

            txt_share_code.setEnabled(true);
            linear_next.setVisibility(View.GONE);
            view_animator.setVisibility(View.GONE);
            linear_register.setVisibility(View.GONE);
            linear_refer.setVisibility(View.VISIBLE);
        }
        else{

            linear_refer.setVisibility(View.GONE);

            txt_share_code.setEnabled(false);
            view_animator.setVisibility(View.VISIBLE);
            linear_register.setVisibility(View.VISIBLE);
            linear_next.setVisibility(View.VISIBLE);
        }

        fetchData();
    }

    private void sendOtp(){

        txt_send_otp.setEnabled(false);
        update= true;
        runCountdown();

        CorporateSendOtp post= new CorporateSendOtp();
        post.setUserId(BeuSalonsSharedPrefrence.getUserId());
        post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        post.setCompanyId(list_id.get(spinner_.getSelectedItemPosition()));
        post.setEmailId(etxt_.getText().toString());

        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<CorporateSendResponse> call= apiInterface.corporateSendOtp(post);
        call.enqueue(new Callback<CorporateSendResponse>() {
            @Override
            public void onResponse(Call<CorporateSendResponse> call, Response<CorporateSendResponse> response) {

                if(response.isSuccessful()){
                    if(response.body().isSuccess()){
                        Log.i("corporatesend", "i'm in success");

                        linear_next.setBackgroundColor(ContextCompat.getColor(CorporateLoginActivity.this,
                                R.color.colorPrimary));
                        txt_next.setTextColor(ContextCompat.getColor(CorporateLoginActivity.this, R.color.white));
                        txt_next.setEnabled(true);
                        txt_next.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(etxt_otp.getText().toString().length()<1){

                                    etxt_otp.setError("Enter OTP");
                                }else{

                                    verifyOtp();
                                }

                            }
                        });

                    }else{

                        view_animator.setInAnimation(CorporateLoginActivity.this, R.anim.slide_from_left);
                        view_animator.showPrevious();

                        linear_next.setBackgroundColor(ContextCompat
                                .getColor(CorporateLoginActivity.this, R.color.colorPrimary));
                        txt_next.setTextColor(ContextCompat.getColor(CorporateLoginActivity.this, R.color.white));
                        txt_next.setEnabled(true);

                        showSnackbar(response.body().getMessage());
                        update= false;

                    }
                }else{

                    Log.i("corporatesend", "i'm in reponse unsuccessful");
                }


            }

            @Override
            public void onFailure(Call<CorporateSendResponse> call, Throwable t) {
                Log.i("corporatesend", "i'm in retrofit failure "+ t.getMessage()+ "  "+ t.getCause()+ " "+ t.getStackTrace());
            }
        });

    }



    private void runCountdown(){

        new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {

                if(update)
                    txt_send_otp.setText("Send OTP In: " + millisUntilFinished / 1000);
                else{

                    this.cancel();
                    this.onFinish();
                }
            }

            public void onFinish() {

                txt_send_otp.setEnabled(true);
                txt_send_otp.setText("Send OTP Again?");
            }
        }.start();
    }

    private void verifyOtp(){

        CorporateVerifyPost post= new CorporateVerifyPost();
        post.setUserId(BeuSalonsSharedPrefrence.getUserId());
        post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        post.setEmailId(etxt_.getText().toString());
        post.setOtp(etxt_otp.getText().toString());

        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<CorporateVerifyResponse> call= apiInterface.corporateVerifyOtp(post);
        call.enqueue(new Callback<CorporateVerifyResponse>() {
            @Override
            public void onResponse(Call<CorporateVerifyResponse> call, Response<CorporateVerifyResponse> response) {

                if(response.isSuccessful()){
                    if(response.body().isSuccess()){
                        Log.i("corporateverify", "i'm in success pe ");

                        dismissKeyboard();
                        logAddCorporateNextEvent();
                        logAddCorporateNextFireBaseEvent();
                        BeuSalonsSharedPrefrence.setVerifyCorporate(true);
                        BeuSalonsSharedPrefrence.setCorporateId(etxt_.getText().toString());

                        UserProfile.cor_id= etxt_.getText().toString();

                        txt_code.setText(BeuSalonsSharedPrefrence.getReferCode());

                        String txt= "\u2022 "+ etxt_.getText().toString();
                        txt_corporate_id.setText(txt);

                        linear_next.setVisibility(View.GONE);
                        view_animator.setVisibility(View.GONE);

                        linear_refer.setVisibility(View.VISIBLE);

                        if(getSupportActionBar()!=null)
                            getSupportActionBar().setTitle("Corporate Referrals");

                        CorporateSuccess success= new CorporateSuccess();
                        Bundle bundle= new Bundle();
                        bundle.putBoolean("corporate_success", true);
                        success.setArguments(bundle);
                        success.show(getSupportFragmentManager(), "corporate_success");


                        fetchData();

                    }else{

                        Log.i("corporateverify", "i'm in else pe ");
                    }


                }else{

                    Log.i("corporateverify", "i'm in else else pe ");
                }


            }

            @Override
            public void onFailure(Call<CorporateVerifyResponse> call, Throwable t) {
                Log.i("corporateverify", "i'm in retofit failure pe "+ t.getCause()+ "  "+ t.getStackTrace()+ " "+t.getMessage());
            }
        });


    }


    private void fetchData(){

        CorporateDetailPost post= new CorporateDetailPost();
        post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        post.setUserId(BeuSalonsSharedPrefrence.getUserId());

        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<CorporateDetailResponse> call= apiInterface.corporateGetDetail(post);
        call.enqueue(new Callback<CorporateDetailResponse>() {
            @Override
            public void onResponse(Call<CorporateDetailResponse> call, Response<CorporateDetailResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().getSuccess()){
                        Log.i("corporatelogin", "i'm in getdetial success pe ");

                        corporateDetailResponse= response.body();
                        txt_register.setEnabled(true);


                        list_name= new ArrayList<>();
                        list_id= new ArrayList<>();
                        list_name.add("Select Your Company");
                        list_id.add("ID");
                        for(int i=0; i<response.body().getData().getCompanies().size();i++){

                            list_name.add(response.body().getData().getCompanies().get(i).getName());
                            list_id.add(response.body().getData().getCompanies().get(i).getCompanyId());
                        }

                        ArrayAdapter<String> adapter= new ArrayAdapter<>(CorporateLoginActivity.this,
                                android.R.layout.simple_spinner_dropdown_item, list_name);
                        spinner_.setAdapter(adapter);

                        linear_dynamic.removeAllViews();

                        if(response.body().getData().getCorporateDeals()!=null &&
                                response.body().getData().getCorporateDeals().size()>0){

                            View view_= LayoutInflater.from(CorporateLoginActivity.this).inflate(R.layout.txt_referral, null);

                            TextView txt_name= (TextView)view_.findViewById(R.id.txt_name);
                            txt_name.setText("Corporate ID Benefits");

                            linear_dynamic.addView(view_);

                            int size_id= response.body().getData().getCorporateDeals().size();
                            for(int j=0; j<size_id; j++){

                                View view= LayoutInflater.from(CorporateLoginActivity.this).inflate(R.layout.corporate_benefits, null);
                                TextView txt_info= (TextView)view.findViewById(R.id.txt_info);
                                TextView txt_1= (TextView)view.findViewById(R.id.txt_1);
                                TextView txt_2= (TextView)view.findViewById(R.id.txt_2);
                                TextView txt_3= (TextView)view.findViewById(R.id.txt_3);
                                View view_last= (View)view.findViewById(R.id.view_);
                                if(size_id-1== j)
                                    view_last.setVisibility(View.VISIBLE);
                                if(j==0)
                                    txt_info.setVisibility(View.VISIBLE);

                                String str_1=  "\u2022 ";
                                txt_1.setText(str_1);
                                txt_2.setText(response.body().getData().getCorporateDeals().get(j).getStart());
                                txt_3.setText(" "+response.body().getData().getCorporateDeals().get(j).getEnd());

                                linear_dynamic.addView(view);
                            }
                        }

                        if(response.body().getData().getCorporateReferalsFemale()!=null &&
                                response.body().getData().getCorporateReferalsFemale().getData()!=null &&
                                response.body().getData().getCorporateReferalsFemale().getData().size()>0){

                            View view__= LayoutInflater.from(CorporateLoginActivity.this).inflate(R.layout.txt_referral, null);

                            LinearLayout linear_= (LinearLayout)view__.findViewById(R.id.linear_);

                            TextView txt_name_= (TextView)view__.findViewById(R.id.txt_name);
                            TextView txt_info= (TextView)view__.findViewById(R.id.txt_info);
                            txt_name_.setText("Corporate Referral Benefits");

                            linear_.setVisibility(View.VISIBLE);
                            txt_info.setText(response.body().getData()
                                    .getCorporateReferalsFemale().getDescription());

                            linear_dynamic.addView(view__);


                            View view_= LayoutInflater.from(CorporateLoginActivity.this).inflate(R.layout.txt_referral_, null);

                            TextView txt_name= (TextView)view_.findViewById(R.id.txt_name);
                            txt_name.setText("Female Upgrades");

                            ImageView img_= (ImageView)view_.findViewById(R.id.img_);

                            try{

                                Glide.with(CorporateLoginActivity.this).load(response.body().getData().getCorporateReferalsFemale().getImage()).into(img_);
                               /* Glide.with(CorporateLoginActivity.this)
                                        .load(response.body().getData().getCorporateReferalsFemale().getImage())
                                        .fitCenter()
                                        .crossFade()
                                        .into(img_);*/
                            }catch (Exception e){
                                e.printStackTrace();
                            }



                            linear_dynamic.addView(view_);

                            int size_f= response.body().getData().getCorporateReferalsFemale().getData().size();
                            for(int k=0; k<size_f; k++){

                                View view= LayoutInflater.from(CorporateLoginActivity.this).inflate(R.layout.corporate_benefits_, null);
                                TextView txt_1= (TextView)view.findViewById(R.id.txt_1);
                                TextView txt_2= (TextView)view.findViewById(R.id.txt_2);
                                TextView txt_3= (TextView)view.findViewById(R.id.txt_3);

                                txt_1.setText(response.body().getData().getCorporateReferalsFemale().getData().get(k).getStart()==null?
                                        "":response.body().getData().getCorporateReferalsFemale().getData().get(k).getStart());
                                txt_2.setText("\u2022 "+response.body().getData().getCorporateReferalsFemale().getData().get(k).getMiddle()==null?
                                        "":response.body().getData().getCorporateReferalsFemale().getData().get(k).getMiddle());
                                txt_3.setText(" "+response.body().getData().getCorporateReferalsFemale().getData().get(k).getEnd()==null?
                                        "":response.body().getData().getCorporateReferalsFemale().getData().get(k).getEnd());

                                linear_dynamic.addView(view);
                            }
                        }

                        if(response.body().getData().getCorporateReferalsMale()!=null &&
                                response.body().getData().getCorporateReferalsMale().getData()!=null &&
                                response.body().getData().getCorporateReferalsMale().getData().size()>0){

                            View view_= LayoutInflater.from(CorporateLoginActivity.this).inflate(R.layout.txt_referral_, null);

                            TextView txt_name= (TextView)view_.findViewById(R.id.txt_name);
                            txt_name.setText("Male Upgrades");

                            ImageView img_= (ImageView)view_.findViewById(R.id.img_);

                            try{
                                Glide.with(CorporateLoginActivity.this).load(response.body().getData().getCorporateReferalsFemale().getImage()).into(img_);
                               /* Glide.with(CorporateLoginActivity.this)
                                        .load(response.body().getData().getCorporateReferalsMale().getImage())
                                        .fitCenter()
                                        .crossFade()
                                        .into(img_);*/
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            linear_dynamic.addView(view_);

                            int size_m= response.body().getData().getCorporateReferalsMale().getData().size();
                            for(int k = 0; k<response.body().getData().getCorporateReferalsMale().getData().size(); k++){

                                View view= LayoutInflater.from(CorporateLoginActivity.this).inflate(R.layout.corporate_benefits_, null);
                                TextView txt_1= (TextView)view.findViewById(R.id.txt_1);
                                TextView txt_2= (TextView)view.findViewById(R.id.txt_2);
                                TextView txt_3= (TextView)view.findViewById(R.id.txt_3);

                                View view_last= view.findViewById(R.id.view_);
                                if(size_m-1== k)
                                    view_last.setVisibility(View.VISIBLE);

                                txt_1.setText(response.body().getData().getCorporateReferalsMale().getData().get(k).getStart()==null?
                                        "":response.body().getData().getCorporateReferalsMale().getData().get(k).getStart());
                                txt_2.setText("\u2022 "+response.body().getData().getCorporateReferalsMale().getData().get(k).getMiddle()==null?
                                        "":response.body().getData().getCorporateReferalsMale().getData().get(k).getMiddle());
                                txt_3.setText(" "+response.body().getData().getCorporateReferalsMale().getData().get(k).getEnd()==null?
                                        "":response.body().getData().getCorporateReferalsMale().getData().get(k).getEnd());

                                linear_dynamic.addView(view);
                            }
                        }

                        if(response.body().getData().getCorporateTnC()!=null &&
                                response.body().getData().getCorporateTnC().getPoints()!=null &&
                                response.body().getData().getCorporateTnC().getPoints().size()>0){
                            linear_t_n_c.setVisibility(View.VISIBLE);

                            txt_1.setText(response.body().getData().getCorporateTnC().getTitle());
                            String points="";
                            for(int i=0; i<response.body().getData().getCorporateTnC().getPoints().size();i++){

                                if(i==0)
                                    txt_2.setText(response.body().getData().getCorporateTnC().getPoints().get(i));
                                else
                                    points+= response.body().getData().getCorporateTnC().getPoints().get(i)+ "\n";
                            }
                            txt_3.setText(points);

                            txt_1.setTypeface(null, Typeface.ITALIC);
                            txt_2.setTypeface(null, Typeface.ITALIC);
                            txt_3.setTypeface(null, Typeface.ITALIC);

                        }else
                            linear_t_n_c.setVisibility(View.GONE);

                        scroll_.setAlpha(1f);
                        scroll_.setEnabled(true);

                        linear_next.setBackgroundColor(ContextCompat
                                .getColor(CorporateLoginActivity.this, R.color.colorPrimary));
                        txt_next.setTextColor(ContextCompat.getColor(CorporateLoginActivity.this, R.color.white));
                        txt_next.setEnabled(true);      //enable the next button

                        getCorporateShareCode();
                    }else{

                        Log.i("corporatelogin", "i'm in getdetial unsuccessful");
                    }



                }else{

                    Log.i("corporatelogin", "i'm in getdetial retofit unsuccessful ");
                }


            }

            @Override
            public void onFailure(Call<CorporateDetailResponse> call, Throwable t) {
                Log.i("corporatelogin", "i'm in getdetail failure: "+ t.getMessage()+ "  "+ t.getStackTrace()+ " "+t.getCause());
            }
        });
    }

    private void getCorporateShareCode(){

        LoyaltyPointsRequest loyaltyPointsRequest= new LoyaltyPointsRequest();
        loyaltyPointsRequest.setUserId(BeuSalonsSharedPrefrence.getUserId());
        loyaltyPointsRequest.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        loyaltyPointsRequest.setLatitude(BeuSalonsSharedPrefrence.getLatitude());
        loyaltyPointsRequest.setLongitude(BeuSalonsSharedPrefrence.getLongitude());
        loyaltyPointsRequest.setFirstLogin(0);

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<LoyaltyPointsResponse> call= apiInterface.loyaltyPoints(loyaltyPointsRequest);
        call.enqueue(new Callback<LoyaltyPointsResponse>() {
            @Override
            public void onResponse(Call<LoyaltyPointsResponse> call, Response<LoyaltyPointsResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().getSuccess()){
                        Log.i("loyaltyincorporate", "good");

                        txt_share_code.setEnabled(true);
                        if(response.body().getData().getCorporateEmailId()!=null &&
                                !response.body().getData().getCorporateEmailId().equalsIgnoreCase("") &&
                                !response.body().getData().getCorporateEmailId().equalsIgnoreCase(" "))
                            BeuSalonsSharedPrefrence.setCorporateReferCode(response.body().getData()
                                    .getCorporateReferalMessage()!=null?
                                    response.body().getData().getCorporateReferalMessage().getMessage():"");
                    }else
                        Log.i("loyaltyincorporate", "bad");

                }else
                    Log.i("loyaltyincorporate", "baddy");


            }

            @Override
            public void onFailure(Call<LoyaltyPointsResponse> call, Throwable t) {
                Log.i("loyaltyincorporate", "failure: "+t.getMessage()+ " "+t.getStackTrace());
            }
        });
    }

    public void showSnackbar(String txt){
        Snackbar snackbar = Snackbar.make( coordinator_,
                txt, 6000);

        //setting the snackbar action button text size
        View view = snackbar.getView();
        TextView txt_text = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txt_text.setTextSize(13);
        txt_text.setAllCaps(false);
        snackbar.show();
    }




    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle("Corporate Log In");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class NumericKeyBoardTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return source;
        }
    }

    private void dismissKeyboard(){

        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }



    private void logAddCorporateNextEvent () {
        Log.e("AddCorporateNext","fine");
        logger.logEvent(AppConstant.AddCorporateNext);
    }
    private void logAddCorporateNextFireBaseEvent () {
        Log.e("AddCorporateNextfire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.AddCorporateNext,bundle);
    }

    private void logTapToShareCodeEvent () {
        Log.e("TapToShareCode","fine");
        logger.logEvent(AppConstant.TapToShareCode);
    }
    private void logTapToShareCodeFireBaseEvent () {
        Log.e("TapToShareCodefire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.TapToShareCode,bundle);
    }
}
