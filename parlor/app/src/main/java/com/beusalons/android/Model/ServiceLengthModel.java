package com.beusalons.android.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Robbin Singh on 10/11/2016.
 */

public class ServiceLengthModel {

    @SerializedName("additions")
    @Expose
    private Integer additions;
    @SerializedName("name")
    @Expose
    private String name;


    /**
     *
     * @return
     * The additions
     */
    public Integer getAdditions() {
        return additions;
    }

    /**
     *
     * @param additions
     * The additions
     */
    public void setAdditions(Integer additions) {
        this.additions = additions;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

}
