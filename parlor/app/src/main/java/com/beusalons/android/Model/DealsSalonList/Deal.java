package com.beusalons.android.Model.DealsSalonList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 6/28/2017.
 */

public class Deal {

    private int dealId;       //yaha int hai

    private String serviceId;                  //service id
    private String parlorDealId;            //yeh string hai, par yeh deal id hai

    private double menuPrice;
    private double dealPrice;

    private int quantity;

    private List<ServicePrice> servicePrices = null;

    public List<ServicePrice> getServicePrices() {
        return servicePrices;
    }

    public void setServicePrices(List<ServicePrice> servicePrices) {
        this.servicePrices = servicePrices;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getParlorDealId() {
        return parlorDealId;
    }

    public void setParlorDealId(String parlorDealId) {
        this.parlorDealId = parlorDealId;
    }

    public int getDealId() {
        return dealId;
    }

    public void setDealId(int dealId) {
        this.dealId = dealId;
    }

    public double getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(double menuPrice) {
        this.menuPrice = menuPrice;
    }

    public double getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(double dealPrice) {
        this.dealPrice = dealPrice;
    }
}
