package com.beusalons.android.Model.newServiceDeals.ServiceByDepartment;

import java.util.List;

/**
 * Created by myMachine on 6/21/2017.
 */

public class Slab {

    private String slabId;
    private List<Range> ranges = null;

    public String getSlabId() {
        return slabId;
    }

    public void setSlabId(String slabId) {
        this.slabId = slabId;
    }

    public List<Range> getRanges() {
        return ranges;
    }

    public void setRanges(List<Range> ranges) {
        this.ranges = ranges;
    }
}
