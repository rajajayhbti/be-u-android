package com.beusalons.android.Model.SalonHome;

/**
 * Created by myMachine on 14-Nov-17.
 */

public class WelcomeOffer {

    private String welcomeOffer;

    public String getWelcomeOffer() {
        return welcomeOffer;
    }

    public void setWelcomeOffer(String welcomeOffer) {
        this.welcomeOffer = welcomeOffer;
    }
}
