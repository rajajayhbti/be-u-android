package com.beusalons.android.Model.Offers;

/**
 * Created by myMachine on 1/22/2017.
 */

public class ProfilePost {

    private String accessToken;
    private String userId;
    private String parlorId;


    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }
}
