package com.beusalons.android.Adapter.FreeBies;

import com.beusalons.android.Model.Loyalty.ArrayInfo;

/**
 * Created by Ashish Sharma on 9/13/2017.
 */

public class Test {
    private InnerFreebyGrid innerFreebyGrid;
    private ArrayInfo arrayInfo;

    public InnerFreebyGrid getInnerFreebyGrid() {
        return innerFreebyGrid;
    }

    public void setInnerFreebyGrid(InnerFreebyGrid innerFreebyGrid) {
        this.innerFreebyGrid = innerFreebyGrid;
    }

    public ArrayInfo getArrayInfo() {
        return arrayInfo;
    }

    public void setArrayInfo(ArrayInfo arrayInfo) {
        this.arrayInfo = arrayInfo;
    }
}
