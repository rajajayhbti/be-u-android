package com.beusalons.android.Adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.Appointments.Services;
import com.beusalons.android.R;

import java.util.List;

/**
 * Created by myMachine on 11/30/2016.
 */

public class OrderSummaryListAdapter extends RecyclerView.Adapter<OrderSummaryListAdapter.MyViewHolder> {

    private List<Services> services_list;

    public OrderSummaryListAdapter(List<Services> services_list){

        this.services_list= services_list;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_service_name, txt_service_price;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_service_name= (TextView)itemView.findViewById(R.id.txt_order_summary_service_name);
            txt_service_price= (TextView)itemView.findViewById(R.id.txt_order_summary_service_price);
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_order_summary_linear_rcy, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final Services services_data= services_list.get(position);

        Log.i("pastTagger", services_data.getName());

        holder.txt_service_name.setText(services_data.getName());
        holder.txt_service_price.setText(AppConstant.CURRENCY+ String.valueOf(services_data.getPrice()));
    }

    @Override
    public int getItemCount() {
        if(services_list!=null &&
                services_list.size()>0)
            return services_list.size();
        return 0;
    }
}
