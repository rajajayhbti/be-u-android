package com.beusalons.android.Model.DealsServices;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ajay on 6/9/2017.
 */

public class Category {

    private String name;
    private List<DealService> deals = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DealService> getDeals() {
        return deals;
    }

    public void setDeals(List<DealService> deals) {
        this.deals = deals;
    }
}
