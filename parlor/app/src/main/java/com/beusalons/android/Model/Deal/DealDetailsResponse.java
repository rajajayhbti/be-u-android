package com.beusalons.android.Model.Deal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ajay on 1/20/2017.
 */

public class DealDetailsResponse {

    private Boolean success;
    private String message;
    private List<DealsDetailsData> data=new ArrayList<>();

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<DealsDetailsData> getData() {
        return data;
    }

    public void setData(List<DealsDetailsData> data) {
        this.data = data;
    }
}
