package com.beusalons.android;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ClipDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.beusalons.android.Adapter.SalonReview;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.Reviews.ParlorReviewResponse;
import com.beusalons.android.Model.SalonReview.Data;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.PaginationScrollListener;
import com.beusalons.android.Utility.Utility;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SalonReviewActivity extends AppCompatActivity {

    public static final String SALON_ID= "salon_id";

    private RecyclerView recycler_;
    private TextView txt_empty;

    private CoordinatorLayout coordinator_;
    private Spinner spinner;
    private RatingBar rating_;
    private LinearLayout linear_, linear_content;
    private TextView txt_rating, txt_review;

    private Button btn_retry;
    private ProgressBar progress_;

    private String salon_id;
    private int sort=1;

    private SalonReview adapter;
    private int PAGE_SIZE = 1;
    private boolean isLoading= false;
    AppEventsLogger logger;

    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon_reviews);
        setToolBar();

        salon_id= getIntent().getStringExtra(SALON_ID);

        coordinator_= (CoordinatorLayout)findViewById(R.id.coordinator_);

        linear_= (LinearLayout)findViewById(R.id.linear_);
        linear_content= (LinearLayout)findViewById(R.id.linear_content);
        rating_= (RatingBar)findViewById(R.id.rating_);

        btn_retry= (Button)findViewById(R.id.btn_retry);
        progress_= (ProgressBar)findViewById(R.id.progress_);
        txt_empty= (TextView)findViewById(R.id.txt_empty);

        recycler_= (RecyclerView)findViewById(R.id.recycler_);
        logger = AppEventsLogger.newLogger(SalonReviewActivity.this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(SalonReviewActivity.this);



        spinner= (Spinner)findViewById(R.id.spinner_);
        txt_rating= (TextView)findViewById(R.id.txt_rating);
        txt_review= (TextView)findViewById(R.id.txt_review);



        btn_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fetchRating();
            }
        });
        btn_retry.performClick();

    }

    private void fetchRating(){

        progress_.setVisibility(View.VISIBLE);
        linear_content.setVisibility(View.GONE);
        btn_retry.setVisibility(View.GONE);
        txt_empty.setVisibility(View.GONE);

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<com.beusalons.android.Model.SalonReview.Response> call= apiInterface.getResponse(salon_id);
        call.enqueue(new Callback<com.beusalons.android.Model.SalonReview.Response>() {
            @Override
            public void onResponse(Call<com.beusalons.android.Model.SalonReview.Response> call, Response<com.beusalons.android.Model.SalonReview.Response> response) {

                if(response.isSuccessful()){

                    if(response.body().isSuccess()){
                        Log.i("salonrating", "I'm in the success");

                        PAGE_SIZE=1;
                        fetchData();

                        double rating= Double.parseDouble(response.body().getData().getAvgRating());
                        rating_.setRating((float)rating);
                        if((rating>3) && Build.VERSION.SDK_INT>=21)
                            rating_.setProgressTintList(ColorStateList.valueOf(
                                    ContextCompat.getColor(linear_.getContext(), R.color.colorGreen)));
                        if(rating==3 && Build.VERSION.SDK_INT>=21)
                            rating_.setProgressTintList(ColorStateList.valueOf(
                                    ContextCompat.getColor(linear_.getContext(), R.color.snackbar)));
                        else if(rating<3 && Build.VERSION.SDK_INT>=21)
                            rating_.setProgressTintList(ColorStateList.valueOf(
                                    ContextCompat.getColor(linear_.getContext(), R.color.colorPrimary)));

                        txt_rating.setText(response.body().getData().getAvgRating());
                        txt_review.setText(response.body().getData().getTotal()+" Reviews");

                        setRatingBar(response.body().getData());


                    }else{

                        Log.i("salonrating", "I'm in the else");
                        showSnackbar("Server Not Responding");
                        linear_content.setVisibility(View.GONE);
                        progress_.setVisibility(View.GONE);
                        txt_empty.setVisibility(View.GONE);
                        btn_retry.setVisibility(View.VISIBLE);
                    }
                }else{

                    Log.i("salonrating", "I'm in the else else");
                    showSnackbar("Server Not Responding");
                    linear_content.setVisibility(View.GONE);
                    progress_.setVisibility(View.GONE);
                    txt_empty.setVisibility(View.GONE);
                    btn_retry.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<com.beusalons.android.Model.SalonReview.Response> call, Throwable t) {
                Log.i("salonrating", "I'm in the failure: "+ t.getMessage()+ " " +t.getCause());
                showSnackbar("Server Not Responding");
                linear_content.setVisibility(View.GONE);
                progress_.setVisibility(View.GONE);
                txt_empty.setVisibility(View.GONE);
                btn_retry.setVisibility(View.VISIBLE);
            }
        });
    }

    private void fetchData(){

        Retrofit retrofit = ServiceGenerator.getClient();
        final ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Log.i("salonreview", "respone pe---> : "+salon_id+  " " + sort+ "  "+ PAGE_SIZE);
        Call<ParlorReviewResponse> call= apiInterface.getParlorReviews(salon_id, sort, PAGE_SIZE);
        call.enqueue(new Callback<ParlorReviewResponse>() {
            @Override
            public void onResponse(Call<ParlorReviewResponse> call, Response<ParlorReviewResponse> response) {

                if(response.isSuccessful()){
                    if(response.body().getSuccess()){
                        Log.i("salonreview", "respone pe");

                        Log.i("salonreview", "respone: "+ response.body().getData().size());
                        if(PAGE_SIZE==1){


                            if(response.body().getData().size()==0){



                                linear_content.setVisibility(View.GONE);
                                progress_.setVisibility(View.GONE);
                                btn_retry.setVisibility(View.GONE);
                                txt_empty.setVisibility(View.VISIBLE);


                            }else{

                                LinearLayoutManager layoutManager= new LinearLayoutManager(SalonReviewActivity.this,
                                        LinearLayoutManager.VERTICAL, false);
                                recycler_.setLayoutManager(layoutManager);
                                recycler_.addOnScrollListener(new PaginationScrollListener(layoutManager) {
                                    @Override
                                    protected void loadMoreItems() {

                                        if(!isLoading){

                                            Log.i("loadinghua", "im here now");
                                            isLoading= true;

                                            adapter.addProgress();

                                            PAGE_SIZE++;
                                            logReviewScrollButton();
                                            logReviewScrollButtonFirebaseEvent();
                                            fetchData();
                                        }else
                                            Log.i("loadinghua", "im else now");

                                    }

                                    @Override
                                    public int getTotalPageCount() {
                                        return 0;
                                    }

                                    @Override
                                    public boolean isLastPage() {
                                        return false;
                                    }

                                    @Override
                                    public boolean isLoading() {
                                        return false;
                                    }
                                });

                                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                        sort= position+1;
                                        PAGE_SIZE= 1;
                                        fetchData();

                                        Log.i("valuespinner", "value of position: "+position+ " "+ sort);
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });

                                adapter= new SalonReview(SalonReviewActivity.this,response.body().getData());
                                recycler_.setAdapter(adapter);

                                linear_content.setVisibility(View.VISIBLE);
                                progress_.setVisibility(View.GONE);
                                btn_retry.setVisibility(View.GONE);
                                txt_empty.setVisibility(View.GONE);
                            }

                        }else{

                            adapter.removeProgress();
                            adapter.addData(response.body().getData());
                            isLoading= false;
                        }



                    }else{
                        Log.i("salonreview", "else pe");
                        showSnackbar("Server Not Responding");
                        linear_content.setVisibility(View.GONE);
                        progress_.setVisibility(View.GONE);
                        txt_empty.setVisibility(View.GONE);
                        btn_retry.setVisibility(View.VISIBLE);
                    }
                }else{

                    Log.i("salonreview", "unsuccessful pe");
                    showSnackbar("Server Not Responding");
                    linear_content.setVisibility(View.GONE);
                    progress_.setVisibility(View.GONE);
                    txt_empty.setVisibility(View.GONE);
                    btn_retry.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ParlorReviewResponse> call, Throwable t) {
                Log.i("salonreview", "failure pe"+ t.getMessage()+ "  "+ t.getStackTrace()+ "  "+ t.getCause());
                showSnackbar("Server Not Responding");
                linear_content.setVisibility(View.GONE);
                progress_.setVisibility(View.GONE);
                txt_empty.setVisibility(View.GONE);
                btn_retry.setVisibility(View.VISIBLE);
            }
        });
    }


    private void setRatingBar(Data data){

        int total= data.getTotal()==0?1: data.getTotal();           //total reviews

        linear_.removeAllViews();
        int size= data.getCounts().size();
        for(int i=size-1; i>=0; i--, size--){

            LinearLayout layout= (LinearLayout)LayoutInflater.from(this).inflate(R.layout.salon_review_, null, false);

            TextView txt_= (TextView)layout.findViewById(R.id.txt_);
            txt_.setText(""+data.getCounts().get(i));
            txt_.setTypeface(null, Typeface.ITALIC);

            ImageView img_= (ImageView)layout.findViewById(R.id.img_);
            double per= ((double)data.getCounts().get(i)/total)*100;
            int clip_level=(int) (((double)per/100)*10000);

            Log.i("valuesvalue", "value: "+ per+ " "+ clip_level+ "  "+i +" "+ size);

            if(size==5 || size==4)
                img_.setBackground(ContextCompat.getDrawable(this, R.drawable.clip_green));
            else if(size==3)
                img_.setBackground(ContextCompat.getDrawable(this, R.drawable.clip_yellow));
            else
                img_.setBackground(ContextCompat.getDrawable(this, R.drawable.clip_red));
            ClipDrawable clipDrawable= (ClipDrawable)img_.getBackground();
            clipDrawable.setLevel(clip_level);

            RatingBar rating_= (RatingBar)layout.findViewById(R.id.rating_);
            rating_.setNumStars(size);
            rating_.setRating(size);

            if((size>3) && Build.VERSION.SDK_INT>=21)
                rating_.setProgressTintList(ColorStateList.valueOf(
                        ContextCompat.getColor(linear_.getContext(), R.color.colorGreen)));
            if(size==3 && Build.VERSION.SDK_INT>=21)
                rating_.setProgressTintList(ColorStateList.valueOf(
                        ContextCompat.getColor(linear_.getContext(), R.color.snackbar)));
            else if(size<3 && Build.VERSION.SDK_INT>=21)
                rating_.setProgressTintList(ColorStateList.valueOf(
                        ContextCompat.getColor(linear_.getContext(), R.color.colorPrimary)));

            linear_.addView(layout);
        }
    }

    private void showSnackbar(String txt){
        Snackbar snackbar = Snackbar.make( coordinator_,
                txt, 4500);

        //setting the snackbar action button text size
        View view = snackbar.getView();
        TextView txt_text = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txt_text.setTextSize(13);
        txt_text.setAllCaps(false);
        snackbar.show();
    }



    private void setToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(getResources().getString(R.string.parlor_review));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }
    }

    public void logReviewScrollButton () {
        Log.e("ReviewScroll","fine");
        logger.logEvent(AppConstant.ReviewScroll);
    }
    public void logReviewScrollButtonFirebaseEvent () {
        Log.e("ReviewScrollfirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.ReviewScroll,bundle);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
