package com.beusalons.android.Adapter.FreeBies;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.beusalons.android.Model.Loyalty.InsideArray;
import com.beusalons.android.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by Ashish Sharma on 9/13/2017.
 */

public class InnerFreebyGrid extends RecyclerView.Adapter<InnerFreebyGrid.ViewHolder> {
    private ArrayList<InsideArray> insideArray;
    private Context context;
    private View.OnClickListener innerClick;
    private int outerPosition;

    public InnerFreebyGrid(ArrayList<InsideArray> insideArray, Context context, View.OnClickListener innerClick, int outerPosition) {
        this.insideArray = insideArray;
        this.context = context;
        this.innerClick=innerClick;
        this.outerPosition=outerPosition;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.deals_department_service_grid, parent, false);
        WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int temwidth;
        if (insideArray.size()>3){
             temwidth=((width-20)/3)-50;
        }else{
             temwidth=((width-20)/3)-20;
        }
        RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(
                temwidth, RecyclerView.LayoutParams.MATCH_PARENT
        );
        params.setMargins(2,2,2,2);
        view.setLayoutParams(params);
        return new InnerFreebyGrid.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position<insideArray.size()){
            holder.txt_name.setText(insideArray.get(position).getInsideTitle());
            insideArray.get(position).getArrayInfo().get(0).setIndideArrayPosition(position);

            Test test=new Test();
            test.setArrayInfo(insideArray.get(position).getArrayInfo().get(0));
            test.setInnerFreebyGrid(this);
            holder.linear_click.setTag(test);

            holder.linear_click.setOnClickListener(innerClick);
            if (insideArray.get(position).isSelected()){
                if (outerPosition % 2==0){

                    holder.linear_click.setBackgroundColor(context.getResources().getColor(R.color.blue_DARK));
                }else {
                    holder.linear_click.setBackgroundColor(context.getResources().getColor(R.color.yellow_fedded));
                }
            }else holder.linear_click.setBackgroundColor(context.getResources().getColor(R.color.white));

            Glide.with(context)
                    .load(insideArray.get(position).getInsideImage())
//                .centerCrop()
                    .into(holder.img_);
        }


    }
    public void notifydata(int pos){
        insideArray.get(pos).setSelected(true);
        for(int i=0;i<insideArray.size();i++){

            if(i!=pos){
                insideArray.get(i).setSelected(false);

            }
        }
    }



    @Override
    public int getItemCount() {
        return insideArray.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_name;
        private ImageView img_;
        private LinearLayout linear_click;


        public ViewHolder(View itemView) {
            super(itemView);
            txt_name = (TextView) itemView.findViewById(R.id.txt_name);
            img_ = (ImageView) itemView.findViewById(R.id.img_);
            linear_click = (LinearLayout) itemView.findViewById(R.id.linear_click);

        }
    }
}
