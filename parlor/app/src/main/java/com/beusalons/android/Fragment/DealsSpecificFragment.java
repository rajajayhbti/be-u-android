package com.beusalons.android.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beusalons.android.Adapter.DealsAdapter;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.DealsServices.Category;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.R;

/**
 * Created by Ajay on 6/9/2017.
 */

public class DealsSpecificFragment extends Fragment {

    private Category category;
    private UserCart saved_cart;
   private String gender;
    public static DealsSpecificFragment newInstance(Category category, UserCart saved_cart,String gender){

        DealsSpecificFragment fragment= new DealsSpecificFragment();
        fragment.category= category;
        fragment.saved_cart= saved_cart;
       fragment.gender=gender;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_deals_specific, container, false);

        RecyclerView recyclerView= (RecyclerView)view.findViewById(R.id.rec_services);
        recyclerView.hasFixedSize();
        LinearLayoutManager manager= new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);

        if(saved_cart!=null &&
                saved_cart.getServicesList().size()>0 &&
                saved_cart.getCartType()!=null &&
                saved_cart.getCartType().equalsIgnoreCase(AppConstant.DEAL_TYPE)){
            populateData();
        }

        if(getActivity()!=null &&
                category!=null &&
                category.getDeals().size()>0){

            DealsAdapter adapter= new DealsAdapter(getActivity(), category.getDeals(),gender);
            recyclerView.setAdapter(adapter);
        }

        return view;
    }

    private void populateData(){

        for(int i=0;i<category.getDeals().size();i++){

            int quantity= 0;
            for(int j=0; j<saved_cart.getServicesList().size();j++){

                if(category.getDeals().get(i).getDealId() == saved_cart.getServicesList().get(j).getDealId())
                    quantity+=saved_cart.getServicesList().get(j).getQuantity();
            }
            category.getDeals().get(i).setQuantity(quantity);
        }
    }
}
