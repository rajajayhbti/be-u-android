package com.beusalons.android.Adapter;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Model.ServiceAvailable.ServiceAvailableData;
import com.beusalons.android.R;

import java.util.ArrayList;

/**
 * Created by Ajay on 10/5/2017.
 */

public class AdapterRemainigServices  extends RecyclerView.Adapter<AdapterRemainigServices.MyViewHolder>{
   Activity activity;
    private ArrayList<ServiceAvailableData> serviceAvailableData;

    public AdapterRemainigServices(Activity context, ArrayList<ServiceAvailableData> list){

        this.activity= context;
        this.serviceAvailableData= list;

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(activity).inflate(R.layout.row_remaining_service, parent, false);
        return new AdapterRemainigServices.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final  MyViewHolder holder, final int position) {

        holder.txtViewName.setText(serviceAvailableData.get(position).getParlorName());
        String[] expDate = serviceAvailableData.get(position).getExpiryDate().split("T");
        String splitExpDate = expDate[0]; //
        holder.txtViewExpirydate.setText("Expiry: "+splitExpDate);

        if (serviceAvailableData.get(position).isSelected()){
            holder.img_animation.setImageResource(R.drawable.ic_arrow_down);
            holder.recyclerViewRemainingService.setVisibility(View.VISIBLE);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
            holder.recyclerViewRemainingService.setLayoutManager(mLayoutManager);
            AdapterRemainingServiceChild adapter= new AdapterRemainingServiceChild(activity,
                    serviceAvailableData.get(position).getServices(), position,false);
            holder.recyclerViewRemainingService.setAdapter(adapter);

        }else {
            holder.img_animation.setImageResource(R.drawable.ic_right);
            holder.recyclerViewRemainingService.setVisibility(View.GONE);
        }


            holder.llRemainingService.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (serviceAvailableData.get(position).isSelected()){
                        holder.img_animation.setImageResource(R.drawable.ic_right);
                        holder.recyclerViewRemainingService.setVisibility(View.GONE);
                        serviceAvailableData.get(position).setSelected(false);
                        holder.viewStart.setVisibility(View.GONE);

                    }else{
                        holder.viewStart.setVisibility(View.VISIBLE);

                        holder.img_animation.setImageResource(R.drawable.ic_arrow_down);
                        holder.recyclerViewRemainingService.setVisibility(View.VISIBLE);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
                        holder.recyclerViewRemainingService.setLayoutManager(mLayoutManager);
                        AdapterRemainingServiceChild adapter= new AdapterRemainingServiceChild(activity,
                                serviceAvailableData.get(position).getServices(), position,false);
                        holder.recyclerViewRemainingService.setAdapter(adapter);
                        serviceAvailableData.get(position).setSelected(true);
                        updateView(position);
                    }
                }
            });
    }
    public void updateView(int pos){
        for(int i=0;i<serviceAvailableData.size();i++){
            if(i!=pos){
                serviceAvailableData.get(i).setSelected(false);

            }
        }
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return serviceAvailableData.size();
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout llRemainingService;
      private    ImageView img_animation;
        private TextView txtViewName,txtViewAddress,txtViewExpirydate,txt_rating;
        private RecyclerView recyclerViewRemainingService;
        private View viewStart;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_rating=(TextView)itemView.findViewById(R.id.txt_rating);
            txtViewAddress=(TextView)itemView.findViewById(R.id.txt_address) ;
            txtViewExpirydate=(TextView)itemView.findViewById(R.id.txtViewExpirydate);;
            txtViewName=(TextView)itemView.findViewById(R.id.txt_salon_name);
            viewStart=(View)itemView.findViewById(R.id.view_start) ;
            llRemainingService=(LinearLayout)itemView.findViewById(R.id.ll_remaining_service);
            img_animation=(ImageView)itemView.findViewById(R.id.img_animation_remain);
            recyclerViewRemainingService=(RecyclerView)itemView.findViewById(R.id.recycler_view_remaining_services);
        }
    }
}
