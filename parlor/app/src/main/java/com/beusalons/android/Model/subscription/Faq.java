package com.beusalons.android.Model.subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Faq {

    @SerializedName("heading")
    @Expose
    private String heading;
    @SerializedName("questions")
    @Expose
    private List<Question> questions = null;
    private List<String> tnc;

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public List<String> getTnc() {
        return tnc;
    }

    public void setTnc(List<String> tnc) {
        this.tnc = tnc;
    }
}