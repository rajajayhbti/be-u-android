package com.beusalons.android.Model.ArtistProfile;

/**
 * Created by Ajay on 1/29/2018.
 */

public class Artist_response {
    private Boolean success;
    private ArtistData data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ArtistData getData() {
        return data;
    }

    public void setData(ArtistData data) {
        this.data = data;
    }
}
