package com.beusalons.android.Model.ServiceDialog;

/**
 * Created by myMachine on 4/14/2017.
 */

public class ServiceSelectionList {


    private int price;
    private int menu_price;                                     //yehi deal price hai..
    private int additions;                       //price additions
    private String service_type_name;                                   //yeh toh hai aapka service type like normal, xl, xxl :D
    private String info;
    private int serviceCode;
    private double tax;
    private String serviceId;
    private String type;                //service type or combo or deal price..
    private int week_day;
    private boolean check_deal;
    private String price_id;
    private int index;              //lenght ke case mai index
    private String save_percentage;

    private boolean isChecked;          //checkbox ko handle karne ke liye

    private boolean isPrice;            //prices ka size 1 se zaida mai yeh true karna hai

    public boolean isPrice() {
        return isPrice;
    }

    public void setPrice(boolean price) {
        isPrice = price;
    }

    public String getSave_percentage() {
        return save_percentage;
    }

    public void setSave_percentage(String save_percentage) {
        this.save_percentage = save_percentage;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getMenu_price() {
        return menu_price;
    }

    public void setMenu_price(int menu_price) {
        this.menu_price = menu_price;
    }

    public int getAdditions() {
        return additions;
    }

    public void setAdditions(int additions) {
        this.additions = additions;
    }

    public String getService_type_name() {
        return service_type_name;
    }

    public void setService_type_name(String service_type_name) {
        this.service_type_name = service_type_name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(int serviceCode) {
        this.serviceCode = serviceCode;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getWeek_day() {
        return week_day;
    }

    public void setWeek_day(int week_day) {
        this.week_day = week_day;
    }

    public boolean isCheck_deal() {
        return check_deal;
    }

    public void setCheck_deal(boolean check_deal) {
        this.check_deal = check_deal;
    }

    public String getPrice_id() {
        return price_id;
    }

    public void setPrice_id(String price_id) {
        this.price_id = price_id;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
