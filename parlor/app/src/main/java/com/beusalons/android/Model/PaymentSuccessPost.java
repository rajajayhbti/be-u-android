package com.beusalons.android.Model;

/**
 * Created by myMachine on 1/20/2017.
 */

public class PaymentSuccessPost {

    private String razorpay_payment_id;
    private Double amount;
    private String appointmentId;
    private int paymentMethod;
    private String accessToken;
    private String userId;
    private int bookByApp;
    private int bookByNewApp;

    private int useLoyalityPoints;


    public int getUseLoyalityPoints() {
        return useLoyalityPoints;
    }

    public void setUseLoyalityPoints(int useLoyalityPoints) {
        this.useLoyalityPoints = useLoyalityPoints;
    }

    public int getBookByNewApp() {
        return bookByNewApp;
    }

    public void setBookByNewApp(int bookByNewApp) {
        this.bookByNewApp = bookByNewApp;
    }

    public int getBookByApp() {
        return bookByApp;
    }

    public void setBookByApp(int bookByApp) {
        this.bookByApp = bookByApp;
    }

    public String getRazorpay_payment_id() {
        return razorpay_payment_id;
    }

    public void setRazorpay_payment_id(String razorpay_payment_id) {
        this.razorpay_payment_id = razorpay_payment_id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public int getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(int paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
