package com.beusalons.android.Dialog;


import android.app.Activity;
import android.app.Dialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.AppointmentDetail.AppointmentDetailResponse;
import com.beusalons.android.R;


/**
 * Created by Ashish on 13/04/2017.
 */

public class ShowDetailsBillSummary {
    final Dialog dialog;

    private TextView txt_bill_summary_membership_price,txt_bill_summary_membership_tax,
            txt_bill_summary_membership_total,txt_membership_credits_used;
    LinearLayout linearLayoutManager,linear_online,ll_freebie_use,ll_extra_discount,ll_coupon_use,
            ll_container_purchase_membership,ll_biil_tax,ll_membership_credits_used,ll_subs_use,ll_subs_amount;
    private AppointmentDetailResponse appointmentDetailResponse;
    private  TextView txt_service_amount,txt_menu_price,txt_extra_discount,txt_bill_summary_subs,txt_bill_summary_subs_price,
            txtBillSummaryGrandTotal,txt_discount,txt_tax,txt_service_total,txt_bill_summary_freebee,txt_bill_summary_coupon;

    public ShowDetailsBillSummary(final Activity activity, AppointmentDetailResponse appointmentDetailResponse,
                                  boolean isonline,
                                  boolean usingFreebie,boolean isMembershipCart,boolean isUseMemberShip){
        int menu_price= 0;

        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_bill_details);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);

        txt_menu_price=(TextView)dialog.findViewById(R.id.txt_bill_summary_menu_price);
        txt_service_amount=(TextView)dialog.findViewById(R.id.txt_service_amount);
        txtBillSummaryGrandTotal=(TextView)dialog.findViewById(R.id.txt_bill_summary_grand_total);
        txt_discount=(TextView)dialog.findViewById(R.id.txt_bill_summary_discount);
        txt_tax=(TextView)dialog.findViewById(R.id.txt_bill_summary_tax);
        txt_service_total=(TextView)dialog.findViewById(R.id.txt_service_total);
        txt_extra_discount=(TextView)dialog.findViewById(R.id.txt_extra_discount);
        txt_bill_summary_freebee=(TextView)dialog.findViewById(R.id.txt_bill_summary_freebee);
        txt_bill_summary_membership_price=(TextView)dialog.findViewById(R.id.txt_bill_summary_membership_price);
        txt_bill_summary_membership_tax=(TextView)dialog.findViewById(R.id.txt_bill_summary_membership_tax);
        txt_bill_summary_membership_total=(TextView)dialog.findViewById(R.id.txt_bill_summary_membership_total);
        linearLayoutManager= (LinearLayout) dialog.findViewById(R.id.ll_show_deal_dialogue);
        linear_online= (LinearLayout) dialog.findViewById(R.id.linear_online);
        ll_biil_tax= (LinearLayout) dialog.findViewById(R.id.ll_biil_tax);
        ll_freebie_use= (LinearLayout) dialog.findViewById(R.id.ll_freebie_use);
        ll_coupon_use= (LinearLayout) dialog.findViewById(R.id.ll_coupon_use);
        ll_container_purchase_membership = (LinearLayout) dialog.findViewById(R.id.ll_container_purchase_membership);
        ll_membership_credits_used = (LinearLayout) dialog.findViewById(R.id.ll_membership_credits_used);
        ll_extra_discount = (LinearLayout) dialog.findViewById(R.id.ll_extra_discount);
        ll_subs_use = (LinearLayout) dialog.findViewById(R.id.ll_subs_use);
        ll_subs_amount = (LinearLayout) dialog.findViewById(R.id.ll_subs_amount);
        txt_membership_credits_used=(TextView)dialog.findViewById(R.id.txt_membership_credits_used);
        txt_bill_summary_coupon=(TextView)dialog.findViewById(R.id.txt_bill_summary_coupon);
        txt_bill_summary_subs_price=(TextView)dialog.findViewById(R.id.txt_bill_summary_subs_price);
        txt_bill_summary_subs=(TextView)dialog.findViewById(R.id.txt_bill_summary_subs);

        int services= appointmentDetailResponse.getData().getServices().size();
        for(int i=0; i<services;i++){

            menu_price+= appointmentDetailResponse.getData().getServices().get(i).getActualPrice();
            Log.i("invaaaaaa", "value in quanitty: "+ appointmentDetailResponse.getData().getServices().get(i).getQuantity());
        }

        txt_menu_price.setText(AppConstant.CURRENCY+menu_price);
        //yeh saved wala hai grand total ke pass wala



        int discount= (int)appointmentDetailResponse.getData().getDiscount()+(int)appointmentDetailResponse.getData().getTotalSaved();
        if(discount> 0.0){

            txt_discount.setText("(-) "+AppConstant.CURRENCY+ (int) discount);

        }else{

            txt_discount.setText("(-) "+AppConstant.CURRENCY+ 0);
        }

        if (appointmentDetailResponse.getData().getLoyalitySubscription()>0){
            ll_subs_use.setVisibility(View.VISIBLE);
            txt_bill_summary_subs.setText("(-) "+AppConstant.CURRENCY+ appointmentDetailResponse.getData().getLoyalitySubscription());
        }else {
            ll_subs_use.setVisibility(View.GONE);
        }
        if (isMembershipCart){
            ll_container_purchase_membership.setVisibility(View.VISIBLE);
            int membershipaAmount=(int)appointmentDetailResponse.getData().getMembershipAmount();
            txt_bill_summary_membership_price.setText(AppConstant.CURRENCY+String.valueOf((int)membershipaAmount));
//            txt_bill_summary_membership_tax.setText(AppConstant.CURRENCY+String.valueOf((int)appointmentDetailResponse.getData().getMembershipTax()));
            txt_bill_summary_membership_total.setText(AppConstant.CURRENCY+String.valueOf((int)appointmentDetailResponse.getData().getMembershipAmount()));

        }else ll_container_purchase_membership.setVisibility(View.GONE);



        int packageDiscount=(int)appointmentDetailResponse.getData().getPackageDiscount();
        if (packageDiscount>0){
            ll_extra_discount.setVisibility(View.VISIBLE);
            txt_extra_discount.setText("(-) "+AppConstant.CURRENCY+packageDiscount);
        }else ll_extra_discount.setVisibility(View.GONE);

        int serviceAmount=menu_price-(discount+packageDiscount+(int)appointmentDetailResponse.getData().getLoyalityPoints());
        txt_service_amount.setText(AppConstant.CURRENCY+serviceAmount);
        double serviceTotal=serviceAmount+appointmentDetailResponse.getData().getTax();

        if(appointmentDetailResponse.getData().getSubscriptionAmount()>0){
            ll_subs_amount.setVisibility(View.VISIBLE);
            txt_bill_summary_subs_price.setText(AppConstant.CURRENCY+appointmentDetailResponse.getData().getSubscriptionAmount());
        }else{
            ll_subs_amount.setVisibility(View.GONE);
        }
        txt_service_total.setText(AppConstant.CURRENCY+(int)serviceTotal);
        double grandTotal=(serviceTotal-appointmentDetailResponse.getData().getCreditUsed())+appointmentDetailResponse.getData().getSubscriptionAmount();
        if (isMembershipCart){
            grandTotal=(serviceTotal-appointmentDetailResponse.getData().getCreditUsed())+appointmentDetailResponse.getData().getMembershipAmount();
        }
        txtBillSummaryGrandTotal.setText(AppConstant.CURRENCY+Math.ceil(grandTotal));
        if (isonline){



            if (appointmentDetailResponse.getData().getTax()>0){
                ll_biil_tax.setVisibility(View.VISIBLE);
                txt_tax.setText(AppConstant.CURRENCY+ appointmentDetailResponse.getData().getTax());
            }
            else ll_biil_tax.setVisibility(View.GONE);
//            if (isMembershipCart){
//                double amount=appointmentDetailResponse.getData().getPayableAmount()+appointmentDetailResponse.getData().getMembershipAmount();
//              //  txtBillSummaryGrandTotal.setText(AppConstant.CURRENCY+(int)amount);
//            }
//            else txtBillSummaryGrandTotal.setText(AppConstant.CURRENCY+(int)appointmentDetailResponse.getData().getPayableAmount());

            double sub_total= (appointmentDetailResponse.getData().getSubtotal()-
                    appointmentDetailResponse.getData().getLoyalityPoints())-appointmentDetailResponse.getData().getOnlineDiscount();


        }else {

            txt_tax.setText(AppConstant.CURRENCY+appointmentDetailResponse.getData().getTax());
//            linear_online.setVisibility(View.GONE);
//            txtBillSummaryGrandTotal.setText(AppConstant.CURRENCY+(int)appointmentDetailResponse.getData().getPayableAmount());
            double sub_total= appointmentDetailResponse.getData().getSubtotal()-
                    appointmentDetailResponse.getData().getLoyalityPoints();

        }
        if (usingFreebie || appointmentDetailResponse.getData().getLoyalityPoints()>0){
            ll_freebie_use.setVisibility(View.VISIBLE);
            int freebie=(int)appointmentDetailResponse.getData().getLoyalityPoints()-(int)appointmentDetailResponse.getData().getLoyalitySubscription();
            txt_bill_summary_freebee.setText("(-) "+freebie);
        }else{
            ll_freebie_use.setVisibility(View.GONE);
        }

        if (appointmentDetailResponse.getData().getCouponLoyalityPoints()>0){
            ll_coupon_use.setVisibility(View.VISIBLE);
            txt_bill_summary_coupon.setText("(-) "+(int)appointmentDetailResponse.getData().getCouponLoyalityPoints());
        }else  ll_coupon_use.setVisibility(View.GONE);


        if (isUseMemberShip){
            ll_membership_credits_used.setVisibility(View.VISIBLE);
            txt_membership_credits_used.setText("(-)"+AppConstant.CURRENCY+String.valueOf((int)appointmentDetailResponse.getData().getCreditUsed()));
        }else{
            ll_membership_credits_used.setVisibility(View.GONE);
        }




        linearLayoutManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        dialog.show();
    }

}
