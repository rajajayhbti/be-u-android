package com.beusalons.android;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.beusalons.android.Adapter.AdapterRemainigServices;
import com.beusalons.android.Event.RemainingServicesEvent.RemainingServiceIndex;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.ServiceAvailable.Service;
import com.beusalons.android.Model.ServiceAvailable.ServiceAvailableData;
import com.beusalons.android.Model.ServiceAvailable.ServiceAvailable_post;
import com.beusalons.android.Model.ServiceAvailable.ServiceAvailable_response;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Task.UserCartTask;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.snappydb.DB;
import com.snappydb.DBFactory;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Ajay on 10/5/2017.
 */

public class RemainingServiceActivity extends AppCompatActivity {

    AdapterRemainigServices adapterRemainigServices;
    UserCart saved_cart= null;

    RecyclerView recyclerViewRemainingSer;
    View loadingForRemainingService;
    private List<UserServices> list= new ArrayList<>();

    private ArrayList<ServiceAvailableData> serviceAvailableData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.remaining_service_activity);
          setToolBar();
           inItView();

    }

    private void inItView(){
        serviceAvailableData=new ArrayList<>();
        loadingForRemainingService=(View)findViewById(R.id.loading_for_remaining_service);
        recyclerViewRemainingSer=(RecyclerView)findViewById(R.id.recycler_view_remaining);
        fetchData();
    }


    private void fetchData(){
        loadingForRemainingService.setVisibility(View.VISIBLE);
        ServiceAvailable_post serviceAvailable_post=new ServiceAvailable_post();
        serviceAvailable_post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        serviceAvailable_post.setUserId(BeuSalonsSharedPrefrence.getUserId());
        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface=retrofit.create(ApiInterface.class);
        Call<ServiceAvailable_response> call=apiInterface.getRemainingService(serviceAvailable_post);

        call.enqueue(new Callback<ServiceAvailable_response>() {
            @Override
            public void onResponse(Call<ServiceAvailable_response> call, Response<ServiceAvailable_response> response) {

          if (response.body().getSuccess()){
              loadingForRemainingService.setVisibility(View.GONE);

              if (serviceAvailableData.size()>0){
                           serviceAvailableData.clear();
                   }

                for (int i=0;i<response.body().getData().size();i++){
                    serviceAvailableData.add(response.body().getData().get(i));

                }

               adapterRemainigServices=new AdapterRemainigServices(RemainingServiceActivity.this,serviceAvailableData);
               RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(RemainingServiceActivity.this);
               recyclerViewRemainingSer.setLayoutManager(mLayoutManager);
               recyclerViewRemainingSer.setAdapter(adapterRemainigServices);
              getCardData();
              adapterRemainigServices.notifyDataSetChanged();
             }


            }

            @Override
            public void onFailure(Call<ServiceAvailable_response> call, Throwable t) {

            }
        });
    }

    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(getResources().getString(R.string.my_remaining_services));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(RemainingServiceIndex event) {

        Log.i("remainingservice", "i'm in the event");

        UserCart userCart= new UserCart();
        userCart.setParlorId("594a359d9856d3158171ea4f");
        userCart.setParlorName(serviceAvailableData.get(event.getParlor_position()).getParlorName());
        userCart.setCartType(AppConstant.SERVICE_TYPE);
//        userCart.setParlorType(homeResponse.getData().getParlorType());
//
//        userCart.setGender(homeResponse.getData().getGender());
//        userCart.setRating(homeResponse.getData().getRating());
//
//        userCart.setOpeningTime(homeResponse.getData().getOpeningTime());
//        userCart.setClosingTime(homeResponse.getData().getClosingTime());
//
//        userCart.setAddress1(homeResponse.getData().getAddress1());
//        userCart.setAddress2(homeResponse.getData().getAddress2());

        //service kara
        UserServices userServices=new UserServices();

        userServices.setName(serviceAvailableData.get(event.getParlor_position())
                .getServices().get(event.getService_position()).getName());

        userServices.setPrice(serviceAvailableData.get(event.getParlor_position())
                .getServices().get(event.getService_position()).getPrice());
        userServices.setMenu_price(serviceAvailableData.get(event.getParlor_position())
                .getServices().get(event.getService_position()).getActualPrice());

        userServices.setService_code(serviceAvailableData.get(event.getParlor_position())
                .getServices().get(event.getService_position()).getServiceCode());
        userServices.setPrice_id(serviceAvailableData.get(event.getParlor_position())
                .getServices().get(event.getService_position()).getServiceCode());

        String brand_id= serviceAvailableData.get(event.getParlor_position())
                .getServices().get(event.getService_position()).getBrandId()==null?"":serviceAvailableData.get(event.getParlor_position())
                .getServices().get(event.getService_position()).getBrandId();
        String product_id= serviceAvailableData.get(event.getParlor_position())
                .getServices().get(event.getService_position()).getProductId()==null?"":serviceAvailableData.get(event.getParlor_position())
                .getServices().get(event.getService_position()).getProductId();
        userServices.setBrand_id(brand_id);
        userServices.setProduct_id(product_id);

        String primary_key= serviceAvailableData.get(event.getParlor_position())
                .getServices().get(event.getService_position()).getServiceCode()+brand_id+product_id;
        userServices.setPrimary_key(primary_key);

        userServices.setRemainingService(true);
        new Thread(new UserCartTask(RemainingServiceActivity.this, userCart, userServices, false, false)).start();
        updateAddData();

//        updateQuantity(list);
    }

    private void updateAddData(){
        Thread  thread = new Thread(){
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(200);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                getCardData();
                            }
                        });

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                /*Intent mainActivity = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(mainActivity);*/
            };
        };
        thread.start();
    }
    private void getCardData(){

        try {
            DB snappyDB= DBFactory.open(RemainingServiceActivity.this);
            saved_cart= null;               //db mai jo saved cart hai
            if(snappyDB.exists(AppConstant.USER_CART_DB)){

                saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
                snappyDB.close();
                if(saved_cart.getServicesList().size()>0){
                    list.clear();
                    list= saved_cart.getServicesList();
                }
                updateParlor(saved_cart,serviceAvailableData);
            }else{
                snappyDB.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }




    private void updateParlor(UserCart userCart,List<ServiceAvailableData> serviceAvailableData){
       for (int i=0;i<serviceAvailableData.size();i++){
           if (userCart.getParlorId().equals("594a359d9856d3158171ea4f")){
               updateQuantity(userCart.getServicesList(),serviceAvailableData.get(i).getServices());
           }
       }
    }
    private void updateQuantity(List<UserServices> list ,List<Service> serviceAvailableData){
        for (int i=0;i<list.size();i++){
            for (int j=0;j<serviceAvailableData.size();j++){
                if (list.get(i).getService_code()==serviceAvailableData.get(j).getServiceCode()){
                     serviceAvailableData.get(j).setMyQuantity(list.get(i).getQuantity());
                }
            }
        }
        adapterRemainigServices.notifyDataSetChanged();
    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
      //  openCart();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
