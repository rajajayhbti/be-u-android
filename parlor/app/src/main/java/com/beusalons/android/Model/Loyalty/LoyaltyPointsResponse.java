package com.beusalons.android.Model.Loyalty;

/**
 * Created by myMachine on 11/28/2016.
 */

public class LoyaltyPointsResponse {

    private Boolean success;
    private String message;
    private DataResponse data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataResponse getData() {
        return data;
    }

    public void setData(DataResponse data) {
        this.data = data;
    }
}
