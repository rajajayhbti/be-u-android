package com.beusalons.android.Fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.beusalons.android.Event.SortFilterEvent;
import com.beusalons.android.Model.SalonFilter.Category;
import com.beusalons.android.Model.SalonFilter.FilterResponse;
import com.beusalons.android.Model.SalonFilter.SubList;
import com.beusalons.android.R;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayout;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 22-Feb-18.
 */

public class SalonFilterFragment extends DialogFragment{


    public static final String DATA= "com.beusalons.salonfilterfragment.data";
    public static final String SORT= "com.beusalons.salonfilterfra.sort";
    public static final String CATEGORY= "com.beusalons.salonfilterfra.category";
    public static final String RATING= "com.beusalons.salonfilterfra.rating";



    private FilterResponse response=null;


    private int tab_index=0;

    //sort ka stuff
    private List<Sort> list_sort;
    private String sort_= "0";

    //filter ka rating
    private List<Rating> list_rating;
    private String rating_= null;

    //category
    private String category_= null;

    //brands;
    private List<String> brands_;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);          //no action bar space


        Bundle bundle= getArguments();
        if(bundle!=null){

            response= new Gson().fromJson(bundle.getString(DATA), FilterResponse.class);
            sort_= bundle.getString(SORT, "0");
            category_= bundle.getString(CATEGORY, null);
            rating_= bundle.getString(RATING, null);
        }


        final RelativeLayout relative_= (RelativeLayout) inflater.inflate(R.layout.fragment_salon_filter, container, false);

        final ViewAnimator animator_= relative_.findViewById(R.id.animator_);
        TabLayout tab_ = relative_.findViewById(R.id.tab_);
        tab_.addTab(tab_.newTab().setText("SORT"));
        tab_.addTab(tab_.newTab().setText("FILTER"));

        tab_.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                int pos= tab.getPosition();
                if(pos==0 &&
                        tab_index!=0){ //sort

                    animator_.setInAnimation(getActivity(), R.anim.slide_from_left);
                    animator_.showPrevious();

                }else if(pos==1 &&
                        tab_index!=1){ //filter

                    animator_.setInAnimation(getActivity(), R.anim.slide_from_right);
                    animator_.showNext();

                }else{
                    //kuch nai
                }

                tab_index= pos;
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



        TextView txt_cancel= relative_.findViewById(R.id.txt_cancel);
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        TextView txt_clear= relative_.findViewById(R.id.txt_clear);
        txt_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                category(relative_);
                brands(relative_);
                rating(relative_);

                sort_= "0";
                rating_= null;
                category_= null;
            }
        });

        TextView txt_apply= relative_.findViewById(R.id.txt_apply);
        txt_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new SortFilterEvent(sort_, rating_, category_,
                        brands_));
                dismiss();
            }
        });


        sort(relative_);
        //category
        response.getData().getCategories()
                .add(new Category(100, "ALL", false));
        category(relative_);

        brands(relative_);
        rating(relative_);


        return relative_;
    }

    private void brands(final RelativeLayout relative_){

        LinearLayout linear_brands= relative_.findViewById(R.id.linear_brands);
        linear_brands.removeAllViews();

        brands_= new ArrayList<>();

        for(int i=0;i<response.getData().getBrands().size();i++){

            final int pos= i;
            FlexboxLayout flex_row= new FlexboxLayout(relative_.getContext());
            flex_row.setLayoutParams(new
                    LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            flex_row.setFlexWrap(FlexWrap.WRAP);

            flex_row.removeAllViews();

            final List<Boolean> isCheck_list= new ArrayList<>();

            for(int j=0;j<response.getData().getBrands().get(i).size();j++){
                final int index= j;

                TextView txt_static= (TextView) LayoutInflater.from(relative_.getContext())
                        .inflate(R.layout.filter_brands_text, null);
                if(pos==0 &&
                        index==0){
                    txt_static.setText("Hair");
                    flex_row.addView(txt_static);
                }else if(pos==1 &&
                        index==0){
                    txt_static.setText("Beauty");
                    flex_row.addView(txt_static);
                }else if(pos==2 &&
                        index==0){
                    txt_static.setText("Hand & feet");
                    flex_row.addView(txt_static);
                }

                isCheck_list.add(false);
                final CardView card_= (CardView) LayoutInflater.from(relative_.getContext())
                        .inflate(R.layout.card_brands, null);

                TextView txt_= card_.findViewById(R.id.txt_);
                txt_.setText(response.getData().getBrands().get(i).get(j));
                card_.setCardElevation(0);
                card_.setAlpha(.6f);

                card_.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(isCheck_list.get(index)){
                            isCheck_list.set(index, false);
                            card_.setCardElevation(0);
                            card_.setAlpha(.6f);

                            for(int a=0;a<brands_.size();a++)
                                if(brands_.get(a).equalsIgnoreCase(response.getData().getBrands().get(pos).get(index)))
                                    brands_.remove(a);


                        }else{
                            isCheck_list.set(index, true);
                            card_.setCardElevation(6);
                            card_.setAlpha(1f);

                            brands_.add(response.getData().getBrands().get(pos).get(index));

                        }

                    }
                });

                flex_row.addView(card_);
            }


            linear_brands.addView(flex_row);

        }


    }

    private void category(RelativeLayout relative_){

        LinearLayout linear_category= relative_.findViewById(R.id.linear_category);
        linear_category.removeAllViews();


        final List<CardView> list_card= new ArrayList<>();
        final int size= response.getData().getCategories().size();
        for(int z=0;z<size;z++)
            response.getData().getCategories().get(z).setCheck(false);


        for(int i=0;i<size;i++){

            final int index= i;
            if(index== size-1){
                final CardView card_row= (CardView) LayoutInflater.from(relative_.getContext())
                        .inflate(R.layout.card_category_empty, null);


                list_card.add(card_row);
                if(response.getData().getCategories().get(index).isCheck() || (category_!=null &&
                        index== Integer.parseInt(category_))){
                    response.getData().getCategories().get(index).setCheck(true);
                    card_row.setCardElevation(6);
                    card_row.setAlpha(1f);
                }else{
                    card_row.setCardElevation(0);
                    card_row.setAlpha(.6f);
                }

                card_row.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(response.getData().getCategories().get(index).isCheck()){

                            response.getData().getCategories().get(index).setCheck(false);
                            card_row.setCardElevation(0);
                            card_row.setAlpha(.6f);

                            category_= null;

                        }else{
                            response.getData().getCategories().get(index).setCheck(true);
                            card_row.setCardElevation(6);
                            card_row.setAlpha(1f);

                            category_= ""+index;

                        }

                        //if nothing is check 1 check dikhayo
//                        int selected_radio_=0;
//                        for(int a=0;a<size;a++)
//                            if(!response.getData().getCategories().get(a).isCheck())
//                                selected_radio_++;
//                        if(selected_radio_==size){
//                            response.getData().getCategories().get(index).setCheck(true);
//                            card_row.setCardElevation(4);
//                            card_row.setAlpha(1f);
//                        }

                        //not uncheck everything else
                        for(int b=0;b<size;b++){
                            if(b!=index){
                                response.getData().getCategories().get(b).setCheck(false);
                                list_card.get(b).setCardElevation(0);
                                list_card.get(b).setAlpha(.6f);
                            }
                        }

                    }
                });
                linear_category.addView(card_row);
            }else{
                final CardView card_row= (CardView) LayoutInflater.from(relative_.getContext())
                        .inflate(R.layout.card_category, null);

                TextView txt_price= card_row.findViewById(R.id.txt_price);

                TextView txt_brands= card_row.findViewById(R.id.txt_brands);
                txt_brands.setText(response.getData().getCategories().get(i).getText());

                ImageView img_= card_row.findViewById(R.id.img_);
                if(response.getData().getCategories().get(i).getParlorType()==0){
                    img_.setImageResource(R.drawable.ic_premium_badge);
                    txt_price.setText(fromHtml("<b><font color='#3e780a'>₹ ₹ ₹ ₹</font>"+"<font color='#58595b'>  -  </font>"+
                            "<font color='#3e780a'>₹ ₹ ₹ ₹ ₹</font></b>"));
                }else if(response.getData().getCategories().get(i).getParlorType()==1){
                    img_.setImageResource(R.drawable.ic_standard_badge);
                    txt_price.setText(fromHtml("<b><font color='#3e780a'>₹ ₹ ₹</font>"+"<font color='#58595b'>  -  </font>"+
                            "<font color='#3e780a'>₹ ₹ ₹ ₹</font></b>"));
                }else if(response.getData().getCategories().get(i).getParlorType()==2){
                    img_.setImageResource(R.drawable.ic_budget_badge);
                    txt_price.setText(fromHtml("<b><font color='#3e780a'>₹ ₹</font>"+"<font color='#58595b'>  -  </font>"+
                            "<font color='#3e780a'>₹ ₹ ₹</font></b>"));
                }
                list_card.add(card_row);
                if(response.getData().getCategories().get(index).isCheck() || (category_!=null &&
                        index== Integer.parseInt(category_))){
                    card_row.setCardElevation(6);
                    card_row.setAlpha(1f);
                    response.getData().getCategories().get(index).setCheck(true);
                }else{
                    card_row.setCardElevation(0);
                    card_row.setAlpha(.6f);
                }

                card_row.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(response.getData().getCategories().get(index).isCheck()){

                            response.getData().getCategories().get(index).setCheck(false);
                            card_row.setCardElevation(0);
                            card_row.setAlpha(.6f);

                            category_= null;

                        }else{
                            response.getData().getCategories().get(index).setCheck(true);
                            card_row.setCardElevation(6);
                            card_row.setAlpha(1f);

                            category_= ""+index;
                        }

                        //if nothing is check 1 check dikhayo
//                        int selected_radio_=0;
//                        for(int a=0;a<size;a++)
//                            if(!response.getData().getCategories().get(a).isCheck())
//                                selected_radio_++;
//                        if(selected_radio_==size){
//                            response.getData().getCategories().get(index).setCheck(true);
//                            card_row.setCardElevation(4);
//                            card_row.setAlpha(1f);
//                        }

                        //not uncheck everything else
                        for(int b=0;b<size;b++){
                            if(b!=index){
                                response.getData().getCategories().get(b).setCheck(false);
                                list_card.get(b).setCardElevation(0);
                                list_card.get(b).setAlpha(.6f);
                            }
                        }

                    }
                });
                linear_category.addView(card_row);
            }

        }


    }


    private void rating(RelativeLayout relative_){

        LinearLayout linear_rating= relative_.findViewById(R.id.linear_rating);
        linear_rating.removeAllViews();

        list_rating= new ArrayList<>();
        list_rating.add(new Rating("4.0", false));
        list_rating.add(new Rating("3.0", false));
        list_rating.add(new Rating("2.0", false));
        list_rating.add(new Rating("1.0", false));

        final int size= list_rating.size();
        final List<CardView> list_card= new ArrayList<>();
        for(int i=0;i<size;i++){

            final int index= i;
            final CardView card_row= (CardView) LayoutInflater.from(relative_.getContext())
                    .inflate(R.layout.card_rating, null);

            TextView txt_rating= card_row.findViewById(R.id.txt_rating);
            txt_rating.setText(list_rating.get(i).getRating());

            list_card.add(card_row);
            if(list_rating.get(index).isCheck || (rating_!=null &&
                    index== Integer.parseInt(rating_))){
                card_row.setCardElevation(6);
                card_row.setAlpha(1f);
                list_rating.get(index).setCheck(true);
            }else{
                card_row.setCardElevation(0);
                card_row.setAlpha(.6f);
            }

            card_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(list_rating.get(index).isCheck){

                        list_rating.get(index).setCheck(false);
                        card_row.setCardElevation(0);
                        card_row.setAlpha(.6f);

                        rating_= null;

                    }else{
                        list_rating.get(index).setCheck(true);
                        card_row.setCardElevation(6);
                        card_row.setAlpha(1f);

                        rating_= ""+index;
                    }

                    //if nothing is check 1 check dikhayo
//                    int selected_radio_=0;
//                    for(int a=0;a<size;a++)
//                        if(!list_rating.get(a).isCheck)
//                            selected_radio_++;
//                    if(selected_radio_==size){
//                        list_rating.get(index).setCheck(true);
//                        card_row.setCardElevation(4);
//                        card_row.setAlpha(1f);
//                    }

                    //not uncheck everything else
                    for(int b=0;b<size;b++){
                        if(b!=index){
                            list_rating.get(b).setCheck(false);
                            list_card.get(b).setCardElevation(0);
                            list_card.get(b).setAlpha(.6f);
                        }
                    }
                }
            });


            linear_rating.addView(card_row);
        }

    }



    private void sort(RelativeLayout relative_){

        LinearLayout linear_sort= relative_.findViewById(R.id.linear_sort);
        linear_sort.removeAllViews();

        list_sort= new ArrayList<>();
        list_sort.add(new Sort("Relevance", sort_.equalsIgnoreCase("0")));
        list_sort.add(new Sort("Near Me", sort_.equalsIgnoreCase("1")));
        list_sort.add(new Sort("PRICE RANGE - High To Low", sort_.equalsIgnoreCase("2")));
        list_sort.add(new Sort("PRICE RANGE - Low To High", sort_.equalsIgnoreCase("3")));
        list_sort.add(new Sort("Rating", sort_.equalsIgnoreCase("4")));

        final int size= list_sort.size();
        final List<RadioButton> list_radio= new ArrayList<>();
        for(int i=0; i<size;i++){

            final int index= i;
            LinearLayout linear_row= (LinearLayout) LayoutInflater.from(relative_.getContext())
                    .inflate(R.layout.linear_sort_row, null);

            TextView txt_= linear_row.findViewById(R.id.txt_);
            txt_.setText(list_sort.get(index).getName());

            final RadioButton radio_= linear_row.findViewById(R.id.radio_);
            if(list_sort.get(index).isCheck){
                radio_.setChecked(true);
            }else{
                radio_.setChecked(false);
            }
            list_radio.add(radio_);

            LinearLayout linear_check= linear_row.findViewById(R.id.linear_check);
            linear_check.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(list_sort.get(index).isCheck){

                        list_sort.get(index).setCheck(false);
                        radio_.setChecked(false);

                    }else{
                        list_sort.get(index).setCheck(true);
                        radio_.setChecked(true);
                    }

                    sort_= ""+index;

                    //if nothing is check 1 check dikhayo
                    int selected_radio_=0;
                    for(int a=0;a<size;a++)
                        if(!list_sort.get(a).isCheck)
                            selected_radio_++;
                    if(selected_radio_==size){
                        list_sort.get(index).setCheck(true);
                        radio_.setChecked(true);
                    }

                    //not uncheck everything else
                    for(int b=0;b<size;b++){
                        if(b!=index){
                            list_sort.get(b).setCheck(false);
                            list_radio.get(b).setChecked(false);
                        }
                    }

                }
            });

            linear_sort.addView(linear_row);
        }
    }


    private class Rating{

        private String rating;
        private boolean isCheck= false;

        public Rating(String rating, boolean isCheck){
            this.rating= rating;
            this.isCheck= isCheck;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public boolean isCheck() {
            return isCheck;
        }

        public void setCheck(boolean check) {
            isCheck = check;
        }
    }

    private class Sort{

        private String name;
        private boolean isCheck= false;

        public Sort(String name, boolean isCheck){
            this.name= name;
            this.isCheck= isCheck;
        }


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isCheck() {
            return isCheck;
        }

        public void setCheck(boolean check) {
            isCheck = check;
        }
    }




    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {

            Window window = getDialog().getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFFFFF")));
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }catch (Exception e){

            e.printStackTrace();
        }

    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

}
