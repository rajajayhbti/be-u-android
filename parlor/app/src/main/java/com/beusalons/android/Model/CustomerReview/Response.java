package com.beusalons.android.Model.CustomerReview;

/**
 * Created by myMachine on 9/13/2017.
 */

public class Response {

    private Boolean success;
    private Data data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
