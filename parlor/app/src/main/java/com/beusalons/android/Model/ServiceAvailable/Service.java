package com.beusalons.android.Model.ServiceAvailable;

/**
 * Created by Ajay on 10/9/2017.
 */

public class Service {

    private Integer serviceCode;
    private String name;
    private String serviceName;
    private Integer quantity=0;
    private String productId;
    private String brandProductDetail;
    private String brandId;
    private double price;
    private Integer actualPrice;
    private Integer myQuantity=0;



    private String serviceId;
    private String type;
    private Integer quantityRemaining;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getQuantityRemaining() {
        return quantityRemaining;
    }

    public void setQuantityRemaining(Integer quantityRemaining) {
        this.quantityRemaining = quantityRemaining;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public Integer getMyQuantity() {
        return myQuantity;
    }

    public void setMyQuantity(Integer myQuantity) {
        this.myQuantity = myQuantity;
    }

    public Integer getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(Integer serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getBrandProductDetail() {
        return brandProductDetail;
    }

    public void setBrandProductDetail(String brandProductDetail) {
        this.brandProductDetail = brandProductDetail;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(Integer actualPrice) {
        this.actualPrice = actualPrice;
    }
}
