package com.beusalons.android.Model.SalonHome;

import com.beusalons.android.Model.HomePage.List_;

import java.util.List;

/**
 * Created by Ashish Sharma on 1/25/2018.
 */

public class Subscription {
    private String type;
    private String title;
    private String subscriptionCount;
    private List<List_> list;

    public String getSubscriptionCount() {
        return subscriptionCount;
    }

    public void setSubscriptionCount(String subscriptionCount) {
        this.subscriptionCount = subscriptionCount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<List_> getList() {
        return list;
    }

    public void setList(List<List_> list) {
        this.list = list;
    }
}
