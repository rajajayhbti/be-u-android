package com.beusalons.android.Model.AllDeals;

/**
 * Created by myMachine on 4/28/2017.
 */

public class Service_ {

    public Integer serviceCode;
    public String name;
    public String gender;

    public Integer getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(Integer serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
