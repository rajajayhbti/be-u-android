package com.beusalons.android.Model.SalonHome;

import com.beusalons.android.Model.newServiceDeals.Service;

import java.util.List;

/**
 * Created by myMachine on 5/26/2017.
 */

public class Data {


    private String name;
    private String parlorId;
    private List<Image> images = null;
    private String gender;
    private String address1;
    private String address2;
    private String landmark;
    private double rating;
    private Integer price;
    private String phoneNumber;
    private String realPhoneNumber;
    private Double latitude;
    private Double longitude;
    private String closingTime;
    private int dayClosed;
    private int parlorType;
    private String link;
    private Boolean favourite;
    private Boolean recent;
    private String openingTime;
    private Subscription subscriptions;
    private List<String> info = null;
    private List<Membership> memberships = null;

    private List<Service> departments = null;                   //yeh hi hai bhai locha naam ka
    private List<Integer> recentRatings;

    private String noOfAppointments;
    private String noOfReviews;

    private String departmentsString;
    private String brandsString;
    private boolean freeWifi;
    private String wifiName;
    private String wifiPassword;
    private String userFbFriendString;
    private String currentTime;
    private WelcomeOffer welcomeOffer;
    private boolean music;
    private String subscriptionPopUpText;

    public boolean isMusic() {
        return music;
    }

    public void setMusic(boolean music) {
        this.music = music;
    }

    public WelcomeOffer getWelcomeOffer() {
        return welcomeOffer;
    }

    public void setWelcomeOffer(WelcomeOffer welcomeOffer) {
        this.welcomeOffer = welcomeOffer;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    private List<ServicesAvailable> servicesAvailable = null;
    private float tax;

    public float getTax() {
        return tax;
    }

    public void setTax(float tax) {
        this.tax = tax;
    }

    public List<ServicesAvailable> getServicesAvailable() {
        return servicesAvailable;
    }

    public void setServicesAvailable(List<ServicesAvailable> servicesAvailable) {
        this.servicesAvailable = servicesAvailable;
    }

    public String getWifiName() {
        return wifiName;
    }

    public void setWifiName(String wifiName) {
        this.wifiName = wifiName;
    }

    public String getWifiPassword() {
        return wifiPassword;
    }

    public void setWifiPassword(String wifiPassword) {
        this.wifiPassword = wifiPassword;
    }

    public String getDepartmentsString() {
        return departmentsString;
    }

    public void setDepartmentsString(String departmentsString) {
        this.departmentsString = departmentsString;
    }

    public String getBrandsString() {
        return brandsString;
    }

    public void setBrandsString(String brandsString) {
        this.brandsString = brandsString;
    }

    public boolean isFreeWifi() {
        return freeWifi;
    }

    public void setFreeWifi(boolean freeWifi) {
        this.freeWifi = freeWifi;
    }

    public String getNoOfAppointments() {
        return noOfAppointments;
    }

    public void setNoOfAppointments(String noOfAppointments) {
        this.noOfAppointments = noOfAppointments;
    }

    public String getNoOfReviews() {
        return noOfReviews;
    }

    public void setNoOfReviews(String noOfReviews) {
        this.noOfReviews = noOfReviews;
    }

    public List<Integer> getRecentRatings() {
        return recentRatings;
    }

    public void setRecentRatings(List<Integer> recentRatings) {
        this.recentRatings = recentRatings;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(String closingTime) {
        this.closingTime = closingTime;
    }

    public int getDayClosed() {
        return dayClosed;
    }

    public void setDayClosed(int dayClosed) {
        this.dayClosed = dayClosed;
    }

    public String getRealPhoneNumber() {
        return realPhoneNumber;
    }

    public void setRealPhoneNumber(String realPhoneNumber) {
        this.realPhoneNumber = realPhoneNumber;
    }

    public int getParlorType() {
        return parlorType;
    }

    public void setParlorType(int parlorType) {
        this.parlorType = parlorType;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Boolean getFavourite() {
        return favourite;
    }

    public void setFavourite(Boolean favourite) {
        this.favourite = favourite;
    }

    public Boolean getRecent() {
        return recent;
    }

    public void setRecent(Boolean recent) {
        this.recent = recent;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public List<String> getInfo() {
        return info;
    }

    public void setInfo(List<String> info) {
        this.info = info;
    }

    public List<Membership> getMemberships() {
        return memberships;
    }

    public void setMemberships(List<Membership> memberships) {
        this.memberships = memberships;
    }

    public List<Service> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Service> departments) {
        this.departments = departments;
    }

    public String getUserFbFriendString() {
        return userFbFriendString;
    }

    public void setUserFbFriendString(String userFbFriendString) {
        this.userFbFriendString = userFbFriendString;
    }

    public Subscription getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(Subscription subscriptions) {
        this.subscriptions = subscriptions;
    }


}
