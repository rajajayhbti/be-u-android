package com.beusalons.android.Model.AllParlors;

import java.util.List;

/**
 * Created by myMachine on 2/9/2017.
 */

public class AllParlorResponse {


    public Boolean success;
    public List<ParlorData> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<ParlorData> getData() {
        return data;
    }

    public void setData(List<ParlorData> data) {
        this.data = data;
    }
}
