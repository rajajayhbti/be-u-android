package com.beusalons.android.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.R;
import com.beusalons.android.SubscriptionActivity;

/**
 * Created by myMachine on 23-Mar-18.
 */

public class ReferSubscriptionFragment extends DialogFragment {

    public static final String DATA= ReferSubscriptionFragment.class.getSimpleName()+".data";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);          //no action bar space
        String txt_="";
        Bundle bundle= getArguments();
        if(bundle!=null &&
                bundle.containsKey(DATA)){
            txt_= bundle.getString(DATA);
        }

        LinearLayout linear_= (LinearLayout) inflater.inflate(R.layout.fragment_refer_subs, container, false);

        WebView txt_detail ;
        TextView txt_refer;
        txt_detail= linear_.findViewById(R.id.txt_detail);
        txt_detail.loadData( txt_,"text/html", "UTF-8");

        txt_refer= linear_.findViewById(R.id.txt_refer);
        txt_refer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), SubscriptionActivity.class));
                dismiss();
            }
        });


        return linear_;
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

}
