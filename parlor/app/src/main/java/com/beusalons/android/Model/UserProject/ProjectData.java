package com.beusalons.android.Model.UserProject;

import com.beusalons.android.Model.ArtistProfile.CreativeField;
import com.beusalons.android.Model.Profile.Collec;
import com.beusalons.android.Model.Profile.Tag;

import java.util.List;


/**
 * Created by Ajay on 2/8/2018.
 */

public class ProjectData {
    private List<Object> comments = null;
    private String id;
    private String artistId;
    private String date;
    private String coverImage;
    private String postTitle;
    private List<String> images = null;
    private String collectionName;
    private List<Tag> tags = null;
    private List<CreativeField> creativeFields = null;
    private Collec collec;
    private Boolean likedStatus;
    private String artistName;
    private String totalLikes;

    public List<Object> getComments() {
        return comments;
    }

    public void setComments(List<Object> comments) {
        this.comments = comments;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<CreativeField> getCreativeFields() {
        return creativeFields;
    }

    public void setCreativeFields(List<CreativeField> creativeFields) {
        this.creativeFields = creativeFields;
    }

    public Collec getCollec() {
        return collec;
    }

    public void setCollec(Collec collec) {
        this.collec = collec;
    }

    public Boolean getLikedStatus() {
        return likedStatus;
    }

    public void setLikedStatus(Boolean likedStatus) {
        this.likedStatus = likedStatus;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        this.totalLikes = totalLikes;
    }
}
