package com.beusalons.android.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.BookingSummaryActivity;
import com.beusalons.android.CustomerReviewActivity;
import com.beusalons.android.DateTimeActivity;
import com.beusalons.android.Model.Appointments.PastData;
import com.beusalons.android.OrderSummaryActivity;
import com.beusalons.android.R;
import com.beusalons.android.WriteReview_Activity;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by myMachine on 11/16/2016.
 */

public class PastAppointmentsListAdapter extends RecyclerView.Adapter<PastAppointmentsListAdapter.PrApptsViewHolder> {

    private List<PastData> details;
    private Context context;

    public PastAppointmentsListAdapter(List<PastData> details, Context context){
        this.details= details;
        this.context= context;
    }

    public static class PrApptsViewHolder extends RecyclerView.ViewHolder{

        //booked salon detail
        private TextView salonName, salonAddress, salonDate, salonTime;
        private LinearLayout review, linear_reorder, linear_details;

        public PrApptsViewHolder(View view) {
            super(view);
            salonName=(TextView)view.findViewById(R.id.txt_prApt_salonName);
            salonAddress=(TextView)view.findViewById(R.id.txt_prApt_salonAddress);
            salonDate=(TextView)view.findViewById(R.id.txt_prApt_salonDate);
            salonTime=(TextView)view.findViewById(R.id.txt_prApt_salonTime);
            review= (LinearLayout)view.findViewById(R.id.btn_previous_appt_review);
            linear_reorder= (LinearLayout)view.findViewById(R.id.linear_reorder);
            linear_details= (LinearLayout)view.findViewById(R.id.linear_details);
        }
    }

    @Override
    public PrApptsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.past_appointments_cardview, parent, false);
        return new PrApptsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PrApptsViewHolder holder, final int position) {

        final PastData data= details.get(position);

        holder.linear_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle= new Bundle();
                bundle.putString(OrderSummaryActivity.COMPLETED_APPOINTMENT, new Gson().toJson(data));
                Intent intent= new Intent(view.getContext(), OrderSummaryActivity.class);
                intent.putExtras(bundle);
                view.getContext().startActivity(intent);
            }
        });

        holder.review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle= new Bundle();
                bundle.putString(CustomerReviewActivity.PARLOR_NAME, data.getParlorName());
                bundle.putString(CustomerReviewActivity.PARLOR_ID, data.getParlorId());
                bundle.putString(CustomerReviewActivity.APPOINTMENT_ID, data.getAppointmentId());
                Intent intent= new Intent(view.getContext(), CustomerReviewActivity.class);
                intent.putExtras(bundle);
                view.getContext().startActivity(intent);
            }
        });

        holder.linear_reorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent  intent1= new Intent(v.getContext(), DateTimeActivity.class);
                intent1.putExtra("apptId",data.getAppointmentId());
                intent1.putExtra("openingTime", data.getOpeningTime());
                intent1.putExtra("closingTime", data.getClosingTime());
                v.getContext().startActivity(intent1);

            }
        });

        Log.i("tagged", " value in upcomi data: "+data.getParlorName());

        holder.salonName.setText(data.getParlorName());
        holder.salonAddress.setText(data.getParlorAddress());
        holder.salonDate.setText(data.getDate());
        holder.salonTime.setText(data.getTime());
    }

    @Override
    public int getItemCount() {
        return details.size();
    }
}
