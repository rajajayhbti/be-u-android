package com.beusalons.android.Fragment;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beusalons.android.Adapter.UpcomingApptListAdapter;
import com.beusalons.android.Event.BookingSummaryPromoEvent;
import com.beusalons.android.Event.CancelAppointment;
import com.beusalons.android.Model.Appointments.CancelAppointMentsPost;
import com.beusalons.android.Model.Appointments.Services;
import com.beusalons.android.Model.Appointments.UpcomingAppointmentsPost;
import com.beusalons.android.Model.Appointments.UpcomingAppointmentsResponse;
import com.beusalons.android.Model.Appointments.UpcomingData;
import com.beusalons.android.Model.ServerTime;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class UpcomingAppointments_Fragment extends Fragment {


    public UpcomingAppointments_Fragment() {
        // Required empty public constructor
    }

    private static final String TAG= UpcomingAppointments_Fragment.class.getSimpleName();

    private List<UpcomingData> details= new ArrayList<>();

//    private ProgressDialog progress;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private View mContentView;
    private View mLoadingView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_upcoming__appointment, container, false);


        mContentView= view.findViewById(R.id.linear_upcoming_appointments);
        mLoadingView= view.findViewById(R.id.loading_spinner_upcoming_appointments);

        // Initially hide the content view.
        //  mContentView.setVisibility(View.GONE);

        recyclerView= (RecyclerView)view.findViewById(R.id.rcyView_upAppt);
        layoutManager= new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);



        //progress dialog
//        progress= DialogSpinner.progressDialog(getActivity(), "Loading..");
//        progress.show();

        adapter= new UpcomingApptListAdapter(details, getActivity());
        recyclerView.setAdapter(adapter);

        fetchData();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void fetchData(){

        mContentView.setVisibility(View.GONE);
        mLoadingView.setVisibility(View.VISIBLE);

        details.clear();
        UpcomingAppointmentsPost upcomingAppointmentsPost= new UpcomingAppointmentsPost();
        upcomingAppointmentsPost.setUserId(BeuSalonsSharedPrefrence.getUserId());
        upcomingAppointmentsPost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());

        Log.i(TAG, "userId: "+BeuSalonsSharedPrefrence.getUserId()+"  accessToken: "+  BeuSalonsSharedPrefrence.getAccessToken()+
                " phoneNumber: "+BeuSalonsSharedPrefrence.getPhoneNumber() + " email: "+ BeuSalonsSharedPrefrence.getUserEmail());

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<UpcomingAppointmentsResponse> call= apiInterface.upcomingAppts(upcomingAppointmentsPost);
        call.enqueue(new Callback<UpcomingAppointmentsResponse>() {
            @Override
            public void onResponse(Call<UpcomingAppointmentsResponse> call, Response<UpcomingAppointmentsResponse> response) {

                Log.i(TAG, "I'm in onResponse");

                if(response.isSuccessful()){

                    if(response.body().getSuccess()){

                        Log.i(TAG, "I'm in the success");

                        serverTime(response.body());

                    }else{
                        Log.i(TAG, "I'm in unsuccessful");
                    }

                }else{
                    Log.i(TAG, "I'm in the response else");
                }


//                progress.dismiss();

            }

            @Override
            public void onFailure(Call<UpcomingAppointmentsResponse> call, Throwable t) {
//                progress.dismiss();

                if(getActivity()!=null){
                    getActivity().finish();
                }
                Log.i(TAG, "I'm in onFailure: "+ t.getMessage()+ "  "+t.getStackTrace()+"   "+t.getCause());
            }
        });
    }

    private void serverTime(final UpcomingAppointmentsResponse appointmentsResponse){

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<ServerTime> call= apiInterface.getServerTime();
        call.enqueue(new Callback<ServerTime>() {
            @Override
            public void onResponse(Call<ServerTime> call, Response<ServerTime> response) {

                if(response.isSuccessful()){

                    if(response.body().isSuccess()){
                        Log.i("servertime", "in the success");

                        SimpleDateFormat source= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                        SimpleDateFormat destDate= new SimpleDateFormat("dd/MM/yy");
                        SimpleDateFormat destTime= new SimpleDateFormat("hh:mm a");

                        for(int i=0; i<appointmentsResponse.getData().size();i++){
                            UpcomingData data= new UpcomingData();
                            data.setParlorName(appointmentsResponse.getData().get(i).getParlorName());
                            data.setParlorAddress(appointmentsResponse.getData().get(i).getParlorAddress());
                            data.setParlorLatitude(appointmentsResponse.getData().get(i).getParlorLatitude());
                            data.setParlorLongitude(appointmentsResponse.getData().get(i).getParlorLongitude());
                            data.setOpeningTime(appointmentsResponse.getData().get(i).getOpeningTime());
                            data.setClosingTime(appointmentsResponse.getData().get(i).getClosingTime());
                            data.setAppointmentId(appointmentsResponse.getData().get(i).getAppointmentId());
                            data.setCurrentTime(response.body().getData());

                            //date
                            String stringDate= appointmentsResponse.getData().get(i).getStartsAt();

                            String formattedDate ="", formattedTime="";

                            try {
                                Date timestamp = source.parse(stringDate);

                                Calendar cal= Calendar.getInstance();       //creating calendar instance
                                cal.setTime(timestamp);
                                cal.add(Calendar.HOUR_OF_DAY, 5);
                                cal.add(Calendar.MINUTE, 30);

                                timestamp= cal.getTime();

                                formattedDate= destDate.format(timestamp);
                                formattedTime= destTime.format(timestamp);

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            int service_size= appointmentsResponse.getData().get(i).getServices().size();
                            List<Services> services_list= new ArrayList<>();
                            for(int j=0; j<service_size;j++){
                                Services services= new Services();
                                services.setName(appointmentsResponse.getData().get(i).getServices().get(j).getName());
                                services.setPrice(appointmentsResponse.getData().get(i).getServices().get(j).getPrice());
                                services_list.add(services);
                            }
                            data.setServices(services_list);

                            Log.i(TAG, "data in date: "+ formattedDate+ " data in time: "+ formattedTime);
                            data.setDate(formattedDate);
                            data.setTime(formattedTime);
                            data.setSubtotal(appointmentsResponse.getData().get(i).getSubtotal());
                            data.setDiscount(appointmentsResponse.getData().get(i).getDiscount());
                            data.setTax(appointmentsResponse.getData().get(i).getTax());
                            data.setPayableAmount(appointmentsResponse.getData().get(i).getPayableAmount());
                            data.setParlorId(appointmentsResponse.getData().get(i).getParlorId());
                            data.setStartsAt(formatDateTime(appointmentsResponse.getData().get(i).getStartsAt()));

                            details.add(data);
                        }
                        adapter.notifyDataSetChanged();




                        mContentView.setVisibility(View.VISIBLE);
                        mLoadingView.setVisibility(View.GONE);


                    }else{
                        Log.i("servertime", "in the unsuccessful");
                    }


                }else{
                    Log.i("servertime", "in the response else");
                }


            }

            @Override
            public void onFailure(Call<ServerTime> call, Throwable t) {
                Log.i("servertime", "i'm in failure: "+ t.getCause()+ " "+ t.getStackTrace());
            }
        });

    }

    @org.greenrobot.eventbus.Subscribe(threadMode = ThreadMode.MAIN)
    public void onCancelAppointMent(CancelAppointment event) {

        Log.i("cancellation", "i'm in the event bus method ");
        fetchData();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private String formatDateTime(String str_date){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        Date date_format=null;
        try {
            date_format = sdf.parse(str_date);
            Calendar cal= Calendar.getInstance();       //creating calendar instance
            cal.setTime(date_format);
            cal.add(Calendar.HOUR_OF_DAY, 5);
            cal.add(Calendar.MINUTE, 30);

            date_format= cal.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("MMM dd yyyy, EEEE KK:mm aa").format(date_format);
    }
}
