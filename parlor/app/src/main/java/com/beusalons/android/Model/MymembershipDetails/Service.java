package com.beusalons.android.Model.MymembershipDetails;

/**
 * Created by Ajay on 11/9/2017.
 */

public class Service {

    private String serviceId;
    private String serviceCode;
    private String name;

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
}
