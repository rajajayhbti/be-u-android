package com.beusalons.android.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.DealsServicesActivity;
import com.beusalons.android.Dialog.ShowDetailsDialogue;
import com.beusalons.android.Event.NewDealEvent.DealComboEvent;
import com.beusalons.android.Fragment.DealFragment.ComboBottomSheet;
import com.beusalons.android.Fragment.DealFragment.NewComboBottomSheet;
import com.beusalons.android.Fragment.DialogFragmentServices;
import com.beusalons.android.Fragment.UserCartFragment;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.DealsServices.DealService;
import com.beusalons.android.Model.Share.ShareDealService;
import com.beusalons.android.Model.UserCart.PackageService;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.newServiceDeals.NewCombo.Selector;
import com.beusalons.android.R;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.michael.easydialog.EasyDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ajay on 6/9/2017.
 */

public class DealsAdapter extends RecyclerView.Adapter<DealsAdapter.ViewHolder> {

    private Context context;
    private List<DealService> list;
    AppEventsLogger logger;

    private FirebaseAnalytics mFirebaseAnalytics;
    private String  gen;
    public DealsAdapter(Context context, List<DealService> list,String gender){

        EventBus.getDefault().register(this);

        this.context= context;
        this.list= list;
        logger = AppEventsLogger.newLogger(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        this.gen=gender;

    }

    @Override
    public void onBindViewHolder(final DealsAdapter.ViewHolder holder, final int position) {

        final int pos= position;

        holder.txt_name.setText(list.get(pos).getName());

        if(list.get(pos).getQuantity()>0){

            holder.txt_quantity.setText(""+list.get(pos).getQuantity());
            holder.linear_add_.setVisibility(View.GONE);
            holder.linear_add_remove.setVisibility(View.VISIBLE);
        }else{

            holder.linear_add_.setVisibility(View.VISIBLE);
            holder.linear_add_remove.setVisibility(View.GONE);
        }

        holder.txt_menu_price.setText("Starting from "+AppConstant.CURRENCY+list.get(pos).getDealPrice());
        if ( list.get(pos).isAllHairLength()){
            holder.txt_heir_length.setVisibility(View.VISIBLE);
        }else  holder.txt_heir_length.setVisibility(View.GONE);

        holder.linear_layout_show_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logShowDetailInDealEvent();
                logShowDetailInDealFireBaseEvent();
                logDealInfoEvent(list.get(pos).getName());
                new ShowDetailsDialogue((Activity) context,list.get(pos).getName(),list.get(pos).getDescription());
            }
        });
        holder.txt_save_per.setText(AppConstant.SAVE+" "+
                (100 -((list.get(pos).getDealPrice()*100)/list.get(pos).getMenuPrice())+"%"));


        holder.txt_description.setText(list.get(pos).getDescription()+"...");

        if (list.get(pos).getDealType().equals("combo")|| list.get(pos).getDealType().equals("newCombo")){
            holder.img_.setImageResource(R.drawable.ic_package);
        }
        else{
            holder.img_.setImageResource(R.drawable.ic_deal_icon);
        }

        if (position%4==2){
            holder.linearLayoutTop.setBackgroundColor(context.getResources().getColor(R.color.blue_DARK));
            holder.linear_add_.setBackgroundResource(R.drawable.drawable_blue);

        }
        else if (position%4==0){
            holder.linearLayoutTop.setBackgroundColor(context.getResources().getColor(R.color.pink_red));
            holder.linear_add_.setBackgroundResource(R.drawable.drawable_pink);
        }
        else if (position%4==1){
            holder.linearLayoutTop.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.linear_add_.setBackgroundResource(R.drawable.linear_add_btn);

        }
        else if (position%4==3){
            holder.linearLayoutTop.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            holder.linear_add_.setBackgroundResource(R.drawable.linear_add_btn);
        }

        holder.linear_add_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addStuff(pos, v);
            }
        });

        holder.linear_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addStuff(pos, v);
            }
        });

        holder.linear_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UserCartFragment fragment= new UserCartFragment();
                Bundle bundle= new Bundle();
                bundle.putBoolean("has_data", true);
                fragment.setArguments(bundle);
                fragment.show(((DealsServicesActivity)context).getSupportFragmentManager(), "user_cart");
            }
        });

        holder.img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                logDealShareEvent(list.get(pos).getName());
                EventBus.getDefault().post(new ShareDealService(list.get(pos).getDealId(), pos));
            }
        });




        if(list.get(pos).getParlorTypes()!=null &&
                list.get(pos).getParlorTypes().size()>0){

           /* holder.linear_types.setVisibility(View.VISIBLE);
            holder.linear_types.removeAllViews();*/

            for(int i=0 ;i<list.get(pos).getParlorTypes().size();i++){
                //    LinearLayout salon_types= (LinearLayout)LayoutInflater.from(holder.linear_types.getContext())
                //              .inflate(R.layout.row_salon_type, null);
                //       TextView save=(TextView) salon_types.findViewById(R.id.save_);
                //        TextView txt_menu_price=(TextView) salon_types.findViewById(R.id.txt_menu_price);
                //         ImageView parlor_img=salon_types.findViewById(R.id.img_parlor);
                //        View vv=(View)salon_types.findViewById(R.id.vv_view);
                //          save.setText("Save "+list.get(pos).getParlorTypes().get(i).getSave()+"%");
                //         txt_menu_price.setText(AppConstant.CURRENCY+list.get(pos).getParlorTypes().get(i).getStartAt());


             /*   if (i==list.get(pos).getParlorTypes().size()-1){
                    vv.setVisibility(View.GONE);
                }else   vv.setVisibility(View.VISIBLE);*/

                if(list.get(pos).getParlorTypes().get(i).getType()==0){
//                    parlor_img.setImageResource(R.drawable.ic_salon_red);
                    if (list.get(pos).getParlorTypes().get(i).getStartAt()>0){
                        holder.txt_save_red.setVisibility(View.VISIBLE);
                        holder.txt_red_price.setVisibility(View.VISIBLE);
                        holder.view_red_.setVisibility(View.GONE);
                        holder.txt_save_red.setText("Save "+list.get(pos).getParlorTypes().get(i).getSave()+"%");
                        holder.txt_red_price.setText(AppConstant.CURRENCY+list.get(pos).getParlorTypes().get(i).getStartAt());
                    }


                }
                else if(list.get(pos).getParlorTypes().get(i).getType()==1){
//                    parlor_img.setImageResource(R.drawable.ic_salon_blue);
                    if (list.get(pos).getParlorTypes().get(i).getStartAt()>0){
                    holder.txt_save_blue.setVisibility(View.VISIBLE);
                    holder.txt_price_blue.setVisibility(View.VISIBLE);
                    holder.view_blue_.setVisibility(View.GONE);
                    holder.txt_save_blue.setText("Save "+list.get(pos).getParlorTypes().get(i).getSave()+"%");
                    holder.txt_price_blue.setText(AppConstant.CURRENCY+list.get(pos).getParlorTypes().get(i).getStartAt());}
                }
                else if(list.get(pos).getParlorTypes().get(i).getType()==2){
                    if (list.get(pos).getParlorTypes().get(i).getStartAt()>0){
                    holder.txt_save_green.setVisibility(View.VISIBLE);
                    holder.txt_price_green.setVisibility(View.VISIBLE);
                    holder.view_green_.setVisibility(View.GONE);
                    holder.txt_save_green.setText("Save "+list.get(pos).getParlorTypes().get(i).getSave()+"%");
                    holder.txt_price_green.setText(AppConstant.CURRENCY+list.get(pos).getParlorTypes().get(i).getStartAt());}
                }
                //   parlor_img.setImageResource(R.drawable.ic_salon_green);

                //  holder.linear_types.addView(salon_types);*/
            }

            holder.linear_types.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setToolTip(v.getContext(), holder.linear_types);
                }
            });

        }else{
            holder.linear_types.setVisibility(View.GONE);
        }

    }

    private void addStuff(int pos, View view){

        logViewedContentEvent(list.get(pos).getName(),"deal","INR");



           if(list.get(pos).getDealType().equalsIgnoreCase("combo") ||
                list.get(pos).getDealType().equalsIgnoreCase("dealPrice") ||
                list.get(pos).getDealType().equalsIgnoreCase("chooseOne") ||
                list.get(pos).getDealType().equalsIgnoreCase("chooseOnePer")){

            if((list.get(pos).getSelectors().size()>0 &&
                    list.get(pos).getSelectors().get(0).getBrands()!=null &&
                    list.get(pos).getSelectors().get(0).getBrands().size()>0)){

                Log.i("dealss", "i'm in 1");
                showNewDialog(list.get(pos), view);             //brand-product-service

            }else if((list.get(pos).getSelectors().size()>1) ||
                    (list.get(pos).getSelectors().get(0).getServices()!=null &&
                            list.get(pos).getSelectors().get(0).getServices().size()>1) ||
                    (list.get(pos).getSelectors().get(0).getServices().get(0).getBrands()!=null &&
                            list.get(pos).getSelectors().get(0).getServices().get(0).getBrands().size()>1)){


                Log.i("dealss", "i'm in 2");


                if(list.get(pos).getDealType().equalsIgnoreCase("combo")){
                    logPackageBottomSheetOpenedEvent(list.get(pos).getName());

                    logDealBottomSheetOpenEvent();
                    logDealBottomSheetOpenFireBaseEvent();
                    ComboBottomSheet bottomSheet= new ComboBottomSheet();
                    Bundle bundle= new Bundle();
                    bundle.putString("combo_service", new Gson().toJson(list.get(pos), DealService.class));
                    bottomSheet.setArguments(bundle);
                    bottomSheet.show((((DealsServicesActivity)context)).getSupportFragmentManager(), "deal_combo");
                }else{

                    showNewDialog(list.get(pos), view);             //service-brand-product
                }

            }else{

                logDealServiceEvent(list.get(pos).getName()+"-"+gen);
                logDealServiceFireBaseEvent(list.get(pos).getName()+"-"+gen);
                Log.i("dealss", "i'm in 3");

                String brand_id="", product_id="";

                DealComboEvent services= new DealComboEvent();

                services.setService_name(list.get(pos).getName());

                services.setService_deal_id(list.get(pos).getParlorDealId());               //service ya deal id hogi isme
                services.setDealId(list.get(pos).getDealId());

                services.setType(list.get(pos).getDealType());
                services.setParlorTypes(list.get(pos).getParlorTypes());

                List<PackageService> packageList= new ArrayList<>();
                PackageService packageService= new PackageService();

                packageService.setService_name(list.get(pos).getSelectors().get(0).getServices().get(0).getName());
                packageService.setService_code(list.get(pos).getSelectors().get(0).getServices().get(0).getServiceCode());
                packageService.setService_id(list.get(pos).getSelectors().get(0).getServices().get(0).getServiceId());

                if(list.get(pos).getSelectors().get(0).getServices().get(0).getBrands()!=null &&
                        list.get(pos).getSelectors().get(0).getServices().get(0).getBrands().size()>0){

                    packageService.setBrand_name(list.get(pos).getSelectors().get(0).getServices().get(0).getBrands()
                            .get(0).getBrandName());
                    packageService.setBrand_id(list.get(pos).getSelectors().get(0).getServices().get(0).getBrands()
                            .get(0).getBrandId());

                    brand_id= packageService.getBrand_id();

                    if(list.get(pos).getSelectors().get(0).getServices().get(0).getBrands().get(0).getProducts()!=null &&
                            list.get(pos).getSelectors().get(0).getServices().get(0).getBrands().get(0).getProducts().size()>0){

                        packageService.setProduct_name(list.get(pos).getSelectors().get(0).getServices().get(0).getBrands()
                                .get(0).getProducts().get(0).getProductName());
                        packageService.setProduct_id(list.get(pos).getSelectors().get(0).getServices().get(0).getBrands()
                                .get(0).getProducts().get(0).getProductId());

                        product_id= packageService.getProduct_id();

                    }
                }
                packageList.add(packageService);

                services.setPackage_services_list(packageList);
                services.setDescription(list.get(pos).getDescription());

                Log.i("singleselector", "value in stuff: "+ list.get(pos).getSelectors().get(0).getServices().get(0).getServiceCode()+
                        "   "+   list.get(pos).getSelectors().get(0).getServices().get(0).getServiceId()+"   "+
                        brand_id+"   "+  product_id);

                services.setPrimary_key(""+list.get(pos).getSelectors().get(0).getServices().get(0).getServiceCode()+
                        list.get(pos).getSelectors().get(0).getServices().get(0).getServiceId()+brand_id+product_id);


                EventBus.getDefault().post(services);
            }

        }else if(list.get(pos).getDealType().equalsIgnoreCase("newCombo")){

            Log.i("dealss", "i'm in 3");
            logDealBottomSheetOpenEvent();
            logDealBottomSheetOpenFireBaseEvent();
            logPackageBottomSheetOpenedEvent(list.get(pos).getName());

            try{

                NewComboBottomSheet bottomSheet= new NewComboBottomSheet();
                Bundle bundle= new Bundle();
                bundle.putString(NewComboBottomSheet.NEW_COMBO, new Gson().toJson(list.get(pos)));
                bottomSheet.setArguments(bundle);
                bottomSheet.show((((DealsServicesActivity)context)).getSupportFragmentManager(), "deal_new_combo");
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }


    public void logDealInfoEvent (String dealName) {
        Log.e("logDealInfoEvent","fine");

        Bundle params = new Bundle();
        params.putString(AppConstant.DealName, dealName);
        logger.logEvent(AppConstant.DealInfo, params);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logDealShareEvent (String dealName) {
        Log.e("logDealShareEvent","fine");

        Bundle params = new Bundle();
        params.putString(AppConstant.DealName, dealName);
        logger.logEvent(AppConstant.DealShare, params);
    }
    private void showNewDialog(final DealService dealService, View view){

        PackageService package_service = new PackageService();
        package_service.setName(dealService.getName());
        package_service.setDescription(dealService.getShortDescription());
        package_service.setStartAt(dealService.getStartAt());
        package_service.setSave(dealService.getSave());

        if(dealService.getSelectors().get(0).getServices()!=null &&                 //service-brand-product
                dealService.getSelectors().get(0).getServices().size()>0){

            dealService.getSelectors().get(0).getServices().get(0).setCheck(true);

            package_service.setService_code(dealService.getSelectors().get(0)
                    .getServices().get(0).getServiceCode());
            package_service.setService_id(dealService.getSelectors().get(0).getServices().
                    get(0).getServiceId());
            package_service.setService_name(dealService.getSelectors().get(0).getServices().
                    get(0).getName());

            if(dealService.getSelectors().get(0).getServices().get(0).getBrands()!=null &&
                    dealService.getSelectors().get(0).getServices().get(0).getBrands().size()>0 ){

                //brand ka stuff-------------------------------------------------
                dealService.getSelectors().get(0).getServices().get(0).getBrands().get(0).setCheck(true);

                package_service.setBrand_id(dealService.getSelectors().get(0).getServices().
                        get(0).getBrands().get(0).getBrandId());
                package_service.setBrand_name(dealService.getSelectors().get(0).getServices().
                        get(0).getBrands().get(0).getBrandName());

                if(dealService.getSelectors().get(0).getServices().
                        get(0).getBrands().get(0).getProducts()!=null &&
                        dealService.getSelectors().get(0).getServices().
                                get(0).getBrands().get(0).getProducts().size()>0){

                    package_service.setProduct_id(dealService.getSelectors().get(0).getServices().
                            get(0).getBrands().get(0).getProducts().get(0).getProductId());
                    package_service.setProduct_name(dealService.getSelectors().get(0).getServices().
                            get(0).getBrands().get(0).getProducts().get(0).getProductName());

                    dealService.getSelectors().get(0).getServices().get(0).getBrands().get(0).
                            getProducts().get(0).setCheck(true);
                }
            }
        }else if(dealService.getSelectors().get(0).getBrands()!=null &&     //brand-product-service
                dealService.getSelectors().get(0).getBrands().size()>0){

            dealService.getSelectors().get(0).getBrands().get(0).setCheck(true);

            package_service.setBrand_id(dealService.getSelectors().get(0).getBrands().get(0).getBrandId());
            package_service.setBrand_name(dealService.getSelectors().get(0).getBrands().get(0).getBrandName());
            if(dealService.getSelectors().get(0).getBrands().get(0).getProducts()!=null &&
                    dealService.getSelectors().get(0).getBrands().get(0).getProducts().size()>0 ){

                //product id ka null check
                if( dealService.getSelectors().get(0).getBrands().get(0).getProducts().get(0).getProductId()!=null){
                    package_service.setProduct_id( dealService.getSelectors().get(0).getBrands().get(0).getProducts()
                            .get(0).getProductId());
                    package_service.setProduct_name( dealService.getSelectors().get(0).getBrands().get(0).getProducts()
                            .get(0).getProductName());
                }

                dealService.getSelectors().get(0).getBrands().get(0).getProducts().get(0).setCheck(true);

                if(dealService.getSelectors().get(0).getBrands().get(0).getProducts().get(0).getServices()!=null &&
                        dealService.getSelectors().get(0).getBrands().get(0).getProducts().get(0).getServices().size()>0){


                    dealService.getSelectors().get(0).getBrands().get(0).getProducts().get(0).getServices().get(0).setCheck(true);

                    package_service.setService_code(dealService.getSelectors().get(0).getBrands()
                            .get(0).getProducts().get(0).getServices().get(0).getServiceCode());
                    package_service.setService_id(dealService.getSelectors().get(0).getBrands().get(0).getProducts().get(0).getServices()
                            .get(0).getServiceId());
                    package_service.setService_name(dealService.getSelectors().get(0).getBrands().get(0).getProducts().get(0).getServices()
                            .get(0).getName());
                }
            }
        }

         logDealServicePopupOpenEvent(package_service.getName());

        logDealServiceEvent(package_service.getName()+"-"+gen);
        logDealServiceFireBaseEvent(package_service.getName()+"-"+gen);
        DialogFragmentServices fragment= new DialogFragmentServices();
        Bundle bundle= new Bundle();
        bundle.putString(DialogFragmentServices.SERVICE_DATA,
                new Gson().toJson(dealService.getSelectors().get(0), Selector.class));
        bundle.putString(DialogFragmentServices.DATA,
                new Gson().toJson(package_service, PackageService.class));
        bundle.putString("isService","true");

        fragment.setArguments(bundle);
        fragment.show(((Activity)view.getContext()).getFragmentManager() ,
                DialogFragmentServices.DIALOG);
        fragment.setListener(new DialogFragmentServices.FragmentListener() {
            @Override
            public void onClick(PackageService packageService) {
                Log.i("humkahahai", "int the gharh");
                addServices(dealService, packageService);
            }
        });

    }

    private void addServices(DealService dealService, PackageService packageService){

        DealComboEvent event= new DealComboEvent();

        event.setService_deal_id(""+dealService.getParlorDealId());             //isme string hai
        event.setDealId(dealService.getDealId());                       //isme ha like 140, 156..    yeh important hai

        event.setType(dealService.getDealType());
        event.setParlorTypes(dealService.getParlorTypes());

        String primary_key="";
        String service_name="",brand_name="", product_name= "";

        //adding the package service
        List<PackageService> package_services_list= new ArrayList<>();
        package_services_list.add(packageService);

        for(int i=0;i<package_services_list.size();i++){

            int service_code= package_services_list.get(i).getService_code();
            String service_id= package_services_list.get(i).getService_id();

            service_name= package_services_list.get(i).getService_name();
            brand_name= package_services_list.get(i).getBrand_name()==null?
                    "":package_services_list.get(i).getBrand_name();
            product_name=package_services_list.get(i).getProduct_name()==null?
                    "":package_services_list.get(i).getProduct_name();

            String brand_id= package_services_list.get(i).getBrand_id()==null?
                    "":package_services_list.get(i).getBrand_id();
            String product_id=package_services_list.get(i).getProduct_id()==null?
                    "":package_services_list.get(i).getProduct_id();
            primary_key+= ""+service_code+ service_id+ brand_id+product_id;

            Log.i("primary_key", "value in: "+ service_code+
                    "  " + service_id+  " "+ brand_id+  " "+ product_id);
        }
        event.setPrimary_key(primary_key);
        Log.i("primary_key", "values: "+primary_key);

        event.setService_name(service_name);

        if(product_name.equalsIgnoreCase("")){
            event.setDescription(brand_name);
        }else
            event.setDescription(brand_name+" - "+product_name);

        event.setPackage_services_list(package_services_list);

        EventBus.getDefault().post(event);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAddEvent(UserCart cart) {

        int deal_id= cart.getInt_deal_id();
        int quantity= 0;

        for(int i=0; i<list.size();i++){

            if(list.get(i).getDealId() == deal_id){
                for(int j=0; j<cart.getServicesList().size(); j++){

                    if(cart.getServicesList().get(j).getDealId() == deal_id)
                        quantity+= cart.getServicesList().get(j).getQuantity();
                }
                list.get(i).setQuantity(quantity);
            }
        }
        notifyDataSetChanged();
    }

    private void setToolTip(Context context, LinearLayout linear_types){
        View view = LayoutInflater.from(context).inflate(R.layout.salon_type_tooltip, null, false);
        TextView txt_premium= (TextView)view.findViewById(R.id.txt_premium);
        TextView txt_standard= (TextView)view.findViewById(R.id.txt_standard);
        TextView txt_budget= (TextView)view.findViewById(R.id.txt_budget);
        TextView txt_info= (TextView)view.findViewById(R.id.txt_info);

        TextView txt_brand_1= (TextView)view.findViewById(R.id.txt_brand_1);
        TextView txt_brand_2= (TextView)view.findViewById(R.id.txt_brand_2);
        TextView txt_brand_3= (TextView)view.findViewById(R.id.txt_brand_3);
        txt_info.setTypeface(null, Typeface.ITALIC);
        txt_brand_1.setTypeface(null, Typeface.ITALIC);
        txt_brand_2.setTypeface(null, Typeface.ITALIC);
        txt_brand_3.setTypeface(null, Typeface.ITALIC);

        final String price_red_false = "<font color='#3e780a'>₹₹₹₹</font>"+"<font color='#FFFFFF'>₹ - </font>"+
                "<font color='#3e780a'>₹₹₹₹₹</font>";
        final String price_blue_false = "<font color='#3e780a'>₹₹₹</font>"+"<font color='#FFFFFF'>₹₹ - </font>"+
                "<font color='#3e780a'>₹₹₹₹</font>"+"<font color='#FFFFFF'>₹</font></i>";
        final String price_green_false = "<font color='#3e780a'>₹₹</font>"+"<font color='#FFFFFF'>₹₹₹ - </font>"+
                "<font color='#3e780a'>₹₹₹</font>"+"<font color='#FFFFFF'>₹₹</font>";
        txt_premium.setText(fromHtml(price_red_false));
        txt_standard.setText(fromHtml(price_blue_false));
        txt_budget.setText(fromHtml(price_green_false));
/*
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(100,500 );
        view.setLayoutParams(params);*/
        new EasyDialog(context)
                // .setLayoutResourceId(R.layout.layout_tip_content_horizontal)//layout resource id
                .setLayout(view)
                .setBackgroundColor(context.getResources().getColor(R.color.tooltip_bg))
                // .setLocation(new location[])//point in screen
                .setLocationByAttachedView(linear_types)
                .setGravity(EasyDialog.GRAVITY_BOTTOM)
                .setAnimationTranslationShow(EasyDialog.DIRECTION_X, 1000, -600, 100, -50, 50, 0)
                .setAnimationAlphaShow(100, 0.3f, 1.0f)
                .setAnimationTranslationDismiss(EasyDialog.DIRECTION_X, 500, -50, 800)
                .setAnimationAlphaDismiss(200, 1.0f, 0.0f)
                .setTouchOutsideDismiss(true)
                .setOutsideColor(context.getResources().getColor(android.R.color.transparent))
                .setMarginLeftAndRight(100, 100)
                .show();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{


        private TextView txt_name, txt_save_per, txt_menu_price, txt_price,txt_quantity,
                txt_description,txt_heir_length,txt_save_red,txt_red_price,txt_save_blue,txt_price_blue,txt_save_green,txt_price_green;
        private LinearLayout linear_add, linear_add_, linear_remove,linearLayoutTop,linearLayout_description,
                linear_layout_show_detail, linear_add_remove, linear_types;
        private ImageView img_, img_share;
        private View view_red_,view_blue_,view_green_;

        public ViewHolder(View itemView) {
            super(itemView);

            txt_heir_length= (TextView)itemView.findViewById(R.id.txt_heir_length);
            txt_description= (TextView)itemView.findViewById(R.id.txt_description);
            txt_quantity= (TextView)itemView.findViewById(R.id.txt_quantity);
            txt_name= (TextView)itemView.findViewById(R.id.txt_name);
            txt_save_per= (TextView)itemView.findViewById(R.id.txt_save_per);
            txt_menu_price= (TextView)itemView.findViewById(R.id.txt_menu_price);
            txt_price= (TextView)itemView.findViewById(R.id.txt_price);

            linear_add_= (LinearLayout)itemView.findViewById(R.id.linear_add_);             //add button

            linear_add_remove= (LinearLayout)itemView.findViewById(R.id.linear_add_remove);
            linear_add= (LinearLayout) itemView.findViewById(R.id.linear_add);      //add items
            linear_remove= (LinearLayout)itemView.findViewById(R.id.linear_remove);         //remove items
            img_= (ImageView)itemView.findViewById(R.id.img_);
            img_share= (ImageView)itemView.findViewById(R.id.img_share);
            linearLayoutTop=(LinearLayout)itemView.findViewById(R.id.linearLayout_top);
            linearLayout_description=(LinearLayout)itemView.findViewById(R.id.linearLayout_description);
            linear_layout_show_detail=(LinearLayout)itemView.findViewById(R.id.linear_layout_show_detail);
            linear_types= (LinearLayout)itemView.findViewById(R.id.linear_types);
            txt_save_red=(TextView) itemView.findViewById(R.id.txt_save_red);
            txt_red_price=(TextView) itemView.findViewById(R.id.txt_red_price);
            txt_save_blue=(TextView) itemView.findViewById(R.id.txt_save_blue);
            txt_price_blue=(TextView) itemView.findViewById(R.id.txt_price_blue);
            txt_save_green=(TextView) itemView.findViewById(R.id.txt_save_green);
            txt_price_green=(TextView) itemView.findViewById(R.id.txt_price_green);
            view_red_=(View) itemView.findViewById(R.id.view_red_);
            view_blue_=(View) itemView.findViewById(R.id.view_blue_);
            view_green_=(View) itemView.findViewById(R.id.view_green_);

        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logDealServiceEvent (String name) {
        Log.e("deal without any","fine");
        Bundle params = new Bundle();
        params.putString("Name", name);
        logger.logEvent(AppConstant.DealService, params);
    }

    public void logViewedContentEvent (String contentType, String contentId, String currency) {
        Log.e("prefine","content view  type"+contentType+ " price");

        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, contentId);
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        logger.logEvent(AppEventsConstants.EVENT_NAME_VIEWED_CONTENT, params);
    }


    public void logAddedToCartEvent (String contentType, String currency) {
        Log.e("prefine","add  type"+contentType+ " price");

        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        logger.logEvent(AppEventsConstants.EVENT_NAME_ADDED_TO_CART, params);
    }
    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logPackageBottomSheetOpenedEvent (String packageName) {
        Log.e("logPackaeBshOpenedEvent","fine"+packageName);

        Bundle params = new Bundle();
        params.putString(AppConstant.PackageName, packageName);
        logger.logEvent(AppConstant.PackageBottomSheetOpened, params);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logDealBottomSheetOpenEvent () {
        Log.e("bottomsheet","fine");

        logger.logEvent(AppConstant.DealBottomSheetOpen);
    }


    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logShowDetailInDealEvent () {
        Log.e("showdetail","fine");
        logger.logEvent(AppConstant.ShowDetailInDeal);
    }


    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logDealServicePopupOpenEvent (String dealName) {
        Log.e("logDealsPopupOpenEvent","fine"+dealName);

        Bundle params = new Bundle();
        params.putString(AppConstant.DealName, dealName);
        logger.logEvent(AppConstant.DealServicePopupOpen, params);
    }

    public void logDealServiceFireBaseEvent (String name) {
        Log.e("deal without firebase","fine");
        Bundle params = new Bundle();
        params.putString("Name", name);
        mFirebaseAnalytics.logEvent(AppConstant.DealService, params);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logDealBottomSheetOpenFireBaseEvent () {
        Log.e("bottomsheetFire","fine");
        Bundle params = new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.DealBottomSheetOpen,params);
    }


    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logShowDetailInDealFireBaseEvent () {
        Log.e("showdetail firebase","fine");

        Bundle params = new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.ShowDetailInDeal,params);
    }

    @Override
    public DealsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.fragment_deals_specific_list, parent, false);
        return new DealsAdapter.ViewHolder(view);
    }

    @Override
    public int getItemCount() {

        if(list!=null && list.size()>0)
            return list.size();
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

}
