package com.beusalons.android.Task;

import android.content.Context;
import android.util.Log;

import com.beusalons.android.Event.MembershipEvent.Event;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.snappydb.DB;
import com.snappydb.DBFactory;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by myMachine on 5/31/2017.
 */

public class UserCartTask implements Runnable{

    private Context context;
    private UserCart cart;
    private UserServices services;
    private Boolean decrease_quantity, remove_service;

    public UserCartTask(Context context, UserCart cart, UserServices services, Boolean decrease_quantity, Boolean remove_service){

        this.context= context;
        this.cart= cart;
        this.services= services;
        this.decrease_quantity= decrease_quantity;
        this.remove_service= remove_service;
    }

    @Override
    public void run() {

        try{
            DB snappyDB= DBFactory.open(context);

            UserCart saved_cart= null;               //db mai jo saved cart hai
            if(snappyDB.exists(AppConstant.USER_CART_DB)){

                saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
                //carttype--- service, deal, memebership

                Log.i("cartstuff", "saved cart type: "+ saved_cart.getCartType());
                if(cart.getCartType()!=null &&
                        cart.getCartType().equalsIgnoreCase(AppConstant.SERVICE_TYPE)){

                    //deals ya membership already hai cart mai toh delete karo use

                    if(saved_cart.getCartType()!=null &&
                            saved_cart.getCartType().equalsIgnoreCase(AppConstant.OTHER_TYPE)){

                        saved_cart.setCartType(cart.getCartType());
                        saved_cart.setParlorId(cart.getParlorId());
                        saved_cart.setParlorName(cart.getParlorName());
                        saved_cart.setParlorType(cart.getParlorType());
                        saved_cart.setGender(cart.getGender());
                        saved_cart.setRating(cart.getRating());
                        saved_cart.setOpeningTime(cart.getOpeningTime());
                        saved_cart.setClosingTime(cart.getClosingTime());
                        saved_cart.setAddress1(cart.getAddress1());
                        saved_cart.setAddress2(cart.getAddress2());

                        services.setQuantity(1);                    //initially set the quanitity

                        saved_cart.getServicesList().add(services);

                    }else if(saved_cart.getCartType()!=null &&
                            saved_cart.getCartType().equalsIgnoreCase(AppConstant.DEAL_TYPE)){

                        saved_cart= cart;
                        saved_cart.getServicesList().clear();

                        services.setQuantity(1);                    //initially set the quanitity

                        saved_cart.getServicesList().add(services);
                    }else{

                        if(cart.getParlorId().equalsIgnoreCase(saved_cart.getParlorId())){           //same parlor id

                            boolean isPresent= false;                               //if not present add as a new service
                            for(int i=0; i<saved_cart.getServicesList().size();i++){

                                Log.i("cart_primary_key", "value: "+ services.getPrimary_key());

                                if(saved_cart.getServicesList().get(i).getPrimary_key()
                                        .equalsIgnoreCase(services.getPrimary_key())){

                                    isPresent= true;
                                    int quantity= saved_cart.getServicesList().get(i).getQuantity();

                                    if(remove_service){         //removing service fuck yeah

                                        saved_cart.getServicesList().remove(i);
                                    }else if(decrease_quantity){         //descrease quantity

                                        saved_cart.getServicesList().get(i).setQuantity(quantity-1);

                                        //if quantity less than 0 remove the fucking service
                                        if(saved_cart.getServicesList().get(i).getQuantity()==0 ||
                                                saved_cart.getServicesList().get(i).getQuantity()<0){

                                            saved_cart.getServicesList().remove(i);
                                        }
                                    }else{          //increase quantity

                                        if(saved_cart.getServicesList().get(i).isSubscription()){

                                        }else{

                                            saved_cart.getServicesList().get(i).setQuantity(quantity+1);
                                        }

                                    }

                                }
                            }
                            if(!isPresent){

                                services.setQuantity(1);
                                saved_cart.getServicesList().add(services);
                            }


                        }else{              //differnt parlor id

                            saved_cart= cart;                    //initially set the quanitity
                            saved_cart.getServicesList().clear();            //clearing items
                            services.setQuantity(1);

                            saved_cart.getServicesList().add(services);
                        }
                    }


                }else if(cart.getCartType()!=null &&
                        cart.getCartType().equalsIgnoreCase(AppConstant.DEAL_TYPE)){

                    if(saved_cart.getCartType()!=null &&
                            saved_cart.getCartType().equalsIgnoreCase(AppConstant.OTHER_TYPE)){

                        saved_cart.setCartType(cart.getCartType());

                        services.setQuantity(1);                    //initially set the quanitity
                        saved_cart.getServicesList().add(services);

                    }else if(saved_cart.getCartType()!=null &&
                    saved_cart.getCartType().equalsIgnoreCase(AppConstant.SERVICE_TYPE)) {

                        saved_cart = cart;
                        saved_cart.getServicesList().clear();

                        services.setQuantity(1);                    //initially set the quanitity
                        saved_cart.getServicesList().add(services);
                    }else{

                        boolean isPresent= false;
                        for(int i=0; i<saved_cart.getServicesList().size();i++){

                            if(saved_cart.getServicesList().get(i).getPrimary_key()
                                    .equalsIgnoreCase(services.getPrimary_key())){

                                isPresent= true;
                                int quantity= saved_cart.getServicesList().get(i).getQuantity();

                                if(decrease_quantity){         //removing stuff

                                    saved_cart.getServicesList().get(i).setQuantity(quantity-1);

                                    //if quantity less than 0 remove the fucker
                                    if(saved_cart.getServicesList().get(i).getQuantity()==0 ||
                                            saved_cart.getServicesList().get(i).getQuantity()<0){

                                        saved_cart.getServicesList().remove(i);
                                    }
                                }else if(saved_cart.getServicesList().get(i).getType().equalsIgnoreCase("deal"))
                                    Log.i("yehtohdeal", "yea toh deal hai");
                                else{          //adding stuff

                                    saved_cart.getServicesList().get(i).setQuantity(quantity+1);
                                }
                            }
                        }

                        if(!isPresent){

                            services.setQuantity(1);
                            saved_cart.getServicesList().add(services);
                        }
                    }
                }else if(cart.getCartType()!=null &&
                        cart.getCartType().equalsIgnoreCase(AppConstant.OTHER_TYPE)) {

                    //service ya deal already hai cart mai toh delete karo use
//                    if(saved_cart.getCartType().equalsIgnoreCase(AppConstant.SERVICE_TYPE) ||
//                            saved_cart.getCartType().equalsIgnoreCase(AppConstant.DEAL_TYPE)){
//
//                        saved_cart= cart;
//                        saved_cart.getServicesList().clear();
//                        services.setQuantity(1);                    //initially set the quanitity
//                        saved_cart.getServicesList().add(services);
//                    }else{

//                     }

                    boolean isPresent= false;
                    for(int i=0; i<saved_cart.getServicesList().size();i++){

                        if(saved_cart.getServicesList().get(i).getPrimary_key()
                                .equalsIgnoreCase(services.getPrimary_key())){

                            isPresent= true;
                            int quantity= saved_cart.getServicesList().get(i).getQuantity();

                            if(decrease_quantity){         //removing stuff

                                saved_cart.getServicesList().get(i).setQuantity(quantity-1);

                                //if quantity less than 0 remove the fucker
                                if(saved_cart.getServicesList().get(i).getQuantity()==0 ||
                                        saved_cart.getServicesList().get(i).getQuantity()<0){

                                    saved_cart.getServicesList().remove(i);
                                }
                            }else{          //adding stuff nai karna isme

                                EventBus.getDefault().post(new Event(services.getName(), true));
                            }
                        }
                    }
                    if(!isPresent){

                        services.setQuantity(1);
                        saved_cart.getServicesList().add(services);
                        EventBus.getDefault().post(new Event(services.getName(), false));
                    }
                }
            }else{

                saved_cart= cart;
                services.setQuantity(1);
                saved_cart.getServicesList().add(services);

                EventBus.getDefault().post(new Event(services.getName(), false));
            }

            //quantity ke liye
            saved_cart.setDeal_id(services.getService_deal_id());
            saved_cart.setService_id(services.getService_id());
            saved_cart.setInt_deal_id(services.getDealId());

            snappyDB.put(AppConstant.USER_CART_DB, saved_cart);
            snappyDB.close();
            EventBus.getDefault().post(saved_cart);

        }catch (Exception e){

            e.printStackTrace();
        }
    }
}
