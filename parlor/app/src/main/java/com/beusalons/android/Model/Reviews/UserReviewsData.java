package com.beusalons.android.Model.Reviews;

/**
 * Created by myMachine on 12/21/2016.
 */

public class UserReviewsData {

    private String name;
    private String address;
    private String parlorId;
    private String reviewedTime;
    private Integer ratingByUser;
    private String reviewContent;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }

    public String getReviewedTime() {
        return reviewedTime;
    }

    public void setReviewedTime(String reviewedTime) {
        this.reviewedTime = reviewedTime;
    }

    public Integer getRatingByUser() {
        return ratingByUser;
    }

    public void setRatingByUser(Integer ratingByUser) {
        this.ratingByUser = ratingByUser;
    }

    public String getReviewContent() {
        return reviewContent;
    }

    public void setReviewContent(String reviewContent) {
        this.reviewContent = reviewContent;
    }
}
