package com.beusalons.android;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Adapter.MemberShipAdapter;
import com.beusalons.android.Event.MembershipEvent.Event;
import com.beusalons.android.Fragment.UserCartFragment;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.MemberShip.MemberShip_Response;
import com.beusalons.android.Model.MemberShip.Membership;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.Utility;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Ajay on 5/20/2017.
 */

public class MemberShipCardAcitvity extends AppCompatActivity {

    Unbinder unbinder;

    @BindView(R.id.recycler_view_membership)
    RecyclerView recyclerViewMember;

    @BindView(R.id.coordinator_)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.loading_for_membership)
    View loading_for_membership;
    @BindView(R.id.txt_cart)
    TextView txt_cart;

    @BindView(R.id.txt_proceed)
    TextView txt_proceed;

    @BindView(R.id.relative_membership)
    RelativeLayout relative_membership;
    private MemberShipAdapter memberShipAdapter;
    private List<Membership> membershipList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.member_ship_activity);
        unbinder= ButterKnife.bind(this);

        Bundle bundle= getIntent().getExtras();

        if(bundle!=null && bundle.containsKey("from_home")){

            txt_proceed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final View view_= v;
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            try {

                                UserCart saved_cart= null;               //db mai jo saved cart hai
                                DB snappyDB = DBFactory.open(view_.getContext());
                                if(snappyDB.exists(AppConstant.USER_CART_DB))
                                    saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);

                                if(saved_cart.getCartType()!=null &&
                                        saved_cart.getCartType().equalsIgnoreCase(AppConstant.DEAL_TYPE)){

                                    for(int i=0;i<saved_cart.getServicesList().size();i++){
                                        if(!saved_cart.getServicesList().get(i).isMembership()){
                                            saved_cart.getServicesList().remove(i);
                                        }
                                    }
                                    snappyDB.put(AppConstant.USER_CART_DB, saved_cart);
                                }

                                snappyDB.close();

                                if(saved_cart.getServicesList().size()>0){
                                    startActivity(new Intent(MemberShipCardAcitvity.this, BookingSummaryActivity.class));
                                }else{

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(MemberShipCardAcitvity.this, "Please Add Membership", Toast.LENGTH_SHORT).show();

                                        }
                                    });

                                }


                            }catch (SnappydbException e){
                                e.printStackTrace();
                            }
                        }
                    }).start();



                }
            });
        }else{

            txt_proceed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    finish();
                }
            });
        }

        setToolBar();
        inItView();
    }

    private void  inItView(){

        memberShipAdapter=new MemberShipAdapter(MemberShipCardAcitvity.this,membershipList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerViewMember.setLayoutManager(mLayoutManager);
        recyclerViewMember.setAdapter(memberShipAdapter);
        loading_for_membership=(View)findViewById(R.id.loading_for_membership);
        txt_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UserCartFragment fragment= new UserCartFragment();
                Bundle bundle= new Bundle();
                bundle.putBoolean("has_data", true);
                bundle.putBoolean(UserCartFragment.BTN_PROCEED, true);
                fragment.setArguments(bundle);
                fragment.show(getSupportFragmentManager(), "user_cart");
            }
        });


        fetchData();
    }

    private void fetchData(){
        loading_for_membership.setVisibility(View.VISIBLE);
        relative_membership.setVisibility(View.GONE);
        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface=retrofit.create(ApiInterface.class);
        Call<MemberShip_Response> call=apiInterface.getMemberShipData(ServiceGenerator.BASE_URL+"api/memberships");
        call.enqueue(new Callback<MemberShip_Response>() {
            @Override
            public void onResponse(Call<MemberShip_Response> call, Response<MemberShip_Response> response) {


                if(response.isSuccessful()){

                    if(response.body().getSuccess()){

                        membershipList.clear();
                        membershipList= response.body().getData().getMemberships();
                        memberShipAdapter.updateList(membershipList);
                    }else{

                        Log.e("memebershipka" , "on success else pe");
                        //Todo: handle karo
                    }

                    if(response.body().getData().getWelcomeOffer()!=null &&
                            response.body().getData().getWelcomeOffer().getWelcomeOffer()!=null &&
                            !response.body().getData().getWelcomeOffer().getWelcomeOffer().equalsIgnoreCase("")){
                        WebView webView_welcome= (WebView)findViewById(R.id.webView_welcome);
                        webView_welcome.setVisibility(View.VISIBLE);
                        webView_welcome.getSettings();
                        webView_welcome.loadData(response.body().getData().getWelcomeOffer().
                                getWelcomeOffer(), "text/html","utf-8");
                        webView_welcome.setBackgroundColor(0);

                    }

                    loading_for_membership.setVisibility(View.GONE);
                    relative_membership.setVisibility(View.VISIBLE);



                }else{
                    Log.e("memebershipka" , "on retrofit else pe");
                    //Todo: handle karo
                }
            }

            @Override
            public void onFailure(Call<MemberShip_Response> call, Throwable t) {

                Log.e("failure","onFailure :"+t.getMessage()+  "  "+ t.getStackTrace()+ " "+ t.getCause());
            }
        });
    }

    //normal service ke case mai yeh hai bhai
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void addServiceEvent(Event event) {

        if (event.isSame_service()){      //duplicate so show dialog

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(fromHtml(event.getName()+ " already added to cart!"));
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    dialog.dismiss();
                }
            }).show();

        }else{      //no membership of same type added to cart to simply add and show snackbar

            showSnackbar(event.getName());
        }
    }

    public void showSnackbar(String service_name){
        Snackbar snackbar = Snackbar.make( coordinatorLayout,
                fromHtml(service_name+" Service Added To Cart!"), Snackbar.LENGTH_LONG);

        //setting the snackbar action button text size
        View view = snackbar.getView();
        TextView txt_action = (TextView) view.findViewById(android.support.design.R.id.snackbar_action);
        TextView txt_text = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txt_action.setTextSize(13);
        txt_action.setAllCaps(false);
        txt_text.setTextSize(13);
        snackbar.setActionTextColor(ContextCompat.getColor(MemberShipCardAcitvity.this, R.color.snackbar));

        snackbar.setAction("View Cart", new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UserCartFragment fragment= new UserCartFragment();
                Bundle bundle= new Bundle();
                bundle.putBoolean("has_data", true);
                bundle.putBoolean(UserCartFragment.BTN_PROCEED, true);
                fragment.setArguments(bundle);
                fragment.show(getSupportFragmentManager(), "user_cart");
            }
        });
        snackbar.show();
    }



    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(getResources().getString(R.string.family_wallet));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }
}
