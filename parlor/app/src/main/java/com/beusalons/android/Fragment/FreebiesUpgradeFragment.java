package com.beusalons.android.Fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.beusalons.android.Event.FreebieUpgradeEvent;
import com.beusalons.android.Model.Loyalty.FreeCorporateService;
import com.beusalons.android.Model.Loyalty.UpgradePost;
import com.beusalons.android.Model.Loyalty.UpgradeResponse;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Upgrade;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by myMachine on 8/18/2017.
 */

public class FreebiesUpgradeFragment extends DialogFragment {

    private FreeCorporateService data= new FreeCorporateService();
    private int upgrade_id;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);          //no action bar space
        View view= inflater.inflate(R.layout.fragment_freebies_upgrade, null, false);

        Bundle bundle= getArguments();
        if (bundle != null && bundle.containsKey("upgrade")) {
            data= new Gson().fromJson(bundle.getString("upgrade"), FreeCorporateService.class);
        }else
            dismiss();


        ImageView img_cancel= (ImageView)view.findViewById(R.id.img_cancel);
        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        TextView txt_info= (TextView)view.findViewById(R.id.txt_info);
        txt_info.setText("Upgrade "+ data.getName()+ " To -");

        LinearLayout linear_= (LinearLayout)view.findViewById(R.id.linear_);

        linear_.removeAllViews();

        data.getUpgrade().get(0).setCheck(true);

        final List<RadioButton> list_radio_= new ArrayList<>();

        for(int i=0; i<data.getUpgrade().size(); i++){

            final int index =i;
            View view_= LayoutInflater.from(getActivity()).
                    inflate(R.layout.freebie_radio, null, false);

            LinearLayout linear_click= (LinearLayout)view_.findViewById(R.id.linear_click);

            TextView txt_name= (TextView)view_.findViewById(R.id.txt_name);
            txt_name.setText(data.getUpgrade().get(i).getName());

            final RadioButton radio_= (RadioButton)view_.findViewById(R.id.radio_);
            list_radio_.add(radio_);                        //dalo radio list mai

            if(data.getUpgrade().get(index).isCheck()){
                upgrade_id= data.getUpgrade().get(index).getId();

                list_radio_.get(index).setChecked(true);
                radio_.setChecked(true);
            }else{
                radio_.setChecked(false);
                list_radio_.get(index).setChecked(false);
            }

            linear_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(list_radio_.get(index).isChecked()){

                        list_radio_.get(index).setChecked(false);
                    }else{

                        list_radio_.get(index).setChecked(true);
                    }

                    int list_radio_size=0;
                    for(int j=0;j<list_radio_.size();j++){

                        if(j==index && list_radio_.get(index).isChecked()){

                            upgrade_id= data.getUpgrade().get(j).getId();
                        }else{

                            list_radio_size++;
                            list_radio_.get(j).setChecked(false);
                        }
                    }
                    if(list_radio_size==list_radio_.size()){

                        upgrade_id= data.getUpgrade().get(index).getId();
                        list_radio_.get(index).setChecked(true);
                    }

                }
            });

            linear_.addView(view_);

        }

        TextView txt_upgrade= (TextView)view.findViewById(R.id.txt_upgrade);
        txt_upgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendData();
            }
        });



        return view;

    }


    private void sendData(){

        UpgradePost post= new UpgradePost();
        post.setUserId(BeuSalonsSharedPrefrence.getUserId());
        post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        post.setId(data.getId());
        post.setUpgradeId(upgrade_id);

        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<UpgradeResponse> call= apiInterface.corporateUpgrade(post);
        call.enqueue(new Callback<UpgradeResponse>() {
            @Override
            public void onResponse(Call<UpgradeResponse> call, Response<UpgradeResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().isSuccess()){
                        Log.i("freeupgrade", "success mai");
                        dismiss();
                        EventBus.getDefault().post(new FreebieUpgradeEvent());

                    }else{
                        Log.i("freeupgrade", "unsuccessful hai yeh");
                    }


                }else{
                    Log.i("freeupgrade", "retrofit unsuccessful hai");
                }


            }

            @Override
            public void onFailure(Call<UpgradeResponse> call, Throwable t) {
                Log.i("freeupgrade", "failure: "+ t.getMessage()+ "  "+ t.getCause()+  " "+ t.getStackTrace());
            }
        });



    }

//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//        try {
//
//            Window window = getDialog().getWindow();
//            window.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFFFFF")));
//            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        }catch (Exception e){
//
//            e.printStackTrace();
//        }
//
//    }

}
