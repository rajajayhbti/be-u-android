package com.beusalons.android.Model.Registration;

import java.io.Serializable;

/**
 * Created by myMachine on 11/1/2016.
 */

public class Registration_Response implements Serializable {


    private boolean success;
    private String message;


    public Registration_Response(){

    }

    public Registration_Response(boolean success, String message){

        this.success= success;
        this.message= message;
    }


    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {

        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
