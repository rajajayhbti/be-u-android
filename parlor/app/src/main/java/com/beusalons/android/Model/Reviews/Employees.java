package com.beusalons.android.Model.Reviews;

/**
 * Created by myMachine on 9/5/2017.
 */

public class Employees {

    private String employeeId;
    private int rating;
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
