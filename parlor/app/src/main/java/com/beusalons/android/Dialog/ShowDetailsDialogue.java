package com.beusalons.android.Dialog;


import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.R;


/**
 * Created by Ashish on 13/04/2017.
 */

public class ShowDetailsDialogue {
    final Dialog dialog;

    private TextView txtViewHeader,txtDiscription;
    LinearLayout linearLayoutManager,llDealPopup;

    private  TextView txtViewService;

    public ShowDetailsDialogue(final Activity activity,  String DealName, String DealDiscriptiom){

        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_deal_details);
        txtViewHeader=(TextView)dialog.findViewById(R.id.xt_show_detail);
        txtDiscription=(TextView)dialog.findViewById(R.id.txt_deal_discription);
        llDealPopup=(LinearLayout)dialog.findViewById(R.id.ll_deal_popup);
        linearLayoutManager= (LinearLayout) dialog.findViewById(R.id.ll_show_deal_dialogue);
        txtDiscription.setText(DealDiscriptiom);
        txtViewHeader.setText(DealName);

        llDealPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        linearLayoutManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

               dialog.show();
    }

}
