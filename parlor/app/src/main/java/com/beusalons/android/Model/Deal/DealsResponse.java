package com.beusalons.android.Model.Deal;

import com.beusalons.android.Model.StatusModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ajay on 1/8/2017.
 */

public class DealsResponse extends StatusModel implements Serializable {



    @SerializedName("data")
    @Expose
    private List<SelectedDealsModel> data;



    public List<SelectedDealsModel> getData() {
        return data;
    }

    public void setData(List<SelectedDealsModel> data) {
        this.data = data;
    }
}
