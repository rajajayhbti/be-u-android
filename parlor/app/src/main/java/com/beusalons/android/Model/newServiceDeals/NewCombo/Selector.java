package com.beusalons.android.Model.newServiceDeals.NewCombo;


import java.util.List;

/**
 * Created by myMachine on 6/17/2017.
 */

//stuff--
public class Selector {

    private String title;
    private String type;
    private String serviceTitle;
    private List<Service_> services = null;
    private List<Brand> brands = null;
    private boolean maxSaving;
    private boolean popularChoice;
    private String description;

    private boolean showToUser;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isMaxSaving() {
        return maxSaving;
    }

    public void setMaxSaving(boolean maxSaving) {
        this.maxSaving = maxSaving;
    }

    public boolean isPopularChoice() {
        return popularChoice;
    }

    public void setPopularChoice(boolean popularChoice) {
        this.popularChoice = popularChoice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

    public List<Service_> getServices() {
        return services;
    }

    public void setServices(List<Service_> services) {
        this.services = services;
    }

    public List<Brand> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

    public boolean isShowToUser() {
        return showToUser;
    }

    public void setShowToUser(boolean showToUser) {
        this.showToUser = showToUser;
    }
}
