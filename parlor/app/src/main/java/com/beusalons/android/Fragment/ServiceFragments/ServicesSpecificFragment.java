package com.beusalons.android.Fragment.ServiceFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beusalons.android.Adapter.NewServiceDeals.ServiceAdapter;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.SalonHome.HomeResponse;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Category;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Data;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Service;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Slabs;
import com.beusalons.android.R;

import java.util.List;

/**
 * Created by myMachine on 5/23/2017.
 */

public class ServicesSpecificFragment extends Fragment {

    private Category category;
    private Slabs slabs;
    private UserCart user_cart;
    private UserCart saved_cart;
    private  HomeResponse homeResponse;

    public static ServicesSpecificFragment newInstance(Category category, Slabs slabs, UserCart user_cart, UserCart saved_cart, HomeResponse homeResponse){

        ServicesSpecificFragment fragment= new ServicesSpecificFragment();

        fragment.category= category;
        fragment.slabs = slabs;
        fragment.user_cart= user_cart;
        fragment.saved_cart= saved_cart;
        fragment.homeResponse=homeResponse;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_services_specific, container, false);

        RecyclerView recyclerView= (RecyclerView)view.findViewById(R.id.rec_services);
        recyclerView.hasFixedSize();
        LinearLayoutManager manager= new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);

        if(category!=null &&
                category.getServices()!=null &&
                category.getServices().size()>0){

            if(saved_cart!=null &&
                    saved_cart.getServicesList().size()>0 &&
                    saved_cart.getCartType()!=null &&
                    saved_cart.getCartType().equalsIgnoreCase(AppConstant.SERVICE_TYPE)){
                populateData();
            }


            ServiceAdapter adapter= new ServiceAdapter(getActivity(), category.getServices(), slabs, user_cart,homeResponse);
            recyclerView.setAdapter(adapter);
        }

        return view;
    }

    private void populateData(){

        for(int j=0; j<category.getServices().size();j++){

            if(category.getServices().get(j).getParlorDealId()!=null){

                int quantity= 0;
                for(int i=0;i<saved_cart.getServicesList().size();i++){

                    if(saved_cart.getServicesList().get(i).getService_deal_id()!=null)
                        if(saved_cart.getServicesList().get(i).getService_deal_id()
                                .equalsIgnoreCase(category.getServices().get(j).getParlorDealId())){

                            quantity+=saved_cart.getServicesList().get(i).getQuantity();
                        }
                }
                category.getServices().get(j).setQuantity(quantity);

            }else if(category.getServices().get(j).getServiceId()!=null){

                int quantity= 0;
                for(int i=0;i<saved_cart.getServicesList().size();i++){
                    if(saved_cart.getServicesList().get(i).getService_id()!=null)
                        if(saved_cart.getServicesList().get(i).getService_id()
                                .equalsIgnoreCase(category.getServices().get(j).getServiceId())){

                            quantity+=saved_cart.getServicesList().get(i).getQuantity();
                        }
                }
                category.getServices().get(j).setQuantity(quantity);
            }
        }


    }


}
