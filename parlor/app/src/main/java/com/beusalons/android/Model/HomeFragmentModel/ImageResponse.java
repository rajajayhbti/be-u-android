package com.beusalons.android.Model.HomeFragmentModel;

import java.util.List;

/**
 * Created by myMachine on 1/26/2017.
 */

public class ImageResponse {


    public Boolean success;
    public List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

}
