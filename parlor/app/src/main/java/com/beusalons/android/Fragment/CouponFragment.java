package com.beusalons.android.Fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beusalons.android.Adapter.AdapterCoupon;
import com.beusalons.android.Event.CouponCodeApply;
import com.beusalons.android.Model.Coupon.CouponCode;
import com.beusalons.android.Model.Coupon.Coupon_Response;
import com.beusalons.android.Model.Coupon.Coupon_post;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Ajay on 12/5/2017.
 */

public class CouponFragment extends DialogFragment{
    private RecyclerView recyclerView;
    private boolean hasOpen;
    private LinearLayout linearHeader,linear_child,linear_one,linear_back,linearLayout_top;
    private TextView txtViewMoreCouponCode;
    private String apptId;
    EditText extView_coupon_code;
    private String useCouponCode,useCouponCodeId;
    private RelativeLayout linearLayout_apply;
    private View loading_for_coupon;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.coupon_screen, container, false);
        Bundle bundle= getArguments();
        if(bundle!=null && bundle.containsKey("has_coupon")){
            hasOpen= bundle.getBoolean("has_coupon", false);
            apptId=bundle.getString("apptId");

        }
        loading_for_coupon=(View)view.findViewById(R.id.loading_for_coupon);
        linearLayout_top=(LinearLayout)view.findViewById(R.id.linearLayout_top);
        txtViewMoreCouponCode=(TextView)view.findViewById(R.id.txtViewMoreCouponCode);
        extView_coupon_code=(EditText)view.findViewById(R.id.txtView_coupon_code);
        recyclerView=(RecyclerView)view.findViewById(R.id.recycler_view_coupon);
        linearHeader=(LinearLayout)view.findViewById(R.id.linear_header);
        linear_child=(LinearLayout)view.findViewById(R.id.linear_child);
        linear_one=(LinearLayout)view.findViewById(R.id.linear_one);
        linear_back=(LinearLayout)view.findViewById(R.id.linear_back);
        linearLayout_apply=(RelativeLayout) view.findViewById(R.id.linearLayout_apply);
        if (hasOpen){
            getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);          //no action bar space
            linearHeader.setVisibility(View.VISIBLE);
            linear_child.setVisibility(View.GONE);
            linear_one.setVisibility(View.VISIBLE);
            txtViewMoreCouponCode.setVisibility(View.VISIBLE);
            linearLayout_apply.setVisibility(View.VISIBLE);
        }else{
            linear_one.setVisibility(View.GONE);
            linearHeader.setVisibility(View.GONE);
            linear_child.setVisibility(View.VISIBLE);
            txtViewMoreCouponCode.setVisibility(View.GONE);
            linearLayout_apply.setVisibility(View.GONE);


        }
        linear_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        linearLayout_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new CouponCodeApply(useCouponCode,useCouponCodeId));
                dismiss();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchCoupon();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {

            Window window = getDialog().getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFFFFF")));
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        }catch (Exception e){

            e.printStackTrace();
        }

    }

    private void fetchCoupon(){
        linearLayout_top.setVerticalGravity(View.GONE);
        loading_for_coupon.setVisibility(View.VISIBLE);
        Retrofit retrofit = ServiceGenerator.getClient();

        final ApiInterface service = retrofit.create(ApiInterface.class);
        Coupon_post coupon_post=new Coupon_post();
        coupon_post.setUserId(BeuSalonsSharedPrefrence.getUserId());
        coupon_post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        if (hasOpen)
            coupon_post.setAppointmentId(apptId);

        Call<Coupon_Response> call=service.couponGetData(coupon_post);

        call.enqueue(new Callback<Coupon_Response>() {
            @Override
            public void onResponse(Call<Coupon_Response> call, Response<Coupon_Response> response) {

                if(response.isSuccessful()){
                    if (response.body().getSuccess()){

                        linearLayout_top.setVerticalGravity(View.VISIBLE);
                        loading_for_coupon.setVisibility(View.GONE);

                        List<CouponCode> list= new ArrayList<>();
                        if (response.body().getData().getCouponCode()!=null &&
                                response.body().getData().getCouponCode().size()>0)
                            for(int i=0;i<response.body().getData().getCouponCode().size();i++){
                                response.body().getData().getCouponCode().get(i).setType(CouponCode.COUPON);
                                response.body().getData().getCouponCode().get(i).setActive(true);
                                list.add(response.body().getData().getCouponCode().get(i));
                            }


                        list.add(new CouponCode(CouponCode.EARN_MORE_COUPON));

                        if(response.body().getData().getInactiveCoupons()!=null &&
                                response.body().getData().getInactiveCoupons().size()>0)
                            for(int i=0;i<response.body().getData().getInactiveCoupons().size();i++){
                                response.body().getData().getInactiveCoupons().get(i).setType(CouponCode.COUPON);
                                response.body().getData().getInactiveCoupons().get(i).setActive(false);
                                list.add(response.body().getData().getInactiveCoupons().get(i));
                            }

                        Log.i("sizeoflist", "value: "+ list.size());

                        AdapterCoupon adapterCoupon=new AdapterCoupon(getActivity(), list, hasOpen);
                        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                        recyclerView.setAdapter(adapterCoupon);


                    }else{

                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<Coupon_Response> call, Throwable t) {
                Log.i("coupon", "i'm in failure: "+ t.getMessage()+ "  "+ t.getCause()+ "  "+ t.getStackTrace());
            }
        });


    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CouponCode  event) {
        /* Do something */
        extView_coupon_code.setText(event.getCode());
        useCouponCode=event.getCode();
        useCouponCodeId=event.getCouponId();
        Log.e("use coupon code",event.getCode());
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

}
