package com.beusalons.android.Adapter;

/**
 * Created by Robbin Singh on 15/11/2016.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Model.Verify_Otp.SearchParlorModel;
import com.beusalons.android.ParlorListActivity;
import com.beusalons.android.R;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.beusalons.android.Helper.AppConstant.LOCATION_SUF;
import static com.beusalons.android.Helper.AppConstant.PARLOR_SUF;


/**
 * Created by Robbin Singh on 03/11/2016.
 */

/**
 * Created by Robbin Singh on 03/11/2016.
 */

public class ParlorsSearchListAdapter extends RecyclerView.Adapter<ParlorsSearchListAdapter.MyViewHolder>  {

    private List<SearchParlorModel> listData;
    private Activity activity;
    public ParlorsSearchListAdapter(List<SearchParlorModel> list, Activity activity) {
        this.listData = list;
        this.activity = activity;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.parlor_search_list_view_holder, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if(listData.get(position).getParlorId()==null){

            holder.name.setText(LOCATION_SUF+ listData.get(position).getTitle());
        }else{

            holder.txt_location.setVisibility(View.VISIBLE);
            holder.name.setText(PARLOR_SUF+ listData.get(position).getTitle());
            holder.txt_location.setText(listData.get(position).getParlor_location());
        }

//        holder.name.setText((listData.get(position).getParlorId() == null ? LOCATION_SUF : PARLOR_SUF) +listData.get(position).getTitle());

        holder.container.setTag(position);
        holder.container.setOnClickListener(openParlorList);
    }

    View.OnClickListener openParlorList = new View.OnClickListener(){
        @Override
        public void onClick(View v) {

            Runnable runnable= new Runnable() {
                @Override
                public void run() {
                    InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
                }
            };

            Handler handler= new Handler();
            handler.postDelayed(runnable, 0);
            int position = (Integer) v.getTag();
            Log.i("investigati", "Value in getparlor: "+listData.get(position).getTitle()+ " parlor id: "+
            listData.get(position).getPlaceId());



            Intent intent1 = new Intent(activity, ParlorListActivity.class);
            intent1.putExtra("placeId", listData.get(position).getPlaceId().equalsIgnoreCase("")?"":listData.get(position).getPlaceId());
            intent1.putExtra("location", listData.get(position).getTitle());

////            Intent intent2 = new Intent(activity, ParlorDetailActivity.class);
//            intent2.putExtra("location",  BeuSalonsSharedPrefrence.getAddressLocalty());
//            intent2.putExtra("parlorId",  listData.get(position).getParlorId());


//            activity.startActivity(intent1);

//            activity.startActivity(listData.get(position).getParlorId() == null ? intent1 : intent2);
//            activity.finish();
        }
    };


    @Override
    public int getItemCount() {
        return listData.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name) TextView name;
        @BindView(R.id.container) LinearLayout container;
        @BindView(R.id.location) TextView txt_location;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}

