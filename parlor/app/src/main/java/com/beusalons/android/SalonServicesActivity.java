package com.beusalons.android;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.beusalons.android.Adapter.NewServiceDeals.ServicesDepartmentAdapter;
import com.beusalons.android.Model.SalonHome.HomeResponse;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.newServiceDeals.Department;
import com.beusalons.android.Model.newServiceDeals.DepartmentResponse;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.google.gson.Gson;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SalonServicesActivity extends AppCompatActivity {

    private ToggleButton toggle_;
    TextView txt_female, txt_male;

    private RecyclerView rec_departments;
    private ServicesDepartmentAdapter adapter;

    private ArrayList<Department> list= new ArrayList<>();
    private String gender= "F";

    private String home_membership;

    private DepartmentResponse departmentResponse;         //navigated from home services
    private HomeResponse homeResponse;          //coming from salon detail page


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);


        //setting the recylerview's
        rec_departments= (RecyclerView)findViewById(R.id.rec_departments);
        txt_female= findViewById(R.id.txt_female);
        txt_male= findViewById(R.id.txt_male);

        toggle_= (ToggleButton)findViewById(R.id.toggle_);
        Bundle bundle= getIntent().getExtras();
        UserCart userCart=null;
        if(bundle!=null && bundle.containsKey("home_response")){            //when user navigated from salon detail page

            homeResponse= new Gson().fromJson(bundle.getString("home_response"), HomeResponse.class);
            home_membership= bundle.getString("home_response", null);

            setToolBar(homeResponse.getData().getName());

            userCart= new UserCart();
            userCart.setParlorId(homeResponse.getData().getParlorId());
            userCart.setParlorName(homeResponse.getData().getName());
            userCart.setParlorType(homeResponse.getData().getParlorType());

            userCart.setGender(homeResponse.getData().getGender());
            userCart.setRating(homeResponse.getData().getRating());

            userCart.setOpeningTime(homeResponse.getData().getOpeningTime());
            userCart.setClosingTime(homeResponse.getData().getClosingTime());

            userCart.setAddress1(homeResponse.getData().getAddress1());
            userCart.setAddress2(homeResponse.getData().getAddress2());

            userCart.setDate_time(bundle.getString("selected_date_time"));
        }

        //department adapter
        rec_departments.hasFixedSize();

        LinearLayoutManager layoutManager= new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(false);
        rec_departments.setLayoutManager(layoutManager);

        adapter= new ServicesDepartmentAdapter(this, list, gender,
                userCart, home_membership);
        rec_departments.setAdapter(adapter);         //male female


        toggle_.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){
                    gender= "M";
                    txt_male.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                            R.color.black));
                    txt_female.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                            R.color.darker_gray));


                    for (int i=0;i<homeResponse.getData().getDepartments().size();i++){
                        if (homeResponse.getData().getDepartments().get(i).getGender().equalsIgnoreCase("M")) {

                            list= homeResponse.getData().getDepartments().get(i).getDepartments();
                        }

                    }

                    adapter.setData(list, gender);
                    adapter.notifyDataSetChanged();
                }else{
                    gender= "F";
                    txt_female.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                            R.color.black));
                    txt_male.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                            R.color.darker_gray));


                    for (int i=0;i<homeResponse.getData().getDepartments().size();i++){
                        if (homeResponse.getData().getDepartments().get(i).getGender().equalsIgnoreCase("F")) {
                            list= homeResponse.getData().getDepartments().get(i).getDepartments();
                        }

                    }

                    adapter.setData(list, gender);
                    adapter.notifyDataSetChanged();
                }
            }
        });




        //use for set default  toggle
        LinearLayout linear_gender= findViewById(R.id.linear_gender);



        if(homeResponse.getData().getDepartments().size()>1) {

            linear_gender.setVisibility(View.VISIBLE);
        }else          linear_gender.setVisibility(View.GONE);


        for (int i=0;i<homeResponse.getData().getDepartments().size();i++){
            if (BeuSalonsSharedPrefrence.getGender().equalsIgnoreCase(homeResponse.getData().getDepartments().get(i).getGender()) ){
                gender=homeResponse.getData().getDepartments().get(i).getGender();
                if (gender.equalsIgnoreCase("M")){
                    gender = "M";
                    txt_male.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                            R.color.black));
                    txt_female.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                            R.color.darker_gray));
                    toggle_.setChecked(true);
                    list = homeResponse.getData().getDepartments().get(i).getDepartments();
                    adapter.setData(list, gender);
                    adapter.notifyDataSetChanged();
                }else{
                    gender = "F";
                    toggle_.setChecked(false);

                    txt_female.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                            R.color.black));
                    txt_male.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                            R.color.darker_gray));
                    list = homeResponse.getData().getDepartments().get(i).getDepartments();
                    adapter.setData(list, gender);
                    adapter.notifyDataSetChanged();
                }


            }else{
                gender=homeResponse.getData().getDepartments().get(i).getGender();
                if (gender.equalsIgnoreCase("M")){
                    gender = "M";
                    txt_male.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                            R.color.black));
                    txt_female.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                            R.color.darker_gray));
                    toggle_.setChecked(true);
                    list = homeResponse.getData().getDepartments().get(i).getDepartments();
                    adapter.setData(list, gender);
                    adapter.notifyDataSetChanged();
                }else{
                    gender = "F";
                    toggle_.setChecked(false);

                    txt_female.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                            R.color.black));
                    txt_male.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                            R.color.darker_gray));
                    list = homeResponse.getData().getDepartments().get(i).getDepartments();
                    adapter.setData(list, gender);
                    adapter.notifyDataSetChanged();
                }
            }
        }
        if (BeuSalonsSharedPrefrence.getGender().equalsIgnoreCase("")){
            gender = homeResponse.getData().getDepartments().get(0).getGender();
            if (gender.equalsIgnoreCase("M")){
                gender = "M";
                txt_male.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                        R.color.black));
                txt_female.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                        R.color.darker_gray));
                toggle_.setChecked(true);
                list = homeResponse.getData().getDepartments().get(0).getDepartments();
                adapter.setData(list, gender);
                adapter.notifyDataSetChanged();
            }else{
                gender = "F";
                toggle_.setChecked(false);

                txt_female.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                        R.color.black));
                txt_male.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                        R.color.darker_gray));
                list = homeResponse.getData().getDepartments().get(0).getDepartments();
                adapter.setData(list, gender);
                adapter.notifyDataSetChanged();
            }

        }
        txt_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle_.setChecked(true);
            }
        });
        txt_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle_.setChecked(false);
            }
        });

        /*if (BeuSalonsSharedPrefrence.getGender()!=null &&BeuSalonsSharedPrefrence.getGender().equals("M") ) {
            if(homeResponse.getData().getDepartments().size()>1) {
                gender = "M";
                txt_male.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                        R.color.black));
                txt_female.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                        R.color.darker_gray));
                toggle_.setChecked(true);
                if (homeResponse.getData().getDepartments().get(0).getGender().equalsIgnoreCase("M"))
                    list = homeResponse.getData().getDepartments().get(0).getDepartments();
                else if (homeResponse.getData().getDepartments().get(1).getGender().equalsIgnoreCase("M"))
                    list = homeResponse.getData().getDepartments().get(1).getDepartments();
            }else{
                gender = "F";
                toggle_.setChecked(false);

                txt_female.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                        R.color.black));
                txt_male.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                        R.color.darker_gray));

                if (homeResponse.getData().getDepartments().get(0).getGender().equalsIgnoreCase("F"))
                    list = homeResponse.getData().getDepartments().get(0).getDepartments();


            }


            adapter.setData(list, gender);
            adapter.notifyDataSetChanged();
        } else if (BeuSalonsSharedPrefrence.getGender()!=null && BeuSalonsSharedPrefrence.getGender().equals("F")) {
            gender = "F";
            toggle_.setChecked(false);

            txt_female.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                    R.color.black));
            txt_male.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                    R.color.darker_gray));

            if (homeResponse.getData().getDepartments().get(0).getGender().equalsIgnoreCase("F"))
                list = homeResponse.getData().getDepartments().get(0).getDepartments();
            else if (homeResponse.getData().getDepartments().get(1).getGender().equalsIgnoreCase("F"))
                list = homeResponse.getData().getDepartments().get(1).getDepartments();

            adapter.setData(list, gender);
            adapter.notifyDataSetChanged();
        }else{
            gender = "F";
            toggle_.setChecked(false);

            txt_female.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                    R.color.black));
            txt_male.setTextColor(ContextCompat.getColor(SalonServicesActivity.this,
                    R.color.darker_gray));

            if (homeResponse.getData().getDepartments().get(0).getGender().equalsIgnoreCase("F"))
                list = homeResponse.getData().getDepartments().get(0).getDepartments();
        }*/
      /*  adapter.setData(list, gender);
        adapter.notifyDataSetChanged();*/

    }

    private void setToolBar(String salon_name){

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        String txt= "Select Services - "+ salon_name.toUpperCase();

        if (getSupportActionBar() != null){

            getSupportActionBar().setTitle(txt);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


}
