package com.beusalons.android.Model.DealsData;

import com.beusalons.android.Model.newServiceDeals.Service;

import java.util.List;

/**
 * Created by Ajay on 6/7/2017.
 */

public class DealsData {
    private List<Service> deals = null;

    public List<Service> getDeals() {
        return deals;
    }

    public void setDeals(List<Service> deals) {
        this.deals = deals;
    }
}
