package com.beusalons.android.Model.UserProject;


import com.beusalons.android.Model.Profile.Project;

import java.util.List;


/**
 * Created by Ajay on 2/8/2018.
 */

public class Project_response {

    private Boolean success;
    private List<Project> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Project> getData() {
        return data;
    }

    public void setData(List<Project> data) {
        this.data = data;
    }
}
