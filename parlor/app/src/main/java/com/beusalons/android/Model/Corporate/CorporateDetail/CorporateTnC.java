package com.beusalons.android.Model.Corporate.CorporateDetail;

import java.util.List;

/**
 * Created by myMachine on 8/19/2017.
 */

public class CorporateTnC {

    private String title;
    private List<String> points = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getPoints() {
        return points;
    }

    public void setPoints(List<String> points) {
        this.points = points;
    }
}
