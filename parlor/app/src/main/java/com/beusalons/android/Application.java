package com.beusalons.android;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.widget.Toast;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.AppCloseResponse;
import com.beusalons.android.Model.Subscription_post;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.facebook.FacebookSdk;
import com.snappydb.DB;
import com.snappydb.DBFactory;


import io.branch.referral.Branch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Ashish on 22/04/2017.
 */

public class Application extends MultiDexApplication implements android.app.Application.ActivityLifecycleCallbacks {
    UserCart saved_cart= null;
    private static int resumed;
    private static int paused;
    private static int started;
    private static int stopped;
    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Lato-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        FacebookSdk.sdkInitialize(this);
        BeuSalonsSharedPrefrence.init(this);
        BeuSalonsSharedPrefrence.setOfferDialog(getApplicationContext(),true);

        int totalCount=BeuSalonsSharedPrefrence.getAppOpenCount();
        totalCount++;
        BeuSalonsSharedPrefrence.setAppOpenCount(totalCount);

        // Initialize the Branch object
        Branch.getAutoInstance(this);

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        if ( getCardData(getApplicationContext())>0)
            killAppPost(getApplicationContext());

    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        ++resumed;
    }

    @Override
    public void onActivityPaused(Activity activity) {
        ++paused;
        android.util.Log.w("test", "application is in foreground: " + (resumed > stopped));
    }

    @Override
    public void onActivityStopped(Activity activity) {
        ++stopped;
        android.util.Log.w("test", "application is visible: " + (resumed > stopped));
        if (!isAppBackground()  &&  getCardData(activity)>0)
            killAppPost(activity);
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
    public boolean isAppBackground(){

        return resumed > stopped;

    }

    private int getCardData(Context activity){
        try {

            DB snappyDB= DBFactory.open(activity);
            saved_cart= null;               //db mai jo saved cart hai
            if(snappyDB.exists(AppConstant.USER_CART_DB)){

                saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
                snappyDB.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        if(saved_cart!=null &&
                saved_cart.getServicesList().size()>0)
            return saved_cart.getServicesList().size();
        return 0;
    }

    private void killAppPost(Context activity ){
//        Toast.makeText(activity,"ho gya band",Toast.LENGTH_SHORT).show();
        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface=retrofit.create(ApiInterface.class);
        Subscription_post userCartPost=new Subscription_post();
        userCartPost.setUserId(BeuSalonsSharedPrefrence.getUserId());

        Call<AppCloseResponse> call=apiInterface.onAppClose(userCartPost);

        call.enqueue(new Callback<AppCloseResponse>() {
            @Override
            public void onResponse(Call<AppCloseResponse> call, Response<AppCloseResponse> response) {
                if (response.isSuccessful()){

                }
            }

            @Override
            public void onFailure(Call<AppCloseResponse> call, Throwable t) {


            }
        });

    }

   }
