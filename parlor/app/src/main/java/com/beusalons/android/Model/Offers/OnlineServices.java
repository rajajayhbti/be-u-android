package com.beusalons.android.Model.Offers;

/**
 * Created by myMachine on 2/23/2017.
 */

public class OnlineServices {

    private String name;
    private int price;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
