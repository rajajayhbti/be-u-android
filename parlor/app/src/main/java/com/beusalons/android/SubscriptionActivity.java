package com.beusalons.android;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.Appointments.AppointmentPost;
import com.beusalons.android.Model.Coupon.Coupon_post;
import com.beusalons.android.Model.subscription.Question;
import com.beusalons.android.Model.subscription.SubsData;
import com.beusalons.android.Model.subscription.SubscribedData;
import com.beusalons.android.Model.subscription.SubscriptionResponse;
import com.beusalons.android.Model.subscription.Tile;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


/**
 * Created by Ashish Sharma on 1/19/2018.
 */

public class SubscriptionActivity extends AppCompatActivity{
    @BindView(R.id.txt_subs_detail_title)
    TextView subsDetailTitle;
    @BindView(R.id.txt_subs_detail_sub_title)
    TextView subsDetailSubTitle;
    @BindView(R.id.txt_subs_dtl_light)
    TextView txt_subs_dtl_light;
    @BindView(R.id.txt_subs_dtl_tnc)
    TextView txt_subs_dtl_tnc;
    @BindView(R.id.txt_sub_salons)
    WebView txt_sub_salons;
    @BindView(R.id.tile_container)
    LinearLayout tile_container;
    @BindView(R.id.container_steps)
    LinearLayout container_steps;

    @BindView(R.id.container_faq)
    LinearLayout container_faq;
    @BindView(R.id.ttttt)
    RelativeLayout ttttt;
    @BindView(R.id.btn_buy_subs)
    TextView btnBuy;
    @BindView(R.id.ll_with_subs)
    LinearLayout containerSubscribed;
    @BindView(R.id.ll_without_subs)
    LinearLayout containerNotSubscribed;
    @BindView(R.id.img_subs)
    ImageView imgSubscriber;
    @BindView(R.id.progress_)
    ProgressBar progess_;
    @BindView(R.id.ll_container)
    LinearLayout ll_container;
    @BindView(R.id.txt_share)
    TextView txt_share;
    @BindView(R.id.txt_share_ur_referal_code)
    TextView txt_share_ur_referal_code;
    @BindView(R.id.txt_refer_friend)
    TextView txt_refer_friend;
    @BindView(R.id.tnc_faq)
    TextView tnc_faq;
    @BindView(R.id.scroll_view)
    NestedScrollView scroll_view;
    @BindView(R.id.testc)
    LinearLayout testc;
    private SubsData data;
    @BindView(R.id.txt_refer)
    WebView txt_refer;

    private Tile selectedSubcription;

    private List<ImageView> img_list;
    @BindView(R.id.card_sub)
    CardView card_sub;
    @BindView(R.id.linear_history)
    LinearLayout linear_history;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);
        ButterKnife.bind(this);
        setToolBar();


        ll_container.setAlpha(.2f);
        progess_.setVisibility(View.VISIBLE);
        fetchSubscriptionDetail(this);

        btnBuy.setOnClickListener(buSubscription);

        txt_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg=data.getSelectSubscription().getSubscribedData().getReferMessage()+"https://beusalons.app.link/hjrjss9mZJ?page=subscription&subscriptionCode="+BeuSalonsSharedPrefrence.getReferCode();
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                msg=msg.replaceAll(" @@ "," "+BeuSalonsSharedPrefrence.getReferCode() +" ");
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent,"Refer Subscription"));

            }
        });
        txt_refer_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                scroll_view.post(new Runnable() {
                    @Override
                    public void run() {
                        scroll_view.scrollTo(0,testc.getBottom() );
                    }
                });

            }
        });


        linear_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(SubscriptionActivity.this,SubscriptionsHistoryActivity.class);
                startActivity(in);
            }
        });

    }
    View.OnClickListener buSubscription=new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            final AppointmentPost appointmentPost= new AppointmentPost();

            appointmentPost.setUserId(BeuSalonsSharedPrefrence.getUserId());
            appointmentPost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
            appointmentPost.setLatitude(BeuSalonsSharedPrefrence.getLatitude());
            appointmentPost.setLongitude(BeuSalonsSharedPrefrence.getLongitude());
            Calendar calendar= Calendar.getInstance();
            Date date= calendar.getTime();
            DateFormat date_format= new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss");
            appointmentPost.setDatetime(date_format.format(date)+" GMT+0530 (India Standard Time)");
            appointmentPost.setParlorId("594a359d9856d3158171ea4f");
            appointmentPost.setMode(1);                 //mode 1 for android, 2 for ios
            appointmentPost.setPaymentMethod(5);
            appointmentPost.setSubscriptionId(selectedSubcription.getSubscriptionId());
            Intent intent= new Intent(SubscriptionActivity.this, BillSummaryActivity.class);
            intent.putExtra("appointment_post", new Gson().toJson(appointmentPost));
            startActivity(intent);


        }
    };

    private void fetchSubscriptionDetail(final Context context){
        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Coupon_post post=new Coupon_post();
        post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());

        post.setUserId(BeuSalonsSharedPrefrence.getUserId());
       /* post.setAccessToken("6667fd4e322b4d53ff09c2e574f728c6912d83b8");
        post.setUserId("58bc455f87969966496cdae9");*/
        Call<SubscriptionResponse> call=apiInterface.getSubscription(post);
        call.enqueue(new Callback<SubscriptionResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<SubscriptionResponse> call, final Response<SubscriptionResponse> response) {


                if(response.isSuccessful()) {

                    if (response.body().getSuccess()) {



                        data =response.body().getData();
                        ll_container.setAlpha(1f);
                        progess_.setVisibility(View.GONE);
                        subsDetailTitle.setText(response.body().getData().getDetail().getHeading1());
                        subsDetailSubTitle.setText(response.body().getData().getDetail().getHeading2());
                        txt_subs_dtl_light.setText(response.body().getData().getDetail().getLightDetail());
                        StringBuilder html;
                        html = new StringBuilder();
                        for (String tnc : response.body().getData().getDetail().getTnc()) {
                            html.append(tnc);
                        }
                        txt_subs_dtl_tnc.setText(Html.fromHtml(html.toString()));
                        //   View view = LayoutInflater.from(SubscriptionActivity.this).inflate(R.layout.row_subscriptions, null);


                        txt_sub_salons.loadData(data.getSelectSubscription().getSubscriptionCount(), "text/html", "utf-8");
                        txt_sub_salons.setBackgroundColor(Color.TRANSPARENT);

                        if (response.body().getData().getSelectSubscription().isSubscribed()) {
                            containerSubscribed.setVisibility(View.VISIBLE);
                            containerNotSubscribed.setVisibility(View.GONE);
                            card_sub.setVisibility(View.VISIBLE);
                            ttttt.setVisibility(View.GONE);

                            txt_share_ur_referal_code.setText(BeuSalonsSharedPrefrence.getReferCode());


                            TextView txtName = ButterKnife.findById(SubscriptionActivity.this, R.id.txt_subs_user);
                            TextView txt_subs_name = ButterKnife.findById(SubscriptionActivity.this, R.id.txt_subs_name);
                            TextView txt_valid_from = ButterKnife.findById(SubscriptionActivity.this, R.id.txt_valid_from);
                            TextView txt_valid_till = ButterKnife.findById(SubscriptionActivity.this, R.id.txt_valid_till);
//                        TextView txt_membership_id=ButterKnife.findById(SubscriptionActivity.this,R.id.txt_membership_id);
                            TextView txt_refer_friend = ButterKnife.findById(SubscriptionActivity.this, R.id.txt_refer_friend);
                            TextView txt_annual_balance = ButterKnife.findById(SubscriptionActivity.this, R.id.txt_annual_balance);
                            TextView txt_month_balance = ButterKnife.findById(SubscriptionActivity.this, R.id.txt_month_balance);
                            TextView txt_history = ButterKnife.findById(SubscriptionActivity.this, R.id.txt_history);
                            SubscribedData data = response.body().getData().getSelectSubscription().getSubscribedData();
                            txtName.setText(data.getName());
                            txt_subs_name.setText("You Are A " + data.getSubscriptionType() + " Subscriber");
                            txt_valid_from.setText(data.getValidFrom());
                            txt_valid_till.setText(data.getValidTill());
                            txt_annual_balance.setText(AppConstant.CURRENCY + data.getAnnualBalance());
                            txt_month_balance.setText(AppConstant.CURRENCY + data.getMonthlyBalance());
                            if (getWindow().getDecorView().isShown()){
                                Glide.with(context).load(data.getProfilePic()).apply(RequestOptions.circleCropTransform()).into(imgSubscriber);
                            }

                            txt_refer.loadData(data.getHeading(), "text/html", "utf-8");
                            txt_refer.setBackgroundColor(Color.TRANSPARENT);

//                        txt_membership_id.setText(data.get);

                        } else {
                            card_sub.setVisibility(View.GONE);

                            containerSubscribed.setVisibility(View.GONE);
                            containerNotSubscribed.setVisibility(View.VISIBLE);
                            AlphaAnimation alpha = new AlphaAnimation(0.5F, 0.5F);
                            alpha.setDuration(0); // Make animation instant
                            alpha.setFillAfter(true); // Tell it to persist after the animation ends
// And then on your layout
                            ttttt.startAnimation(alpha);
                            ttttt.setVisibility(View.VISIBLE);
                            tile_container.removeAllViews();


                            img_list = new ArrayList<>();
                            for (int i = 0; i < response.body().getData().getSelectSubscription().getTile().size(); i++) {
                                LayoutInflater inflater = LayoutInflater.from(SubscriptionActivity.this);
                                View view = inflater.inflate(R.layout.row_subscriptions, null, false);
                                HtmlTextView head1 = ButterKnife.findById(view, R.id.txt_head1);
                                HtmlTextView head2 = ButterKnife.findById(view, R.id.txt_head2);
                                ImageView imgRecommend = ButterKnife.findById(view, R.id.ic_recommended);
                                ImageView img_type= ButterKnife.findById(view, R.id.img_type);
                                final ImageView img = ButterKnife.findById(view, R.id.img_subs);



                                img_list.add(img);

                                //yeh condition add kara hai
                                if(response.body().getData().getSelectSubscription().getTile().get(i).getTitle().equalsIgnoreCase("GOLD"))
                                    img_type.setImageResource(R.drawable.ic_gold);
                                else
                                    img_type.setImageResource(R.drawable.ic_silver);


                                LinearLayout linearLayout = ButterKnife.findById(view, R.id.container_terms);
                                linearLayout.removeAllViews();
                                for (int j = 0; j < response.body().getData().getSelectSubscription().getTile().get(i).getPoints().size(); j++) {
                                    LayoutInflater inf = LayoutInflater.from(SubscriptionActivity.this);
                                    View tncView = inf.inflate(R.layout.layout_points, null, false);
                                    TextView tv = ButterKnife.findById(tncView, R.id.txt_name);
                                    tv.setText(response.body().getData().getSelectSubscription().getTile().get(i).getPoints().get(j));
                                    linearLayout.addView(tncView);
                                }

                                if (response.body().getData().getSelectSubscription().getTile().get(i).getRecommended().equalsIgnoreCase(BeuSalonsSharedPrefrence.getGender())) {
                                    response.body().getData().getSelectSubscription().getTile().get(i).setSelect(true);
                                    AlphaAnimation alpha1 = new AlphaAnimation(1.0F, 1.0F);
                                    alpha1.setDuration(0); // Make animation instant
                                    alpha.setFillAfter(true); // Tell it to persist after the animation ends
// And then on your layout
                                    ttttt.startAnimation(alpha1);
                                    selectedSubcription = response.body().getData().getSelectSubscription().getTile().get(i);
                                    img.setVisibility(View.VISIBLE);

                                    String msg = "Buy " + response.body().getData().getSelectSubscription().getTile().get(i).getTitle() + " Subscription @ " + AppConstant.CURRENCY + response.body().getData().getSelectSubscription().getTile().get(i).getAmount();
                                    btnBuy.setText(msg);


                                }
                                if (response.body().getData().getSelectSubscription().getTile().get(i).getRecommended().equalsIgnoreCase("F")) {

                                    imgRecommend.setBackground(getResources().getDrawable(R.drawable.ic_female));
                                } else {
                                    imgRecommend.setBackground(getResources().getDrawable(R.drawable.ic_male));
                                }
                                view.setTag(response.body().getData().getSelectSubscription().getTile().get(i));

                                final int pos = i;
                                view.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Tile tile = (Tile) view.getTag();
                                        AlphaAnimation alpha = new AlphaAnimation(1.0F, 1.0F);
                                        alpha.setDuration(0); // Make animation instant
                                        alpha.setFillAfter(true); // Tell it to persist after the animation ends
// And then on your layout
                                        selectedSubcription = tile;
                                        ttttt.startAnimation(alpha);
//                                    if (tile.isSelect()){
//                                        img.setVisibility(View.INVISIBLE);
//                                        tile.setSelect(false);
//                                    }else {
//                                        img.setVisibility(View.VISIBLE);
//                                        tile.setSelect(true);
//
//                                    }

                                        for (int j = 0; j < img_list.size(); j++) {

                                            if (img_list.get(j).equals(img)) {
                                                img.setVisibility(View.VISIBLE);
                                            } else
                                                img_list.get(j).setVisibility(View.INVISIBLE);

                                        }

                                        String msg = "Buy " + tile.getTitle() + " Subscription @ " + AppConstant.CURRENCY + tile.getAmount();
                                        btnBuy.setText(msg);
                                    }
                                });
                                head1.setHtml(response.body().getData().getSelectSubscription().getTile().get(i).getHeading1HTML().trim());
                                head2.setHtml(response.body().getData().getSelectSubscription().getTile().get(i).getHeading2HTML().trim());
                                tile_container.addView(view);
                            }

                        }


                        container_steps.removeAllViews();
                        for (int i = 0; i < response.body().getData().getAvailSteps().getSteps().size(); i++) {
                            LayoutInflater inflater = LayoutInflater.from(SubscriptionActivity.this);
                            View view = inflater.inflate(R.layout.row_steps_subs, null, false);
                            TextView head1 = ButterKnife.findById(view, R.id.txt_step_subs);
                            TextView head2 = ButterKnife.findById(view, R.id.txt_step_subs_detail);
                            ImageView img_steps = ButterKnife.findById(view, R.id.img_steps);
                            try{
                                if (getWindow().getDecorView().isShown()){
                                    Glide.with(view.getContext()).load(response.body().getData().getAvailSteps().getSteps().get(i).getIcon()).apply(RequestOptions.circleCropTransform()).into(img_steps);
                                }

                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            head1.setText(response.body().getData().getAvailSteps().getSteps().get(i).getTitle());
                            head2.setText(response.body().getData().getAvailSteps().getSteps().get(i).getContent());
                            container_steps.addView(view);
                        }
                        container_faq.removeAllViews();
                        for (int i = 0; i < response.body().getData().getFaq().getQuestions().size(); i++) {
                            LayoutInflater inflater = LayoutInflater.from(SubscriptionActivity.this);
                            View view = inflater.inflate(R.layout.row_subs_faq, null, false);
                            final TextView quest = ButterKnife.findById(view, R.id.txt_faq);
                            final TextView ans = ButterKnife.findById(view, R.id.txt_faq_answer);
                            final ImageView img_faq = ButterKnife.findById(view, R.id.img_faq);
                            View view_faq = ButterKnife.findById(view, R.id.view_faq);
                            quest.setText(response.body().getData().getFaq().getQuestions().get(i).getQuestion());
                            ans.setText(response.body().getData().getFaq().getQuestions().get(i).getAnswer());
                            if (i == response.body().getData().getFaq().getQuestions().size() - 1) {
                                view_faq.setVisibility(View.GONE);
                            }
                            view.setTag(response.body().getData().getFaq().getQuestions().get(i));
                            view.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Question questio = (Question) view.getTag();
                                    if (questio.isIsselect()) {
                                        ans.setVisibility(View.GONE);
                                        questio.setIsselect(false);
                                        img_faq.setBackground(getResources().getDrawable(R.drawable.ic_plus_icon));
                                    } else {
                                        ans.setVisibility(View.VISIBLE);
                                        ans.setText(questio.getAnswer());
                                        questio.setIsselect(true);
                                        img_faq.setBackground(getResources().getDrawable(R.drawable.ic_minus_icon));
                                    }

                                }
                            });
                            String tncFaq = "";
                            for (String s:response.body().getData().getFaq().getTnc()){
                                tncFaq+=s;
                            }
                            tnc_faq.setText("*"+tncFaq);
                            container_faq.addView(view);

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SubscriptionResponse> call, Throwable t) {

            }
        });
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){

            getSupportActionBar().setTitle(getResources().getString(R.string.subscription));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }
    }
}
