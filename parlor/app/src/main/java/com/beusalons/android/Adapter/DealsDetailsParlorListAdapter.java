package com.beusalons.android.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.DealsSalonList.Parlor;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.R;
import com.beusalons.android.SalonPageActivity;
import com.bumptech.glide.Glide;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.snappydb.DB;
import com.snappydb.DBFactory;

import org.greenrobot.eventbus.EventBus;

import java.util.List;


/**
 * Created by Ajay on 1/21/2017.
 */

public class DealsDetailsParlorListAdapter extends RecyclerView.Adapter<DealsDetailsParlorListAdapter.MyViewHolder> {

    private Activity activity;
    private List<Parlor> list;
    private AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;
    private   List<UserServices> userCart;
    public DealsDetailsParlorListAdapter(List<Parlor> list, List<UserServices> userCart, Activity activity) {

        this.list = list;
        this.activity = activity;
        this.userCart=userCart;
        logger = AppEventsLogger.newLogger(activity);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.relative_.setVisibility(View.VISIBLE);

        holder.txt_name.setText(list.get(position).getName());
        holder.txt_address.setText(list.get(position).getAddress1()+ " "+list.get(position).getAddress2());
        Glide.with(activity)
                .load(list.get(position).getImages().get(0).getAppImageUrl())
                .into(holder.img_salon);

        holder.txt_distance.setText(list.get(position).getDistance()+"Km");

        if(list.get(position).getParlorType()==0)
            holder.img_type.setImageResource(R.drawable.ic_premium_badge);
        else if(list.get(position).getParlorType()==1)
            holder.img_type.setImageResource(R.drawable.ic_standard_badge);
        else
            holder.img_type.setImageResource(R.drawable.ic_budget_badge);

        holder.txt_rating.setText(""+list.get(position).getRating());

        int price= list.get(position).getPrice();
        String cost="";
        if(price==1)
            cost= "<font color='#3e780a'>₹</font>"+" "+"₹"+" "+"₹"+" "+"₹"+" "+"₹";
        else if(price==2)
            cost= "<font color='#3e780a'>₹ ₹</font>"+" "+"₹"+" "+"₹"+" "+"₹";
        else if(price==3)
            cost= "<font color='#3e780a'>₹ ₹ ₹</font>"+" "+"₹"+" "+"₹";
        else if(price==4)
            cost= "<font color='#3e780a'>₹ ₹ ₹ ₹</font>"+" "+"₹";
        else
            cost= "<font color='#3e780a'>₹ ₹ ₹ ₹ ₹</font>";
        holder.txt_cost.setText(fromHtml(cost));
//        holder.txt_cost.setText(getServiceName(list.get(position).getDealPrice()));

        if(list.get(position).getFavourite())
            holder.img_fav.setVisibility(View.VISIBLE);
        else
            holder.img_fav.setVisibility(View.GONE);

        if (list.get(position).getDeals()!=null){
            holder.relative_.setVisibility(View.VISIBLE);


            holder.txt_price.setText("₹"+ list.get(position).getDealPrice());
            holder.txt_menu_price.setText( "₹"+list.get(position).getMenuPrice());
            holder.txt_menu_price. setPaintFlags(  holder.txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                double per_discount_= (100- ((int)list.get(position).getDealPrice()*100)
                        /list.get(position).getMenuPrice());
                holder.txt_save_per.setText(AppConstant.SAVE+" "+
                        (int)per_discount_ +"%");


            if (list.get(position).isSelected()){


                holder.ll_deal_container.setVisibility(View.VISIBLE);
                holder.txt_details.setText("Details ˄");



                holder.ll_deal_container.removeAllViews();

                for (int i = 0; i < list.get(position).getDeals().size(); i++) {
                    View view1 = LayoutInflater.from(activity).inflate(R.layout.row_deal_parlor_list, null);
                    TextView dealName = (TextView) view1.findViewById(R.id.txt_deal_name);
                    TextView dealQuantity = (TextView) view1.findViewById(R.id.txt_deal_quantity);
                    TextView dealprice = (TextView) view1.findViewById(R.id.txt_price);
                    TextView dealSave = (TextView) view1.findViewById(R.id.txt_save_per);
                    TextView txt_menu_price = (TextView) view1.findViewById(R.id.txt_menu_price);

                    dealName.setText(getServiceName(list.get(position).getDeals().get(i).getDealId()));
                    dealQuantity.setText(" x "+
                            list.get(position).getDeals().get(i).getQuantity());

                    dealprice.setText("₹" + (int)list.get(position).getDeals().get(i).getDealPrice());
                    txt_menu_price.setText("₹" + (int)list.get(position).getDeals().get(i).getMenuPrice());
                    txt_menu_price.setPaintFlags(holder.txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                    double per_discount=100- (((int)list.get(position).getDeals().get(i).getDealPrice()*100)
                            /list.get(position).getDeals().get(i).getMenuPrice());
                    dealSave.setText(AppConstant.SAVE+" "+
                            (int)per_discount +"%");
                    // if (i!=list.get(position).getDeals().size()-1){

                    //  }
                    holder.ll_deal_container.addView(view1);

                  /*  View seprator=LayoutInflater.from(activity).inflate(R.layout.sepreator,null, false);
                    holder.ll_deal_container.addView(seprator);*/

                }

            }else{

                holder.ll_deal_container.setVisibility(View.GONE);
                holder.txt_details.setText("Details ˃");

            }
            holder.relative_.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!list.get(position).isSelected()) {
                        list.get(position).setSelected(true);
                        updateView(position);

                   /* holder.ll_deal_container.setVisibility(View.VISIBLE);
                    holder.ll_deal_container.removeAllViews();

                    for (int i = 0; i < list.get(position).getDeals().size(); i++) {
                        View view1 = LayoutInflater.from(activity).inflate(R.layout.row_deal_parlor_list, null);
                        TextView dealName = (TextView) view1.findViewById(R.id.txt_deal_name);
                        TextView dealprice = (TextView) view1.findViewById(R.id.txt_price);
                        TextView txt_menu_price = (TextView) view1.findViewById(R.id.txt_menu_price);

                        dealName.setText(getServiceName(list.get(position).getDeals().get(i).getDealId()));
                        dealprice.setText("₹" + String.valueOf(list.get(position).getDeals().get(i).getDealPrice()));
                        txt_menu_price.setText("₹" + String.valueOf(list.get(position).getDeals().get(i).getMenuPrice()));
                        txt_menu_price.setPaintFlags(holder.txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        holder.ll_deal_container.addView(view1);

                    }*/
                    }else { list.get(position).setSelected(false);
                        updateView(position);
                    }

                }
            });


        }else  holder.relative_.setVisibility(View.GONE);



        holder.linear_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {

//                        populateDealData(list.get(position));
                        logSalonListTileEvent(""+position);
                        logSalonClickEvent();
                        logSalonClickFireBaseEvent();
                        Intent intent = new Intent(activity, SalonPageActivity.class);
                        intent.putExtra(SalonPageActivity.HAS_DEAL_DATA, true);
                        intent.putExtra("parlorId", list.get(position).getParlorId());
                        activity.startActivity(intent);
                    }
                }).start();

            }
        });
    }


    private void populateDealData(final Parlor parlor){

        final UserCart userCart= new UserCart();

        userCart.setCartType(AppConstant.SERVICE_TYPE);

        userCart.setParlorId(parlor.getParlorId());
        userCart.setParlorName(parlor.getName());
        userCart.setParlorType(parlor.getParlorType());
        userCart.setGender(parlor.getGender());
        userCart.setRating(parlor.getRating());
        userCart.setOpeningTime(parlor.getOpeningTime());
        userCart.setClosingTime(parlor.getClosingTime());
        userCart.setAddress1(parlor.getAddress1());
        userCart.setAddress2(parlor.getAddress2());

        try {
            DB snappyDB= DBFactory.open(activity);

            UserCart savedCart= null;               //db mai jo saved cart hai
            if(snappyDB.exists(AppConstant.USER_CART_DB)) {

                savedCart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);

                //populating the deal data and setting the price...
                for(int i=0; i<savedCart.getServicesList().size(); i++){

                    Log.e("dealskakandi", "value in service_deal_id: "+ savedCart.getServicesList().get(i).getService_deal_id());
                    if (parlor.getDeals()!=null) {
                        for (int j = 0; j < parlor.getDeals().size(); j++) {

                            if (savedCart.getServicesList().get(i).getDealId() == parlor.getDeals().get(j).getDealId()) {

                                Log.i("dealprice", "value in price: "+ savedCart.getServicesList().get(i).getPrice());

                                savedCart.getServicesList().get(i).setService_deal_id(
                                        parlor.getDeals().get(j).getParlorDealId());
                                savedCart.getServicesList().get(i).setPrice(
                                        parlor.getDeals().get(j).getDealPrice());
                                savedCart.getServicesList().get(i).setMenu_price(
                                        parlor.getDeals().get(j).getMenuPrice());
                                savedCart.getServicesList().get(i).setService_id(
                                        parlor.getDeals().get(j).getServiceId());
                            }
                        }
                    } else {

                        for (int k = 0; k < parlor.getParlorservices().size(); k++) {

                            if (savedCart.getServicesList().get(i).getService_code() == parlor.getParlorservices().get(k).getServiceCode()) {

                                savedCart.getServicesList().get(i).setService_code(parlor.getParlorservices().get(k).getServiceCode());
                                savedCart.getServicesList().get(i).setName(parlor.getParlorservices().get(k).getName());
                                savedCart.getServicesList().get(i).setQuantity(parlor.getParlorservices().get(k).getQuantity());
                                savedCart.getServicesList().get(i).setMemberShipRemaingType("membership");
                                savedCart.getServicesList().get(i).setMyMembershipFreeService(true);
                                savedCart.getServicesList().get(i).setType("membership");

                            }
                        }


                    }
                    Log.e("dealskakandi", "value in service_deal_id: "+ savedCart.getServicesList().get(i).getService_deal_id());
                }

//                removing the service jo selected parlor provide nai karta
                for(int i=0; i<savedCart.getServicesList().size(); i++){

                    if((savedCart.getServicesList().get(i).getPrice()==0 ||
                            savedCart.getServicesList().get(i).getPrice() == 0.0 ) && !savedCart.getServicesList().get(i).isMyMembershipFreeService()){
                        savedCart.getServicesList().remove(i);
                    }
                }
                userCart.setServicesList(savedCart.getServicesList());      //user cart mai service dala

                snappyDB.put(AppConstant.USER_CART_DB, userCart);
                snappyDB.close();

                EventBus.getDefault().post(userCart);
            }else{

                snappyDB.close();
            }





        }catch (Exception e){

            e.printStackTrace();
        }
    }

    private void updateView(int pos){


        for(int i=0;i<list.size();i++){

            if(i!=pos){
                list.get(i).setSelected(false);

            }
        }
        notifyDataSetChanged();
    }
    public void logSalonClickEvent () {
        Log.e("SalonClick","fine");
        logger.logEvent(AppConstant.SalonClick);
    }
    public void logSalonClickFireBaseEvent () {
        Log.e("SalonClickFirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.SalonClick,bundle);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_salon, img_type, img_fav;
        private TextView txt_name, txt_address, txt_cost, txt_rating, txt_distance, txt_save_per, txt_menu_price, txt_price,txt_details;
        private LinearLayout linear_click,ll_deal_container;
        private RelativeLayout relative_;

        public MyViewHolder(View view) {
            super(view);

            img_salon= (ImageView)view.findViewById(R.id.img_salon);
            img_type= (ImageView)view.findViewById(R.id.img_type);
            img_fav= (ImageView)view.findViewById(R.id.img_fav);
            txt_name= (TextView) view.findViewById(R.id.txt_name);
            txt_address= (TextView)view.findViewById(R.id.txt_address);
            txt_cost= (TextView)view.findViewById(R.id.txt_cost);
            txt_rating= (TextView)view.findViewById(R.id.txt_rating);
            txt_distance= (TextView)view.findViewById(R.id.txt_distance);
            txt_save_per= (TextView)view.findViewById(R.id.txt_save_per);
            txt_menu_price= (TextView)view.findViewById(R.id.txt_menu_price);
            txt_price= (TextView)view.findViewById(R.id.txt_price);
            txt_details= (TextView)view.findViewById(R.id.txt_details);

            relative_= (RelativeLayout)view.findViewById(R.id.relative_);
            linear_click= (LinearLayout)view.findViewById(R.id.linear_click);
            ll_deal_container= (LinearLayout)view.findViewById(R.id.ll_deal_container);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.salon_list, parent, false);
        return new MyViewHolder(itemView);
    }
    public void logSalonListTileEvent (String index) {
        Log.e("SalonClickFirebase","fine"+index);

        Bundle params = new Bundle();
        params.putString("index", index);
        logger.logEvent(AppConstant.SalonListTile, params);
    }
    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }
    private String getServiceName(int dealId){
        String name="";
        for (int i=0;i<userCart.size();i++){
            if (userCart.get(i).getDealId()==dealId){
                name=userCart.get(i).getName();
                return userCart.get(i).getName();
            }
        }


        return name;
    }
}
