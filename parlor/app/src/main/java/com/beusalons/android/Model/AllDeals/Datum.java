package com.beusalons.android.Model.AllDeals;

import java.util.List;

/**
 * Created by myMachine on 4/28/2017.
 */

public class Datum {

    public Integer categoryId;
    public Integer type;
    public String name;
    public List<Service> services = null;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }
}
