package com.beusalons.android.Helper;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.beusalons.android.Model.AppCloseResponse;
import com.beusalons.android.Model.Subscription_post;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.snappydb.DB;
import com.snappydb.DBFactory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MyLifecycleHandler1 implements Application.ActivityLifecycleCallbacks {
    // I use four separate variables here. You can, of course, just use two and
    // increment/decrement them instead of using four and incrementing them all.
    UserCart saved_cart= null;
    private static int resumed;
    private static int paused;
    private static int started;
    private static int stopped;



    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {

        Log.e("test","appdestroy");
    }

    @Override
    public void onActivityResumed(Activity activity) {
        ++resumed;
    }




    @Override
    public void onActivityPaused(Activity activity) {
        ++paused;
        android.util.Log.w("test", "application is in foreground: " + (resumed > stopped));

    }





    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

        ++started;

    }

    @Override
    public void onActivityStopped(Activity activity) {
        ++stopped;
        android.util.Log.w("test", "application is visible: " + (resumed > stopped));
         if (!isAppBackground()  &&  getCardData(activity)>0)
             killAppPost(activity);
    }

    public boolean isAppBackground(){

        return resumed > stopped;

    }

    private int getCardData(Activity activity){
        try {

            DB snappyDB= DBFactory.open(activity);
            saved_cart= null;               //db mai jo saved cart hai
            if(snappyDB.exists(AppConstant.USER_CART_DB)){

                saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
                snappyDB.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        if(saved_cart!=null &&
                saved_cart.getServicesList().size()>0)
            return saved_cart.getServicesList().size();
        return 0;
    }

    private void killAppPost(Activity activity ){
//        Toast.makeText(activity,"ho gya band",Toast.LENGTH_SHORT).show();
        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface=retrofit.create(ApiInterface.class);
        Subscription_post userCartPost=new Subscription_post();
        userCartPost.setUserId(BeuSalonsSharedPrefrence.getUserId());

        Call<AppCloseResponse> call=apiInterface.onAppClose(userCartPost);

        call.enqueue(new Callback<AppCloseResponse>() {
            @Override
            public void onResponse(Call<AppCloseResponse> call, Response<AppCloseResponse> response) {
                if (response.isSuccessful()){

                }
            }

            @Override
            public void onFailure(Call<AppCloseResponse> call, Throwable t) {


            }
        });

    }
}