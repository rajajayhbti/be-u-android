package com.beusalons.android.Model.newServiceDeals.NewCombo;

import com.beusalons.android.Model.DealsServices.ParlorTypes;

import java.util.List;

/**
 * Created by myMachine on 6/17/2017.
 */

//--
public class Product {

    private String productId;
    private String productName;
    private List<Service_> services = null;
    private int price;
    private int menuPrice;
    private String title;
    private String name;
    private double ratio;
    private boolean maxSaving;
    private boolean popularChoice;
    private String lowest;
    private String saveUpto;
    private List<ParlorTypes> parlorTypes;

    private boolean isCheck= false;

    public List<ParlorTypes> getParlorTypes() {
        return parlorTypes;
    }

    public void setParlorTypes(List<ParlorTypes> parlorTypes) {
        this.parlorTypes = parlorTypes;
    }

    public String getLowest() {
        return lowest;
    }

    public void setLowest(String lowest) {
        this.lowest = lowest;
    }

    public String getSaveUpto() {
        return saveUpto;
    }

    public void setSaveUpto(String saveUpto) {
        this.saveUpto = saveUpto;
    }

    public boolean isMaxSaving() {
        return maxSaving;
    }

    public void setMaxSaving(boolean maxSaving) {
        this.maxSaving = maxSaving;
    }

    public boolean isPopularChoice() {
        return popularChoice;
    }

    public void setPopularChoice(boolean popularChoice) {
        this.popularChoice = popularChoice;
    }

    public double getRatio() {
        return ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public List<Service_> getServices() {
        return services;
    }

    public void setServices(List<Service_> services) {
        this.services = services;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(int menuPrice) {
        this.menuPrice = menuPrice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }
}
