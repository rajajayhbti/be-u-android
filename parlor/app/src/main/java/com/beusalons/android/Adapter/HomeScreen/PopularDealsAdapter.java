package com.beusalons.android.Adapter.HomeScreen;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.HomeFragmentModel.PopularDeals;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.ParlorListActivity;
import com.beusalons.android.R;
import com.beusalons.android.Task.UserCartTask;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashish  on 3/2/2017.
 */

public class PopularDealsAdapter extends RecyclerView.Adapter<PopularDealsAdapter.ViewHolder> {

    AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;

    private Context context;
    private List<PopularDeals> list;


    public  PopularDealsAdapter(Context mcontext,List<PopularDeals> list){

        this.list=list;
        this.context=mcontext;
        if (context!=null){
            logger = AppEventsLogger.newLogger(mcontext);
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        }

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_popular_deals, parent, false);
        return new PopularDealsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final PopularDeals deals= list.get(position);

        holder.tvDealName.setText(deals.getCategory());
        holder.tvDealPrice.setText(fromHtml("₹"+ +deals.getPrice()));
        holder.tvDealSave.setText("Save "+deals.getDiscount()+"%");
        Glide.with(context).load(deals.getImage()).apply(RequestOptions.circleCropTransform()).into(holder.imgDeals);
       /* Glide.with(context).
                load(deals.getImage()).asBitmap().
                centerCrop().
                into(new BitmapImageViewTarget(holder.imgDeals) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        holder.imgDeals.setImageDrawable(circularBitmapDrawable);
                    }
                });*/

        holder.cardViewDeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                logPopularDealsHomeButtonEvent(deals.getCategory());
                logPopularDealsHomeButtonFireBaseEvent(deals.getCategory());

                UserCart user_cart= new UserCart();
                user_cart.setCartType(AppConstant.DEAL_TYPE);

                UserServices services= new UserServices();

                services.setName(deals.getCategory());
                services.setType(deals.getDealType());

                services.setDealId(deals.getDealId());

                String brand_id= deals.getBrandId()==null?"":deals.getBrandId();
                String product_id= deals.getProductId()==null?"":deals.getProductId();

                services.setService_code(deals.getServiceCodes().get(0));
                services.setPrice_id(deals.getServiceCodes().get(0));

                services.setBrand_id(brand_id);
                services.setProduct_id(product_id);

                String primary_key= ""+deals.getServiceCodes().get(0)+ deals.getDealId()+brand_id+product_id;
                Log.i("populardeals", "value in: "+primary_key);
                services.setPrimary_key(primary_key);

                new Thread(new UserCartTask(context, user_cart, services, false, false)).start();

                Toast.makeText(context, deals.getCategory()+" Service Added to Cart!", Toast.LENGTH_LONG).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Intent intent=new Intent(context, ParlorListActivity.class);
                        intent.putExtra("isDeal", true);
                        context.startActivity(intent);
                    }
                }, 250);
            }
        });
    }
    public void setData(ArrayList<PopularDeals> list){
        this.list=list;
        notifyDataSetChanged();

    }

    public void logPopularDealsHomeButtonEvent (String dealName) {
        Log.e("PopularDealsHomeButton","fine");
        Bundle params = new Bundle();
        params.putString("DealName", dealName);
        logger.logEvent(AppConstant.PopularDealsHomeButton, params);
    }
    public void logPopularDealsHomeButtonFireBaseEvent (String dealName) {
        Log.e("PopularDealsHomefire","fine");
        Bundle params = new Bundle();
        params.putString("DealName", dealName);
        mFirebaseAnalytics.logEvent(AppConstant.PopularDealsHomeButton, params);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imgDeals;
        TextView tvDealName;
        TextView tvDealPrice;
        TextView  tvDealSave;
        CardView cardViewDeal;

        public ViewHolder(View itemView) {
            super(itemView);
            cardViewDeal=(CardView)itemView.findViewById(R.id.cardViewDeals);
            imgDeals= (ImageView) itemView.findViewById(R.id.img_popular_deals);
            tvDealName= (TextView) itemView.findViewById(R.id.tv_deals_cate_popular);
            tvDealPrice= (TextView) itemView.findViewById(R.id.tv_deals_price_popular);
            tvDealSave= (TextView) itemView.findViewById(R.id.tv_deals_save_popular);
        }
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

}
