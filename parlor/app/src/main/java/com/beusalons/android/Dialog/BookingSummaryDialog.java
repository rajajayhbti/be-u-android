package com.beusalons.android.Dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Adapter.NewServiceDeals.BookingSummaryMembershipAdapter;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.SalonHome.HomeResponse;
import com.beusalons.android.Model.SalonHome.Membership;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.R;
import com.google.gson.Gson;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
/**
 * Created by Ajay on 7/10/2017.
 */

public class BookingSummaryDialog extends DialogFragment{
   // final Dialog dialog;
    private RecyclerView recMemebershipBillSummary;
    private BookingSummaryMembershipAdapter membershipAdapter;
    private ImageView imgCancel;
    private TextView txtViewBillSummaryHeader,txtViewBillsummarySubTitle,txtViewYourPuchaseTitle,
            txtViewYourMembership,txtViewYourPurchase,txtViewDiscount,
            txtViewRemainingBalance,txtViewFreebee,txtViewMembershipDiscount,
            txtViewMembershipRemainingBalance,txtViewMembershipFreebee,txtViewNoThanks,txtViewBuyMembership;
    String strHeader,strSubTitle;
    private  HomeResponse homeResponse;
    private LinearLayout linearLayout_cancel;
    private UserCart cart;
    String balance;
    private double priceSubTotal=0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);          //no action bar space
        View view = inflater.inflate(R.layout.bill_summary_dialog, container);
        getBundleData();

        recMemebershipBillSummary=(RecyclerView)view.findViewById(R.id.rec_memebership_bill_summary);
        imgCancel=(ImageView)view.findViewById(R.id.img_cancel);

        txtViewBillSummaryHeader=(TextView)view.findViewById(R.id.txtView_billsummary_header);
        txtViewBillsummarySubTitle=(TextView)view.findViewById(R.id.txtView_billsummary_subtitle);
        txtViewYourPuchaseTitle=(TextView)view.findViewById(R.id.txtView_your_puchase_title);
        txtViewYourMembership=(TextView)view.findViewById(R.id.txtView_your_membership);
        txtViewYourPurchase=(TextView)view.findViewById(R.id.txtView_your_purchase);
        txtViewDiscount=(TextView)view.findViewById(R.id.txtView_discount);
        txtViewRemainingBalance=(TextView)view.findViewById(R.id.txtView_remaining_balance);
        txtViewFreebee=(TextView)view.findViewById(R.id.txtView_freebee);
        txtViewMembershipDiscount=(TextView)view.findViewById(R.id.txtView_membership_discount);
        txtViewMembershipRemainingBalance=(TextView)view.findViewById(R.id.txtView_membership_remaining_balance);
        txtViewMembershipFreebee=(TextView)view.findViewById(R.id.txtView_membership_freebee);
        txtViewNoThanks=(TextView)view.findViewById(R.id.txtView_no_thanks);
        txtViewBuyMembership=(TextView)view.findViewById(R.id.txtView_buy_membership);
        linearLayout_cancel=(LinearLayout)view.findViewById(R.id.linearLayout_cancel);
        inItView();
        inItListner();
        return view;
    }


    private void getServicesPrice(Membership membershipList){
      double  remainingBalance=0,price=0,discount=0,discountSubtotal=0, priceTotal=0;
        int quantity=0;
       for (int i=0;i<cart.getServicesList().size();i++){
           if (!cart.getServicesList().get(i).isMembership()) {
               if (cart.getServicesList().get(i).getType().equals("service")) {
                   quantity=cart.getServicesList().get(i).getQuantity();
                   discount = (cart.getServicesList().get(i).getPrice() * membershipList.getNormalPercentage()) / 100;
                   price += ((cart.getServicesList().get(i).getPrice() - discount)*quantity);
                   discountSubtotal += (discount*quantity);
                   priceTotal +=(cart.getServicesList().get(i).getPrice()*quantity);
                   Log.e("total","discount" +discount +"priceSubTotal" +priceTotal +"discountSubTotal"+discountSubtotal+"price"+price+"quantity"+quantity);

               } else {
                   quantity=cart.getServicesList().get(i).getQuantity();
                   discount = (cart.getServicesList().get(i).getPrice() * membershipList.getDealPercentage()) / 100;
                   price += ((cart.getServicesList().get(i).getPrice() - discount)*quantity);
                   discountSubtotal += (discount*quantity);
                   priceTotal +=(cart.getServicesList().get(i).getPrice()*quantity);

                   Log.e("total","discount" +discount +"priceSubTotal" +priceTotal +"discountSubTotal"+discountSubtotal+"price"+price+"quantity"+quantity);
               }
           }
       }
        txtViewDiscount.setText(""+discountSubtotal);
        txtViewMembershipDiscount.setText(""+discountSubtotal);
        txtViewYourPuchaseTitle.setText(""+priceTotal);
        txtViewYourPurchase.setText("(-)"+priceTotal);
        remainingBalance=Double.valueOf(balance)-price;
        txtViewMembershipRemainingBalance.setText(""+remainingBalance);
        txtViewRemainingBalance.setText(""+remainingBalance);


    }

    public void cartData(){
        DB snappyDB = null;
        try {
            snappyDB = DBFactory.open(getActivity());
            if (snappyDB.exists(AppConstant.USER_CART_DB)) {
                    cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
            }
            snappyDB.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }

    }

    private void inItView(){
        strHeader = " Your Bill Summary <font color=#d2232a>Without</font> Membership Card";
        strSubTitle=" Your Bill Summary <font color=#d2232a>with</font> Membership Card";
        txtViewBillSummaryHeader.setText(fromHtml(strHeader));
        txtViewBillsummarySubTitle.setText(fromHtml(strSubTitle));
        recMemebershipBillSummary.hasFixedSize();
        LinearLayoutManager layoutManager1= new LinearLayoutManager( getActivity(),LinearLayoutManager.HORIZONTAL, false);
        recMemebershipBillSummary.setLayoutManager(layoutManager1);
        membershipAdapter= new BookingSummaryMembershipAdapter(getActivity(), homeResponse.getData().getMemberships());
        recMemebershipBillSummary.setAdapter(membershipAdapter);


    }

    private void getTotalPrice(){
        double quantity=0;
        for (int i=0;i<cart.getServicesList().size();i++){
            if (!cart.getServicesList().get(i).isMembership()) {
                if (cart.getServicesList().get(i).getType().equals("service")) {
                    quantity=cart.getServicesList().get(i).getQuantity();
                    priceSubTotal +=(cart.getServicesList().get(i).getPrice()*quantity);

                } else {
                    quantity=cart.getServicesList().get(i).getQuantity();
                    priceSubTotal +=(cart.getServicesList().get(i).getPrice()*quantity);
                }
            }
        }
    }
    private void getBundleData(){
        Bundle bundle= getArguments();
        if (bundle!=null){
            homeResponse=new Gson().fromJson(bundle.getString("membership",null),HomeResponse.class);

        }
    }

    private void inItListner(){
        cartData();
        getTotalPrice();
        getSelctedMemberShip();
        linearLayout_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             dismiss();
            }
        });
        txtViewNoThanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
     }

    private  void getSelctedMemberShip(){
        boolean selected=true;
        for (int i=0;i<homeResponse.getData().getMemberships().size();i++){
            for (int j=0;j<homeResponse.getData().getMemberships().get(i).getDetails().size();j++){
                String price=homeResponse.getData().getMemberships().get(i).getDetails().get(j).getValues().get(3).getValue();
                double membershipPrice=Double.valueOf(price.substring(2,price.length()));
                if ((membershipPrice>priceSubTotal) && selected){
                    homeResponse.getData().getMemberships().get(i).setSelectedMembership(true);
                    setSelectedData(homeResponse.getData().getMemberships().get(i));
                    selected=false;
                }
            }
        }
    }
    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    private void setSelectedData(Membership membership){

        txtViewYourMembership.setText(membership.getDetails().get(0).getValues().get(3).getValue());
        txtViewFreebee.setText(membership.getDetails().get(0).getValues().get(3).getValue().substring(2,
                membership.getDetails().get(0).getValues().get(3).getValue().length()));
        balance=membership.getDetails().get(0).getValues().get(3).getValue().substring(2,
                membership.getDetails().get(0).getValues().get(3).getValue().length());
        txtViewMembershipFreebee.setText(balance);

        getServicesPrice(membership);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Membership event) {
        /* Do something */
        setSelectedData(event);
    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {

            Window window = getDialog().getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFFFFF")));
//            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


        }catch (Exception e){

            e.printStackTrace();
        }

    }


}
