package com.beusalons.android.Model;

/**
 * Created by Ajay on 2/28/2018.
 */

public class Subscription_post {
    private String userId;
    private String accessToken;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
