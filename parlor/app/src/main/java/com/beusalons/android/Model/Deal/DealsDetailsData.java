package com.beusalons.android.Model.Deal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ajay on 1/20/2017.
 */

public class DealsDetailsData {

    private Integer dealFinalPrice;
    @SerializedName("menuPrice")
    @Expose
    private Integer menuPrice;
    @SerializedName("dealPrice")
    @Expose
    private Integer dealPrice;
    @SerializedName("deals")

    @Expose
    private List<DealsDetails> deals=new ArrayList<>();
    @SerializedName("parlorId")
    @Expose
    private String parlorId;

    @SerializedName("parlorName")
    @Expose
    private String parlorName;


    @SerializedName("parlorAddress1")
    @Expose
    private String parlorAddress1;
    @SerializedName("parlorAddress2")
    @Expose
    private String parlorAddress2;
    @SerializedName("distance")
    @Expose
    private float distance;
    @SerializedName("rating")
    @Expose
    private int rating;

    private int parlorType;
    private boolean favourite;

    public int getParlorType() {
        return parlorType;
    }

    public void setParlorType(int parlorType) {
        this.parlorType = parlorType;
    }

    @SerializedName("parlorImage")
    @Expose
    private List<DealsImageUrl> parlorImage =new ArrayList<>();

    public Integer getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(Integer menuPrice) {
        this.menuPrice = menuPrice;
    }

    public Integer getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(Integer dealPrice) {
        this.dealPrice = dealPrice;
    }

    public List<DealsDetails> getDeals() {
        return deals;
    }

    public void setDeals(List<DealsDetails> deals) {
        this.deals = deals;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }

    public String getParlorName() {
        return parlorName;
    }

    public void setParlorName(String parlorName) {
        this.parlorName = parlorName;
    }

    public String getParlorAddress1() {
        return parlorAddress1;
    }

    public void setParlorAddress1(String parlorAddress1) {
        this.parlorAddress1 = parlorAddress1;
    }

    public String getParlorAddress2() {
        return parlorAddress2;
    }

    public void setParlorAddress2(String parlorAddress2) {
        this.parlorAddress2 = parlorAddress2;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public List<DealsImageUrl> getParlorImage() {
        return parlorImage;
    }

    public void setParlorImage(List<DealsImageUrl> parlorImage) {
        this.parlorImage = parlorImage;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }
}
