package com.beusalons.android.Model.ArtistProfile;

/**
 * Created by Ajay on 1/31/2018.
 */

public class ArtistCreativeField_ {

    private String catId;
    private String collectionId;
    private String collectionName;
    private String _id;

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(String collectionId) {
        this.collectionId = collectionId;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
