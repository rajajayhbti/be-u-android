package com.beusalons.android.Model.Favourites;

/**
 * Created by myMachine on 3/31/2017.
 */

public class SalonFavResponse {

    private boolean success;
    private String data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
