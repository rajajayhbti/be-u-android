package com.beusalons.android.Adapter.FreeBies;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beusalons.android.Adapter.DealsFragmentAdapter;
import com.beusalons.android.Model.Loyalty.NewFreeBy;
import com.beusalons.android.ParlorListActivity;
import com.beusalons.android.R;
import com.beusalons.android.WebViewActivity;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by Ashish Sharma on 9/7/2017.
 */

public class FreebiesCategoryAdapter extends RecyclerView.Adapter<FreebiesCategoryAdapter.ViewHolder> {
    private Context context;
    private List<NewFreeBy > freeByList;


    public FreebiesCategoryAdapter(Context context,List<NewFreeBy > freeByList) {
        this.context=context;
        this.freeByList=freeByList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.row_new_freebie, parent, false);
        return new FreebiesCategoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final NewFreeBy freeBy=freeByList.get(position);

        holder.txt_name.setText(freeBy.getTitle());
        holder.txt_info.setText(freeBy.getMessage());
        final int pos=position;
        Glide.with(context)
                .load(freeBy.getImage())
//                .centerCrop()
                .into(holder.img_);

        holder.linear_click.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
        holder.Linear_share.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
        holder.view1.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
        holder.txt_name_outer.setText(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoTitle());
        holder.txt_info_outer.setText(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(0));
        holder.linear_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (freeByList.get(pos).isSelected()){

                    holder.img_animation.setImageResource(R.drawable.ic_right);
                    holder.inside_array.setVisibility(View.GONE);
                    freeByList.get(pos).setSelected(false);
                }else{
//                    final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,1);

                    freeByList.get(pos).setSelected(true);
                    holder.img_animation.setImageResource(R.drawable.ic_arrow_down);


                    if (freeBy.getInsideArray().size()==1){
                        holder.inside_array.setVisibility(View.VISIBLE);
                        Glide.with(context)
                                .load(freeBy.getInsideArray().get(0).getInsideImage())
//                .centerCrop()
                                .into(holder.img_title);
                        holder.txt_title.setText(freeBy.getInsideArray().get(0).getInsideTitle());

                        Glide.with(context)
                                .load(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoIcon())
//                .centerCrop()
                                .into(holder.img_outer);
                        holder.img_2.setVisibility(View.INVISIBLE);
                        holder.img_3.setVisibility(View.INVISIBLE);
                        holder.txt_title2.setVisibility(View.INVISIBLE);
                        holder.txt_title3.setVisibility(View.INVISIBLE);
                        holder.linear_click2.setClickable(false);
                        holder.linear_click3.setClickable(false);

                        holder.linear_click.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                freeBy.getInsideArray().get(0).setSelected(true);
                                holder.linear_click.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
                                holder.Linear_share.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
                                holder.view1.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));



//                                params.setMargins(1, 1, 1, 0);
//                                holder.linear_click.setLayoutParams(params);
                                if (freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().size()>1){

                                    String txt="";
                                    for(int t=0;t<freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().size();t++){

                                        txt= txt+ "\u2022 " + freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(t).toString() + "\n";

                                    }
                                    holder.txt_info_outer.setText(txt);

                                }else holder.txt_info_outer.setText(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(0));


                            }
                        });






                        //  holder.txt_name_outer.setText(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoTitle());
                        //  holder.txt_info_outer.setText(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(0));
                    }

                    /**
                     * gfdgfdgfdgfd
                     */

                    if (freeBy.getInsideArray().size()==2){
                        holder.inside_array.setVisibility(View.VISIBLE);
                        holder.img_2.setVisibility(View.VISIBLE);
                        holder.img_3.setVisibility(View.INVISIBLE);
                        holder.txt_title2.setVisibility(View.VISIBLE);
                        holder.txt_title3.setVisibility(View.INVISIBLE);
                        holder.linear_click2.setClickable(true);
//                        holder.linear_click.setClickable(true);
                        holder.linear_click3.setClickable(false);
                        Glide.with(context)
                                .load(freeBy.getInsideArray().get(0).getInsideImage())
//                .centerCrop()
                                .into(holder.img_title);
                        holder.txt_title.setText(freeBy.getInsideArray().get(0).getInsideTitle());

                        Glide.with(context)
                                .load(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoIcon())
//                .centerCrop()
                                .into(holder.img_outer);

                        Glide.with(context)
                                .load(freeBy.getInsideArray().get(1).getInsideImage())
//                .centerCrop()
                                .into(holder.img_2);
                        holder.txt_title2.setText(freeBy.getInsideArray().get(1).getInsideTitle());






                        holder.linear_click.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.e("linear_click", "onClick: "+ freeBy.getInsideArray().get(0).getInsideTitle());
                                freeBy.getInsideArray().get(0).setSelected(true);
                                holder.linear_click.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
                                holder.Linear_share.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
                                holder.view1.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
                                holder.linear_click2.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                                holder.view2.setBackgroundColor(ContextCompat.getColor(context, R.color.black_ninety));
//                                params.setMargins(1, 1, 1, 0);
//                                holder.linear_click.setLayoutParams(params);
                                holder.txt_name_outer.setText(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoTitle());
//                                holder.txt_info_outer.setText(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(0));

                                if (freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().size()>1){

                                    String txt="";
                                    for(int t=0;t<freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().size();t++){

                                        txt= txt+ "\u2022 " + freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(t).toString() + "\n";

                                    }
                                    holder.txt_info_outer.setText(txt);

                                }else holder.txt_info_outer.setText(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(0));

                               /* if (freeBy.getInsideArray().get(0).getArrayInfo().get(0).getButtons().size()==1){
                                    holder.btnFirst.setVisibility(View.VISIBLE);
                                    holder.btnFirst.setText(freeBy.getInsideArray().get(1).getArrayInfo().get(0).getButtons().get(0).getButtonTitle());
//                               notifyItemChanged(position);
                                }
                                if (freeBy.getInsideArray().get(0).getArrayInfo().get(0).getButtons().size()==2){
                                    holder.btnSecond.setVisibility(View.VISIBLE);
                                    holder.btnSecond.setText(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getButtons().get(1).getButtonTitle());
//                                    notifyItemChanged(position);
                                }*/

                            }
                        });


                        holder.linear_click2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.e("linear_click2", "onClick: "+ freeBy.getInsideArray().get(1).getInsideTitle());
                                freeBy.getInsideArray().get(1).setSelected(true);
                                holder.linear_click.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
//                                holder.Linear_share.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
                                holder.view1.setBackgroundColor(ContextCompat.getColor(context, R.color.black_ninety));

                                holder.linear_click2.setBackgroundColor(ContextCompat.getColor(context, R.color.light_yellow));
                                holder.Linear_share.setBackgroundColor(ContextCompat.getColor(context, R.color.light_yellow));
                                holder.view2.setBackgroundColor(ContextCompat.getColor(context, R.color.light_yellow));
                                holder.txt_name_outer.setText(freeBy.getInsideArray().get(1).getArrayInfo().get(0).getInfoTitle());
                                if (freeBy.getInsideArray().get(1).getArrayInfo().get(0).getInfoMessage().size()>1){

                                    String txt="";
                                    for(int t=0;t<freeBy.getInsideArray().get(1).getArrayInfo().get(0).getInfoMessage().size();t++){

                                        txt= txt+ "\u2022 " + freeBy.getInsideArray().get(1).getArrayInfo().get(0).getInfoMessage().get(t).toString() + "\n";

                                    }
                                    holder.txt_info_outer.setText(txt);

                                }else holder.txt_info_outer.setText(freeBy.getInsideArray().get(1).getArrayInfo().get(0).getInfoMessage().get(0));
//                                holder.txt_info_outer.setText(freeBy.getInsideArray().get(1).getArrayInfo().get(0).getInfoMessage().get(0));
//                                params.setMargins(1, 1, 1, 0);
//                                holder.linear_click.setLayoutParams(params);
                              /*  if (freeBy.getInsideArray().get(1).getArrayInfo().get(0).getButtons().size()==1){
                                    Log.e("linear_click2", "1: "+ freeBy.getInsideArray().get(1).getInsideTitle());
                                    holder.btnFirst.setVisibility(View.VISIBLE);
                                    holder.btnFirst.setText(freeBy.getInsideArray().get(1).getArrayInfo().get(0).getButtons().get(0).getButtonTitle());
//                               notifyItemChanged(position);
                                }
                                if (freeBy.getInsideArray().get(1).getArrayInfo().get(0).getButtons().size()==2){
                                    Log.e("button", "2: "+ freeBy.getInsideArray().get(1).getInsideTitle());
                                    holder.btnSecond.setVisibility(View.VISIBLE);
                                    holder.btnSecond.setText(freeBy.getInsideArray().get(1).getArrayInfo().get(0).getButtons().get(1).getButtonTitle());
//                                    notifyItemChanged(position);
                                }*/

                            }
                        });





                    }
                    /**
                     * 3 ka case
                     */
                    if (freeBy.getInsideArray().size()==3){
                        holder.inside_array.setVisibility(View.VISIBLE);
                        Glide.with(context)
                                .load(freeBy.getInsideArray().get(0).getInsideImage())
//                .centerCrop()
                                .into(holder.img_title);
                        holder.txt_title.setText(freeBy.getInsideArray().get(0).getInsideTitle());

                        Glide.with(context)
                                .load(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoIcon())
//                .centerCrop()
                                .into(holder.img_outer);


                        holder.img_2.setVisibility(View.VISIBLE);
                        holder.img_3.setVisibility(View.VISIBLE);
                        holder.txt_title2.setVisibility(View.VISIBLE);
                        holder.txt_title3.setVisibility(View.VISIBLE);
                        holder.linear_click2.setClickable(true);
//                        holder.linear_click.setClickable(true);
                        holder.linear_click3.setClickable(true);



                        holder.linear_click.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                freeBy.getInsideArray().get(0).setSelected(true);
                                holder.linear_click.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
                                holder.Linear_share.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
                                holder.view1.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
//                                holder.linear_click.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
//
                                holder.linear_click2.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                                holder.view2.setBackgroundColor(ContextCompat.getColor(context, R.color.black_ninety));
                                holder.view3.setBackgroundColor(ContextCompat.getColor(context, R.color.black_ninety));
                                holder.linear_click3.setBackgroundColor(ContextCompat.getColor(context, R.color.white));

//                                holder.Linear_share.setBackgroundColor(ContextCompat.getColor(context, R.color.memberShip_background));
                                holder.txt_name_outer.setText(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoTitle());
                                if (freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().size()>1){

                                    String txt="";
                                    for(int t=0;t<freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().size();t++){

                                        txt= txt+ "\u2022 " + freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(t).toString() + "\n";

                                    }
                                    holder.txt_info_outer.setText(txt);

                                }else holder.txt_info_outer.setText(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(0));
//                                holder.txt_info_outer.setText(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(0));


                            }
                        });

                        Glide.with(context)
                                .load(freeBy.getInsideArray().get(1).getInsideImage())
//                .centerCrop()
                                .into(holder.img_2);
                        holder.txt_title2.setText(freeBy.getInsideArray().get(1).getInsideTitle());

                        Glide.with(context)
                                .load(freeBy.getInsideArray().get(1).getArrayInfo().get(0).getInfoIcon())
//                .centerCrop()
                                .into(holder.img_outer);
                        holder.linear_click2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                freeBy.getInsideArray().get(1).setSelected(true);
                                holder.linear_click.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
//                                holder.Linear_share.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
                                holder.view1.setBackgroundColor(ContextCompat.getColor(context, R.color.black_ninety));

                                holder.linear_click2.setBackgroundColor(ContextCompat.getColor(context, R.color.light_yellow));
                                holder.linear_click3.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                                holder.Linear_share.setBackgroundColor(ContextCompat.getColor(context, R.color.light_yellow));
                                holder.view2.setBackgroundColor(ContextCompat.getColor(context, R.color.light_yellow));
                                holder.txt_name_outer.setText(freeBy.getInsideArray().get(1).getArrayInfo().get(0).getInfoTitle());
//                                holder.txt_info_outer.setText(freeBy.getInsideArray().get(1).getArrayInfo().get(0).getInfoMessage().get(0));
//                                params.setMargins(1, 1, 1, 0);
//                                holder.linear_click.setLayoutParams(params);
                                if (freeBy.getInsideArray().get(1).getArrayInfo().get(0).getInfoMessage().size()>1){

                                    String txt="";
                                    for(int t=0;t<freeBy.getInsideArray().get(1).getArrayInfo().get(0).getInfoMessage().size();t++){

                                        txt= txt+ "\u2022 " + freeBy.getInsideArray().get(1).getArrayInfo().get(0).getInfoMessage().get(t).toString() + "\n";

                                    }
                                    holder.txt_info_outer.setText(txt);

                                }else holder.txt_info_outer.setText(freeBy.getInsideArray().get(1).getArrayInfo().get(0).getInfoMessage().get(0));

                            }
                        });


                        Glide.with(context)
                                .load(freeBy.getInsideArray().get(2).getInsideImage())
//                .centerCrop()
                                .into(holder.img_3);

                        holder.txt_title3.setText(freeBy.getInsideArray().get(2).getInsideTitle());

                        Glide.with(context)
                                .load(freeBy.getInsideArray().get(2).getArrayInfo().get(0).getInfoIcon())
//                .centerCrop()
                                .into(holder.img_outer);
                        holder.linear_click3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
//                                freeBy.getInsideArray().get(2).setSelected(true);
                                holder.linear_click.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
//                                holder.Linear_share.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
                                holder.view1.setBackgroundColor(ContextCompat.getColor(context, R.color.black_ninety));

                                holder.linear_click2.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
//                                holder.Linear_share.setBackgroundColor(ContextCompat.getColor(context, R.color.light_yellow));
                                holder.view2.setBackgroundColor(ContextCompat.getColor(context, R.color.black_ninety));
                                holder.view3.setBackgroundColor(ContextCompat.getColor(context, R.color.memberShip_background));
                                holder.linear_click3.setBackgroundColor(ContextCompat.getColor(context, R.color.memberShip_background));
                                holder.Linear_share.setBackgroundColor(ContextCompat.getColor(context, R.color.memberShip_background));

                                holder.txt_name_outer.setText(freeBy.getInsideArray().get(2).getArrayInfo().get(0).getInfoTitle());
                                if (freeBy.getInsideArray().get(2).getArrayInfo().get(0).getInfoMessage().size()>1){

                                    String txt="";
                                    for(int t=0;t<freeBy.getInsideArray().get(2).getArrayInfo().get(0).getInfoMessage().size();t++){

                                        txt= txt+ "\u2022 " + freeBy.getInsideArray().get(2).getArrayInfo().get(0).getInfoMessage().get(t).toString() + "\n";

                                    }
                                    holder.txt_info_outer.setText(txt);

                                }else holder.txt_info_outer.setText(freeBy.getInsideArray().get(2).getArrayInfo().get(0).getInfoMessage().get(0));
//                                params.setMargins(1, 1, 1, 0);
//                                holder.linear_click.setLayoutParams(params);

                                /*if (freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getButtons().size()==1){
                                    Log.e("linear_click2", "1: "+ freeByList.get(pos).getInsideArray().get(1).getInsideTitle());
                                    holder.btnFirst.setVisibility(View.VISIBLE);
                                    holder.btnFirst.setText(freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getButtons().get(0).getButtonTitle());
                                    holder.btnFirst.setTag(freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getButtons().get(0).getButtonAction());
                                    holder.btnFirst.setOnClickListener(btnFirstClick);
                                }
                                if (freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getButtons().size()==2){
                                    Log.e("button", "2: "+ freeByList.get(pos).getInsideArray().get(1).getInsideTitle());
                                    holder.btnSecond.setVisibility(View.VISIBLE);
                                    holder.btnSecond.setText(freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getButtons().get(1).getButtonTitle());


//                notifyItemChanged(position);
                                }*/
                            }
                        });




                    }

                }

                updateView(pos);
            }
        });

        if(freeByList.get(position).isSelected()){
//            holder.img_animation.setImageResource(R.drawable.ic_arrow_down);
            isSelected(pos, holder);
        }else{

            holder.img_animation.setImageResource(R.drawable.ic_right);
            holder.inside_array.setVisibility(View.GONE);
//            freeByList.get(pos).setSelected(false);
        }

    }


    private void isSelected(final int pos, final ViewHolder holder){

        holder.inside_array.setVisibility(View.VISIBLE);
        freeByList.get(pos).setSelected(false);
        holder.img_animation.setImageResource(R.drawable.ic_arrow_down);
        if (freeByList.get(pos).getInsideArray().size()>0){





            holder.linear_click.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
            holder.Linear_share.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
            holder.view1.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));

            if (freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getButtons().size()==1){
                holder.btnFirst.setVisibility(View.VISIBLE);
                holder.btnFirst.setText(freeByList.get(pos).getInsideArray().get(1).getArrayInfo().get(0).getButtons().get(0).getButtonTitle());
                holder.btnFirst.setTag(freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getButtons().get(0).getButtonAction());
                holder.btnFirst.setOnClickListener(btnFirstClick);

//             bvcbvc                  notifyItemChanged(position);
            }
            if (freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getButtons().size()==2){
                Log.e("button", "2: "+ freeByList.get(pos).getInsideArray().get(0).getInsideTitle());
                holder.btnSecond.setVisibility(View.VISIBLE);
                holder.btnSecond.setText(freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getButtons().get(1).getButtonTitle());
//                                    notifyItemChanged(position);
                holder.btnSecond.setTag(freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getButtons().get(1).getButtonAction());
            }


        }

        if (freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().size()>1){

            String txt="";
            for(int t=0;t<freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().size();t++){

                txt= txt+ "\u2022 " + freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(t).toString() + "\n";

            }
            holder.txt_info_outer.setText(txt);

        }else holder.txt_info_outer.setText(freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(0));


    }

    private View.OnClickListener btnFirstClick=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String action=(String)v.getTag();

            if (action.equalsIgnoreCase("book")){
                Intent intent= new Intent(context, ParlorListActivity.class);
                intent.putExtra("isService",true);
                context.startActivity(intent);

            }else if (action.equalsIgnoreCase("refer")){
                Intent sendIntent= new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");

                //have to set title and content
                sendIntent.putExtra(Intent.EXTRA_TEXT ,"hfghdfgh");
//                sendIntent.putExtra(Intent.EXTRA_TEXT, "Share this link with your friends. Avail yourself a free haircut at any of the Be U salons by downloading Be U app.");
                context.startActivity(Intent.createChooser(sendIntent, "Share code:"));
            }
        }
    };



    private void updateView(int pos){


        for(int i=0;i<freeByList.size();i++){

            if(i!=pos){
                freeByList.get(i).setSelected(false);

            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return freeByList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_name, txt_info,txt_title,txt_info_outer,txt_name_outer,txt_title2,txt_title3,btnFirst,btnSecond;
        private ImageView img_, img_animation,img_title,img_outer,img_2,img_3;
        private RelativeLayout linear_title;
        LinearLayout inside_array,Linear_share,linear_click,linear_click2,linear_click3;
        private RecyclerView rec_services;
        private View view1,view2,view3;

        public ViewHolder(View itemView) {
            super(itemView);

            txt_name= (TextView)itemView.findViewById(R.id.txt_name);
            txt_info= (TextView)itemView.findViewById(R.id.txt_info);
            txt_title= (TextView)itemView.findViewById(R.id.txt_title);
            txt_info_outer= (TextView)itemView.findViewById(R.id.txt_info_outer);
            txt_name_outer= (TextView)itemView.findViewById(R.id.txt_name_outer);
            txt_title2= (TextView)itemView.findViewById(R.id.txt_title2);
            txt_title3= (TextView)itemView.findViewById(R.id.txt_title3);
            img_= (ImageView) itemView.findViewById(R.id.img_);
            img_title= (ImageView) itemView.findViewById(R.id.img_title);
            img_2= (ImageView) itemView.findViewById(R.id.img_2);
            img_3= (ImageView) itemView.findViewById(R.id.img_3);
            img_outer= (ImageView) itemView.findViewById(R.id.img_outer);
            img_animation= (ImageView)itemView.findViewById(R.id.img_animation);
            linear_title= (RelativeLayout) itemView.findViewById(R.id.linear_title);
            inside_array= (LinearLayout) itemView.findViewById(R.id.inside_array);
            Linear_share= (LinearLayout) itemView.findViewById(R.id.Linear_share);
            linear_click= (LinearLayout) itemView.findViewById(R.id.linear_click);
            linear_click2= (LinearLayout) itemView.findViewById(R.id.linear_click2);
            linear_click3= (LinearLayout) itemView.findViewById(R.id.linear_click3);
            view1=(View) itemView.findViewById(R.id.view1);
            view2=(View) itemView.findViewById(R.id.view2);
            view3=(View) itemView.findViewById(R.id.view3);
            btnFirst= (TextView) itemView.findViewById(R.id.btn_freeby_first);
            btnSecond= (TextView) itemView.findViewById(R.id.btn_freeby_second);
//            rec_services= (RecyclerView) itemView.findViewById(R.id.rec_services);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }
}
