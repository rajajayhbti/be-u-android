package com.beusalons.android.Model;

/**
 * Created by myMachine on 1/13/2017.
 */

public class CustomizeDealModel {

    public static final int HEADER_TYPE= 0;
    public static final int DEAL_TYPE= 1;

    private int mType;          //for viewholder

    private String dealName;
    private Integer serviceCode;            //fake code :P
    private String dealId;
    private String description;
    private Integer tax;

    private Integer menuPrice;
    private Integer dealPrice;

    private Integer actualServiceCode;  // this is the acutal service code
    private String serviceName;


    private int priceCode;        // this is the price id
    private String serviceId;       // this is the long service id string
    private String type;        // pass 'service' for service or the deal name in case of deals
    private int quantity;

    public int getmType() {
        return mType;
    }

    public void setmType(int mType) {
        this.mType = mType;
    }

    public Integer getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(Integer menuPrice) {
        this.menuPrice = menuPrice;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }


    public Integer getActualServiceCode() {
        return actualServiceCode;
    }

    public void setActualServiceCode(Integer actualServiceCode) {
        this.actualServiceCode = actualServiceCode;
    }

    public int getPriceCode() {
        return priceCode;
    }

    public void setPriceCode(int priceCode) {
        this.priceCode = priceCode;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDealName() {
        return dealName;
    }

    public void setDealName(String dealName) {
        this.dealName = dealName;
    }

    public Integer getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(Integer serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getTax() {
        return tax;
    }

    public void setTax(Integer tax) {
        this.tax = tax;
    }

    public Integer getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(Integer dealPrice) {
        this.dealPrice = dealPrice;
    }
}
