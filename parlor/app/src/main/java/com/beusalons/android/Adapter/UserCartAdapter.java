package com.beusalons.android.Adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.AddServiceUserCart.AddService_response;
import com.beusalons.android.Model.AddServiceUserCart.UserCart_post;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Task.UserCartTask;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by myMachine on 5/31/2017.
 */

public class UserCartAdapter extends RecyclerView.Adapter<UserCartAdapter.ViewHolder> {

    private Context context;
    private UserCart saved_cart;
    private List<UserServices> list;
    private Button btn_proceed;
    private boolean from_home;                  //if from home then if list size is 0, hide the proceed button
    private LinearLayout linear_no_item;

    public UserCartAdapter(Context context, UserCart saved_cart, List<UserServices> list,
                           Button btn_proceed, boolean from_home, LinearLayout linear_no_item) {

        this.context = context;
        this.saved_cart = saved_cart;
        this.list = list;
        this.btn_proceed = btn_proceed;
        this.from_home = from_home;
        this.linear_no_item = linear_no_item;
    }

    public void setList(List<UserServices> list) {

        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        //final UserServices service= list.get(position);

        holder.txt_save_per.setVisibility(View.GONE);

        if(list.get(position).getName()!=null)
            holder.txt_name.setText(fromHtml(list.get(position).getName()));
        holder.txt_quantity.setText("" + list.get(position).getQuantity());
        int total = (int) list.get(position).getPrice() * list.get(position).getQuantity();


        if (list.get(position).isRemainingService()){
            holder.linearLayout_price.setVisibility(View.GONE);
            holder.linear_layout_reamining.setVisibility(View.VISIBLE);
            holder.txt_quantity_remaining.setVisibility(View.VISIBLE);
            holder.txt_quantity_remaining.setText("Quantity Remaining: "+(list.get(position).getRemainingTotalQuantity()-list.get(position).getQuantity()));
        }else if (list.get(position).isMyMembershipFreeService()){
            holder.linearLayout_price.setVisibility(View.GONE);
            holder.linear_layout_reamining.setVisibility(View.VISIBLE);
            holder.txt_quantity_remaining.setVisibility(View.VISIBLE);
            holder.txt_quantity_remaining.setText("Quantity Remaining: "+(list.get(position).getRemainingTotalQuantity()-list.get(position).getQuantity()));
        }

        if (total==0 && total==0.0){
            holder.txt_total_price.setVisibility(View.GONE);
        }
        else{
            holder.txt_total_price.setVisibility(View.VISIBLE);
            holder.txt_total_price.setText(AppConstant.CURRENCY + total);

        }
//        if (list.get(position).getBrand_name() != null && list.get(position).getBrand_name().length() > 0 && list.get(position).getProduct_name() != null && list.get(position).getProduct_name().length() > 0)


//        else holder.txt_description.setText(list.get(position).getBrand_name());

        if (list.get(position).getPrice() == 0 || list.get(position).getPrice() == 0.0) {
            holder.txt_price.setVisibility(View.GONE);
            holder.txtViewCross.setVisibility(View.GONE);

        } else {
            holder.txtViewCross.setVisibility(View.VISIBLE);
            holder.txt_price.setVisibility(View.VISIBLE);
            holder.txt_price.setText(AppConstant.CURRENCY + (int) list.get(position).getPrice());
        }

        if (list.get(position).getMenu_price() == 0 || list.get(position).getMenu_price() == 0.0) {

            holder.txt_save_per.setVisibility(View.GONE);
            holder.txt_menu_price.setVisibility(View.GONE);
        } else {
            int totalMenuPrice = (int) list.get(position).getMenu_price() * list.get(position).getQuantity();
            holder.txtViewCross.setVisibility(View.VISIBLE);
            holder.txt_menu_price.setVisibility(View.VISIBLE);
            holder.txt_menu_price.setText(AppConstant.CURRENCY + totalMenuPrice);
            holder.txt_menu_price.setPaintFlags(holder.txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.txt_price.setVisibility(View.VISIBLE);

            //save per
            holder.txt_save_per.setVisibility(View.VISIBLE);
            holder.txt_save_per.setText(AppConstant.SAVE + " " +
                    (int) (100 - (((int) list.get(position).getPrice() * 100) / (int) list.get(position).getMenu_price())) + "%");

            holder.txt_save_per.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
            holder.txt_save_per.setBackgroundResource(R.drawable.discount_seletor);
        }

        if (list.get(position).getDescription() == null || list.get(position).getDescription().equalsIgnoreCase("")) {

            holder.txt_description.setVisibility(View.GONE);
        } else {

            if (list.get(position).getType().equals("newCombo") || list.get(position).getType().equals("combo")) {

                //   holder.linear_show_detail.setVisibility(View.VISIBLE);
//                holder.txt_description.setVisibility(View.GONE);
                /*holder.linear_show_detail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new ShowDetailsServiceDialog((Activity) context,list.get(position).getName(),list.get(position).getDescription());
                    }
                });*/


            } else {

                if (list.get(position).getDescription()!=null && list.get(position).getDescription().length()>2) {
                    holder.txt_description.setVisibility(View.VISIBLE);

//                holder.linear_show_detail.setVisibility(View.GONE);
                    holder.txt_description.setText(list.get(position).getDescription());
                }
            }
        }


        //other types ka case handle kiya hai, zaise show karna hai cart mai
        if (list.get(position).isMembership()) {
            holder.txt_price.setVisibility(View.GONE);
            holder.txtViewCross.setVisibility(View.GONE);
            holder.linear_add_remove.setVisibility(View.GONE);
            holder.linear_remove_.setVisibility(View.VISIBLE);
        } else if (list.get(position).isFree_service()) {
            holder.txt_price.setVisibility(View.GONE);
            holder.txtViewCross.setVisibility(View.GONE);

           /* holder.txt_price.setVisibility(View.VISIBLE);
            holder.txt_price.setText(AppConstant.CURRENCY + 0);*/

            holder.txt_menu_price.setVisibility(View.VISIBLE);
            holder.txt_menu_price.setText(AppConstant.CURRENCY + (int) list.get(position).getPrice());
            holder.txt_menu_price.setPaintFlags(holder.txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.linear_add_remove.setVisibility(View.GONE);
            holder.linear_remove_.setVisibility(View.VISIBLE);
            holder.txt_total_price.setText(AppConstant.CURRENCY + 0);
        } else {

            holder.linear_remove_.setVisibility(View.GONE);
            holder.linear_add_remove.setVisibility(View.VISIBLE);
        }


        holder.linear_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (list.get(position).isRemainingService()){
                    final int remainingQuantity=list.get(position).getRemainingTotalQuantity()-list.get(position).getQuantity();
                    if (remainingQuantity>0){
                        Log.i("stopstealing", "in the linear add button");
                        new Thread(new UserCartTask(context, saved_cart, list.get(position), false, false)).start();

                        updateQuantity(false, list, position, holder.txt_quantity, holder.txt_total_price);
                    }else{
                        Toast.makeText(context,"You have reached the maximum quantity.",Toast.LENGTH_SHORT).show();
                    }
                }
                else if (list.get(position).isMyMembershipFreeService()){
                    final int remainingQuantity=list.get(position).getRemainingTotalQuantity()-list.get(position).getQuantity();
                    if (remainingQuantity>0){
                        Log.i("stopstealing", "in the linear add button");
                        new Thread(new UserCartTask(context, saved_cart, list.get(position), false, false)).start();

                        updateQuantity(false, list, position, holder.txt_quantity, holder.txt_total_price);
                    }else{
                        Toast.makeText(context,"You have reached the maximum quantity.",Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    new Thread(new UserCartTask(context, saved_cart, list.get(position), false, false)).start();

                    updateQuantity(false, list, position, holder.txt_quantity, holder.txt_total_price);
                }


            }
        });

        holder.linear_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Log.i("stopstealing", "in the linear remove button");
                new Thread(new UserCartTask(context, saved_cart, list.get(position), true, false)).start();

                updateQuantity(true, list, position, holder.txt_quantity, holder.txt_total_price);
            }
        });

        holder.linear_remove_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i("stopstealing", "in the linear remove ...... button");
                new Thread(new UserCartTask(context, saved_cart, list.get(position), true, false)).start();

                updateQuantity(true, list, position, holder.txt_quantity, holder.txt_total_price);
            }
        });

        if (list.get(position).getDescription()!=null && list.get(position).getDescription().length()>2) {
            holder.txt_description.setVisibility(View.VISIBLE);
            holder.txt_description.setText(list.get(position).getDescription());
        }


    }

    private void updateQuantity(boolean add_rem, List<UserServices> list, int position, TextView txt_quantity, TextView txt_total_price) {



        int quantity = list.get(position).getQuantity();
        if (add_rem) {

            quantity -= 1;
            list.get(position).setQuantity(quantity);
            txt_quantity.setText("" + quantity);
            int total = (int) list.get(position).getPrice() * quantity;
            txt_total_price.setText(AppConstant.CURRENCY + total);

               addServiceToCart(saved_cart.getParlorId(),list.get(position).getService_code(),false);
        } else {
            quantity += 1;
            list.get(position).setQuantity(quantity);
            txt_quantity.setText("" + quantity);
            int total = (int) list.get(position).getPrice() * quantity;

            txt_total_price.setText(AppConstant.CURRENCY + total);

            addServiceToCart(saved_cart.getParlorId(),list.get(position).getService_code(),true);

        }



        if (quantity == 0 || quantity < 0)
            list.remove(position);

        //whether to show proceed button or not
        if (list.size() <= 0) {
            linear_no_item.setVisibility(View.VISIBLE);
            if (from_home)
                btn_proceed.setVisibility(View.GONE);
        } else
            linear_no_item.setVisibility(View.GONE);

        notifyDataSetChanged();
        EventBus.getDefault().post(list);
    }



    private void addServiceToCart(String parlorId,int serviceCode ,boolean add){
        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface= retrofit.create(ApiInterface.class);
        UserCart_post userCartPost=new UserCart_post();
        userCartPost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        userCartPost.setUserId(BeuSalonsSharedPrefrence.getUserId());
        userCartPost.setParlorId(parlorId);
        userCartPost.setServiceCode(serviceCode);
        if (add){
            userCartPost.setQuantity(1);

        }else{
            userCartPost.setQuantity(-1);
        }
        Call<AddService_response> call=apiInterface.addServicetoCart(userCartPost);
        call.enqueue(new Callback<AddService_response>() {
            @Override
            public void onResponse(Call<AddService_response> call, Response<AddService_response> response) {
                if (response.isSuccessful()){
                    if (response.body().isSuccess()){
                        Log.e("stuff add service cart", "i'm retrofit getStatus true :(");

                    }else{
                        Log.e("stuff add service cart", "i'm retrofit getStatus false:(");

                    }
                }else{
                    Log.e("stuff add service cart", "i'm retrofit failure :(");

                }
            }

            @Override
            public void onFailure(Call<AddService_response> call, Throwable t) {
                Log.e("stuff add service cart", "i'm in failure: "+ t.getMessage()+ "   "+
                        t.getStackTrace()+ t.getCause()+ " "+ t.getLocalizedMessage());
            }
        });

    }
    public class ViewHolder extends RecyclerView.ViewHolder {


        private TextView txt_name, txt_price, txt_save_per, txt_menu_price, txt_description, txt_quantity, txtViewCross,txtView_show_details, txt_total_price,txt_quantity_remaining;
        private LinearLayout linear_add, linear_remove, linear_add_remove, linear_remove_,
                linear_show_detail,linear_layout_reamining,linearLayout_price;
        private ImageView imgDes;


        public ViewHolder(View view) {
            super(view);

            txt_quantity = (TextView) view.findViewById(R.id.txt_quantity);
            txt_name = (TextView) view.findViewById(R.id.txt_name);
            txt_price = (TextView) view.findViewById(R.id.txt_price);
            txt_save_per = (TextView) view.findViewById(R.id.txt_save_per);
            txt_menu_price = (TextView) view.findViewById(R.id.txt_menu_price);
            txt_description = (TextView) view.findViewById(R.id.txt_description);
            txt_total_price = (TextView) view.findViewById(R.id.txt_total_price);

            linear_add = (LinearLayout) view.findViewById(R.id.linear_add);
            linear_remove = (LinearLayout) view.findViewById(R.id.linear_remove);

            linear_add_remove = (LinearLayout) view.findViewById(R.id.linear_add_remove);
            linear_remove_ = (LinearLayout) view.findViewById(R.id.linear_remove_);
//            imgDes=(ImageView)view.findViewById(R.id.imgDes);
//            linear_show_detail=(LinearLayout)view.findViewById(R.id.linear_show_detail);
            txtViewCross=(TextView)view.findViewById(R.id.txtViewCross);
            txtView_show_details = (TextView) view.findViewById(R.id.txtView_show_detail);
            linear_layout_reamining=(LinearLayout)itemView.findViewById(R.id.linear_layout_reamining);
            linearLayout_price=(LinearLayout)itemView.findViewById(R.id.linearLayout_price);
            txt_quantity_remaining=(TextView)itemView.findViewById(R.id.txt_quantity_remaining);

        }

        @Override
        public String toString() {
            return super.toString();
        }
    }


    @Override
    public int getItemCount() {
        if(list!=null &&
                list.size()>0)
            return list.size();
        return 0;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.fragment_user_cart_adapter, parent, false);
        return new ViewHolder(view);
    }
    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }
}
