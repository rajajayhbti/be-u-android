package com.beusalons.android.Model;

/**
 * Created by Ajay on 1/20/2017.
 */

public class DealsIdPost {

    private int dealId;
    private int serviceCode;
    private int quantity;

    public int getDealId() {
        return dealId;
    }

    public void setDealId(int dealId) {
        this.dealId = dealId;
    }

    public int getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(int serviceCode) {
        this.serviceCode = serviceCode;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
