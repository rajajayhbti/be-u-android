package com.beusalons.android.Model.ParlorDetail;

import com.beusalons.android.Model.ServiceModel;

import java.util.List;

/**
 * Created by myMachine on 4/5/2017.
 */

public class Categories {

    private String name;

    private List<ServiceModel> services;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ServiceModel> getServices() {
        return services;
    }

    public void setServices(List<ServiceModel> services) {
        this.services = services;
    }
}
