package com.beusalons.android.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.beusalons.android.Adapter.Artist.AdapterArtistCollection;
import com.beusalons.android.Model.Profile.CollectionWisePost;
import com.beusalons.android.Model.UserCollection.CollectionUser_response;
import com.beusalons.android.Model.UserProject.ProjectData_post;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.PaginationScrollListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Ajay on 2/21/2018.
 */

public class ArtistCollectionFragment extends Fragment {

    private RecyclerView recyclerViewCollection;
    private AdapterArtistCollection adapterNewCollection;
    private List<CollectionWisePost> collectionList;
    private int PAGE_SIZE = 0;
    private boolean isLoading= false;
    private boolean type=false;
    private String artistId;
    public static ArtistCollectionFragment newInstance(String artistId){

        ArtistCollectionFragment fragment= new ArtistCollectionFragment();
        fragment.artistId=artistId;
        Bundle bundle= new Bundle();
        fragment.setArguments(bundle);

        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.frag_new_collections, container, false);
        recyclerViewCollection=view.findViewById(R.id.recycler_collection);
        inItView();
        return view;
    }

    private void inItView(){
        LinearLayoutManager manager=new LinearLayoutManager(getActivity());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewCollection.setLayoutManager(manager);
        adapterNewCollection= new AdapterArtistCollection(getActivity(),collectionList);
        recyclerViewCollection.setAdapter(adapterNewCollection);
        recyclerViewCollection.addOnScrollListener(new PaginationScrollListener(manager) {
            @Override
            protected void loadMoreItems() {
                if (!isLoading){
                    isLoading= true;

                    adapterNewCollection.addProgress();
                    PAGE_SIZE++;
                    fetchData();

                }

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });

        fetchData();

        if (!type){
            recyclerViewCollection.setPadding(0,0,0,80);
        }
    }




    private  void fetchData(){
        Retrofit retrofit = ServiceGenerator.getPortfolioClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        ProjectData_post projectData_post=new ProjectData_post();


        if (artistId!=null)
            projectData_post.setArtistId(artistId);

        projectData_post.setPage(PAGE_SIZE);
        Call<CollectionUser_response> call= apiInterface.getCollection(projectData_post);

        call.enqueue(new Callback<CollectionUser_response>() {
            @Override
            public void onResponse(Call<CollectionUser_response> call, Response<CollectionUser_response> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){




                        Log.i("homestuff", " isSuccess");
                        if(response.body().getData()!=null &&
                                response.body().getData().size()>0){
                            Log.i("homestuff", " data hai isme");
                            //progress_bar.setVisibility(View.GONE);
                            if(PAGE_SIZE==0){
                                collectionList=new ArrayList<>();
                                collectionList.addAll(response.body().getData());
                                adapterNewCollection.setInitialData(collectionList);

                            }
                            else{
                                adapterNewCollection.removeProgress();
                                adapterNewCollection.addData(response.body().getData());

                                isLoading= false;
                            }


                            recyclerViewCollection.setVisibility(View.VISIBLE);
                            //   progress_bar.setVisibility(View.GONE);

                        }else{
                            Log.i("homestuff", "no data");



                            if (adapterNewCollection.getItemCount()>0){

                            }else {
                                recyclerViewCollection.setVisibility(View.GONE);
                            }
                            // progress_bar.setVisibility(View.GONE);

                            //Toast.makeText(coordinator_.getContext(), "No Posts To Show", Toast.LENGTH_SHORT).show();
                            //size 0 do something else
                            adapterNewCollection.removeProgress();

                        }




                    }else if (!response.body().getSuccess()){

                        if (adapterNewCollection.getItemCount()>0){
                            adapterNewCollection.removeProgress();
                            //       progress_bar.setVisibility(View.GONE);

                        }else{


                            recyclerViewCollection.setVisibility(View.GONE);
                            //   progress_bar.setVisibility(View.GONE);
//                            Toast.makeText(getActivity(), "Looks Like The Server Is Taking To Long To Respond\n" +
//                                    " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();

                        }


//                        Toast.makeText(getActivity(), "Looks Like The Server Is Taking To Long To Respond\n" +
//                                " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
                        Log.i("viewprofile", "not success");
                    }

                }else{
                    Toast.makeText(getActivity(), "Looks Like The Server Is Taking To Long To Respond\n" +
                            " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
                    Log.i("viewprofile", "response not successful");
                }
            }

            @Override
            public void onFailure(Call<CollectionUser_response> call, Throwable t) {
                if (getActivity()!=null)
                    Toast.makeText(getActivity(), "Looks Like The Server Is Taking To Long To Respond\n" +
                            " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
                Log.i("viewprofile", "failure: "+ t.getLocalizedMessage()+ "  "+t.getMessage()+"  "+ t.getCause());

            }
        });

    }


}
