package com.beusalons.android.Model.SalonReview;

import java.util.List;

/**
 * Created by myMachine on 9/4/2017.
 */

public class Data {

    private int total;
    List<Integer> counts= null;
    private String avgRating;
    private Percentage percentage;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Integer> getCounts() {
        return counts;
    }

    public void setCounts(List<Integer> counts) {
        this.counts = counts;
    }

    public String getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(String avgRating) {
        this.avgRating = avgRating;
    }

    public Percentage getPercentage() {
        return percentage;
    }

    public void setPercentage(Percentage percentage) {
        this.percentage = percentage;
    }
}
