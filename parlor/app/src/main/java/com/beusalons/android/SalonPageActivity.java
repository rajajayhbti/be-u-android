package com.beusalons.android;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Dialog.CorporateSuccess;
import com.beusalons.android.Fragment.SalonImagesFragment;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.AddServiceUserCart.AddService_response;
import com.beusalons.android.Model.AddServiceUserCart.UserCart_post;
import com.beusalons.android.Model.DealsSalonList.DealSalonListResponse;
import com.beusalons.android.Model.DealsServices.DealDetail.Deal_Detail_Post;
import com.beusalons.android.Model.DealsServices.DealDetail.PostDealDetail;
import com.beusalons.android.Model.DealsServices.DealDetail.ServicesList;
import com.beusalons.android.Model.Favourites.SalonFavResponse;
import com.beusalons.android.Model.Favourites.SalonFavpost;
import com.beusalons.android.Model.ParlorDetail.FacebookCheckinPost;
import com.beusalons.android.Model.ParlorDetail.FacebookCheckinResponse;
import com.beusalons.android.Model.SalonHome.HomeResponse;
import com.beusalons.android.Model.SalonHome.salonDepartments.SalonDeparttmentsData;
import com.beusalons.android.Model.SalonHome.salonDepartments.SalonsDepartmentsResponse;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Service.FetchDepartmentsService;
import com.beusalons.android.SongsActivity.SalonSongsActivity;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.beusalons.android.Views_Custom.CircularTextView;
import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by myMachine on 8/9/2017.
 */
public class SalonPageActivity extends AppCompatActivity {

    public static final String HAS_DEAL_DATA= "has_deal_data";
    private boolean has_deal_data= false;

    private ImageView img_salon, img_favourite, img_type, img_wifi,img_share, img_fb;
    private TextView txt_rating, txt_status, txt_name, txt_appointments, txt_reviews, txt_address, txt_landmark,
            txt_address_, txt_rating_, txt_book,txt_fb_friends, txt_fb_txt, txt_fb_detail;
    private LinearLayout linear_back, linear_favourite, linear_wifi, linear_call, linear_photos,
            linear_reviews, linear_direction, linear_ratings_, linear_details, linear_ratings,ll_fb_friends,linear_chekin;

    private GoogleApiClient mGoogleApiClient= null;
    protected static final int REQUEST_CHECK_SETTINGS = 100;
    private static final int GPS_PERMISSION = 60;

    private AppBarLayout app_bar;
    private CollapsingToolbarLayout collapsing_;

    private HomeResponse response_;
    SalonDeparttmentsData salonDeparttmentsData=null;
    private boolean salon_favourite= false;
    private RelativeLayout relative_;
    private ProgressBar progess_;

    private AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;

    private boolean isWifiConnected= false;
    private Runnable runnable;
    private Handler handler;
    private CallbackManager callbackManager;
    String parlor_id="";
    String departmentId,departmentNeme,gender;
    int index;

    private TextView txt_photo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon_page);

        final Bundle bundle= getIntent().getExtras();


        if(bundle!=null){
            parlor_id = bundle.getString("parlorId");
            departmentId=bundle.getString("departmentId",null);
            departmentNeme=bundle.getString("departmentName",null);
            gender=bundle.getString("gender",null);
            index=bundle.getInt("index",-1);

            has_deal_data= bundle.getBoolean(HAS_DEAL_DATA);
        }


        callbackManager = CallbackManager.Factory.create();
        app_bar = (AppBarLayout) findViewById(R.id.app_bar);
        collapsing_= (CollapsingToolbarLayout)findViewById(R.id.collapsing_);
        setToolBar();

        relative_= (RelativeLayout)findViewById(R.id.relative_);
        progess_= (ProgressBar)findViewById(R.id.progress_);
        relative_.setAlpha(.2f);
        progess_.setVisibility(View.VISIBLE);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        logger = AppEventsLogger.newLogger(this);

        fetchData(parlor_id);

        img_salon= (ImageView)findViewById(R.id.img_salon);
        img_share= (ImageView)findViewById(R.id.img_share);
        img_favourite= (ImageView)findViewById(R.id.img_favourite);
        img_type= (ImageView)findViewById(R.id.img_type);
        img_wifi= (ImageView)findViewById(R.id.img_wifi);

        txt_rating= (TextView)findViewById(R.id.txt_rating);
        txt_status= (TextView)findViewById(R.id.txt_status);
        txt_name= (TextView)findViewById(R.id.txt_name);
        txt_appointments= (TextView)findViewById(R.id.txt_appointments);
        txt_reviews= (TextView)findViewById(R.id.txt_reviews);
        txt_address= (TextView)findViewById(R.id.txt_address);
        txt_landmark= (TextView)findViewById(R.id.txt_landmark);
        txt_address_= (TextView)findViewById(R.id.txt_address_);
        txt_rating_= (TextView)findViewById(R.id.txt_rating_);
        txt_book= (TextView)findViewById(R.id.txt_book);
        txt_fb_friends= (TextView)findViewById(R.id.txt_fb_friends);
        txt_fb_txt= findViewById(R.id.txt_fb_txt);
        txt_fb_detail= findViewById(R.id.txt_fb_detail);
        img_fb= findViewById(R.id.img_fb);
        txt_photo= findViewById(R.id.txt_photo);

//        linear_back= (LinearLayout)findViewById(R.id.linear_back);
        linear_favourite= (LinearLayout)findViewById(R.id.linear_favourite);
        linear_wifi= (LinearLayout)findViewById(R.id.linear_wifi);
        linear_call= (LinearLayout)findViewById(R.id.linear_call);
        linear_chekin= (LinearLayout)findViewById(R.id.linear_chekin);
        linear_photos= (LinearLayout)findViewById(R.id.linear_photos);
        linear_reviews= (LinearLayout)findViewById(R.id.linear_reviews);
        linear_direction= (LinearLayout)findViewById(R.id.linear_direction);
        linear_ratings_= (LinearLayout)findViewById(R.id.linear_ratings_);
        linear_details= (LinearLayout)findViewById(R.id.linear_details);
        linear_ratings= (LinearLayout)findViewById(R.id.linear_ratings);
        ll_fb_friends= (LinearLayout)findViewById(R.id.ll_fb_friends);

//        linear_back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                finish();
//            }
//        });


        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logShareSalonEvent(txt_name.getText().toString());
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "https://beusalons.app.link/a/key_live_lftOr7qepHyQDI7uVSmpCkndstebwh0V?page=SalonDetail&parlorId="+parlor_id);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);

                String longUrl="Hey check this out! Details of salon on Be U Salons https://beusalons.app.link/a/key_live_lftOr7qepHyQDI7uVSmpCkndstebwh0V";
      /*  URLShortener.shortUrl(longUrl, new URLShortener.LoadingCallback() {
            @Override
            public void startedLoading() {
            }

            @Override
            public void finishedLoading(@Nullable String shortUrl) {
                //make sure the string is not null
                if(shortUrl != null){
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, shortUrl);
                    sendIntent.setType("text/plain");
                    startActivity(Intent.createChooser(sendIntent, "Share Deals"));
                }

                else {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "Hey check this out! Details of salon on Be U Salons https://beusalons.app.link/a/key_live_lftOr7qepHyQDI7uVSmpCkndstebwh0V?page=SalonDetail&parlorId="+parlor_id);
                    sendIntent.setType("text/plain");
                    startActivity(Intent.createChooser(sendIntent, "Share Deals"));
                }
            }
        });*/
               /* LinkProperties lp = new LinkProperties()
                        .setChannel("facebook")
                        .setFeature("sharing")
                        .addControlParameter("$desktop_url", longUrl)
                        .addControlParameter("page", "SalonDetail")
                        .addControlParameter("parlorId", parlor_id);
                BranchUniversalObject buo = new BranchUniversalObject()
                        .setCanonicalIdentifier("content/12345")
                        .setTitle("Share Deal")
                        .setContentDescription("My Content Description")

                        .setContentImageUrl("https://lorempixel.com/400/400")
                        .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                        .setContentMetadata(new ContentMetadata().addCustomMetadata("key1", "value1"));
                buo.generateShortUrl(SalonPageActivity.this, lp, new Branch.BranchLinkCreateListener() {
                    @Override
                    public void onLinkCreate(String url, BranchError error) {
                        if (error == null) {
                            Intent sendIntent = new Intent();
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.putExtra(Intent.EXTRA_TEXT, url);
                            sendIntent.setType("text/plain");
                            startActivity(Intent.createChooser(sendIntent, "Share Deals"));
                        }
                    }
                });*/

            }
        });

        txt_rating.setVisibility(View.GONE);

        fbChekin();
    }

    private void fbChekin(){


        linear_chekin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                LoginManager.getInstance().logOut();

//                new GraphRequest(
//                        AccessToken.getCurrentAccessToken(),
//                        "/"+AccessToken.getCurrentAccessToken().getUserId(),
//                        null,
//                        HttpMethod.GET,
//                        new GraphRequest.Callback() {
//                            public void onCompleted(GraphResponse response) {
//            /* handle the result */
//                            }
//                        }
//                ).executeAsync();


                LoginManager.getInstance().logInWithReadPermissions(SalonPageActivity.this, Arrays.asList("user_tagged_places"));
                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(LoginResult loginResult) {
                                Log.e("investiga", "I'm in onSuccess");
                                final String str= loginResult.getAccessToken().getToken();
                                Log.e("fb ckekin", "I'm in onSuccess"+str);

                                ShareDialog shareDialog = new ShareDialog(SalonPageActivity.this);
//
                                shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                                    @Override
                                    public void onSuccess(Sharer.Result result) {
//                                        Log.e("fb",result.getPostId());

                                        Toast.makeText(SalonPageActivity.this,"Posted Successfully",Toast.LENGTH_SHORT).show();

                                        updateServerForChekin(str);

//                                        btn_fb_checkin.setText("You have checked in at this salon");
                                    }

                                    @Override
                                    public void onCancel() {
                                        Log.e("fb","xcancel");
                                    }

                                    @Override
                                    public void onError(FacebookException error) {
                                        Log.e("fb",error.toString());
                                    }
                                });

                                if (ShareDialog.canShow(ShareLinkContent.class)) {
                                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                            .setContentUrl(Uri.parse("http://www.beusalons.com/"))
                                            .build();

                                    shareDialog.show(linkContent, ShareDialog.Mode.NATIVE);
                                }

                            }

                            @Override
                            public void onCancel() {
                                Log.e("investiga", "I'm in onCancel");
                            }

                            @Override
                            public void onError(FacebookException exception) {
                                Log.e("investiga", "I'm in onError: "+ exception.getMessage()+ " "+exception.getCause());
                            }
                        });
            }
        });
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void ongetDepartments(SalonDeparttmentsData data){
        this.salonDeparttmentsData=data;
        response_.getData().setDepartments(data.getDepartments());
        response_.getData().setSubscriptions(data.getSubscriptions());
        response_.getData().setWelcomeOffer(data.getWelcomeOffer());
        response_.getData().setTax(data.getTax());
        Log.e("data came ",data.getParlorId());


    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void fetchData(final String parlor_id){

        Retrofit retrofit = ServiceGenerator.getClient();

        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<HomeResponse> call = service.getSalonStuff(parlor_id, BeuSalonsSharedPrefrence.getUserId());
        call.enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().getSuccess()){
                        Log.i("salonpagelog", "in the success");

                        Intent i = new Intent(SalonPageActivity.this, FetchDepartmentsService.class);
                        i.putExtra("parlorId", parlor_id);
                        startService(i);
                        response_= response.body();

                        linear_details.removeAllViews();

                        if(response_.getData().isFreeWifi()){

                            linear_wifi.setVisibility(View.VISIBLE);

                            try {

                                enableWifi();
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                        }else{
                            linear_wifi.setVisibility(View.GONE);
                        }
                        checkWifi();

                        int photo_size= response_.getData().getImages().size();
                        if(photo_size==1){
                            txt_photo.setText(photo_size+ " Photo");
                        }else{
                            txt_photo.setText(photo_size+ " Photos");
                        }

                        app_bar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                            boolean isShow = false;
                            int scrollRange = -1;

                            @Override
                            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                                if (scrollRange == -1) {
                                    scrollRange = appBarLayout.getTotalScrollRange();
                                }
                                if (scrollRange + verticalOffset == 0) {
                                    collapsing_.setTitle(response_.getData().getName());
                                    collapsing_.setCollapsedTitleTextAppearance(R.style.personal_collapsed_title);
                                    isShow = true;
                                } else if(isShow) {
                                    collapsing_.setTitle("");//carefull there should a space between double quote otherwise it wont work
                                    collapsing_.setClickable(false);
                                    isShow = false;
                                }
                            }
                        });

                        //glide crashing- telling load in destroy ..
                        try{

                            Glide.with(SalonPageActivity.this).load(
                                    response.body().getData().getImages().get(0).getAppImageUrl())
                                    .into(img_salon);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        if(response.body().getData().getParlorType()==0)
                            img_type.setImageResource(R.drawable.ic_premium_badge);
                        else if(response.body().getData().getParlorType()==1)
                            img_type.setImageResource(R.drawable.ic_standard_badge);
                        else
                            img_type.setImageResource(R.drawable.ic_budget_badge);

                        salon_favourite= response.body().getData().getFavourite();
                        if(salon_favourite){
                            img_favourite.setImageResource(R.drawable.ic_favourite_red);
                        }else{
                            img_favourite.setImageResource(R.drawable.ic_favourite_white);
                        }
                        favSalon(parlor_id);

                        txt_rating.setText(""+response.body().getData().getRating());
                        setRecentRatings();

                        txt_appointments.setText(response.body().getData().getNoOfAppointments()+" Appointments");
                        txt_reviews.setText(response.body().getData().getNoOfReviews()+ " Reviews");

                        txt_name.setText(response.body().getData().getName());
                        if (response.body().getData().getUserFbFriendString()!=null && response.body().getData().getUserFbFriendString().length()>0){
                            txt_fb_friends.setText(response.body().getData().getUserFbFriendString());
                        }else ll_fb_friends.setVisibility(View.GONE);

                        txt_address.setText(response.body().getData().getAddress1());
                        txt_address_.setText(response.body().getData().getAddress2());
                        if(response.body().getData().getLandmark()==null ||
                                response.body().getData().getLandmark().equalsIgnoreCase(""))
                            txt_landmark.setVisibility(View.GONE);
                        else
                            txt_landmark.setText(response.body().getData().getLandmark());

                        int price= response.body().getData().getPrice();
                        String cost="";
                        if(price==1)
                            cost= "<font color='#3e780a'>₹</font>"+" "+"₹"+" "+"₹"+" "+"₹"+" "+"₹";
                        else if(price==2)
                            cost= "<font color='#3e780a'>₹ ₹</font>"+" "+"₹"+" "+"₹"+" "+"₹";
                        else if(price==3)
                            cost= "<font color='#3e780a'>₹ ₹ ₹</font>"+" "+"₹"+" "+"₹";
                        else if(price==4)
                            cost= "<font color='#3e780a'>₹ ₹ ₹ ₹</font>"+" "+"₹";
                        else
                            cost= "<font color='#3e780a'>₹ ₹ ₹ ₹ ₹</font>";
                        txt_rating_.setText(fromHtml(cost));

                        setClicks();

                        int start_hr= Integer.parseInt(response.body().getData().getOpeningTime().substring(0, 2));
                        int start_min= Integer.parseInt(response.body().getData().getOpeningTime().substring(3,5));
                        int closing_hr= Integer.parseInt(response.body().getData().getClosingTime().substring(0, 2));
                        int closing_min= Integer.parseInt(response.body().getData().getClosingTime().substring(3, 5));
                        Calendar cal= Calendar.getInstance();
                        if(cal.get(Calendar.DAY_OF_WEEK)== response.body().getData().getDayClosed())
                            txt_status.setText("Closed Today");
                        else if(cal.get(Calendar.HOUR_OF_DAY)>= start_hr &&
                                cal.get(Calendar.HOUR_OF_DAY)<= closing_hr)
                            txt_status.setText("Open Now");
                        else
                            txt_status.setText("Closed Now");
                        txt_status.setTypeface(null, Typeface.ITALIC);

                        //free wifi
                        View wifi_= LayoutInflater.from(SalonPageActivity.this)
                                .inflate(R.layout.inflate_salon_detail, null);

                        TextView txt_name= (TextView)wifi_.findViewById(R.id.txt_name);
                        TextView txt_detail= (TextView)wifi_.findViewById(R.id.txt_detail);
                        txt_name.setText("FREE WIFI");
                        txt_detail.setText(response_.getData().isFreeWifi()?
                                "Available":"Unavailable");
                        linear_details.addView(wifi_);

                        //departments
                        View departments_= LayoutInflater.from(SalonPageActivity.this)
                                .inflate(R.layout.inflate_salon_detail_, null);
                        TextView txt_name_d= (TextView)departments_.findViewById(R.id.txt_name);
                        TextView txt_detail_d= (TextView)departments_.findViewById(R.id.txt_detail);
                        TextView txt_name_d_= (TextView)departments_.findViewById(R.id.txt_name_);
                        TextView txt_detail_d_= (TextView)departments_.findViewById(R.id.txt_detail_);

                        txt_name_d.setText("DEPARTMENTS");
                        txt_detail_d.setText(response_.getData().getDepartmentsString());
                        txt_name_d_.setText("SERVICES");
                        txt_detail_d_.setText(response_.getData().getGender());
                        linear_details.addView(departments_);

                        //brands
                        View brands_= LayoutInflater.from(SalonPageActivity.this)
                                .inflate(R.layout.inflate_salon_detail, null);
                        TextView txt_name_b= (TextView)brands_.findViewById(R.id.txt_name);
                        TextView txt_detail_b= (TextView)brands_.findViewById(R.id.txt_detail);
                        txt_name_b.setText("BRANDS");
                        txt_detail_b.setText(response_.getData().getBrandsString());
                        linear_details.addView(brands_);

                        View working_days_= LayoutInflater.from(SalonPageActivity.this)
                                .inflate(R.layout.inflate_salon_detail_, null);
                        TextView txt_name_w= (TextView)working_days_.findViewById(R.id.txt_name);
                        TextView txt_detail_w= (TextView)working_days_.findViewById(R.id.txt_detail);
                        TextView txt_name_w_= (TextView)working_days_.findViewById(R.id.txt_name_);
                        TextView txt_detail_w_= (TextView)working_days_.findViewById(R.id.txt_detail_);
                        TextView txt_details_w_= (TextView)working_days_.findViewById(R.id.txt_details);

                        txt_name_w.setText("WORKING DAYS");
                        txt_detail_w_.setText(response_.getData().getOpeningTime()+"AM - "+
                                response_.getData().getClosingTime()+"PM");

                        if(response_.getData().getDayClosed()==0)
                            txt_detail_w.setText("7 Days A Week");
                        else{
                            txt_detail_w.setText("6 Days A Week");

                            txt_details_w_.setVisibility(View.VISIBLE);
                            int day_closed= response_.getData().getDayClosed();
                            if(day_closed==1)
                                txt_details_w_.setText("Sunday Closed");
                            else if(day_closed==2)
                                txt_details_w_.setText("Monday Closed");
                            else if(day_closed==3)
                                txt_details_w_.setText("Tuesday Closed");
                            else if(day_closed==4)
                                txt_details_w_.setText("Wednesday Closed");
                            else if(day_closed==5)
                                txt_details_w_.setText("Thursday Closed");
                            else if(day_closed==6)
                                txt_details_w_.setText("Friday Closed");
                            else if(day_closed==7)
                                txt_details_w_.setText("Saturday Closed");
                        }
                        linear_details.addView(working_days_);
                        linear_details.setPadding(0, 0, 0, 16);

                        txt_rating.setVisibility(View.VISIBLE);




                        if(response_.getData().isMusic()){
                            txt_fb_detail.setVisibility(View.GONE);
                            txt_fb_txt.setText("Music");

                            img_fb.setImageResource(R.drawable.ic_song);

                            linear_chekin.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    Intent intent= new Intent(view.getContext(), SalonSongsActivity.class);
                                    intent.putExtra(SalonSongsActivity.SALON_NAME, response_.getData().getName());
                                    intent.putExtra(SalonSongsActivity.SALON_ID, parlor_id);
                                    startActivity(intent);
                                }
                            });
                        }
                        txt_book.setEnabled(true);
                        txt_book.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                logBookAppointmentClickedEvent();
                                logBookAppointmentClickedFireBaseEvent();
                                Intent intent = new Intent(getApplicationContext(), DateTimeActivity.class);
                                Bundle bundle= new Bundle();
                                //class ka naam service hai par yeh departments ha, json mai chodu naam rakha hai mc
                                bundle.putString(DateTimeActivity.CURRENT_TIME, response_.getData().getCurrentTime());
                                bundle.putString(DateTimeActivity.DATA_HOME_RESPONSE, new Gson().toJson(response_, HomeResponse.class));
                                bundle.putString(DateTimeActivity.OPENING_TIME,response_.getData().getOpeningTime());
                                bundle.putString(DateTimeActivity.CLOSING_TIME,response_.getData().getClosingTime());
                                bundle.putInt(DateTimeActivity.DAY_CLOSED, response_.getData().getDayClosed());
                                if (salonDeparttmentsData!=null){
                                    bundle.putString(DateTimeActivity.DATA_DEPARTMENT_RESPONSE, new Gson().toJson(salonDeparttmentsData, SalonDeparttmentsData.class));
                                }
                                if (departmentId!=null){
                                    bundle.putString("departmentName",departmentNeme);
                                    bundle.putString("departmentId",departmentId);
                                    bundle.putString("gender",gender);
                                    bundle.putInt("index",index);
                                }
                                intent.putExtras(bundle);
                                startActivity(intent);

                            }
                        });
                        //save tax
                        if(response_.getData().getTax()==0.0){
                            //@Todo: handle karo yeh
                            BeuSalonsSharedPrefrence.setServiceTax(1.0f);
                        }else{
                            BeuSalonsSharedPrefrence.setServiceTax(response_.getData().getTax());
                        }

                        //deal wali services ke liye
                        if(has_deal_data)
                            openDB(parlor_id);
                        else{

                            relative_.setAlpha(1f);
                            progess_.setVisibility(View.GONE);
                        }

                    }else{
                        Log.i("salonpagelog", "in the body else" );

                        finish();
                    }

                }else{
                    Log.i("salonpagelog", "response not success");
                    finish();
                }
            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {
                Log.i("salonpagelog", "failure: "+ t.getMessage()+ " "+ t.getStackTrace()+ " "+t.getCause());
                finish();
            }
        });
    }


    private void openDB(String parlor_id){

        try{
            UserCart savedCart= new UserCart();
            DB snappyDB = DBFactory.open(this);

            //db mai jo saved cart hai
            if (snappyDB.exists(AppConstant.USER_CART_DB))
                savedCart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
            snappyDB.close();

            int size= savedCart==null?0:savedCart.getServicesList().size();
            if(savedCart!=null &&
                    savedCart.getCartType().equalsIgnoreCase(AppConstant.DEAL_TYPE) &&
                    size>0) {

                Deal_Detail_Post deal_post= new Deal_Detail_Post();
                List<PostDealDetail> list= new ArrayList<>();

                for(int i=0;i<size;i++){

                    PostDealDetail detail= new PostDealDetail();
                    detail.setDealId(savedCart.getServicesList().get(i).getDealId());
                    detail.setQuantity(savedCart.getServicesList().get(i).getQuantity());

                    if(savedCart.getServicesList().get(i).getPackageServices()!=null &&
                            savedCart.getServicesList().get(i).getPackageServices().size()>0){

                        List<ServicesList> services= new ArrayList<>();
                        for(int j=0; j<savedCart.getServicesList().get(i).getPackageServices().size(); j++){

                            ServicesList service= new ServicesList();
                            service.setServiceCode(savedCart.getServicesList().get(i)
                                    .getPackageServices().get(j).getService_code());
                            service.setBrandId(savedCart.getServicesList().get(i)
                                    .getPackageServices().get(j).getBrand_id());

                            service.setProductId(savedCart.getServicesList().get(i)
                                    .getPackageServices().get(j).getProduct_id());

                            services.add(service);
                        }
                        detail.setServices(services);

                    }else if (savedCart.getServicesList().get(i).isMyMembershipFreeService()){

                        //don't do anything
//                        serviceCodes.add(Integer.parseInt(String.valueOf(savedCart.getServicesList().get(i).getService_code()).toString().trim()));
//                        isMemberShip=true;
                    }else{

                        List<ServicesList> services= new ArrayList<>();
                        ServicesList service= new ServicesList();
                        service.setServiceCode(savedCart.getServicesList().get(i).getService_code());
                        service.setBrandId(savedCart.getServicesList().get(i).getBrand_id());
                        service.setProductId(savedCart.getServicesList().get(i).getProduct_id());
                        services.add(service);

                        detail.setServices(services);
                    }

                    list.add(detail);
                }
                deal_post.setSelectedDeals(list);

                fetchDealServices(deal_post, parlor_id, savedCart);



                //addd all servies to cart api

                pushServiceToCart(savedCart.getServicesList(),parlor_id);

            }else{

                relative_.setAlpha(1f);
                progess_.setVisibility(View.GONE);
            }
        }catch (SnappydbException e){
            e.printStackTrace();
        }
    }

    private void fetchDealServices(Deal_Detail_Post deal_post, String parlor_id, final UserCart savedCart){

        deal_post.setLatitude(Double.parseDouble(BeuSalonsSharedPrefrence.getLatitude()));
        deal_post.setLongitude(Double.parseDouble(BeuSalonsSharedPrefrence.getLongitude()));
        deal_post.setParlorId(parlor_id);               //setting parlor id, kyu I need the salon deal services

        //@Todo:api change karo
        Retrofit retrofit3 = ServiceGenerator.getClient();
        final ApiInterface service = retrofit3.create(ApiInterface.class);
        Call<DealSalonListResponse> call = service.getNewDealData(deal_post);
        call.enqueue(new Callback<DealSalonListResponse>() {
            @Override
            public void onResponse(Call<DealSalonListResponse> call, final Response<DealSalonListResponse> response) {

                if(response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        Log.i("salonpagedeal", "on success");


                        relative_.setAlpha(1f);
                        progess_.setVisibility(View.GONE);

                        if(response.body().getData()!=null &&
                                response.body().getData().getParlors()!=null &&
                                response.body().getData().getParlors().size()>0 &&
                                response.body().getData().getParlors().get(0).getDeals()!=null &&
                                response.body().getData().getParlors().get(0).getDeals().size()>0){

                            new Thread(new Runnable() {
                                @Override
                                public void run() {

                                    UserCart newCart= new UserCart();
                                    newCart.setCartType(AppConstant.SERVICE_TYPE);
                                    newCart.setParlorId(response_.getData().getParlorId());
                                    newCart.setParlorName(response_.getData().getName());
                                    newCart.setParlorType(response_.getData().getParlorType());
                                    newCart.setGender(response_.getData().getGender());
                                    newCart.setRating(response_.getData().getRating());
                                    newCart.setOpeningTime(response_.getData().getOpeningTime());
                                    newCart.setClosingTime(response_.getData().getClosingTime());
                                    newCart.setAddress1(response_.getData().getAddress1());
                                    newCart.setAddress2(response_.getData().getAddress2());


                                    for(int i=0; i<savedCart.getServicesList().size(); i++){

                                        for(int j=0; j<response.body().getData().getParlors().get(0).getDeals().size();j++){

                                            if(savedCart.getServicesList().get(i).getDealId() ==
                                                    response.body().getData().getParlors().get(0).getDeals().get(j).getDealId()){

                                                if(response.body().getData().getParlors().get(0).getDeals().get(j).getServicePrices()!=null &&
                                                        response.body().getData().getParlors().get(0).getDeals().get(j).getServicePrices().size()>0){

                                                    for(int k=0; k<response.body().getData().getParlors().get(0).getDeals().get(j).getServicePrices().size();k++){
                                                        for(int l=0; l<savedCart.getServicesList().get(i).getPackageServices().size(); l++){

                                                            if(response.body().getData().getParlors().get(0).getDeals().
                                                                    get(j).getServicePrices().get(k).getServiceCode() ==
                                                                    savedCart.getServicesList().get(i).getPackageServices().get(l).getService_code()){

                                                                UserServices services= new UserServices();
                                                                services.setName(savedCart.getServicesList().get(i).getPackageServices().get(l).getService_name());

                                                                services.setService_deal_id(response.body().getData().
                                                                        getParlors().get(0).getDeals().get(j).getParlorDealId());               //service ya deal id hogi isme
                                                                services.setDealId(response.body().getData().
                                                                        getParlors().get(0).getDeals().get(j).getDealId());
                                                                services.setService_id(response.body().getData().
                                                                        getParlors().get(0).getDeals().get(j).getParlorDealId());

                                                                services.setService_code(savedCart.getServicesList().get(i).getPackageServices().get(l).getService_code());
                                                                services.setPrice_id(savedCart.getServicesList().get(i).getPackageServices().get(l).getService_code());
                                                                services.setType(savedCart.getServicesList().get(i).getType());

                                                                services.setPrice(response.body().getData().getParlors().get(0).getDeals().
                                                                        get(j).getServicePrices().get(k).getPrice());
                                                                services.setMenu_price(response.body().getData().getParlors().get(0).getDeals().
                                                                        get(j).getServicePrices().get(k).getMenuPrice());
                                                                services.setQuantity(response.body().getData().getParlors().get(0).getDeals().get(j).getQuantity());
                                                                services.setParlorTypes(null);

                                                                String brand_name= "", brand_id= "", product_name= "", product_id="";
                                                                brand_name= savedCart.getServicesList().get(i).getPackageServices().get(l).getBrand_name()==null ||
                                                                        savedCart.getServicesList().get(i).getPackageServices().get(l).getBrand_name().equalsIgnoreCase("")?
                                                                        "":savedCart.getServicesList().get(i).getPackageServices().get(l).getBrand_name();
                                                                brand_id= savedCart.getServicesList().get(i).getPackageServices().get(l).getBrand_id()==null ||
                                                                        savedCart.getServicesList().get(i).getPackageServices().get(l).getBrand_id().equalsIgnoreCase("")?
                                                                        "":savedCart.getServicesList().get(i).getPackageServices().get(l).getBrand_id();
                                                                product_name= savedCart.getServicesList().get(i).getPackageServices().get(l).getProduct_name()==null ||
                                                                        savedCart.getServicesList().get(i).getPackageServices().get(l).getBrand_name().equalsIgnoreCase("")?
                                                                        "":savedCart.getServicesList().get(i).getPackageServices().get(l).getProduct_name();
                                                                product_id= savedCart.getServicesList().get(i).getPackageServices().get(l).getProduct_id()==null ||
                                                                        savedCart.getServicesList().get(i).getPackageServices().get(l).getProduct_id().equalsIgnoreCase("")?
                                                                        "":savedCart.getServicesList().get(i).getPackageServices().get(l).getProduct_id();

                                                                services.setBrand_name(brand_name);
                                                                services.setBrand_id(brand_id);
                                                                services.setProduct_name(product_name);
                                                                services.setProduct_id(product_id);

                                                                services.setDescription(brand_name+ " "+product_name);
                                                                services.setPrimary_key(""+savedCart.getServicesList().get(i).getPackageServices().get(l).getService_code()
                                                                        +response.body().getData().
                                                                        getParlors().get(0).getDeals().get(j).getParlorDealId()+brand_id+product_id);

                                                                newCart.getServicesList().add(services);

                                                            }

                                                        }

                                                    }

                                                }else{


                                                    UserServices services= new UserServices();
                                                    services.setName(savedCart.getServicesList().get(i).getName());

                                                    services.setService_deal_id(response.body().getData().
                                                            getParlors().get(0).getDeals().get(j).getParlorDealId());               //service ya deal id hogi isme
                                                    services.setDealId(response.body().getData().
                                                            getParlors().get(0).getDeals().get(j).getDealId());
                                                    services.setService_id(response.body().getData().
                                                            getParlors().get(0).getDeals().get(j).getParlorDealId());

                                                    services.setService_code(savedCart.getServicesList().get(i).getService_code());
                                                    services.setPrice_id(savedCart.getServicesList().get(i).getService_code());

                                                    services.setType(savedCart.getServicesList().get(i).getType());

                                                    services.setPrice(response.body().getData().getParlors().get(0).getDeals().get(j).getDealPrice());
                                                    services.setMenu_price(response.body().getData().getParlors().get(0).getDeals().get(j).getMenuPrice());
                                                    services.setQuantity(response.body().getData().getParlors().get(0).getDeals().get(j).getQuantity());
                                                    services.setParlorTypes(null);

                                                    if(savedCart.getServicesList().get(i).isFree_service())
                                                        services.setFree_service(savedCart.getServicesList().get(i).isFree_service());
                                                    else if(savedCart.getServicesList().get(i).isRemainingService())
                                                        services.setRemainingService(savedCart.getServicesList().get(i).isRemainingService());
                                                    else if(savedCart.getServicesList().get(i).isMyMembershipFreeService())
                                                        services.setMyMembershipFreeService(savedCart.getServicesList().get(i).isMyMembershipFreeService());

                                                    String brand_name= "", brand_id= "", product_name= "", product_id="";
                                                    brand_name= savedCart.getServicesList().get(i).getBrand_name()==null ||
                                                            savedCart.getServicesList().get(i).getBrand_name().equalsIgnoreCase("")?
                                                            "":savedCart.getServicesList().get(i).getBrand_name();
                                                    brand_id= savedCart.getServicesList().get(i).getBrand_id()==null ||
                                                            savedCart.getServicesList().get(i).getBrand_id().equalsIgnoreCase("")?
                                                            "":savedCart.getServicesList().get(i).getBrand_id();
                                                    product_name= savedCart.getServicesList().get(i).getProduct_name()==null ||
                                                            savedCart.getServicesList().get(i).getBrand_name().equalsIgnoreCase("")?
                                                            "":savedCart.getServicesList().get(i).getProduct_name();
                                                    product_id= savedCart.getServicesList().get(i).getProduct_id()==null ||
                                                            savedCart.getServicesList().get(i).getProduct_id().equalsIgnoreCase("")?
                                                            "":savedCart.getServicesList().get(i).getProduct_id();

                                                    services.setBrand_name(brand_name);
                                                    services.setBrand_id(brand_id);
                                                    services.setProduct_name(product_name);
                                                    services.setProduct_id(product_id);

                                                    services.setDescription(brand_name+ " "+product_name);
                                                    services.setPrimary_key(""+savedCart.getServicesList().get(i).getService_code()
                                                            +response.body().getData().
                                                            getParlors().get(0).getDeals().get(j).getParlorDealId()+brand_id+product_id);

                                                    newCart.getServicesList().add(services);
                                                }
                                            }

                                        }
                                    }

                                    try {

                                        DB snappyDB = DBFactory.open(SalonPageActivity.this);
                                        snappyDB.put(AppConstant.USER_CART_DB, newCart);
                                        snappyDB.close();

                                    }catch (SnappydbException e){
                                        e.printStackTrace();
                                    }


                                }
                            }).start();

                        }else{

                            UserCart newCart= new UserCart();
                            newCart.setCartType(AppConstant.SERVICE_TYPE);
                            newCart.setParlorId(response_.getData().getParlorId());
                            newCart.setParlorName(response_.getData().getName());
                            newCart.setParlorType(response_.getData().getParlorType());
                            newCart.setGender(response_.getData().getGender());
                            newCart.setRating(response_.getData().getRating());
                            newCart.setOpeningTime(response_.getData().getOpeningTime());
                            newCart.setClosingTime(response_.getData().getClosingTime());
                            newCart.setAddress1(response_.getData().getAddress1());
                            newCart.setAddress2(response_.getData().getAddress2());
                            newCart.setServicesList(savedCart.getServicesList());
                            try {

                                DB snappyDB = DBFactory.open(SalonPageActivity.this);
                                snappyDB.put(AppConstant.USER_CART_DB, newCart);
                                snappyDB.close();

                            }catch (SnappydbException e){
                                e.printStackTrace();
                            }

                        }

                    }else{

                        Log.i("salonpagedeal", "on unsuccessful");
                    }

                }else{

                    Log.i("salonpagedeal", "on response unsuccessful");
                }

            }

            @Override
            public void onFailure(Call<DealSalonListResponse> call, Throwable t) {
                Log.i("salonpagedeal", "on failure: "+ t.getMessage()+ " " +
                        t.getStackTrace()+ " "+ t.getLocalizedMessage());
            }
        });
    }


    private void updateServerForChekin(String fbAccessTocken){

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface service = retrofit.create(ApiInterface.class);
        FacebookCheckinPost checkinPost=new FacebookCheckinPost();
        checkinPost.setUserId(BeuSalonsSharedPrefrence.getUserId());
        checkinPost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        checkinPost.setFbAccessToken(fbAccessTocken);
        Call<FacebookCheckinResponse> call = service.reqCheckin(checkinPost);


        call.enqueue(new Callback<FacebookCheckinResponse>() {
            @Override
            public void onResponse(Call<FacebookCheckinResponse> call, Response<FacebookCheckinResponse> response) {
                if (response.body().isSuccess()){
                    Toast.makeText(getApplicationContext(),response.body().getData(),Toast.LENGTH_LONG).show();
                }else Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<FacebookCheckinResponse> call, Throwable t) {

            }
        });
    }

    private void enableWifi(){

        linear_wifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final CorporateSuccess success= new CorporateSuccess();
                Bundle bundle= new Bundle();
                bundle.putBoolean("wifi_success", true);
                success.setArguments(bundle);

                if(isWifiConnected){

                    ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo mWifi = connManager.getActiveNetworkInfo();

                    String wifi_name=  '"'+response_.getData().getWifiName()+'"';
                    if(mWifi!=null && mWifi.getType()== ConnectivityManager.TYPE_WIFI &&
                            mWifi.getExtraInfo().equalsIgnoreCase(wifi_name)){      //success

                        success.show(getSupportFragmentManager(), "wifi_popup");
                    }else {         //connect nai hua

                        isWifiConnected= false;
                        img_wifi.setImageResource(R.drawable.wifi_enable);
                        linear_wifi.performClick();
                    }

                }else{

                    WifiManager wifiManager = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                    try{                             //   if ssid will not available it will throw exception
                        if(!wifiManager.isWifiEnabled())
                            wifiManager.setWifiEnabled(true);
                        else
                            wifiManager.disconnect();

                        WifiConfiguration wifiConfig = new WifiConfiguration();
                        wifiConfig.SSID = String.format("\"%s\"", response_.getData().getWifiName());
                        wifiConfig.preSharedKey = String.format("\"%s\"", response_.getData().getWifiPassword());

                        wifiManager.enableNetwork(wifiManager.addNetwork(wifiConfig), true);
                        wifiManager.reconnect();

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    runnable= new Runnable() {
                        @Override
                        public void run() {

                            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                            NetworkInfo mWifi = connManager.getActiveNetworkInfo();

                            String wifi_name=  '"'+response_.getData().getWifiName()+'"';
                            if(mWifi!=null && mWifi.getType()== ConnectivityManager.TYPE_WIFI &&
                                    mWifi.getExtraInfo().equalsIgnoreCase(wifi_name)){      //success

                                isWifiConnected= true;
                                success.show(getSupportFragmentManager(), "wifi_popup");
                                img_wifi.setImageResource(R.drawable.wifi_enabled);
                            }else {         //connect nai hua

                                isWifiConnected= false;
                                Toast.makeText(SalonPageActivity.this, "Error Connecting", Toast.LENGTH_SHORT).show();
                            }
                        }
                    };
                    handler= new Handler();
                    handler.postDelayed(runnable, 6500);
                }
            }
        });

    }



    private void pushServiceToCart(final List<UserServices> servicesList , final String parlorId){

        Handler handler = new Handler();
        for (int i=0;i<servicesList.size();i++) {
            final int finalI = i;
            handler.postDelayed(new Runnable() {
                public void run() {

                    addServiceToCart(parlorId, servicesList.get(finalI).getService_code());
                }

            }, 2000);

        }
    }


    private void addServiceToCart(String parlorId,int serviceCode  ){
        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface= retrofit.create(ApiInterface.class);
        UserCart_post userCartPost=new UserCart_post();
        userCartPost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        userCartPost.setUserId(BeuSalonsSharedPrefrence.getUserId());
        userCartPost.setParlorId(parlorId);
        userCartPost.setServiceCode(serviceCode);
        userCartPost.setQuantity(1);
        Call<AddService_response> call=apiInterface.addServicetoCart(userCartPost);
        call.enqueue(new Callback<AddService_response>() {
            @Override
            public void onResponse(Call<AddService_response> call, Response<AddService_response> response) {
                if (response.isSuccessful()){
                    if (response.body().isSuccess()){
                        Log.e("stuff add service cart", "i'm retrofit getStatus true :(");

                    }else{
                        Log.e("stuff add service cart", "i'm retrofit getStatus false:(");

                    }
                }else{
                    Log.e("stuff add service cart", "i'm retrofit failure :(");

                }
            }

            @Override
            public void onFailure(Call<AddService_response> call, Throwable t) {
                Log.e("stuff add service cart", "i'm in failure: "+ t.getMessage()+ "   "+
                        t.getStackTrace()+ t.getCause()+ " "+ t.getLocalizedMessage());
            }
        });

    }



    private void checkWifi(){

        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getActiveNetworkInfo();

        String wifi_name=  '"'+response_.getData().getWifiName()+'"';
        if(mWifi!=null && mWifi.getType()== ConnectivityManager.TYPE_WIFI &&
                mWifi.getExtraInfo().equalsIgnoreCase(wifi_name)){              //wifi enabled hai

            isWifiConnected=true;
            img_wifi.setImageResource(R.drawable.wifi_enabled);
        }else{                                                          //wifi not enabled

            isWifiConnected=false;
            img_wifi.setImageResource(R.drawable.wifi_enable);
        }
    }

    private void setClicks(){

        linear_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                logCallButtonClickedEvent();
                logCallButtonClickedFireBaseEvent();
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:"+response_.getData().getPhoneNumber()));
                startActivity(callIntent);
            }
        });

        linear_photos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logPhotosButtonClickedEvent();
                logPhotosButtonClickedFireBaseEvent();

                ArrayList<String> list= new ArrayList<>();
                for(int i=0; i<response_.getData().getImages().size();i++){
                    if(response_.getData().getImages().get(i).getAppImageUrl()!=null &&
                            !response_.getData().getImages().get(i).getAppImageUrl().equalsIgnoreCase(""))
                        list.add(response_.getData().getImages().get(i).getAppImageUrl());
                }

                SalonImagesFragment fragment= new SalonImagesFragment();
                Bundle bundle= new Bundle();
                bundle.putStringArrayList(SalonImagesFragment.IMAGES, list);
                fragment.setArguments(bundle);
                fragment.show(((SalonPageActivity)v.getContext()).getFragmentManager(), SalonImagesFragment.IMAGES);

//                Intent intent1= new Intent(SalonPageActivity.this, SalonImagesFragment.class);
//                intent1.putExtra("parlorImage", response_.getData().getImages().get(0).getAppImageUrl());
//                startActivity(intent1);
            }
        });
img_salon.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        linear_photos.performClick();
    }
});
        linear_reviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logReviewsButtonClickedEvent();
                logReviewsButtonClickedFireBaseEvent();
                Intent intent= new Intent(SalonPageActivity.this, SalonReviewActivity.class);
                intent.putExtra(SalonReviewActivity.SALON_ID, response_.getData().getParlorId());
//                intent.putExtra("parlorImage", response_.getData().getImages().get(0).getAppImageUrl());
//                intent.putExtra("parlorId", response_.getData().getParlorId());
//                intent.putExtra("parlorName", response_.getData().getName());
                startActivity(intent);
            }
        });

        linear_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logDirectionButtonClickedEvent();
                logDirectionButtonClickedFireBaseEvent();

                //    if(Build.VERSION.SDK_INT < 23){

                //  checkLocation();
                Uri uri = Uri.parse("http://maps.google.com/maps?q=" + response_.getData().getLatitude() + "," + response_.getData().getLongitude() + "(" + response_.getData().getName() + ")&iwloc=A&hl=es");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
               /* }else {
                    requestContactPermission();
                }*/
            }
        });

    }

    private  void setRecentRatings(){
        if (response_.getData().getRecentRatings()!=null &&
                response_.getData().getRecentRatings().size()>0){

            linear_ratings.setVisibility(View.VISIBLE);

            linear_ratings_.removeAllViews();
            for (int i=0;i<response_.getData().getRecentRatings().size();i++){
                CircularTextView textView= (CircularTextView) getLayoutInflater().inflate(
                        R.layout.row_circular_text,null);

                textView.setText(response_.getData().getRecentRatings().get(i)+"");
                if (response_.getData().getRecentRatings().get(i)==1){
                    textView.setStrokeColor("#ef433b");
                }else if(response_.getData().getRecentRatings().get(i)==2){
                    textView.setStrokeColor("#ef433b");
                }else if(response_.getData().getRecentRatings().get(i)==3){
                    textView.setStrokeColor("#eccb02");
                }else if(response_.getData().getRecentRatings().get(i)==4){
                    textView.setStrokeColor("#66992e");
                }else if(response_.getData().getRecentRatings().get(i)==5){
                    textView.setStrokeColor("#66992e");
                }else{
                    textView.setStrokeColor("#ef433b");
                }
                LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                llp.setMargins(10, 0, 0, 0);
                textView.setLayoutParams(llp);
                linear_ratings_.addView(textView);
            }


        }else
            linear_ratings.setVisibility(View.GONE);

    }

    private void favSalon(final String parlor_id){
        linear_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(salon_favourite){

                    img_favourite.setImageResource(R.drawable.ic_favourite_white);
                    salon_favourite= false;

                    SalonFavpost post= new SalonFavpost();
                    post.setUserId(BeuSalonsSharedPrefrence.getUserId());
                    post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
                    post.setParlorId(parlor_id);

                    Log.i("parlordetail", "i'm in salon favourite true: "+ post.getUserId()+ "  "+ post.getAccessToken());

                    Retrofit retrofit = ServiceGenerator.getClient();
                    ApiInterface service = retrofit.create(ApiInterface.class);
                    Call<SalonFavResponse> call_delete= service.deleteSalonFavourite(post);
                    call_delete.enqueue(new Callback<SalonFavResponse>() {
                        @Override
                        public void onResponse(Call<SalonFavResponse> call, Response<SalonFavResponse> response) {

                            if(response.isSuccessful()){
                                if(response.body().isSuccess()){

                                    Log.i("parlordetail", "i'm in delete favourite success: "+ response.body().getData());



                                }else{

                                    Log.i("parlordetail", "i'm in delete favourite unsuccessful");
                                }
                            }else{

                                Log.i("parlordetail", "i'm in delete favourite retrofit unsuccessful");
                            }

                        }

                        @Override
                        public void onFailure(Call<SalonFavResponse> call, Throwable t) {

                            Log.i("parlordetail", "i'm in delete favourite failure: "+ t.getMessage()+ "  "+t.getCause());
                        }
                    });

                }else{

                    img_favourite.setImageResource(R.drawable.ic_favourite_red);
                    salon_favourite= true;

                    SalonFavpost post= new SalonFavpost();
                    post.setUserId(BeuSalonsSharedPrefrence.getUserId());
                    post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
                    post.setParlorId(parlor_id);

                    Log.i("parlordetail", "i'm in salon favourite false: "+ post.getUserId()+ "  "+ post.getAccessToken());

                    Retrofit retrofit = ServiceGenerator.getClient();
                    ApiInterface service = retrofit.create(ApiInterface.class);
                    Call<SalonFavResponse> call_post= service.postSalonFavourite(post);
                    call_post.enqueue(new Callback<SalonFavResponse>() {
                        @Override
                        public void onResponse(Call<SalonFavResponse> call, Response<SalonFavResponse> response) {

                            if(response.isSuccessful()){
                                if(response.body().isSuccess()){

                                    Log.i("parlordetail", "i'm in  favourite success: "+ response.body().getData());



                                }else{

                                    Log.i("parlordetail", "i'm in  favourite unsuccessful");
                                }
                            }else{

                                Log.i("parlordetail", "i'm in  favourite retrofit unsuccessful");
                            }

                        }

                        @Override
                        public void onFailure(Call<SalonFavResponse> call, Throwable t) {

                            Log.i("parlordetail", "i'm in  favourite failure: "+ t.getMessage()+ "  "+t.getCause());
                        }
                    });
                }
            }
        });
    }

    private void checkLocation(){

        if(mGoogleApiClient==null){
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        final LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.
                checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i("salonpagelog", "All location settings are satisfied.");

                        Bundle bundle= new Bundle();
                        bundle.putDouble("latitude", response_.getData().getLatitude());
                        bundle.putDouble("longitude", response_.getData().getLongitude());
                        bundle.putString("parlorName", response_.getData().getName());
                        bundle.putString("parlorAddress", response_.getData().getAddress1());
                        Intent intent= new Intent(SalonPageActivity.this, Activity_GoogleMaps.class);

                        intent.putExtras(bundle);
                        startActivity(intent);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i("salonpagelog", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {

                            status.startResolutionForResult(SalonPageActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {

                            Log.i("salonpagelog", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i("salonpagelog", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_CHECK_SETTINGS && resultCode==RESULT_OK){

            Log.i("salonpagelog", "i'm in on activity result");
            Bundle bundle= new Bundle();
            bundle.putDouble("latitude", response_.getData().getLatitude());
            bundle.putDouble("longitude", response_.getData().getLongitude());
            bundle.putString("parlorName", response_.getData().getName());
            bundle.putString("parlorAddress", response_.getData().getAddress1());
            Intent intent= new Intent(SalonPageActivity.this, Activity_GoogleMaps.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);            //facebook ka hai yeh
    }

    private void requestContactPermission() {

        int hasContactPermission = ActivityCompat.checkSelfPermission(this , android.Manifest.permission.ACCESS_FINE_LOCATION);

        if(hasContactPermission != PackageManager.PERMISSION_GRANTED ) {
            Log.i("permissions", "i'm in not onReqest permis granterd");
            ActivityCompat.requestPermissions(this, new String[]   {android.Manifest.permission.ACCESS_FINE_LOCATION}, GPS_PERMISSION);
        }else {
            Log.i("permissions", "i'm in already onReqest permis granterd");
            checkLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case GPS_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("permissions", "i'm in onReqest permis granterd");
                    checkLocation();
                } else {
                    Log.i("permissions", "i'm in permission denied onReqest permis granterd");
                    Uri uri= Uri.parse("http://maps.google.com/maps?q="+
                            response_.getData().getLatitude()+"," +
                            response_.getData().getLongitude()+"("+ response_.getData().getName() +
                            ")&iwloc=A&hl=es");
                    Intent intent= new Intent(Intent.ACTION_VIEW, uri);
                    intent.setPackage("com.google.android.apps.maps");
                    startActivity(intent);
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mGoogleApiClient!=null){
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        logSalonProfileBackButtonFireBaseEvent();
        logSalonProfileBackButtonEvent();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mGoogleApiClient!=null){
            mGoogleApiClient.disconnect();
        }

        if(runnable!=null && handler!=null)
            handler.removeCallbacks(runnable);
    }

    public void logShareSalonEvent (String salonName) {
        Bundle params = new Bundle();
        params.putString(AppConstant.SalonName, salonName);
        logger.logEvent(AppConstant.ShareSalon, params);
    }
    public void logBookAppointmentClickedEvent () {
        Log.e("BookAppointmentClicked","fine");
        logger.logEvent(AppConstant.BookAppointmentClicked);
    }
    public void logBookAppointmentClickedFireBaseEvent () {
        Log.e("BookAppointmentClicked","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.BookAppointmentClicked,bundle);
    }


    public void logCallButtonClickedEvent () {
        Log.e("CallButtonClicked","fine");
        logger.logEvent(AppConstant.CallButtonClicked);
    }
    public void logCallButtonClickedFireBaseEvent () {
        Log.e("CallButtonClickedFire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.CallButtonClicked,bundle);
    }

    public void logPhotosButtonClickedEvent () {
        Log.e("PhotosButtonClicked","fine");
        logger.logEvent(AppConstant.PhotosButtonClicked);
    }
    public void logPhotosButtonClickedFireBaseEvent () {
        Log.e("PhotosButtonClickedfire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.PhotosButtonClicked,bundle);
    }
    public void logReviewsButtonClickedEvent () {
        Log.e("ReviewsButtonClicked","fine");
        logger.logEvent(AppConstant.ReviewsButtonClicked);
    }
    public void logReviewsButtonClickedFireBaseEvent () {
        Log.e("ReviewsButtonfire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.ReviewsButtonClicked,bundle);
    }
    public void logDirectionButtonClickedEvent () {
        Log.e("DirectionButtonClicked","fine");
        logger.logEvent(AppConstant.DirectionButtonClicked);
    }
    public void logDirectionButtonClickedFireBaseEvent () {
        Log.e("DirectionButtonClifire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.DirectionButtonClicked,bundle);
    }
    public void logSalonProfileBackButtonEvent () {
        Log.e("SalonProfileBackButton","fine");
        logger.logEvent(AppConstant.SalonProfileBackButton);
    }
    public void logSalonProfileBackButtonFireBaseEvent () {
        Log.e("SalonProfileBackfire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.SalonProfileBackButton,bundle);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
