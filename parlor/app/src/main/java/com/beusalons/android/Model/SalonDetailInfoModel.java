package com.beusalons.android.Model;

/**
 * Created by myMachine on 11/30/2016.
 */

public class SalonDetailInfoModel {

    private String info;

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
