package com.beusalons.android.Fragment;

import android.app.DialogFragment;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beusalons.android.Adapter.NewServiceDeals.ServicesAdapter.NewBrandAdapter;
import com.beusalons.android.Adapter.NewServiceDeals.ServicesAdapter.NewProductAdapter;
import com.beusalons.android.Adapter.NewServiceDeals.ServicesAdapter.NewServiceAdapter;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.UserCart.PackageService;
import com.beusalons.android.Model.newServiceDeals.NewCombo.Selector;
import com.beusalons.android.R;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.facebook.appevents.AppEventsLogger;
import com.google.gson.Gson;

/**
 * Created by myMachine on 09-Dec-17.
 */

public class DialogFragmentServices extends DialogFragment {

    public static final String DIALOG= "com.beusalons."+DialogFragmentServices.class.getSimpleName();
    public static final String SERVICE_DATA= "com.beusalons.dialog.fragment.service.data";
    public static final String DATA= "com.beusalons.dialog.fragment.data";

    private PackageService packageService;
    private Selector selector;
    private boolean isCombo= false;
    AppEventsLogger logger;

    private NewServiceAdapter serviceAdapter= null;
    private NewBrandAdapter brandAdapter= null;
    private NewProductAdapter productAdapter= null;
    private boolean has_service=false, has_brand=false, has_product=false;

    private int service_index=0, brand_index=0, product_index=0;
    private String  isService;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle= getArguments();
        if(bundle!=null &&
                bundle.containsKey(SERVICE_DATA)){

            selector= new Gson().fromJson(bundle.getString(SERVICE_DATA,
                    null), Selector.class);
            packageService= new Gson().fromJson(bundle.getString(DATA,
                    null), PackageService.class);
            isService=bundle.getString("isService");
            if(selector!=null &&
                    selector.getServices()!=null &&
                    selector.getServices().size()>0)
                isCombo= true;
            else
                isCombo= false;

        }
    }

    public interface FragmentListener{
        void onClick(PackageService packageService);
    }
    private FragmentListener listener;
    public void setListener(FragmentListener listener){
        this.listener= listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);          //no action bar space
        LinearLayout linear_= (LinearLayout)
                inflater.inflate(R.layout.dialog_fragment_services, container, false);

        logger = AppEventsLogger.newLogger(getActivity());


        TextView txt_name= linear_.findViewById(R.id.txt_name);
        TextView txt_description= linear_.findViewById(R.id.txt_description);
        TextView txt_menu_price= linear_.findViewById(R.id.txt_menu_price);
        TextView txt_save_per= linear_.findViewById(R.id.txt_save_per);
        TextView txt_price= linear_.findViewById(R.id.txt_price);
        TextView txt_price_text= linear_.findViewById(R.id.txt_price_text);

        txt_name.setText(packageService.getName());

        if(packageService.getDescription()!=null){
            txt_description.setVisibility(View.VISIBLE);
            txt_description.setText(packageService.getDescription());
        }else
            txt_description.setVisibility(View.GONE);

        RelativeLayout relative_price= linear_.findViewById(R.id.relative_price);

        if(packageService.getPrice()==0){       //deal mai hai

            if(packageService.getStartAt()!=null &&
                    !packageService.getStartAt().equalsIgnoreCase("")){
                relative_price.setVisibility(View.VISIBLE);

                txt_price_text.setText("Lowest Price: ");
                txt_price.setText(AppConstant.CURRENCY+packageService.getStartAt());

                String txt= "<font color='#58595b'>Saving Upto </font>"+"<font color='#3e780a'>"+packageService.getSave()+"%</font>";
                txt_save_per.setText(fromHtml(txt));
                txt_save_per.setTypeface(null, Typeface.ITALIC);
            }else{
                relative_price.setVisibility(View.GONE);
            }




        }else{          //salon ke andar
            relative_price.setVisibility(View.VISIBLE);

            txt_price.setVisibility(View.VISIBLE);
            txt_menu_price.setVisibility(View.VISIBLE);
            txt_save_per.setVisibility(View.VISIBLE);


            txt_price_text.setText("Service Price: ");
            //with tax
            int price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                    packageService.getPrice());
            int menu_price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                    packageService.getMenu_price());




            txt_price.setText(AppConstant.CURRENCY+ packageService.getPrice());


            if(menu_price_>price_){
                txt_menu_price.setText(AppConstant.CURRENCY+ packageService.getMenu_price());
                txt_menu_price.setPaintFlags(txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                txt_save_per.setText(AppConstant.SAVE+" "+
                        (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                                price_)*100)/
                                menu_price_))+"%");
                txt_save_per.setBackgroundResource(R.drawable.discount_seletor);
            }


        }

        setRecyclerData(linear_);

        TextView txt_cancel= linear_.findViewById(R.id.txt_cancel);
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 if (isService!=null && isService.equals("false")){
                     logPackageBottomSheetChangeCancelEvent(packageService.getName());


                 }else if (isService!=null &&isService.equals("true")){
                     logDealServicePopupCancelEvent(packageService.getName());

                 }
                getDialog().dismiss();

            }
        });



        return linear_;
    }

    private void setRecyclerData(final LinearLayout linear_){

        TextView txt_step1= linear_.findViewById(R.id.txt_step1);
        TextView txt_step2= linear_.findViewById(R.id.txt_step2);
        TextView txt_step3= linear_.findViewById(R.id.txt_step3);

        TextView txt_mentioned_1= linear_.findViewById(R.id.txt_mentioned_1);
        TextView txt_mentioned_2= linear_.findViewById(R.id.txt_mentioned_2);
        TextView txt_mentioned_3= linear_.findViewById(R.id.txt_mentioned_3);
        txt_mentioned_1.setTypeface(null, Typeface.ITALIC);
        txt_mentioned_2.setTypeface(null, Typeface.ITALIC);
        txt_mentioned_3.setTypeface(null, Typeface.ITALIC);

        LinearLayout linear_1= linear_.findViewById(R.id.linear_1);
        LinearLayout linear_2= linear_.findViewById(R.id.linear_2);
        LinearLayout linear_3= linear_.findViewById(R.id.linear_3);

        //service
        RecyclerView rec_1= linear_.findViewById(R.id.rec_1);
        rec_1.setLayoutManager(new LinearLayoutManager(linear_.getContext(),
                LinearLayoutManager.VERTICAL, false){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });

        //brands
        RecyclerView rec_2= linear_.findViewById(R.id.rec_2);
        rec_2.setLayoutManager(new LinearLayoutManager(linear_.getContext(),
                LinearLayoutManager.VERTICAL, false){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });

        //products
        RecyclerView rec_3= linear_.findViewById(R.id.rec_3);
        rec_3.setLayoutManager(new LinearLayoutManager(linear_.getContext(),
                LinearLayoutManager.VERTICAL, false){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });

        if(isCombo){

            if(selector!=null &&
                    selector.getServices()!=null &&
                    selector.getServices().size()>0){
                has_service= true;
                if(selector.getServices().get(service_index).getBrands()!=null &&
                        selector.getServices().get(service_index).getBrands().size()>0){
                    has_brand= true;
                    if(selector.getServices().get(service_index).getBrands().get(brand_index).getProducts()!=null &&
                            selector.getServices().get(service_index).getBrands().get(brand_index).getProducts().size()>0){
                        has_product= true;
                    }else
                        has_product= false;
                }else{
                    has_brand= false;
                    has_product= false;
                }

            }else{
                has_service= false;
                has_brand= false;
                has_product= false;
            }

            if(has_service){

                //boolean ko false karne ka loop
                for(int i=0;i<selector.getServices().size();i++)
                    selector.getServices().get(i).setCheck(false);
                selector.getServices().get(service_index).setCheck(true);

                serviceAdapter= new NewServiceAdapter(selector.getServices(), has_brand);
                serviceAdapter.setListener(new NewServiceAdapter.ServiceListener() {
                    @Override
                    public void onClick(int index) {

                        service_index= index;
                        brand_index= 0;
                        product_index= 0;
                        setRecyclerData(linear_);
                    }
                });
                rec_1.setAdapter(serviceAdapter);

                if(has_brand){
                    linear_2.setVisibility(View.VISIBLE);

                    //boolean ko false karne ka loop
                    for(int i=0;i<selector.getServices().get(service_index).getBrands().size();i++)
                        selector.getServices().get(service_index).getBrands().get(i).setCheck(false);
                    selector.getServices().get(service_index).getBrands().get(brand_index).setCheck(true);

                    brandAdapter= new NewBrandAdapter(selector.getServices().get(service_index).getBrands(),
                            has_product);
                    brandAdapter.setListener(new NewBrandAdapter.BrandListener() {
                        @Override
                        public void onClick(int index) {

                            brand_index= index;
                            product_index= 0;
                            setRecyclerData(linear_);
                        }
                    });
                    rec_2.setAdapter(brandAdapter);

                    if(has_product){

                        linear_3.setVisibility(View.VISIBLE);

                        //boolean ko false karne ka loop
                        for(int i=0;i< selector.getServices().get(service_index).getBrands().
                                get(brand_index).getProducts().size();i++)
                            selector.getServices().get(service_index).getBrands().
                                    get(brand_index).getProducts().get(i).setCheck(false);
                        selector.getServices().get(service_index).getBrands().
                                get(brand_index).getProducts().get(product_index).setCheck(true);

                        productAdapter= new NewProductAdapter(selector.getServices()
                                .get(service_index).getBrands().get(brand_index).getProducts(), false);

                        rec_3.setAdapter(productAdapter);

                    }else
                        linear_3.setVisibility(View.GONE);


                }else{
                    linear_2.setVisibility(View.GONE);
                    linear_3.setVisibility(View.GONE);
                }

                if(selector.getServices().size() ==1){
                    linear_1.setVisibility(View.GONE);

                    packageService.setService_name(selector.getServices().get(service_index).getName());
                    packageService.setService_code(selector.getServices().get(service_index).getServiceCode());
                    packageService.setService_id(selector.getServices().get(service_index).getServiceId());
                    packageService.setPrice(selector.getServices().get(service_index).getPrice());
                    packageService.setMenu_price(selector.getServices().get(service_index).getMenuPrice());
                }



            }

            if(has_product){

                if(selector.getServices().size()==1){

                    txt_mentioned_1.setVisibility(View.GONE);
                    txt_mentioned_2.setVisibility(View.GONE);
                    txt_mentioned_3.setVisibility(View.VISIBLE);

                    txt_step2.setText("Step 1 Of 2");
                    txt_step3.setText("Step 2 Of 2");
                }else{

                    txt_mentioned_1.setVisibility(View.GONE);
                    txt_mentioned_2.setVisibility(View.GONE);
                    txt_mentioned_3.setVisibility(View.VISIBLE);

                    txt_step1.setText("Step 1 Of 3");
                    txt_step2.setText("Step 2 Of 3");
                    txt_step3.setText("Step 3 Of 3");
                }

            }else if(has_brand){

                if(selector.getServices().size()==1){
                    txt_step2.setText("Step 1 Of 1");

                    txt_mentioned_1.setVisibility(View.GONE);
                    txt_mentioned_2.setVisibility(View.VISIBLE);
                    txt_mentioned_3.setVisibility(View.GONE);
                }else{
                    txt_step1.setText("Step 1 Of 2");
                    txt_step2.setText("Step 2 Of 2");

                    txt_mentioned_1.setVisibility(View.GONE);
                    txt_mentioned_2.setVisibility(View.VISIBLE);
                    txt_mentioned_3.setVisibility(View.GONE);
                }

            }else if(has_service){

                txt_step1.setText("Step 1 Of 1");

                txt_mentioned_1.setVisibility(View.VISIBLE);
                txt_mentioned_2.setVisibility(View.GONE);
                txt_mentioned_3.setVisibility(View.GONE);
            }

        }else{          //new combo ka case


            if(selector!=null &&
                    selector.getBrands()!=null &&
                    selector.getBrands().size()>0){
                has_brand= true;
                if(selector.getBrands().get(brand_index).getProducts()!=null &&
                        selector.getBrands().get(brand_index).getProducts().size()>0){
                    has_product= true;
                    if(selector.getBrands().get(brand_index).getProducts().get(product_index).getServices()!=null &&
                            selector.getBrands().get(brand_index).getProducts().get(product_index).getServices().size()>0){
                        has_service= true;
                    }else
                        has_service= false;
                }else{
                    has_product= false;
                    has_service= false;
                }
            }else{
                has_brand= false;
                has_product= false;
                has_service= false;
            }

            if(has_brand){

                //boolean ka stuff
                for(int i=0;i<selector.getBrands().size();i++)
                    selector.getBrands().get(i).setCheck(false);
                selector.getBrands().get(brand_index).setCheck(true);

                brandAdapter= new NewBrandAdapter(selector.getBrands(), has_product);
                brandAdapter.setListener(new NewBrandAdapter.BrandListener() {
                    @Override
                    public void onClick(int index) {

                        brand_index= index;
                        product_index= 0;
                        service_index= 0;
                        setRecyclerData(linear_);
                    }
                });
                rec_1.setAdapter(brandAdapter);

                //yaha sab ulta hai
                if(has_product){

                    if(selector.getBrands().get(brand_index).getProducts().get(0).getProductId()==null)
                        linear_2.setVisibility(View.GONE);
                    else{

                        linear_2.setVisibility(View.VISIBLE);
                        //boolean ka stuff
                        for(int i=0;i<selector.getBrands().get(brand_index).getProducts().size();i++)
                            selector.getBrands().get(brand_index).getProducts().get(i).setCheck(false);
                        selector.getBrands().get(brand_index).getProducts().get(product_index).setCheck(true);

                        productAdapter= new NewProductAdapter(selector.getBrands().get(brand_index).getProducts(),
                                has_service);
                        productAdapter.setListener(new NewProductAdapter.ProductListener() {
                            @Override
                            public void onClick(int index) {

                                product_index= index;
                                service_index= 0;
                                setRecyclerData(linear_);
                            }
                        });
                        rec_2.setAdapter(productAdapter);
                    }


                    if(has_service){

                        linear_3.setVisibility(View.VISIBLE);
                        //boolean ka stuff
                        for(int i=0;i<selector.getBrands().get(brand_index).getProducts().get(product_index).getServices().size();
                            i++)
                            selector.getBrands().get(brand_index).getProducts().get(product_index).getServices()
                                    .get(i).setCheck(false);
                        selector.getBrands().get(brand_index).getProducts().get(product_index).getServices()
                                .get(service_index).setCheck(true);
                        serviceAdapter= new NewServiceAdapter( selector.getBrands().get(brand_index)
                                .getProducts().get(product_index).getServices(), false);
                        rec_3.setAdapter(serviceAdapter);

                    }else
                        linear_3.setVisibility(View.GONE);

                }else{
                    linear_2.setVisibility(View.GONE);
                    linear_3.setVisibility(View.GONE);
                }

            }

            if(has_service){

                if(selector.getBrands().get(brand_index).getProducts().get(0).getProductId()==null){

                    txt_mentioned_1.setVisibility(View.GONE);
                    txt_mentioned_2.setVisibility(View.GONE);
                    txt_mentioned_3.setVisibility(View.VISIBLE);

                    txt_step1.setText("Step 1 Of 2");
                    txt_step3.setText("Step 2 Of 2");

                }else{
                    txt_step1.setText("Step 1 Of 3");
                    txt_step2.setText("Step 2 Of 3");
                    txt_step3.setText("Step 3 Of 3");

                    txt_mentioned_1.setVisibility(View.GONE);
                    txt_mentioned_2.setVisibility(View.GONE);
                    txt_mentioned_3.setVisibility(View.VISIBLE);
                }


            }else if(has_product){

                if(selector.getBrands().get(brand_index).getProducts().get(0).getProductId()==null){
                    txt_step1.setText("Step 1 Of 1");

                    txt_mentioned_1.setVisibility(View.VISIBLE);
                    txt_mentioned_2.setVisibility(View.GONE);
                    txt_mentioned_3.setVisibility(View.GONE);
                }else{

                    txt_step1.setText("Step 1 Of 2");
                    txt_step2.setText("Step 2 Of 2");

                    txt_mentioned_1.setVisibility(View.GONE);
                    txt_mentioned_2.setVisibility(View.VISIBLE);
                    txt_mentioned_3.setVisibility(View.GONE);
                }

            }else if(has_brand){

                txt_step1.setText("Step 1 Of 1");

                txt_mentioned_1.setVisibility(View.VISIBLE);
                txt_mentioned_2.setVisibility(View.GONE);
                txt_mentioned_3.setVisibility(View.GONE);
            }

        }


        TextView txt_confirm= linear_.findViewById(R.id.txt_confirm);
        txt_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sendData();
                if (isService!=null && isService.equals("false")){
                    logPackageBottomSheetChangeConfirmEvent( packageService.getService_name(),
                            packageService.getBrand_name(),packageService.getProduct_name());
                }else if (isService!=null && isService.equals("true")){
                    logDealServicePopupConfirmEvent(packageService.getName(),
                            packageService.getService_name(),packageService.getBrand_name(),packageService.getProduct_name());

                }

                getDialog().dismiss();
            }
        });

    }

    private void sendData(){

        if(isCombo){        //combo: service brand product

            if(selector.getServices().size()==1){
                //do nothing
            }else{

                packageService.setService_name(serviceAdapter.getService_name());
                packageService.setService_code(serviceAdapter.getService_code());
                packageService.setService_id(serviceAdapter.getService_id());
            }

            if(has_product){
                packageService.setBrand_id(brandAdapter.getBrand_id());
                packageService.setBrand_name(brandAdapter.getBrand_name());
                packageService.setProduct_id(productAdapter.getProduct_id());
                packageService.setProduct_name(productAdapter.getProduct_name());
                packageService.setPrice((int)productAdapter.getPrice());
                packageService.setMenu_price((int)productAdapter.getMenu_price());
            }else if(has_brand){
                packageService.setBrand_id(brandAdapter.getBrand_id());
                packageService.setBrand_name(brandAdapter.getBrand_name());
                packageService.setPrice((int)brandAdapter.getPrice());
                packageService.setMenu_price((int)brandAdapter.getMenu_price());
            }else if(has_service){

                if(selector.getServices().size()==1){
                    //do nothing
                }else{

                    packageService.setPrice((int)serviceAdapter.getPrice());
                    packageService.setMenu_price((int)serviceAdapter.getMenu_price());
                }

            }
        }else{          //new combo: brand product service

            if(has_service){

                packageService.setBrand_id(brandAdapter.getBrand_id());
                packageService.setBrand_name(brandAdapter.getBrand_name());

                if(selector.getBrands().get(brand_index).getProducts().get(0).getProductId()!=null){

                    packageService.setProduct_id(productAdapter.getProduct_id());
                    packageService.setProduct_name(productAdapter.getProduct_name());
                }


                packageService.setService_name(serviceAdapter.getService_name());
                packageService.setService_code(serviceAdapter.getService_code());
                packageService.setService_id(serviceAdapter.getService_id());
                packageService.setPrice((int)serviceAdapter.getPrice());
                packageService.setMenu_price((int)serviceAdapter.getMenu_price());
            }else if(has_product){

                packageService.setBrand_id(brandAdapter.getBrand_id());
                packageService.setBrand_name(brandAdapter.getBrand_name());

                if(selector.getBrands().get(brand_index).getProducts().get(0).getProductId()!=null){

                    packageService.setProduct_id(productAdapter.getProduct_id());
                    packageService.setProduct_name(productAdapter.getProduct_name());
                    packageService.setPrice((int)productAdapter.getPrice());
                    packageService.setMenu_price((int)productAdapter.getMenu_price());
                }


            }else if(has_brand){

                packageService.setBrand_id(brandAdapter.getBrand_id());
                packageService.setBrand_name(brandAdapter.getBrand_name());
                packageService.setPrice((int)brandAdapter.getPrice());
                packageService.setMenu_price((int)brandAdapter.getMenu_price());
            }

        }

        if(listener!=null)
            listener.onClick(packageService);
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logPackageBottomSheetChangeCancelEvent (String serviceName) {
        Log.e("logPackageChangeCance","fine"+serviceName);

        Bundle params = new Bundle();
        params.putString(AppConstant.ServiceName, serviceName);
        logger.logEvent(AppConstant.PackageBottomSheetChangeCancel, params);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logPackageBottomSheetChangeConfirmEvent (String serviceName, String brand, String product) {
        Log.e("logpackPopConfEvent","fine" +"service" +serviceName +"brand"+ brand + "product"+product);

        Bundle params = new Bundle();
        params.putString(AppConstant.ServiceName, serviceName+"-"+brand+"-"+product);
//        params.putString(AppConstant.Brand, brand);
//        params.putString(AppConstant.Product, product);
        logger.logEvent(AppConstant.PackageBottomSheetChangeConfirm, params);
    }
    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logDealServicePopupCancelEvent (String dealName) {
        Log.e("logDealPopupCancelEvent","fine"+dealName);

        Bundle params = new Bundle();
        params.putString(AppConstant.DealName, dealName);
        logger.logEvent(AppConstant.DealServicePopupCancel, params);

    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logDealServicePopupConfirmEvent (String dealName, String serviceName, String brand, String product) {
        Log.e("logDealServicePopup","fine"+dealName +"service" +serviceName +"brand"+ brand + "product"+product);

        Bundle params = new Bundle();
        params.putString(AppConstant.DealName, dealName+"-"+serviceName+"-"+brand+"-"+product);
//        params.putString(AppConstant.ServiceName, serviceName);
//        params.putString(AppConstant.Brand, brand);
//        params.putString(AppConstant.Product, product);
        logger.logEvent(AppConstant.DealServicePopupConfirm, params);
    }

}
