package com.beusalons.android.Model.newServiceDeals.ServiceByDepartment;

import java.util.List;

/**
 * Created by myMachine on 5/30/2017.
 */

public class Brand__ {

    private String id;
    private String name;
    private double ratio;
    private String brandId;
    private List<Product_> products = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRatio() {
        return ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public List<Product_> getProducts() {
        return products;
    }

    public void setProducts(List<Product_> products) {
        this.products = products;
    }
}
