package com.beusalons.android.Model.selectArtist;

/**
 * Created by Ashish Sharma on 12/29/2017.
 */

public class SelectEmployeePost {
private String appointmentId;
private String userId;
private String accessToken;

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
