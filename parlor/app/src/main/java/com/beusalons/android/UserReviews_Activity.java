package com.beusalons.android;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.beusalons.android.Utility.Utility;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.beusalons.android.Adapter.UserReviewsList_Adapter;
import com.beusalons.android.Model.Reviews.UserReviewsData;
import com.beusalons.android.Model.Reviews.UserReviewsPost;
import com.beusalons.android.Model.Reviews.UserReviewsResponse;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class UserReviews_Activity extends AppCompatActivity {

    private static final String TAG= AppCompatActivity.class.getSimpleName();

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private TextView userName,txtViewActionBarName;
    private ImageView userImage,imgViewBack;

    Retrofit retrofit;

//    private ProgressDialog progress;

    List<UserReviewsData> details= new ArrayList<>();

    private View mContentView;
    private View mLoadingView;

    private int mShortAnimationDuration;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_reviews);

        setToolBar();

        //new ToolbarHelper(getSupportActionBar(), getLayoutInflater(), true, this);

        mContentView= findViewById(R.id.relative_user_reviews);
        mLoadingView= findViewById(R.id.loading_spinner_user_reviews);
        // Initially hide the content view.
        mContentView.setVisibility(View.GONE);

        // Retrieve and cache the system's default "short" animation time.
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        recyclerView= (RecyclerView)findViewById(R.id.recycle_user_reviews);
        layoutManager= new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter= new UserReviewsList_Adapter(details, this);

        recyclerView.setAdapter(adapter);

        //progress dialog
//        progress= DialogSpinner.progressDialog(this, "Loading..");
//        progress.show();


        fetchData();

    }
    private void setToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(getResources().getString(R.string.txt_myReview_title));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);

        }


    }
    public void fetchData(){

        userName= (TextView)findViewById(R.id.txt_user_reviews_user_name);
        userImage= (ImageView) findViewById(R.id.img_MyReviews_userImage);

        SharedPreferences preferences= getSharedPreferences("userDetails", Context.MODE_PRIVATE);

        String name= "User Name";
        String profilePic= "", userId= "", accessToken= "";
        if(preferences!=null) {
            name = preferences.getString("name", "Name");
            profilePic= preferences.getString("profilePic", "?");
            userId= preferences.getString("userId", "");
            accessToken= preferences.getString("accessToken", "");

        }
        userName.setText(name);
        try{
            Glide.with(this).load(profilePic).apply(RequestOptions.circleCropTransform()).into(userImage);
          /*  Glide.with(this)
                    .load(profilePic).asBitmap().centerCrop()
                    .into(new BitmapImageViewTarget(userImage){
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            userImage.setImageDrawable(circularBitmapDrawable);
                        }
                    });*/
        }catch (Exception e){
            e.printStackTrace();
        }


        UserReviewsPost post= new UserReviewsPost();
        post.setUserId(userId);
        post.setAccessToken(accessToken);

        retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<UserReviewsResponse> call= apiInterface.userReviews(post);
        call.enqueue(new Callback<UserReviewsResponse>() {
            @Override
            public void onResponse(Call<UserReviewsResponse> call, Response<UserReviewsResponse> response) {

                Log.i(TAG, "I'm in UserReview response success :D");

                if(response.isSuccessful()){

                    if(response.body().getSuccess()){
                        UserReviewsResponse reviewsResponse= response.body();

                        if(reviewsResponse!=null){

                            for(int i=0; i<reviewsResponse.getData().size();i++){

                                UserReviewsData data= new UserReviewsData();
                                data.setParlorId(reviewsResponse.getData().get(i).getParlorId());
                                data.setName(reviewsResponse.getData().get(i).getName());
                                data.setAddress(reviewsResponse.getData().get(i).getAddress());
                                data.setReviewedTime(reviewsResponse.getData().get(i).getReviewedTime());
                                data.setReviewContent(reviewsResponse.getData().get(i).getReviewContent());
                                data.setRatingByUser(reviewsResponse.getData().get(i).getRatingByUser());

                                details.add(data);
                            }
                            adapter.notifyDataSetChanged();
                            crossfade();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<UserReviewsResponse> call, Throwable t) {
//                progress.dismiss();
                finish();
                Log.i(TAG, "I'm in UserReviews response failure :(");
            }
        });





    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void crossfade() {

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        mContentView.setAlpha(0f);
        mContentView.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        mContentView.animate()
                .alpha(1f)
                .setDuration(mShortAnimationDuration)
                .setListener(null);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        mLoadingView.animate()
                .alpha(0f)
                .setDuration(mShortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLoadingView.setVisibility(View.GONE);
                    }
                });
    }


}
