package com.beusalons.android.Model.Deal;

import java.io.Serializable;

/**
 * Created by Ajay on 1/6/2017.
 */

public class ServicesCode implements Serializable {

   private String serviceName;
  private Integer   serviceCode;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Integer getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(Integer serviceCode) {
        this.serviceCode = serviceCode;
    }
}
