package com.beusalons.android.Model.UserCart;

import com.beusalons.android.Model.DealsServices.ParlorTypes;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 5/31/2017.
 */

public class UserServices {


    private String name;
    private String description;

    private double price;
    private double menu_price;
    private int save_per;
    private double tax;

    private int quantity;
    private int service_code;
    private int price_id;

    private String service_deal_id;                             //jo appointment ke liye bhejna hai woh isme hai
    private String service_id="";
    private int dealId;                         //bahar wale deals ke liye hai

    private String type;                //service ya deal type ka name

    private String primary_key;

    private String brand_name;
    private String brand_id;

    private String product_name;
    private String product_id;

    private String type_name="";                       //additions ka hai yeh
    private int type_index;          //types ka index
    private int type_additions;                     //types ke andar additions ka price


    //----------------------------------membership ka stuff---------------


    private String membership_id;
    private String memberShipRemaingType;
    private boolean isMembership= false;

    private int remainingTotalQuantity;
    private boolean isRemainingService=false;

    private boolean isMyMembershipFreeService=false;   //use for membership details free service

    //-----------------------------package ka stuff-------------------------


    private List<PackageService> packageServices= new ArrayList<>();

    //--------------------------------Free service ka stuff----------------------------------

    private boolean free_service= false;

    //--------------------------------salon type------------------------------

    private List<ParlorTypes> parlorTypes= new ArrayList<>();


    //-------------------------------------subscription------------
    private int subscriptionId;
    private boolean isSubscription= false;

    public boolean isSubscription() {
        return isSubscription;
    }

    public void setSubscription(boolean subscription) {
        isSubscription = subscription;
    }

    public int getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(int subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public List<ParlorTypes> getParlorTypes() {
        return parlorTypes;
    }

    public void setParlorTypes(List<ParlorTypes> parlorTypes) {
        this.parlorTypes = parlorTypes;
    }

    public String getMemberShipRemaingType() {
        return memberShipRemaingType;
    }

    public void setMemberShipRemaingType(String memberShipRemaingType) {
        this.memberShipRemaingType = memberShipRemaingType;
    }

    public boolean isMyMembershipFreeService() {
        return isMyMembershipFreeService;
    }

    public void setMyMembershipFreeService(boolean myMembershipFreeService) {
        isMyMembershipFreeService = myMembershipFreeService;
    }

    public int getRemainingTotalQuantity() {
        return remainingTotalQuantity;
    }

    public void setRemainingTotalQuantity(int remainingTotalQuantity) {
        this.remainingTotalQuantity = remainingTotalQuantity;
    }

    public boolean isRemainingService() {
        return isRemainingService;
    }

    public void setRemainingService(boolean remainingService) {
        isRemainingService = remainingService;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public boolean isFree_service() {
        return free_service;
    }

    public void setFree_service(boolean free_service) {
        this.free_service = free_service;
    }

    public boolean isMembership() {
        return isMembership;
    }

    public void setMembership(boolean membership) {
        isMembership = membership;
    }

    public int getDealId() {
        return dealId;
    }

    public void setDealId(int dealId) {
        this.dealId = dealId;
    }

    public int getType_additions() {
        return type_additions;
    }

    public void setType_additions(int type_additions) {
        this.type_additions = type_additions;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMembership_id() {
        return membership_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getMenu_price() {
        return menu_price;
    }

    public void setMenu_price(double menu_price) {
        this.menu_price = menu_price;
    }

    public int getSave_per() {
        return save_per;
    }

    public void setSave_per(int save_per) {
        this.save_per = save_per;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getService_code() {
        return service_code;
    }

    public void setService_code(int service_code) {
        this.service_code = service_code;
    }

    public int getPrice_id() {
        return price_id;
    }

    public void setPrice_id(int price_id) {
        this.price_id = price_id;
    }

    public String getService_deal_id() {
        return service_deal_id;
    }

    public void setService_deal_id(String service_deal_id) {
        this.service_deal_id = service_deal_id;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public int getType_index() {
        return type_index;
    }

    public void setType_index(int type_index) {
        this.type_index = type_index;
    }

    public String getPrimary_key() {
        return primary_key;
    }

    public void setPrimary_key(String primary_key) {
        this.primary_key = primary_key;
    }

    public void setMembership_id(String membership_id) {
        this.membership_id = membership_id;
    }

    public List<PackageService> getPackageServices() {
        return packageServices;
    }

    public void setPackageServices(List<PackageService> packageServices) {
        this.packageServices = packageServices;
    }
}
