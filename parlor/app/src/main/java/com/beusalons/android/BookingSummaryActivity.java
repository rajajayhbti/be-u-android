package com.beusalons.android;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Adapter.AdapterBookingSummaryServices;
import com.beusalons.android.Adapter.AdapterRemainingServiceChild;
import com.beusalons.android.Event.RemainingServicesEvent.RemainingServiceIndex;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.Appointments.AppointmentPost;
import com.beusalons.android.Model.Appointments.AppointmentResponse;
import com.beusalons.android.Model.Appointments.AppointmentServicesPost;
import com.beusalons.android.Model.Appointments.CeateAppointMent.PackageService_;
import com.beusalons.android.Model.SalonHome.HomeResponse;
import com.beusalons.android.Model.ServiceAvailable.Service;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Task.UserCartTask;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.michael.easydialog.EasyDialog;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BookingSummaryActivity extends AppCompatActivity {


    private static final String TAG= BookingSummaryActivity.class.getSimpleName();

    public static final String HOME_RESPONSE= "home_response";

    //private AppointmentAdapter adapter;
    private AdapterRemainingServiceChild adapter;
    private static List<UserServices> service_list= new ArrayList<>();

    private UserCart cart;
    private HomeResponse homeResponse=null;
    private View mContentView;
    private View mLoadingView;

    private EditText etxt_comments;
    private ImageView img_animation_remain;

    private boolean is_mem= false;
    private String str_time="";
    private List<UserServices> list= new ArrayList<>();
    private  List<Service> serviceList;
    private TextView txtBookingSummaryStaticDetails,txtViewCurentDiscount;
    private TextView txtText,txtViewValidity,txtGetMore,txtViewPrice,txt_menu_price,txt_save_per,txt_view_more,txt_view_more1;

    private int total_amt= 0, service_count= 0;
    private boolean free_service = false;
    AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;
    UserCart saved_cart= null;
    TextView txt_parlor_name,txtPackageDiscount,txt_buy_membership;
    RecyclerView recyclerView;
    private RecyclerView recycler_view_remaining_services;
    private  LinearLayout ll_remaining_sevice_cart, linearLayout_name;

    boolean isSelectedRemaining=false;
    private boolean firstTime=true;
    private String date_= null;
    private boolean membershipBuying=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_summary);
        setToolBar();

        LinearLayout linear_detail= (LinearLayout)findViewById(R.id.linear_detail);

        TextView txt_timeDate = (TextView) findViewById(R.id.txt_booking_summary_parlor_booking_time);
        linearLayout_name=(LinearLayout)findViewById(R.id.linearLayout_name);
//        txtBookingSummaryStaticDetails=(TextView)findViewById(R.id.txt_booking_summary_static_details);
 //       txtViewCurentDiscount=(TextView)findViewById(R.id.txtViewCurentDiscount);
        logger = AppEventsLogger.newLogger(BookingSummaryActivity.this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(BookingSummaryActivity.this);
        serviceList=new ArrayList<>();
        ll_remaining_sevice_cart=(LinearLayout) findViewById(R.id.ll_remaining_sevice_cart);

        img_animation_remain=(ImageView)findViewById(R.id.img_animation_remain);

        recycler_view_remaining_services= (RecyclerView)findViewById(R.id.recycler_view_remaining_services);

        txtPackageDiscount=(TextView)findViewById(R.id.txt_package_discount);

        txt_view_more1=(TextView)findViewById(R.id.txt_view_more);
        txtGetMore=(TextView)findViewById(R.id.txtViewGetMore);
        txtText=(TextView)findViewById(R.id.txtViewtext);
        txt_view_more=(TextView)findViewById(R.id.txt_View_nxt_validity);
        txtViewValidity=(TextView)findViewById(R.id.txtViewValidity);
        Bundle bundle= getIntent().getExtras();
        Log.e("activity log","test");

        txtViewPrice=(TextView)findViewById(R.id.txtViewPrice);
        txt_menu_price=(TextView)findViewById(R.id.txt_menu_price);
        txt_save_per=(TextView)findViewById(R.id.txt_save_per);


        if(bundle!=null && bundle.containsKey("user_cart")){

            linear_detail.setVisibility(View.VISIBLE);
            txt_timeDate.setVisibility(View.VISIBLE);
            is_mem= false;
            cart= new Gson().fromJson(bundle.getString("user_cart", null), UserCart.class);
            homeResponse=new Gson().fromJson(bundle.getString(HOME_RESPONSE,null),HomeResponse.class);
            if (homeResponse!=null){
                setRemainingServiceData();         //use for remaining service
                date_= cart.getDate_time();
                is_mem=true;
            }else{
                linear_detail.setVisibility(View.GONE);
                txt_timeDate.setVisibility(View.GONE);
            }


        }else if (bundle!=null && bundle.containsKey("expresss_checkout")){
            linear_detail.setVisibility(View.VISIBLE);
            txt_timeDate.setVisibility(View.VISIBLE);
            homeResponse=new Gson().fromJson(bundle.getString(HOME_RESPONSE,null),HomeResponse.class);

                //use for remaining service
            if (homeResponse!=null){
                setRemainingServiceData();         //use for remaining service
            }else{
                linear_detail.setVisibility(View.GONE);
                txt_timeDate.setVisibility(View.GONE);
                is_mem=true;
            }

        }

        else{
            linear_detail.setVisibility(View.GONE);
            txt_timeDate.setVisibility(View.GONE);
            is_mem= true;
        }

        mContentView= findViewById(R.id.relative_booking_summary);
        mLoadingView= findViewById(R.id.loading_spinner_booking_summary);
        mLoadingView.setVisibility(View.GONE);

         txt_parlor_name   = (TextView) findViewById(R.id.txt_booking_summary_parlor_name);
        TextView txt_rating = (TextView) findViewById(R.id.txt_booking_summary_parlor_rating);
        TextView txt_address1 = (TextView) findViewById(R.id.txt_booking_summary_parlor_address_1);
        TextView txt_address2 = (TextView) findViewById(R.id.txt_booking_summary_parlor_address_2);
        etxt_comments= (EditText)findViewById(R.id.etxt_comment);



        //setting the services recycler view
        recyclerView  = (RecyclerView) findViewById(R.id.recycler_);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(BookingSummaryActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        //adapter= new AppointmentAdapter(this, service_list, cart);


        if(cart!=null){

            txt_parlor_name.setText(cart.getParlorName());
            txt_rating.setText(String.valueOf(cart.getRating()));
            txt_address1.setText(cart.getAddress1());
            txt_address2.setText(cart.getAddress2());
            txt_timeDate.setText(formatDateTime(cart.getDate_time()));           //format the json date-time
            Log.i("mayakahahai", "cart not equal to null pe hoon");
        }


        TextView txt_cart= (TextView)findViewById(R.id.txt_cart);
        txt_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*logCartButtonClickedEvent();
                logCartButtonClickedFireBaseEvent();
                UserCartFragment fragment= new UserCartFragment();
                Bundle bundle= new Bundle();
                bundle.putBoolean("has_data", true);
                fragment.setArguments(bundle);
                fragment.show(getSupportFragmentManager(), "user_cart");*/

                onBackPressed();
            }
        });

        TextView txt_proceed= (TextView)findViewById(R.id.txt_proceed);
        txt_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cartData();                     // card is use
                logConfirmBookingclickedEvent();
                logConfirmBookingclickedFireBaseEvent();
                if (saved_cart!=null &&saved_cart.getParlorName()!=null)
                logInitiatedCheckoutEvent(saved_cart.getParlorName(),"INR",total_amt);

                if(service_count==0){

                    AlertDialog.Builder builder = new AlertDialog.Builder(BookingSummaryActivity.this);
                    builder.setTitle("Cart Empty!");
                    builder.setMessage("Please add a service.");
                    builder.setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    }).show();
                }else if(service_count>0 && free_service){

                    if(total_amt >= BeuSalonsSharedPrefrence.getFreeHeircutBar()){

                        mContentView.setVisibility(View.GONE);
                   //     mLoadingView.setVisibility(View.VISIBLE);
                        createAppointment();
                    }else{

                        AlertDialog.Builder builder = new AlertDialog.Builder(BookingSummaryActivity.this);
                        builder.setTitle("Free Service In Cart!");
                        builder.setMessage("Minimum Bill Value To Avail Free Hair Cut Is ₹"+
                        BeuSalonsSharedPrefrence.getFreeHeircutBar()+".");
                        builder.setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        }).show();
                    }

                }else if(service_count>0 && total_amt<=0 && !saved_cart.getServicesList().get(0).isRemainingService()&&!saved_cart.getServicesList().get(0).getType().equalsIgnoreCase("membership")){

                    AlertDialog.Builder builder = new AlertDialog.Builder(BookingSummaryActivity.this);
                    builder.setTitle("Invalid Service!");
                    builder.setMessage("Seems Like The Selected Service Amount Is Not Valid. Please Remove The Service & " +
                            "Try AGAIN");
                    builder.setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    }).show();

                }else if(service_count>0&&total_amt<=0||saved_cart.getServicesList().get(0).isRemainingService()){
                    mContentView.setVisibility(View.GONE);
             //       mLoadingView.setVisibility(View.VISIBLE);
                    createAppointment();


                }
                else if(service_count>0 && total_amt>0){         //agar service hai aur amt 0 se upar hai

                    mContentView.setVisibility(View.GONE);
             //       mLoadingView.setVisibility(View.VISIBLE);
                    createAppointment();
                }else{
                    Log.i("yehkaunsa", "bsdk yeh kaun sa case hai");
                }



            }
        });



//        txtBookingSummaryStaticDetails.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//              /*  BookingSummaryDialog fragment= new BookingSummaryDialog();
//                Bundle bundle= new Bundle();
//                bundle.putString("membership", new Gson().toJson(homeResponse, HomeResponse.class));
//                fragment.setArguments(bundle);
//                FragmentManager fm = getSupportFragmentManager();
//                fragment.show(fm, TAG);*/
//            }
//        });

        new Thread(new Runnable() {
            @Override
            public void run() {
             // getCardData();         //populate card data with remaining service and normal service

                //cartData();             //populate cart data
            }
        }).start();
    }




     private void setRemainingServiceData(){
         if (homeResponse.getData()!=null && homeResponse.getData().getServicesAvailable()!=null
                 && homeResponse.getData().getServicesAvailable().size()>0){

             ll_remaining_sevice_cart.setVisibility(View.VISIBLE);
             if (serviceList.size()>0){
                 serviceList.clear();
             }

             serviceList.addAll(homeResponse.getData().getServicesAvailable().get(0).getServices());

             if (serviceList.size()>0){
                 ll_remaining_sevice_cart.setVisibility(View.VISIBLE);

             }else  ll_remaining_sevice_cart.setVisibility(View. GONE);

             RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(BookingSummaryActivity.this);
             recycler_view_remaining_services.setLayoutManager(mLayoutManager);
             adapter= new AdapterRemainingServiceChild(BookingSummaryActivity.this,
                     homeResponse.getData().getServicesAvailable().get(0).getServices() , 0,true);
             recycler_view_remaining_services.setAdapter(adapter);
             if (isSelectedRemaining){
                 recycler_view_remaining_services.setVisibility(View.VISIBLE);
                 img_animation_remain.setImageResource(R.drawable.ic_arrow_down);
                 recycler_view_remaining_services.setAdapter(adapter);
             }else{
                 recycler_view_remaining_services.setVisibility(View.GONE);
                 img_animation_remain.setImageResource(R.drawable.ic_right);

             }

             ll_remaining_sevice_cart.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     if (isSelectedRemaining){
                         img_animation_remain.setImageResource(R.drawable.ic_arrow_down);
                         recycler_view_remaining_services.setVisibility(View.VISIBLE);

                         isSelectedRemaining=false;
                     }else{
                         isSelectedRemaining=true;
                         img_animation_remain.setImageResource(R.drawable.ic_right);

                         recycler_view_remaining_services.setVisibility(View.GONE);
                     }

                 }
             });

             adapter.notifyDataSetChanged();
         }
          else ll_remaining_sevice_cart.setVisibility(View.GONE);
     }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(RemainingServiceIndex event) {

        Log.i("remainingservice", "i'm in the event");
        if(this!=null)
            Toast.makeText(this, "Service Added To Cart!", Toast.LENGTH_SHORT).show();

        //service kara
        UserServices userServices=new UserServices();

        userServices.setName(homeResponse.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position()).getName());


        userServices.setService_code(homeResponse.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position()).getServiceCode());
        userServices.setPrice_id(homeResponse.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position()).getServiceCode());



        if (homeResponse.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position()).getType()!=null)
            userServices.setMemberShipRemaingType(homeResponse.getData().getServicesAvailable().get(0).getServices()
                    .get(event.getService_position()).getType());


        String brand_id= homeResponse.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position()).getBrandId()==null?"":homeResponse.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position()).getBrandId();
        String product_id= homeResponse.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position())==null?"":homeResponse.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position()).getProductId();
        userServices.setBrand_id(brand_id);
        userServices.setProduct_id(product_id);
        userServices.setRemainingTotalQuantity(homeResponse.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position()).getQuantity());
        String primary_key= homeResponse.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position()).getServiceCode()+brand_id+product_id;
        userServices.setPrimary_key(primary_key);


        if (homeResponse.getData().getServicesAvailable().get(0).getServices().get(event.getService_position()).getType()!=null &&
                homeResponse.getData().getServicesAvailable().get(0).getServices().get(event.getService_position()).getType()
                        .equals("membership")){
            userServices.setMyMembershipFreeService(true);
            userServices.setType("membership");

        }
        else {

            if (homeResponse.getData().getServicesAvailable().get(0).getServices()
                    .get(event.getService_position()).getPrice()!=0)
                userServices.setPrice(homeResponse.getData().getServicesAvailable().get(0).getServices()
                        .get(event.getService_position()).getPrice());


            if (homeResponse.getData().getServicesAvailable().get(0).getServices()
                    .get(event.getService_position()).getActualPrice()!=null)
                userServices.setMenu_price(homeResponse.getData().getServicesAvailable().get(0).getServices()
                        .get(event.getService_position()).getActualPrice());
            userServices.setRemainingService(true);
            userServices.setType("serviceAvailable");

        }
            new Thread(new UserCartTask(this,cart ,userServices, false, false)).start();

        try {
            updateAddData();
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    private void updateAddData(){
        Thread  thread = new Thread(){
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(200);

                      runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                getCardData();
                            }
                        });


                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                /*Intent mainActivity = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(mainActivity);*/
            };
        };
        thread.start();
    }


    private void getCardData(){
        try {

            DB snappyDB= DBFactory.open(this);
            saved_cart= null;               //db mai jo saved cart hai
            if(snappyDB.exists(AppConstant.USER_CART_DB)){

                saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
                snappyDB.close();
                if(saved_cart.getServicesList().size()>0){


                    txt_parlor_name.setText(saved_cart.getParlorName());
                    list.clear();
                    list= saved_cart.getServicesList();
                    AdapterBookingSummaryServices adapter = new AdapterBookingSummaryServices(this, saved_cart, list,true);
                    recyclerView.setAdapter(adapter);
                    if (homeResponse!=null && homeResponse.getData().getServicesAvailable()!=null && homeResponse.getData().getServicesAvailable().size()>0){

                        updateQuantity(list,homeResponse.getData().getServicesAvailable().get(0).getServices());

                    }else ll_remaining_sevice_cart.setVisibility(View.GONE);
                    setData(list);
                    adapter.notifyDataSetChanged();

                }else{
                    setData(list);
                    txt_parlor_name.setText(saved_cart.getParlorName());
                }
            }else{

                snappyDB.close();
                setData(list);
            }
        }catch (Exception e){

            e.printStackTrace();
        }




        if (firstTime){
            Thread  thread = new Thread(){
                @Override
                public void run() {
                    try {
                        synchronized (this) {
                            wait(500);

                           runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    setToolTip();
                                    firstTime=false;
                                }
                            });

                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                /*Intent mainActivity = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(mainActivity);*/
                };
            };
            thread.start();

        }
    }
    private void setToolTip(){
        View view = getLayoutInflater().inflate(R.layout.tooltip_cart, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT );
        view.setLayoutParams(params);
        new EasyDialog(BookingSummaryActivity.this)
                // .setLayoutResourceId(R.layout.layout_tip_content_horizontal)//layout resource id
                .setLayout(view)
                .setBackgroundColor(getResources().getColor(R.color.tooltip_bg))
                // .setLocation(new location[])//point in screen
                .setLocationByAttachedView(txt_view_more1)
                .setGravity(EasyDialog.GRAVITY_BOTTOM)
                .setAnimationTranslationShow(EasyDialog.DIRECTION_X, 1000, -600, 100, -50, 50, 0)
                .setAnimationAlphaShow(100, 0.3f, 1.0f)
                .setAnimationTranslationDismiss(EasyDialog.DIRECTION_X, 500, -50, 800)
                .setAnimationAlphaDismiss(200, 1.0f, 0.0f)
                .setTouchOutsideDismiss(true)
                .setOutsideColor(getResources().getColor(android.R.color.transparent))
                .setMatchParent(true)
                .setMarginLeftAndRight(24, 24)
                .show();
    }

    private void updateQuantity(List<UserServices> list ,List<Service> serviceAvailableData){
        for (int i=0;i<list.size();i++){
            for (int j=0;j<serviceAvailableData.size();j++){
                if (list.get(i).getService_code()==serviceAvailableData.get(j).getServiceCode() ){
                    serviceAvailableData.remove(j);
                }
            }
        }
        if (list.size()==0){
            homeResponse.getData().getServicesAvailable().get(0).setServices(serviceList);

        }
        Log.e("addremain",""+homeResponse.getData().getServicesAvailable().get(0).getServices().size());

        if (homeResponse.getData().getServicesAvailable().get(0).getServices().size()==0){
            ll_remaining_sevice_cart.setVisibility(View.GONE);
        }else              ll_remaining_sevice_cart.setVisibility(View.VISIBLE);

        if (adapter!=null)
            adapter.notifyDataSetChanged();
    }
    private void addServiceData(List<UserServices> list,List<Service> serviceList){
        ArrayList<Integer> serviceCode=new ArrayList<>();
        if (serviceCode.size()>0){
            serviceCode.clear();
        }
        for (int i=0;i<list.size();i++){
            serviceCode.add(list.get(i).getService_code());
        }
        List<Service> service=new ArrayList<>();
        for(int j=0;j<serviceList.size();j++){
            if (!serviceCode.contains(serviceList.get(j).getServiceCode())){

                service.add(serviceList.get(j));
            }
        }

        homeResponse.getData().getServicesAvailable().get(0).setServices(service);
        adapter = new AdapterRemainingServiceChild(BookingSummaryActivity.this,
                homeResponse.getData().getServicesAvailable().get(0).getServices(),0,true);
        recycler_view_remaining_services.setAdapter(adapter);
        Log.e("listSize",""+homeResponse.getData().getServicesAvailable().get(0).getServices().size());

        if (homeResponse.getData().getServicesAvailable().get(0).getServices().size()==0){
            ll_remaining_sevice_cart.setVisibility(View.GONE);
        }else              ll_remaining_sevice_cart.setVisibility(View.VISIBLE);

        if (adapter!=null)
            adapter.notifyDataSetChanged();

    }



    public void cartData() {

        service_list.clear();

        service_count = 0;
        total_amt = 0;
        free_service = false;

        UserCart saved_cart;
        DB snappyDB = null;
        try {
            snappyDB = DBFactory.open(getApplicationContext());
            if (snappyDB.exists(AppConstant.USER_CART_DB)) {

                saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);

                //agar other type ka hai toh convert to service type for booking...
                if (saved_cart != null &&
                        saved_cart.getCartType()!=null &&
                        saved_cart.getCartType().equalsIgnoreCase(AppConstant.OTHER_TYPE)) {
                        cart=new UserCart();
                        cart.getServicesList().clear();
                        cart.setServicesList(saved_cart.getServicesList());      //user cart mai service dala

                    // use for discount changes ke liye methods call kiya h

                    saved_cart = null;
                    snappyDB.put(AppConstant.USER_CART_DB, cart);
                    snappyDB.close();
                } else {

                    if (cart != null)
                        cart.getServicesList().clear();

                    cart = saved_cart;
                    snappyDB.close();
                }

                for (int i = 0; i < cart.getServicesList().size(); i++) {

                    UserServices model = new UserServices();

                    model.setName(cart.getServicesList().get(i).getName());

                    model.setService_id(cart.getServicesList().get(i).getService_id());
                    model.setService_deal_id(cart.getServicesList().get(i).getService_deal_id());

                    model.setPrice(cart.getServicesList().get(i).isFree_service() ? 0 :
                            cart.getServicesList().get(i).getPrice() *
                                    cart.getServicesList().get(i).getQuantity());
                    model.setPrimary_key(cart.getServicesList().get(i).getPrimary_key());
                    model.setQuantity(cart.getServicesList().get(i).getQuantity());
                    model.setDescription(cart.getServicesList().get(i).getDescription());
                    model.setType(cart.getServicesList().get(i).getType());

                    service_list.add(model);

                    service_count++;
                    total_amt += cart.getServicesList().get(i).isFree_service() || cart.getServicesList().get(i).isRemainingService() ? 0 :
                            cart.getServicesList().get(i).getPrice() * cart.getServicesList().get(i).getQuantity();
                    if (cart.getServicesList().get(i).isFree_service())
                        free_service = true;
                }


            } else {

                snappyDB.close();
            }


        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (cart != null && cart.getServicesList().size() > 0) {
                    setData(cart.getServicesList());
                } else {
//                    txtViewCurentDiscount.setVisibility(View.GONE);
//                    txtBookingSummaryStaticDetails.setVisibility(View.GONE);
                }
                //adapter.updateAdapter(service_list, cart);
                if (adapter!=null)
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void setEmptyCart(List<UserServices> list){
        if (list.size()==0){
            linearLayout_name.setVisibility(View.GONE);

        }
        else{
            linearLayout_name.setVisibility(View.VISIBLE);

        }

    }
    private void setData(List<UserServices> list){
        double totalPrice=0,menuTotalPrice=0,memberShipPrice=0;
        for (int i=0;i<list.size();i++){
            int q=list.get(i).getQuantity();
            double price=list.get(i).getPrice()*q;
            double menuPrice=list.get(i).getMenu_price()*q;
            if (list.get(i).isFree_service() || list.get(i).isRemainingService() || list.get(i).isMembership()){
                totalPrice+=0;
                menuTotalPrice+=0;

                if (list.get(i).isMembership()){
                     memberShipPrice+=price;
                }
            }else{

                totalPrice+= price;
                if (menuPrice==0){
                    menuTotalPrice+=price;

                }else{
                    menuTotalPrice+=menuPrice;
                }
            }
        }
        linearLayout_name.setVisibility(View.VISIBLE);
        txt_menu_price.setText(AppConstant.CURRENCY+(int)(menuTotalPrice+memberShipPrice));
        txt_menu_price.setPaintFlags(txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        txt_save_per.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        txt_save_per.setBackgroundResource(R.drawable.discount_seletor);
        if (BeuSalonsSharedPrefrence.getDiscount(this)!=null) {

            for (int i = 0; i < BeuSalonsSharedPrefrence.getDiscount(this).size(); i++) {

                if (totalPrice >= BeuSalonsSharedPrefrence.getDiscount(this).get(i).getMin() &&
                        totalPrice < BeuSalonsSharedPrefrence.getDiscount(this).get(i).getMax()) {
                    txtText.setVisibility(View.VISIBLE);
                    txtText.setText(BeuSalonsSharedPrefrence.getDiscount(this).get(i).getText());
                    txtPackageDiscount.setVisibility(View.VISIBLE);
                    txtPackageDiscount.setText(BeuSalonsSharedPrefrence.getDiscount(this).get(i).getDiscountPercent()+"% Package Discount");
                    double discountPrice = 0;
                    discountPrice = (totalPrice * BeuSalonsSharedPrefrence.getDiscount(this).get(i).getDiscountPercent()) / 100;

                    txtViewPrice.setText(AppConstant.CURRENCY +(int) (totalPrice - discountPrice+memberShipPrice));
                    int pos = i + 1;
                    if (BeuSalonsSharedPrefrence.getDiscount(this).size() > pos) {
                        txtGetMore.setVisibility(View.VISIBLE);
                        String val=BeuSalonsSharedPrefrence.getDiscount(this).get(i + 1).getValidity();

                        txt_view_more.setText("Validity: "+fromHtml(val));
                        double price = (BeuSalonsSharedPrefrence.getDiscount(this).get(i + 1).getMin() - totalPrice);
                        txtGetMore.setText("Upgrade to " + BeuSalonsSharedPrefrence.getDiscount(this).get(i + 1).getDiscountPercent() + "% Package Discount On Adding Services Worth Rs." + AppConstant.CURRENCY + (int)price);

                    } else {
                        txtGetMore.setVisibility(View.GONE);
                        txt_view_more.setVisibility(View.GONE);
                    }
                    if (menuTotalPrice!=0){
                        double discountPercentage=(1-((totalPrice-discountPrice)/menuTotalPrice))*100;
                        txt_save_per.setText(AppConstant.SAVE +" "+ (int)Math.round(discountPercentage)+"%");

                    }else
                        {
                            double discountPercentage=(1-((totalPrice-0)/menuTotalPrice))*100;
                            txt_save_per.setText(AppConstant.SAVE +" "+ (int)Math.round(discountPercentage)+"%");

                        }
                    txtViewValidity.setVisibility(View.VISIBLE);
                    txtViewValidity.setText("Validity: " + BeuSalonsSharedPrefrence.getDiscount(this).get(i).getValidity());
                    updateDiscountPercentage(i, totalPrice,menuTotalPrice,memberShipPrice);
                } else if (totalPrice < BeuSalonsSharedPrefrence.getDiscount(this).get(0).getMin()) {
                    txtViewValidity.setVisibility(View.VISIBLE);
                    txtText.setVisibility(View.VISIBLE);
                    txtPackageDiscount.setVisibility(View.GONE);
                    txtText.setText(BeuSalonsSharedPrefrence.getDiscount(this).get(0).getText());
                    updateDiscountPercentage(0, totalPrice,menuTotalPrice,memberShipPrice);
                }

            }

        }

        setEmptyCart(list);
    }
    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }


    private void updateDiscountPercentage(int i,double totalPrice,double menuTotalPrice,double membershipPrice){
        int q=0;
        for (int j=0;j<list.size();j++){
            q+=list.get(j).getQuantity();
        }
        if ((q<=BeuSalonsSharedPrefrence.getDiscount(this).get(i).getMinNoOfService())
                && (totalPrice>=BeuSalonsSharedPrefrence.getDiscount(this).get(0).getExcludeNoOfServiceAmount())){
            txtText.setVisibility(View.VISIBLE);
            txtViewValidity.setVisibility(View.VISIBLE);
            double discountPrice = 0;
            discountPrice = (totalPrice * BeuSalonsSharedPrefrence.getDiscount(this).get(i).getDiscountPercent()) / 100;
            txtViewPrice.setText(AppConstant.CURRENCY + (int)(totalPrice - discountPrice+membershipPrice));
            if (menuTotalPrice!=0){
                double discountPercentage=(1-((totalPrice-discountPrice)/menuTotalPrice))*100;
                txt_save_per.setText(AppConstant.SAVE +" "+ (int)Math.round(discountPercentage)+"%");

            }else
            {
                double discountPercentage=(1-((totalPrice-0)/menuTotalPrice))*100;
                txt_save_per.setText(AppConstant.SAVE +" "+ (int)Math.round(discountPercentage)+"%");

            }
           // txt_save_per.setText(AppConstant.SAVE + " Extra " +BeuSalonsSharedPrefrence.getDiscount(this).get(i).getDiscountPercent() + "%");
        }
        else if (q>=BeuSalonsSharedPrefrence.getDiscount(this).get(i).getMinNoOfService()){
            if (totalPrice<BeuSalonsSharedPrefrence.getDiscount(this).get(i).getMin()){
                txtText.setVisibility(View.GONE);
                txtViewPrice.setText(AppConstant.CURRENCY+(int)(totalPrice+membershipPrice));
                txtViewValidity.setVisibility(View.GONE);
                //txt_save_per.setText(AppConstant.SAVE + " 0"+"%");
                double discountPercentage=(1-((totalPrice-0)/menuTotalPrice))*100;
                txt_save_per.setText(AppConstant.SAVE +" "+ (int)Math.round(discountPercentage)+"%");

                double  price =BeuSalonsSharedPrefrence.getDiscount(this).get(i).getMin()-totalPrice;
                txtGetMore.setText("Upgrade to "+BeuSalonsSharedPrefrence.getDiscount(this).get(i).getDiscountPercent()+"% Package Discount On Adding Services Worth Rs."+AppConstant.CURRENCY+(int)price);
                String val=BeuSalonsSharedPrefrence.getDiscount(this).get(i ).getValidity();

                txt_view_more.setText("Validity: "+fromHtml(val));

            }
        }
        else{
            txtViewValidity.setText("Validity: " + BeuSalonsSharedPrefrence.getDiscount(this).get(0).getValidity());
            txtGetMore.setVisibility(View.VISIBLE);
            if (totalPrice>=BeuSalonsSharedPrefrence.getDiscount(this).get(i).getMin()){
                if (q<BeuSalonsSharedPrefrence.getDiscount( this).get(i).getMinNoOfService()){
                    String val=BeuSalonsSharedPrefrence.getDiscount(this).get(i ).getValidity();

                    txt_view_more.setText("Validity: "+fromHtml(val));
                    txtGetMore.setText("Upgrade to "+BeuSalonsSharedPrefrence.getDiscount(BookingSummaryActivity.this).get(i).getDiscountPercent()+"% Package Discount On Adding "+(BeuSalonsSharedPrefrence.getDiscount(this).get(i).getMinNoOfService()-q)+" More Service ");
                    txtViewPrice.setText(AppConstant.CURRENCY+(int)(totalPrice+membershipPrice));
                    double discountPercentage=(1-((totalPrice-0)/menuTotalPrice))*100;
                    txt_save_per.setText(AppConstant.SAVE +" "+ (int)Math.round(discountPercentage)+"%");

                   // txt_save_per.setText(AppConstant.SAVE + " 0"  + "%");
                    txtText.setVisibility(View.GONE);
                    txtViewValidity.setVisibility(View.GONE);

                }



            }
            else {
                if (q<BeuSalonsSharedPrefrence.getDiscount(this).get(i).getMinNoOfService()){
                    String val=BeuSalonsSharedPrefrence.getDiscount(this).get(i ).getValidity();

                    txt_view_more.setText("Validity: "+fromHtml(val));

                    double  price =BeuSalonsSharedPrefrence.getDiscount(this).get(i).getMin()-totalPrice;
                    txtGetMore.setText("Upgrade to "+BeuSalonsSharedPrefrence.getDiscount(this).get(i).getDiscountPercent()+"% Package Discount On Adding "+(BeuSalonsSharedPrefrence.getDiscount(this).get(i).getMinNoOfService()-q)+" More Service ");
                    txtViewPrice.setText(AppConstant.CURRENCY+(int)(totalPrice+membershipPrice));
                    double discountPercentage=(1-((totalPrice-0)/menuTotalPrice))*100;
                    txt_save_per.setText(AppConstant.SAVE +" "+ (int)Math.round(discountPercentage)+"%");

                 //   txt_save_per.setText(AppConstant.SAVE + " 0" + "%");
                    txtText.setVisibility(View.GONE);
                    txtViewValidity.setVisibility(View.GONE);


                }
                else if (totalPrice<BeuSalonsSharedPrefrence.getDiscount(this).get(i).getMin()){
                    txtText.setVisibility(View.GONE);
                    txtViewValidity.setVisibility(View.GONE);
                    txtViewPrice.setText(AppConstant.CURRENCY+(int)(totalPrice+membershipPrice));
                    double discountPercentage=(1-((totalPrice-0)/menuTotalPrice))*100;
                    txt_save_per.setText(AppConstant.SAVE +" "+ (int)Math.round(discountPercentage)+"%");

                    //txt_save_per.setText(AppConstant.SAVE + " 0" + "%");
                    String val=BeuSalonsSharedPrefrence.getDiscount(this).get(i ).getValidity();
                    txt_view_more.setText("Validity: "+fromHtml(val));
                    double  price =BeuSalonsSharedPrefrence.getDiscount(this).get(i).getMin()-totalPrice;
                    txtGetMore.setText("Upgrade to "+BeuSalonsSharedPrefrence.getDiscount(this).get(i).getDiscountPercent()+"% Package Discount On Adding Services Worth Rs."+AppConstant.CURRENCY+(int)price);
                }

            }

        }

        if (totalPrice==0){

            txtViewValidity.setText("Validity: " + BeuSalonsSharedPrefrence.getDiscount(this).get(0).getValidity());
            txtText.setVisibility(View.GONE);
            txtViewValidity.setVisibility(View.GONE);

            txtGetMore.setVisibility(View.GONE);
            txt_view_more.setVisibility(View.GONE);

            linearLayout_name.setVisibility(View.GONE);
            txtViewPrice.setText(AppConstant.CURRENCY+(totalPrice+membershipPrice));
            txt_save_per.setText(AppConstant.SAVE + " 0" + "%");
        }
    }








    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final List<UserServices> event) {
        /* Do something */
       updateAddData();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                addServiceData(event,serviceList);

            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        logBookingSummaryBackButtonEvent();
        logBookingSummaryBackButtonFireBaseEvent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCardData();               //update card here
        txt_view_more1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setToolTip();
            }
        });
        mContentView.setVisibility(View.VISIBLE);
        mLoadingView.setVisibility(View.INVISIBLE);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logCartButtonClickedEvent () {

        Log.e("CartButtonClicked","fine");
        logger.logEvent(AppConstant.CartButtonClicked);
    }
    public void logCartButtonClickedFireBaseEvent () {

        Log.e("CartButnClickfirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.CartButtonClicked,bundle);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logConfirmBookingclickedEvent () {
        Log.e("ConfirmBookingClicked","fine");

        logger.logEvent(AppConstant.ConfirmBookingClicked);
    }

    public void logConfirmBookingclickedFireBaseEvent () {
        Log.e("ConfirmBClickedfirebase","fine");
            Bundle bundle=new Bundle();
            mFirebaseAnalytics.logEvent(AppConstant.ConfirmBookingClicked,bundle);
    }
    public void logBookingSummaryBackButtonEvent () {
        Log.e("BookingSummaryBkButton","fine");

        logger.logEvent(AppConstant.BookingSummaryBackButton);
    }

    public void logBookingSummaryBackButtonFireBaseEvent () {
        Log.e("BookinbkButtonfirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.BookingSummaryBackButton,bundle);
    }



    public void createAppointment(){

        final AppointmentPost appointmentPost= new AppointmentPost();

        appointmentPost.setUserId(BeuSalonsSharedPrefrence.getUserId());
        appointmentPost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        appointmentPost.setLatitude(BeuSalonsSharedPrefrence.getLatitude());
        appointmentPost.setLongitude(BeuSalonsSharedPrefrence.getLongitude());

        if(date_==null){      //membership mai get date time null hai

            Calendar calendar= Calendar.getInstance();
            Date date= calendar.getTime();
            DateFormat date_format= new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss");
            appointmentPost.setDatetime(date_format.format(date)+" GMT+0530 (India Standard Time)");
            Log.i("yehkayahai", "i'm in membership");
        }else{

            appointmentPost.setDatetime(date_+" GMT+0530 (India Standard Time)");
            Log.i("yehkayahai", "i'm not membership");
        }

        appointmentPost.setParlorId(cart.getParlorId());
        appointmentPost.setMode(1);                 //mode 1 for android, 2 for ios
        appointmentPost.setPaymentMethod(5);
        appointmentPost.setComment(etxt_comments.getText().toString());

        //initially setting membership value     -- assuming no membership in cart



//            appointmentPost.setBuyMembershipId(null);
         //   appointmentPost.setUseMembershipCredits(0);


        boolean isMembership= false;
        List<AppointmentServicesPost> servicesPosts= new ArrayList<>();
        for(int i=0; i<cart.getServicesList().size();i++){

            AppointmentServicesPost services_= new AppointmentServicesPost();

            List<PackageService_> list_= new ArrayList<>();
            /*if(cart.getServicesList().get(i).getPackageServices().size()>0){

                for(int j=0; j<cart.getServicesList().get(i).getPackageServices().size();j++){

                    PackageService_ service= new PackageService_();
                    service.setBrandId(cart.getServicesList().get(i).getPackageServices().get(j).getBrand_id());
                    service.setProductId(cart.getServicesList().get(i).getPackageServices().get(j).getProduct_id());
                    service.setServiceId(cart.getServicesList().get(i).getPackageServices().get(j).getService_id());
                    service.setServiceCode(cart.getServicesList().get(i).getPackageServices().get(j).getService_code());

                    list_.add(service);
                }
                services_.setServices(list_);

                services_.setServiceId(cart.getServicesList().get(i).getService_deal_id());  //service ya deal type hoga
                services_.setType(cart.getServicesList().get(i).getType());
                services_.setQuantity(cart.getServicesList().get(i).getQuantity());
                services_.setFrequencyUsed(false);

                servicesPosts.add(services_);

            } else*/
            if(cart.getServicesList().get(i).isMembership()){          //membership hai toh

                isMembership= true;
                appointmentPost.setBuyMembershipId(cart.getServicesList().get(i).getMembership_id());
                appointmentPost.setUseMembershipCredits(1);

            }else if(cart.getServicesList().get(i).isSubscription()){

                appointmentPost.setSubscriptionId(cart.getServicesList().get(i).getSubscriptionId());
                appointmentPost.setUseSubscriptionCredits(true);
            }
            else {                     //baki sab ke liye yeh hai

                Log.i("priceid", "value of price id:" + cart.getServicesList().get(i).getPrice_id() +
                        " "+ cart.getServicesList().get(i).getService_code());

                services_.setServiceCode(cart.getServicesList().get(i).getService_code());
                services_.setCode(cart.getServicesList().get(i).getPrice_id());

                services_.setBrandId(cart.getServicesList().get(i).getBrand_id());
                services_.setProductId(cart.getServicesList().get(i).getProduct_id());
                services_.setTypeIndex(cart.getServicesList().get(i).getType_index());
                services_.setAddition(cart.getServicesList().get(i).getType_additions());

                services_.setServiceId(cart.getServicesList().get(i).getService_deal_id());

                //deal, service, dealprice wagera khatam ab
                if(cart.getServicesList().get(i).isFree_service()){
                    services_.setType(cart.getServicesList().get(i).getType());
                }else if(cart.getServicesList().get(i).isRemainingService()){
                    services_.setType("serviceAvailable");
                }else if(cart.getServicesList().get(i).isMyMembershipFreeService()){
                    services_.setType("membership");
                }else if(cart.getServicesList().get(i).isSubscription())
                    services_.setType("subscription");
                else
                    services_.setType("newPackage");

                services_.setQuantity(cart.getServicesList().get(i).getQuantity());
                services_.setFrequencyUsed(cart.getServicesList().get(i).isFree_service());

                servicesPosts.add(services_);
            }
        }

        if(BeuSalonsSharedPrefrence.getMembershipPoints()>0 && !isMembership){

            appointmentPost.setUseMembershipCredits(1);
        }else {
            appointmentPost.setUseMembershipCredits(0);
        }


        if(BeuSalonsSharedPrefrence.getMyLoyaltyPoints()>0){
            appointmentPost.setUseLoyalityPoints(true);
        }

        appointmentPost.setServices(servicesPosts);



        Intent intent= new Intent(BookingSummaryActivity.this, BillSummaryActivity.class);
        intent.putExtra("appointment_post", new Gson().toJson(appointmentPost));
        intent.putExtra("membership",new Gson().toJson(homeResponse, HomeResponse.class));
        startActivity(intent);


      /*  Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<AppointmentResponse> call= apiInterface.createAppts(appointmentPost);
        call.enqueue(new Callback<AppointmentResponse>() {
            @Override
            public void onResponse(Call<AppointmentResponse> call, SelectEmployeeResponse<AppointmentResponse> response) {

                Log.i(TAG, "i'm in on response susccesdfjalsdlfjsdal");

                if(response.isSuccessful()){

                    if(response.body().getSuccess()){

                        Log.i(TAG, "on success..appointmentId: "+response.body().getData().getAppointmentId());

                        Intent intent= new Intent(BookingSummaryActivity.this, BillSummaryActivity.class);
                        intent.putExtra("appointment_post", new Gson().toJson(appointmentPost));
                        intent.putExtra("membership",new Gson().toJson(homeResponse, HomeResponse.class));
                        intent.putExtra("appointment_id", response.body().getData().getAppointmentId());      //yeh beu ke liye hai
                        startActivity(intent);

                        mContentView.setVisibility(View.VISIBLE);
                        mLoadingView.setVisibility(View.INVISIBLE);


                    }else{
                        Log.i(TAG, "on not successs");
                        mContentView.setVisibility(View.VISIBLE);
                        mLoadingView.setVisibility(View.INVISIBLE);
                    }


                }else{
                    Log.i(TAG, "on retrofit not successs");
                    mContentView.setVisibility(View.VISIBLE);
                    mLoadingView.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<AppointmentResponse> call, Throwable t) {

                mContentView.setVisibility(View.VISIBLE);
                mLoadingView.setVisibility(View.INVISIBLE);
                Toast.makeText(BookingSummaryActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                Log.i(TAG, "i'm in on failure: "+ t.getMessage()+ "  "+ t.toString()+ "  "+
                        t.getCause()+ "  "+ t.getStackTrace());
            }
        });*/
    }





    public String formatDateTime(String str_date){

        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss");
        Date date_format=null;
        try {
            date_format = sdf.parse(str_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("MMM dd yyyy, EEEE HH:mm aa").format(date_format);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){

            getSupportActionBar().setTitle(getResources().getString(R.string.txt_booking_summary_static_title));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }
    }


    public void logInitiatedCheckoutEvent (String contentType, String currency, double totalPrice) {
        Log.e("prefine",contentType+ "no of items"  +"price"+totalPrice);
        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        logger.logEvent(AppEventsConstants.EVENT_NAME_INITIATED_CHECKOUT, totalPrice, params);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}


