package com.beusalons.android.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robbin Singh on 10/11/2016.
 */

public class ServiceModel implements Serializable{


    private String name;
    private int quantity = 0;
    private String subTitle;
    private Integer serviceCode;
    private String gender;
    private List<ServicePriceModel> prices = new ArrayList<ServicePriceModel>();
    private List<ServicesDeals> deals= new ArrayList<>();
    private boolean isSelected = false;
    private int service_index;
    private String serviceId;

    private String nameOnApp;

    public Integer getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(Integer serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public int getService_index() {
        return service_index;
    }

    public void setService_index(int service_index) {
        this.service_index = service_index;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    /**
     *

     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }


    public String getNameOnApp() {
        return nameOnApp;
    }

    public void setNameOnApp(String nameOnApp) {
        this.nameOnApp = nameOnApp;
    }

    /**
     *
     * @param gender
     * The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     *
     * @return
     * The prices
     */
    public List<ServicePriceModel> getPrices() {
        return prices;
    }

    /**
     *
     * @param prices
     * The prices
     */
    public void setPrices(List<ServicePriceModel> prices) {
        this.prices = prices;
    }

    public List<ServicesDeals> getDeals() {
        return deals;
    }

    public void setDeals(List<ServicesDeals> deals) {
        this.deals = deals;
    }
}
