package com.beusalons.android.Model.SalonReview;

/**
 * Created by myMachine on 9/4/2017.
 */

public class Response {

    private boolean success;
    private Data data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
