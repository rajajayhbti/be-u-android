package com.beusalons.android.Fragment;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Event.FiltersEvent;
import com.beusalons.android.R;
import com.esotericsoftware.kryo.serializers.FieldSerializer;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by myMachine on 4/12/2017.
 */

public class FilterFragment extends DialogFragment {

    public FilterFragment(){

        //empty constructor
    }

    @BindView(R.id.txt_apply) TextView txt_apply;
    @BindView(R.id.txt_cancel) TextView txt_cancel;


    //sort stuff hai yeh
    @BindView(R.id.linear_popularity) LinearLayout linear_popularity;
    @BindView(R.id.img_popularity_1) ImageView img_popularity_1;
    @BindView(R.id.img_popularity_2) ImageView img_popularity_2;
    @BindView(R.id.txt_popularity_1) TextView txt_popularity_1;
    @BindView(R.id.txt_popularity_2) TextView txt_popularity_2;

    @BindView(R.id.linear_price_low) LinearLayout linear_price_low;
    @BindView(R.id.img_price_low_1) ImageView img_price_low_1;
    @BindView(R.id.img_price_low_2) ImageView img_price_low_2;
    @BindView(R.id.txt_price_low_1) TextView txt_price_low_1;
    @BindView(R.id.txt_price_low_2) TextView txt_price_low_2;

    @BindView(R.id.linear_price_high) LinearLayout linear_price_high;
    @BindView(R.id.img_price_high_1) ImageView img_price_high_1;
    @BindView(R.id.img_price_high_2) ImageView img_price_high_2;
    @BindView(R.id.txt_price_high_1) TextView txt_price_high_1;
    @BindView(R.id.txt_price_high_2) TextView txt_price_high_2;

    @BindView(R.id.linear_nearest) LinearLayout linear_nearest;
    @BindView(R.id.img_nearest_1) ImageView img_nearest_1;
    @BindView(R.id.img_nearest_2) ImageView img_nearest_2;
    @BindView(R.id.txt_nearest_1) TextView txt_nearest_1;
    @BindView(R.id.txt_nearest_2) TextView txt_nearest_2;

    //filter stuff
    @BindView(R.id.txt_rating_1) TextView txt_rating_1;
    @BindView(R.id.txt_rating_2) TextView txt_rating_2;
    @BindView(R.id.txt_rating_3) TextView txt_rating_3;
    @BindView(R.id.txt_rating_4) TextView txt_rating_4;

    @BindView(R.id.txt_price_red) TextView txt_price_red;
    @BindView(R.id.txt_price_blue) TextView txt_price_blue;
    @BindView(R.id.txt_price_green) TextView txt_price_green;
    @BindView(R.id.txt_price_all) TextView txt_price_all;

    @BindView(R.id.txt_male) TextView txt_male;
    @BindView(R.id.txt_female) TextView txt_female;
    @BindView(R.id.txt_unisex) TextView txt_unisex;

    private boolean isMale=false, isFemale= false, isUnisex= false;
    private boolean isPopularity= false, isPriceLow= false, isPriceHigh= false, isNearest= false;
    private boolean isRating_1= false, isRating_2= false, isRating_3= false, isRating_4= false;
    private boolean isPrice_red= false, isPrice_blue= false, isPrice_green= false, isPrice_all= false;

    private HashMap<String, Boolean> filters_hashmap;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);          //no action bar space
        View view = inflater.inflate(R.layout.fragment_filters, container);
        ButterKnife.bind(this, view);   //setting butterknife

        filters_hashmap= new HashMap<>();

        Bundle bundle= getArguments();
        if(bundle!=null){

            //sort stuff
            isPopularity= bundle.getBoolean("isPopularity", false);
            isPriceLow= bundle.getBoolean("isPriceLow", false);
            isPriceHigh= bundle.getBoolean("isPriceHigh", false);
            isNearest= bundle.getBoolean("isNearest", false);

            //----------------------filter stuff-------------------
            //gender stuff
            isMale= bundle.getBoolean("isMale", false);
            isFemale= bundle.getBoolean("isFemale", false);
            isUnisex= bundle.getBoolean("isUnisex", false);

            //rating stuff
            isRating_1= bundle.getBoolean("isRating_1", false);
            isRating_2= bundle.getBoolean("isRating_2", false);
            isRating_3= bundle.getBoolean("isRating_3", false);
            isRating_4= bundle.getBoolean("isRating_4", false);

            //price stuff
            isPrice_red= bundle.getBoolean("isPrice_red", false);
            isPrice_blue= bundle.getBoolean("isPrice_blue", false);
            isPrice_green= bundle.getBoolean("isPrice_green", false);
            isPrice_all= bundle.getBoolean("isPrice_all", false);

        }

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();
            }
        });

        txt_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EventBus.getDefault().post(new FiltersEvent(filters_hashmap));
                dismiss();
            }
        });

        //sort
        sortStuff();

        //filtes
        ratingStuff();
        priceStuff();
        genderStuff();

        return view;
    }



    public void sortStuff(){

        if(isPopularity){

            txt_popularity_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));
            txt_popularity_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));

            img_popularity_1.setImageResource(R.drawable.ic_filters_rating);
            img_popularity_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));
            img_popularity_2.setVisibility(View.VISIBLE);
            img_popularity_2.setImageResource(R.drawable.ic_filters_blue_tick);

            filters_hashmap.put("isPopularity", true);
        }else{
            txt_popularity_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
            txt_popularity_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));

            img_popularity_1.setImageResource(R.drawable.ic_filters_rating);
            img_popularity_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
            img_popularity_2.setVisibility(View.INVISIBLE);

            filters_hashmap.put("isPopularity", false);
        }

        if(isPriceLow){

            txt_price_low_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));
            txt_price_low_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));

            img_price_low_1.setImageResource(R.drawable.ic_filter_rupee_up);
            img_price_low_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));
            img_price_low_2.setVisibility(View.VISIBLE);
            img_price_low_2.setImageResource(R.drawable.ic_filters_blue_tick);

            filters_hashmap.put("isPriceLow", true);

        }else{

            txt_price_low_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
            txt_price_low_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));

            img_price_low_1.setImageResource(R.drawable.ic_filter_rupee_up);
            img_price_low_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
            img_price_low_2.setVisibility(View.INVISIBLE);

            filters_hashmap.put("isPriceLow", false);
        }

        if(isPriceHigh){

            txt_price_high_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));
            txt_price_high_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));

            img_price_high_1.setImageResource(R.drawable.ic_filter_rupee_down);
            img_price_high_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));
            img_price_high_2.setVisibility(View.VISIBLE);
            img_price_high_2.setImageResource(R.drawable.ic_filters_blue_tick);

            filters_hashmap.put("isPriceHigh", true);

        }else{
            txt_price_high_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
            txt_price_high_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));

            img_price_high_1.setImageResource(R.drawable.ic_filter_rupee_down);
            img_price_high_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
            img_price_high_2.setVisibility(View.INVISIBLE);

            filters_hashmap.put("isPriceHigh", false);
        }

        if(isNearest){
            txt_nearest_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));
            txt_nearest_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));

            img_nearest_1.setImageResource(R.drawable.ic_filters_nearest);
            img_nearest_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));
            img_nearest_2.setVisibility(View.VISIBLE);
            img_nearest_2.setImageResource(R.drawable.ic_filters_blue_tick);

            filters_hashmap.put("isNearest", true);
        }else{
            txt_nearest_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
            txt_nearest_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));

            img_nearest_1.setImageResource(R.drawable.ic_filters_nearest);
            img_nearest_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
            img_nearest_2.setVisibility(View.INVISIBLE);

            filters_hashmap.put("isNearest", false);
        }


        linear_popularity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isPopularity){//false

                    txt_popularity_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                    txt_popularity_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));

                    img_popularity_1.setImageResource(R.drawable.ic_filters_rating);
                    img_popularity_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                    img_popularity_2.setVisibility(View.INVISIBLE);

                    filters_hashmap.put("isPopularity", false);
                    isPopularity= false;

                }else{  //true hai yeh

                    txt_popularity_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));
                    txt_popularity_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));

                    img_popularity_1.setImageResource(R.drawable.ic_filters_rating);
                    img_popularity_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));
                    img_popularity_2.setVisibility(View.VISIBLE);
                    img_popularity_2.setImageResource(R.drawable.ic_filters_blue_tick);

                    filters_hashmap.put("isPopularity", true);
                    isPopularity= true;
                }

                //disable rest
                txt_price_low_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                txt_price_low_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_price_low_1.setImageResource(R.drawable.ic_filter_rupee_up);
                img_price_low_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_price_low_2.setVisibility(View.INVISIBLE);
                filters_hashmap.put("isPriceLow", false);
                isPriceLow= false;

                txt_price_high_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                txt_price_high_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_price_high_1.setImageResource(R.drawable.ic_filter_rupee_down);
                img_price_high_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_price_high_2.setVisibility(View.INVISIBLE);
                filters_hashmap.put("isPriceHigh", false);
                isPriceHigh= false;

                txt_nearest_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                txt_nearest_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_nearest_1.setImageResource(R.drawable.ic_filters_nearest);
                img_nearest_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_nearest_2.setVisibility(View.INVISIBLE);
                filters_hashmap.put("isNearest", false);
                isNearest= false;



            }
        });

        linear_price_low.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPriceLow){

                    txt_price_low_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                    txt_price_low_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));

                    img_price_low_1.setImageResource(R.drawable.ic_filter_rupee_up);
                    img_price_low_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                    img_price_low_2.setVisibility(View.INVISIBLE);

                    filters_hashmap.put("isPriceLow", false);
                    isPriceLow= false;
                }else{

                    txt_price_low_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));
                    txt_price_low_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));

                    img_price_low_1.setImageResource(R.drawable.ic_filter_rupee_up);
                    img_price_low_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));
                    img_price_low_2.setVisibility(View.VISIBLE);
                    img_price_low_2.setImageResource(R.drawable.ic_filters_blue_tick);

                    filters_hashmap.put("isPriceLow", true);
                    isPriceLow= true;
                }

                //disable rest
                txt_popularity_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                txt_popularity_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_popularity_1.setImageResource(R.drawable.ic_filters_rating);
                img_popularity_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_popularity_2.setVisibility(View.INVISIBLE);
                filters_hashmap.put("isPopularity", false);
                isPopularity= false;

                txt_price_high_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                txt_price_high_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_price_high_1.setImageResource(R.drawable.ic_filter_rupee_down);
                img_price_high_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_price_high_2.setVisibility(View.INVISIBLE);
                filters_hashmap.put("isPriceHigh", false);
                isPriceHigh= false;

                txt_nearest_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                txt_nearest_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_nearest_1.setImageResource(R.drawable.ic_filters_nearest);
                img_nearest_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_nearest_2.setVisibility(View.INVISIBLE);
                filters_hashmap.put("isNearest", false);
                isNearest= false;


            }
        });

        linear_price_high.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPriceHigh){

                    txt_price_high_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                    txt_price_high_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));

                    img_price_high_1.setImageResource(R.drawable.ic_filter_rupee_down);
                    img_price_high_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                    img_price_high_2.setVisibility(View.INVISIBLE);

                    filters_hashmap.put("isPriceHigh", false);
                    isPriceHigh= false;

                }else{

                    txt_price_high_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));
                    txt_price_high_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));

                    img_price_high_1.setImageResource(R.drawable.ic_filter_rupee_down);
                    img_price_high_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));
                    img_price_high_2.setVisibility(View.VISIBLE);
                    img_price_high_2.setImageResource(R.drawable.ic_filters_blue_tick);

                    filters_hashmap.put("isPriceHigh", true);
                    isPriceHigh= true;
                }

                //disable rest
                txt_popularity_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                txt_popularity_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_popularity_1.setImageResource(R.drawable.ic_filters_rating);
                img_popularity_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_popularity_2.setVisibility(View.INVISIBLE);
                filters_hashmap.put("isPopularity", false);
                isPopularity= false;

                txt_price_low_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                txt_price_low_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_price_low_1.setImageResource(R.drawable.ic_filter_rupee_up);
                img_price_low_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_price_low_2.setVisibility(View.INVISIBLE);
                filters_hashmap.put("isPriceLow", false);
                isPriceLow= false;

                txt_nearest_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                txt_nearest_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_nearest_1.setImageResource(R.drawable.ic_filters_nearest);
                img_nearest_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_nearest_2.setVisibility(View.INVISIBLE);
                filters_hashmap.put("isNearest", false);
                isNearest= false;

            }
        });

        linear_nearest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNearest){
                    txt_nearest_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                    txt_nearest_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));

                    img_nearest_1.setImageResource(R.drawable.ic_filters_nearest);
                    img_nearest_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                    img_nearest_2.setVisibility(View.INVISIBLE);

                    filters_hashmap.put("isNearest", false);
                    isNearest= false;
                }else{

                    txt_nearest_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));
                    txt_nearest_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));

                    img_nearest_1.setImageResource(R.drawable.ic_filters_nearest);
                    img_nearest_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));
                    img_nearest_2.setVisibility(View.VISIBLE);
                    img_nearest_2.setImageResource(R.drawable.ic_filters_blue_tick);

                    filters_hashmap.put("isNearest", true);
                    isNearest= true;
                }

                //disable rest
                txt_popularity_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                txt_popularity_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_popularity_1.setImageResource(R.drawable.ic_filters_rating);
                img_popularity_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_popularity_2.setVisibility(View.INVISIBLE);
                filters_hashmap.put("isPopularity", false);
                isPopularity= false;

                txt_price_high_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                txt_price_high_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_price_high_1.setImageResource(R.drawable.ic_filter_rupee_down);
                img_price_high_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_price_high_2.setVisibility(View.INVISIBLE);
                filters_hashmap.put("isPriceHigh", false);
                isPriceHigh= false;

                txt_price_low_1.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                txt_price_low_2.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_price_low_1.setImageResource(R.drawable.ic_filter_rupee_up);
                img_price_low_1.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimaryText));
                img_price_low_2.setVisibility(View.INVISIBLE);
                filters_hashmap.put("isPriceLow", false);
                isPriceLow= false;

            }
        });

    }

    public void priceStuff(){

        final String price_red_false = "<font color='#3e780a'>₹₹₹₹</font>"+"<font color='#58595b'>₹ - </font>"+
                "<font color='#3e780a'>₹₹₹₹₹</font>";
        final String price_red_true = "<font color='#3e780a'>₹₹₹₹</font>"+"<font color='#FFFFFF'>₹ - </font>"+
                "<font color='#3e780a'>₹₹₹₹₹</font>";

        final String price_blue_false = "<font color='#3e780a'>₹₹₹</font>"+"<font color='#58595b'>₹₹ - </font>"+
                "<font color='#3e780a'>₹₹₹₹</font>"+"<font color='#58595b'>₹</font>";
        final String price_blue_true = "<font color='#3e780a'>₹₹₹</font>"+"<font color='#FFFFFF'>₹₹ - </font>"+
                "<font color='#3e780a'>₹₹₹₹</font>"+"<font color='#FFFFFF'>₹</font>";

        final String price_green_false = "<font color='#3e780a'>₹₹</font>"+"<font color='#58595b'>₹₹₹ - </font>"+
                "<font color='#3e780a'>₹₹₹</font>"+"<font color='#58595b'>₹₹</font>";
        final String price_green_true = "<font color='#3e780a'>₹₹</font>"+"<font color='#FFFFFF'>₹₹₹ - </font>"+
                "<font color='#3e780a'>₹₹₹</font>"+"<font color='#FFFFFF'>₹₹</font>";



        if(isPrice_red){
            txt_price_red.setText(fromHtml(price_red_true));
            txt_price_red.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
            filters_hashmap.put("isPrice_red", true);
        }else{
            txt_price_red.setText(fromHtml(price_red_false));
            txt_price_red.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
            filters_hashmap.put("isPrice_red", false);
        }
        if(isPrice_blue){
            txt_price_blue.setText(fromHtml(price_blue_true));
            txt_price_blue.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
            filters_hashmap.put("isPrice_blue", true);
        }else{
            txt_price_blue.setText(fromHtml(price_blue_false));
            txt_price_blue.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
            filters_hashmap.put("isPrice_blue", false);
        }
        if(isPrice_green){
            txt_price_green.setText(fromHtml(price_green_true));
            txt_price_green.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
            filters_hashmap.put("isPrice_green", true);
        }else{
            txt_price_green.setText(fromHtml(price_green_false));
            txt_price_green.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
            filters_hashmap.put("isPrice_green", false);
        }
        if(isPrice_all){
            txt_price_all.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            txt_price_all.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
            filters_hashmap.put("isPrice_all", true);
        }else{
            txt_price_all.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
            txt_price_all.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
            filters_hashmap.put("isPrice_all", false);
        }


        txt_price_red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPrice_red){
                    txt_price_red.setText(fromHtml(price_red_false));
                    txt_price_red.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                    filters_hashmap.put("isPrice_red", false);
                    isPrice_red= false;
                }else{
                    txt_price_red.setText(fromHtml(price_red_true));
                    txt_price_red.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
                    filters_hashmap.put("isPrice_red", true);
                    isPrice_red= true;
                }
                txt_price_blue.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_price_blue.setText(fromHtml(price_blue_false));
                filters_hashmap.put("isPrice_blue", false);
                isPrice_blue= false;
                txt_price_green.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_price_green.setText(fromHtml(price_green_false));
                filters_hashmap.put("isPrice_green", false);
                isPrice_green= false;
                txt_price_all.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_price_all.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isPrice_all", false);
                isPrice_all= false;
            }
        });
        txt_price_blue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPrice_blue){
                    txt_price_blue.setText(fromHtml(price_blue_false));
                    txt_price_blue.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                    filters_hashmap.put("isPrice_blue", false);
                    isPrice_blue= false;
                }else{
                    txt_price_blue.setText(fromHtml(price_blue_true));
                    txt_price_blue.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
                    filters_hashmap.put("isPrice_blue", true);
                    isPrice_blue= true;
                }
                txt_price_red.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_price_red.setText(fromHtml(price_red_false));
                filters_hashmap.put("isPrice_red", false);
                isPrice_red= false;
                txt_price_green.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_price_green.setText(fromHtml(price_green_false));
                filters_hashmap.put("isPrice_green", false);
                isPrice_green= false;
                txt_price_all.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_price_all.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isPrice_all", false);
                isPrice_all= false;
            }

        });
        txt_price_green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPrice_green){
                    txt_price_green.setText(fromHtml(price_green_false));
                    txt_price_green.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                    filters_hashmap.put("isPrice_green", false);
                    isPrice_green= false;
                }else{
                    txt_price_green.setText(fromHtml(price_green_true));
                    txt_price_green.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
                    filters_hashmap.put("isPrice_green", true);
                    isPrice_green= true;
                }

                txt_price_red.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_price_red.setText(fromHtml(price_red_false));
                filters_hashmap.put("isPrice_red", false);
                isPrice_red= false;
                txt_price_blue.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_price_blue.setText(fromHtml(price_blue_false));
                filters_hashmap.put("isPrice_blue", false);
                isPrice_blue= false;
                txt_price_all.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_price_all.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isPrice_all", false);
                isPrice_all= false;
            }
        });
        txt_price_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPrice_all){
                    txt_price_all.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                    txt_price_all.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                    filters_hashmap.put("isPrice_all", false);
                    isPrice_all= false;
                }else{
                    txt_price_all.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                    txt_price_all.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
                    filters_hashmap.put("isPrice_all", true);
                    isPrice_all= true;
                }

                txt_price_red.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_price_red.setText(fromHtml(price_red_false));
                filters_hashmap.put("isPrice_red", false);
                isPrice_red= false;
                txt_price_blue.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_price_blue.setText(fromHtml(price_blue_false));
                filters_hashmap.put("isPrice_blue", false);
                isPrice_blue= false;
                txt_price_green.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_price_green.setText(fromHtml(price_green_false));
                filters_hashmap.put("isPrice_green", false);
                isPrice_green= false;
            }
        });

    }


    public void genderStuff(){

        if(isMale){
            txt_male.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
            txt_male.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            filters_hashmap.put("isMale", true);
        }else{
            txt_male.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
            txt_male.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
            filters_hashmap.put("isMale", false);
        }

        if(isFemale){
            txt_female.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
            txt_female.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            filters_hashmap.put("isFemale", true);
        }else{
            txt_female.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
            txt_female.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
            filters_hashmap.put("isFemale", false);
        }

        if(isUnisex){
            txt_unisex.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
            txt_unisex.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            filters_hashmap.put("isUnisex", true);
        }else{
            txt_unisex.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
            txt_unisex.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
            filters_hashmap.put("isUnisex", false);
        }

        txt_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isMale){//false

                    txt_male.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                    txt_male.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                    filters_hashmap.put("isMale", false);
                    isMale=false;
                }else{              //yeh true ka case hai

                    txt_male.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
                    txt_male.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                    filters_hashmap.put("isMale", true);
                    isMale=true;
                }
                //female aur unisex ko karo false
                txt_female.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_female.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isFemale", false);
                isFemale=false;

                txt_unisex.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_unisex.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isUnisex", false);
                isUnisex=false;
            }
        });

        txt_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isFemale){//false

                    txt_female.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                    txt_female.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                    filters_hashmap.put("isFemale", false);
                    isFemale=false;

                }else{              //yeh true ka case hai

                    txt_female.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
                    txt_female.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                    filters_hashmap.put("isFemale", true);
                    isFemale=true;
                }

                //male aur unisex false
                txt_male.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_male.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isMale", false);
                isMale=false;

                txt_unisex.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_unisex.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isUnisex", false);
                isUnisex=false;
            }
        });

        txt_unisex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isUnisex){//false

                    txt_unisex.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                    txt_unisex.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                    filters_hashmap.put("isUnisex", false);
                    isUnisex=false;
                }else{              //yeh true ka case hai

                    txt_unisex.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
                    txt_unisex.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                    filters_hashmap.put("isUnisex", true);
                    isUnisex=true;
                }

                //male aur female for false kyuki yeh toh single select hai
                txt_male.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_male.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isMale", false);
                isMale=false;

                txt_female.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_female.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isFemale", false);
                isFemale=false;
            }
        });

    }


    public void ratingStuff(){

        if(isRating_1){
            txt_rating_1.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
            txt_rating_1.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            filters_hashmap.put("isRating_1", true);
        }else{
            txt_rating_1.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
            txt_rating_1.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
            filters_hashmap.put("isRating_1", false);
        }
        if(isRating_2){
            txt_rating_2.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
            txt_rating_2.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            filters_hashmap.put("isRating_2", true);
        }else{
            txt_rating_2.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
            txt_rating_2.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
            filters_hashmap.put("isRating_2", false);
        }
        if(isRating_3){
            txt_rating_3.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
            txt_rating_3.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            filters_hashmap.put("isRating_3", true);
        }else{
            txt_rating_3.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
            txt_rating_3.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
            filters_hashmap.put("isRating_3", false);
        }
        if(isRating_4){
            txt_rating_4.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
            txt_rating_4.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            filters_hashmap.put("isRating_4", true);
        }else{
            txt_rating_4.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
            txt_rating_4.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
            filters_hashmap.put("isRating_4", false);
        }


        txt_rating_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isRating_1){//

                    txt_rating_1.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                    txt_rating_1.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                    filters_hashmap.put("isRating_1", false);
                    isRating_1=false;
                }else{//true ka case hai yeh

                    txt_rating_1.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
                    txt_rating_1.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                    filters_hashmap.put("isRating_1", true);
                    isRating_1= true;
                }

                //baki sab off
                txt_rating_2.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_rating_2.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isRating_2", false);
                isRating_2=false;
                txt_rating_3.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_rating_3.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isRating_3", false);
                isRating_3=false;
                txt_rating_4.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_rating_4.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isRating_4", false);
                isRating_4=false;
            }
        });

        txt_rating_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isRating_2){//

                    txt_rating_2.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                    txt_rating_1.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                    filters_hashmap.put("isRating_2", false);
                    isRating_2=false;
                }else{//true ka case hai yeh

                    txt_rating_2.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
                    txt_rating_2.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                    filters_hashmap.put("isRating_2", true);
                    isRating_2= true;
                }
                txt_rating_1.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_rating_1.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isRating_1", false);
                isRating_1=false;
                txt_rating_3.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_rating_3.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isRating_3", false);
                isRating_3=false;
                txt_rating_4.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_rating_4.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isRating_4", false);
                isRating_4=false;
            }
        });

        txt_rating_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isRating_3){//

                    txt_rating_3.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                    txt_rating_3.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                    filters_hashmap.put("isRating_3", false);
                    isRating_3=false;
                }else{//true ka case hai yeh

                    txt_rating_3.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
                    txt_rating_3.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                    filters_hashmap.put("isRating_3", true);
                    isRating_3= true;
                }

                txt_rating_1.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_rating_1.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isRating_1", false);
                isRating_1=false;
                txt_rating_2.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_rating_2.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isRating_2", false);
                isRating_2=false;
                txt_rating_4.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_rating_4.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isRating_4", false);
                isRating_4=false;
            }
        });

        txt_rating_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isRating_4){//

                    txt_rating_4.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                    txt_rating_4.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                    filters_hashmap.put("isRating_4", false);
                    isRating_4=false;
                }else{//true ka case hai yeh

                    txt_rating_4.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
                    txt_rating_4.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                    filters_hashmap.put("isRating_4", true);
                    isRating_4= true;
                }

                txt_rating_1.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_rating_1.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isRating_1", false);
                isRating_1=false;
                txt_rating_2.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_rating_2.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isRating_2", false);
                isRating_2=false;
                txt_rating_3.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
                txt_rating_3.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
                filters_hashmap.put("isRating_3", false);
                isRating_3=false;
            }
        });


    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {

            Window window = getDialog().getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFFFFF")));
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }


}
