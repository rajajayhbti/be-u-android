package com.beusalons.android.Model.ParlorDetail;

/**
 * Created by Ashish Sharma on 10/13/2017.
 */

public class FacebookCheckinResponse {
    private boolean success;
    private String data;
private String message;
    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
