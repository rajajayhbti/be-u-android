package com.beusalons.android.Model;

import java.util.List;

/**
 * Created by myMachine on 19-Feb-18.
 */

public class UpdateAppResponse {

    private boolean success;
    private List<Datum> data= null;


    public class Datum{

        private String image;
        private String heading;
        private String description;


        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getHeading() {
            return heading;
        }

        public void setHeading(String heading) {
            this.heading = heading;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
}
