package com.beusalons.android.Model.Logout;

/**
 * Created by Ajay on 2/18/2017.
 */

public class LogoutModel {
    private String userId;
    private String accessToken;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
