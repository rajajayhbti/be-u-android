package com.beusalons.android;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Fragment.ReferSubscriptionFragment;
import com.beusalons.android.Fragment.SelectEmployeeFragment;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.Appointments.AppointmentPost;
import com.beusalons.android.Model.CartModel;
import com.beusalons.android.Model.PaymentSuccessPost;
import com.beusalons.android.Model.PaymentSuccessResponse;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.Utility;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PaymentFailSuccessActivity extends AppCompatActivity {

    private static final String TAG= PaymentFailSuccessActivity.class.getSimpleName();
    private FirebaseAnalytics mFirebaseAnalytics;
    private TextView txt_orderID, txt_day, txt_date, txt_day_text, txt_salonName, txt_salonAddress,
    /*txt_amount,*/ txt_message, txt_confirmation,txt_suggestion,txt_bcash_info;

    private ProgressBar progressBar;
    private LinearLayout linear_home, linear_call_us, linear_try_again,txt_direction,txt_select_emp;

    private String razor_pay_key,subscriptionPopUpText;
    private String accessToken, userID, appt_id="", salonNumber="", salonName="", salonAddress="", startsAt, salonAppointmentID;
    private Double amount, latitude, longitude;
    private AppointmentPost appointment_post;

    private Toolbar toolbar;
    private LinearLayout linear_bg,ll_appointment_date,ll_salon_container;
    AppEventsLogger logger;
    private ImageView img_hai;
    private View view_last,view_try_;

    private int retry_payment= 0;
    CartModel  recent_cart;

    private boolean is_notifcation= false;
    private String alertMessage;
    private int isUseFreeBie;

    private  double serviceTotal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_fail_success);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle("Order Summary");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        logger = AppEventsLogger.newLogger(this);
        txt_orderID= (TextView)findViewById(R.id.txt_orderID);
        txt_suggestion= (TextView)findViewById(R.id.txt_suggestion);
        txt_day= (TextView)findViewById(R.id.txt_day);
        txt_date= (TextView)findViewById(R.id.txt_date);
        txt_day_text= (TextView)findViewById(R.id.txt_day_text);

        txt_salonName= (TextView)findViewById(R.id.txt_salonName);
        txt_salonAddress= (TextView)findViewById(R.id.txt_salonAddress);
//        view_try_= (View)findViewById(R.id.view_try_);
//        view_last= (View)findViewById(R.id.view_last);

        txt_message= (TextView)findViewById(R.id.txt_message);
        txt_confirmation= (TextView)findViewById(R.id.txt_confirmation);
        txt_bcash_info= (TextView)findViewById(R.id.txt_bcash_info);

//        txt_amount= (TextView)findViewById(R.id.txt_amount);
        linear_bg= (LinearLayout)findViewById(R.id.linear_bg);
        ll_appointment_date= (LinearLayout)findViewById(R.id.ll_appointment_date);

        img_hai= (ImageView)findViewById(R.id.img_success_failure);

        txt_direction= findViewById(R.id.txt_direction);
        txt_select_emp= findViewById(R.id.txt_select_emp);
        txt_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri= Uri.parse("http://maps.google.com/maps?q="+ latitude  +"," +
                        longitude+"("+ salonName + ")&iwloc=A&hl=es");
                Intent intent= new Intent(Intent.ACTION_VIEW, uri);
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
        });

        progressBar= (ProgressBar) findViewById(R.id.progress_linear);
        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this,
                R.color.colorPrimary),
                android.graphics.PorterDuff.Mode.MULTIPLY);

        linear_try_again= (LinearLayout)findViewById(R.id.linear_try_again);
        ll_salon_container= (LinearLayout)findViewById(R.id.ll_salon_container);
        linear_try_again.setVisibility(View.GONE);

        linear_home= (LinearLayout)findViewById(R.id.linear_home);
        linear_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        linear_call_us= (LinearLayout)findViewById(R.id.linear_call_us);
        linear_call_us.setVisibility(View.VISIBLE);
        linear_call_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                if (salonNumber.length()>0) callIntent.setData(Uri.parse("tel:"+salonNumber));
               else callIntent.setData(Uri.parse("tel:"+"8690291910"));
                startActivity(callIntent);

            }
        });

        SharedPreferences preferences= getSharedPreferences("userDetails", Context.MODE_PRIVATE);
        if(preferences!=null){

            userID= preferences.getString("userId", null);
            accessToken= preferences.getString("accessToken", null);
            Log.i(TAG, "email: "+ preferences.getString("emailId", null)+ "  mobiel: "+preferences.getString("phoneNumber", null));
        }

        Bundle bundle= getIntent().getExtras();
        if(bundle!=null){
            appointment_post=new Gson().fromJson(bundle.getString("appointment_post",null),AppointmentPost.class);
            appt_id= bundle.getString("appointmentId", "");
            razor_pay_key= bundle.getString("razorPayKey");
            amount= bundle.getDouble("amount", 0);
            salonName= bundle.getString("salonName", "");
            subscriptionPopUpText=bundle.getString("subscriptionPopUpText", "");
            salonNumber= bundle.getString("salonNumber", "");
            salonAddress= bundle.getString("salonAddress", "");
            alertMessage= bundle.getString("cashbackmessage", "");
            salonAppointmentID= bundle.getString("salonAppointmentId", "");
            startsAt= bundle.getString("startsAt", "");
            latitude= bundle.getDouble("latitude", 4.0);
            longitude= bundle.getDouble("longitude", 4.0);
            serviceTotal=bundle.getDouble("serviceTotal");
            isUseFreeBie= bundle.getInt("useFreebie",5);

            is_notifcation= bundle.getBoolean("isNotification", false );

            Log.i("valueinthe", "vljflda: " + latitude+ " " + longitude+  " " +amount);

            Double id= Double.parseDouble(salonAppointmentID);
//            txt_orderID.setText("Your Order ID: "+id.intValue());
            txt_salonName.setText(salonName);
            txt_salonAddress.setText(salonAddress);
//            txt_amount.setText(AppConstant.CURRENCY+amount);



            Log.i("datewala", "value in startsAt: "+ startsAt);

            DateFormat startsAt_format= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            Date date= new Date();


            try{
                date= startsAt_format.parse(startsAt);

                Calendar cal= Calendar.getInstance();
                cal.setTime(date);

                txt_day.setText(""+cal.get(Calendar.DAY_OF_MONTH));         //day set kiya yaha

                int day_= cal.get(Calendar.DAY_OF_MONTH);
                Log.i("datewala" , "value in day_: "+day_);

                if(day_ == 1 || day_ == 21 || day_ == 31){
                    txt_day_text.setText("st");
                }else if(day_==2 || day_==22){
                    txt_day_text.setText("nd");
                }else if(day_ == 3 || day_ == 23){
                    txt_day_text.setText("rd");
                }else{
                    txt_day_text.setText("th");
                }

                DateFormat month_format= new SimpleDateFormat("MMMM");
                DateFormat time_format= new SimpleDateFormat("HH.mm a");

                cal.add(Calendar.HOUR_OF_DAY, 5);           //sychronizing the json time i.e.. sarepanch ghante aage
                cal.add(Calendar.MINUTE, 30);
                date=  cal.getTime();

                Log.i("datewala"," my values lj: "+ month_format.format(date) + "   "+ time_format.format(date));
                txt_date.setText(""+month_format.format(date)+" at "+time_format.format(date)); // yaha baki ka date set kiya
            }catch (Exception e){
                e.printStackTrace();
            }

            if(bundle.getBoolean("isOnline")){              // online payment hai yeh
                if(bundle.getBoolean("isSuccess")){     //payment successful in online mode
                    txt_select_emp.setVisibility(View.VISIBLE);     //gone kara yaha
                    appointment_post=new Gson().fromJson(bundle.getString("appointment_post",null),AppointmentPost.class);
                    logOnlinePaymentEvent();
                    logBookingConfirmedEvent();
                    logBookingConfirmedFireBaseEvent();
                    logOnlinePaymentFireBaseEvent();

                    if (appointment_post!=null && appointment_post.getBuyMembershipId()!=null)
                        logBuyFamilyWalletEvent(""+amount);                  //membership buy event


                    if (appointment_post!=null && appointment_post.getUseMembershipCredits()==1 && appointment_post.getBuyMembershipId()==null){
                        logUseFamilyWalletEvent();                                    //for use membership cradits
                    }

                    img_hai.setImageResource(R.drawable.ic_payment_success);
                    toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.toolbar_green));
                    linear_bg.setBackground(ContextCompat.getDrawable(this, R.drawable.gradient_green));//initially green set kiya- failure pe set to red

                    Log.i("india", " online success pay hai");
                    bookOnlinePayment(razor_pay_key, amount*100, 5, appt_id);

                }else{              //payment unsuccessful in online mode----- failure

                    Log.i("india", " online failure pay hai");
                    logBookingCancelledEvent();
                    logBookingCancelledFirBaseEvent();
                    linear_try_again.setVisibility(View.VISIBLE);
                    txt_select_emp.setVisibility(View.GONE);
                    linear_call_us.setVisibility(View.VISIBLE);
//                    txt_orderID.setVisibility(View.GONE);
                    txt_day.setVisibility(View.GONE);
                    txt_date.setVisibility(View.GONE);
                    txt_day_text.setVisibility(View.GONE);
                    txt_salonName.setVisibility(View.GONE);
                    txt_salonAddress.setVisibility(View.GONE);
                    ll_appointment_date.setVisibility(View.INVISIBLE);
                    txt_suggestion.setVisibility(View.GONE);

                    img_hai.setImageResource(R.drawable.ic_payment_failure);
                    txt_message.setText("PAYMENT\nFAILED");
                    txt_orderID.setText("Sorry For The Inconvenience.\nWe Are Currently Facing Some Technical Issues.");
                    txt_confirmation.setVisibility(View.GONE);
                    txt_bcash_info.setVisibility(View.GONE);
                    ll_salon_container.setVisibility(View.INVISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
//                    view_try_.setVisibility(View.INVISIBLE);
//                    view_last.setVisibility(View.INVISIBLE);
                    toolbar.setBackgroundColor(ContextCompat.getColor(PaymentFailSuccessActivity.this, R.color.toolbar_red));
                    linear_bg.setBackground(ContextCompat.getDrawable(PaymentFailSuccessActivity.this, R.drawable.gradient_red));

                }
            }else{                  //cash payment hai yeh
                Log.i("india", "offline succes pay hai "+"value in start at: "+ startsAt);
                if(bundle.getBoolean("isSuccess")){             //payment successful hai offline ke case mai
                    logPurchasedEvent(salonName,salonAppointmentID,"INR",(int)serviceTotal);

                    logPurchasedFireBaseEvent(salonName,salonAppointmentID,"INR",(int)serviceTotal);

                    if (appointment_post!=null && appointment_post.getBuyMembershipId()!=null)
                        logBuyFamilyWalletEvent(""+amount);                  //membership buy event


                    if (appointment_post!=null && appointment_post.getUseMembershipCredits()==1 &&  appointment_post.getBuyMembershipId()==null ){
                        logUseFamilyWalletEvent();
                    }
                    txt_select_emp.setVisibility(View.VISIBLE);     //gone kara yaha
                    img_hai.setImageResource(R.drawable.ic_payment_success);
                    txt_message.setText("BOOKING\nSUCCESSFUL");
                    txt_orderID.setText("We Have Confirmed Your Appointment For Cash Payment Of "+AppConstant.CURRENCY+amount+".");
                    txt_confirmation.setText("Get excited! Your session is confirmed for ");
                    txt_bcash_info.setText(alertMessage);

                    progressBar.setVisibility(View.INVISIBLE);              //by default isko hide karo gone nai-- success mai bhi hide
                    toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.toolbar_green));
                    linear_bg.setBackground(ContextCompat.getDrawable(this, R.drawable.gradient_green));//initially green set kiya- failure pe set to red

                    if (subscriptionPopUpText!=null && subscriptionPopUpText.length()>0){
                        ReferSubscriptionFragment dialog=new ReferSubscriptionFragment();
                        Bundle args=new Bundle();
                        args.putString(ReferSubscriptionFragment.DATA,subscriptionPopUpText);
                        dialog.setArguments(args);
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        dialog.show(fragmentManager,"ashish");
                    }
                   /* new AlertDialog.Builder(PaymentFailSuccessActivity.this)
                            .setMessage(alertMessage)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    dialog.dismiss();
                                }
                            })
                            .setCancelable(false)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();*/

                    DB snappyDB = null;
                    try {

                        snappyDB = DBFactory.open(this);
                        if(snappyDB.exists(AppConstant.USER_CART_DB)) {
                            Log.i("mainyahatukaha", " existing hai");
                            UserCart saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);

                            for (int i=0;i<saved_cart.getServicesList().size();i++){
                                if (saved_cart.getServicesList().get(i).isFree_service()){
                                    logAvailFreebieEvent();
                                    logAvailFreebieFireBaseEvent();
                                }
                            }
                            snappyDB.del(AppConstant.USER_CART_DB);
                        }
                        snappyDB.close();
                    } catch (SnappydbException e) {

                        e.printStackTrace();
                    }


                }else{                      //unsuccessful hai----- failure

//                    linear_try_again.setVisibility(View.VISIBLE);
                    txt_select_emp.setVisibility(View.GONE);
                    logBookingCancelledEvent();
                    logBookingCancelledFirBaseEvent();
                    linear_call_us.setVisibility(View.VISIBLE);
                    img_hai.setImageResource(R.drawable.ic_payment_failure);
                    txt_message.setText("PAYMENT\nFAILED");
                    txt_bcash_info.setVisibility(View.GONE);
                    txt_orderID.setText("Sorry For The Inconvenience.\nWe Are Currently Facing Some Technical Issues.");
                    txt_confirmation.setText("Confirmation failed for");
                    progressBar.setVisibility(View.INVISIBLE);
                    toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.toolbar_red));
                    linear_bg.setBackground(ContextCompat.getDrawable(this, R.drawable.gradient_red));
                    txt_orderID.setVisibility(View.GONE);
                    txt_day.setVisibility(View.GONE);
                    txt_date.setVisibility(View.GONE);
                    txt_day_text.setVisibility(View.GONE);
                    txt_salonName.setVisibility(View.GONE);
                    txt_salonAddress.setVisibility(View.GONE);
                    txt_confirmation.setVisibility(View.GONE);
                    ll_salon_container.setVisibility(View.INVISIBLE);
//                    view_try_.setVisibility(View.INVISIBLE);
//                    view_last.setVisibility(View.INVISIBLE);
                    ll_appointment_date.setVisibility(View.INVISIBLE);
                    txt_suggestion.setVisibility(View.GONE);

                }

            }

        }

        linear_try_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent= is_notifcation? new Intent(PaymentFailSuccessActivity.this, OnlinePaymentActivity.class):
                        new Intent(PaymentFailSuccessActivity.this, BillSummaryActivity.class);
                intent.putExtra("appointment_id", appt_id);
                intent.putExtra("appointment_post", new Gson().toJson(appointment_post));
                intent.putExtra("phone_number", "normalAppointment");
                startActivity(intent);
            }
        });
        txt_select_emp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent=new Intent(PaymentFailSuccessActivity.this,SelectStylistActivity.class);
//                intent.putExtra("appointment_id", appt_id);
//                startActivity(intent);

                SelectEmployeeFragment fragment= new SelectEmployeeFragment();
                Bundle bundle= new Bundle();
                bundle.putString(SelectEmployeeFragment.SALON_NAME, salonName);
                bundle.putString(SelectEmployeeFragment.APPOINTMENT_ID, appt_id);
                fragment.setArguments(bundle);
                fragment.show(getSupportFragmentManager(), "select_emp" );

            }
        });

    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logAvailFreebieEvent () {
        Log.e("AvaiFreebie","fine");

        logger.logEvent(AppConstant.AvaiFreebie);
    }

    public void logAvailFreebieFireBaseEvent () {
        Log.e("AvaiFreebiefirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.AvaiFreebie,bundle);
    }



    public void logPurchasedEvent (String contentType, String contentId, String currency, double price) {
        Log.e("prefine  sucess",contentType+" price"+price);
        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, contentId);
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        logger.logPurchase(BigDecimal.valueOf(price), Currency.getInstance(currency),params);

    }
    public void logPurchasedFireBaseEvent (String contentType, String contentId, String currency, double price) {
        Log.e("prefine  fire sucess",contentType+" price"+price);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.CURRENCY, currency);
        bundle.putDouble(FirebaseAnalytics.Param.VALUE, price);
        bundle.putString(FirebaseAnalytics.Param.SHIPPING, contentType);
        bundle.putString(FirebaseAnalytics.Param.TRANSACTION_ID, contentId);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.ECOMMERCE_PURCHASE, bundle);


    }

    public void logUseFamilyWalletEvent () {
        Log.e("logUseFamilyWalletEvent","fine");

        logger.logEvent(AppConstant.UseFamilyWallet);
    }
    public void logBuyFamilyWalletEvent (String amount) {
        Log.e("logBuyFamilyWalletEvent","fine"+amount);

        Bundle params = new Bundle();
        params.putString("Amount", amount);
        logger.logEvent(AppConstant.BuyFamilyWallet, params);
    }
    public void logOnlinePaymentEvent () {
        Log.e("OnlinePayment","fine");

        logger.logEvent(AppConstant.OnlinePayment);
    }
    public void logOnlinePaymentFireBaseEvent () {
        Log.e("OnlinePaymentfirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.OnlinePayment,bundle);
    }
    public void logBookingConfirmedEvent () {
        Log.e("BookingConfirmed","fine");

        logger.logEvent(AppConstant.BookingConfirmed);
    }
    public void logBookingConfirmedFireBaseEvent () {
        Log.e("BookingConfirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.BookingConfirmed,bundle);
    }
    public void logBookingCancelledEvent () {

        Log.e("BookingCancelled","fine");
        logger.logEvent(AppConstant.BookingCancelled);
    }
    public void logBookingCancelledFirBaseEvent () {

        Log.e("BookingCancelled","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.BookingCancelled,bundle);
    }


    public void bookOnlinePayment(String payment_method_key, final Double amount, int payment_method_id, final String appointmentId){

        linear_home.setClickable(false);

        // abhi awaiting hai na isliye
        progressBar.setVisibility(View.VISIBLE);
        txt_message.setText("");
        txt_confirmation.setText("Awaiting confirmation for");

        PaymentSuccessPost post= new PaymentSuccessPost();

        if (isUseFreeBie==1){
            post.setUseLoyalityPoints(1);
        }else post.setUseLoyalityPoints(0);

        post.setRazorpay_payment_id(payment_method_key);
        post.setAmount(amount);
        post.setPaymentMethod(payment_method_id);

        Log.i("investigaaaaa", "value in the klsdafjklsdakljslda jsdaklsdafkljsfdakl: "+
                appointmentId+ "     " + accessToken+ " "   + userID +  " "+payment_method_key+ " "
                +amount+ " "+ payment_method_id);

        post.setAppointmentId(appointmentId);
        post.setAccessToken(accessToken);
        post.setUserId(userID);

        post.setBookByNewApp(1);

        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<PaymentSuccessResponse> call= apiInterface.bookCapturePayment(post);
        call.enqueue(new Callback<PaymentSuccessResponse>() {
            @Override
            public void onResponse(Call<PaymentSuccessResponse> call, Response<PaymentSuccessResponse> response) {

                Log.i(TAG, "i'm in cash payment success response ");

                if(response.isSuccessful()){
                    if(response.body().getSuccess()){
                        Log.i(TAG, "i'm in cash payment success true pe ");

                        logPurchasedEvent(salonName,appointmentId,"INR",(int)serviceTotal);
                        logPurchasedFireBaseEvent(salonName,appointmentId,"INR",(int)serviceTotal);
                        DB snappyDB = null;
                        try {

                            snappyDB = DBFactory.open(PaymentFailSuccessActivity.this);
                            if(snappyDB.exists(AppConstant.USER_CART_DB)) {
                                Log.i("mainyahatukaha", " existing hai");
                                UserCart saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);

                                for (int i=0;i<saved_cart.getServicesList().size();i++){
                                    if (saved_cart.getServicesList().get(i).isFree_service()){
                                        logAvailFreebieEvent();
                                        logAvailFreebieFireBaseEvent();
                                    }
                                }
                                snappyDB.del(AppConstant.USER_CART_DB);
                            }
                            snappyDB.close();
                        } catch (SnappydbException e) {

                            e.printStackTrace();
                        }




                        try{

                            Runnable runnable= new Runnable() {
                                @Override
                                public void run() {
                                    txt_select_emp.setVisibility(View.VISIBLE);     //gone kara yaha
                                    txt_message.setText("BOOKING\nSUCCESSFUL");
                                    txt_orderID.setText("We Have Received Your Online Payment Of "+AppConstant.CURRENCY+(amount/100)+".");
                                    txt_confirmation.setText("Get excited! Your session is confirmed for ");
                                    txt_bcash_info.setVisibility(View.VISIBLE);
                                    txt_bcash_info.setText(alertMessage);
                                    img_hai.setImageResource(R.drawable.ic_payment_success);
                                    linear_try_again.setVisibility(View.GONE);
                                    progressBar.setVisibility(View.INVISIBLE);

                                    linear_home.setClickable(true);
                                    if (subscriptionPopUpText!=null && subscriptionPopUpText.length()>0){
                                        ReferSubscriptionFragment dialog=new ReferSubscriptionFragment();
                                        Bundle args=new Bundle();
                                        args.putString(ReferSubscriptionFragment.DATA,subscriptionPopUpText);
                                        dialog.setArguments(args);
                                        FragmentManager fragmentManager = getSupportFragmentManager();
                                        dialog.show(fragmentManager,"ashish");
                                    }

                                 /*   new AlertDialog.Builder(PaymentFailSuccessActivity.this)
                                            .setMessage(alertMessage)
                                            .setTitle("Booking Confirmed")
                                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // continue with delete
                                                    dialog.dismiss();
                                                }
                                            })
                                            .setCancelable(false)
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .show();*/


                                }
                            };

                            new Handler().postDelayed(runnable, 3000);

                        }catch (Exception e){
                            e.printStackTrace();
                        }




                    }else{
                        logBookingCancelledEvent();
                        logBookingCancelledFirBaseEvent();
                        linear_home.setClickable(true);
                        linear_try_again.setVisibility(View.VISIBLE);
                        linear_call_us.setVisibility(View.VISIBLE);

                        img_hai.setImageResource(R.drawable.ic_payment_failure);
                        txt_select_emp.setVisibility(View.GONE);
                        txt_message.setText("PAYMENT\nFAILED");
                        txt_orderID.setText("Sorry For The Inconvenience.\nWe Are Currently Facing Some Technical Issues.");
                        txt_confirmation.setText("Unable To Process Your Payment, \n Please Try Again");
                        progressBar.setVisibility(View.INVISIBLE);
                        toolbar.setBackgroundColor(ContextCompat.getColor(PaymentFailSuccessActivity.this, R.color.toolbar_red));
                        linear_bg.setBackground(ContextCompat.getDrawable(PaymentFailSuccessActivity.this, R.drawable.gradient_red));

                        img_hai.setImageResource(R.drawable.ic_payment_failure);

                        linear_try_again.setVisibility(View.VISIBLE);
                        linear_call_us.setVisibility(View.VISIBLE);
//                    txt_orderID.setVisibility(View.GONE);
                        txt_day.setVisibility(View.GONE);
                        txt_date.setVisibility(View.GONE);
                        txt_day_text.setVisibility(View.GONE);
                        txt_salonName.setVisibility(View.GONE);
                        txt_salonAddress.setVisibility(View.GONE);
//                    txt_confirmation.setVisibility(View.GONE);
                        ll_salon_container.setVisibility(View.INVISIBLE);
//                        view_try_.setVisibility(View.INVISIBLE);
//                        view_last.setVisibility(View.INVISIBLE);
                        ll_appointment_date.setVisibility(View.INVISIBLE);
                        txt_suggestion.setVisibility(View.GONE);
                    }
                }else{
                    logBookingCancelledEvent();
                    logBookingCancelledFirBaseEvent();
                    // retrofit ke response baad mai handle kar diyo
                    Log.i(TAG, "i'm in retrofit success failure pe right now ");

                    linear_home.setClickable(true);
                    txt_message.setText("PAYMENT\nFAILED");
                    txt_orderID.setText("Sorry For The Inconvenience.\nWe Are Currently Facing Some Technical Issues.");
                    txt_confirmation.setText("Unable To Process Your Payment, \n Please Try Again");
                    progressBar.setVisibility(View.INVISIBLE);
                    toolbar.setBackgroundColor(ContextCompat.getColor(PaymentFailSuccessActivity.this, R.color.toolbar_red));
                    linear_bg.setBackground(ContextCompat.getDrawable(PaymentFailSuccessActivity.this, R.drawable.gradient_red));

                    img_hai.setImageResource(R.drawable.ic_payment_failure);

                    linear_try_again.setVisibility(View.VISIBLE);
                    linear_call_us.setVisibility(View.VISIBLE);
//                    txt_orderID.setVisibility(View.GONE);
                    txt_day.setVisibility(View.GONE);
                    txt_date.setVisibility(View.GONE);
                    txt_day_text.setVisibility(View.GONE);
                    txt_salonName.setVisibility(View.GONE);
                    txt_salonAddress.setVisibility(View.GONE);
//                    txt_confirmation.setVisibility(View.GONE);
                    ll_salon_container.setVisibility(View.INVISIBLE);
//                    view_try_.setVisibility(View.INVISIBLE);
//                    view_last.setVisibility(View.INVISIBLE);
                    ll_appointment_date.setVisibility(View.INVISIBLE);
                    txt_suggestion.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<PaymentSuccessResponse> call, Throwable t) {

                if(retry_payment<2){

                    bookOnlinePayment(razor_pay_key, amount*100, 5, appt_id);

                }else{
                    logBookingCancelledEvent();
                    logBookingCancelledFirBaseEvent();
                    linear_home.setClickable(true);
                    txt_message.setText("PAYMENT\nFAILED");
                    txt_orderID.setText("Sorry For The Inconvenience.\nWe Are Currently Facing Some Technical Issues.");
                    txt_confirmation.setText("Unable To Process Your Payment, \n Please Try Again");
                    progressBar.setVisibility(View.INVISIBLE);
                    toolbar.setBackgroundColor(ContextCompat.getColor(PaymentFailSuccessActivity.this, R.color.toolbar_red));
                    linear_bg.setBackground(ContextCompat.getDrawable(PaymentFailSuccessActivity.this, R.drawable.gradient_red));

                    img_hai.setImageResource(R.drawable.ic_payment_failure);

                    linear_try_again.setVisibility(View.VISIBLE);
                    linear_call_us.setVisibility(View.VISIBLE);
//                    txt_orderID.setVisibility(View.GONE);
                    txt_day.setVisibility(View.GONE);
                    txt_date.setVisibility(View.GONE);
                    txt_day_text.setVisibility(View.GONE);
                    txt_salonName.setVisibility(View.GONE);
                    txt_salonAddress.setVisibility(View.GONE);
//                    txt_confirmation.setVisibility(View.GONE);
                    ll_salon_container.setVisibility(View.INVISIBLE);
//                    view_try_.setVisibility(View.INVISIBLE);
//                    view_last.setVisibility(View.INVISIBLE);
                    ll_appointment_date.setVisibility(View.INVISIBLE);
                    txt_suggestion.setVisibility(View.GONE);
                    Toast.makeText(PaymentFailSuccessActivity.this, "No Internet connection", Toast.LENGTH_SHORT);
                }

                Log.i(TAG, "i'm in payment failure response pe : "+ t.getMessage()+ "  "+t.getLocalizedMessage()+"  "
                        +t.getCause()+ "  "+t.getStackTrace());

            }
        });
    }


    @Override
    public void onBackPressed() {

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
