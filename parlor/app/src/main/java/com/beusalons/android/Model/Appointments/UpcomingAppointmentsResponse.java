package com.beusalons.android.Model.Appointments;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 11/16/2016.
 */

public class UpcomingAppointmentsResponse {

    private Boolean success;
    private List<UpcomingData> data= new ArrayList<>();

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<UpcomingData> getData() {
        return data;
    }

    public void setData(List<UpcomingData> data) {
        this.data = data;
    }
}
