package com.beusalons.android.Model.ServiceDialog;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.beusalons.android.Model.newServiceDeals.Department;
import com.beusalons.android.R;
import com.beusalons.android.ServiceSpecificActivity;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter<String> {
     
    private Activity activity;
    private ArrayList data;
    public Resources res;
    Department tempValues=null;
    LayoutInflater inflater;
     
    /*************  CustomAdapter Constructor *****************/
    public CustomAdapter(
                          Activity activitySpinner,
                          int textViewResourceId,   
                          ArrayList objects,
                          Resources resLocal
                         ) 
     {
        super(activitySpinner, textViewResourceId, objects);
         
        /********** Take passed values **********/
        activity = activitySpinner;
        data     = objects;
        res      = resLocal;
    
        /***********  Layout inflator to call external xml layout () **********************/
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         
      }
 
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
 
    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {
 
        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
        View row = inflater.inflate(R.layout.spinner_rows, parent, false);
         
        /***** Get each Model object from Arraylist ********/
        tempValues = null;
        tempValues = (Department) data.get(position);
         
        TextView label        = (TextView)row.findViewById(R.id.service_names);
      //  TextView sub          = (TextView)row.findViewById(R.id.sub);
        ImageView companyLogo = (ImageView)row.findViewById(R.id.image);
/*
        Glide.with(activity)
                .load(data.get(position)).getImage())
                .into(companyLogo);*/
            label.setText(tempValues.getName());
            // Set values for spinner each row 
           // label.setText(tempValues.getName());
        //    sub.setText(tempValues.getUrl());
        //    companyLogo.setImageResource(res.getIdentifier
       //                                  ("com.androidexample.customspinner:drawable/"
      //                                    + tempValues.getImage(),null,null));
     //

 
        return row;
      }
 }