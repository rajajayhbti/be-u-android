package com.beusalons.android.Model.HomeFragmentModel;

/**
 * Created by myMachine on 1/26/2017.
 */

public class Datum {

    public String imagePath;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
