package com.beusalons.android.Helper;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

/**
 * Created by Robbin Singh on 16/11/2016.
 */

public class AppConstant {

    public static final String CART_COLLECTION_NAME = "cartV3";
    public static final String NOTIFICATION_DB= "notifications";


    //database version-- 16 november, 2017
    public static final String USER_CART_DB= "version_7";             //21 dec, 2017 ka update kara

    //cart type
    public static final String SERVICE_TYPE="service_type";
    public static final String DEAL_TYPE= "deal_type";
    public static final String OTHER_TYPE = "other_type";

    public static int ITEMS_IN_CART;

    public static final String SAVE= "Save";

    public static final String CHOOSE_YOUR = "Choose Your ";
    public static String LOCATION_SUF = "Location - ";
    public static String PARLOR_SUF = "Salon - ";
    public static LatLngBounds PLACE_BOUNDS_DELHI = new LatLngBounds(new LatLng(28.412593, 76.83806899999999),new LatLng(28.881338, 77.34845780000001) );
    public static LatLngBounds PLACE_BOUNDS_BENGLORE = new LatLngBounds(new LatLng(12.7342888, 77.3791981),new LatLng(13.173706, 77.8826809));
    public static String USER_PREF = "USER";
    public static String LOCATION_PREF = "LOCATION";
    public static String CURRENCY = "₹ ";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    //use for facebook analytics event log
    public static final String SearchSalonButton = "SearchSalonButton";  //
    public static final  String FreebiesHomeButton ="FreebiesHomeButton";  //
    public static final String NearbySalonsHomeButton="NearbySalonsHomeButton";  //
    public static final String PopularDealsHomeButton="PopularDealsHomeButton";  //
    public static final String PackageHomeButton="PackagesHomeButton";            // not now package
    public static final String PopularSalonsHomeButton="PopularSalonsHomeButton"; //
    public static final String DealsPage="DealsPageFooter";                      //
    public static final String FreebiesPage="FreebiesPageFooter";                //
    public static final String ProceedInServicePage="ServicePageProceed";           //not need now
    public static final String ServicesInCart="ServicesInCart";                        //not need now
    public static final String AddDeal="AddDeals";                                  //not need now
    public static final String AddServiceFromAll="SelectServiceFromAll";            //not need now
    public static final String DealName="DealName";                                //not need now
    public static final String ProceedInDealPage="DealPageProceed";             //not need now
    public static final String SalonClick="SalonClick";                         //
    public static final String TimeSelcted="TimeSelcted";                      //
    public static final String AddService="AddServiceButton";                        //not need now
    public static final String SericeDetailPopUpOpened="ServiceDetailPopupOpened";    //not need now
    public static final String ServiceDetailPopUpLengthSelected="ServiceDetailPopUpLengthSelected";   //not need now
    public  static final String ServiceDetailPopUpComboSelected="ServiceDetailPopUpComboSelected";    //not need now
    public static final String CheckoutButtonClicked="CheckoutButtonClicked";
    public static final String CartButtonClicked="CartButtonClicked";
    public static final String ConfirmBookingClicked="ConfirmBookingClicked";
    public  static final String CashPayment="CashPayment";
    public  static final String OnlinePayment="OnlinePayment";
    public static final String AvaiFreebie="AvailFreebie";
    public static final String ShowDetail="ShowDetail";
    public static final String PayButtonClicked="PayButtonClicked";
    public static final String FreeBeeUseing="FreebiesPointsUsed";
    public static final String BookingConfirmed="BookingConfirmed";
    public static final String BookingCancelled="BookingCancelled";
    public static final String BookAppointmentClicked="BookAppointmentClicked";





    //new Event start from here
    public static final String DealDepartment="DealDepartment";
    public static final String DealCategory="DealCategory";
    public static final String DealService="DealService";
    public static final String DealBottomSheetChange="DealBottomSheetChange";
    public static final String DealBottomSheetDone="DealBottomSheetDone";
    public static final String DealBottomSheetCancel="DealBottomSheetCancel";
    public static final String DealBottomSheetOpen="DealBottomSheetOpen";
    public static final String ViewCartInDeal="ViewCartInDeal";
    public static final String ProceedInDeal="ProceedInDeal";
    //service page
    public static final String ServiceDepertment="ServiceDepertment";
    public static final String ServiceSelected="ServiceSelected";
    public static final String ServiceCategory="ServiceCategory";
    public static final String EditServicePageOpen="EditServicePageOpen";
    public static final String EditServicePageOpenAddToCart="EditServicePageOpenAddToCart";
    public static final String EditServiceBuyPackage="EditServiceBuyPackage";
    public static final String EditServiceBuyUpgrade="EditServiceBuyUpgrade";
    public static final String EditServiceCheckOut="EditServiceCheckOut";
    public static final String EditServiceContinueShoping="EditServiceContinueShoping";
    public static final String EditServiceBrandChange="EditServiceBrandChange";
    public static final String EditServiceProductChange="EditServiceProductChange";
    public static final String EditServiceLengthChange="EditServiceLengthChange";
    public static final String EditServiceUpgradeDone="EditServiceUpgradeDone";
    public static final String EditServicePackageDone="EditServicePackageDone";

    //deal and normal menu switch
    public static final String NormalMenuSwitch="NormalMenuSwitch";
    public static final String DealMenuSwitch="DealMenuSwitch";

    public static final String MemberShipViewMore="MemberShipViewMore";
    public static final String MemberShipShowDetail="MemberShipShowDetail";
    public static final String MembershipBuy="MembershipBuy";
    public static final String MembershipType="MembershipType";
    public static final String ServiceName="ServiceName";

    public static final String PackageBottomSheetChange="PackageBottomSheetChange";
    public static final String PackageBottomSheetDone="PackageBottomSheetDone";

    public static final String PackageBottomSheetCancel="PackageBottomSheetCancel";
    public static final String PackageBottomSheetOpen="PackageBottomSheetOpen";
    public static final String ProceedService="ProceedService";
    public static final String ViewCartService="ViewCartService";
    public static final String FreebiesPointsUsed="FreebiesPointsUsed";
    public static final String ComplimentryThreading="ComplimentryThreading";


    public static final String  ShowDetailPackage ="ShowDetailPackage";

    public static final String  ShowDetailInDeal="ShowDetailInDeal";




 // new Event

    public  static final String ServicePageBackButton="ServicePageBackButton";
    public  static final String ReviewScroll="ReviewScroll";
    public  static final String CallButtonClicked="CallButtonClicked";
    public  static final String PhotosButtonClicked="PhotosButtonClicked";
    public  static final String ReviewsButtonClicked="ReviewsButtonClicked";
    public  static final String DirectionButtonClicked="DirectionButtonClicked";
    public  static final String SalonProfileBackButton="SalonProfileBackButton";
    public  static final String NextDay="NextDay";
    public  static final String BlackViewCartPopUp="BlackViewCartPopUp";
    public  static final String PreviousDay="PreviousDay";
    public  static final String TimePageBackButton="TimePageBackButton";
    public  static final String SalonScroll="SalonScroll";
    public  static final String SalonBackButton="SalonBackButton";
    public  static final String SalonCategories="SalonCategories";
    public  static final String CategoriesName="CategoriesName";
    public  static final String AddCorporateIdClick="AddCorporateIdClick";
    public  static final String AddCorporateNext="AddCorporateNext";
    public  static final String ReferColleagues="ReferColleagues";
    public  static final String TapToShareCode="TapToShareCode";

    public  static final String  DealPageBackButton="DealPageBackButton";
    public  static final String  HomePageBannerSlider="HomePageBannerSlider";
    public  static final String  BannerName="BannerName";

    public  static final String BookingSummaryBackButton="BookingSummaryBackButton";
    public  static final String AddMoreButtonClicked="AddMoreButtonClicked";
    public  static final String ContinueShoppingClicked="ContinueShoppingClicked";
   // public  static final String





    //new facebook event  2 jan 2018
    public  static final String DealInfo="Deal-Info";
    public  static final String DealShare="DealShare";
    public  static final String DealServicePopupOpen="DealServicePopupOpen";
    public  static final String DealServicePopupConfirm="DealServicePopupConfirm";
    public  static final String DealServicePopupCancel="DealServicePopupCancel";
    public  static final String PackageBottomSheetOpened="PackageBottomSheetOpened";
    public  static final String PackageBottomSheetChangeCancel="PackageBottomSheetChangeCancel";
    public  static final String PackageBottomSheetChangeConfirm="PackageBottomSheetChangeConfirm";
    public  static final String PackageBottomSheetAdd="PackageBottomSheetAdd";
    public  static final String PackageBottomSheetViewCart="PackageBottomSheetViewCart";
    public  static final String SalonListTile="SalonListTile";
    public  static final String ShareSalon="ShareSalon";
    public  static final String BuyFamilyWallet="BuyFamilyWallet";
    public  static final String NotificationReceived="NotificationReceived";
    public  static final String NotificationClicked="NotificationClicked";
    public  static final String  NotificationDate="Notification date";
    public  static final String SeviceShared="SeviceShared";
    public  static final String UseFamilyWallet="UseFamilyWallet";
    public  static final String DepName="DepName";
    public  static final String Gender="Gender";
    public  static final String Name="Name";
    public  static final String SalonName="SalonName";
    public  static final String PackageName="PackageName";
    public  static final String Brand="Brand";
    public  static final String Product="Product";















}
