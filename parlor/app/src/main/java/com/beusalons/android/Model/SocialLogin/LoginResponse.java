package com.beusalons.android.Model.SocialLogin;

import java.io.Serializable;

/**
 * Created by myMachine on 3/2/2017.
 */

public class LoginResponse implements Serializable{

    private boolean success;
    private String message;
    private LoginResponseData data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginResponseData getData() {
        return data;
    }

    public void setData(LoginResponseData data) {
        this.data = data;
    }
}
