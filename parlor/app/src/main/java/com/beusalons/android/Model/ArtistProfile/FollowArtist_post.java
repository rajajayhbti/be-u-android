package com.beusalons.android.Model.ArtistProfile;

/**
 * Created by Ajay on 1/30/2018.
 */

public class FollowArtist_post {
    private String userId;
    private String artistId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }
}
