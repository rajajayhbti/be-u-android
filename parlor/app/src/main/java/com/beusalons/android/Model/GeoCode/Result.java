package com.beusalons.android.Model.GeoCode;

import java.util.ArrayList;

/**
 * Created by bhrigu on 2/13/17.
 */

public class Result {

    public String formatted_address;
    public String place_id;
    public ArrayList<AddressComponent> address_components;

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }


    public ArrayList<AddressComponent> getAddress_components() {
        return address_components;
    }

    public void setAddress_components(ArrayList<AddressComponent> address_components) {
        this.address_components = address_components;
    }


}
