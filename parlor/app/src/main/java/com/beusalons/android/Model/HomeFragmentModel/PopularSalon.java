package com.beusalons.android.Model.HomeFragmentModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PopularSalon {

@SerializedName("name")
@Expose
public String name;
@SerializedName("address1")
@Expose
public String address1;
@SerializedName("address2")
@Expose
public String address2;
@SerializedName("image")
@Expose
public String image;
@SerializedName("rating")
@Expose
public Integer rating;
@SerializedName("distance")
@Expose
public Double distance;
    int parlorType;

    public String parlorId;

    private Boolean favourite;

    public Boolean getFavourite() {
        return favourite;
    }

    public void setFavourite(Boolean favourite) {
        this.favourite = favourite;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public int getParlorType() {
        return parlorType;
    }

    public void setParlorType(int parlorType) {
        this.parlorType = parlorType;
    }


    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }
}