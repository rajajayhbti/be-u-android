package com.beusalons.android.Model.newServiceDeals.ServiceByDepartment;

import com.beusalons.android.Model.newServiceDeals.NewCombo.Selector;

import java.util.List;

/**
 * Created by myMachine on 5/30/2017.
 */

public class Service {

    private String name;
    private List<Price> prices = null;

    private String subTitle;                //subtitle hahahaha

    private int serviceCode;
    private String serviceId;
    private String dealId;
    private String dealType;
    private int dealPrice;
    private int menuPrice;
    private List<Deal> deals = null;              //iska karna hai
    private List<Upgrade> upgrades= null;              //upgrades hai yeh
    private String upgradeType;
    private List<Service> packages = null;                  //same type ka tha toh rakh lia
    private String imageUrl;

    private String estimatedTime;

    private boolean isFirstSubtitleElement= false;

    //-------------package ka data is class mai save kara kyunki recyclerview mai yeh service list ja rahi hai--------

    private String category= "service";                 //sabse important, yeh batah hai ki service hai ya koi aur like package..

    //packages ke stuff hai yeh..... jugaadh
    private String parlorDealId;
    private int price;
    private String description;
    private String shortDescription;
    private List<Object> newCombo = null;
    private List<Selector> selectors = null;
    private String slabId;

    private boolean isFirstPackageElement;


    private int quantity=0;

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isFirstSubtitleElement() {
        return isFirstSubtitleElement;
    }

    public void setFirstSubtitleElement(boolean firstSubtitleElement) {
        isFirstSubtitleElement = firstSubtitleElement;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getParlorDealId() {
        return parlorDealId;
    }

    public void setParlorDealId(String parlorDealId) {
        this.parlorDealId = parlorDealId;
    }

    public List<Service> getPackages() {
        return packages;
    }

    public void setPackages(List<Service> packages) {
        this.packages = packages;
    }

    public String getSlabId() {
        return slabId;
    }

    public void setSlabId(String slabId) {
        this.slabId = slabId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Price> getPrices() {
        return prices;
    }

    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public int getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(int serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public int getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(int dealPrice) {
        this.dealPrice = dealPrice;
    }

    public int getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(int menuPrice) {
        this.menuPrice = menuPrice;
    }

    public List<Deal> getDeals() {
        return deals;
    }

    public void setDeals(List<Deal> deals) {
        this.deals = deals;
    }

    public List<Upgrade> getUpgrades() {
        return upgrades;
    }

    public void setUpgrades(List<Upgrade> upgrades) {
        this.upgrades = upgrades;
    }

    public String getUpgradeType() {
        return upgradeType;
    }

    public void setUpgradeType(String upgradeType) {
        this.upgradeType = upgradeType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Object> getNewCombo() {
        return newCombo;
    }

    public void setNewCombo(List<Object> newCombo) {
        this.newCombo = newCombo;
    }

    public List<Selector> getSelectors() {
        return selectors;
    }

    public void setSelectors(List<Selector> selectors) {
        this.selectors = selectors;
    }

    public boolean isFirstPackageElement() {
        return isFirstPackageElement;
    }

    public void setFirstPackageElement(boolean firstPackageElement) {
        isFirstPackageElement = firstPackageElement;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
