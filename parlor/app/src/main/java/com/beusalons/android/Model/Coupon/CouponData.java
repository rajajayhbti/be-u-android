package com.beusalons.android.Model.Coupon;

import java.util.List;

/**
 * Created by Ajay on 12/5/2017.
 */

public class CouponData{

    private List<CouponCode> couponCode = null;
    private List<CouponCode> inactiveCoupons= null;

    public List<CouponCode> getInactiveCoupons() {
        return inactiveCoupons;
    }

    public void setInactiveCoupons(List<CouponCode> inactiveCoupons) {
        this.inactiveCoupons = inactiveCoupons;
    }

    public List<CouponCode> getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(List<CouponCode> couponCode) {
        this.couponCode = couponCode;
    }
}
