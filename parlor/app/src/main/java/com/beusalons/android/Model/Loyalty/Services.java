package com.beusalons.android.Model.Loyalty;

/**
 * Created by myMachine on 8/21/2017.
 */

public class Services {

    private Integer serviceCode;
    private String serviceId;

    public Integer getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(Integer serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
}
