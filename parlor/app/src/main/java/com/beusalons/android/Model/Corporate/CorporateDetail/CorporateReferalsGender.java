package com.beusalons.android.Model.Corporate.CorporateDetail;

import java.util.List;

/**
 * Created by myMachine on 8/19/2017.
 */

public class CorporateReferalsGender {

    private List<Datum_> data = null;
    private String image;

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Datum_> getData() {
        return data;
    }

    public void setData(List<Datum_> data) {
        this.data = data;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
