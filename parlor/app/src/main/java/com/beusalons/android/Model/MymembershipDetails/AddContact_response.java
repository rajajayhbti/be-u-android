package com.beusalons.android.Model.MymembershipDetails;

/**
 * Created by Ajay on 11/9/2017.
 */

public class AddContact_response {
    private Boolean success;
    private String data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
