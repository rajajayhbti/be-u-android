package com.beusalons.android.Service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.beusalons.android.Event.MembershipEvent.Event;
import com.beusalons.android.Model.SalonHome.HomeResponse;
import com.beusalons.android.Model.SalonHome.salonDepartments.SalonsDepartmentsResponse;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Ashish Sharma on 2/15/2018.
 */

public class FetchDepartmentsService extends IntentService {


    public FetchDepartmentsService() {
        super(FetchDepartmentsService.class.getName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String parlorId=intent.getStringExtra("parlorId");
        getParlorDepartments(parlorId);


    }

    private void getParlorDepartments(String parlorId){
        Retrofit retrofit = ServiceGenerator.getClient();

        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<SalonsDepartmentsResponse> call = service.getSalonsDepartments(parlorId, BeuSalonsSharedPrefrence.getUserId());
        call.enqueue(new Callback<SalonsDepartmentsResponse>() {
            @Override
            public void onResponse(Call<SalonsDepartmentsResponse> call, Response<SalonsDepartmentsResponse> response) {
                if (response.body().isSuccess()){

                    EventBus.getDefault().post(response.body().getData());


                }

            }

            @Override
            public void onFailure(Call<SalonsDepartmentsResponse> call, Throwable t) {
                Log.e("exception",call.toString());

            }
        });

    }


}
