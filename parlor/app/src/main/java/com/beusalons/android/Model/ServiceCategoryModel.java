package com.beusalons.android.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robbin Singh on 10/11/2016.
 */

public class ServiceCategoryModel implements Serializable{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("services")
    @Expose
    private List<ServiceModel> services = new ArrayList<ServiceModel>();

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The services
     */
    public List<ServiceModel> getServices() {
        return services;
    }

    /**
     *
     * @param services
     * The services
     */
    public void setServices(List<ServiceModel> services) {
        this.services = services;
    }

}

