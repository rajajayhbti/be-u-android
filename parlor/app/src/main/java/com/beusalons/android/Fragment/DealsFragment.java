package com.beusalons.android.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.beusalons.android.Adapter.DealsFragmentAdapter;
import com.beusalons.android.Model.DealsData.DealsData;
import com.beusalons.android.Model.DealsData.DealsDepartmentResponse;
import com.beusalons.android.Model.newServiceDeals.Department;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Ajay on 6/7/2017.
 */

public class DealsFragment extends Fragment {

    private String gender= "F";
    private ArrayList<Department> list= new ArrayList<>();

    RecyclerView recyclerViewDepartment;
    DealsFragmentAdapter dealsFragmentAdapter;
    ToggleButton toggleButton;

    DealsData dealsData=new DealsData();
    private LinearLayout linear_gender;

    TextView txt_female, txt_male;

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_deals_, container, false);

        recyclerViewDepartment= view.findViewById(R.id.rec_departments_deals);

        linear_gender= view.findViewById(R.id.linear_gender  );

        txt_female= view.findViewById(R.id.txt_female);
        txt_male= view.findViewById(R.id.txt_male);

        toggleButton= view.findViewById(R.id.toggle_);

        linear_gender.setVisibility(View.GONE);
        recyclerViewDepartment.hasFixedSize();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setStackFromEnd(false);
        recyclerViewDepartment.setLayoutManager(layoutManager);

        try{

            dealsFragmentAdapter = new DealsFragmentAdapter(getActivity(), list, gender,dealsData);
            recyclerViewDepartment.setAdapter(dealsFragmentAdapter);
        }catch (Exception e){
            e.printStackTrace();
        }

        fetchDepartments(view);

        txt_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleButton.setChecked(false);
            }
        });
        txt_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleButton.setChecked(true);
            }
        });

        return view;
    }

    private void fetchDepartments(final View view) {

        Retrofit retrofit = ServiceGenerator.getClient();

        final ApiInterface service = retrofit.create(ApiInterface.class);

        Call<DealsDepartmentResponse> call = service.getDeals(ServiceGenerator.BASE_URL+"api/dealsDepartment");

        call.enqueue(new Callback<DealsDepartmentResponse>() {

            @Override
            public void onResponse(Call<DealsDepartmentResponse> call, Response<DealsDepartmentResponse> response) {

                if (response.isSuccessful()) {


                    if(response.body().getSuccess()){

                        list.clear();
                        dealsData= response.body().getData();

                        if(dealsData.getDeals().get(0).getGender().equalsIgnoreCase("F"))
                            list= dealsData.getDeals().get(0).getDepartments();
                        else if(dealsData.getDeals().get(1).getGender().equalsIgnoreCase("F"))
                            list= dealsData.getDeals().get(1).getDepartments();

                        dealsFragmentAdapter.setData(list, gender,dealsData);
                        dealsFragmentAdapter.notifyDataSetChanged();


                        linear_gender.setVisibility(View.VISIBLE);
                       if (BeuSalonsSharedPrefrence.getGender()!=null && BeuSalonsSharedPrefrence.getGender().equals("M")){
                           gender= "M";

                           txt_male.setTextColor(ContextCompat.getColor(view.getContext(),
                                   R.color.black));
                           txt_female.setTextColor(ContextCompat.getColor(view.getContext(),
                                   R.color.darker_gray));

                           if(dealsData.getDeals().get(0).getGender().equalsIgnoreCase("M"))
                               list= dealsData.getDeals().get(0).getDepartments();
                           else if(dealsData.getDeals().get(1).getGender().equalsIgnoreCase("M"))
                               list= dealsData.getDeals().get(1).getDepartments();
                           dealsFragmentAdapter.setData(list, gender,dealsData);
                           toggleButton.setChecked(true);
                       }else if (BeuSalonsSharedPrefrence.getGender() !=null  && BeuSalonsSharedPrefrence.getGender().equals("F")){
                           toggleButton.setChecked(false);
                           gender= "F";

                           txt_female.setTextColor(ContextCompat.getColor(view.getContext(),
                                   R.color.black));
                           txt_male.setTextColor(ContextCompat.getColor(view.getContext(),
                                   R.color.darker_gray));

                           if(dealsData.getDeals().get(0).getGender().equalsIgnoreCase("F"))
                               list= dealsData.getDeals().get(0).getDepartments();
                           else if(dealsData.getDeals().get(1).getGender().equalsIgnoreCase("F"))
                               list= dealsData.getDeals().get(1).getDepartments();
                           dealsFragmentAdapter.setData(list, gender,dealsData);
                       }



                        //male female
                        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                                if(isChecked){
                                    gender= "M";

                                    txt_male.setTextColor(ContextCompat.getColor(view.getContext(),
                                            R.color.black));
                                    txt_female.setTextColor(ContextCompat.getColor(view.getContext(),
                                            R.color.darker_gray));

                                    if(dealsData.getDeals().get(0).getGender().equalsIgnoreCase("M"))
                                        list= dealsData.getDeals().get(0).getDepartments();
                                    else if(dealsData.getDeals().get(1).getGender().equalsIgnoreCase("M"))
                                        list= dealsData.getDeals().get(1).getDepartments();
                                    dealsFragmentAdapter.setData(list, gender,dealsData);
                                }else{
                                    gender= "F";

                                    txt_female.setTextColor(ContextCompat.getColor(view.getContext(),
                                            R.color.black));
                                    txt_male.setTextColor(ContextCompat.getColor(view.getContext(),
                                            R.color.darker_gray));

                                    if(dealsData.getDeals().get(0).getGender().equalsIgnoreCase("F"))
                                        list= dealsData.getDeals().get(0).getDepartments();
                                    else if(dealsData.getDeals().get(1).getGender().equalsIgnoreCase("F"))
                                        list= dealsData.getDeals().get(1).getDepartments();
                                    dealsFragmentAdapter.setData(list, gender,dealsData);
                                }
                            }
                        });
                        dealsFragmentAdapter.notifyDataSetChanged();


                    }else{
                        Log.i("departmentstuff", "i'm in response false");
                    }
                }else{
                    Log.i("departmentstuff", "i'm in retrofit unsuccessful");
                }
            }

            @Override
            public void onFailure(Call<DealsDepartmentResponse> call, Throwable t) {
                Log.i("departmentstuff", "i'm in failure: "+ t.getMessage()+ "  "+ t.getCause()+ "  "+ t.getStackTrace());
            }
        });
    }
}
