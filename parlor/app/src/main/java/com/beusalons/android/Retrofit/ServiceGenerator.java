package com.beusalons.android.Retrofit;

import com.beusalons.android.BuildConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.facebook.FacebookSdk.getCacheDir;

/**
 * Created by myMachine on 10/31/2016.
 */

public class ServiceGenerator {

    //base url

    public static  String BASE_URL= "http://beusalons.com/";

//    public static  String BASE_URL= "http://13.126.45.78/";        //pre live but live database

//    public static  String BASE_URL= "http://13.126.90.129/";        //test
//    public static final String BASE_URL= "http://10.0.3.8/";        //local nikita

    //google distance matrix api
    private static final String BASE_DISTANCE_URL= "http://maps.googleapis.com/maps/api/distancematrix/";

    private static Retrofit retrofit = null;





    private static  String PORTFOLIO_BASE_URL= "http://pofo.io/"; //live



    public static Retrofit getPortfolioClient(){
        Retrofit  retrofit2=null;
        if (retrofit2==null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.networkInterceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    final Request request = chain.request().newBuilder()
                            .addHeader("CUSTOM_HEADER_NAME_1", "CUSTOM_HEADER_VALUE_1")
                            .build();
                    return chain.proceed(request);
                }
            });

          /*  OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging) .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30L, TimeUnit.SECONDS)
                    .writeTimeout(30L, TimeUnit.SECONDS).build();*/
            httpClient.addInterceptor(logging);
            if (BuildConfig.DEBUG) {
//                BASE_URL= "http://beusalons.com/";        //test
                // PORTFOLIO_BASE_URL= "http://10.0.3.94:3000/";         //pre live
                PORTFOLIO_BASE_URL= "http://pofo.io/"; //live
                httpClient.addInterceptor(logging);
            }else PORTFOLIO_BASE_URL= "http://pofo.io/"; //live


            retrofit2 = new Retrofit.Builder()
                    .baseUrl(PORTFOLIO_BASE_URL)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit2;
    }


    public static Retrofit getClient() {

        if (retrofit==null) {

            int cacheSize = 10 * 1024 * 1024; // 10 MB
            Cache cache = new Cache(getCacheDir(), cacheSize);
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

//////////////////////

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
             httpClient.connectTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(15, TimeUnit.SECONDS)
           .cache(cache)
            .networkInterceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    final Request request = chain.request().newBuilder()
                            .addHeader("CUSTOM_HEADER_NAME_1", "CUSTOM_HEADER_VALUE_1")
                            .build();
                    return chain.proceed(request);
                }
            });
//////////////////////// add logging as last intesrceptor
            if (BuildConfig.DEBUG) {
//                BASE_URL= "http://beusalons.com/";        //test
                BASE_URL= "http://13.126.45.78/";         //pre live
                httpClient.addInterceptor(logging);
            }else BASE_URL= "http://beusalons.com/";

////            // <-- this is the important line!

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }


    public static Retrofit getDistance() {

        if (retrofit==null) {

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_DISTANCE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }



}
