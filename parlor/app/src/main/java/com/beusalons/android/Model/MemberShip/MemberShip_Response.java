package com.beusalons.android.Model.MemberShip;

/**
 * Created by Ajay on 5/26/2017.
 */

public class MemberShip_Response {

    private Boolean success;
    private MemberShipData data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public MemberShipData getData() {
        return data;
    }

    public void setData(MemberShipData data) {
        this.data = data;
    }
}
