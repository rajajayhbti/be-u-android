package com.beusalons.android.Model.SocialLogin;

/**
 * Created by myMachine on 3/2/2017.
 */

public class LoginPost {

    private String accessToken;
    private int socialLoginType;            // 1 for fb and 2 for g+

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public int getSocialLoginType() {
        return socialLoginType;
    }

    public void setSocialLoginType(int socialLoginType) {
        this.socialLoginType = socialLoginType;
    }
}
