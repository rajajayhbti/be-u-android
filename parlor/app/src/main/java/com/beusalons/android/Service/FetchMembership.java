package com.beusalons.android.Service;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Adapter.FreeBies.FreebiesCategoryAdapterBackUp;
import com.beusalons.android.Adapter.FreebieCorporate;
import com.beusalons.android.BuildConfig;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.AppointmentDetail.AppointmentDetailDataResponse;
import com.beusalons.android.Model.Loyalty.LoyaltyPointsRequest;
import com.beusalons.android.Model.Loyalty.LoyaltyPointsResponse;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.MyAppointments_Activity;
import com.beusalons.android.ParlorListActivity;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Task.UserCartTask;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.WebViewActivity;
import com.bumptech.glide.Glide;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Ashish Sharma on 1/7/2018.
 */

public class FetchMembership extends IntentService {
    Service  service;

    public FetchMembership() {
        super(FetchMembership.class.getName());
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        fetchLoyaltyData();
    }



    public void fetchLoyaltyData(){



        final LoyaltyPointsRequest loyaltyPointsRequest= new LoyaltyPointsRequest();
        int firstLogin=0;           //0 matalb user ka first login nahi hai



        loyaltyPointsRequest.setUserId(BeuSalonsSharedPrefrence.getUserId());
        loyaltyPointsRequest.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        loyaltyPointsRequest.setLatitude(BeuSalonsSharedPrefrence.getLatitude());
        loyaltyPointsRequest.setLongitude(BeuSalonsSharedPrefrence.getLongitude());
        loyaltyPointsRequest.setVersionAndroid(BuildConfig.VERSION_NAME);
        loyaltyPointsRequest.setFirstLogin(firstLogin);


//        final SharedPreferences.Editor editor= preferences.edit();

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<LoyaltyPointsResponse> call= apiInterface.loyaltyPoints(loyaltyPointsRequest);
        call.enqueue(new Callback<LoyaltyPointsResponse>() {
            @Override
            public void onResponse(Call<LoyaltyPointsResponse> call, final Response<LoyaltyPointsResponse> response) {

                try{
                    if(response.isSuccessful()){
                        if(response.body().getSuccess()){



                            BeuSalonsSharedPrefrence.setMyLoyaltyPoints(response.body().getData().getPoints());
                            BeuSalonsSharedPrefrence.setReferCode(getApplicationContext() ,response.body().getData().getCode());
                            BeuSalonsSharedPrefrence.setFreeHeircutBar(response.body().getData().getFreeHairCutBar());
                            BeuSalonsSharedPrefrence.setNormalMessage(response.body().getData().getNoramlReferalMessage().getMessage());

                            Float amt;
                            String name="";
                            if(response.body().getData().getActiveMemberships()!=null &&
                                    response.body().getData().getActiveMemberships().size()>0){

                                amt= (float)response.body().getData().getActiveMemberships().get(0).getCredits();
                                name= response.body().getData().getActiveMemberships().get(0).getName();
                            }
                            else
                                amt=(float)0;

                            BeuSalonsSharedPrefrence.setMembershipName(name);
                            BeuSalonsSharedPrefrence.setMembershipPoints(amt);

                            if (response.body().getData().isSubscribed()){
                                BeuSalonsSharedPrefrence.setisSubscribe(true);
                                BeuSalonsSharedPrefrence.setSubsReferMsg(response.body().getData().getSubscriptionReferMessage());
                                BeuSalonsSharedPrefrence.setSubsBalance(response.body().getData().getRemainingSubscriptionAmount());

                            }

                            if(response.body().getData().getCorporateEmailId()!=null &&
                                    !response.body().getData().getCorporateEmailId().equalsIgnoreCase("") &&
                                    !response.body().getData().getCorporateEmailId().equalsIgnoreCase(" ")){

                                BeuSalonsSharedPrefrence.setCorporateReferCode(
                                        response.body().getData().getCorporateReferalMessage().getMessage());
                                BeuSalonsSharedPrefrence.setVerifyCorporate(true);
                                BeuSalonsSharedPrefrence.setCorporateId(response.body().getData().getCorporateEmailId());
                            }









                        }else{

                            Log.i("fetchloyalty", "I'm in loyalty response failure");
                        }
                    }else{

                        Log.i("fetchloyalty", "I'm in loyalty retrofit response failure");
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<LoyaltyPointsResponse> call, Throwable t) {

                Log.i("fetchloyalty", "Loyalty request failure: "+ t.getCause()+ "  "+t.getMessage()+  " "+ t.getStackTrace()+ " "+
                        t.getLocalizedMessage());
            }
        });
    }
}
