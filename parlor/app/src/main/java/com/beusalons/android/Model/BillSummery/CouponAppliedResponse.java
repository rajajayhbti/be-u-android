package com.beusalons.android.Model.BillSummery;


/**
 * Created by Ashish Sharma on 4/19/2017.
 */

public class CouponAppliedResponse {

    private boolean success;
    private CouponData data;
    private String message;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public CouponData getData() {
        return data;
    }

    public void setData(CouponData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
