package com.beusalons.android.Service;


import android.util.Log;

import com.beusalons.android.Model.FireBaseResponse;
import com.beusalons.android.Model.FirebaseModel.FireBaseIdPost;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Ashish Sharma on 9/11/2017.
 */

public class FireBaseInstanceId extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        // Get updated InstanceID token.
        FirebaseInstanceId instance = FirebaseInstanceId.getInstance();
        String refreshedToken;
        if (instance != null) {
            refreshedToken = instance.getToken();
            // ...
//            sendTokenToMyBackend(refreshedToken); // example for general use case
            // ...
          //  AppsFlyerLib.getInstance().updateServerUninstallToken(getApplicationContext(), refreshedToken); // ADD THIS LINE HERE
            postFirBaseRegId(refreshedToken);
        }
    }

    public void postFirBaseRegId(String token){

        //get token

        FireBaseIdPost fireBaseIdPost=new FireBaseIdPost();
        fireBaseIdPost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        fireBaseIdPost.setUserId(BeuSalonsSharedPrefrence.getUserId());
        fireBaseIdPost.setFirebaseId(token);

        Retrofit retrofit1 = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit1.create(ApiInterface.class);
        Call<FireBaseResponse> call= apiInterface.fireBaseIdsPost(fireBaseIdPost);
        call.enqueue(new Callback<FireBaseResponse>() {
            @Override
            public void onResponse(Call<FireBaseResponse> call, Response<FireBaseResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().isSuccess()){

                        Log.i("reponsefirebaseclass", "success pe hoon");
                        BeuSalonsSharedPrefrence.setFirebaseSuccess(true);
                    }else
                        Log.i("reponsefirebaseclass", "else pe hoon");

                }else
                    Log.i("reponsefirebaseclass", "double else pe hoon");
            }

            @Override
            public void onFailure(Call<FireBaseResponse> call, Throwable t) {
                Log.i("reponsefirebaseclass", "failure pe hoon: "+t.getMessage()+  " " +t.getCause());
            }
        });
    }
}
