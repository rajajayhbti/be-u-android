package com.beusalons.android.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.beusalons.android.Model.Offers.MyOffers_Model;
import com.beusalons.android.R;

import java.util.List;

/**
 * Created by myMachine on 11/16/2016.
 */

public class MyOfferList_Adapter extends RecyclerView.Adapter<MyOfferList_Adapter.MyOffers_ViewHolder>{

    private List<MyOffers_Model> dealDetails;

    public MyOfferList_Adapter(List<MyOffers_Model> dealDetails){
        this.dealDetails= dealDetails;
    }

    public static class MyOffers_ViewHolder extends RecyclerView.ViewHolder{

        //booked salon details
        private TextView dealName, dealDetail, dealSalon, dealExpirtyDate;
        private Button salonDetailPage;


        public MyOffers_ViewHolder(View view) {
            super(view);

            dealName=(TextView)view.findViewById(R.id.txt_myOffers_dealName);
            dealDetail=(TextView)view.findViewById(R.id.txt_myOffers_dealDetail);
            dealSalon=(TextView)view.findViewById(R.id.txt_myOffers_dealSalon);
            dealExpirtyDate=(TextView)view.findViewById(R.id.txt_myOffers_dealExpiryDate);
            salonDetailPage= (Button)view.findViewById(R.id.btn_myOffers_salonDetailPage);
        }
    }

    @Override
    public MyOffers_ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.my_offers_cardview, parent, false);
        return new MyOffers_ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyOffers_ViewHolder holder, int position) {

        MyOffers_Model myOffers_model= dealDetails.get(position);
        //// TODO: 11/16/2016 code
    }

    @Override
    public int getItemCount() {
        return dealDetails.size();
    }
}
