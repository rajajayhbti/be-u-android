package com.beusalons.android.Model.SalonHome.salonDepartments;

/**
 * Created by Ashish Sharma on 2/15/2018.
 */

public class SalonsDepartmentsResponse {
    private boolean success;
    private SalonDeparttmentsData data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public SalonDeparttmentsData getData() {
        return data;
    }

    public void setData(SalonDeparttmentsData data) {
        this.data = data;
    }
}
