package com.beusalons.android.Adapter.NewServiceDeals;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.beusalons.android.Model.SalonHome.Membership;
import com.beusalons.android.R;
import com.bumptech.glide.Glide;
import org.greenrobot.eventbus.EventBus;
import java.util.List;

/**
 * Created by Ajay on 7/10/2017.
 */

public class BookingSummaryMembershipAdapter extends RecyclerView.Adapter<BookingSummaryMembershipAdapter.ViewHolder>{

    private Context context;
    private List<Membership> list;

    public BookingSummaryMembershipAdapter(Context context, List<Membership> list){

        this.context= context;
        this.list= list;
    }
    @Override
    public void onBindViewHolder(BookingSummaryMembershipAdapter.ViewHolder holder, final int position) {

      final   Membership membership= list.get(position);
        String strTxtView5 = " Save <font color=#d2232a><b>"+membership.getDealPercentage()+"%</b></font> on Deal Menu";
        String strTxtView20 = " Save <font color=#d2232a><b>"+membership.getNormalPercentage()+"%</b></font> on Normal Menu";
        String strMemberShip="<font color=#d2232a><b>"+membership.getTitle()+"</b></font>";
        holder.txt_nameHeader.setText(fromHtml(strMemberShip));
        holder.txtView_dealsave.setText(fromHtml(strTxtView5));
        holder.txtView_normalsave.setText(fromHtml(strTxtView20));

        Glide.with(context)
                .load(membership.getCardImage())
//                .centerCrop()
                .into(holder.imgView_servicememberShip);
        final boolean check_= membership.isSelectedMembership();
        if (check_){
            holder.rdBtn.setChecked(true);
        }
        else {
            holder.rdBtn.setChecked(false);
        }
    holder.relativeLayout_color.setOnClickListener(new View.OnClickListener() {
       @Override
       public void onClick(View v) {
           if (check_){
               EventBus.getDefault().post(list.get(position));
               membership.setSelectedMembership(false);
           }else {
               EventBus.getDefault().post(list.get(position));
               membership.setSelectedMembership(true);
           }
           updateView(position);
       }
   });

    }
    private void updateView(int pos){

        for(int i=0; i<list.size();i++){
            if(i!=pos){
                list.get(i).setSelectedMembership(false);
            }
        }
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_nameHeader,txtView_dealsave,txtView_normalsave;
        private ImageView imgView_servicememberShip;
        private RadioButton rdBtn;
        private RelativeLayout relativeLayout_color;
        public ViewHolder(View itemView) {
            super(itemView);

            txt_nameHeader= (TextView)itemView.findViewById(R.id.txtViewServiceMemberShip);
            txtView_dealsave=(TextView)itemView.findViewById(R.id.txtView_dealsave);
            txtView_normalsave=(TextView)itemView.findViewById(R.id.txtView_normalsave);

            imgView_servicememberShip= (ImageView)itemView.findViewById(R.id.imgView_servicememberShip);
            rdBtn=(RadioButton) itemView.findViewById(R.id.radio_);
            relativeLayout_color=(RelativeLayout)itemView.findViewById(R.id.linearLayout_color);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public BookingSummaryMembershipAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.billsummary_membership_adapter, parent, false);
        return new BookingSummaryMembershipAdapter.ViewHolder(view);
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }
}
