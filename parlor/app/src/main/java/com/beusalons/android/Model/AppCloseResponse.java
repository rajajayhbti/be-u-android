package com.beusalons.android.Model;

/**
 * Created by Ajay on 2/28/2018.
 */

public class AppCloseResponse {
     private boolean success;
     private  String data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
