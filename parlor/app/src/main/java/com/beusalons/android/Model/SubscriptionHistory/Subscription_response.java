package com.beusalons.android.Model.SubscriptionHistory;

import java.util.List;

/**
 * Created by Ajay on 2/26/2018.
 */

public class Subscription_response {
    private Boolean success;
    private List<SubscriptionData> data = null;


    public List<SubscriptionData> getData() {
        return data;
    }

    public void setData(List<SubscriptionData> data) {
        this.data = data;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
