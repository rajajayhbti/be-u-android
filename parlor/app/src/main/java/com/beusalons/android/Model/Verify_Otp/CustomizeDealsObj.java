package com.beusalons.android.Model.Verify_Otp;

import com.bignerdranch.expandablerecyclerview.Model.ParentObject;
import com.beusalons.android.Model.Deal.DealsServiceModel;

import java.util.List;

/**
 * Created by Ajay on 12/30/2016.
 */

public class CustomizeDealsObj implements ParentObject {

    private String name;
    private List<DealsServiceModel> childData;

    public List<DealsServiceModel> getChildData() {
        return childData;
    }

    public void setChildData(List<DealsServiceModel> childData) {
        this.childData = childData;
    }

    public CustomizeDealsObj() {
    }

    public String getName() {
        return name;
    }

    public CustomizeDealsObj(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public List<Object> getChildObjectList() {
        return null;
    }

    @Override
    public void setChildObjectList(List<Object> list) {

    }
}
