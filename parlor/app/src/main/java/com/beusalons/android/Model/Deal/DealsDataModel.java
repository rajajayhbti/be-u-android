package com.beusalons.android.Model.Deal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ajay on 12/29/2016.
 */

public class DealsDataModel implements Serializable{

    @SerializedName("categoryId")
    @Expose
    private String categoryId;

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("services")
    @Expose
    private List<DealsServiceModel> services = new ArrayList<DealsServiceModel>();

    public DealsDataModel() {
    }

    public DealsDataModel(List<DealsServiceModel> services) {
        this.services = services;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String   getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DealsServiceModel> getServices() {
        return services;
    }

    public void setServices(List<DealsServiceModel> services) {
        this.services = services;
    }
}
