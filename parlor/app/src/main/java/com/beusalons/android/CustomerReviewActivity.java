package com.beusalons.android;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Model.CustomerReview.Employee;
import com.beusalons.android.Model.CustomerReview.Post;
import com.beusalons.android.Model.CustomerReview.Response;
import com.beusalons.android.Model.Reviews.Employees;
import com.beusalons.android.Model.Reviews.WriteReviewPost;
import com.beusalons.android.Model.Reviews.WriteReviewResponse;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CustomerReviewActivity extends AppCompatActivity {

    public static final String PARLOR_ID= "parlor_id";
    public static final String PARLOR_NAME= "parlor_name";
    public static final String APPOINTMENT_ID= "appointment_id";

    private int rating=0;
    private String str_comment="";
    private String parlor_id="", parlor_name="", appointment_id="";

    private ScrollView scroll_;
    private LinearLayout linear_bottom;

    private ProgressBar progress_;
    private Button btn_retry;

    private TextView txt_text1,txt_text2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_review);
        setToolBar();

        Intent intent= getIntent();
        if(intent!=null){

            parlor_id= intent.getStringExtra(PARLOR_ID);
            parlor_name= intent.getStringExtra(PARLOR_NAME);
            appointment_id= intent.getStringExtra(APPOINTMENT_ID);
        }

        scroll_= (ScrollView)findViewById(R.id.scroll_);

        txt_text1= (TextView)findViewById(R.id.txt_text1);
        txt_text2= (TextView)findViewById(R.id.txt_text2);
        linear_bottom= (LinearLayout)findViewById(R.id.linear_bottom);

        progress_= (ProgressBar)findViewById(R.id.progress_);
        btn_retry= (Button)findViewById(R.id.btn_retry);

        btn_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchData();
            }
        });

        serviceReview();
        fetchData();

        EditText etxt_comment= (EditText)findViewById(R.id.etxt_comment);
        final TextView txt_limit= (TextView)findViewById(R.id.txt_limit);
        txt_limit.setText("0");

        etxt_comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                int length= s.toString().length();
                txt_limit.setText("" + length);
                str_comment= s.toString();

//                if(rating>0 && str_comment.length()>=25){
//                    txt_submit.setTextColor(ContextCompat.getColor(CustomerReviewActivity.this, R.color.white));
//                    linear_bottom.setBackgroundColor(ContextCompat.getColor(CustomerReviewActivity.this,
//                            R.color.colorPrimary));
//                }else{
//                    txt_submit.setTextColor(ContextCompat.getColor(CustomerReviewActivity.this, R.color.black));
//                    linear_bottom.setBackgroundColor(ContextCompat.getColor(CustomerReviewActivity.this,
//                            R.color.corporate_next));
//                }
            }
        });

    }


    public void serviceReview(){

        TextView txt_= (TextView)findViewById(R.id.txt_);

        final ImageView img_1= (ImageView)findViewById(R.id.img_1);
        final ImageView img_2= (ImageView)findViewById(R.id.img_2);
        final ImageView img_3= (ImageView)findViewById(R.id.img_3);
        final ImageView img_4= (ImageView)findViewById(R.id.img_4);
        final ImageView img_5= (ImageView)findViewById(R.id.img_5);

        txt_.setText("Review Your Experience");
        if(parlor_name!=null &&
                !parlor_name.equalsIgnoreCase(""))
            txt_.setText("Review Your Experience - at "+parlor_name+"'s");

        txt_.setTypeface(null, Typeface.BOLD);

        img_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rating=1;

                img_1.setImageResource(R.drawable.ic_star_red);
                img_2.setImageDrawable(null);
                img_3.setImageDrawable(null);
                img_4.setImageDrawable(null);
                img_5.setImageDrawable(null);

                if(rating>0){
                    linear_bottom.setAlpha(1);
                }else{
                    linear_bottom.setAlpha(.6f);
                }
            }
        });

        img_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rating=2;

                img_1.setImageResource(R.drawable.ic_star_red);
                img_2.setImageResource(R.drawable.ic_star_red);
                img_3.setImageDrawable(null);
                img_4.setImageDrawable(null);
                img_5.setImageDrawable(null);

                if(rating>0){
                    linear_bottom.setAlpha(1);
                }else{
                    linear_bottom.setAlpha(.6f);
                }
            }
        });
        img_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rating=3;

                img_1.setImageResource(R.drawable.ic_star_yellow);
                img_2.setImageResource(R.drawable.ic_star_yellow);
                img_3.setImageResource(R.drawable.ic_star_yellow);
                img_4.setImageDrawable(null);
                img_5.setImageDrawable(null);

                if(rating>0){
                    linear_bottom.setAlpha(1);
                }else{
                    linear_bottom.setAlpha(.6f);
                }
            }
        });
        img_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rating=4;

                img_1.setImageResource(R.drawable.ic_star_green);
                img_2.setImageResource(R.drawable.ic_star_green);
                img_3.setImageResource(R.drawable.ic_star_green);
                img_4.setImageResource(R.drawable.ic_star_green);
                img_5.setImageDrawable(null);

                if(rating>0){
                    linear_bottom.setAlpha(1);
                }else{
                    linear_bottom.setAlpha(.6f);
                }
            }
        });
        img_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rating=5;

                img_1.setImageResource(R.drawable.ic_star_green);
                img_2.setImageResource(R.drawable.ic_star_green);
                img_3.setImageResource(R.drawable.ic_star_green);
                img_4.setImageResource(R.drawable.ic_star_green);
                img_5.setImageResource(R.drawable.ic_star_green);

                if(rating>0){
                    linear_bottom.setAlpha(1);
                }else{
                    linear_bottom.setAlpha(.6f);
                }
            }
        });
    }

    private void fetchData(){

        scroll_.setVisibility(View.GONE);
        linear_bottom.setVisibility(View.GONE);
        progress_.setVisibility(View.VISIBLE);
        btn_retry.setVisibility(View.GONE);

        Post post= new Post();
        post.setUserId(BeuSalonsSharedPrefrence.getUserId());
        post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        post.setAppointmentId(appointment_id);

        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<Response> call= apiInterface.getResponse(post);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, final retrofit2.Response<Response> response) {

                if(response.isSuccessful()){

                    if(response.body().getSuccess()){
                        Log.i("customer_re", "in the success");

                        final List<String> emp_id= new ArrayList<>();
                        final List<Integer> emp_rating= new ArrayList<>();
                        final List<String> emp_comment= new ArrayList<>();

                        if(response.body().getData().getEmployees()!=null &&
                                response.body().getData().getEmployees().size()>0){

                            LinearLayout linear_= (LinearLayout)findViewById(R.id.linear_);

                            for(int i=0; i<response.body().getData().getEmployees().size();i++){

                                LinearLayout linear__= (LinearLayout) LayoutInflater.from(CustomerReviewActivity.this)
                                        .inflate(R.layout.review_row, null, false);

                                TextView txt_= linear__.findViewById(R.id.txt_);

                                final ImageView img_1= linear__.findViewById(R.id.img_1);
                                final ImageView img_2= linear__.findViewById(R.id.img_2);
                                final ImageView img_3= linear__.findViewById(R.id.img_3);
                                final ImageView img_4= linear__.findViewById(R.id.img_4);
                                final ImageView img_5= linear__.findViewById(R.id.img_5);

                                String services= "";
                                for(int a=0;a<response.body().getData().getEmployees().get(i).getServices().size();a++){

                                    if (a == 0) {
                                        services+= response.body().getData().getEmployees().get(i).getServices().get(a);
                                    }else{
                                        services+= ", "+ response.body().getData().getEmployees().get(i).getServices().get(a);
                                    }

                                }

                                txt_.setText("Review Employee - "+response.body().getData().getEmployees().get(i).getName()+
                                        " For "+services);
                                txt_.setTextColor(ContextCompat.getColor(linear__.getContext(), R.color.black));
//                                txt_.setTypeface(null, Typeface.BOLD);

                                emp_id.add(response.body().getData().getEmployees().get(i).getEmployeeId());
                                emp_rating.add(-1);
                                emp_comment.add("");

                                txt_text1.setText(response.body().getData().getEmployees().get(i).getText1());
                                txt_text2.setText(response.body().getData().getEmployees().get(i).getText2());
                                final int index= i;
                                img_1.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        emp_rating.set(index, 1);

                                        img_1.setImageResource(R.drawable.ic_star_red);
                                        img_2.setImageDrawable(null);
                                        img_3.setImageDrawable(null);
                                        img_4.setImageDrawable(null);
                                        img_5.setImageDrawable(null);
                                    }
                                });
                                img_2.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        emp_rating.set(index, 2);

                                        img_1.setImageResource(R.drawable.ic_star_red);
                                        img_2.setImageResource(R.drawable.ic_star_red);
                                        img_3.setImageDrawable(null);
                                        img_4.setImageDrawable(null);
                                        img_5.setImageDrawable(null);
                                    }
                                });
                                img_3.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        emp_rating.set(index, 3);

                                        img_1.setImageResource(R.drawable.ic_star_yellow);
                                        img_2.setImageResource(R.drawable.ic_star_yellow);
                                        img_3.setImageResource(R.drawable.ic_star_yellow);
                                        img_4.setImageDrawable(null);
                                        img_5.setImageDrawable(null);
                                    }
                                });
                                img_4.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        emp_rating.set(index, 4);

                                        img_1.setImageResource(R.drawable.ic_star_green);
                                        img_2.setImageResource(R.drawable.ic_star_green);
                                        img_3.setImageResource(R.drawable.ic_star_green);
                                        img_4.setImageResource(R.drawable.ic_star_green);
                                        img_5.setImageDrawable(null);
                                    }
                                });
                                img_5.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        emp_rating.set(index, 5);

                                        img_1.setImageResource(R.drawable.ic_star_green);
                                        img_2.setImageResource(R.drawable.ic_star_green);
                                        img_3.setImageResource(R.drawable.ic_star_green);
                                        img_4.setImageResource(R.drawable.ic_star_green);
                                        img_5.setImageResource(R.drawable.ic_star_green);
                                    }
                                });

                                TextView txt_comment= linear__.findViewById(R.id.txt_comment);
                                txt_comment.setVisibility(View.VISIBLE);
                                txt_comment.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        Log.i("tagthis", "on the comment click");

                                        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(view.getContext());
                                        View dialogView = LayoutInflater.from(view.getContext())
                                                .inflate(R.layout.linear_comment, null);
                                        dialogBuilder.setView(dialogView);
                                        final AlertDialog alertDialog = dialogBuilder.create();
                                        final EditText etxt_ = dialogView.findViewById(R.id.etxt_);
                                        etxt_.setText(emp_comment.get(index));
                                        etxt_.setSelection(etxt_.getText().length());
                                        TextView txt_= dialogView.findViewById(R.id.txt_);
                                        txt_.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                emp_comment.set(index, etxt_.getText().toString());
                                                alertDialog.dismiss();
                                            }
                                        });

                                        alertDialog.show();


                                    }
                                });

                                linear_.addView(linear__);
                            }

                        }

                        linear_bottom.setAlpha(.6f);
                        linear_bottom.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(rating==0)
                                    Toast.makeText(CustomerReviewActivity.this, "Please Rate Your Experience!", Toast.LENGTH_SHORT).show();
//                                else if(str_comment.length()==0)
//                                    Toast.makeText(CustomerReviewActivity.this, "Please Leave Your Comment", Toast.LENGTH_SHORT).show();
//                                else if(str_comment.length()<25)
//                                    Toast.makeText(CustomerReviewActivity.this, "Minimum 25 Characters Required", Toast.LENGTH_SHORT).show();
                                else{
                                    postReview(emp_id, emp_rating, emp_comment);
                                    finish();

                                }


                            }
                        });

                        scroll_.setVisibility(View.VISIBLE);
                        linear_bottom.setVisibility(View.VISIBLE);
                        progress_.setVisibility(View.GONE);
                        btn_retry.setVisibility(View.GONE);

                    }else{

                        Log.i("customer_re", "in the else");
                        scroll_.setVisibility(View.GONE);
                        linear_bottom.setVisibility(View.GONE);
                        progress_.setVisibility(View.GONE);
                        btn_retry.setVisibility(View.VISIBLE);
                    }




                }else{
                    Log.i("customer_re", "in the else else");
                    scroll_.setVisibility(View.GONE);
                    linear_bottom.setVisibility(View.GONE);
                    progress_.setVisibility(View.GONE);
                    btn_retry.setVisibility(View.VISIBLE);
                }



            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Log.i("customer_re", "in failure: "+ t.getMessage()+ " "+ t.getStackTrace()+ " "+t.getCause());
                scroll_.setVisibility(View.GONE);
                linear_bottom.setVisibility(View.GONE);
                progress_.setVisibility(View.GONE);
                btn_retry.setVisibility(View.VISIBLE);
            }
        });



    }

    private void postReview(List<String> emp_id, List<Integer> emp_rating, List<String> emp_comment){

        WriteReviewPost writeReviewPost= new WriteReviewPost();

        writeReviewPost.setUserId(BeuSalonsSharedPrefrence.getUserId());
        writeReviewPost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());

        writeReviewPost.setRating(rating);
        writeReviewPost.setParlorId(parlor_id);
        writeReviewPost.setAppointmentId(appointment_id);
        writeReviewPost.setText(str_comment);

        List<Employees> list= new ArrayList<>();
        for(int i=0;i<emp_id.size();i++){

            Employees employee= new Employees();
            employee.setEmployeeId(emp_id.get(i));
            employee.setRating(emp_rating.get(i));
            employee.setText(emp_comment.get(i));

            list.add(employee);
        }

        writeReviewPost.setEmployees(list);

        Log.i("writereview", "value in stuff:--rating "+writeReviewPost.getRating()+ "user id: "+ writeReviewPost.getUserId()+
                " accesstoken : " + writeReviewPost.getAccessToken()+ " parlor id: "+ writeReviewPost.getParlorId()+
                " appt id: "+writeReviewPost.getAppointmentId()+ "  get text: "+ writeReviewPost.getText());

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<WriteReviewResponse> call= apiInterface.writeReview(writeReviewPost);
        call.enqueue(new Callback<WriteReviewResponse>() {
            @Override
            public void onResponse(Call<WriteReviewResponse> call, retrofit2.Response<WriteReviewResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().getSuccess()){
                        Log.i("writerevie_", "in the success");
                        Toast.makeText(CustomerReviewActivity.this, "Review Posted", Toast.LENGTH_LONG).show();

                    }else{
                        Log.i("writerevie_", "in else pe");
                        Toast.makeText(CustomerReviewActivity.this, "Appointment Already Reviewed", Toast.LENGTH_LONG).show();

                    }



                }else{
                    Log.i("writerevie_", "in the else else");
                    Toast.makeText(CustomerReviewActivity.this, "Server Not Responding", Toast.LENGTH_SHORT).show();
                }



            }

            @Override
            public void onFailure(Call<WriteReviewResponse> call, Throwable t) {
                Log.i("writerevie_", t.getLocalizedMessage() + " " + t.getMessage() + " " + t.getCause() + " " + t.getStackTrace());
                Toast.makeText(CustomerReviewActivity.this, "Server Not Responding", Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){

            getSupportActionBar().setTitle("Review Us");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }

    }

}
