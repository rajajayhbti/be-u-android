package com.beusalons.android.Model.ArtistProfile;

import java.util.List;

/**
 * Created by Ajay on 1/29/2018.
 */

public class Project_ {

    private String _id;
    private String artistId;
    private String artistName;
    private String coverImage;
    private String postTitle;
    private Double postLatitude;
    private Double postLongitude;
    private String collectionName;
    private Integer __v;
    private Object artistPic;
    private ArtistCollec collec;
    private List<ArtistTag_> tags = null;
    private String createdAt;
    private Boolean followedByMe;
    private Integer portfolioLikes;
    private List<Object> comments = null;
    private List<Object> likes = null;
    private List<String> images = null;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public Double getPostLatitude() {
        return postLatitude;
    }

    public void setPostLatitude(Double postLatitude) {
        this.postLatitude = postLatitude;
    }

    public Double getPostLongitude() {
        return postLongitude;
    }

    public void setPostLongitude(Double postLongitude) {
        this.postLongitude = postLongitude;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public Integer get__v() {
        return __v;
    }

    public void set__v(Integer __v) {
        this.__v = __v;
    }

    public Object getArtistPic() {
        return artistPic;
    }

    public void setArtistPic(Object artistPic) {
        this.artistPic = artistPic;
    }

    public ArtistCollec getCollec() {
        return collec;
    }

    public void setCollec(ArtistCollec collec) {
        this.collec = collec;
    }

    public List<ArtistTag_> getTags() {
        return tags;
    }

    public void setTags(List<ArtistTag_> tags) {
        this.tags = tags;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getFollowedByMe() {
        return followedByMe;
    }

    public void setFollowedByMe(Boolean followedByMe) {
        this.followedByMe = followedByMe;
    }

    public Integer getPortfolioLikes() {
        return portfolioLikes;
    }

    public void setPortfolioLikes(Integer portfolioLikes) {
        this.portfolioLikes = portfolioLikes;
    }

    public List<Object> getComments() {
        return comments;
    }

    public void setComments(List<Object> comments) {
        this.comments = comments;
    }

    public List<Object> getLikes() {
        return likes;
    }

    public void setLikes(List<Object> likes) {
        this.likes = likes;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
