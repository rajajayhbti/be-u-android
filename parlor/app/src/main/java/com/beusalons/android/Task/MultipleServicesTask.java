package com.beusalons.android.Task;

import android.content.Context;
import android.util.Log;

import com.beusalons.android.Event.MembershipEvent.Event;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.snappydb.DB;
import com.snappydb.DBFactory;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by myMachine on 9/18/2017.
 */

public class MultipleServicesTask implements Runnable {

    private Context context;
    private UserCart cart;
    private List<UserServices> list;

    public MultipleServicesTask(Context context, UserCart cart, List<UserServices> list){

        this.context= context;
        this.cart= cart;
        this.list= list;
    }

    @Override
    public void run() {
        try{
            DB snappyDB= DBFactory.open(context);

            UserCart saved_cart= null;               //db mai jo saved cart hai
            if(snappyDB.exists(AppConstant.USER_CART_DB)){

                saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
                //carttype--- service, deal, memebership

                Log.i("cartstuff", "saved cart type: "+ saved_cart.getCartType());
                if(cart.getCartType()!=null &&
                        cart.getCartType().equalsIgnoreCase(AppConstant.SERVICE_TYPE)){

                    //deals ya membership already hai cart mai toh delete karo use
                    if(saved_cart.getCartType()!=null &&
                            saved_cart.getCartType().equalsIgnoreCase(AppConstant.OTHER_TYPE)){

                        saved_cart.setCartType(cart.getCartType());
                        saved_cart.setParlorId(cart.getParlorId());
                        saved_cart.setParlorName(cart.getParlorName());
                        saved_cart.setParlorType(cart.getParlorType());
                        saved_cart.setGender(cart.getGender());
                        saved_cart.setRating(cart.getRating());
                        saved_cart.setOpeningTime(cart.getOpeningTime());
                        saved_cart.setClosingTime(cart.getClosingTime());
                        saved_cart.setAddress1(cart.getAddress1());
                        saved_cart.setAddress2(cart.getAddress2());

                        for(int i=0;i<list.size();i++){

                            list.get(i).setQuantity(1);             //initially set the quanitity
                            saved_cart.getServicesList().add(list.get(i));
                        }

                    }else if(saved_cart.getCartType()!=null &&
                            saved_cart.getCartType().equalsIgnoreCase(AppConstant.DEAL_TYPE)){

                        saved_cart= cart;
                        saved_cart.getServicesList().clear();

                        for(int i=0;i<list.size();i++){

                            list.get(i).setQuantity(1);             //initially set the quanitity
                            saved_cart.getServicesList().add(list.get(i));
                        }
                    }else{

                        if(cart.getParlorId().equalsIgnoreCase(saved_cart.getParlorId())){        //same parlor id

                            //if not present add as a new service
                            for(int i=0;i<list.size();i++){

                                boolean isPresent= false;
                                for(int j=0; j<saved_cart.getServicesList().size();j++){

                                    if(list.get(i).getPrimary_key().
                                            equalsIgnoreCase(saved_cart.getServicesList().get(j).getPrimary_key())){

                                        isPresent= true;
                                        int quantity= saved_cart.getServicesList().get(i).getQuantity();
                                        saved_cart.getServicesList().get(i).setQuantity(quantity+1);
                                    }
                                }
                                if(!isPresent){

                                    list.get(i).setQuantity(1);
                                    saved_cart.getServicesList().add(list.get(i));
                                }


                            }

                        }else{              //differnt parlor id

                            saved_cart= cart;                    //initially set the quanitity
                            saved_cart.getServicesList().clear();            //clearing items

                            for(int i=0;i<list.size();i++){

                                list.get(i).setQuantity(1);             //initially set the quanitity
                                saved_cart.getServicesList().add(list.get(i));
                            }
                        }
                    }

                }else if(saved_cart.getCartType()!=null &&
                        cart.getCartType().equalsIgnoreCase(AppConstant.DEAL_TYPE)){

                    if(saved_cart.getCartType()!=null &&
                            saved_cart.getCartType().equalsIgnoreCase(AppConstant.OTHER_TYPE)){

                        saved_cart.setCartType(cart.getCartType());

                        for(int i=0;i<list.size();i++){

                            list.get(i).setQuantity(1);             //initially set the quanitity
                            saved_cart.getServicesList().add(list.get(i));
                        }

                    }else if(saved_cart.getCartType()!=null &&
                            saved_cart.getCartType().equalsIgnoreCase(AppConstant.SERVICE_TYPE)) {

                        saved_cart = cart;
                        saved_cart.getServicesList().clear();

                        for(int i=0;i<list.size();i++){

                            list.get(i).setQuantity(1);             //initially set the quanitity
                            saved_cart.getServicesList().add(list.get(i));
                        }
                    }else{

                        for(int i=0;i<list.size();i++){

                            boolean isPresent= false;
                            for(int j=0; j<saved_cart.getServicesList().size();j++){

                                if(list.get(i).getPrimary_key().
                                        equalsIgnoreCase(saved_cart.getServicesList().get(j).getPrimary_key())){

                                    isPresent= true;
                                    int quantity= saved_cart.getServicesList().get(i).getQuantity();
                                    saved_cart.getServicesList().get(i).setQuantity(quantity+1);
                                }
                            }
                            if(!isPresent){

                                list.get(i).setQuantity(1);
                                saved_cart.getServicesList().add(list.get(i));
                            }
                        }

                    }
                }else if(saved_cart.getCartType()!=null &&
                        cart.getCartType().equalsIgnoreCase(AppConstant.OTHER_TYPE)) {

                    //yeh toh karunga hi nai
                }
            }else{

                saved_cart= cart;
                saved_cart.getServicesList().clear();

                for(int i=0;i<list.size();i++){

                    list.get(i).setQuantity(1);             //initially set the quanitity
                    saved_cart.getServicesList().add(list.get(i));
                }
            }

            //quantity ke liye
//            saved_cart.setDeal_id(services.getService_deal_id());
//            saved_cart.setService_id(services.getService_id());
//            saved_cart.setInt_deal_id(services.getDealId());

            snappyDB.put(AppConstant.USER_CART_DB, saved_cart);
            snappyDB.close();
            EventBus.getDefault().post(saved_cart);

        }catch (Exception e){

            e.printStackTrace();
        }
    }
}
