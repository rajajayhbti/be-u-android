package com.beusalons.android;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Model.Corporate.CorporateDetail.CorporateDetailResponse;
import com.beusalons.android.Model.Corporate.CorporateRequest.CorporateRequestPost;
import com.beusalons.android.Model.Corporate.CorporateRequest.CorporateRequestResponse;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RegisterCorporateActivity extends AppCompatActivity {

    public static final String CORPORATE_RESPONSE="respone";
    private CorporateDetailResponse response= null;

    private TextView txt_offer, txt_request, txt_1, txt_2, txt_3;
    private EditText etxt_name, etxt_email, etxt_location;
    private LinearLayout linear_dynamic, linear_new, linear_success, linear_t_n_c;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_corporate);

        setToolBar();

        txt_request= (TextView)findViewById(R.id.txt_request); 
        txt_1= (TextView)findViewById(R.id.txt_1);
        txt_2= (TextView)findViewById(R.id.txt_2);
        txt_3= (TextView)findViewById(R.id.txt_3);
        
        
        etxt_name= (EditText) findViewById(R.id.etxt_name);
        etxt_email= (EditText)findViewById(R.id.etxt_email);
        etxt_location= (EditText)findViewById(R.id.etxt_location);
        
        
        linear_dynamic= (LinearLayout)findViewById(R.id.linear_dynamic);
        linear_new= (LinearLayout)findViewById(R.id.linear_new);
        linear_success= (LinearLayout)findViewById(R.id.linear_success);
        linear_t_n_c= (LinearLayout)findViewById(R.id.linear_t_n_c);

        Bundle bundle= getIntent().getExtras();
        if(bundle!=null)
            response= new Gson().fromJson(bundle.getString(CORPORATE_RESPONSE), CorporateDetailResponse.class);
        else
            finish();

        if(response!=null){

           setContent();

        }

        txt_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(etxt_name.getText().toString().length()==0)
                    Toast.makeText(RegisterCorporateActivity.this, "Enter Company Name", Toast.LENGTH_SHORT).show();
                else if(etxt_email.getText().toString().length()==0)
                    Toast.makeText(RegisterCorporateActivity.this, "Enter HR Email ID", Toast.LENGTH_SHORT).show();
                else if(etxt_location.getText().toString().length()==0)
                    Toast.makeText(RegisterCorporateActivity.this, "Enter Company Location", Toast.LENGTH_SHORT).show();
                else
                    sendData();
            }
        });

        linear_new.setVisibility(View.VISIBLE);
        linear_success.setVisibility(View.GONE);
    }

    
    public void setContent(){
        linear_dynamic.removeAllViews();

        if(response.getData().getCorporateDeals()!=null &&
                response.getData().getCorporateDeals().size()>0){

            View view_= LayoutInflater.from(RegisterCorporateActivity.this).inflate(R.layout.txt_referral, null);

            TextView txt_name= (TextView)view_.findViewById(R.id.txt_name);
            txt_name.setText("Corporate ID Benefits");

            linear_dynamic.addView(view_);

            int size_id= response.getData().getCorporateDeals().size();
            for(int j=0; j<size_id; j++){

                View view= LayoutInflater.from(RegisterCorporateActivity.this).inflate(R.layout.corporate_benefits, null);
                TextView txt_info= (TextView)view.findViewById(R.id.txt_info);
                TextView txt_1= (TextView)view.findViewById(R.id.txt_1);
                TextView txt_2= (TextView)view.findViewById(R.id.txt_2);
                TextView txt_3= (TextView)view.findViewById(R.id.txt_3);
                View view_last= (View)view.findViewById(R.id.view_);
                if(size_id-1== j)
                    view_last.setVisibility(View.VISIBLE);
                if(j==0)
                    txt_info.setVisibility(View.VISIBLE);

                String str_1=  "\u2022 ";
                txt_1.setText(str_1);
                txt_2.setText(response.getData().getCorporateDeals().get(j).getStart());
                txt_3.setText(" "+response.getData().getCorporateDeals().get(j).getEnd());

                linear_dynamic.addView(view);
            }
        }



        if(response.getData().getCorporateReferalsFemale()!=null &&
                response.getData().getCorporateReferalsFemale().getData()!=null &&
                response.getData().getCorporateReferalsFemale().getData().size()>0){

            View view__= LayoutInflater.from(RegisterCorporateActivity.this).inflate(R.layout.txt_referral, null);

            LinearLayout linear_= (LinearLayout)view__.findViewById(R.id.linear_);

            TextView txt_name_= (TextView)view__.findViewById(R.id.txt_name);
            TextView txt_info= (TextView)view__.findViewById(R.id.txt_info);
            txt_name_.setText("Corporate Referral Benefits");

            linear_.setVisibility(View.VISIBLE);
            txt_info.setText(response.getData().getCorporateReferalsFemale().getDescription());

            linear_dynamic.addView(view__);


            View view_= LayoutInflater.from(RegisterCorporateActivity.this).inflate(R.layout.txt_referral_, null);

            TextView txt_name= (TextView)view_.findViewById(R.id.txt_name);
            txt_name.setText("Female Upgrades");

            ImageView img_= (ImageView)view_.findViewById(R.id.img_);
             Glide.with(RegisterCorporateActivity.this).load(response.getData().getCorporateReferalsFemale().getImage()).into(img_);
           /* Glide.with(RegisterCorporateActivity.this)
                    .load(response.getData().getCorporateReferalsFemale().getImage())
                    .fitCenter()
                    .crossFade()
                    .into(img_);*/

            linear_dynamic.addView(view_);

            int size_f= response.getData().getCorporateReferalsFemale().getData().size();
            for(int k=0; k<size_f; k++){

                View view= LayoutInflater.from(RegisterCorporateActivity.this).inflate(R.layout.corporate_benefits_, null);
                TextView txt_1= (TextView)view.findViewById(R.id.txt_1);
                TextView txt_2= (TextView)view.findViewById(R.id.txt_2);
                TextView txt_3= (TextView)view.findViewById(R.id.txt_3);

                txt_1.setText(response.getData().getCorporateReferalsFemale().getData().get(k).getStart()==null?"":
                        response.getData().getCorporateReferalsFemale().getData().get(k).getStart());
                txt_2.setText("\u2022 "+response.getData().getCorporateReferalsFemale().getData().get(k).getMiddle()==null?
                "":response.getData().getCorporateReferalsFemale().getData().get(k).getMiddle());
                txt_3.setText(" "+response.getData().getCorporateReferalsFemale().getData().get(k).getEnd()==null?
                "":response.getData().getCorporateReferalsFemale().getData().get(k).getEnd());

                linear_dynamic.addView(view);
            }
        }

        if(response.getData().getCorporateReferalsMale()!=null &&
                response.getData().getCorporateReferalsMale().getData()!=null &&
                response.getData().getCorporateReferalsMale().getData().size()>0){

            View view_= LayoutInflater.from(RegisterCorporateActivity.this).inflate(R.layout.txt_referral_, null);

            TextView txt_name= (TextView)view_.findViewById(R.id.txt_name);
            txt_name.setText("Male Upgrades");

            ImageView img_= (ImageView)view_.findViewById(R.id.img_);
            Glide.with(RegisterCorporateActivity.this).load(response.getData().getCorporateReferalsMale().getImage()).into(img_);
            /*Glide.with(RegisterCorporateActivity.this)
                    .load(response.getData().getCorporateReferalsMale().getImage())
                    .fitCenter()
                    .crossFade()
                    .into(img_);*/

            linear_dynamic.addView(view_);

            int size_m= response.getData().getCorporateReferalsMale().getData().size();
            for(int k = 0; k<response.getData().getCorporateReferalsMale().getData().size(); k++){

                View view= LayoutInflater.from(RegisterCorporateActivity.this).inflate(R.layout.corporate_benefits_, null);
                TextView txt_1= (TextView)view.findViewById(R.id.txt_1);
                TextView txt_2= (TextView)view.findViewById(R.id.txt_2);
                TextView txt_3= (TextView)view.findViewById(R.id.txt_3);

                View view_last= view.findViewById(R.id.view_);
                if(size_m-1== k)
                    view_last.setVisibility(View.VISIBLE);

                txt_1.setText(response.getData().getCorporateReferalsMale().getData().get(k).getStart()==null?
                "":response.getData().getCorporateReferalsMale().getData().get(k).getStart());
                txt_2.setText("\u2022 "+response.getData().getCorporateReferalsMale().getData().get(k).getMiddle()==null?
                "":response.getData().getCorporateReferalsMale().getData().get(k).getMiddle());
                txt_3.setText(" "+response.getData().getCorporateReferalsMale().getData().get(k).getEnd()==null?
                "":response.getData().getCorporateReferalsMale().getData().get(k).getEnd());

                linear_dynamic.addView(view);
            }
        }

        if(response.getData().getCorporateTnC()!=null &&
                response.getData().getCorporateTnC().getPoints()!=null &&
                response.getData().getCorporateTnC().getPoints().size()>0){
            linear_t_n_c.setVisibility(View.VISIBLE);

            txt_1.setText(response.getData().getCorporateTnC().getTitle());
            String points="";
            for(int i=0; i<response.getData().getCorporateTnC().getPoints().size();i++){

                if(i==0)
                    txt_2.setText(response.getData().getCorporateTnC().getPoints().get(i));
                else
                    points+= response.getData().getCorporateTnC().getPoints().get(i)+ "\n";
            }
            txt_3.setText(points);

            txt_1.setTypeface(null, Typeface.ITALIC);
            txt_2.setTypeface(null, Typeface.ITALIC);
            txt_3.setTypeface(null, Typeface.ITALIC);

        }else
            linear_t_n_c.setVisibility(View.GONE);
    }

    private void sendData(){

        CorporateRequestPost post= new CorporateRequestPost();
        post.setUserId(BeuSalonsSharedPrefrence.getUserId());
        post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        post.setCompanyName(etxt_name.getText().toString());
        post.setHrEmail(etxt_email.getText().toString());
        post.setLocation(etxt_location.getText().toString());

        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<CorporateRequestResponse> call= apiInterface.corporateRequest(post);
        call.enqueue(new Callback<CorporateRequestResponse>() {
            @Override
            public void onResponse(Call<CorporateRequestResponse> call, Response<CorporateRequestResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().isSuccess()){

                        Log.i("corporaterequest","i'm in success");
                        linear_new.setVisibility(View.GONE);
                        linear_success.setVisibility(View.VISIBLE);


                    }else{
                        Log.i("corporaterequest","i'm in unsuccessful");
                    }

                }else{

                    Log.i("corporaterequest","i'm in response unsuccessful");
                }



            }

            @Override
            public void onFailure(Call<CorporateRequestResponse> call, Throwable t) {
                Log.i("corporaterequest","i'm in failure: "+ t.getMessage()+ " "+ t.getCause()+ " "+ t.getStackTrace());
            }
        });



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle("Register My Corporate");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);

        }
    }

    private void dismissKeyboard(){

        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
