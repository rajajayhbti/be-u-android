package com.beusalons.android.Adapter.HomeScreen;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Event.CartNotificationEvent;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.HomeFragmentModel.PopularSalon;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.R;
import com.beusalons.android.SalonPageActivity;
import com.bumptech.glide.Glide;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.snappydb.DB;
import com.snappydb.DBFactory;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashish on 3/3/2017.
 */

public class PopularSalonsAdapter extends RecyclerView.Adapter<PopularSalonsAdapter.ViewHolder>{
    private Context context;
    private List<PopularSalon> popularSalons;

    AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;
    public PopularSalonsAdapter(Context mcontext,List<PopularSalon> mpopularSalons){
        popularSalons=new ArrayList<PopularSalon>();
        this.popularSalons=mpopularSalons;
        this.context=mcontext;
        if (context!=null){
            logger = AppEventsLogger.newLogger(context);
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        }



    }
    @Override
    public PopularSalonsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.row_popular_salons,parent,false);
        return new PopularSalonsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PopularSalonsAdapter.ViewHolder holder, final int position) {

        holder.txtViewTitle.setText(popularSalons.get(position).getName());
        if (popularSalons.get(position).getParlorType() == 0)
            holder.img_.setImageResource(R.drawable.ic_premium_badge);
        else if (popularSalons.get(position).getParlorType() == 1)
            holder.img_.setImageResource(R.drawable.ic_standard_badge);
        else if (popularSalons.get(position).getParlorType() == 2)
            holder.img_.setImageResource(R.drawable.ic_budget_badge);

        if (popularSalons.get(position).getFavourite()){
           holder.imgFave.setVisibility(View.VISIBLE);
        }else{
            holder.imgFave.setVisibility(View.GONE);
        }

      //  holder.txtViewTitle.setText(fromHtml(parlorColor+" - "+popularSalons.get(position).getName()));
       // holder.txtViewAddress1.setText(popularSalons.get(position).getAddress1());
        holder.txtViewAddress2.setText(popularSalons.get(position).getAddress2());
        double dist=popularSalons.get(position).getDistance();
        holder.distance.setText(popularSalons.get(position).getDistance() + " km");
       /* if (dist>9.0){


        //    holder.distance.setTextColor(context.getResources().getColor(R.color.light_yellow));
        }else{
         //   holder.distance.setTextColor(context.getResources().getColor(R.color.colorGreen));
        }*/
        Glide.with(context).load(popularSalons.get(position).getImage()).into(holder.imgSalons);
       /* Glide.with(context)
                .load(popularSalons.get(position).getImage())
                .centerCrop()
                .placeholder(R.drawable.master_salon)
                .crossFade()
                .into(holder.imgSalons);*/

        holder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(context, SalonPageActivity.class);
//                intent.putExtra("parlorId", popularSalons.get(position).getParlorId());
//                context.startActivity(intent);

                boolean show_dialog= false;
                UserCart saved_cart = null;
                //opening cart, checking cart type equal to service type, and parlor id are same, if not same show dialog
                try {

                    DB snappyDB = DBFactory.open(context);
                    //db mai jo saved cart hai
                    if (snappyDB.exists(AppConstant.USER_CART_DB)) {

                        saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
                    }
                    snappyDB.close();

                    if(saved_cart!=null){

                        if(saved_cart.getCartType()!=null &&
                                saved_cart.getCartType().equalsIgnoreCase(AppConstant.SERVICE_TYPE)){

                            if(saved_cart.getParlorId().equalsIgnoreCase(popularSalons.get(position).getParlorId()))
                                show_dialog= false;
                            else{

                                if(saved_cart.getServicesList().size()>0)
                                    show_dialog= true;
                                else
                                    show_dialog= false;
                            }
                        }else{
                            show_dialog= false;
                            try {

                                DB snappyDB_ = DBFactory.open(context);
                                snappyDB_.destroy();
                                snappyDB_.close();

                                EventBus.getDefault().post(new CartNotificationEvent());
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                        }

                    }else
                        show_dialog= false;

                }catch (Exception e){
                    e.printStackTrace();
                }

                if(show_dialog){

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Replace cart item?");
                    builder.setMessage("Your cart contains services from "+ saved_cart.getParlorName()+". Do you " +
                            "wish to discard the selection and add services from "+ popularSalons.get(position).getName()+"?");
                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, int id) {

                            try {

                                DB snappyDB = DBFactory.open(context);
                                snappyDB.destroy();
                                snappyDB.close();

                                EventBus.getDefault().post(new CartNotificationEvent());
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            Intent intent = new Intent(context, SalonPageActivity.class);
                            intent.putExtra("parlorId", popularSalons.get(position).getParlorId());
                            context.startActivity(intent);

                        }
                    });
                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.dismiss();
                        }
                    }).show();

                }else{
                    logPopularSalonsHomeButtonEvent();
                    logPopularSalonsHomeButtonFireBaseEvent();
                    Intent intent = new Intent(context, SalonPageActivity.class);
                    intent.putExtra("parlorId", popularSalons.get(position).getParlorId());
                    context.startActivity(intent);
                }

            }

        });

    }

    //comment by Ajay 3 march 17
    public void setData(ArrayList<PopularSalon> popularSalons) {
        this.popularSalons = popularSalons;
        notifyDataSetChanged();
    }


    public void logPopularSalonsHomeButtonEvent () {
        Log.e("popularSalon","fine");
        logger.logEvent(AppConstant.PopularSalonsHomeButton);
    }
    public void logPopularSalonsHomeButtonFireBaseEvent () {
        Log.e("popularSalonfirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.PopularSalonsHomeButton,bundle);
    }


    @Override
    public int getItemCount() {
        return popularSalons.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtViewTitle;
        TextView txtViewAddress2;
        ImageView imgSalons,imgFave, img_;
        LinearLayout llParent;
        public TextView distance;

        public ViewHolder(View itemView) {
            super(itemView);
            txtViewTitle=(TextView)itemView.findViewById(R.id.txt_salon_title);
            txtViewAddress2=(TextView)itemView.findViewById(R.id.txtView_address2);
            imgSalons=(ImageView)itemView.findViewById(R.id.imgSalons);
            imgFave=(ImageView)itemView.findViewById(R.id.img_fav_salon);
            llParent= (LinearLayout) itemView.findViewById(R.id.ll_popular_salons);
            distance= (TextView) itemView.findViewById(R.id.distance_salon);
            img_= (ImageView) itemView.findViewById(R.id.img_);
        }
    }
}

