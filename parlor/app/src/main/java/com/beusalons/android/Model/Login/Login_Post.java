package com.beusalons.android.Model.Login;

import java.io.Serializable;

/**
 * Created by myMachine on 10/31/2016.
 */

public class Login_Post implements Serializable {


    private String phoneNumber;
    private String password;


    public Login_Post(){

    }

    public Login_Post(String phoneNumber, String password){

        this.phoneNumber= phoneNumber;
        this.password= password;
    }



    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
