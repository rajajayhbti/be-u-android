package com.beusalons.android.Fragment;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.DealsServicesActivity;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.R;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by myMachine on 18-Dec-17.
 */

public class DealsServicePopUpFragment extends DialogFragment {

    public static final String THIS_FRAGMENT= "com.beusalons."+DealsServicePopUpFragment.class.getSimpleName();
    public static final String PREMIUM_SALON= "com.beusalons.premium.salon";
    public static final String STANDARD_SALON= "com.beusalons.standard.salon";
    public static final String BUDGET_SALON= "com.beusalons.budget.salon";

    private boolean premium=false, standard=false, budget=false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle= getArguments();
        if(bundle!=null){
            premium= bundle.getBoolean(PREMIUM_SALON, false);
            standard= bundle.getBoolean(STANDARD_SALON, false);
            budget= bundle.getBoolean(BUDGET_SALON, false);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);          //no action bar space
        LinearLayout linear_= (LinearLayout)
                inflater.inflate(R.layout.deals_service_popup, container, false);

        TextView txt_clear= linear_.findViewById(R.id.txt_clear);
        TextView txt_continue= linear_.findViewById(R.id.txt_continue);
        LinearLayout linear_types= linear_.findViewById(R.id.linear_types);

        if(premium){

            LinearLayout linear= (LinearLayout)LayoutInflater.from(linear_.getContext())
                    .inflate(R.layout.linear_salon, null);

            ImageView img_= linear.findViewById(R.id.img_);
            img_.setImageResource(R.drawable.ic_premium_badge);

            TextView txt_= linear.findViewById(R.id.txt_);
            String str_premium= "<font color='#3e780a'>₹₹₹₹</font>"+"₹ - "+
                    "<font color='#3e780a'>₹₹₹₹₹</font>";
            txt_.setText(fromHtml(str_premium));

            linear_types.addView(linear);
        }
        if(standard){

            LinearLayout linear= (LinearLayout)LayoutInflater.from(linear_.getContext())
                    .inflate(R.layout.linear_salon, null);

            ImageView img_= linear.findViewById(R.id.img_);
            img_.setImageResource(R.drawable.ic_standard_badge);

            TextView txt_= linear.findViewById(R.id.txt_);
            String str_standard= "<font color='#3e780a'>₹₹₹</font>"+"₹₹ - "+
                    "<font color='#3e780a'>₹₹₹₹</font>"+"₹";
            txt_.setText(fromHtml(str_standard));

            linear_types.addView(linear);
        }

        if(budget){

            LinearLayout linear= (LinearLayout)LayoutInflater.from(linear_.getContext())
                    .inflate(R.layout.linear_salon, null);

            ImageView img_= linear.findViewById(R.id.img_);
            img_.setImageResource(R.drawable.ic_budget_badge);

            TextView txt_= linear.findViewById(R.id.txt_);
            String str_budget= "<font color='#3e780a'>₹₹</font>"+"₹"+"₹"+"₹ - "+
                    "<font color='#3e780a'>₹₹₹</font>"+"₹₹";
            txt_.setText(fromHtml(str_budget));

            linear_types.addView(linear);
        }

        txt_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final View view_= view;
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        try{

                            DB snappyDB= DBFactory.open(view_.getContext());
                            UserCart saved_cart= null;

                            if(snappyDB.exists(AppConstant.USER_CART_DB)) {

                                saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
                                saved_cart.getServicesList().clear();
                                snappyDB.put(AppConstant.USER_CART_DB, saved_cart);
                                EventBus.getDefault().post(saved_cart);
                            }
                            snappyDB.close();

                        }catch (SnappydbException e){
                            e.printStackTrace();
                        }


                    }
                }).start();


                dismiss();
            }
        });

        txt_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        ImageView img_cancel= linear_.findViewById(R.id.img_cancel);
        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });


        return linear_;
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }
}
