package com.beusalons.android.Model.Deal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ajay on 12/29/2016.
 */

public class DealsServiceModel implements Serializable{

    public DealsServiceModel() {
    }

    private List<Object> mChildrenList;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("dealPrice")
    @Expose
    private Integer dealPrice;

    @SerializedName("dealId")
    @Expose
    private String dealId;

    @SerializedName("category")
    @Expose
    private String category;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("dealIdParlor")
    @Expose
    private Integer dealIdParlor;

    @SerializedName("menuPrice")
    @Expose
    private Integer menuPrice;

    @SerializedName("tax")
    @Expose
    private Integer tax;

    @SerializedName("dealPercentage")
    @Expose
    private double dealPercentage;
    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("weekDay")
    @Expose
    private Integer weekDay;

    @SerializedName("dealType")
    @Expose
    private DealsTypes dealType;
    private boolean isSelected = false;

    private Integer quantity;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public DealsServiceModel(String name) {
        this.name = name;
    }

    public List<Object> getmChildrenList() {
        return mChildrenList;
    }

    public void setmChildrenList(List<Object> mChildrenList) {
        this.mChildrenList = mChildrenList;
    }

    public DealsServiceModel(List<DealsTypesServices> services) {
        this.services = services;
    }

    public DealsTypes getDealType() {
        return dealType;
    }

    public void setDealType(DealsTypes dealType) {
        this.dealType = dealType;
    }

    @SerializedName("services")
    @Expose
   private List<DealsTypesServices> services = new ArrayList<>();

    @SerializedName("couponCode")
    @Expose
    private Integer couponCode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(Integer dealPrice) {
        this.dealPrice = dealPrice;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public double getDealPercentage() {
        return dealPercentage;
    }

    public void setDealPercentage(double dealPercentage) {
        this.dealPercentage = dealPercentage;
    }

    public Integer getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(Integer menuPrice) {
        this.menuPrice = menuPrice;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDealIdParlor() {
        return dealIdParlor;
    }

    public void setDealIdParlor(Integer dealIdParlor) {
        this.dealIdParlor = dealIdParlor;
    }

    public Integer getTax() {
        return tax;
    }

    public void setTax(Integer tax) {
        this.tax = tax;
    }

    public Integer getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(Integer weekDay) {
        this.weekDay = weekDay;
    }



    public List<DealsTypesServices> getServices() {
        return services;
    }

    public void setServices(List<DealsTypesServices> services) {
        this.services = services;
    }

    public Integer getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(Integer couponCode) {
        this.couponCode = couponCode;
    }
}
