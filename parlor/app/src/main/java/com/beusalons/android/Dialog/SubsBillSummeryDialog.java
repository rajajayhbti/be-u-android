package com.beusalons.android.Dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.BillSummaryActivity;
import com.beusalons.android.Event.BuySubscriptionOnBillEvent;
import com.beusalons.android.Event.CancelAppointment;
import com.beusalons.android.Event.SayNoToSubscription;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.R;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Ashish Sharma on 2/23/2018.
 */

public class SubsBillSummeryDialog extends DialogFragment
{
    String titleSubs,tnC,sugg,userSubscriptionOfferTerms;
    double totalPrice,subsPrice,redemm,ussableAmount,payableAmount;

    TextView txtTotalPrice,txtSubsPrice,txtRedeemSubs,txt_tnc,txtSubTotalPrice,txt_heading1,txt_no_thanks,txt_add_membership,txt_sugg_subs;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        titleSubs=getArguments().getString("subsTitle");
        totalPrice =getArguments().getDouble("totalAmount");
        subsPrice =getArguments().getDouble("subsAmount");
        payableAmount =getArguments().getDouble("payable");
        ussableAmount=getArguments().getDouble("usable");
        redemm =getArguments().getDouble("redeem");
        tnC= getArguments().getString("term");
        sugg= getArguments().getString("sugg");
        userSubscriptionOfferTerms= getArguments().getString("userSubscriptionOfferTerms");

        View rootView = inflater.inflate(R.layout.dialog_subs_bill_summery, container,
                false);
        // getDialog().setTitle("DialogFragment Tutorial");

        txtSubTotalPrice=rootView.findViewById(R.id.txt_sub_total_price);
        txt_tnc=rootView.findViewById(R.id.txt_tnc);
        txt_no_thanks=rootView.findViewById(R.id.txt_no_thanks);
        txt_add_membership=rootView.findViewById(R.id.txt_add_membership);
        txt_heading1=rootView.findViewById(R.id.txt_heading1);
        txt_sugg_subs=rootView.findViewById(R.id.txt_sugg_subs);
        txtSubsPrice=rootView.findViewById(R.id.txt_subs_price);
        txtRedeemSubs=rootView.findViewById(R.id.txt_subs_redeem);
        txtTotalPrice=rootView.findViewById(R.id.txt_total_price);
        txt_heading1.setText(Html.fromHtml(titleSubs));
        txtSubTotalPrice.setText(""+(int)totalPrice);
        txtSubsPrice.setText((int)subsPrice+"");
        txtRedeemSubs.setText("(-)"+(int)ussableAmount);
        txt_add_membership.setText("Yes,Save " + AppConstant.CURRENCY+(int)ussableAmount);
        txt_sugg_subs.setText(sugg);
        txt_no_thanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
                EventBus.getDefault().post(new SayNoToSubscription());
            }
        });
        txt_tnc.setText(userSubscriptionOfferTerms);
        double totalprice=totalPrice+subsPrice;
        txtTotalPrice.setText(AppConstant.CURRENCY+(int)payableAmount);
        txt_add_membership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new BuySubscriptionOnBillEvent());
                //   ((BillSummaryActivity)view.getContext()).txt_buy_membership.performClick();
                getDialog().dismiss();
            }
        });
        // Do something else
        return rootView;
    }
    @Override
    public void onResume() {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        super.onResume();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Override
    public void onStart() {
        super.onStart();
    }
}
