package com.beusalons.android.Fragment;

import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.beusalons.android.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class SalonImagesFragment extends DialogFragment {

    public static final String IMAGES= "com.beusalons.android.Fragment.salon.images";


//    private int[] images= {R.drawable.carosel_1, R.drawable.carosel_2};

    private ArrayList<String> list;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);          //no action bar space
        View view= inflater.inflate(R.layout.fragment_salon_images, container, false);

        list= new ArrayList<>();
        Bundle bundle= getArguments();
        if(bundle!=null &&
                bundle.containsKey(IMAGES))
            list= bundle.getStringArrayList(IMAGES);


        LinearLayout linear_back= view.findViewById(R.id.linear_back);
        linear_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        ViewPager pager= view.findViewById(R.id.pager_);
        ImagePagerAdapter adapter= new ImagePagerAdapter(view.getContext(), list);
        pager.setAdapter(adapter);

        return view;
    }

    private class ImagePagerAdapter extends PagerAdapter{

        private Context context;
        private ArrayList<String> list;

        ImagePagerAdapter(Context context, ArrayList<String> list){
            this.context= context;
            this.list= list;
        }

        @Override
        public int getCount() {
            if(list!=null &&
                    list.size()>0)
                return list.size();
            return 0;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            ImageView img_= new ImageView(context);
            img_.setScaleType(ImageView.ScaleType.FIT_CENTER);

            try{
                Glide.with(SalonImagesFragment.this).load(list.get(position))
                        .into(img_);
            }catch (Exception e){
                e.printStackTrace();
            }
            container.addView(img_);

            return img_;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((ImageView) object);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {

            Window window = getDialog().getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFFFFF")));
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }catch (Exception e){

            e.printStackTrace();
        }

    }
}
