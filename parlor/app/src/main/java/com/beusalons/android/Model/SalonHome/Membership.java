package com.beusalons.android.Model.SalonHome;

import com.beusalons.android.Model.MemberShip.MemberShipDetails;

import java.util.List;

/**
 * Created by myMachine on 5/26/2017.
 */

public class Membership {

    private String title;
    private String cardImage;
    private String detailImage;
    private String membershipId;
    private Integer dealPercentage;
    private Integer normalPercentage;
    private List<MemberShipDetails> details = null;
    private boolean isSelectedMembership=false;
    private String line2;
    private String line1;
    private String line3;
    private String validity;
    private String noOfMembersAllowed;
    private String countDown1;
    private String countDown2;

    public String getCountDown1() {
        return countDown1;
    }

    public void setCountDown1(String countDown1) {
        this.countDown1 = countDown1;
    }

    public String getCountDown2() {
        return countDown2;
    }

    public void setCountDown2(String countDown2) {
        this.countDown2 = countDown2;
    }

    public String getLine3() {
        return line3;
    }

    public void setLine3(String line3) {
        this.line3 = line3;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getNoOfMembersAllowed() {
        return noOfMembersAllowed;
    }

    public void setNoOfMembersAllowed(String noOfMembersAllowed) {
        this.noOfMembersAllowed = noOfMembersAllowed;
    }

    public boolean isSelectedMembership() {
        return isSelectedMembership;
    }

    public void setSelectedMembership(boolean selectedMembership) {
        isSelectedMembership = selectedMembership;
    }

    public List<MemberShipDetails> getDetails() {
        return details;
    }

    public void setDetails(List<MemberShipDetails> details) {
        this.details = details;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCardImage() {
        return cardImage;
    }

    public void setCardImage(String cardImage) {
        this.cardImage = cardImage;
    }

    public String getDetailImage() {
        return detailImage;
    }

    public void setDetailImage(String detailImage) {
        this.detailImage = detailImage;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }

    public Integer getDealPercentage() {
        return dealPercentage;
    }

    public void setDealPercentage(Integer dealPercentage) {
        this.dealPercentage = dealPercentage;
    }

    public Integer getNormalPercentage() {
        return normalPercentage;
    }

    public void setNormalPercentage(Integer normalPercentage) {
        this.normalPercentage = normalPercentage;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }
}
