package com.beusalons.android.Model.DealsData;

/**
 * Created by Ajay on 6/7/2017.
 */

public class DealsDepartmentResponse {

    private Boolean success;
    private DealsData data;


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public DealsData getData() {
        return data;
    }

    public void setData(DealsData data) {
        this.data = data;
    }
}
