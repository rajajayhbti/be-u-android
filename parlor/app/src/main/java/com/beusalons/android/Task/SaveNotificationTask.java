package com.beusalons.android.Task;

import android.app.Activity;
import android.util.Log;

import com.beusalons.android.Event.NotificationEvent;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.Notifications.NotificationDetail;
import com.beusalons.android.Model.Notifications.Notifications;
import com.snappydb.DB;
import com.snappydb.DBFactory;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 4/4/2017.
 */

public class SaveNotificationTask implements Runnable {

    private Activity activity;
    private NotificationDetail notificationDetail;

    public SaveNotificationTask(Activity activity, NotificationDetail notificationDetail){

        this.notificationDetail= notificationDetail;
        this.activity= activity;
    }


    @Override
    public void run() {

        try{

            DB snappyDB=null;
            snappyDB= DBFactory.open(activity);

            Notifications detail= new Notifications();

            if(snappyDB.exists(AppConstant.NOTIFICATION_DB)){
                Log.i("mainyahatukaha", " existing hai");
                detail= snappyDB.getObject(AppConstant.NOTIFICATION_DB, Notifications.class);

                boolean isNew= true;
                for(int i=0; i<detail.getList().size(); i++){

                    if(detail.getList().get(i).getNotificationId()
                            .equalsIgnoreCase(notificationDetail.getNotificationId())){
                        Log.i("mainyahatukaha", " new ke loop mai hai");
                        isNew=false;                 //match kar gaya so no addition
                    }
                }

                if(isNew){

                    detail.getList().add(notificationDetail);
                }



            }else{
                Log.i("mainyahatukaha", " naya hai");

                List<NotificationDetail> add= new ArrayList<>();
                add.add(notificationDetail);
                detail.setList(add);
            }
            snappyDB.put(AppConstant.NOTIFICATION_DB, detail);
            snappyDB.close();
            EventBus.getDefault().post(new NotificationEvent());


        }catch (Exception e){
            e.printStackTrace();

        }

    }







}
