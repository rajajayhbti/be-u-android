package com.beusalons.android.Fragment.ServiceFragments;

import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.beusalons.android.Event.NewServicesEvent.UpgradeEvent_;
import com.beusalons.android.Fragment.DialogFragmentServices;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.UserCart.PackageService;
import com.beusalons.android.Model.newServiceDeals.NewCombo.Selector;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Upgrade;
import com.beusalons.android.R;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 6/9/2017.
 */

public class UpgradeBottomSheet extends BottomSheetDialogFragment {

    public static final String SERVICE_NAME= "service_name";
    private Upgrade upgrade;

    private PackageService packageService;

    private TextView txt_price, txt_menu_price, txt_save_per;

    private int brand_index= 0, product_index= 0, service_index= 0;         //yehi toh hai bhai jannat ka darwaza

    private AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        logger = AppEventsLogger.newLogger(getActivity());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());

        Bundle bundle= getArguments();
        String service_name= "";
        if (bundle != null && bundle.containsKey("upgrade")) {
            upgrade= new Gson().fromJson(bundle.getString("upgrade"), Upgrade.class);
            service_name= bundle.getString(SERVICE_NAME);
        }

        View view= inflater.inflate(R.layout.fragment_services_specific_upgrade_bottomsheet, container, false);

        final TextView txt_customize, txt_name_ , txt_description_, txt_select;
        final LinearLayout linear_cancel, linear_done, linear_info,
                linear_price, linear_;
        linear_done= (LinearLayout)view.findViewById(R.id.linear_done);     //yehi apply bhi hai
        linear_price= (LinearLayout)view.findViewById(R.id.linear_price);
        linear_cancel= (LinearLayout)view.findViewById(R.id.linear_cancel);
        linear_= (LinearLayout)view.findViewById(R.id.linear_);


        txt_select= (TextView)view.findViewById(R.id.txt_select);
        txt_select.setText("Please Select");
        txt_customize= (TextView)view.findViewById(R.id.txt_customize);
        txt_customize.setText("Upgrade Your Service");


        txt_name_=(TextView) view.findViewById(R.id.txt_name_);
        txt_name_.setText(upgrade.getName());
        txt_description_= (TextView)view.findViewById(R.id.txt_description_);
        txt_description_.setText(upgrade.getDetail());


        txt_price= (TextView)view.findViewById(R.id.txt_price);
        txt_menu_price= (TextView)view.findViewById(R.id.txt_menu_price);
        txt_save_per= (TextView)view.findViewById(R.id.txt_save_per);

        txt_menu_price.setVisibility(View.GONE);

        txt_save_per.setText("Save "+upgrade.getSave()+"%");
        txt_save_per.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        txt_save_per.setBackgroundResource(R.drawable.discount_seletor);

        //---------------------------------------------------------------------------+++++++++++++++++++++++++++++++++++
        packageService= new PackageService();

        packageService.setName(upgrade.getName());
        packageService.setDescription(upgrade.getDetail());

        packageService.setService_code(upgrade.getSelectors().get(0).getBrands().
                get(brand_index).getProducts().get(product_index).getServices().get(service_index).getServiceCode());
        packageService.setBrand_name(upgrade.getSelectors().get(0).getBrands().get(0).getBrandName());
        packageService.setProduct_name(upgrade.getSelectors().get(0).getBrands()
                .get(brand_index).getProducts().get(0).getName()==null?"":upgrade.getSelectors().get(0).getBrands()
                .get(brand_index).getProducts().get(0).getProductName());
        packageService.setBrand_id(upgrade.getSelectors().get(0).getBrands().get(0).getBrandId());
        packageService.setProduct_id(upgrade.getSelectors().get(0).getBrands()
                .get(brand_index).getProducts().get(0).getName()==null?"":upgrade.getSelectors().get(0).getBrands()
                .get(brand_index).getProducts().get(0).getProductId());
        packageService.setService_id(upgrade.getSelectors().get(0).getBrands().get(0).
                getProducts().get(0).getServices().get(0).getServiceId());
        packageService.setPrice(upgrade.getSelectors().get(0).getBrands().get(0).
                getProducts().get(0).getServices().get(0).getPrice());
        packageService.setMenu_price(upgrade.getSelectors().get(0).getBrands().get(0).
                getProducts().get(0).getServices().get(0).getMenuPrice());


        packageService.setType(upgrade.getSelectors().get(0).getBrands().get(0).
                getProducts().get(0).getServices().get(0).getDealId()==null?"service":
                upgrade.getSelectors().get(0).getBrands().get(0).
                        getProducts().get(0).getServices().get(0).getDealType());

        //setting brand ka stuff initially
        upgrade.getSelectors().get(0).getBrands().get(0).setCheck(true);


        if(upgrade.getSelectors().get(0).getBrands().get(0).getProducts().get(0).getName()==null){

            Log.i("upgrademai", "main product name == null mai");
//            txt_product.setText("-");
//            linear_product.setEnabled(false);
//            linear_product_.setVisibility(View.GONE);


        }else{

            Log.i("upgrademai", "main product name == null naiiiiii");

            upgrade.getSelectors().get(0).getBrands().get(0).getProducts().get(0).setCheck(true);

        }


        upgrade.getSelectors().get(0).getBrands().get(0).getProducts().get(0).getServices().get(0).setCheck(true);

        //--------------------------------------------------------------------------------------++++++++++++++++++++++++++

        linear_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logEditServiceUpgradeDoneEvent();
                logEditServiceUpgradeDoneFireBaseEvent();

                saveToCart();
                dismiss();
            }
        });

        linear_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        setView(linear_);

        return view;
    }

    private void setView(LinearLayout linear_){

        LinearLayout linear_stuff= (LinearLayout) LayoutInflater.from(linear_.getContext()).
                inflate(R.layout.bottom_sheet_services, null, false);
        TextView txt_name= linear_stuff.findViewById(R.id.txt_name);
        final TextView txt_des= linear_stuff.findViewById(R.id.txt_description);
        TextView txt_menu_price_= linear_stuff.findViewById(R.id.txt_menu_price);
        TextView txt_save_per_= linear_stuff.findViewById(R.id.txt_save_per);
        TextView txt_price_= linear_stuff.findViewById(R.id.txt_price);
        LinearLayout linear_change= linear_stuff.findViewById(R.id.linear_change);
        LinearLayout linear_show= linear_stuff.findViewById(R.id.linear_show);

        txt_name.setText(upgrade.getSelectors().get(0).getTitle());
        txt_price_.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                packageService.getPrice()));

        if(packageService.getPrice()<packageService.getMenu_price()){

            txt_menu_price_.setVisibility(View.VISIBLE);
            txt_save_per_.setVisibility(View.VISIBLE);

            //with tax
            int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                    packageService.getMenu_price());

            txt_menu_price_.setText(AppConstant.CURRENCY+menu_price_tax);
            txt_menu_price_.setPaintFlags( txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            txt_save_per_.setText(AppConstant.SAVE+" "+
                    (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                            packageService.getPrice())*100)/
                            menu_price_tax))+"%");
            txt_save_per_.setBackgroundResource(R.drawable.discount_seletor);
        }else{

            txt_menu_price_.setVisibility(View.GONE);
            txt_save_per_.setVisibility(View.GONE);
        }

        String brand_name= packageService.getBrand_name()==null ||
                packageService.getBrand_name().equalsIgnoreCase("")?"":packageService.getBrand_name();
        String product_name= packageService.getProduct_name()==null ||
                packageService.getProduct_name().equalsIgnoreCase("")?"":packageService.getProduct_name();

        if(brand_name.equalsIgnoreCase("") &&
                product_name.equalsIgnoreCase("")){

//                linear_des.setVisibility(View.GONE);
            txt_des.setText("");
        }
        else{

            if(!product_name.equalsIgnoreCase(""))
                brand_name= brand_name + " - ";

//                linear_des.setVisibility(View.VISIBLE);
            txt_des.setText(brand_name+ product_name);
        }

        linear_show.setVisibility(View.VISIBLE);
        linear_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                DialogFragmentServices fragment= new DialogFragmentServices();
                Bundle bundle= new Bundle();
                bundle.putString(DialogFragmentServices.SERVICE_DATA,
                        new Gson().toJson(upgrade.getSelectors().get(0), Selector.class));
                bundle.putString(DialogFragmentServices.DATA,
                        new Gson().toJson(packageService, PackageService.class));

                fragment.setArguments(bundle);
                fragment.show(getActivity().getFragmentManager(),
                        DialogFragmentServices.DIALOG);
                fragment.setListener(new DialogFragmentServices.FragmentListener() {
                    @Override
                    public void onClick(PackageService package_service) {
                        Log.i("humkahahai", "int the gharh");
                        packageService= package_service;

                        String brand_name= packageService.getBrand_name()==null ||
                                packageService.getBrand_name().equalsIgnoreCase("")?"":packageService.getBrand_name();
                        String product_name= packageService.getProduct_name()==null ||
                                packageService.getProduct_name().equalsIgnoreCase("")?"":packageService.getProduct_name();

                        if(brand_name.equalsIgnoreCase("") &&
                                product_name.equalsIgnoreCase(""))
                            txt_des.setText("");
                        else
                        if(!product_name.equalsIgnoreCase(""))
                            brand_name= brand_name + " - ";

                        txt_des.setText(brand_name+ product_name);
                        updatePrice();

                    }
                });
            }
        });
        linear_.addView(linear_stuff);


        updatePrice();
    }


    private void saveToCart(){

        UpgradeEvent_ event= new UpgradeEvent_();

        event.setService_name(upgrade.getSelectors().get(0).getBrands().
                get(brand_index).getProducts().get(product_index).getServices().get(service_index).getName());
        event.setPrice(upgrade.getSelectors().get(0).getBrands().
                get(brand_index).getProducts().get(product_index).getServices().get(service_index).getPrice());
        event.setMenu_price(packageService.getMenu_price());
        event.setType(packageService.getType());

        String service_deal_id= upgrade.getSelectors().get(0).getBrands().
                get(brand_index).getProducts().get(product_index).getServices().get(service_index).getDealId()==null?
                upgrade.getSelectors().get(0).getBrands().
                        get(brand_index).getProducts().get(product_index).getServices().get(service_index).getServiceId():
                upgrade.getSelectors().get(0).getBrands().
                        get(brand_index).getProducts().get(product_index).getServices().get(service_index).getDealId();
        String primary_key= packageService.getService_code()+ packageService.getBrand_id()+packageService.getProduct_id()+packageService.getService_id();

        event.setService_deal_id(service_deal_id);
        event.setService_id_(upgrade.getServiceId());

        String brand_name, product_name, service_name;
        brand_name= upgrade.getSelectors().get(0).getBrands().
                get(brand_index).getBrandName()==null?"":upgrade.getSelectors().get(0).getBrands().
                get(brand_index).getBrandName()+" | ";
        product_name= upgrade.getSelectors().get(0).getBrands().
                get(brand_index).getProducts().get(product_index).getName()==null?"":upgrade.getSelectors().get(0).getBrands().
                get(brand_index).getProducts().get(product_index).getName()+ " | ";
//        service_name= upgrade.getSelectors().get(0).getBrands().get(brand_index).getProducts()
//                .get(product_index).getServices().get(service_index).getName()==null?"":
//                upgrade.getSelectors().get(0).getBrands().get(brand_index).getProducts()
//                        .get(product_index).getServices().get(service_index).getName();

        event.setDescription(brand_name+product_name);
        event.setPrimary_key(primary_key);

        event.setService_code(packageService.getService_code());

        event.setBrand_id(packageService.getBrand_id());
        event.setProduct_id(packageService.getProduct_id());
        event.setService_id_(packageService.getService_id());

        EventBus.getDefault().post(event);
    }

    public void updatePrice(){

        txt_price.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                packageService.getPrice()));

        if(packageService.getPrice()<packageService.getMenu_price()){

            txt_menu_price.setVisibility(View.VISIBLE);
            txt_save_per.setVisibility(View.VISIBLE);

            txt_menu_price.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                    packageService.getMenu_price()));
            txt_menu_price.setPaintFlags(txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            txt_save_per.setText(AppConstant.SAVE+" "+
                    (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                            packageService.getPrice())*100)
                            /(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                            packageService.getMenu_price()))+"%"));
            txt_save_per.setBackgroundResource(R.drawable.discount_seletor);

        }else{

            txt_menu_price.setVisibility(View.GONE);
            txt_save_per.setVisibility(View.GONE);
        }

    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logEditServiceUpgradeDoneEvent () {
        Log.e("EditServiceUpgradeDone","fine");
        logger.logEvent(AppConstant.EditServiceUpgradeDone);
    }
    public void logEditServiceUpgradeDoneFireBaseEvent () {
        Log.e("EditUpgradeDonefirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.EditServiceUpgradeDone,bundle);
    }
}
