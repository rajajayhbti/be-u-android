package com.beusalons.android.Model.HomeFragmentModel;

import java.util.List;

/**
 * Created by myMachine on 9/18/2017.
 */

public class SelectedDeals {

    private String dealType;
    private String name;
    private String brandId;
    private String productId;
    private int dealId;
    private List<Integer> serviceCodes;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getDealId() {
        return dealId;
    }

    public void setDealId(int dealId) {
        this.dealId = dealId;
    }

    public List<Integer> getServiceCodes() {
        return serviceCodes;
    }

    public void setServiceCodes(List<Integer> serviceCodes) {
        this.serviceCodes = serviceCodes;
    }
}
