package com.beusalons.android.Model.newServiceDeals.ServiceByDepartment;

/**
 * Created by myMachine on 5/30/2017.
 */

public class Product {

    private double ratio;
    private String name;
    private String productId;
    private Double dealRatio;

    private String type;                    //deal type or service type

    private double price;
    private int menu_price;
    private int save_per;

    private boolean isCheck= false;         //radio btn ka check

    private String service_deal_id;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getRatio() {
        return ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Double getDealRatio() {
        return dealRatio;
    }

    public void setDealRatio(Double dealRatio) {
        this.dealRatio = dealRatio;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getMenu_price() {
        return menu_price;
    }

    public void setMenu_price(int menu_price) {
        this.menu_price = menu_price;
    }

    public int getSave_per() {
        return save_per;
    }

    public void setSave_per(int save_per) {
        this.save_per = save_per;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public String getService_deal_id() {
        return service_deal_id;
    }

    public void setService_deal_id(String service_deal_id) {
        this.service_deal_id = service_deal_id;
    }
}
