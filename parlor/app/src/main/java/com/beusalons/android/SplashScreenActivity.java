package com.beusalons.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.beusalons.android.Event.RetryEvent;
import com.beusalons.android.Fragment.ForcedUpdateFragment;
import com.beusalons.android.Fragment.NoInternetFragment;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Helper.CheckConnectionGoogle;
import com.beusalons.android.Model.VersionCheck.VersionCheckResponse;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.facebook.appevents.AppEventsLogger;
import com.snappydb.DB;
import com.snappydb.DBFactory;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.LinkProperties;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashScreenActivity extends AppCompatActivity {

    private static final int DELAY_TIME= 0;
    Activity activity;
    private String fromCampaign="";
    private AppEventsLogger logger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__splash_screen);
        activity = this;

        logger = AppEventsLogger.newLogger(activity);


        Bundle extras = getIntent().getExtras();
        String value;
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
            public boolean verify(String hostname, SSLSession arg1) {
                Log.d("HTTPS", hostname + ":" + arg1);
                return hostname.equalsIgnoreCase("graph.facebook.com") ||
                        hostname.equalsIgnoreCase("api.branch.io") ||
                        hostname.equalsIgnoreCase("t.appsflyer.com") ||
                        hostname.equalsIgnoreCase(" api.crashlytics.com")
                        || hostname.equalsIgnoreCase("res.cloudinary.com")
                        || hostname.equalsIgnoreCase("stats.appsflyer.com")
                        || hostname.equalsIgnoreCase("chat.zendesk.com")
                        || hostname.equalsIgnoreCase("organicthemes.com")
                        || hostname.equalsIgnoreCase("scontent.xx.fbcdn.net")
                        || hostname.equalsIgnoreCase("lh5.googleusercontent.com")
                        || hostname.equalsIgnoreCase("*.zopim.com")
                        || hostname.equalsIgnoreCase("api.rollbar.com")
                        || hostname.equalsIgnoreCase(" lh6.googleusercontent.com")
                        || hostname.equalsIgnoreCase("*.zendesk.com");
            }});


        int type;
        String type_String;
        if (extras!=null  && extras.containsKey("type")) {
            value = extras.getString("appointmentId");
            type= extras.getInt("type");
            type_String=extras.getString("type_String");
            Log.i("investiga", "i'm in the splash screen intent now"+value+ "  "+ type);
            logNotificationClickedEvent(""+type_String,currentTime());
            Intent intent1;
            if(type==0){            //0 ka matlab user ko payment page dikhana hai

                intent1= new Intent(SplashScreenActivity.this, OnlinePaymentActivity.class);
                intent1.putExtra("apptId", value);
            }else if (type==8) {
                intent1= new Intent(SplashScreenActivity.this, SalonPageActivity.class);
                String parlorId= extras.getString("parlorId");
                intent1.putExtra("parlorId", parlorId);
            }else if (type==18) {
                intent1= new Intent(SplashScreenActivity.this, SubscriptionActivity.class);

            }else{ //jab data toh hai par koi type ka nahi hai

                intent1= new Intent(SplashScreenActivity.this, MainActivity.class);
                intent1.putExtra("apptId", value);
            }

            //adding the stuff
            intent1.putExtra("type", type);
            startActivity(intent1);
            finish();
        }else{                  //bundle mai data jab nahi hai


//           enterApp();
            Runnable runnable= new Runnable() {
                @Override
                public void run() {

                    Intent intent= null;
                    if (BeuSalonsSharedPrefrence.isLogin()){
                        Log.e("investiga", "i'm in the islogin");

                        intent = BeuSalonsSharedPrefrence.getLatitude() == null ? new Intent(SplashScreenActivity.this, FetchingLocationActivity.class)
                                : new Intent(SplashScreenActivity.this, MainActivity.class);



                    }else{
                        Log.e("investiga", "i'm in the is not login");

                        DB snappyDB = null;
                        try {

                            snappyDB = DBFactory.open(activity);
                            snappyDB.destroy();

                            if(snappyDB.isOpen())
                                snappyDB.close();
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        BeuSalonsSharedPrefrence.clearData();
                        intent= new Intent(SplashScreenActivity.this, LoginActivity.class);
                    }
                    startActivity(intent);
                    finish();
                }
            };
            new Handler().postDelayed(runnable, DELAY_TIME);

        }
    }


    private void enterApp(){

        if (CheckConnectionGoogle.isConnected(this)) {      //agar connected to internet

            Log.i("checkinternet", "i'm in on connected");
            checkVersion();
        }else{
            Log.i("checkinternet", "i'm in on not connected");

            NoInternetFragment fragment= new NoInternetFragment();
            fragment.show(getSupportFragmentManager(), "noInternet");
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(RetryEvent event) {
        Log.i("mainactivity", "i'm in cartmodel event bus notification");
        enterApp();
    }

    private void checkVersion(){

        int versionCode = BuildConfig.VERSION_CODE;
        Log.i("versioncode", "value in version code: "+ versionCode);

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<VersionCheckResponse> call= apiInterface.checkVersion(""+versionCode);
        call.enqueue(new Callback<VersionCheckResponse>() {
            @Override
            public void onResponse(Call<VersionCheckResponse> call, Response<VersionCheckResponse> response) {

                if(response.isSuccessful()){
                    if(response.body().isSuccess()){

                        if(response.body().getData().isForcedUpdate()){ //forced update

                            ForcedUpdateFragment fragment= new ForcedUpdateFragment();
                            fragment.show(getSupportFragmentManager(), "forcedUpdate");
                        }else{ //recommended update



                        }
                    }else{              //koi update nai hai

                        Runnable runnable= new Runnable() {
                            @Override
                            public void run() {

                                SharedPreferences preferences= getSharedPreferences("userDetails", Context.MODE_PRIVATE);
                                Intent intent= null;
                                if (BeuSalonsSharedPrefrence.isLogin()){
                                    Log.e("investiga", "i'm in the islogin");
                                    intent = BeuSalonsSharedPrefrence.getLatitude() == null ? new Intent(SplashScreenActivity.this, FetchingLocationActivity.class)
                                            : new Intent(SplashScreenActivity.this, MainActivity.class);

                                }else{
                                    Log.e("investiga", "i'm in the is not login");

                                    DB snappyDB = null;
                                    try {
                                        snappyDB = DBFactory.open(activity);
                                        if (snappyDB.exists("cartV1") || snappyDB.exists("cartV2")) {
                                            Log.i("investiga", "i'm in the cart delete stuff");
                                            snappyDB.destroy();
                                        }
                                        snappyDB.close();
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                    BeuSalonsSharedPrefrence.clearData();

                                    preferences.edit().clear().apply();
                                    intent= new Intent(SplashScreenActivity.this, LoginActivity.class);
                                }
                                startActivity(intent);
                                finish();
                            }
                        };
                        new Handler().postDelayed(runnable, DELAY_TIME);
                    }
                }else{


                }


            }

            @Override
            public void onFailure(Call<VersionCheckResponse> call, Throwable t) {


            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAndRemoveTask();
        }else{
            this.finishAffinity();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        Branch branch = Branch.getInstance();

        branch.initSession(new Branch.BranchUniversalReferralInitListener() {
            @Override
            public void onInitFinished(BranchUniversalObject branchUniversalObject, LinkProperties linkProperties, BranchError error) {
                if (error == null &&linkProperties!=null) {
                    // params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
                    // params will be empty if no data found
                    // ... insert custom logic here ...
                    //     Toast.makeText(getApplicationContext(), linkProperties.getCampaign(),Toast.LENGTH_LONG).show();
                    //   fromCampaign=linkProperties.getCampaign();
                    //  linkProperties.getControlParams().get("$marketing_title");
                    if (linkProperties.getControlParams()!=null&& linkProperties.getControlParams().get("$marketing_title").trim().equalsIgnoreCase("Deal Home Page")){
                        Intent intent1=new Intent(SplashScreenActivity.this,MainActivity.class);
                        intent1.putExtra("type", 1);
                        startActivity(intent1);
                        finish();
                        fromCampaign="deal";
                    }else if(linkProperties.getControlParams().get("$marketing_title").trim().equalsIgnoreCase("freebie")){
                        fromCampaign="freebie";
                    }
                } else {
                    //    Log.i("MyApp", error.getMessage());
                }
            }
        }, this.getIntent().getData(), this);

        // listener (within Main Activity's onStart)
        Branch.getInstance().initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    Log.e("BRANCH SDK", referringParams.toString());
                    Boolean non_branch_link=false;

                    try{
                        non_branch_link=referringParams.getBoolean("+clicked_branch_link");
                    }catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (non_branch_link){
                        try{
                            fromCampaign=referringParams.getString("page");
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                        if (fromCampaign.length()>0 && fromCampaign.trim().equalsIgnoreCase("dealHomePage")){
                            Intent  intent=new Intent(SplashScreenActivity.this,MainActivity.class);
                            intent.putExtra("type", 1);
                            startActivity(intent);

                        }else if (fromCampaign.length()>0 && fromCampaign.trim().equalsIgnoreCase("dealServices")){
                            Intent intent1= new Intent(SplashScreenActivity.this, DealsServicesActivity.class);
                            String departmentId= null;
                            String gender= null;
                            String index= null;
                            try {
                                departmentId = referringParams.getString("departmentId");
//                                String department_name= referringParams.getString("name");
                                gender= referringParams.getString("gender");
                                index= referringParams.getString("index");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            intent1.putExtra("department_id", departmentId);
//                            intent1.putExtra("department_name", department_name);
                            intent1.putExtra("gender", gender);
                            intent1.putExtra("position", Integer.valueOf(index));

                            startActivity(intent1);

                        }else if (fromCampaign.length()>0 && fromCampaign.trim().equalsIgnoreCase("freebie")){
                            Intent intent=new Intent(SplashScreenActivity.this,MainActivity.class);
                            intent.putExtra("type", 3);
                            startActivity(intent);
                        }else if (fromCampaign.length()>0 && fromCampaign.trim().equalsIgnoreCase("subscription")){
                            Intent intent=new Intent(SplashScreenActivity.this,SubscriptionActivity.class);
                            try {
                                String referCode=referringParams.getString("subscriptionCode");
                                BeuSalonsSharedPrefrence.setSubsRefer(referCode);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            startActivity(intent);
                        }

                    }else{

                        Uri uri = null;
                        try {
                            uri = Uri.parse(referringParams.getString("+non_branch_link"));
                            fromCampaign=referringParams.getString("page");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (uri!=null && uri.getQueryParameter("page").length()>0) {
                            String page = uri.getQueryParameter("page");

                            //   String serviceId = uri.getQueryParameter("serviceId");
                            //   Log.e("serviceId", serviceId + " page " + page + " parlorId " + parlorId);
                            if (page.equalsIgnoreCase("pay")) {
                                String appointMentID = uri.getQueryParameter("appt");
                                Intent intent1 = new Intent(SplashScreenActivity.this, OnlinePaymentActivity.class);
                                intent1.putExtra("apptId", appointMentID);
                                intent1.putExtra("type", 0);
                                startActivity(intent1);

                            } else if (page.equalsIgnoreCase("SalonDetail")) {
                                Intent intent1= new Intent(SplashScreenActivity.this, SalonPageActivity.class);
                                String parlorId= uri.getQueryParameter("parlorId");
                                intent1.putExtra("parlorId", parlorId);
                                intent1.putExtra("type", 8);

                                startActivity(intent1);

                            }else if (page.equalsIgnoreCase("subscription")) {
                                Intent intent1= new Intent(SplashScreenActivity.this, SalonPageActivity.class);
                                String referCode= uri.getQueryParameter("parlorId");
                                intent1.putExtra("type", 15);

                                startActivity(intent1);

                            }else if (page.equalsIgnoreCase("SelectService")) {
                                Intent intent1= new Intent(SplashScreenActivity.this, SalonPageActivity.class);
                                String parlorId= uri.getQueryParameter("parlorId");
                                String departmentId= uri.getQueryParameter("departmentId");
                                String gender= uri.getQueryParameter("gender");
                                String index= uri.getQueryParameter("index");
                                String departmentName= uri.getQueryParameter("name");
                                intent1.putExtra("departmentId", departmentId);
                                intent1.putExtra("parlorId", parlorId);
                                intent1.putExtra("departmentName", departmentName);
                                intent1.putExtra("gender", gender);
                                intent1.putExtra("index", Integer.valueOf(index));

                                startActivity(intent1);

                            }else if (page.equalsIgnoreCase("dealServices")) {
                                Intent intent1= new Intent(SplashScreenActivity.this, DealsServicesActivity.class);
                                String departmentId= uri.getQueryParameter("departmentId");
                                String department_name= uri.getQueryParameter("name");
                                String gender= uri.getQueryParameter("gender");
                                String index= uri.getQueryParameter("index");
                                intent1.putExtra("department_id", departmentId);
                                intent1.putExtra("department_name", department_name);
                                intent1.putExtra("gender", gender);
                                intent1.putExtra("position", Integer.valueOf(index));

                                startActivity(intent1);

                            }

                        }
                    }
/*

                    Uri uri = null;
                    try {
                        uri = Uri.parse(referringParams.getString("+non_branch_link"));
                        fromCampaign=referringParams.getString("page");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (uri!=null && uri.getQueryParameter("page").length()>0) {
                        String page = uri.getQueryParameter("page");
                        String parlorId = uri.getQueryParameter("parlorId");
                        String serviceId = uri.getQueryParameter("serviceId");
                        Log.e("serviceId", serviceId + " page " + page + " parlorId " + parlorId);
                        if (page.equalsIgnoreCase("pay")) {
                            String appointMentID = uri.getQueryParameter("appt");
                            Intent intent1 = new Intent(SplashScreenActivity.this, OnlinePaymentActivity.class);
                            intent1.putExtra("apptId", appointMentID);
                            intent1.putExtra("type", 0);
                            startActivity(intent1);

                        }
                    }else if (fromCampaign.length()>0 && fromCampaign.trim().equalsIgnoreCase("Deal Home Page")){
                        Intent intent1=new Intent(SplashScreenActivity.this,MainActivity.class);
                        intent1.putExtra("type", 1);
                        startActivity(intent1);
                        finish();
                    }
*/

                }  else {
                    Log.i("BRANCH SDK", error.getMessage());
                }
            }
        }, this.getIntent().getData(), this);
    }



    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }


    public void logNotificationClickedEvent (String type, String notificationDate) {
        Bundle params = new Bundle();
        params.putString("type", type+"-"+notificationDate);
        //params.putString(AppConstant.NotificationDate, notificationDate);
        logger.logEvent(AppConstant.NotificationClicked, params);
    }

    public String currentTime(){

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

}
