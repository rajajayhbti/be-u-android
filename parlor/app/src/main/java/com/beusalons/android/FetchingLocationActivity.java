package com.beusalons.android;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.GeoCode.GeoCodingResponse;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;



public class FetchingLocationActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.type_your_location)
    Button typeYourLocation;
    @BindView(R.id.progress)
    ProgressBar progressWheel;
    Activity activity;

    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 200;
    private static final int GPS_PERMISSION = 100;                  //marshmallow permission
    private static final int REQUEST_CHECK_SETTINGS = 300;               //this is for turning the gps from the dialog


    private GoogleApiClient mGoogleApiClient = null;
    private Location mLastLocation;
    private Retrofit retrofit;

    private int count = 0;

    private int retry = 0;
    TextView txtViewActionBarName;
    ImageView imgViewBack;
    private MyCountDownTimer myCountDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetching_location);
        ButterKnife.bind(this);
        setToolBar();
        activity = this;
        myCountDownTimer = new MyCountDownTimer(20000, 1000);
        myCountDownTimer.start();
        //new ToolbarHelper(getSupportActionBar(), getLayoutInflater(), false, this);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }


        typeYourLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openLocationIntent();
            }
        });


    }

    private void setToolBar() {


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getResources().getString(R.string.fetching_location_by_gps));
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            Utility.applyFontForToolbarTitle(this);

        }


    }

    private void requestContactPermission() {

        int hasContactPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (hasContactPermission != PackageManager.PERMISSION_GRANTED) {
            Log.i("permissions", "i'm in not onReqest permis granterd");
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, GPS_PERMISSION);
        } else {
            Log.i("permissions", "i'm in already onReqest permis granterd");
            checkLocation();

//            Toast.makeText(this, "Contact Permission is already granted", Toast.LENGTH_LONG).show();
        }
    }

    protected void openLocationIntent() {
        try {
            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                    .setTypeFilter(AutocompleteFilter.TYPE_FILTER_REGIONS)
                    .build();

            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(typeFilter)
                         //   .setBoundsBias(AppConstant.PLACE_BOUNDS_DELHI)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case GPS_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("permissions", "i'm in onReqest permis granterd");
                    checkLocation();


                } else {
                    Log.i("permissions", "i'm in permission denied onReqest permis granterd");

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    BeuSalonsSharedPrefrence.saveAddress(FetchingLocationActivity.this, "New Delhi", "New Delhi", "28.6139",
                            "77.2090");
                    activity.startActivity(new Intent(activity, MainActivity.class));
                    Toast.makeText(getApplicationContext(),"Your Current Location Is Set As New Delhi. You Can Change Your Location By Clicking The Bar At The Top Of The Page.",Toast.LENGTH_SHORT).show();
                    activity.finish();
                }


            }
        }

    }

    private void checkLocation() {

        // Create an instance of GoogleAPIClient.


        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        final LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.
                checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i("investigatingg", "i'm in succes of location setting");       //idhar location get kar

                        mGoogleApiClient = new GoogleApiClient.Builder(FetchingLocationActivity.this)
                                .addConnectionCallbacks(FetchingLocationActivity.this)
                                .addOnConnectionFailedListener(FetchingLocationActivity.this)
                                .addApi(LocationServices.API)
                                .build();
                        mGoogleApiClient.connect();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i("investigatingg", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            if (count < 2) {

                                status.startResolutionForResult(FetchingLocationActivity.this, REQUEST_CHECK_SETTINGS);
                                count++;
                            } else {
                                typeYourLocation.setFocusable(true);
                            }
                        } catch (IntentSender.SendIntentException e) {

                            Log.i("investigatingg", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i("investigatingg", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                //  Log.e("address ashish",  place.getAddress().charAt(0)+"");
//                new ConstantHelper().createLocation(activity, String.valueOf(place.getLatLng().latitude),
//                        String.valueOf(place.getLatLng().longitude), place.getAddress().toString());
//                new ConstantHelper().initLocation(activity);
                BeuSalonsSharedPrefrence.saveAddress(FetchingLocationActivity.this, place.getName().toString(), place.getAddress().toString(), String.valueOf(place.getLatLng().latitude),
                        String.valueOf(place.getLatLng().longitude));
                activity.startActivity(new Intent(activity, MainActivity.class));
                activity.finish();
                Log.e("investigatingg", place.getName().toString());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);

                // TODO: Handle the error.
                Log.e("investigatingg", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                Log.e("investigatingg", "so the user cancelled the autocomplete acitivty");
            }
        } else if (requestCode == REQUEST_CHECK_SETTINGS) {

            Log.i("investigatingg", "I'm in the reuqest code : request check setting");     //aur idhar location get kar
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();


        }
    }

    @Override
    protected void onStart() {
        if (mGoogleApiClient != null) {

            mGoogleApiClient.connect();
        }
        super.onStart();
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null) {

            if (mGoogleApiClient.isConnected()) {

                mGoogleApiClient.disconnect();
            }

        }

        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        Log.i("investigatingg", "i'm in on connected of google api client");


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            requestContactPermission();
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

//        Log.i("investigatingg", "value in locat" + mLastLocation.getLongitude()+ "  "+ mLastLocation.getLatitude());

        if(mLastLocation==null){

            if(Build.VERSION.SDK_INT < 23){

                checkLocation();
            }else {
                requestContactPermission();
            }
        }else{
            handleNewLocation(mLastLocation);
        }



    }

    @Override
    public void onConnectionSuspended(int i) {

        Log.i("investigatingg", "i'm in onConnectionSuspended of google api client");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Log.i("investigatingg", "i'm in onConnectionSuspended of google api client");
    }

    public void handleNewLocation(Location lastLocation) {

        try {

            final double lat = lastLocation.getLatitude(), lon = lastLocation.getLongitude();

            retrofit = ServiceGenerator.getClient();
            ApiInterface apiInterface = retrofit.create(ApiInterface.class);

            Call<GeoCodingResponse> call = apiInterface.getAddress(
                    "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon + "&sensor=true"+"&key=AIzaSyDwsi-O7uCC9IgO6Jx8m8wMWoLbdfyOLT4");
            call.enqueue(new Callback<GeoCodingResponse>() {
                @Override
                public void onResponse(Call<GeoCodingResponse> call, Response<GeoCodingResponse> response) {

                    if(response.isSuccessful()) {

                        try {

                            if(response.body().getResults().size()>0){

                                String address = response.body().getResults().get(0).getFormatted_address();
                                String localtyAddress = response.body().getResults().get(0).getAddress_components().get(1).getLong_name();
                                if (address != null && address != "") {

                                    BeuSalonsSharedPrefrence.saveAddress(FetchingLocationActivity.this, localtyAddress, address, String.valueOf(lat), String.valueOf(lon));
                                    activity.startActivity(new Intent(activity, MainActivity.class));

                                    if(activity!=null)
                                        activity.finish();
                                }
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<GeoCodingResponse> call, Throwable t) {

                    retry++;
                    if(retry<2){
                        handleNewLocation(mLastLocation);
                    }

                }
            });

        }catch (Exception e){
            Log.e("investigatingg", e.getMessage());
        }

    }




    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {

            int progress = (int) (millisUntilFinished/1000);

            // progressBar.setProgress(progressBar.getMax()-progress);
        }

        @Override
        public void onFinish() {
            if (BeuSalonsSharedPrefrence.getLatitude()!=null&&BeuSalonsSharedPrefrence.getLatitude().length()>0 &&BeuSalonsSharedPrefrence.getLongitude()!=null&& BeuSalonsSharedPrefrence.getLongitude().length()>0){
                finish();
            }else{
                BeuSalonsSharedPrefrence.saveAddress(FetchingLocationActivity.this, "New Delhi", "New Delhi", "28.6139",
                        "77.2090");
                Toast.makeText(getApplicationContext(),"Your Current Location Is Set As New Delhi. You Can Change Your Location By Clicking The Bar At The Top Of The Page.",Toast.LENGTH_SHORT).show();
                activity.startActivity(new Intent(activity, MainActivity.class));
                activity.finish();
            }


        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myCountDownTimer.cancel();
    }
}