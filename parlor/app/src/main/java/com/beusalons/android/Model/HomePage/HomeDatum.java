package com.beusalons.android.Model.HomePage;

import java.util.List;

/**
 * Created by myMachine on 23-Jan-18.
 */

public class HomeDatum{

    public static final int PARLOR=1;
    public static final int SUBSCRIPTION=2;
    public static final int BANNER=3;
    public static final int FREEBIES=4;
    public static final int SEARCH= 5;

    public static final int PROGRESS= 100;

    public HomeDatum(String type, int type_index){
        this.type= type;
        this.type_index= type_index;
    }

    private Scorecard scorecard;
    private Banner banner;

    private String name;
    private String parlorId;
    private String image;
    private String gender;
    private String address1;
    private String address2;
    private int parlorType;
    private String closingTime;
    private String openingTime;
    private double rating;
    private int price;
    private double distance;
    private String type="";
    private Boolean favourite;
    private List<Appointment> appointments = null;
    private String title;
    private List<List_> list = null;
    private int dayClosed;
    private String subscriptionCount;

    private int type_index= 0;                        //default 0 hai, jo kuch bhi nai hai

    private boolean isSelected= false;

    public int getType_index() {

        if(type!=null &&
                type.equalsIgnoreCase("parlor")){
            type_index=1;
        }else if(type!=null &&
                type.equalsIgnoreCase("subscription")){
            type_index=2;
        }else if(type!=null &&
                (type.equalsIgnoreCase("secondAdBanner") ||
                        type.equalsIgnoreCase("firstAdBanner"))){
            type_index=3;
        }else if(type!=null &&
                type.equalsIgnoreCase("freebies")){
            type_index=4;
        }else if(type!=null &&
                type.equalsIgnoreCase("search")) {
            type_index=5;
        } else if(type!=null &&
                type.equalsIgnoreCase("progress")){
            type_index=100;
        }
        return type_index;
    }

    public String getSubscriptionCount() {
        return subscriptionCount;
    }

    public void setSubscriptionCount(String subscriptionCount) {
        this.subscriptionCount = subscriptionCount;
    }

    public void setType_index(int type_index) {
        this.type_index = type_index;
    }

    public class Appointment{
        private double amount;
        private String appointmentId;
        private String date;
        private String parlorId;

        public double getAmount() {
            return amount;
        }

        public void setAmount(double amount) {
            this.amount = amount;
        }

        public String getAppointmentId() {
            return appointmentId;
        }

        public void setAppointmentId(String appointmentId) {
            this.appointmentId = appointmentId;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getParlorId() {
            return parlorId;
        }

        public void setParlorId(String parlorId) {
            this.parlorId = parlorId;
        }
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getDayClosed() {
        return dayClosed;
    }

    public void setDayClosed(int dayClosed) {
        this.dayClosed = dayClosed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public int getParlorType() {
        return parlorType;
    }

    public void setParlorType(int parlorType) {
        this.parlorType = parlorType;
    }

    public String getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(String closingTime) {
        this.closingTime = closingTime;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getFavourite() {
        return favourite;
    }

    public void setFavourite(Boolean favourite) {
        this.favourite = favourite;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<List_> getList() {
        return list;
    }

    public void setList(List<List_> list) {
        this.list = list;
    }

    public Scorecard getScorecard() {
        return scorecard;
    }

    public void setScorecard(Scorecard scorecard) {
        this.scorecard = scorecard;
    }

    public Banner getBanner() {
        return banner;
    }

    public void setBanner(Banner banner) {
        this.banner = banner;
    }
}