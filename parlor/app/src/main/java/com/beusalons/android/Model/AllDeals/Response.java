package com.beusalons.android.Model.AllDeals;

import java.util.List;

/**
 * Created by myMachine on 4/28/2017.
 */

public class Response {

    public Boolean success;
    public List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
}
