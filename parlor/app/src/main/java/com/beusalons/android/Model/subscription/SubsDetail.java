package com.beusalons.android.Model.subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubsDetail {

    @SerializedName("heading1")
    @Expose
    private String heading1;
    @SerializedName("heading2")
    @Expose
    private String heading2;
    @SerializedName("lightDetail")
    @Expose
    private String lightDetail;
    @SerializedName("tnc")
    @Expose
    private List<String> tnc = null;

    public String getHeading1() {
        return heading1;
    }

    public void setHeading1(String heading1) {
        this.heading1 = heading1;
    }

    public String getHeading2() {
        return heading2;
    }

    public void setHeading2(String heading2) {
        this.heading2 = heading2;
    }

    public String getLightDetail() {
        return lightDetail;
    }

    public void setLightDetail(String lightDetail) {
        this.lightDetail = lightDetail;
    }

    public List<String> getTnc() {
        return tnc;
    }

    public void setTnc(List<String> tnc) {
        this.tnc = tnc;
    }

}