package com.beusalons.android.Adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.beusalons.android.Model.Reviews.ParlorReviewData;
import com.beusalons.android.R;

import java.util.List;

/**
 * Created by myMachine on 11/19/2016.
 */

public class ParlorReviewListAdapter extends RecyclerView.Adapter<ParlorReviewListAdapter.SalonReviewViewHolder> {


    private List<ParlorReviewData> details;
    private Boolean isCheck= true;
    private String parlorImage;
    Activity activity;

    public ParlorReviewListAdapter(List<ParlorReviewData> details, String parlorImage, Activity activity){
        this.details= details;
        this.parlorImage= parlorImage;
        this.activity= activity;
        Log.i("ParlorReview", "details size: "+ details.size());
    }

    public class SalonReviewViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_user_name, txt_review, txt_time, txt_rating;
        private CardView card_parlor_review;
        private ImageView rImg_parlorImage;

        public SalonReviewViewHolder(View itemView) {
            super(itemView);

            txt_user_name= (TextView)itemView.findViewById(R.id.txt_salonReview_userName);
            txt_review= (TextView)itemView.findViewById(R.id.txt_salonReview_review);
            txt_time= (TextView)itemView.findViewById(R.id.txt_salonReview_reviewDate);
            txt_rating= (TextView)itemView.findViewById(R.id.txt_salonReview_rating);
            card_parlor_review= (CardView)itemView.findViewById(R.id.card_parlor_review);
            rImg_parlorImage= (ImageView) itemView.findViewById(R.id.rImg_parlor_image);

        }
    }


    @Override
    public SalonReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.salon_review_cardview, parent, false);
        return new SalonReviewViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final SalonReviewViewHolder holder, int position) {

        ParlorReviewData data= details.get(position);
        Glide.with(activity).load(parlorImage).into(holder.rImg_parlorImage);
/*
        Glide.with(activity)
                .load(parlorImage)
                .centerCrop()
                .placeholder(R.drawable.img_salon)
                .crossFade()
                .into(holder.rImg_parlorImage);*/

        holder.txt_user_name.setText(data.getUserName());
        holder.txt_review.setText(data.getText());

//        int txt_length= data.getText().length();
//
//        if(txt_length>25){
//            String review= data.getText().substring(0,24)+fromHtml("<font color='#d2232a'>read more..</font>");
//            Log.i("i'mreview", "value in stuff: "+ review);
//        }




        holder.txt_time.setText(data.getTime());
        holder.txt_rating.setText(String.valueOf(data.getRating()));
        holder.card_parlor_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isCheck){
                    holder.txt_review.setMaxLines(10);
                    isCheck= false;
                }else{
                    holder.txt_review.setMaxLines(2);
                    isCheck= true;
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return details.size();
    }


    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

}
