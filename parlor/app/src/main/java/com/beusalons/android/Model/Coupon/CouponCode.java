package com.beusalons.android.Model.Coupon;

/**
 * Created by Ajay on 12/5/2017.
 */

public class CouponCode {

    public static final int COUPON=1;
    public static final int EARN_MORE_COUPON= 5;

    private Integer limit;
    private Integer offPercentage;
    private String couponDescription;
    private String couponTitle;
    private String imageUrl;
    private String couponId;
    private String code;
    private Integer newAmount;
    private String expiry;
    private String popUpButtonText;
    private String popUpText;
    private int type= 1;
    private boolean active;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public CouponCode(int type){
        this.type= type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getPopUpButtonText() {
        return popUpButtonText;
    }

    public void setPopUpButtonText(String popUpButtonText) {
        this.popUpButtonText = popUpButtonText;
    }

    public String getPopUpText() {
        return popUpText;
    }

    public void setPopUpText(String popUpText) {
        this.popUpText = popUpText;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public Integer getNewAmount() {
        return newAmount;
    }

    public void setNewAmount(Integer newAmount) {
        this.newAmount = newAmount;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffPercentage() {
        return offPercentage;
    }

    public void setOffPercentage(Integer offPercentage) {
        this.offPercentage = offPercentage;
    }

    public String getCouponDescription() {
        return couponDescription;
    }

    public void setCouponDescription(String couponDescription) {
        this.couponDescription = couponDescription;
    }

    public String getCouponTitle() {
        return couponTitle;
    }

    public void setCouponTitle(String couponTitle) {
        this.couponTitle = couponTitle;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
