package com.beusalons.android.Model.Loyalty;

import com.beusalons.android.Model.AppointmentDetail.UsableMembership;

import java.util.List;

/**
 * Created by myMachine on 11/28/2016.
 */

public class DataResponse {

    private int corporateReferralCount;
    private List<UsableMembership> activeMemberships = null;
    private int freeHairCutBar;
    private List<Freeby> freebies = null;
    private List<NewFreeBy> newFreebies=null;
    private int points;
    private String code;
    private Tnc tnc;
    private String wifiName;
    private String wifiPassword;
    private List<FreeService> freeServices = null;
    private List<FreeCorporateService> freeCorporateServices = null;
    private String corporateEmailId;
    private boolean isSubscribed;
    private String subscriptionReferMessage;
    private int remainingSubscriptionAmount;

    private CorporateReferalMessage corporateReferalMessage;
    private NormalReferalMessage noramlReferalMessage;

    private CouponCode couponCode;
    private String freeExpiry;

    public String getFreeExpiry() {
        return freeExpiry;
    }

    public void setFreeExpiry(String freeExpiry) {
        this.freeExpiry = freeExpiry;
    }

    public CouponCode getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(CouponCode couponCode) {
        this.couponCode = couponCode;
    }

    public CorporateReferalMessage getCorporateReferalMessage() {
        return corporateReferalMessage;
    }

    public void setCorporateReferalMessage(CorporateReferalMessage corporateReferalMessage) {
        this.corporateReferalMessage = corporateReferalMessage;
    }

    public NormalReferalMessage getNoramlReferalMessage() {
        return noramlReferalMessage;
    }

    public void setNoramlReferalMessage(NormalReferalMessage noramlReferalMessage) {
        this.noramlReferalMessage = noramlReferalMessage;
    }

    public String getCorporateEmailId() {
        return corporateEmailId;
    }

    public void setCorporateEmailId(String corporateEmailId) {
        this.corporateEmailId = corporateEmailId;
    }

    public int getCorporateReferralCount() {
        return corporateReferralCount;
    }

    public void setCorporateReferralCount(int corporateReferralCount) {
        this.corporateReferralCount = corporateReferralCount;
    }

    public List<UsableMembership> getActiveMemberships() {
        return activeMemberships;
    }

    public void setActiveMemberships(List<UsableMembership> activeMemberships) {
        this.activeMemberships = activeMemberships;
    }

    public int getFreeHairCutBar() {
        return freeHairCutBar;
    }

    public void setFreeHairCutBar(int freeHairCutBar) {
        this.freeHairCutBar = freeHairCutBar;
    }

    public List<Freeby> getFreebies() {
        return freebies;
    }

    public void setFreebies(List<Freeby> freebies) {
        this.freebies = freebies;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Tnc getTnc() {
        return tnc;
    }

    public void setTnc(Tnc tnc) {
        this.tnc = tnc;
    }

    public String getWifiName() {
        return wifiName;
    }

    public void setWifiName(String wifiName) {
        this.wifiName = wifiName;
    }

    public String getWifiPassword() {
        return wifiPassword;
    }

    public void setWifiPassword(String wifiPassword) {
        this.wifiPassword = wifiPassword;
    }

    public List<FreeService> getFreeServices() {
        return freeServices;
    }

    public void setFreeServices(List<FreeService> freeServices) {
        this.freeServices = freeServices;
    }

    public List<FreeCorporateService> getFreeCorporateServices() {
        return freeCorporateServices;
    }

    public void setFreeCorporateServices(List<FreeCorporateService> freeCorporateServices) {
        this.freeCorporateServices = freeCorporateServices;
    }

    public List<NewFreeBy> getNewFreebies() {
        return newFreebies;
    }

    public void setNewFreebies(List<NewFreeBy> newFreebies) {
        this.newFreebies = newFreebies;
    }

    public boolean isSubscribed() {
        return isSubscribed;
    }

    public void setSubscribed(boolean subscribed) {
        isSubscribed = subscribed;
    }

    public String getSubscriptionReferMessage() {
        return subscriptionReferMessage;
    }

    public void setSubscriptionReferMessage(String subscriptionReferMessage) {
        this.subscriptionReferMessage = subscriptionReferMessage;
    }

    public int getRemainingSubscriptionAmount() {
        return remainingSubscriptionAmount;
    }

    public void setRemainingSubscriptionAmount(int remainingSubscriptionAmount) {
        this.remainingSubscriptionAmount = remainingSubscriptionAmount;
    }

    public class CouponCode{

        private String code;
        private String couponDescription;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getCouponDescription() {
            return couponDescription;
        }

        public void setCouponDescription(String couponDescription) {
            this.couponDescription = couponDescription;
        }

    }


}
