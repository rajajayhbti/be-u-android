package com.beusalons.android.Fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beusalons.android.Adapter.SelectEmpAdapter;
import com.beusalons.android.MainActivity;
import com.beusalons.android.Model.selectArtist.SelectEmployeePost;
import com.beusalons.android.Model.selectArtist.SelectEmployeeResponse;
import com.beusalons.android.Model.selectArtist.SelectedPost;
import com.beusalons.android.Model.selectArtist.SelectedResponse;
import com.beusalons.android.Model.selectArtist.Service;
import com.beusalons.android.Model.selectArtist.Services;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.michael.easydialog.EasyDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by myMachine on 15-Mar-18.
 */

public class SelectEmployeeFragment extends DialogFragment {

    public static final String APPOINTMENT_ID="com.beusalons.selectemployeefragment.apptId";
    public static final String SALON_NAME= "com.beusalons.selectemployeefragment.salon";

    private List<Services> list;
    private SelectEmpAdapter adapter;

    private String appointment_id= null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);          //no action bar space
        RelativeLayout relative_= (RelativeLayout) inflater.inflate(R.layout.fragment_select_employee, container, false);

        Bundle bundle= getArguments();
        String salon_name="";
        if(bundle!=null &&
                bundle.containsKey(APPOINTMENT_ID)){
            appointment_id= bundle.getString(APPOINTMENT_ID, null);
            salon_name= bundle.getString(SALON_NAME, "");
        }else
            dismiss();

        TextView txt_name= relative_.findViewById(R.id.txt_name);
        txt_name.setText("Choose Artist - "+salon_name);
        RecyclerView recycler_= relative_.findViewById(R.id.recycler_);
        recycler_.setLayoutManager(new LinearLayoutManager(relative_.getContext()));
        list= new ArrayList<>();
        if(getActivity()!=null)
            adapter= new SelectEmpAdapter(list, getActivity());
        recycler_.setAdapter(adapter);

        LinearLayout linear_back= relative_.findViewById(R.id.linear_back);
        linear_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        TextView txt_cancel= relative_.findViewById(R.id.txt_cancel);
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        TextView txt_confirm= relative_.findViewById(R.id.txt_confirm);
        txt_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        sendData();
                    }
                }).start();
                dismiss();
            }
        });

        fetchData();

        return relative_;
    }


    private void sendData(){

        SelectedPost post= new SelectedPost();
//        post.setAppointmentId("5aaa3a94e01468372ea769d3");
        post.setAppointmentId(appointment_id);

        List<Service> services= new ArrayList<>();
        for(int i=0;i<list.size();i++){

            Service service= new Service();
            service.setServiceCode(list.get(i).getServiceCode());
            service.setBrandId(list.get(i).getBrandId());
            service.setProductId(list.get(i).getProductId());
            int j;
            for(j=0;j<list.get(i).getEmployees().size();j++){
                if(list.get(i).getEmployees().get(j).isSelected())
                    break;
            }
            if(j== list.get(i).getEmployees().size()){
                service.setEmployeeId(list.get(i).getEmployees().get(0).getEmployeeId());
            }else{
                service.setEmployeeId(list.get(i).getEmployees().get(j).getEmployeeId());
            }
            services.add(service);
        }
        post.setServices(services);

        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<SelectedResponse> call= apiInterface.sendData(post);
        call.enqueue(new Callback<SelectedResponse>() {
            @Override
            public void onResponse(Call<SelectedResponse> call, Response<SelectedResponse> response) {

                if(response.isSuccessful()){
                    if(response.body().isSuccess()){
                        Log.i("sendselectedata", "in the success");

                    }else{
                        Log.i("sendselectedata", "in the not success");
                    }
                }else{
                    Log.i("sendselectedata", "in the else");
                }
            }
            @Override
            public void onFailure(Call<SelectedResponse> call, Throwable t) {
                Log.i("sendselectedata", "in the failure: "+ t.getStackTrace()+ " "+ t.getCause());
            }
        });

    }


    private void fetchData(){

        SelectEmployeePost post=new SelectEmployeePost();
        post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        post.setUserId(BeuSalonsSharedPrefrence.getUserId());
//        post.setAppointmentId("5aaa3a94e01468372ea769d3");
        post.setAppointmentId(appointment_id);

        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<SelectEmployeeResponse> call=apiInterface.getArtist(post);
        call.enqueue(new Callback<SelectEmployeeResponse>() {
            @Override
            public void onResponse(Call<SelectEmployeeResponse> call, Response<SelectEmployeeResponse> response) {

                if(response.isSuccessful()){
                    if(response.body().isSuccess()){
                        Log.i("selectemployee", "in the success");

                        list.addAll(response.body().getData().getServices());
                        adapter.notifyDataSetChanged();
                    }else{
                        Log.i("selectemployee", "in the not success");
                    }
                }else {
                    Log.i("selectemployee", "in the else");
                }
            }

            @Override
            public void onFailure(Call<SelectEmployeeResponse> call, Throwable t) {
                Log.i("selectemployee", "on failure: "+ t.getCause()+ " "+ t.getStackTrace());
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            Window window = getDialog().getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFFFFF")));
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }catch (Exception e){

            e.printStackTrace();
        }
    }
}
