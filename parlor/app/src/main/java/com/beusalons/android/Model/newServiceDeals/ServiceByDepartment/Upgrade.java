package com.beusalons.android.Model.newServiceDeals.ServiceByDepartment;

import com.beusalons.android.Model.newServiceDeals.NewCombo.Selector;

import java.util.List;

/**
 * Created by myMachine on 6/8/2017.
 */

public class Upgrade {

    private String name;
    private String detail;
    private int price;
    private int save;
    private String serviceId;
    private int menuPrice;

    private List<Selector> selectors = null;

    public int getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(int menuPrice) {
        this.menuPrice = menuPrice;
    }

    public List<Selector> getSelectors() {
        return selectors;
    }

    public void setSelectors(List<Selector> selectors) {
        this.selectors = selectors;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getSave() {
        return save;
    }

    public void setSave(int save) {
        this.save = save;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
}
