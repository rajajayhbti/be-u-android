package com.beusalons.android.Model.Notifications;

/**
 * Created by Ashish Sharma on 3/30/2017.
 */

public class NotificationPost {
    private String userId;
    private String accessToken;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
