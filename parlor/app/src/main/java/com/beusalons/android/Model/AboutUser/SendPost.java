package com.beusalons.android.Model.AboutUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 17-Feb-18.
 */

public class SendPost {

    private String userId;
    private String accessToken;
    private List<QuestionAns> questionAnswers= new ArrayList<>();


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public List<QuestionAns> getQuestionAnswers() {
        return questionAnswers;
    }

    public void setQuestionAnswers(List<QuestionAns> questionAnswers) {
        this.questionAnswers = questionAnswers;
    }
}
