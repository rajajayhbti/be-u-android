package com.beusalons.android;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Helper.CheckConnection;
import com.beusalons.android.Model.Reviews.WriteReviewPost;
import com.beusalons.android.Model.Reviews.WriteReviewResponse;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.Utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class WriteReview_Activity extends AppCompatActivity {

    private final static String TAG= WriteReview_Activity.class.getSimpleName();
    TextView txtViewActionBarName;
    ImageView imgViewBack;
    //instanciating widgets
    private TextView txt_salonName;

    private EditText etxt_review;

    private Button btn_submitReviews;

    private Retrofit retrofit;

    private String parlorId, appointmentId, userId;
    private int rating= 5 ;

    private String accessToken;


    private TextView txt_words;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_review_);
        setToolBar();

        this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        //new ToolbarHelper(getSupportActionBar(), getLayoutInflater(), true, this);
        Bundle bundle= getIntent().getExtras();


        txt_salonName= (TextView)findViewById(R.id.txt_writeReview_salonName);
        etxt_review= (EditText)findViewById(R.id.etxt_writeReview_review);
        btn_submitReviews=(Button)findViewById(R.id.btn_writeReview_submitReview);
        txt_words= (TextView)findViewById(R.id.txt_words);
        txt_words.setVisibility(View.VISIBLE);


        if(bundle!=null){

            txt_salonName.setText(bundle.getString("parlorName"));
            parlorId= bundle.getString("parlorId");
            appointmentId= bundle.getString("appointmentId");
        }

        btn_submitReviews= (Button)findViewById(R.id.btn_writeReview_submitReview);
        btn_submitReviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Submitting Review");

                String txt= etxt_review.getText().toString();

                if(txt.length()>=25){

                    if(CheckConnection.isConnected(WriteReview_Activity.this)){
                        uploadData(txt);
                    }
                }else{

                    Toast.makeText(WriteReview_Activity.this, "Review Must Be Of Minimum 25 Characters", Toast.LENGTH_SHORT).show();
                    etxt_review.callOnClick();
                }




            }
        });


        final RadioGroup radioGroup = (RadioGroup) findViewById(R.id.rbtn_group_write_review);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId) {
                    case R.id.rbtn_blah:

                       rating=1;
//                        etxt_review.setText("Blah!");

                        break;
                    case R.id.rbtn_not_bad:

                        rating=2;
//                        etxt_review.setText("Not Bad!");

                        break;
                    case R.id.rbtn_decent:

                        rating=3;
//                        etxt_review.setText("Decent!");

                        break;
                    case R.id.rbtn_awesome:

                        rating=4;
//                        etxt_review.setText("Awesome!");

                        break;
                    case R.id.rbtn_super_awesome:

                        rating=5;
//                        etxt_review.setText("Super Awesome!");

                        break;

                }
            }
        });


    }

    private void setToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){

            getSupportActionBar().setTitle(getResources().getString(R.string.write_review));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }

    }

    public void uploadData(String txt){

        SharedPreferences preferences= getSharedPreferences("userDetails", Context.MODE_PRIVATE);
        if(preferences!=null){

            userId= preferences.getString("userId", null);
            accessToken= preferences.getString("accessToken", null);
        }

        WriteReviewPost writeReviewPost= new WriteReviewPost();

        writeReviewPost.setRating(rating);
        writeReviewPost.setUserId(userId);
        writeReviewPost.setAccessToken(accessToken);
        writeReviewPost.setParlorId(parlorId);
        writeReviewPost.setAppointmentId(appointmentId);
        writeReviewPost.setText(txt);

        Log.i("writereview", "value in stuff:--rating "+writeReviewPost.getRating()+ "user id: "+ writeReviewPost.getUserId()+
                    " accesstoken : " + writeReviewPost.getAccessToken()+ " parlor id: "+ writeReviewPost.getParlorId()+
                    " appt id: "+writeReviewPost.getAppointmentId()+ "  get text: "+ writeReviewPost.getText());

        retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<WriteReviewResponse> call= apiInterface.writeReview(writeReviewPost);
        call.enqueue(new Callback<WriteReviewResponse>() {
            @Override
            public void onResponse(Call<WriteReviewResponse> call, Response<WriteReviewResponse> response) {

                Log.i(TAG, "I'm in onResponse");


                if(response.isSuccessful()){
                    WriteReviewResponse reviewResponse= response.body();

                    Log.i(TAG, "I'm in onResponse issuccesful");
                    if(reviewResponse.getSuccess()){

                        Log.i(TAG, "I'm in onResponse: yeh to chal parah");
                        Toast.makeText(WriteReview_Activity.this, reviewResponse.getData(), Toast.LENGTH_SHORT).show();
                        finish();

                    }else{

                        Log.i(TAG," success false pe hoon mai");
                        Toast.makeText(WriteReview_Activity.this, "Review posted", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }else{

                    Log.i(TAG, "response body unsuccessful pe hoon");
                    finish();
                }

                //Todo: handle karo isko



            }

            @Override
            public void onFailure(Call<WriteReviewResponse> call, Throwable t) {

                Toast.makeText(WriteReview_Activity.this, "There was some problem posting your review", Toast.LENGTH_SHORT).show();
                Log.i(TAG, "I'm in onFailure");
            }
        });
    }

    /*@Subscribe
    public void navView_imageUri(OttoImageUri ottoImageUri){

        Uri imageUri= ottoImageUri.getUri();
        if(imageUri !=null){

            ImageLoad imageLoad= new ImageLoad();
            imageLoad.execute(imageUri);
        }

    }

    private class ImageLoad extends AsyncTask<Uri, Void, Void> {

        Bitmap decodedImage = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Uri... imageUri) {

            Uri uri = imageUri[0];
            Bitmap image = null;
            try {
                image = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
                decodedImage = BitmapFactory.decodeStream(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));

                byteArrayOutputStream.close();
                byteArrayOutputStream.flush();
                image.recycle();


            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


        }

    }*/

    public static int countWords(String s){

        int wordCount = 0;

        boolean word = false;
        int endOfLine = s.length() - 1;

        for (int i = 0; i < s.length(); i++) {
            // if the char is a letter, word = true.
            if (Character.isLetter(s.charAt(i)) && i != endOfLine) {
                word = true;
                // if char isn't a letter and there have been letters before,
                // counter goes up.
            } else if (!Character.isLetter(s.charAt(i)) && word) {
                wordCount++;
                word = false;
                // last word of String; if it doesn't end with a non letter, it
                // wouldn't count without this.
            } else if (Character.isLetter(s.charAt(i)) && i == endOfLine) {
                wordCount++;
            }
        }
        return wordCount;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
