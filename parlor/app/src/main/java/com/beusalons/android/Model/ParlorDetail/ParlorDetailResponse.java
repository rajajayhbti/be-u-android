package com.beusalons.android.Model.ParlorDetail;

/**
 * Created by myMachine on 4/5/2017.
 */

public class ParlorDetailResponse {

    private boolean success;

    private Data data;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
