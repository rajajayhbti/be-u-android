package com.beusalons.android;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Adapter.ParlorsSearchListAdapter;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Helper.CheckConnection;
import com.beusalons.android.Model.AllParlors.AllParlorResponse;
import com.beusalons.android.Model.AllParlors.ParlorData;
import com.beusalons.android.Model.Verify_Otp.SearchParlorModel;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SearchParlorActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    @BindView(R.id.search_content) EditText searchContent;
//    @BindView(R.id.close_img) MaterialIconView close;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;
//    @BindView(R.id.retry_button) Button retryButton;
    @BindView(R.id.progress)
        ProgressBar progressWheel;


    private ParlorsSearchListAdapter parlorsSearchListAdapter;
    private List<SearchParlorModel> listData = new ArrayList<SearchParlorModel>();
    private GoogleApiClient googleApiClient;

    Boolean loading = false, loadingRequired = false;
    Activity activity;

    private Retrofit retrofit;

    private List<ParlorData> parlor_details= new ArrayList<>();

    private int retryFetchData=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_parlor);
        setToolBar();

        ButterKnife.bind(this);
        activity = this;
       // new ToolbarHelper(getSupportActionBar(), getLayoutInflater(), true, this);

        if(CheckConnection.isConnected(activity)){
            fetchDataAllParlors();
        }else{
            Toast.makeText(activity, "Can't connect right now.", Toast.LENGTH_SHORT).show();
        }


        googleApiClient = new GoogleApiClient
                .Builder( this )
                .enableAutoManage( this, 0, this )
                .addApi( Places.GEO_DATA_API )
                .addApi( Places.PLACE_DETECTION_API )
                .addConnectionCallbacks( this )
                .addOnConnectionFailedListener( this )
                .build();
        addTextWatcher();
//        close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                searchContent.setText("");
//                close.setVisibility(View.GONE);
//            }
//        });
        parlorsSearchListAdapter = new ParlorsSearchListAdapter(listData,
                this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(parlorsSearchListAdapter);
        addLocalDataAndUpdateAdapter(listData);



    }

    private void setToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(getResources().getString(R.string.search_salons));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);

        }
    }
    private void addTextWatcher() {
        ((TextView) searchContent).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                close.setVisibility(searchContent.getText().toString().equals("") ? View.GONE : View.VISIBLE);

//                Log.i("investigating", "value in charsequnce: "+s+"   start int mai: "+ start+"  before: "+ before+
//                " count mai: "+count);

                Log.e("asd", searchContent.getText().toString());
                if(!loading)fetchData();
                else{
                    loadingRequired = true;
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });
    }

    private void fetchData() {
//        retryButton.setVisibility(View.GONE);
//        ActivityHelper.showProgress(progressWheel);
        progressWheel.setVisibility(View.VISIBLE);

        if(listData!=null){

            listData.clear();
        }

        if(parlor_details!=null || parlor_details.size()>0){

            String user_text= searchContent.getText().toString().toLowerCase();           //user typed text

            for(int i=0; i<parlor_details.size();i++){

                Log.i("investigaaaati", "value in the boolean stuff: "+ parlor_details.get(i).getName());

                String parlor_name= parlor_details.get(i).getName().toLowerCase();

                if(parlor_name.contains(user_text) || parlor_name.startsWith(user_text)){

                    Log.i("investigaaaati", " value ni the parlor: "+parlor_details.get(i).getName());

                    SearchParlorModel searchParlorModel = new SearchParlorModel();
                    searchParlorModel.setTitle(parlor_details.get(i).getName());
                    searchParlorModel.setParlorId(parlor_details.get(i).getParlorId());
                    searchParlorModel.setParlor_location(parlor_details.get(i).getAddress1()+ " "+ parlor_details.get(i).getAddress2());

                    listData.add(searchParlorModel);
                }
            }
        }

        final PendingResult<AutocompletePredictionBuffer> result =
                Places.GeoDataApi.getAutocompletePredictions(googleApiClient, searchContent.getText().toString(),
                        AppConstant.PLACE_BOUNDS_DELHI, null);
        Thread thread = new Thread() {
            @Override
            public void run() {
                final AutocompletePredictionBuffer autocompletePredictions = result.await();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        for(int i = 0; i < autocompletePredictions.getCount(); i++){

                            SearchParlorModel searchParlorModel = new SearchParlorModel();
                            searchParlorModel.setTitle("" + autocompletePredictions.get(i).getFullText(null));
                            searchParlorModel.setPlaceId("" + autocompletePredictions.get(i).getPlaceId());

                            Log.i("investigatinggg", "valluein place id: "+ autocompletePredictions.get(i).getPlaceId());
                            listData.add(searchParlorModel);
                        }

                        addLocalDataAndUpdateAdapter(listData);
                        progressWheel.setVisibility(View.GONE);
                        loading = false;
                        if(loadingRequired){
                            loadingRequired = false;
                            fetchData();
                        }

                    }
                });

            }
        };
        if(searchContent.getText().toString().equals("")){
            loading = false;
            addLocalDataAndUpdateAdapter(listData);
            progressWheel.setVisibility(View.GONE);
            loadingRequired = false;
        }else{
            thread.start();
        }

    }

    private void addLocalDataAndUpdateAdapter(List<SearchParlorModel> listData) {

        SearchParlorModel searchParlorModel = new SearchParlorModel();
        searchParlorModel.setTitle(BeuSalonsSharedPrefrence.getAddressLocalty());
        searchParlorModel.setPlaceId("");
        listData.add(searchParlorModel);
        parlorsSearchListAdapter.notifyDataSetChanged();
    }


    public void fetchDataAllParlors(){

        retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<AllParlorResponse> call= apiInterface.getAllParlors("http://beusalons.com/api/allParlor");
        call.enqueue(new Callback<AllParlorResponse>() {
            @Override
            public void onResponse(Call<AllParlorResponse> call, Response<AllParlorResponse> response) {

                if(parlor_details!=null){
                    parlor_details.clear();
                }

                if(response.isSuccessful()){

                    if(response.body().getSuccess()){

                        for(int i=0; i<response.body().getData().size(); i++){

                            ParlorData data= new ParlorData();
                            data.setName(response.body().getData().get(i).getName());
                            data.setParlorId(response.body().getData().get(i).getParlorId());
                            data.setAddress1(response.body().getData().get(i).getAddress1());
                            data.setAddress2(response.body().getData().get(i).getAddress2());

                            parlor_details.add(data);
                            Log.i("investiga", "value in the parlor name: "+ response.body().getData().get(i).getName());
                        }
                    }else{

                        finish();
                        //// TODO: 3/25/2017 toast dikhao
                    }
                }else{

                    finish();
                    //// TODO: 3/25/2017 toast dikhao
                }
            }

            @Override
            public void onFailure(Call<AllParlorResponse> call, Throwable t) {
                Log.i("investiga", "i'm in all parlor response failure");
                if(retryFetchData<2){
                    fetchData();
                }else{
                    finish();
                    Toast.makeText(SearchParlorActivity.this, "No Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    @Override
    protected void onStart() {
        super.onStart();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
