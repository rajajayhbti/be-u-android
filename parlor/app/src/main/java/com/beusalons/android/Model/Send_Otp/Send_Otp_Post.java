package com.beusalons.android.Model.Send_Otp;

import java.io.Serializable;

/**
 * Created by myMachine on 11/1/2016.
 */

public class Send_Otp_Post implements Serializable {


    private String phoneNumber;
    private Boolean resetPassword;
    private int retry;

    public Send_Otp_Post(){
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Boolean getResetPassword() {
        return resetPassword;
    }

    public void setResetPassword(Boolean resetPassword) {
        this.resetPassword = resetPassword;
    }

    public int getRetry() {
        return retry;
    }

    public void setRetry(int retry) {
        this.retry = retry;
    }
}
