package com.beusalons.android.Adapter;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Model.Reviews.ParlorReviewData;
import com.beusalons.android.Model.SalonReview.HelpFull_Response;
import com.beusalons.android.Model.SalonReview.Helpfull_post;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.beusalons.android.Model.Reviews.ParlorReviewData.DATA_TYPE;
import static com.beusalons.android.Model.Reviews.ParlorReviewData.PROGRESS_TYPE;

/**
 * Created by myMachine on 11/19/2016.
 */

public class SalonReview extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ParlorReviewData> list;
    private  Activity myActivity;
    public SalonReview(Activity activity,List<ParlorReviewData> list){

        this.list= list;
        this.myActivity=activity;
    }

    public class ViewHolder1 extends RecyclerView.ViewHolder{

        private LinearLayout linear_;
        public ViewHolder1(LinearLayout itemView) {
            super(itemView);
            linear_= itemView;
        }
    }

    public class ViewHolder2 extends RecyclerView.ViewHolder{

        private LinearLayout linear_progress;
        public ViewHolder2(View itemView) {
            super(itemView);
            linear_progress= (LinearLayout) itemView;
        }
    }

    public void addProgress(){

        if(list!=null && list.size()>0){

            list.add(new ParlorReviewData(1));
            notifyItemInserted(list.size()-1);
        }
    }

    public void removeProgress(){

        int size= list!=null && list.size()>0?list.size():0;

        if(size>0){

            list.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }

    public void addData(List<ParlorReviewData> list){

        int size= this.list.size();
        this.list.addAll(list);             //add kara
        notifyItemRangeInserted(size, this.list.size());        //existing size, aur new size
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LinearLayout linear_;
        switch (viewType){
            case DATA_TYPE:
                linear_= (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_salon_review, parent, false);
                return new ViewHolder1(linear_);
            case PROGRESS_TYPE:
                linear_= (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_progress, parent, false);
                return new ViewHolder2(linear_);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        switch (list.get(position).getType()) {

            case DATA_TYPE:

                final LinearLayout linear_= ((ViewHolder1)holder).linear_;

                final ImageView img_user= (ImageView)linear_.findViewById(R.id.img_user);
                RatingBar rating_= (RatingBar)linear_.findViewById(R.id.rating_);

                TextView txt_name= (TextView)linear_.findViewById(R.id.txt_name);
                TextView txt_date= (TextView)linear_.findViewById(R.id.txt_date);
                TextView txt_rating= (TextView)linear_.findViewById(R.id.txt_rating);
                TextView txt_review= (TextView)linear_.findViewById(R.id.txt_review);

                txt_name.setText(list.get(position).getUserName());
                txt_date.setText(list.get(position).getTime());
                txt_rating.setText(""+list.get(position).getRating());

                if(list.get(position).getText()!=null &&
                        list.get(position).getText().length()>0)
                    txt_review.setVisibility(View.VISIBLE);
                else
                    txt_review.setVisibility(View.GONE);

                try{
                    Glide.with(linear_.getContext()).load(list.get(position).getUserImage()).apply(RequestOptions.circleCropTransform()).into(img_user);
                  /*  Glide.with(linear_.getContext())
                            .load(list.get(position).getUserImage()).asBitmap().centerCrop()
                            .into(new BitmapImageViewTarget(img_user){
                                @Override
                                protected void setResource(Bitmap resource) {
                                    RoundedBitmapDrawable circularBitmapDrawable =
                                            RoundedBitmapDrawableFactory.create(linear_.getContext().getResources(), resource);
                                    circularBitmapDrawable.setCircular(true);
                                    img_user.setImageDrawable(circularBitmapDrawable);
                                }
                            });*/
                }catch (Exception e){
                    e.printStackTrace();
                }

                rating_.setRating((float) list.get(position).getRating());
                if(list.get(position).getRating()<=2.0){

                    txt_rating.setBackgroundResource(R.drawable.shape_round_primary);
                    if(Build.VERSION.SDK_INT>=21)
                        rating_.setProgressTintList(ColorStateList.valueOf(
                                ContextCompat.getColor(linear_.getContext(), R.color.colorPrimary)));

                }else if(list.get(position).getRating()>2.0 &&
                        list.get(position).getRating()<=3.0){

                    txt_rating.setBackgroundResource(R.drawable.shape_round_snackbar);
                    if(Build.VERSION.SDK_INT>=21)
                        rating_.setProgressTintList(ColorStateList.valueOf(
                                ContextCompat.getColor(linear_.getContext(), R.color.snackbar)));

                }else{

                    txt_rating.setBackgroundResource(R.drawable.shape_round_green);
                    if(Build.VERSION.SDK_INT>=21)
                        rating_.setProgressTintList(ColorStateList.valueOf(
                                ContextCompat.getColor(linear_.getContext(), R.color.colorGreen)));
                }

                LinearLayout linear_helpful= (LinearLayout)linear_.findViewById(R.id.linear_helpful);
                TextView txt_helpful= (TextView)linear_.findViewById(R.id.txt_helpful);
                TextView txt_not_helpful= (TextView)linear_.findViewById(R.id.txt_not_helpful);

                txt_helpful.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        postHelpFullOrNot(list.get(position).getAppointmentId(),true,false);
                    }
                });

                txt_not_helpful.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        postHelpFullOrNot(list.get(position).getAppointmentId(),false,true);

                    }
                });
                if(list.get(position).getText()!=null &&
                        list.get(position).getText().length()>0 &&
                        list.get(position).getText().trim().length()>0){

                    linear_helpful.setVisibility(View.VISIBLE);
                    txt_review.setVisibility(View.VISIBLE);
                    txt_review.setText(list.get(position).getText().trim());
                }else{

                    txt_review.setVisibility(View.GONE);
                    linear_helpful.setVisibility(View.GONE);
                }





                break;

            case PROGRESS_TYPE:


                break;
        }
    }


    private void postHelpFullOrNot(String appId,boolean helpFull,boolean notHelpFull){

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);


        Helpfull_post helpfull_post=new Helpfull_post();
        helpfull_post.setAppId(appId);
        helpfull_post.setHelpful(helpFull);
        helpfull_post.setNotHelpful(notHelpFull);
        helpfull_post.setUserId(BeuSalonsSharedPrefrence.getUserId());
        helpfull_post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());

        Call<HelpFull_Response> call=apiInterface.helpFulPost(helpfull_post);
        call.enqueue(new Callback<HelpFull_Response>() {
            @Override
            public void onResponse(Call<HelpFull_Response> call, Response<HelpFull_Response> response) {
                if (response.body()!=null){
                    if (response.body().isSuccess()){
                        Toast.makeText(myActivity,response.body().getData(),Toast.LENGTH_SHORT).show();

                    }
                    else
                        if (!response.body().isSuccess()){
                            Toast.makeText(myActivity,response.body().getData(),Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<HelpFull_Response> call, Throwable t) {

            }
        });


    }
    @Override
    public int getItemViewType(int position) {

        if(list!=null && list.size()>0)
            return list.get(position).getType();
        return 0;
    }

    @Override
    public int getItemCount() {
        if(list!=null && list.size()>0)
            return list.size();
        return 0;
    }
}
