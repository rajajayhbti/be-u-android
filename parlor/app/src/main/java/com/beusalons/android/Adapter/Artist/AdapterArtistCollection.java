package com.beusalons.android.Adapter.Artist;

import android.app.Activity;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beusalons.android.Model.Profile.CollectionWisePost;
import com.beusalons.android.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ajay on 2/21/2018.
 */

public class AdapterArtistCollection extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
private static final int PROTFOLIO_TYPE = 0;
private static final int PROGRESS_TYPE = 1;
        Activity act;
private List<CollectionWisePost> list;
public AdapterArtistCollection(Activity activity, List<CollectionWisePost> list){
        this.act=activity;
        this.list=list;
        }
@Override
public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout linear_;
        switch (viewType){
        case PROTFOLIO_TYPE:
        linear_ = (LinearLayout) LayoutInflater.from(parent.getContext())
        .inflate(R.layout.row_new_collections, parent, false);
        return new MyViewHolder(linear_);
        case PROGRESS_TYPE:
        linear_= (LinearLayout)
        LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_progress, parent, false);
        return new ViewHolder2(linear_);
        }
        return null;
        }

@Override
public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (list.get(position).getType()){
        case PROTFOLIO_TYPE:
final AdapterArtistCollection.MyViewHolder temp= (AdapterArtistCollection.MyViewHolder) holder;
final int pos=position;
        temp.txtCollectionName.setText(list.get(position).getName());

        if(list.get(position).getProjects().size()==0 ||
        list.get(position).getProjects().size()==1)
        temp.txt_project_count.setText(list.get(position).getProjects().size()+" Project");
        else
        temp.txt_project_count.setText(list.get(position).getProjects().size()+" Projects");


//        temp.linear_.setOnClickListener(new View.OnClickListener() {
//@Override
//public void onClick(View v) {
//        Intent intent=new Intent(act, SearchDetailsActivity.class);
//
//        intent.putExtra("catId",list.get(pos).getProjects().get(0).getCollec().getCollecId());
//        intent.putExtra("type",1);
//        intent.putExtra("artistId",list.get(pos).getProjects().get(0).getArtistId());
//        act.startActivity(intent);
//
//        }
//        });

//        if (type){
//        temp.txtViewFollow.setVisibility(View.VISIBLE);
//        }
//        if (list.get(position).getProjects().get(0).isFollowedByMe()){
//        temp.txtViewFollow.setBackground(act.getResources().getDrawable(R.drawable.following_border));
//        temp.txtViewFollow.setText(act.getResources().getString(R.string.Following));
//        temp.txtViewFollow.setTextColor(act.getResources().getColor(R.color.white));
//
//        }else if (!list.get(position).getProjects().get(0).isFollowedByMe()){
//        temp.txtViewFollow.setBackground(act.getResources().getDrawable(R.drawable.follow_border));
//        temp.txtViewFollow.setText(act.getResources().getString(R.string.Follow));
//        temp.txtViewFollow.setTextColor(act.getResources().getColor(R.color.primaryColor));
//
//
//        }
 //       temp.txtViewFollow.setOnClickListener(new View.OnClickListener() {
//@Override
//public void onClick(View v) {
//        if (BeuSalonsSharedPrefrence.getUserId()!=null ){
//        followData(list.get(pos).getProjects().get(0).getCollec().getCollecId(),pos);
//
//
//        }else{
//        Util.createLoginAlert(act);
//        }
//        }
//        });

//        if (list.get(position).getFollowingCollection()){
//
//        }else if (!list.get(position).getFollowingCollection()){
//
//        }
        AdapterArtistImgCollection adapterImgCollection=new AdapterArtistImgCollection(act,list.get(position).getProjects());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(act, LinearLayoutManager.HORIZONTAL, false);
        temp.recycler_.setLayoutManager(mLayoutManager);
        temp.recycler_.setAdapter(adapterImgCollection);
        break;
        case PROGRESS_TYPE:
        break;
        }
        }

@Override
public int getItemViewType(int position) {

        if(list!=null && list.size()>0)
        return list.get(position).getType();
        return 0;
        }

@Override
public int getItemCount() {

        if(list!=null && list.size()>0)
        return list.size();
        return 0;
        }
public void setInitialData(List<CollectionWisePost> list){

        this.list=list;
        notifyDataSetChanged();
        }

public void addData(List<CollectionWisePost> list){

        int size= this.list.size();
        Log.i("sizeing", "size of new: "+ list.size()+ " "+ "old: "+ this.list.size());
        this.list.addAll(list);             //add kara
        Log.i("sizeing", "size: "+ this.list.size());
        notifyItemRangeInserted(size, this.list.size());        //existing size, aur new size
        }

public void addProgress(){

        if(list!=null && list.size()>0){

        list.add(new CollectionWisePost(1));
        if (list.size()>1){
        Handler handler = new Handler();

      final Runnable r = new Runnable() {
      public void run() {
              notifyItemInserted(list.size()-1);
        }
        };

        handler.post(r);
        }

        }
        }

public void removeProgress(){

        int size= list!=null && list.size()>0?list.size():0;

        if(size>0){

        list.remove(size-1);
        notifyItemRemoved(size-1);
        }

        }

public class ViewHolder2 extends RecyclerView.ViewHolder{

    private ProgressBar progress_;
    public ViewHolder2(View itemView) {
        super(itemView);
        progress_= (ProgressBar)itemView.findViewById(R.id.progress_);
    }
}
public class MyViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.txt_collection_name)
    TextView txtCollectionName;
    @BindView(R.id.txt_project_count)
    TextView txt_project_count;
    @BindView(R.id.txt_view_follow)
    TextView txtViewFollow;
    @BindView(R.id.recycler_)
    RecyclerView recycler_;
    @BindView(R.id.linear_)
    LinearLayout linear_;
    public MyViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }
}



//    private void followData( String collId, final int pos){
//        Retrofit retrofit= ServiceGenerator.getPortfolioClient();
//        ApiInterface apiInterface= retrofit.create(ApiInterface.class);
//
//        Follow_post follow_post=new Follow_post();
//        follow_post.setCollectionId(collId);
//        if (BeuSalonsSharedPrefrence.getUserId()!=null)
//            follow_post.setUserId(BeuSalonsSharedPrefrence.getUserId());
//
//        Call<Follow_response> call=apiInterface.post_follow(follow_post);
//
//        call.enqueue(new Callback<Follow_response>() {
//            @Override
//            public void onResponse(Call<Follow_response> call, Response<Follow_response> response) {
//                if (response.isSuccessful()){
//                    if (response.body().getSuccess()){
//
//                        if (list.get(pos).getProjects().get(0).isFollowedByMe()){
//
//                            list.get(pos).getProjects().get(0).setFollowedByMe(false);
//                            Toast.makeText(act, "Successfully Follow", Toast.LENGTH_SHORT).show();
//                            notifyDataSetChanged();
//                        }else if(!list.get(pos).getProjects().get(0).isFollowedByMe()){
//                            list.get(pos).getProjects().get(0).setFollowedByMe(true);
//                            Toast.makeText(act, "Successfully Following", Toast.LENGTH_SHORT).show();
//                            notifyDataSetChanged();
//                        }
//
//
//                    }else if (!response.body().getSuccess()){
//                        Toast.makeText(act, "Looks Like The Server Is Taking To Long To Respond\n" +
//                                " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
//                    }
//                }else if (!response.isSuccessful()){
//                    Toast.makeText(act, "Looks Like The Server Is Taking To Long To Respond\n" +
//                            " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Follow_response> call, Throwable t) {
//                Toast.makeText(act, "Looks Like The Server Is Taking To Long To Respond\n" +
//                        " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
//                Log.i("homestuff", "i'm in failure: "+t.getMessage()+ " " + t.getCause()+ "   "+ t.getStackTrace());
//            }
//        });
//    }
}
