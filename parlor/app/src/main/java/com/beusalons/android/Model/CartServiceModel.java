package com.beusalons.android.Model;

/**
 * Created by Robbin Singh on 26/11/2016.
 */

public class CartServiceModel{


    private String name;
    private String additions;           // detail about the customize selected item
    private double price;
    private double tax;
    private int quantity;
    private  String serviceName;
    private int dealIdParlor;
    private String dealName;
    private int dealPrice;
    private int menuPrice;
    private String dealId;
    private  int weekDay;
    private  Integer totalPrice;
    private int actualServiceCode;  // this is the acutal service code to be send to server
    private int serviceCode;   // save the service code----- this is going to be the dummy service code for uniquness of items
    private String serviceId;       // this is the long service id string
    private int priceId;        // this is the price id ....service code types wala
    private String type;        // pass 'service' for service or the deal name in case of deals

    private String memberShipId;   //use for membership


    private int service_serviceCode;            //yeh service service code hai

    public String getMemberShipId() {
        return memberShipId;
    }

    public void setMemberShipId(String memberShipId) {
        this.memberShipId = memberShipId;
    }

    public int getService_serviceCode() {
        return service_serviceCode;
    }

    public void setService_serviceCode(int service_serviceCode) {
        this.service_serviceCode = service_serviceCode;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    private int priceAdditions;             //price additions price

    private int index;                          //types array ka index, remaining places mai I have passed 0



    // below stuff for booking summary page---------------------------------------------------
    private Boolean offer_check;            // yeh free service ke liye hai... checkbox check karne ke liye

    //------------------------------------------------------------------------------

    //stuff to check deal to show the menu and deal price in the cart, yeh deals ke liye hai
    private Boolean check_deal;

    //-------------------------------------------


    public Boolean getCheck_deal() {
        return check_deal;
    }

    public void setCheck_deal(Boolean check_deal) {
        this.check_deal = check_deal;
    }

    public Boolean getOffer_check() {
        return offer_check;
    }

    public void setOffer_check(Boolean offer_check) {
        this.offer_check = offer_check;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getPriceId() {
        return priceId;
    }

    public void setPriceId(int priceId) {
        this.priceId = priceId;
    }

    public int getPriceAdditions() {
        return priceAdditions;
    }

    public void setPriceAdditions(int priceAdditions) {
        this.priceAdditions = priceAdditions;
    }

    public int getActualServiceCode() {
        return actualServiceCode;
    }

    public void setActualServiceCode(int actualServiceCode) {
        this.actualServiceCode = actualServiceCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }


    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public int getDealIdParlor() {
        return dealIdParlor;
    }

    public void setDealIdParlor(int dealIdParlor) {
        this.dealIdParlor = dealIdParlor;
    }

    public String getDealName() {
        return dealName;
    }

    public void setDealName(String dealName) {
        this.dealName = dealName;
    }

    public int getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(int dealPrice) {
        this.dealPrice = dealPrice;
    }

    public int getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(int menuPrice) {
        this.menuPrice = menuPrice;
    }

    public int getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(int weekDay) {
        this.weekDay = weekDay;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(int serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getAdditions() {
        return additions;
    }

    public void setAdditions(String additions) {
        this.additions = additions;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
