package com.beusalons.android;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.beusalons.android.Event.NewDealEvent.DealComboEvent;
import com.beusalons.android.Event.NewDealEvent.PackageListEvent;
import com.beusalons.android.Fragment.DealsServicePopUpFragment;
import com.beusalons.android.Fragment.DealsSpecificFragment;
import com.beusalons.android.Fragment.UserCartFragment;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.DealsData.DealsData;
import com.beusalons.android.Model.DealsServices.Data;
import com.beusalons.android.Model.DealsServices.DealsResponse;
import com.beusalons.android.Model.ServiceDialog.CustomAdapter;
import com.beusalons.android.Model.Share.ShareDealService;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.Model.newServiceDeals.Department;
import com.beusalons.android.Model.newServiceDeals.Service;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Task.MultipleServicesTask;
import com.beusalons.android.Task.UserCartTask;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Ajay on 6/9/2017.
 */

public class DealsServicesActivity extends AppCompatActivity {

    private CoordinatorLayout coordinator_;
    private ViewPager viewPager;

    private TextView txt_item;

    private int vpager_position=0;

    View mLoadingView;
    RelativeLayout relative;
    AppEventsLogger logger;
    private DealsServicesActivity.DealsFragmentPager adapter;

    private List<String> str_tab_name= new ArrayList<>();

    private Data data;

    private String gender, departmentId, department_name;

    private FirebaseAnalytics mFirebaseAnalytics;
    private TextView txt_location_name;
    private Spinner spinnerDepartMent;
    private UserCart saved_cart= null;
    private Department department;
    private DealsData dealsData;
    public  ArrayList<Department> CustomListViewValuesArr = new ArrayList<Department>();
    CustomAdapter Cadapter;
    private boolean isSpinnerTouched=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deals_services);

        inItView();
    }

    private void inItView(){

        Bundle bundle= getIntent().getExtras();
        if(bundle!=null && bundle.containsKey("department_id")){
            dealsData=  new Gson().fromJson(bundle.getString("dealsData", null), DealsData.class);
            departmentId= bundle.getString("department_id", null);
            department_name=  bundle.getString("department_name", department_name);
            gender= bundle.getString("gender", null);
            vpager_position= bundle.getInt("position",0);

        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar()!=null){
            getSupportActionBar().setTitle(department_name);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        txt_location_name= (TextView)findViewById(R.id.txt_service_name);
        spinnerDepartMent= (Spinner) findViewById(R.id.spinner_department);
        txt_location_name.setText(department_name);


        txt_location_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerDepartMent.performClick();

            }
        });

        txt_item= (TextView)findViewById(R.id.txt_item);
        openCart();

        mLoadingView=findViewById(R.id.loading_for_deals);
        coordinator_= (CoordinatorLayout)findViewById(R.id.coordinator_);
        relative=(RelativeLayout)findViewById(R.id.relative);
        viewPager= (ViewPager)findViewById(R.id.viewpager_);
        logger = AppEventsLogger.newLogger(DealsServicesActivity.this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(DealsServicesActivity.this);

        TextView txt_inclusive= findViewById(R.id.txt_inclusive);
        txt_inclusive.setTypeface(null, Typeface.ITALIC);


        LinearLayout linear_cart= (LinearLayout) findViewById(R.id.linear_cart);
        linear_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logViewCartInDealEvent();
                logViewCartInDealFireBaseEvent();
                UserCartFragment fragment= new UserCartFragment();
                Bundle bundle= new Bundle();
                bundle.putBoolean("has_data", true);
                bundle.putBoolean(UserCartFragment.SHOW_PROCEED, true);
                fragment.setArguments(bundle);
                fragment.show(getSupportFragmentManager(), "user_cart");
            }
        });

        Button btn_proceed= (Button) findViewById(R.id.btn_proceed);
        btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logProceedInDealEvent();
                logProceedInDealFireBaseEvent();
                Intent intent= new Intent(DealsServicesActivity.this, ParlorListActivity.class);
                intent.putExtra("isDeal", true);
                startActivity(intent);
            }
        });

        //  fetchData();
        if (dealsData!=null&& dealsData.getDeals().size()>0){
            int genderPosition=-1;

            // Now i have taken static values by loop.
            // For further inhancement we can take data by webservice / json / xml;
            for(int i=0;i<dealsData.getDeals().size();i++){
                if (gender.equalsIgnoreCase(dealsData.getDeals().get(i).getGender())){
                    genderPosition=i;
                }
            }
            setSpinner(genderPosition);
        }else{
            fetchData(departmentId);
        }

    }


    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logViewCartInDealEvent () {
        Log.e("viewCartindeal","fine");
        logger.logEvent(AppConstant.ViewCartInDeal);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logProceedInDealEvent () {
        Log.e("ProceedInDeal","fine");

        logger.logEvent(AppConstant.ProceedInDeal);
    }


    public void logViewCartInDealFireBaseEvent () {
        Log.e("viewCartindealFireBase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.ViewCartInDeal,bundle);
    }


    public void logDealPageBackButtonEvent () {
        Log.e("DealPageBackButton","fine");
        logger.logEvent(AppConstant.DealPageBackButton);
    }

    public void logDealPageBackButtonFire () {
        Log.e("DealPageBackButtonfire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.DealPageBackButton,bundle);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logProceedInDealFireBaseEvent () {
        Log.e("ProceedInDealFireBase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.ProceedInDeal,bundle);
    }
    int check = 0;
    private void setSpinner(int position){
        Resources res = getResources();
        int selectionPosition = -1;

        final Service genderData=dealsData.getDeals().get(position);

       /* if (gender.equalsIgnoreCase("M")){
            for (int i=0;i<dealsData.getDeals().get(0).getDepartments().size();i++){
                final Department sched = new Department();
                dealsData.getDeals().get(0).getGender().equalsIgnoreCase("F");
                sched.setDepartmentId(dealsData.getDeals().get(0).getDepartments().get(i).getDepartmentId());
                sched.setName(dealsData.getDeals().get(0).getDepartments().get(i).getName());
                sched.setImage(dealsData.getDeals().get(0).getDepartments().get(i).getImage());
                if (department_name.equalsIgnoreCase(dealsData.getDeals().get(0).getDepartments().get(i).getName())){
                    spinnerDepartMent.setSelection(i);
                }
                CustomListViewValuesArr.add(sched);
            }
        }else {
            if (dealsData.getDeals().size()>1){
                for (int i=0;i<dealsData.getDeals().get(0).getDepartments().size();i++) {

                    final Department sched = new Department();

                    *//******* Firstly take data in model object ******//*

                    sched.setDepartmentId(dealsData.getDeals().get(1).getDepartments().get(i).getDepartmentId());
                    sched.setName(dealsData.getDeals().get(1).getDepartments().get(i).getName());
                    sched.setImage(dealsData.getDeals().get(1).getDepartments().get(i).getImage());

                    *//******** Take Model Object in ArrayList **********//*
                    CustomListViewValuesArr.add(sched);
                    if (department_name.equalsIgnoreCase(dealsData.getDeals().get(1).getDepartments().get(i).getName())){
                        selectionPosition=i;
                    }
                }}else{
                for (int i=0;i<dealsData.getDeals().get(0).getDepartments().size();i++){
                    final Department sched = new Department();
                    sched.setDepartmentId(dealsData.getDeals().get(0).getDepartments().get(i).getDepartmentId());
                    sched.setName(dealsData.getDeals().get(0).getDepartments().get(i).getName());
                    sched.setImage(dealsData.getDeals().get(0).getDepartments().get(i).getImage());
                    if (department_name.equalsIgnoreCase(dealsData.getDeals().get(0).getDepartments().get(i).getName())){
                        spinnerDepartMent.setSelection(i);
                    }
                    CustomListViewValuesArr.add(sched);
                }
            }
        }*/

        for (int i=0;i<genderData.getDepartments().size();i++){
            final Department sched = new Department();
            sched.setDepartmentId(genderData.getDepartments().get(i).getDepartmentId());
            sched.setName(genderData.getDepartments().get(i).getName());
            sched.setImage(genderData.getDepartments().get(i).getImage());
            if (department_name.equalsIgnoreCase(genderData.getDepartments().get(i).getName())){
                spinnerDepartMent.setSelection(i);
            }
            CustomListViewValuesArr.add(sched);
        }

        // Create custom adapter object ( see below CustomAdapter.java )
        Cadapter = new CustomAdapter(DealsServicesActivity.this, R.layout.spinner_rows, CustomListViewValuesArr,res);

        // Set adapter to spinner
        spinnerDepartMent.setAdapter(Cadapter);
        spinnerDepartMent.setSelection(selectionPosition);



        spinnerDepartMent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                isSpinnerTouched = true;
                return false;
            }
        });

        spinnerDepartMent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
                // your code here

                // Get selected row data to show on screen
                //    String Company    = ((TextView) v.findViewById(R.id.company)).getText().toString();
                //   String CompanyUrl = ((TextView) v.findViewById(R.id.sub)).getText().toString();

                //   String OutputMsg = "Selected Company : \n\n"+Company+"\n"+CompanyUrl;
//               output.setText(OutputMsg);


                //        Toast.makeText(
                //              getApplicationContext(),homeResponse.getData().getDepartments().get(0).getDepartments().get(position).getName(), Toast.LENGTH_LONG).show();
                //
                if (++check > 1) {
                    txt_location_name.setText(genderData.getDepartments().get(position).getName());
                    fetchData(genderData.getDepartments().get(position).getDepartmentId());
                  /*  if (dealsData.getDeals().get(0).getDepartments().size() == 2) {
                        if (gender.equalsIgnoreCase("M")) {
                            txt_location_name.setText(dealsData.getDeals().get(0).getDepartments().get(position).getName());
                            fetchData(dealsData.getDeals().get(0).getDepartments().get(position).getDepartmentId());
                        } else {
                            txt_location_name.setText(dealsData.getDeals().get(1).getDepartments().get(position).getName());
                            fetchData(dealsData.getDeals().get(1).getDepartments().get(position).getDepartmentId());
                        }

                    } else {
                        txt_location_name.setText(dealsData.getDeals().get(0).getDepartments().get(position).getName());
                        fetchData(dealsData.getDeals().get(0).getDepartments().get(position).getDepartmentId());
                    }
                }else{
                    txt_location_name.setText(department_name);
                    fetchData(departmentId);
                }*/
                }else{
                    txt_location_name.setText(department_name);
                    fetchData(departmentId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        spinnerDepartMent.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                isSpinnerTouched = true;
            }
        });
    }




    public void openCart(){

        try{
            DB snappyDB= DBFactory.open(DealsServicesActivity.this);

            saved_cart= null;
            if(snappyDB.exists(AppConstant.USER_CART_DB)) {
                saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
            }
            snappyDB.close();

            if(saved_cart!=null){

                int quantity=0;
                if(saved_cart.getServicesList().size()==1){

                    quantity= saved_cart.getServicesList().get(0).getQuantity();
                    if(quantity==1)
                        txt_item.setText(quantity+ " Item In Cart");
                    else
                        txt_item.setText(quantity+ " Items In Cart");
                }
                else if(saved_cart.getServicesList().size()>1){

                    for(int i=0;i<saved_cart.getServicesList().size();i++){

                        quantity+= saved_cart.getServicesList().get(i).getQuantity();
                    }

                    txt_item.setText(quantity+ " Items In Cart");
                }else
                    txt_item.setText("0 Item In Cart");
            }else
                txt_item.setText("0 Item In Cart");

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void fetchData(String departmentid){

        mLoadingView.setVisibility(View.VISIBLE);
        relative.setVisibility(View.GONE);
        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface= retrofit.create(ApiInterface.class);
        Call<DealsResponse> call= apiInterface.getDealsDepartmentWise(gender, departmentid, 1,
                1, BeuSalonsSharedPrefrence.getLatitude(), BeuSalonsSharedPrefrence.getLongitude());
        call.enqueue(new Callback<DealsResponse>() {
            @Override
            public void onResponse(Call<DealsResponse> call, Response<DealsResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().getSuccess()){
                        Log.i("stuffIdont", "i'm onResponse success");

                        str_tab_name.clear();
                        for(int i=0; i<response.body().getData().getCategories().size();i++){

                            str_tab_name.add(response.body().getData().getCategories().get(i).getName());
                        }

                        data=null;
                        data= response.body().getData();

                        adapter= new DealsServicesActivity.DealsFragmentPager(getSupportFragmentManager(), DealsServicesActivity.this);
                        viewPager.setAdapter(adapter);
                        viewPager.setCurrentItem(vpager_position);
                        TabLayout tabLayout= findViewById(R.id.tab_services);
                        tabLayout.setupWithViewPager(viewPager);

                        for (int i = 0; i < tabLayout.getTabCount(); i++) {
                            TabLayout.Tab tab = tabLayout.getTabAt(i);
                            tab.setCustomView(adapter.getTabView(i));
                        }

                        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                            @Override
                            public void onTabSelected(TabLayout.Tab tab) {

                                if(tab.getCustomView()!=null){

                                    TextView txt= tab.getCustomView().findViewById(R.id.txt_tab_name);
                                    txt.setTextColor(ContextCompat.getColor(DealsServicesActivity.this, R.color.colorPrimary));
                                }
                            }

                            @Override
                            public void onTabUnselected(TabLayout.Tab tab) {
                                if(tab.getCustomView()!=null) {
                                    TextView txt = (TextView) tab.getCustomView().findViewById(R.id.txt_tab_name);
                                    txt.setTextColor(ContextCompat.getColor(DealsServicesActivity.this, R.color.colorPrimaryText));
                                }
                            }

                            @Override
                            public void onTabReselected(TabLayout.Tab tab) {

                            }
                        });
                        relative.setVisibility(View.VISIBLE);
                        mLoadingView.setVisibility(View.GONE);

                    }else{
                        Log.i("stuffIdont", "i'm onResponse failure");
                    }

                }else{

                    Log.i("stuffIdont", "i'm retrofit failure :(");
                }
            }

            @Override
            public void onFailure(Call<DealsResponse> call, Throwable t) {

                Log.i("stuffIdont", "i'm in failure: "+ t.getMessage()+ "   "+ t.getStackTrace());
            }
        });
    }


    private class DealsFragmentPager extends FragmentStatePagerAdapter {

        private Context context;



        private DealsFragmentPager(FragmentManager fm, Context context){
            super(fm);
            this.context= context;
        }

        @Override
        public Fragment getItem(int position) {

            try{

                return DealsSpecificFragment.newInstance(data.getCategories().get(position), saved_cart,gender);
            }catch (Exception e){
                e.printStackTrace();


            }
            return null;

        }

        @Override
        public int getCount() {
            return data.getCategories().size();
        }

        private View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(context).inflate(R.layout.activity_services_custom_tab, null);

            TextView textView= (TextView)v.findViewById(R.id.txt_tab_name);

            if(position==vpager_position){
                textView.setTextColor(ContextCompat.getColor(DealsServicesActivity.this, R.color.colorPrimary));
            }else{
                textView.setTextColor(ContextCompat.getColor(DealsServicesActivity.this, R.color.colorPrimaryText));
            }
            textView.setText(str_tab_name.get(position));

            return v;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(final DealComboEvent event) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                //add user cart event
                logAddedToCartEvent(event.getService_name(),"INR");
                UserCart user_cart= new UserCart();
                user_cart.setCartType(AppConstant.DEAL_TYPE);

                UserServices services= new UserServices();

                services.setName(event.getService_name());

                services.setService_deal_id(event.getService_deal_id());               //service ya deal id hogi isme
                services.setDealId(event.getDealId());

                services.setType(event.getType());
                if(event.getParlorTypes()!=null){
                    Log.i("nothesametype", "if pe hoon bc: "+ event.getParlorTypes().size());
                    services.setParlorTypes(event.getParlorTypes());
                }
                else
                    Log.i("nothesametype", "else pe hoon bc");

                if(services.getType().equalsIgnoreCase("dealPrice") ||
                        services.getType().equalsIgnoreCase("chooseOne") ||
                        services.getType().equalsIgnoreCase("chooseOnePer")){

                    Log.i("dealskakand", "i'm in dealprice, choo.....blah blah");
                    services.setService_code(event.getPackage_services_list().get(0).getService_code());
                    services.setPrice_id(event.getPackage_services_list().get(0).getService_code());
                    services.setBrand_id(event.getPackage_services_list().get(0).getBrand_id());
                    services.setProduct_id(event.getPackage_services_list().get(0).getProduct_id());

                }else{

                    Log.i("dealskakand", "i'm in multiple pe");
                    services.setPackageServices(event.getPackage_services_list());
                }

                services.setDescription(event.getDescription());

                services.setPrimary_key(event.getPrimary_key());


                try{
                    DB snappyDB= DBFactory.open(DealsServicesActivity.this);
                    UserCart saved_cart= null;

                    if(snappyDB.exists(AppConstant.USER_CART_DB)) {

                        saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
                        snappyDB.close();

                        if(saved_cart!=null &&
                                saved_cart.getServicesList()!=null &&
                                saved_cart.getServicesList().size()>0){


                            if(saved_cart.getCartType()!=null &&
                                    !saved_cart.getCartType().equalsIgnoreCase(AppConstant.SERVICE_TYPE)){

                                //-------------------------------------
                                List<Integer> popup_types= new ArrayList<>();       //yeh hai sab ka intersection
                                //---------------------------------------------
                                int count=0;        //increasing count when service is available in any salon
                                for(int i=0;i<saved_cart.getServicesList().size();i++){

                                    List<Integer> types= new ArrayList<>();             //saved types
                                    List<Integer> selected_types= new ArrayList<>();            //selected types
                                    for(int j=0;j<saved_cart.getServicesList().get(i).getParlorTypes().size();j++){

                                        types.add(saved_cart.getServicesList().get(i).getParlorTypes().get(j).getType());
                                        if(i==0)
                                            popup_types.add(saved_cart.getServicesList().get(i).getParlorTypes().get(j).getType());
                                    }
                                    if(i!=0)
                                        popup_types.retainAll(types);

                                    for(int k=0;k<event.getParlorTypes().size();k++)
                                        selected_types.add(event.getParlorTypes().get(k).getType());

                                    types.retainAll(selected_types);
                                    if(types.size()>0)
                                        count++;
                                }

                                if(count!= saved_cart.getServicesList().size()){

                                    boolean premium= false, standard= false, budget= false;
                                    for(int b=0;b<popup_types.size();b++){
                                        if(popup_types.get(b)==0)
                                            premium= true;
                                        else if(popup_types.get(b)==1)
                                            standard= true;
                                        else if(popup_types.get(b)==2)
                                            budget= true;
                                    }

                                    DealsServicePopUpFragment fragment= new DealsServicePopUpFragment();
                                    Bundle bundle= new Bundle();
                                    bundle.putBoolean(DealsServicePopUpFragment.PREMIUM_SALON, premium);
                                    bundle.putBoolean(DealsServicePopUpFragment.STANDARD_SALON, standard);
                                    bundle.putBoolean(DealsServicePopUpFragment.BUDGET_SALON, budget);

                                    fragment.setArguments(bundle);
                                    fragment.show(getFragmentManager(),
                                            DealsServicePopUpFragment.THIS_FRAGMENT);

                                }else{

                                    showSnackbar(event.getService_name());
                                    new Thread(new UserCartTask(DealsServicesActivity.this, user_cart, services, false, false)).start();
                                }


                            }else{
                                showSnackbar(event.getService_name());
                                new Thread(new UserCartTask(DealsServicesActivity.this, user_cart, services, false, false)).start();
                            }

                        }else{
                            showSnackbar(event.getService_name());
                            new Thread(new UserCartTask(DealsServicesActivity.this, user_cart, services, false, false)).start();
                        }


                    }else{
                        snappyDB.close();
                        showSnackbar(event.getService_name());
                        new Thread(new UserCartTask(DealsServicesActivity.this, user_cart, services, false, false)).start();
                    }

                }catch (SnappydbException e){
                    e.printStackTrace();
                }

            }
        }, 250);

    }

    public void showSnackbar(String service_name){
        Snackbar snackbar = Snackbar.make( coordinator_,
                service_name+" Service Added To Cart!", 1000);

        //setting the snackbar action button text size
        View view = snackbar.getView();
        TextView txt_action = (TextView) view.findViewById(android.support.design.R.id.snackbar_action);
        TextView txt_text = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txt_action.setTextSize(13);
        txt_action.setAllCaps(false);
        txt_text.setTextSize(13);
        snackbar.setActionTextColor(ContextCompat.getColor(DealsServicesActivity.this, R.color.snackbar));

        snackbar.setAction("View Cart", new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UserCartFragment fragment= new UserCartFragment();
                Bundle bundle= new Bundle();
                bundle.putBoolean("has_data", true);
                bundle.putBoolean(UserCartFragment.SHOW_PROCEED, true);
                fragment.setArguments(bundle);
                fragment.show(getSupportFragmentManager(), "user_cart");
            }
        });
        snackbar.show();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(UserCart saved_cart) {

        if(saved_cart!=null && saved_cart.getServicesList().size()>0){

            int quantity=0;
            if(saved_cart.getServicesList().size()==1){

                quantity= saved_cart.getServicesList().get(0).getQuantity();
                if(quantity==1)
                    txt_item.setText(quantity+ " Item In Cart");
                else
                    txt_item.setText(quantity+ " Items In Cart");
            }
            else if(saved_cart.getServicesList().size()>1){

                for(int i=0;i<saved_cart.getServicesList().size();i++){

                    quantity+= saved_cart.getServicesList().get(i).getQuantity();
                }

                txt_item.setText(quantity+ " Items In Cart");
            }else
                txt_item.setText("0 Item In Cart");
        }else
            txt_item.setText("0 Item In Cart");

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void packageListEvent(PackageListEvent event) {

        UserCart user_cart= new UserCart();
        user_cart.setCartType(AppConstant.DEAL_TYPE);

        showSnackbar(event.getName());
        new Thread(new MultipleServicesTask(this, user_cart, event.getList())).start();

        if(event.isAlter())
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {


                }
            }, 400);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShareServiceEvent(ShareDealService event) {

        Log.i("intheshareser", "in the page" + department_name+ " "+ departmentId + " "+
                viewPager.getCurrentItem());


        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Hey check this out! Deals at Be U Salons https://beusalons.app.link/a/key_live_lftOr7qepHyQDI7uVSmpCkndstebwh0V?page=dealServices&departmentId="+departmentId+"&gender="+gender+"&index="+viewPager.getCurrentItem()+"&name="+department_name);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share Deals Service"));

    }


    public void logAddedToCartEvent (String contentType, String currency) {
        Log.e("prefine","add  type"+contentType+ " price");

        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        logger.logEvent(AppEventsConstants.EVENT_NAME_ADDED_TO_CART, params);
    }


    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){

            getSupportActionBar().setTitle(department_name);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        logDealPageBackButtonEvent();
        logDealPageBackButtonFire();

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
