package com.beusalons.android.Model.DealsServices;

import java.util.List;

/**
 * Created by Ajay on 6/9/2017.
 */

public class Data {

    private List<Category> categories = null;

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
