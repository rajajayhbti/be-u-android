package com.beusalons.android.Model;

/**
 * Created by myMachine on 5/4/2017.
 */

public class RescheduleAppointmentPost {

    private String userId;
    private String accessToken;
    private String appointmentId;
    private String startAts;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getStartAts() {
        return startAts;
    }

    public void setStartAts(String startAts) {
        this.startAts = startAts;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }
}
