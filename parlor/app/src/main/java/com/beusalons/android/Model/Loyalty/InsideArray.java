package com.beusalons.android.Model.Loyalty;

import java.util.ArrayList;

/**
 * Created by Ashish Sharma on 9/6/2017.
 */

public class InsideArray {
    private String insideTitle;
    private String insideImage;
    private boolean isSelected=false;
    private ArrayList<ArrayInfo >arrayInfo;

    public String getInsideTitle() {
        return insideTitle;
    }

    public void setInsideTitle(String insideTitle) {
        this.insideTitle = insideTitle;
    }

    public String getInsideImage() {
        return insideImage;
    }

    public void setInsideImage(String insideImage) {
        this.insideImage = insideImage;
    }

    public ArrayList<ArrayInfo> getArrayInfo() {
        return arrayInfo;
    }

    public void setArrayInfo(ArrayList<ArrayInfo> arrayInfo) {
        this.arrayInfo = arrayInfo;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
