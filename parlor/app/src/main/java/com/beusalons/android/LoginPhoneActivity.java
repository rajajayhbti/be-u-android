package com.beusalons.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Model.Registration.Registration_Post;
import com.beusalons.android.Model.Registration.Registration_Response;
import com.beusalons.android.Model.SocialLogin.LoginResponse;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.Utility;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginPhoneActivity extends AppCompatActivity {

    private EditText etxt_phone, etxt_refferal;
    private Button btn_next_active, btn_next_inactive;

    private LoginResponse loginResponse;
    private String social_token;
    private int social_type=0;

    private int retry=0, gender_position=0;
    private ProgressBar progressBar;

    private LinearLayout linear_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_phone);

        setToolBar();

        Bundle bundle= getIntent().getExtras();
        if(bundle!=null){

            loginResponse= (LoginResponse) bundle.getSerializable("loginResponse");
            social_token= bundle.getString("socialToken");
            social_type= bundle.getInt("socialType");

            Log.i("logintype", "value: "+ social_token+ " " +social_type);
        }

        progressBar= (ProgressBar)findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);

        linear_number= (LinearLayout)findViewById(R.id.linear_number);
        linear_number.setVisibility(View.VISIBLE);

        btn_next_inactive= (Button)findViewById(R.id.btn_next_inactive);
        btn_next_active= (Button)findViewById(R.id.btn_next_active);
        btn_next_inactive.setVisibility(View.VISIBLE);
        btn_next_inactive.setClickable(false);

        etxt_refferal= (EditText)findViewById(R.id.etxt_refferal);
        etxt_refferal.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

        final Spinner spinner = (Spinner) findViewById(R.id.spinner_gender);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {

                    ((TextView)v.findViewById(android.R.id.text1))
                            .setText("");
                    ((TextView)v.findViewById(android.R.id.text1))
                            .setHint(getItem(getCount())); //"Hint to be displayed"
                }

                return v;
            }

            @Override
            public int getCount() {
                return super.getCount()-1; // you dont display last item. It is used as hint.
            }
        };

        adapter.setDropDownViewResource(R.layout.row_spinner);
        adapter.add("Male");
        adapter.add("Female");
        adapter.add("Gender"); //This is the text that will be displayed as hint.


        spinner.setAdapter(adapter);
        spinner.setSelection(adapter.getCount());

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                gender_position= position;
                Log.i("spinner", "value in position"+ parent.getItemAtPosition(position)+ " "+ gender_position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.i("spinner", "nothing selected");
            }
        });

        View linear_spinner= findViewById(R.id.linear_spinner);         // hide spinner in case of fb login
        if(social_type==1){
            linear_spinner.setVisibility(View.GONE);
        }else{
            linear_spinner.setVisibility(View.VISIBLE);
        }

        etxt_phone= (EditText)findViewById(R.id.etxt_phone);
        etxt_phone.setTransformationMethod(new NumericKeyBoardTransformationMethod());      //method to convert password dot to digit
        etxt_phone.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        imm.showSoftInput(etxt_phone, InputMethodManager.SHOW_FORCED);


        etxt_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length()>=10){
                    btn_next_active.setVisibility(View.VISIBLE);
                    btn_next_inactive.setVisibility(View.GONE);
                }else{
                    btn_next_inactive.setVisibility(View.VISIBLE);
                    btn_next_inactive.setClickable(false);
                    btn_next_active.setVisibility(View.GONE);
                }

            }
        });

        btn_next_active.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(gender_position==2){
//                 spinner.setFocusable(true);
//                 spinner.setFocusableInTouchMode(true);
//                 spinner.requestFocus();
                    spinner.performClick();
                    Toast.makeText(LoginPhoneActivity.this, "Select Gender", Toast.LENGTH_SHORT).show();
                }else{

                    retry=0;
                    fetchData();
                }
            }
        });

    }


    public void fetchData(){
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        linear_number.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        etxt_phone.setClickable(false);
        btn_next_active.setClickable(false);

        final Registration_Post registrationPost = new Registration_Post();
        registrationPost.setPhoneNumber(etxt_phone.getText().toString());
        registrationPost.setAccessToken(social_token);
        registrationPost.setSocialLoginType(social_type);
        registrationPost.setGender(gender_position==0?"M":"F");                    //send male female
        registrationPost.setMobile(1);              //in case of mobile send 1
        //promo code hai yeh
        registrationPost.setReferCode(etxt_refferal.getText().toString().length()==0?"": etxt_refferal.getText().toString());

        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface= retrofit.create(ApiInterface.class);

        Call<Registration_Response> call= apiInterface.registerUser(registrationPost);
        call.enqueue(new Callback<Registration_Response>() {
            @Override
            public void onResponse(Call<Registration_Response> call, Response<Registration_Response> response) {

                Log.i("loginphoneacti", "i'm in onResponse");
                try{
                    if(response.isSuccessful()){


                        if(response.body().getSuccess()){
                            linear_number.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            etxt_phone.setClickable(true);
                            btn_next_active.setClickable(true);
                            etxt_phone.requestFocus();
                            Intent intent= new Intent(LoginPhoneActivity.this, OtpVerificationActivity.class);
                            intent.putExtra("phoneNumber", ""+etxt_phone.getText().toString());
                            intent.putExtra("gender", "");
                            intent.putExtra("socialType", 0);
                            intent.putExtra("socialToken", "");
                            startActivity(intent);

                        }else{
                            linear_number.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            etxt_phone.setClickable(true);
                            btn_next_active.setClickable(true);
                            etxt_phone.requestFocus();

                            Intent intent= new Intent(LoginPhoneActivity.this, OtpVerificationActivity.class);
                            intent.putExtra("phoneNumber", ""+etxt_phone.getText().toString());
                            intent.putExtra("gender", gender_position==0?"M":"F");
                            intent.putExtra("socialType", social_type);
                            intent.putExtra("socialToken", social_token);
                            startActivity(intent);
                            Log.i("loginphoneacti", "i'm in onResponse else");
                        }
                    }else{

                        //// TODO: 3/25/2017 toast dikhao
                        Log.i("loginphoneacti", "i'm in onResponse else else");
                        btn_next_active.setClickable(true);
                        Toast.makeText(LoginPhoneActivity.this, "Please try again", Toast.LENGTH_SHORT).show();

                        // finish();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                    Log.i("loginphoneacti", "i'm in onResponse");
                }
            }

            @Override
            public void onFailure(Call<Registration_Response> call, Throwable t) {

                if(retry<2){

                    fetchData();
                    retry++;
                }else{
                    linear_number.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    etxt_phone.setClickable(true);
                    btn_next_active.setClickable(true);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                    etxt_phone.requestFocus();
                    imm.showSoftInput(etxt_phone, InputMethodManager.SHOW_FORCED);
                    Toast.makeText(LoginPhoneActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

                Log.i("loginphoneacti", "i'm in onFailure"+ t.getMessage()+ "  "+ t.getCause());

            }
        });

    }

    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle("Sign up");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);

        }

    }


    private class NumericKeyBoardTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return source;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
