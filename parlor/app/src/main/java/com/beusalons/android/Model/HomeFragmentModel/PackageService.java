package com.beusalons.android.Model.HomeFragmentModel;

import java.util.List;

/**
 * Created by Ajay on 3/21/2017.
 */

public class PackageService {


    private String dealId;

    private String name;

    private String category;

    private String gender;

    private Integer dealIdParlor;

    private String description;

    private Integer menuPrice;

    private Integer dealPrice;

    private Integer tax;

    private Integer dealPercentage;

    private Integer weekDay;

    private PackageDealType dealType;

    private List<Service> services = null;

    private Object couponCode;

    private Integer type;

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getDealIdParlor() {
        return dealIdParlor;
    }

    public void setDealIdParlor(Integer dealIdParlor) {
        this.dealIdParlor = dealIdParlor;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(Integer menuPrice) {
        this.menuPrice = menuPrice;
    }

    public Integer getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(Integer dealPrice) {
        this.dealPrice = dealPrice;
    }

    public Integer getTax() {
        return tax;
    }

    public void setTax(Integer tax) {
        this.tax = tax;
    }

    public Integer getDealPercentage() {
        return dealPercentage;
    }

    public void setDealPercentage(Integer dealPercentage) {
        this.dealPercentage = dealPercentage;
    }

    public Integer getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(Integer weekDay) {
        this.weekDay = weekDay;
    }


    public PackageDealType getDealType() {
        return dealType;
    }

    public void setDealType(PackageDealType dealType) {
        this.dealType = dealType;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public Object getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(Object couponCode) {
        this.couponCode = couponCode;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
