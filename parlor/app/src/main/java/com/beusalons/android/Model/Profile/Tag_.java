package com.beusalons.android.Model.Profile;

/**
 * Created by Ajay on 1/23/2018.
 */

public class Tag_ {
    private String tagId;
    private String tagName;
    private String _id;

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}

