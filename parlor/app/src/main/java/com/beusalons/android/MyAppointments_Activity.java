package com.beusalons.android;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.beusalons.android.Fragment.PastAppointments_Fragment;
import com.beusalons.android.Fragment.UpcomingAppointments_Fragment;
import com.beusalons.android.Utility.Utility;
import com.beusalons.android.Views_Custom.View_Pager;


import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MyAppointments_Activity extends AppCompatActivity {


    UpcomingAppointments_Fragment upcoming_appointmentsFragment =null;
    PastAppointments_Fragment previous_appointmentsFragment = null;
    boolean iscameFromFreebie=false;

    TextView txtViewActionBarName;
    ImageView imgViewBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_appointments);

        //new ToolbarHelper(getSupportActionBar(), getLayoutInflater(), true, this);
        setToolBar();
        Bundle bundle= getIntent().getExtras();


        if(bundle!=null && !bundle.isEmpty()){

            iscameFromFreebie= bundle.getBoolean("fromFreebies");

        }


        final View_Pager viewPager= (View_Pager)findViewById(R.id.vp_myAppointments);
        viewPager.setPagingEnabled(true);

        ViewPagerAdapter viewPagerAdapter= new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(iscameFromFreebie ?1:0);
        TabLayout tabLayout= (TabLayout)findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);



    }
    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(getResources().getString(R.string.my_appointment));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);

        }


    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {


        final int TAB_COUNT= 2;

        private String TAB_NAME[]= new String[]{"Booked Appointments", "Completed Appointments"};


        private ViewPagerAdapter(FragmentManager fragmentManager){
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    if(upcoming_appointmentsFragment ==null){
                        upcoming_appointmentsFragment = new UpcomingAppointments_Fragment();
                    }
                    return upcoming_appointmentsFragment;
                case 1:
                    if(previous_appointmentsFragment ==null){
                        previous_appointmentsFragment = new PastAppointments_Fragment();
                    }
                    return previous_appointmentsFragment;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return TAB_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TAB_NAME[position];
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
