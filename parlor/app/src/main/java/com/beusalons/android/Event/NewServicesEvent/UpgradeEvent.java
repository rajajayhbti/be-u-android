package com.beusalons.android.Event.NewServicesEvent;

/**
 * Created by myMachine on 6/9/2017.
 */

public class UpgradeEvent {

    private String serviceId;

    public UpgradeEvent(String serviceId){

        this.serviceId= serviceId;
    }

    public String getServiceId() {
        return serviceId;
    }
}
