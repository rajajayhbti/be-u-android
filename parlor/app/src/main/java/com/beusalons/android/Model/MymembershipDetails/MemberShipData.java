package com.beusalons.android.Model.MymembershipDetails;

import java.util.List;

/**
 * Created by Ajay on 11/9/2017.
 */

public class MemberShipData {
    private String membershipSaleId;
    private String name;
    private String menuDiscountString;
    private String dealDiscountString;
    private Integer credits;
    private Integer menuDiscount;
    private Integer dealDiscount;
    private Integer creditsLeft;
    private String expiryDate;
    private List<Member> members = null;
    private List<CreditHistory> history = null;
    private List<FreeService> freeServices = null;
    private int noOfMembersAllowed;




    public String getMembershipSaleId() {
        return membershipSaleId;
    }

    public void setMembershipSaleId(String membershipSaleId) {
        this.membershipSaleId = membershipSaleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMenuDiscountString() {
        return menuDiscountString;
    }

    public void setMenuDiscountString(String menuDiscountString) {
        this.menuDiscountString = menuDiscountString;
    }

    public String getDealDiscountString() {
        return dealDiscountString;
    }

    public void setDealDiscountString(String dealDiscountString) {
        this.dealDiscountString = dealDiscountString;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public Integer getMenuDiscount() {
        return menuDiscount;
    }

    public void setMenuDiscount(Integer menuDiscount) {
        this.menuDiscount = menuDiscount;
    }

    public Integer getDealDiscount() {
        return dealDiscount;
    }

    public void setDealDiscount(Integer dealDiscount) {
        this.dealDiscount = dealDiscount;
    }

    public Integer getCreditsLeft() {
        return creditsLeft;
    }

    public void setCreditsLeft(Integer creditsLeft) {
        this.creditsLeft = creditsLeft;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }


    public List<CreditHistory> getHistory() {
        return history;
    }

    public void setHistory(List<CreditHistory> history) {
        this.history = history;
    }

    public List<FreeService> getFreeServices() {
        return freeServices;
    }

    public void setFreeServices(List<FreeService> freeServices) {
        this.freeServices = freeServices;
    }

    public int getNoOfMembersAllowed() {
        return noOfMembersAllowed;
    }

    public void setNoOfMembersAllowed(int noOfMembersAllowed) {
        this.noOfMembersAllowed = noOfMembersAllowed;
    }
}
