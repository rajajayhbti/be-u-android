package com.beusalons.android.Dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.beusalons.android.MainActivity;
import com.beusalons.android.R;

/**
 * Created by myMachine on 8/8/2017.
 */

public class CorporateSuccess extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        Bundle bundle= getArguments();

        boolean corporate_success= false;
        boolean wifi_success= false;

        if(bundle!=null){

            corporate_success= bundle.getBoolean("corporate_success");
            wifi_success= bundle.getBoolean("wifi_success");
        }

        View view = inflater.inflate(R.layout.corporate_success, container);
        ImageView img_= (ImageView)view.findViewById(R.id.img_);
        LinearLayout linear_= (LinearLayout)view.findViewById(R.id.linear_);

        if(corporate_success){

            img_.setImageResource(R.drawable.coporate_login_success);
            linear_.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    getActivity().finish();
                    AHBottomNavigation bottomNavigation= MainActivity.getBottomNav();
                    bottomNavigation.setCurrentItem(2);
                }
            });
        } else{

            img_.setImageResource(R.drawable.wifi_popup);
            linear_.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
        }

        return view;
    }


}
