package com.beusalons.android.Fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.beusalons.android.AboutUserActivity;
import com.beusalons.android.CorporateLoginActivity;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Helper.CheckConnection;
import com.beusalons.android.MainActivity;
import com.beusalons.android.MemberShipCardAcitvity;
import com.beusalons.android.MyAppointments_Activity;
import com.beusalons.android.MyMembershipDetails;
import com.beusalons.android.NotificationsActivity;
import com.beusalons.android.R;
import com.beusalons.android.RemainingServiceActivity;
import com.beusalons.android.UserReviews_Activity;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfile extends Fragment {

    public UserProfile(){

    }

    private final static String TAG= UserProfile.class.getSimpleName();


    //widget initialization
    private TextView txt_name, txt_corporate_id;
    private FirebaseAnalytics mFirebaseAnalytics;
    //variables
    private String name;

//    private ImageView img_profilePic;

    private ImageView img_profilePic;
    //    final Drawable drawable= null;
    AppEventsLogger logger;

    public static String cor_id="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_user_profile, container, false);


        //widget initialization
        txt_name= (TextView)view.findViewById(R.id.txt_name);
        img_profilePic= (ImageView) view.findViewById(R.id.imageView_Profile);

        logger = AppEventsLogger.newLogger(getActivity());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());

        //initialize the views with shared preference
        SharedPreferences preferences= getActivity().getSharedPreferences("userDetails", Context.MODE_PRIVATE);

        TextView txt_mem_points= (TextView)view.findViewById(R.id.txt_mem_points);
        TextView txt_mem_name= (TextView)view.findViewById(R.id.txt_mem_name);
        if(BeuSalonsSharedPrefrence.getMembershipPoints()>0){
            txt_mem_points.setVisibility(View.VISIBLE);
            txt_mem_name.setVisibility(View.VISIBLE);
            txt_mem_name.setText(BeuSalonsSharedPrefrence.getMembershipName());
            txt_mem_points.setText("Balance: "+(int)BeuSalonsSharedPrefrence.getMembershipPoints());
            txt_mem_points.setTypeface(null, Typeface.BOLD_ITALIC);

        }else{

            txt_mem_name.setVisibility(View.GONE);
            txt_mem_points.setVisibility(View.GONE);
        }

        if(preferences!=null){
            String name= preferences.getString("name", "Name");
            String profilePic= preferences.getString("profilePic", "");
            txt_name.setText(name);


            try{
                Glide.with(this).load(profilePic).apply(RequestOptions.circleCropTransform()).into(img_profilePic);
               /* Glide.with(this)
                        .load(profilePic).asBitmap().centerCrop()
                        .into(new BitmapImageViewTarget(img_profilePic){
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(getActivity().getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                img_profilePic.setImageDrawable(circularBitmapDrawable);
                            }
                        });*/
            }catch (Exception e){
                e.printStackTrace();
            }


        }
//        _bottomSheet = new BottomSheet();

//        Button btn_ChangeProfile= (Button)view.findViewById(R.id.btn_ChangeProfile);
//        btn_ChangeProfile.setVisibility(View.GONE);
//        btn_ChangeProfile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                _bottomSheet.show(getFragmentManager(), TAG);
//
//            }
//        });

//        Button btn_MyOffers= (Button)view.findViewById(R.id.btn_MyOffers);
//        btn_MyOffers.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent intent= new Intent(getActivity(), MyOffers_Activity.class);
//                startActivity(intent);
//            }
//        });

        LinearLayout btn_notification= (LinearLayout)view.findViewById(R.id.btn_notifications);
        btn_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity)getActivity()).bottomNavigation.setNotification("",4);
                startActivity(new Intent(getActivity(), NotificationsActivity.class));
            }
        });

        LinearLayout btn_MyAppointments= (LinearLayout)view.findViewById(R.id.btn_MyAppointments);
        btn_MyAppointments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getActivity(), MyAppointments_Activity.class));

            }
        });

        LinearLayout btn_MyLoyaltyPoints= (LinearLayout)view.findViewById(R.id.btn_MyLoyaltyPoints);
        btn_MyLoyaltyPoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity mainActivity = ((MainActivity)getActivity());
                AHBottomNavigation bottomNavigation= MainActivity.getBottomNav();
                bottomNavigation.setCurrentItem(2);

            }
        });

        LinearLayout btn_MyReview= (LinearLayout)view.findViewById(R.id.btn_MyReview);
        btn_MyReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(CheckConnection.isConnected(getActivity())){
                    startActivity(new Intent(getActivity(), UserReviews_Activity.class));
                }else{
                    Toast.makeText(getActivity(), "Can't connect right now.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        LinearLayout linear_corporate= (LinearLayout)view.findViewById(R.id.linear_corporate);
        linear_corporate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BeuSalonsSharedPrefrence.getVerifyCorporate()){
                   logReferColleaguesEvent();
                    logReferColleaguesFireBaseEvent();
                }
                else {
                    logAddCorporateIdClickEvent();
                    logAddCorporateIdClickFireBaseEvent();
                }
                startActivity(new Intent(getActivity(), CorporateLoginActivity.class));
            }
        });

        LinearLayout linear_preference= view.findViewById(R.id.linear_preference);
        linear_preference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), AboutUserActivity.class));

            }
        });



        TextView txt_corporate= (TextView)view.findViewById(R.id.txt_corporate);
        txt_corporate_id= (TextView)view.findViewById(R.id.txt_corporate_id);

        if(BeuSalonsSharedPrefrence.getVerifyCorporate()){

            txt_corporate.setText("Refer Colleagues");
            cor_id= BeuSalonsSharedPrefrence.getCorporateId();
        } else{
            txt_corporate.setText("Add Corporate ID");

        }



        LinearLayout linear_remaining=(LinearLayout)view.findViewById(R.id.linear_remaining_service);
        linear_remaining.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(getActivity(), RemainingServiceActivity.class);
                startActivity(in);
            }
        });

        LinearLayout linearLayoutMembership=(LinearLayout)view.findViewById(R.id.linear_membership);
        linearLayoutMembership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(getActivity(), MemberShipCardAcitvity.class);
                in.putExtra("from_home", true);
                startActivity(in);
            }
        });
        LinearLayout linear_membership_details=(LinearLayout)view.findViewById(R.id.linear_membership_details);
        linear_membership_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(getActivity(), MyMembershipDetails.class);
                startActivity(in);
            }
        });




        return view;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

        if(BeuSalonsSharedPrefrence.getVerifyCorporate()){

            if(BeuSalonsSharedPrefrence.getCorporateId()!=null){

                txt_corporate_id.setVisibility(View.VISIBLE);
                txt_corporate_id.setText("Corporate ID: "+cor_id);
            }else
                txt_corporate_id.setVisibility(View.GONE);
        }
    }

    private void logAddCorporateIdClickEvent () {
        Log.e("AddCorporateIdClick","fine");
        logger.logEvent(AppConstant.AddCorporateIdClick);
    }
    private void logAddCorporateIdClickFireBaseEvent () {
        Log.e("AddCorporateIdClickfire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.AddCorporateIdClick,bundle);
    }

    private void logReferColleaguesEvent () {
        Log.e("ReferColleagues","fine");
        logger.logEvent(AppConstant.ReferColleagues);
    }
    private void logReferColleaguesFireBaseEvent () {
        Log.e("ReferColleaguesFire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.ReferColleagues,bundle);
    }
//    @Subscribe
//    public void receiveOttoLoginInfo(OttoUserInfo ottoUserInfo){
//
//        Log.i(TAG, "hey i'm in receiveOttoLoginInfo");
//
//        if(ottoUserInfo !=null){
//
//
//
//
//
//
//
//        }
//
//    }



//    @Subscribe
//    public void receiveOttoImageUri(OttoImageUri ottoImageUri){
//
//        Log.i(TAG, "hey i'm in receiverOttoMessage");
//
//        Uri imageUri= ottoImageUri.getUri();
//
//
//        if(imageUri !=null){
//
//            ImageLoad imageLoad= new ImageLoad();
//            imageLoad.execute(imageUri);
//        }
//
//    }
//
//    @Subscribe
//    public void receiveOttoRemovePhoto(OttoRemovePhoto ottoRemovePhoto){
//
////        img_profilePic.setImageResource(R.drawable.travel_insurance_bag);
//    }

//    private class ImageLoad extends AsyncTask<Uri, Void, Void>{
//
//        Bitmap decodedImage=null;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//        @Override
//        protected Void doInBackground(Uri... imageUri) {
//
//            Uri uri= imageUri[0];
//            Bitmap image= null;
//
//
//            try{
//                image= MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
//                ByteArrayOutputStream byteArrayOutputStream= new ByteArrayOutputStream();
//                image.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
//                decodedImage= BitmapFactory.decodeStream(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
//
//                byteArrayOutputStream.close();
//                byteArrayOutputStream.flush();
//                image.recycle();
//
//
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//
//            return null;
//        }
//
//        @Override
//        protected void onProgressUpdate(Void... values) {
//            super.onProgressUpdate(values);
//        }
//
//        @Override
//        protected void onPostExecute(Void aVoid) {
//            super.onPostExecute(aVoid);
//
//            img_profilePic.setImageBitmap(decodedImage);
//        }
//
//
//    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }




}
