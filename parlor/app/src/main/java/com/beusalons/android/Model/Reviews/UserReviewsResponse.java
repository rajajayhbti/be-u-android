package com.beusalons.android.Model.Reviews;

import java.util.List;

/**
 * Created by myMachine on 12/21/2016.
 */

public class UserReviewsResponse {


    private Boolean success;
    private List<UserReviewsData> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<UserReviewsData> getData() {
        return data;
    }

    public void setData(List<UserReviewsData> data) {
        this.data = data;
    }
}
