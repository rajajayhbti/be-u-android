package com.beusalons.android.Model.MemberShip;

import java.util.List;

/**
 * Created by Ajay on 5/27/2017.
 */

public class MemberShipDetails {

    private String head;
    private List<MemberShipValue> values = null;

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public List<MemberShipValue> getValues() {
        return values;
    }

    public void setValues(List<MemberShipValue> values) {
        this.values = values;
    }
}
