package com.beusalons.android.Model.newServiceDeals.ServiceByDepartment;

import java.util.List;

/**
 * Created by myMachine on 5/30/2017.
 */

public class Addition {

    private List<Type> types = null;
    private String name;

    public List<Type> getTypes() {
        return types;
    }

    public void setTypes(List<Type> types) {
        this.types = types;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
