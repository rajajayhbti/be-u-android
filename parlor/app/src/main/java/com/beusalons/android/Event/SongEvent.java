package com.beusalons.android.Event;

/**
 * Created by myMachine on 05-Jan-18.
 */

public class SongEvent {

    private int songId;

    public SongEvent(int songId){
        this.songId= songId;
    }

    public int getSongId() {
        return songId;
    }

}
