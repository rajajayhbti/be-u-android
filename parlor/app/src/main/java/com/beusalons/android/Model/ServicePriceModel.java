package com.beusalons.android.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robbin Singh on 10/11/2016.
 */

public class ServicePriceModel {

    @SerializedName("priceId")
    @Expose
    private Integer priceId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("tax")
    @Expose
    private Integer tax;
    @SerializedName("estimatedTime")
    @Expose
    private Integer estimatedTime;
    @SerializedName("additions")
    @Expose
    private List<AdditionsModel> additions = new ArrayList<AdditionsModel>();

    private ArrayList<Object> childObjectList;



    /**
     *
     * @return
     * The priceId
     */
    public Integer getPriceId() {
        return priceId;
    }

    /**
     *
     * @param priceId
     * The priceId
     */
    public void setPriceId(Integer priceId) {
        this.priceId = priceId;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The price
     */
    public Integer getPrice() {
        return price;
    }

    /**
     *
     * @param price
     * The price
     */
    public void setPrice(Integer price) {
        this.price = price;
    }

    /**
     *
     * @return
     * The tax
     */
    public Integer getTax() {
        return tax;
    }

    /**
     *
     * @param tax
     * The tax
     */
    public void setTax(Integer tax) {
        this.tax = tax;
    }

    /**
     *
     * @return
     * The estimatedTime
     */
    public Integer getEstimatedTime() {
        return estimatedTime;
    }

    /**
     *
     * @param estimatedTime
     * The estimatedTime
     */
    public void setEstimatedTime(Integer estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public ArrayList<Object> getChildObjectList() {
        return childObjectList;
    }

    public List<AdditionsModel> getAdditions() {
        return additions;
    }

    public void setAdditions(List<AdditionsModel> additions) {
        this.additions = additions;
    }

    public void setChildObjectList(ArrayList<Object> childObjectList) {
        this.childObjectList = childObjectList;
    }
}
