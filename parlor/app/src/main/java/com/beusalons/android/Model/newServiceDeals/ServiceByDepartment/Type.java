package com.beusalons.android.Model.newServiceDeals.ServiceByDepartment;

/**
 * Created by myMachine on 5/30/2017.
 */

public class Type {

    private double percentageDifference;
    private int additions;
    private String name;
    private String imageUrl;

    private String service_name;
    private String service_deal_id;
    private String type;                    //deal type or service type
    private int service_code;
    private int price_id;

    private int price;
    private int menu_price;
    private int save_per;
    private boolean isCheck= false;


    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPrice_id() {
        return price_id;
    }

    public void setPrice_id(int price_id) {
        this.price_id = price_id;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getService_deal_id() {
        return service_deal_id;
    }

    public int getService_code() {
        return service_code;
    }

    public void setService_code(int service_code) {
        this.service_code = service_code;
    }

    public void setService_deal_id(String service_deal_id) {
        this.service_deal_id = service_deal_id;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getMenu_price() {
        return menu_price;
    }

    public void setMenu_price(int menu_price) {
        this.menu_price = menu_price;
    }

    public int getSave_per() {
        return save_per;
    }

    public void setSave_per(int save_per) {
        this.save_per = save_per;
    }

    public double getPercentageDifference() {
        return percentageDifference;
    }

    public void setPercentageDifference(double percentageDifference) {
        this.percentageDifference = percentageDifference;
    }

    public int getAdditions() {
        return additions;
    }

    public void setAdditions(int additions) {
        this.additions = additions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
