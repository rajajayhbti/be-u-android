package com.beusalons.android.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robbin Singh on 03/11/2016.
 */

public class ParlorModel {

    public static final int PARLOR_TYPE = 0;            //default yeh hai
    public static final int PROGRESS_TYPE = 1;

    private long id;
    private String name;
    private String parlorId;
    private List<ParlorImagesModel> images = new ArrayList<ParlorImagesModel>();
    private String gender;
    private String address1;
    private String address2;
    private double rating;
    private int price;
    private String distance;

    private String openingTime;
    private String closingTime;

    private List<Integer> services = new ArrayList<Integer>();

    private int parlorType;                                                 //class a[red=0] parlor or class b[blue=1] parlor

    private Boolean favourite;

    private int type= 0;                        //loading ke liye hai

    public ParlorModel(int type){
        this.type= type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public String getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(String closingTime) {
        this.closingTime = closingTime;
    }

    public Boolean getFavourite() {
        return favourite;
    }

    public void setFavourite(Boolean favourite) {
        this.favourite = favourite;
    }

    public int getParlorType() {
        return parlorType;
    }

    public void setParlorType(int parlorType) {
        this.parlorType = parlorType;
    }

    public List<ParlorImagesModel> getImages() {
        return images;
    }

    public void setImages(List<ParlorImagesModel> images) {
        this.images = images;
    }

    @SerializedName("parlorservices")
    @Expose
    private List<ParlorServicesModel> parlorservices = new ArrayList<ParlorServicesModel>();

    public List<ParlorServicesModel> getParlorservices() {
        return parlorservices;
    }

    public void setParlorservices(List<ParlorServicesModel> parlorservices) {
        this.parlorservices = parlorservices;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    /**

     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The parlorId
     */
    public String getParlorId() {
        return parlorId;
    }

    /**
     *
     * @param parlorId
     * The parlorId
     */
    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }


    /**
     *
     * @return
     * The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     * The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     *
     * @return
     * The address
     */
    public String getAddress1() {
        return address1;
    }

    /**
     *
     * @param address
     * The address
     */
    public void setAddress1(String address) {
        this.address1 = address;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    /**
     *
     * @return
     * The price
     */
    public int getPrice() {
        return price;
    }

    /**
     *
     * @param price
     * The price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     *
     * @return
     * The distance
     */
    public String getDistance() {
        return distance;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    /**
     *
     * @param distance
     * The distance
     */
    public void setDistance(String distance) {
        this.distance = distance;
    }

    /**
     *
     * @return
     * The services
     */
    public List<Integer> getServices() {
        return services;
    }

    /**
     *
     * @param services
     * The services
     */
    public void setServices(List<Integer> services) {
        this.services = services;
    }

}
