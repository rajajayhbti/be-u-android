package com.beusalons.android.Adapter.BookingSummary;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Event.BookingSummaryDiscountChanges;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.R;
import com.beusalons.android.Task.UserCartTask;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by myMachine on 12/12/2016.
 */

public class AppointmentAdapter extends RecyclerView.Adapter<AppointmentAdapter.MyViewHolder> {

    private Activity context;
    private List<UserServices> list= new ArrayList<>();
    private UserCart cart;

    public AppointmentAdapter( Activity context, List<UserServices> list, UserCart cart){

        this.context= context;
        this.list= list;
        this.cart= cart;
    }


    public void updateAdapter(List<UserServices> list, UserCart cart){

        this.list= list;
        this.cart= cart;
        notifyDataSetChanged();
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final UserServices service= list.get(position);

        holder.txt_name.setText(fromHtml(service.getName()));
        holder.txt_quanitity.setText(""+ service.getQuantity());
        holder.txt_price.setText(AppConstant.CURRENCY+ (int)service.getPrice());

        if(service.getDescription()==null || service.getDescription().equalsIgnoreCase("")){

            holder.linear_detail.setVisibility(View.GONE);
        }else{

            holder.linear_detail.setVisibility(View.VISIBLE);
        }

        holder.linear_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(service.getName());
                builder.setMessage(service.getDescription())
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.dismiss();
                            }
                        });

                builder.create().show();
            }
        });

        holder.linear_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        new Thread(new UserCartTask(context, cart, service, false, true)).start();
                        list.remove(position);

                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                notifyDataSetChanged();
                            }
                        });

                        EventBus.getDefault().post(new BookingSummaryDiscountChanges());
                    }
                }).start();
            }

        });

    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_name, txt_price, txt_quanitity;     //txt_price booking ke liye hai
        private LinearLayout linear_remove, linear_detail;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_name= (TextView)itemView.findViewById(R.id.txt_name);
            txt_quanitity= (TextView)itemView.findViewById(R.id.txt_quantity);
            txt_price= (TextView)itemView.findViewById(R.id.txt_price);

            linear_remove= (LinearLayout)itemView.findViewById(R.id.linear_remove);
            linear_detail= (LinearLayout)itemView.findViewById(R.id.linear_detail);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.activity_booking_summary_recyl, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }
}
