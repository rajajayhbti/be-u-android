package com.beusalons.android;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Dialog.ShowDetailsBillSummary;
import com.beusalons.android.Dialog.SubsBillSummeryDialog;
import com.beusalons.android.Event.BookingSummaryPromoEvent;
import com.beusalons.android.Event.BuySubscriptionOnBillEvent;
import com.beusalons.android.Event.CancelAppointment;
import com.beusalons.android.Event.CouponCodeApply;
import com.beusalons.android.Event.SayNoToSubscription;
import com.beusalons.android.Fragment.CouponFragment;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Helper.CheckConnection;
import com.beusalons.android.Model.AppointmentDetail.AppointmentDetailPost;
import com.beusalons.android.Model.AppointmentDetail.AppointmentDetailResponse;
import com.beusalons.android.Model.Appointments.AppointmentPost;
import com.beusalons.android.Model.Appointments.PastReview;
import com.beusalons.android.Model.BillSummery.ApplyPromoRequest;
import com.beusalons.android.Model.BillSummery.CouponAppliedResponse;
import com.beusalons.android.Model.PaymentSuccessPost;
import com.beusalons.android.Model.PaymentSuccessResponse;
import com.beusalons.android.Model.SalonHome.HomeResponse;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.bumptech.glide.Glide;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.math.BigDecimal;
import java.util.Currency;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BillSummaryActivity extends AppCompatActivity implements PaymentResultListener {

    private static final String TAG= BillSummaryActivity.class.getSimpleName();

    private TextView txt_suggetion, txt_menu_price, testt, txt_grand_total,txt_saved,  txt_100;

    private TextView txt_membership_suggetion,txt_sugg_membership_subtitle;

    private CheckBox cbUsePoints,cb_use_membership,cb_use_subscription;

    private LinearLayout linear_online,ll_sugg_subscription_container;
    CardView ll_sugg_membership_container;
    ImageView imgViewBack;
    private Button btn_pay;

    private int payOption= 2;
    private String applyCouponCode;
    private AppointmentDetailResponse appointmentDetailResponse;

    //    CartModel cartModel = new CartModel();
    private String userId, accessToken, mobile, email;       //yeh appointment id api hit karne ke liye bhej raha hoon

    private  String appt_id="", phone_number="";

    private View mContentView;
    private View mLoadingView;

    private double payable_amount;
    private TextView txtShowDetails,txt_freeBee_points,txt_sub_redeem,testttt;
    private double subtotalTemp=0;
    private RelativeLayout rl_threading,rl_membership,rl_subscription;
    private AppointmentPost appointmentPost;
    private CheckBox cbUseThreading;
    private TextView txt_freebee_se_tnc;
    private RadioButton radio_cash_payment;
    AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;
    private TextView txt_use_coupon,txt_subscription_suggetion,txt_sugg_subs_subtitle,txt_use_coupon_,txt_choose_coupon,txt_buy_subs;
    public TextView txt_buy_membership,txt_apply_coupon;
    private LinearLayout linear_coupon,ll_freebies;
    private EditText etxt_coupon,etxt_refer_code;
    private HomeResponse homeResponse;
    private RelativeLayout rl_member_sugg,rl_sugg_membership_buy,rl_sugg_subs_buy,rl_subs_sugg;
    private boolean membershipBuying=false;
    private boolean subscriptionBuying=false;
    private ImageView imgViewMemberShipCard;
    private boolean isCouponApplied=false;
    private String couponCode="";
    private boolean isFirstDialog=true;
    private int tempSubsId;
    private TextView txt_subs_more;
    private EditText txt_paste_coupon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_summary);
        setToolBar();
        logger = AppEventsLogger.newLogger(BillSummaryActivity.this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(BillSummaryActivity.this);

        Bundle bundle= getIntent().getExtras();
        if(bundle!=null && bundle.containsKey("appointment_post")){
            appointmentPost= new Gson().fromJson(bundle.getString("appointment_post"), AppointmentPost.class);
            appt_id=appointmentPost.getAppointmentId();
            homeResponse= new Gson().fromJson(bundle.getString("membership"), HomeResponse.class);
        }


        mContentView= findViewById(R.id.relative_bill_summary);
        mLoadingView= findViewById(R.id.loading_spinner_bill_summary);
        btn_pay= (Button)findViewById(R.id.btn_bill_summary_pay);
        txt_paste_coupon= findViewById(R.id.txt_paste_coupon);
        //initially gone kara
        mContentView.setVisibility(View.GONE);
        btn_pay.setVisibility(View.GONE);
        mLoadingView.setVisibility(View.VISIBLE);

        radio_cash_payment= (RadioButton) findViewById(R.id.radio_cash_payment);
        userId= BeuSalonsSharedPrefrence.getUserId();
        accessToken= BeuSalonsSharedPrefrence.getAccessToken();
        email= BeuSalonsSharedPrefrence.getUserEmail();
        mobile= BeuSalonsSharedPrefrence.getUserPhone();

        rl_threading=(RelativeLayout)findViewById(R.id.rl_threading);
        rl_membership=(RelativeLayout)findViewById(R.id.rl_membership);
        rl_subscription=(RelativeLayout)findViewById(R.id.rl_subscription);
        ll_freebies=(LinearLayout) findViewById(R.id.ll_freebies);

        testt= (TextView)findViewById(R.id.testt);
        txt_membership_suggetion= (TextView)findViewById(R.id.txt_membership_suggetion);
        imgViewMemberShipCard= (ImageView) findViewById(R.id.imgView_memberSHipCard);
        txt_sugg_membership_subtitle= (TextView)findViewById(R.id.txt_sugg_membership_subtitle);
        txt_menu_price= (TextView)findViewById(R.id.txt_bill_summary_menu_price);
        txt_freeBee_points= (TextView)findViewById(R.id.txt_freeBee_points);

        txt_100= (TextView)findViewById(R.id.txt_100);
        txt_apply_coupon= (TextView)findViewById(R.id.txt_apply_coupon);
        txt_freebee_se_tnc= (TextView)findViewById(R.id.txt_frebie_use_tnc);

        //  txt_online_discount= (TextView)findViewById(R.id.txt_online_discount);
        // linear_online= (LinearLayout)findViewById(R.id.linear_online);

        txt_saved= (TextView)findViewById(R.id.txt_bill_summary_saved) ;
        txt_suggetion= (TextView)findViewById(R.id.txt_suggetion);
        //   txt_discount= (TextView)findViewById(R.id.txt_bill_summary_discount);
        //  txt_tax= (TextView)findViewById(R.id.txt_bill_summary_tax);
        txt_grand_total= (TextView)findViewById(R.id.txt_bill_summary_grand_total);
//        txt_loyalty= (TextView)findViewById(R.id.txt_bill_summary_loyalty);

        txtShowDetails=(TextView)findViewById(R.id.txt_show_detail_bill);
        cbUsePoints= (CheckBox) findViewById(R.id.cb_use_points);
        cb_use_membership= (CheckBox) findViewById(R.id.cb_use_membership);
        cb_use_subscription= (CheckBox) findViewById(R.id.cb_use_subscription);
        cbUseThreading= (CheckBox) findViewById(R.id.cb_use_threading);
        ll_sugg_membership_container=(CardView) findViewById(R.id.ll_sugg_membership_container);
        ll_sugg_subscription_container=(LinearLayout) findViewById(R.id.ll_sugg_subscription_container);
        rl_member_sugg=(RelativeLayout) findViewById(R.id.rl_member_sugg);
        rl_sugg_subs_buy=(RelativeLayout) findViewById(R.id.rl_sugg_subs_buy);
        rl_subs_sugg=(RelativeLayout) findViewById(R.id.rl_subs_sugg);
        rl_sugg_membership_buy=(RelativeLayout) findViewById(R.id.rl_sugg_membership_buy);
        txt_buy_membership=(TextView)findViewById(R.id.txt_buy_membership);
        txt_buy_subs=(TextView)findViewById(R.id.txt_buy_subs);
        txt_sugg_subs_subtitle=(TextView)findViewById(R.id.txt_sugg_subs_subtitle);
        txt_subs_more=(TextView)findViewById(R.id.txt_subs_more);
        txt_subscription_suggetion=(TextView)findViewById(R.id.txt_subscription_suggetion);
        txt_sub_redeem=(TextView)findViewById(R.id.txt_sub_redeem);
        testttt=(TextView)findViewById(R.id.testttt);
        rl_member_sugg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rl_sugg_membership_buy.getVisibility()==View.GONE)
                    rl_sugg_membership_buy.setVisibility(View.VISIBLE);
                else rl_sugg_membership_buy.setVisibility(View.GONE);
            }
        });
        rl_subs_sugg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rl_sugg_subs_buy.getVisibility()==View.GONE)
                    rl_sugg_subs_buy.setVisibility(View.VISIBLE);
                else rl_sugg_subs_buy.setVisibility(View.GONE);
            }
        });


        txt_buy_membership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                membershipBuying=true;
                cb_use_membership.setChecked(true);
//                createAppointment(false);
            }
        });
        txt_buy_subs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rl_subscription.setVisibility(View.VISIBLE);
                subscriptionBuying=true;

                cb_use_subscription.setChecked(true);
//                cb_use_subscription.performClick();

            }
        });
        if (BeuSalonsSharedPrefrence.getSubsBalance()>0  ){
            cb_use_subscription.setChecked(true);
        }else  cb_use_subscription.setChecked(false);


        if (appointmentPost.getBuyMembershipId()!=null &&
                appointmentPost.getBuyMembershipId().length()>0){
            radio_cash_payment.setVisibility(View.GONE);


            if (appointmentPost.getServices().size()>0){
                cbUsePoints.setClickable(true);
                cb_use_membership.setClickable(true);
                cb_use_membership.setChecked(true);


                if(
                        BeuSalonsSharedPrefrence.getMyLoyaltyPoints()>0){
                    cbUsePoints.setChecked(true);
                }
                rl_membership.setVisibility(View.VISIBLE);
                ll_freebies.setVisibility(View.VISIBLE);
            }else{
                rl_membership.setVisibility(View.GONE);
                ll_freebies.setVisibility(View.GONE);
            }



        }else{

            radio_cash_payment.setVisibility(View.VISIBLE);
            cb_use_membership.setChecked(false);
            if (BeuSalonsSharedPrefrence.getMembershipPoints()>0){
                cb_use_membership.setChecked(true);
            }else   cb_use_membership.setChecked(false);

            if(
                    BeuSalonsSharedPrefrence.getMyLoyaltyPoints()>0){
                cbUsePoints.setChecked(true);
            }
        }
        rl_threading.setVisibility(View.GONE);
        txt_freeBee_points.setText("You Have "+BeuSalonsSharedPrefrence.getMyLoyaltyPoints()+" B-Cash.");

        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (appointmentDetailResponse.getData().getSubscriptionAmount()==0&&!BeuSalonsSharedPrefrence.getIsSubscribed()&& appointmentDetailResponse.getData().getSubscriptionSuggestion()!=null){
                    // if (isFirstDialog){
                    FragmentManager manager = getFragmentManager();
                    SubsBillSummeryDialog summeryDialog=new SubsBillSummeryDialog();
                    Bundle args = new Bundle();
                    args.putString("subsTitle",appointmentDetailResponse.getData().getSubscriptionSuggestion().getHeading2() );
                    args.putString("term", appointmentDetailResponse.getData().getSubscriptionSuggestion().getTnC());
                    args.putString("sugg", appointmentDetailResponse.getData().getSubscriptionSuggestion().getSuggestion());
                    args.putDouble("totalAmount",appointmentDetailResponse.getData().getPayableAmount());
                    args.putDouble("subsAmount",appointmentDetailResponse.getData().getSubscriptionSuggestion().getAmount());
                    args.putDouble("redeem",appointmentDetailResponse.getData().getSubscriptionSuggestion().getRedemption());
                    args.putDouble("payable",appointmentDetailResponse.getData().getSubscriptionSuggestion().getPayableAmount());
                    args.putDouble("usable",appointmentDetailResponse.getData().getSubscriptionSuggestion().getRedemableAmount());
                    args.putString("userSubscriptionOfferTerms",appointmentDetailResponse.getData().getSubscriptionSuggestion().getUserSubscriptionOfferTerms());
                    summeryDialog.setArguments(args);
                    summeryDialog.show(manager,"test");
                    isFirstDialog=false;

                    //  }
                }else{
                    logPayButtonClickedEvent();
                    logPayButtonClickedFireBaseEvent();
                    if(payOption==1){
                        //cash payment
                        if(CheckConnection.isConnected(BillSummaryActivity.this)){
                            mContentView.setVisibility(View.GONE);
                            btn_pay.setVisibility(View.GONE);
                            mLoadingView.setVisibility(View.VISIBLE);
                            bookCashPayment("", payable_amount, 1);

                        }else{
                            mContentView.setVisibility(View.VISIBLE);
                            btn_pay.setVisibility(View.VISIBLE);
                            mLoadingView.setVisibility(View.GONE);
                            Toast.makeText(BillSummaryActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                        }

                    }else if(payOption==2){

                        if(CheckConnection.isConnected(BillSummaryActivity.this)){
                            mContentView.setVisibility(View.GONE);
                            btn_pay.setVisibility(View.GONE);
                            mLoadingView.setVisibility(View.VISIBLE);

                            if (payable_amount>0)
                                razorpay();
                            else  bookCashPayment("", payable_amount, 1);

                        }else{
                            mContentView.setVisibility(View.VISIBLE);
                            btn_pay.setVisibility(View.VISIBLE);
                            mLoadingView.setVisibility(View.GONE);
                            Toast.makeText(BillSummaryActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                        }

                    }else {

                        Log.i(TAG, "I'm in payOption others");
                    }
                }



            }
        });


        txtShowDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                logShowDetailEvent();
                logShowDetailFireBaseEvent();
                if (payOption==2){

                    if (cbUsePoints.isChecked()){
                        //     new ShowDetailsBillSummary(BillSummaryActivity.this,appointmentDetailResponse,true,true,false,false);
                        if (cb_use_membership.isChecked() &&appointmentPost.getBuyMembershipId()==null){
                            new ShowDetailsBillSummary(BillSummaryActivity.this,appointmentDetailResponse,true,true,false,true);
                        }else if(appointmentPost.getBuyMembershipId()!=null &&
                                appointmentPost.getBuyMembershipId().length()>0){
                            new ShowDetailsBillSummary(BillSummaryActivity.this,appointmentDetailResponse,true,true,true,true);
                        }else{
                            new ShowDetailsBillSummary(BillSummaryActivity.this,appointmentDetailResponse,true,true,false,false);
                        }
                    }else{
                        if (appointmentPost.getBuyMembershipId()!=null &&
                                appointmentPost.getBuyMembershipId().length()>0){
                            new ShowDetailsBillSummary(BillSummaryActivity.this,appointmentDetailResponse,true,false,true,true);
                        }else{
                            if (cb_use_membership.isChecked()){
                                new ShowDetailsBillSummary(BillSummaryActivity.this,appointmentDetailResponse,true,false,false,true);
                            }else new ShowDetailsBillSummary(BillSummaryActivity.this,appointmentDetailResponse,true,false,false,false);

                        }

                    }


                }else{
                    if (cbUsePoints.isChecked()){
                        new ShowDetailsBillSummary(BillSummaryActivity.this,appointmentDetailResponse,false,true,false,false);
                    }else{
                        new ShowDetailsBillSummary(BillSummaryActivity.this,appointmentDetailResponse,false,false,false,false);
                    }
                    if (cb_use_membership.isChecked()){
                        new ShowDetailsBillSummary(BillSummaryActivity.this,appointmentDetailResponse,false,false,false,true);
                    }else new ShowDetailsBillSummary(BillSummaryActivity.this,appointmentDetailResponse,false,false,false,false);
                }

            }
        });

        cbUsePoints.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    createAppointment();
                    /*payable_amount=   payable_amount-BeuSalonsSharedPrefrence.getMyLoyaltyPoints();
                    txt_grand_total.setText(AppConstant.CURRENCY+ String.valueOf(payable_amount));
                    btn_pay.setText("PAY "+AppConstant.CURRENCY+payable_amount);*/

                }else{
                    createAppointment();
                    /*payable_amount=   payable_amount+BeuSalonsSharedPrefrence.getMyLoyaltyPoints();
                    txt_grand_total.setText(AppConstant.CURRENCY+ String.valueOf(payable_amount));
                    btn_pay.setText("PAY "+AppConstant.CURRENCY+payable_amount);*/

                }
            }
        });

        cbUseThreading.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                createAppointment();
            }
        });
        cb_use_membership.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                createAppointment();
            }
        });
        cb_use_subscription.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                createAppointment();
            }
        });

        txt_use_coupon= (TextView)findViewById(R.id.txt_use_coupon);
        txt_use_coupon_= (TextView)findViewById(R.id.txt_use_coupon_);
        txt_choose_coupon= (TextView)findViewById(R.id.txt_choose_coupon);
        etxt_coupon= (EditText)findViewById(R.id.etxt_coupon);
        etxt_refer_code= (EditText)findViewById(R.id.etxt_refer_code);
        final Button btn_coupon= (Button)findViewById(R.id.btn_coupon);
        linear_coupon= (LinearLayout)findViewById(R.id.linear_coupon);

        txt_choose_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                new ApplyPromoCodeDialogue(BillSummaryActivity.this,userId,accessToken,appt_id);

                CouponFragment fragment= new CouponFragment();
                Bundle bundle= new Bundle();
                bundle.putBoolean("has_coupon", true);
                bundle.putString("apptId",appt_id);
                fragment.setArguments(bundle);
                fragment.show(getSupportFragmentManager(), "coupon");

                //    linear_coupon.setVisibility(View.VISIBLE);
                //    txt_use_coupon.setVisibility(View.GONE);
            }
        });
        etxt_refer_code.setOnEditorActionListener(new EditText.OnEditorActionListener() {



            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    appointmentPost.setSubscriptionReferralCode(etxt_refer_code.getText().toString());
                    createAppointment();
                    return true;
                }
                return false;
            }
        });

        txt_apply_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  if(etxt_coupon.getText().toString().length()==0)
                    Toast.makeText(BillSummaryActivity.this, "Enter Coupon Code", Toast.LENGTH_LONG).show();
                else
                    postPromoCode(etxt_coupon.getText().toString());
*/
                applyCouponCode=txt_paste_coupon.getText().toString();

//                Log.e("applyCouponCode",event.getCouponCode());
                isCouponApplied=true;
                couponCode="2";
                createAppointment();
            }
        });


        createAppointment();

    }

    public void postPromoCode(String code){

        mLoadingView.setVisibility(View.VISIBLE);

        ApplyPromoRequest applyPromoRequest=new ApplyPromoRequest();
        applyPromoRequest.setAccessToken(accessToken);
        applyPromoRequest.setUserId(userId);
        applyPromoRequest.setApppointment(appt_id);
        applyPromoRequest.setCouponCode(code);

        Retrofit retrofit1 = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit1.create(ApiInterface.class);
        Call<CouponAppliedResponse> call= apiInterface.postApplyCoupon(applyPromoRequest);
        call.enqueue(new Callback<CouponAppliedResponse>() {
            @Override
            public void onResponse(Call<CouponAppliedResponse> call, Response<CouponAppliedResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().isSuccess()){

//                        EventBus.getDefault().post(new BookingSummaryPromoEvent(response.body().getData().getLoyalityPoints()));

//                        cbUsePoints.setChecked(true);
//                        BeuSalonsSharedPrefrence.setMyLoyaltyPoints(response.body().getData().getLoyalityPoints());
//                        txt_freeBee_points.setText("Total Freebie Points Are "+response.body().getData().getLoyalityPoints());
//                        createAppointment(true);
                        isCouponApplied=true;
                        //        getUserDetails();

                        Toast.makeText(BillSummaryActivity.this ,response.body().getData().getMessage(),Toast.LENGTH_LONG).show();


                    }else{

                        mLoadingView.setVisibility(View.GONE);
                        etxt_coupon.setText("");
                        Toast.makeText(BillSummaryActivity.this,response.body().getMessage(),Toast.LENGTH_LONG).show();

                    }




                }else{

                    mLoadingView.setVisibility(View.GONE);
                    etxt_coupon.setText("");
                    Toast.makeText(BillSummaryActivity.this,response.message(),Toast.LENGTH_LONG).show();
                }



            }

            @Override
            public void onFailure(Call<CouponAppliedResponse> call, Throwable t) {
                Log.i("couponerror", "value: "+ t.getStackTrace()+ "  "+ t.getMessage()+ " " + t.getLocalizedMessage());
                mLoadingView.setVisibility(View.GONE);
                Toast.makeText(BillSummaryActivity.this, "Looks Like The Server Is Taking To Long To Respond\n" +
                        " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
            }
        });

    }



    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {

            case R.id.radio_cash_payment:
                payOption=1;

                rl_threading.setVisibility(View.GONE);
                if(appointmentDetailResponse.getData().isOnlinePaymentDiscountAvailable()){

                    txt_100.setVisibility(View.VISIBLE);
                    //  txt_online_discount.setText("(-) "+AppConstant.CURRENCY+0);
                    //    linear_online.setVisibility(View.VISIBLE);
                }else{

                    //   txt_100.setVisibility(View.GONE);
                    //  linear_online.setVisibility(View.GONE);
                }
                txt_100.setText(appointmentDetailResponse.getData().getDiscountMessage());

              /*  Double menu_price=0.0;
                int services= appointmentDetailResponse.getData().getServices().size();
                for(int i=0; i<services;i++){

                    menu_price+= appointmentDetailResponse.getData().getServices().get(i).getActualPrice();
                    Log.i("invaaaaaa", "value in quanitty: "+ appointmentDetailResponse.getData().getServices().get(i).getQuantity());
                }
                Double saved=  menu_price- appointmentDetailResponse.getData().getPayableAmount();
                if (saved > 0.0) {

                    txt_saved.setVisibility(View.VISIBLE);


                    txt_saved.setText("Saved "+AppConstant.CURRENCY+saved);
                    txt_saved.setBackgroundResource(R.drawable.discount_seletor);
                }else {
                    txt_saved.setVisibility(View.INVISIBLE);
                }
*/
                payable_amount= appointmentDetailResponse.getData().getPayableAmount();

                txt_grand_total.setText(AppConstant.CURRENCY+ String.valueOf(payable_amount));
                //       txt_menu_price.setText(AppConstant.CURRENCY+ String.valueOf(menu_price));
                btn_pay.setText("PAY "+AppConstant.CURRENCY+appointmentDetailResponse.getData().getPayableAmount());
                createAppointment();

                break;

            case R.id.radio_razorpay:
                if (checked)
                    payOption= 2;
                Log.i(TAG, "i'm in razorpay");

                if (appointmentDetailResponse.getData().isThreadingDiscountAvailable() && appointmentDetailResponse.getData().getPayableAmount()>200.00 ){
                    //   rl_threading.setVisibility(View.VISIBLE);
                    //  cbUseThreading.setVisibility(View.VISIBLE);
                }else{
                    rl_threading.setVisibility(View.GONE);
                    //  cbUseThreading.setVisibility(View.GONE);
                }

                if(appointmentDetailResponse.getData().isOnlinePaymentDiscountAvailable()){

                    //   txt_100.setVisibility(View.VISIBLE);
                    // linear_online.setVisibility(View.VISIBLE);
                    int discounted_price= (int)(0.10*appointmentDetailResponse.getData().getSubtotal());
                    payable_amount= appointmentDetailResponse.getData().getPayableAmount();
                    txt_grand_total.setText(AppConstant.CURRENCY+ String.valueOf(payable_amount));


                    if(appointmentDetailResponse.getData().getPayableAmount()>=200.00){


                        Double saved_online=  appointmentDetailResponse.getData().getTotalSaved()+
                                appointmentDetailResponse.getData().getLoyalityPoints()+appointmentDetailResponse.getData().getOnlineDiscount();

                        Log.i("maintohyahahoon", "value : "+ saved_online+ "  "+ saved_online.intValue()
                                + appointmentDetailResponse.getData().getTotalSaved()
                                +"  "+appointmentDetailResponse.getData().getLoyalityPoints() );

                        txt_saved.setText("Saved "+AppConstant.CURRENCY+saved_online.intValue());
                        txt_saved.setBackgroundResource(R.drawable.discount_seletor);
                        txt_saved.setVisibility(View.VISIBLE);

                        //  txt_online_discount.setText("(-) "+AppConstant.CURRENCY+100);

                        btn_pay.setText("PAY "+AppConstant.CURRENCY+String.valueOf(payable_amount));
                    }else{

                        cbUseThreading.setVisibility(View.GONE);

                        Double saved_online=  appointmentDetailResponse.getData().getTotalSaved() +
                                appointmentDetailResponse.getData().getLoyalityPoints();
                        if (saved_online > 0.0) {

                            txt_saved.setText("Saved "+AppConstant.CURRENCY+saved_online.intValue());
                            txt_saved.setBackgroundResource(R.drawable.discount_seletor);
                            txt_saved.setVisibility(View.VISIBLE);
                        }else {

                            txt_saved.setVisibility(View.INVISIBLE);
                        }
                        //  payable_amount= appointmentDetailResponse.getData().getPayableAmount();

                        txt_grand_total.setText(AppConstant.CURRENCY+String.valueOf(payable_amount));

                        //   txt_online_discount.setText("(-) "+AppConstant.CURRENCY+0);

                        btn_pay.setText("PAY "+AppConstant.CURRENCY+String.valueOf(payable_amount));

                    }

                }else{

                    //  txt_100.setVisibility(View.GONE);
                    //        linear_online.setVisibility(View.GONE);

                    Double saved_online=  appointmentDetailResponse.getData().getTotalSaved() +
                            appointmentDetailResponse.getData().getLoyalityPoints();


                    if (saved_online > 0.0) {

                        txt_saved.setText("Saved "+AppConstant.CURRENCY+saved_online.intValue());
                        txt_saved.setBackgroundResource(R.drawable.discount_seletor);
                        txt_saved.setVisibility(View.VISIBLE);
                    }else {

                        txt_saved.setVisibility(View.INVISIBLE);
                    }
                    payable_amount= appointmentDetailResponse.getData().getPayableAmount();
                    txt_grand_total.setText(AppConstant.CURRENCY + String.valueOf(payable_amount));
                    btn_pay.setText("PAY "+AppConstant.CURRENCY + String.valueOf(payable_amount));
                    createAppointment();
                }
                break;
        }
    }




    public void razorpay(){

        Log.i("investigaaaaaaaaa", "value in the stuff: "+ payable_amount+
                "      "+ appt_id);

        final Activity activity = this;
        final Checkout checkout = new Checkout();
        checkout.setImage(R.drawable.ic_beu_razorpay);
        try {
            JSONObject options = new JSONObject();

            options.put("name", "Be U Salons");
            options.put("description", "OrderID: "+ appt_id);
            //You can omit the image option to fetch the image from dashboard
//            options.put("image", "https://rzp-mobile.s3.amazonaws.com/images/rzp.png");

            options.put("currency", "INR");
            options.put("appointmentId", appt_id); //parlor apt id is to be shown

            double amount=100 * payable_amount;

            Log.i("BillSummar", "value in payable amt: "+ amount+ "  "+ payable_amount);

            options.put("amount", amount);

            //theme color chnage
            JSONObject theme= new JSONObject();
            theme.put("color", "#d2232a");
            options.put("theme", theme);

            JSONObject notes= new JSONObject();
            notes.put("appointmentId", appt_id);
            options.put("notes", notes);

            //prefill
            JSONObject preFill = new JSONObject();
            preFill.put("email", email);
            preFill.put("contact", mobile);
            options.put("prefill", preFill);

            checkout.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }


    //cash payment method   --- for razor pay go to order summary activity
    public void bookCashPayment(String payment_method_key,final Double amount, int payment_method_id){

        PaymentSuccessPost post= new PaymentSuccessPost();

        post.setRazorpay_payment_id(payment_method_key);
        post.setAmount(amount);
        post.setPaymentMethod(payment_method_id);

        Log.i("BillSummar", "value in payable amt bookcashpayment api: "+ amount);
        if (cbUsePoints.isChecked()){
            post.setUseLoyalityPoints(1);
        }else  post.setUseLoyalityPoints(0);
        post.setAppointmentId(appt_id);
        post.setAccessToken(accessToken);
        post.setUserId(userId);

        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<PaymentSuccessResponse> call= apiInterface.bookCapturePayment(post);
        call.enqueue(new Callback<PaymentSuccessResponse>() {
            @Override
            public void onResponse(Call<PaymentSuccessResponse> call, Response<PaymentSuccessResponse> response) {

                Log.i(TAG, "i'm in cash payment success response ");
                if(response.isSuccessful()){
                    if(response.body().getSuccess()){

                        logCashPaymentEvent();
                        logBookingConfirmedEvent();
                        logCashPaymentFireBaseEvent();
                        logBookingConfirmedFireBaseEvent();
                        if ( cbUsePoints.isChecked()) {
                            logFreebiesPointsUsedEvent();
                            logFreebiesPointsUsedFireBaseEvent();
                        }
                        if (cbUseThreading.isChecked()){
                            logComplimentryThreadingFireBaseEvent();
                            logComplimentryThreadingEvent();
                        }

                        Log.i(TAG, "value in appt id: "+appointmentDetailResponse.getData().getAppointmentId());
                        Intent intent= new Intent(BillSummaryActivity.this, PaymentFailSuccessActivity.class);
                        Bundle bundle= new Bundle();
                        bundle.putString("appointmentId", appt_id);
                        bundle.putString("razorPaykey", "");
                        bundle.putDouble("amount", amount);
                        bundle.putDouble("serviceTotal", serviceTotal);
                        bundle.putString("subscriptionPopUpText", appointmentDetailResponse.getData().getSubscriptionPopUpText()!=null?appointmentDetailResponse.getData().getSubscriptionPopUpText():"");
                        bundle.putString("cashbackmessage", appointmentDetailResponse.getData().getAlertMessage());
                        bundle.putString("salonAppointmentId", String.valueOf(appointmentDetailResponse.getData().getParlorAppointmentId()));
                        bundle.putString("salonName", appointmentDetailResponse.getData().getParlorName());

                        if (homeResponse!=null)
                            bundle.putString("salonNumber", homeResponse.getData().getRealPhoneNumber());

                        bundle.putString("salonAddress", appointmentDetailResponse.getData().getParlorAddress());
                        bundle.putDouble("latitude", appointmentDetailResponse.getData().getParlorLatitude());
                        bundle.putDouble("longitude", appointmentDetailResponse.getData().getParlorLongitude());
                        bundle.putString("startsAt", appointmentDetailResponse.getData().getStartsAt());
                        bundle.putBoolean("isOnline", false);
                        bundle.putBoolean("isSuccess", true);
                        bundle.putBoolean("isNotification", false);
//                        intent.putExtra("appointment_post", new Gson().toJson(appointmentPost));
                        bundle.putString("appointment_post", new Gson().toJson(appointmentPost));

                        intent.putExtras(bundle);
                        startActivity(intent);
                        finish();
                    }else{
                        Log.i(TAG, "I'm in the  success false pe ...");
                        Intent intent= new Intent(BillSummaryActivity.this, PaymentFailSuccessActivity.class);Bundle bundle= new Bundle();
                        bundle.putString("appointmentId", appt_id);
                        bundle.putString("razorPaykey", "");
                        bundle.putDouble("amount", amount);
                        bundle.putDouble("serviceTotal", serviceTotal);
                        bundle.putString("salonAppointmentId", String.valueOf(appointmentDetailResponse.getData().getParlorAppointmentId()));
                        bundle.putString("salonName", appointmentDetailResponse.getData().getParlorName());
                        bundle.putString("salonAddress", appointmentDetailResponse.getData().getParlorAddress());
                        bundle.putDouble("latitude", appointmentDetailResponse.getData().getLatitude());
                        bundle.putDouble("longitude", appointmentDetailResponse.getData().getLongitude());
                        bundle.putString("startsAt", appointmentDetailResponse.getData().getStartsAt());
                        bundle.putBoolean("isOnline", false);
                        bundle.putBoolean("isSuccess", false);
                        bundle.putBoolean("isNotification", false);
                        if (cbUsePoints.isChecked()){
                            bundle.putInt("useFreebie",1);
                        }else   bundle.putInt("useFreebie",0);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        finish();
                    }
                }else{
                    Log.i(TAG, "I'm in the  success false pe ...");
                    Intent intent= new Intent(BillSummaryActivity.this, PaymentFailSuccessActivity.class);
                    Bundle bundle= new Bundle();
                    bundle.putString("appointmentId", appt_id);
                    bundle.putString("razorPaykey", "");
                    bundle.putDouble("amount", amount);
                    bundle.putDouble("serviceTotal", serviceTotal);
                    bundle.putString("salonAppointmentId", String.valueOf(appointmentDetailResponse.getData().getParlorAppointmentId()));
                    bundle.putString("salonName", appointmentDetailResponse.getData().getParlorName());
                    bundle.putString("salonAddress", appointmentDetailResponse.getData().getParlorAddress());
                    bundle.putDouble("latitude", appointmentDetailResponse.getData().getLatitude());
                    bundle.putDouble("longitude", appointmentDetailResponse.getData().getLongitude());
                    bundle.putString("startsAt", appointmentDetailResponse.getData().getStartsAt());
                    bundle.putBoolean("isOnline", false);
                    bundle.putBoolean("isSuccess", false);

                    bundle.putBoolean("isNotification", false);
                    if (cbUsePoints.isChecked()){
                        bundle.putInt("useFreebie",1);
                    }else   bundle.putInt("useFreebie",0);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<PaymentSuccessResponse> call, Throwable t) {

                //Todo: inform the user
                Toast.makeText(BillSummaryActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                mContentView.setVisibility(View.VISIBLE);
                btn_pay.setVisibility(View.VISIBLE);
                mLoadingView.setVisibility(View.INVISIBLE);
                Log.i(TAG, "i'm in payment failure response ");
            }
        });
    }

    @Override
    public void onPaymentSuccess(String s) {

        Log.i(TAG, "onPaymentSuccess: "+s);
        Intent intent= new Intent(BillSummaryActivity.this, PaymentFailSuccessActivity.class);
        Bundle bundle= new Bundle();
        bundle.putString("appointmentId", appt_id);
        bundle.putString("razorPayKey", s);                 //razorpay key
        bundle.putDouble("amount", payable_amount);
        bundle.putDouble("serviceTotal", serviceTotal);
        bundle.putString("salonAppointmentId", String.valueOf(appointmentDetailResponse.getData().getParlorAppointmentId()));
        bundle.putString("salonName", appointmentDetailResponse.getData().getParlorName());
        bundle.putString("salonNumber", homeResponse.getData().getRealPhoneNumber());
        bundle.putString("subscriptionPopUpText", appointmentDetailResponse.getData().getSubscriptionPopUpText()!=null?appointmentDetailResponse.getData().getSubscriptionPopUpText():"");
        bundle.putString("salonAddress", appointmentDetailResponse.getData().getParlorAddress());
        bundle.putDouble("latitude", appointmentDetailResponse.getData().getParlorLatitude());
        bundle.putDouble("longitude", appointmentDetailResponse.getData().getParlorLongitude());
        bundle.putString("startsAt", appointmentDetailResponse.getData().getStartsAt());
        bundle.putString("cashbackmessage", appointmentDetailResponse.getData().getAlertMessage());
        bundle.putString("appointment_post", new Gson().toJson(appointmentPost));
        bundle.putBoolean("isOnline", true);
        bundle.putBoolean("isSuccess", true);
        if (cbUsePoints.isChecked()){
            bundle.putInt("useFreebie",1);
            logFreebiesPointsUsedEvent();
            logFreebiesPointsUsedFireBaseEvent();
        }else   bundle.putInt("useFreebie",0);

        if (cbUseThreading.isChecked()){
            logComplimentryThreadingEvent();
            logComplimentryThreadingFireBaseEvent();
        }
        bundle.putBoolean("isNotification", false);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();

//        bookCaptureAppt(s, 100*(appointmentDetailResponse.getData().getPayableAmount()), 5);
    }

    @Override
    public void onPaymentError(int i, String s) {

        Log.i(TAG, "onPaymentFailure: "+s);
        Intent intent= new Intent(this, PaymentFailSuccessActivity.class);
        Bundle bundle= new Bundle();
        bundle.putString("appointmentId", appt_id);
        bundle.putString("razorPayKey", s);                     //razorpay key
        bundle.putDouble("amount", payable_amount);                 //payable amt
        bundle.putDouble("serviceTotal", serviceTotal);
        bundle.putString("salonAppointmentId", String.valueOf(appointmentDetailResponse.getData().getParlorAppointmentId()));
        bundle.putString("salonName", appointmentDetailResponse.getData().getParlorName());
        bundle.putString("salonAddress", appointmentDetailResponse.getData().getParlorAddress());
        bundle.putDouble("latitude", appointmentDetailResponse.getData().getLatitude());
        bundle.putDouble("longitude", appointmentDetailResponse.getData().getLongitude());
        bundle.putString("startsAt", appointmentDetailResponse.getData().getStartsAt());
        bundle.putBoolean("isOnline", true);
        bundle.putBoolean("isSuccess", false);
        bundle.putString("appointment_post", new Gson().toJson(appointmentPost));
        bundle.putBoolean("isNotification", false);
        if (cbUsePoints.isChecked()){
            bundle.putInt("useFreebie",1);
        }else   bundle.putInt("useFreebie",0);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    /**
     *
     * {@link android.support.v7.widget.RecyclerView.RecycledViewPool}
     */

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private   double serviceTotal=0;
    public void createAppointment(){

        mLoadingView.setVisibility(View.VISIBLE);
        btn_pay.setClickable(false);
        // Retrieve and cache the system's default "short" animation time.

        if (cbUsePoints.isChecked()){
            appointmentPost.setAppointmentId(appt_id);
            appointmentPost.setUseLoyalityPoints(true);
        }else{
            appointmentPost.setAppointmentId(appt_id);
            appointmentPost.setUseLoyalityPoints(false);
        }
        if (cb_use_subscription!=null && cb_use_subscription.isChecked()){
            appointmentPost.setUseSubscriptionCredits(true);
        }else appointmentPost.setUseSubscriptionCredits(false);

        if (subscriptionBuying){
            if (appointmentDetailResponse.getData().getSubscriptionSuggestion()!=null)
                appointmentPost.setSubscriptionId(appointmentDetailResponse.getData().getSubscriptionSuggestion().getSubscriptionId());
            else appointmentPost.setSubscriptionId(tempSubsId);
//            appointmentPost.setUseSubscriptionCredits(true);
            if (cb_use_subscription!=null && cb_use_subscription.isChecked()){
                appointmentPost.setUseSubscriptionCredits(true);
            }else appointmentPost.setUseSubscriptionCredits(false);

            if (etxt_refer_code.getText().length()>0){
                appointmentPost.setSubscriptionReferralCode(etxt_refer_code.getText().toString());
            }
        }else {
            //   appointmentPost.setBuyMembershipId(null);
        }

        if (etxt_refer_code.getText().length()>0){
            appointmentPost.setSubscriptionReferralCode(etxt_refer_code.getText().toString());
        }

        if (cbUseThreading.isChecked())
            appointmentPost.setUseFreeThreading(1);
        else
            appointmentPost.setUseFreeThreading(0);

        if (cb_use_membership.isChecked())
            appointmentPost.setUseMembershipCredits(1);
        else
            appointmentPost.setUseMembershipCredits(0);

        if (payOption==2)
            appointmentPost.setPaymentMethod(5);
        else
            appointmentPost.setPaymentMethod(1);

        if (membershipBuying){
            appointmentPost.setBuyMembershipId(appointmentDetailResponse.getData().getMembershipSuggestion().getMembershipId());
            appointmentPost.setUseMembershipCredits(1);
        }else {
            //   appointmentPost.setBuyMembershipId(null);
        }

        if (isCouponApplied){
            if (couponCode.equalsIgnoreCase("2")){
                appointmentPost.setCouponCodeId("2");
                appointmentPost.setCouponCode(applyCouponCode);
            }else
            appointmentPost.setCouponCodeId(couponCode);
        }
        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<AppointmentDetailResponse> call= apiInterface.createAppointment(appointmentPost);
        call.enqueue(new Callback<AppointmentDetailResponse>() {
            @Override
            public void onResponse(Call<AppointmentDetailResponse> call, Response<AppointmentDetailResponse> response) {

                Log.i(TAG, "i'm in on response susccess");

                if(response.isSuccessful()){

                    if(response.isSuccessful()){
                        if(response.body().isSuccess()){
                            appointmentDetailResponse = response.body();
                            Log.e("ashish","ashish :"+appointmentDetailResponse.getData().getSubscriptionPopUpText());
                            if(isCouponApplied){

                                if (response.body().getData().getCouponError()!=null && response.body().getData().getCouponError().length()>0){
                                    txt_use_coupon_.setText(response.body().getData().getCouponError());
                                }else

                                txt_use_coupon_.setVisibility(View.VISIBLE);
//ds
                                txt_paste_coupon.setText(applyCouponCode);
                                linear_coupon.setVisibility(View.GONE);
                              /*  txt_use_coupon.setVisibility(View.VISIBLE);

                                txt_use_coupon.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        //do nothing
                                    }
                                });*/
                            }




                            Log.i(TAG, "value in the response stuff: "+ response.body().getData().getAppointmentId()+ " latitude: "+
                                    response.body().getData().getLatitude()+ "   "+
                                    response.body().getData().isOnlinePaymentDiscountAvailable());


                            appt_id= appointmentDetailResponse.getData().getAppointmentId();
                            subtotalTemp=appointmentDetailResponse.getData().getSubtotal();
                            txt_suggetion.setText(appointmentDetailResponse.getData().getSuggestion());
                            if (appointmentDetailResponse.getData().getSubscriptionAmount()>0){
                                etxt_refer_code.setVisibility(View.VISIBLE);
                                etxt_refer_code.setText(BeuSalonsSharedPrefrence.getSubsRefer());
                            }else  etxt_refer_code.setVisibility(View.GONE);

                            if (appointmentDetailResponse.getData().getSubscriptionAmount()>0){
                                if (etxt_refer_code.getText().length()>0){
                                    appointmentPost.setSubscriptionReferralCode(etxt_refer_code.getText().toString());
                                }
                            }
                            if (appointmentDetailResponse.getData().getUsableMembership()!=null &&
                                    appointmentDetailResponse.getData().getUsableMembership().size()>0&& appointmentPost.getBuyMembershipId()==null){

                                rl_membership.setVisibility(View.VISIBLE);

                                testt.setText("Your Family Wallet Balance is: "+appointmentDetailResponse.getData()
                                        .getUsableMembership().get(0).getCredits());


                            }else if(appointmentDetailResponse.getData().getMembershipAmount()>0 && appointmentPost.getBuyMembershipId()==null&& appointmentPost.getBuyMembershipId().length()==0){

                                rl_membership.setVisibility(View.VISIBLE);
                                testt.setText("Your Family Wallet Balance is: "+appointmentDetailResponse.getData()
                                        .getMembershipAmount());
                            } else if(appointmentDetailResponse.getData().getMembershipAmount()>0 && appointmentPost.getBuyMembershipId()!=null&& appointmentPost.getBuyMembershipId().length()>0 && appointmentPost.getServices().size()>0){
                                rl_membership.setVisibility(View.VISIBLE);
                                testt.setText("Your Family Wallet Balance is: "+appointmentDetailResponse.getData()
                                        .getMembershipAmount());
                            }else
                                rl_membership.setVisibility(View.GONE);

                            if (appointmentDetailResponse.getData().getRedeemableSubscriptionLoyality()>0){
                                rl_subscription.setVisibility(View.VISIBLE);
                                txt_sub_redeem.setText("Would You Like To Use This Balance");

                                testttt.setText("Your Subcription Balance Is "+AppConstant.CURRENCY+appointmentDetailResponse.getData().getRedeemableSubscriptionLoyality());


                            }else{
                                if (subscriptionBuying){
                                    rl_subscription.setVisibility(View.VISIBLE);
                                }else   rl_subscription.setVisibility(View.GONE);
                            }

                            /**
                             * set suggested membership
                             */
                            if (appointmentDetailResponse.getData().getMembershipSuggestion()!=null && appointmentDetailResponse.getData().getMembershipSuggestion().getMembershipId()!=null){
                                ll_sugg_membership_container.setVisibility(View.VISIBLE);
                                //@Todo: handle below wala stuff
                                if(appointmentDetailResponse.getData().getMembershipSuggestion().getTitle()!=null &&
                                        appointmentDetailResponse.getData().getMembershipSuggestion().getTitle().length()>0)
                                    txt_membership_suggetion.setText(fromHtml(appointmentDetailResponse.getData().getMembershipSuggestion().getTitle()));
                                if(appointmentDetailResponse.getData().getMembershipSuggestion().getSubtitle()!=null &&
                                        appointmentDetailResponse.getData().getMembershipSuggestion().getSubtitle().length()>0)
                                    txt_sugg_membership_subtitle.setText(fromHtml(appointmentDetailResponse.getData().getMembershipSuggestion().getSubtitle()));
                                try{

                                    Glide.with(BillSummaryActivity.this).load(appointmentDetailResponse.getData().getMembershipSuggestion().getCardUrl())
                                            .into(imgViewMemberShipCard);
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                if (membershipBuying){
                                    txt_buy_membership.setText("ADDED");
                                    txt_buy_membership.setEnabled(false);
                                }else{
                                    txt_buy_membership.setText("BUY");
                                    txt_buy_membership.setEnabled(true);
                                }

                            }else ll_sugg_membership_container.setVisibility(View.GONE);

// set menu
                            // subscription suggetion

                            if (appointmentDetailResponse.getData().getSubscriptionSuggestion()!=null && appointmentDetailResponse.getData().getSubscriptionSuggestion().getHeading1()!=null){
                                /*if (isFirstDialog){
                                    FragmentManager manager = getFragmentManager();
                                    SubsBillSummeryDialog summeryDialog=new SubsBillSummeryDialog();
                                    Bundle args = new Bundle();
                                    args.putString("subsTitle",appointmentDetailResponse.getData().getSubscriptionSuggestion().getHeading2() );
                                    args.putString("term", appointmentDetailResponse.getData().getSubscriptionSuggestion().getTnC());
                                    args.putString("sugg", appointmentDetailResponse.getData().getSubscriptionSuggestion().getSuggestion());
                                    args.putDouble("totalAmount",appointmentDetailResponse.getData().getPayableAmount());
                                    args.putDouble("subsAmount",appointmentDetailResponse.getData().getSubscriptionSuggestion().getAmount());
                                    args.putDouble("redeem",appointmentDetailResponse.getData().getSubscriptionSuggestion().getRedemption());
                                    summeryDialog.setArguments(args);
                                    summeryDialog.show(manager,"test");
                                    isFirstDialog=false;

                                }*/
                                tempSubsId=appointmentDetailResponse.getData().getSubscriptionSuggestion().getSubscriptionId();
                                ll_sugg_subscription_container.setVisibility(View.VISIBLE);
                                ll_sugg_membership_container.setVisibility(View.GONE);
                                txt_subscription_suggetion.setText(Html.fromHtml(appointmentDetailResponse.getData().getSubscriptionSuggestion().getHeading1()));
                                txt_sugg_subs_subtitle.setText(Html.fromHtml(appointmentDetailResponse.getData().getSubscriptionSuggestion().getHeading2()));


                            }else{
                                ll_sugg_subscription_container.setVisibility(View.GONE);
                            }

                            Double menu_price=0.0;
                            int services= appointmentDetailResponse.getData().getServices().size();
                            for(int i=0; i<services;i++){

                                menu_price+= appointmentDetailResponse.getData().getServices().get(i).getActualPrice();
                                Log.i("invaaaaaa", "value in quanitty: "+ appointmentDetailResponse.getData().getServices().get(i).getQuantity());
                            }
                            Double saved=  menu_price- appointmentDetailResponse.getData().getPayableAmount();
                            if (saved > 0.0) {

                                txt_saved.setVisibility(View.VISIBLE);


                                txt_saved.setText("Saved "+AppConstant.CURRENCY+saved);
                                txt_saved.setBackgroundResource(R.drawable.discount_seletor);
                            }else {
                                txt_saved.setVisibility(View.INVISIBLE);
                            }
                            if (menu_price>0) {
                                txt_menu_price.setVisibility(View.VISIBLE);
                                txt_menu_price.setText(AppConstant.CURRENCY + menu_price);

                                txt_menu_price.setPaintFlags(txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            }else txt_menu_price.setVisibility(View.GONE);


                            int packageDiscount=(int)appointmentDetailResponse.getData().getPackageDiscount();

                            int discount= (int)appointmentDetailResponse.getData().getDiscount()+(int)appointmentDetailResponse.getData().getTotalSaved();
                            double serviceAmount=menu_price-(discount+packageDiscount+(int)appointmentDetailResponse.getData().getLoyalityPoints());
                            serviceTotal=serviceAmount+appointmentDetailResponse.getData().getTax();




                            appointmentDetailResponse.getData().getLoyalityPoints();
                            if (payOption==2){
                                payable_amount=appointmentDetailResponse.getData().getPayableAmount()+appointmentDetailResponse.getData().getMembershipAmount();
                                if (appointmentDetailResponse.getData().isThreadingDiscountAvailable() && appointmentDetailResponse.getData().getPayableAmount()>200.00 ){
                                    //  rl_threading.setVisibility(View.VISIBLE);
                                }else{
                                    rl_threading.setVisibility(View.GONE);
                                }
                            }else  payable_amount= appointmentDetailResponse.getData().getPayableAmount()+appointmentDetailResponse.getData().getMembershipAmount();

                            if (appointmentDetailResponse.getData().getSubscriptionAmount()!=0){
                                payable_amount= payable_amount+appointmentDetailResponse.getData().getSubscriptionAmount();

                            }
                            txt_grand_total.setText(AppConstant.CURRENCY+ payable_amount);
                            btn_pay.setText("PAY "+AppConstant.CURRENCY+payable_amount);
                            txt_100.setText(appointmentDetailResponse.getData().getDiscountMessage());
                            txt_freebee_se_tnc.setText(appointmentDetailResponse.getData().getFreebiesTerms());


                            /// membership with services


                            mContentView.setVisibility(View.VISIBLE);
                            btn_pay.setVisibility(View.VISIBLE);
                            mLoadingView.setVisibility(View.INVISIBLE);

                        }else{
                            Log.i("investigate", "so i'm in success false pe");
                            Toast.makeText(BillSummaryActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                            BillSummaryActivity.this.finish();
                        }
                        btn_pay.setClickable(true);

                    }else{

                        Toast.makeText(BillSummaryActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                        BillSummaryActivity.this.finish();
                    }


                }else{
                    Log.i(TAG, "on retrofit not successs");
                    mContentView.setVisibility(View.VISIBLE);
                    btn_pay.setVisibility(View.VISIBLE);
                    mLoadingView.setVisibility(View.INVISIBLE);
                    Toast.makeText(BillSummaryActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AppointmentDetailResponse> call, Throwable t) {

                mContentView.setVisibility(View.VISIBLE);
                btn_pay.setVisibility(View.VISIBLE);
                mLoadingView.setVisibility(View.INVISIBLE);
                Toast.makeText(BillSummaryActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                Log.i(TAG, "i'm in on failure: "+ t.getMessage()+ "  "+ t.toString()+ "  "+
                        t.getCause()+ "  "+ t.getStackTrace());
                BillSummaryActivity.this.finish();
            }
        });
    }

    //update list
    @org.greenrobot.eventbus.Subscribe(threadMode = ThreadMode.MAIN)
    public void promoCodeEvent(BookingSummaryPromoEvent event) {
        cbUsePoints.setChecked(true);
        BeuSalonsSharedPrefrence.setMyLoyaltyPoints(event.getLoyaltyPoints());
        txt_freeBee_points.setText("Total B-Cash Is "+event.getLoyaltyPoints());
        Log.i("investiii", "yeh prices ke liye hai bas: "+ event.getLoyaltyPoints());
        //   getUserDetails();
//        createAppointment(false);
    }

    @org.greenrobot.eventbus.Subscribe(threadMode = ThreadMode.MAIN)
    public void onBuySubscription(BuySubscriptionOnBillEvent event) {
        txt_buy_subs.performClick();

        Log.i("cancellation", "i'm in the event bus method ");
    }

    @org.greenrobot.eventbus.Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoSubscription(SayNoToSubscription event) {
        // btn_pay.performClick();
        logPayButtonClickedEvent();
        logPayButtonClickedFireBaseEvent();
        if(payOption==1){
            //cash payment
            if(CheckConnection.isConnected(BillSummaryActivity.this)){
                mContentView.setVisibility(View.GONE);
                btn_pay.setVisibility(View.GONE);
                mLoadingView.setVisibility(View.VISIBLE);
                bookCashPayment("", payable_amount, 1);

            }else{
                mContentView.setVisibility(View.VISIBLE);
                btn_pay.setVisibility(View.VISIBLE);
                mLoadingView.setVisibility(View.GONE);
                Toast.makeText(BillSummaryActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }

        }else if(payOption==2){

            if(CheckConnection.isConnected(BillSummaryActivity.this)){
                mContentView.setVisibility(View.GONE);
                btn_pay.setVisibility(View.GONE);
                mLoadingView.setVisibility(View.VISIBLE);

                if (payable_amount>0)
                    razorpay();
                else  bookCashPayment("", payable_amount, 1);

            }else{
                mContentView.setVisibility(View.VISIBLE);
                btn_pay.setVisibility(View.VISIBLE);
                mLoadingView.setVisibility(View.GONE);
                Toast.makeText(BillSummaryActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }

        }else {

            Log.i(TAG, "I'm in payOption others");
        }

    }

    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(getResources().getString(R.string.bill_summary));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);

        }

    }
    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */


    public void logPurchasedEvent (int numItems, String contentType, String contentId, String currency, double price) {
        Bundle params = new Bundle();
        params.putInt(AppEventsConstants.EVENT_PARAM_NUM_ITEMS, numItems);
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, contentId);
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        logger.logPurchase(BigDecimal.valueOf(price), Currency.getInstance(currency),params);

    }


    public void logComplimentryThreadingEvent () {
        Log.e("ComplimentryThreading","fine");
        logger.logEvent(AppConstant.ComplimentryThreading);
    }

    public void logComplimentryThreadingFireBaseEvent () {
        Log.e("ComplimentryThrefir","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.ComplimentryThreading,bundle);
    }
    public void logFreebiesPointsUsedEvent () {
        Log.e("FreebiesPointsUsed","fine");

        logger.logEvent(AppConstant.FreebiesPointsUsed);
    }

    public void logFreebiesPointsUsedFireBaseEvent () {
        Log.e("FreebiesPointsUsedfire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.FreebiesPointsUsed,bundle);
    }

    public void logBookingConfirmedEvent () {
        Log.e("BookingConfirmed","fine");

        logger.logEvent(AppConstant.BookingConfirmed);
    }
    public void logBookingConfirmedFireBaseEvent () {
        Log.e("BookingConfirmedfire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.BookingConfirmed,bundle);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logCashPaymentEvent () {

        Log.e("CashPayment","fine");
        logger.logEvent(AppConstant.CashPayment);
    }
    public void logCashPaymentFireBaseEvent () {

        Log.e("CashPaymentfirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.CashPayment,bundle);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logShowDetailEvent () {
        Log.e("ShowDetail","fine");

        logger.logEvent(AppConstant.ShowDetail);
    }

    public void logShowDetailFireBaseEvent () {
        Log.e("ShowDetailfirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.ShowDetail,bundle);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logPayButtonClickedEvent () {
        Log.e("PayButtonClicked","fine");
        logger.logEvent(AppConstant.PayButtonClicked);
    }
    public void logPayButtonClickedFireBaseEvent () {
        Log.e("PayButtonClifirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.PayButtonClicked,bundle);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CouponCodeApply event) {
        applyCouponCode=event.getCouponCode();

        Log.e("applyCouponCode",event.getCouponCode());
        isCouponApplied=true;
        couponCode=event.getCouponId();
        createAppointment();

        /* Do something */
    };
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

}
