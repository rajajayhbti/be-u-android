package com.beusalons.android.Model.OnlinePayment;

/**
 * Created by myMachine on 2/23/2017.
 */

public class OnlinePaymentModel {

    private String name;
    private Double price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
