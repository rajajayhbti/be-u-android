package com.beusalons.android.Model.Appointments;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 11/28/2016.
 */

public class PastAppointmentsResponse {

    private Boolean success;
    private List<PastData> data= new ArrayList<>();

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<PastData> getData() {
        return data;
    }

    public void setData(List<PastData> data) {
        this.data = data;
    }


}
