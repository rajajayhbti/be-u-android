package com.beusalons.android.Event.NewServicesEvent;

/**
 * Created by myMachine on 6/23/2017.
 */

public class AddServiceEvent {

    public AddServiceEvent(){

    }

    private String name;
    private String description;

    private int price;
    private int menu_price;
    private int save_per;
    private double tax;

    private int quantity;
    private int service_code;
    private int price_id;
    private String service_deal_id;
    private String service_id;

    private String type;                //service ya deal type ka name

    private String brand_id;
    private String product_id;

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    private String primary_key;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getMenu_price() {
        return menu_price;
    }

    public void setMenu_price(int menu_price) {
        this.menu_price = menu_price;
    }

    public int getSave_per() {
        return save_per;
    }

    public void setSave_per(int save_per) {
        this.save_per = save_per;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getService_code() {
        return service_code;
    }

    public void setService_code(int service_code) {
        this.service_code = service_code;
    }

    public int getPrice_id() {
        return price_id;
    }

    public void setPrice_id(int price_id) {
        this.price_id = price_id;
    }

    public String getService_deal_id() {
        return service_deal_id;
    }

    public void setService_deal_id(String service_deal_id) {
        this.service_deal_id = service_deal_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrimary_key() {
        return primary_key;
    }

    public void setPrimary_key(String primary_key) {
        this.primary_key = primary_key;
    }
}
