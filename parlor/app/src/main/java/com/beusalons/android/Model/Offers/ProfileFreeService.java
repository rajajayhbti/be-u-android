package com.beusalons.android.Model.Offers;

/**
 * Created by myMachine on 1/21/2017.
 */

public class ProfileFreeService {

    private String createdAt;
    private String categoryId;
    private String serviceId;
    private Integer code;
    private Object dealId;
    private Object parlorId;
    private Integer noOfService;
    private Integer price;
    private String id;
    private String name;

    private Boolean check;

    public Boolean getCheck() {
        return check;
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Object getDealId() {
        return dealId;
    }

    public void setDealId(Object dealId) {
        this.dealId = dealId;
    }

    public Object getParlorId() {
        return parlorId;
    }

    public void setParlorId(Object parlorId) {
        this.parlorId = parlorId;
    }

    public Integer getNoOfService() {
        return noOfService;
    }

    public void setNoOfService(Integer noOfService) {
        this.noOfService = noOfService;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
