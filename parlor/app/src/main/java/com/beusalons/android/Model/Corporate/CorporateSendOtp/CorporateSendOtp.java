package com.beusalons.android.Model.Corporate.CorporateSendOtp;

/**
 * Created by myMachine on 7/31/2017.
 */

public class CorporateSendOtp {

    private String userId;
    private String accessToken;
    private String emailId;
    private String companyId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
