package com.beusalons.android.Model.HomePage;

import java.util.List;

/**
 * Created by bhrigu on 1/24/2018.
 */

public class Banner {

    private String type;
    private String title;
    private List<List_> list = null;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<List_> getList() {
        return list;
    }

    public void setList(List<List_> list) {
        this.list = list;
    }
}
