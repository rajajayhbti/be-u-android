package com.beusalons.android.Model.Appointments;

/**
 * Created by myMachine on 11/28/2016.
 */

public class UpcomingEmployees {

    private String name;
    private Integer distribution;
    private Boolean isSpecialist;
    private Integer commission;
    private String id;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDistribution() {
        return distribution;
    }

    public void setDistribution(Integer distribution) {
        this.distribution = distribution;
    }

    public Boolean getSpecialist() {
        return isSpecialist;
    }

    public void setSpecialist(Boolean specialist) {
        isSpecialist = specialist;
    }

    public Integer getCommission() {
        return commission;
    }

    public void setCommission(Integer commission) {
        this.commission = commission;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
