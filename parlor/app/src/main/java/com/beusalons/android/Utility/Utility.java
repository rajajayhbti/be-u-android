package com.beusalons.android.Utility;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.beusalons.android.Fragment.SelectContactDailog;
import com.beusalons.android.Model.MymembershipDetails.ContactModel;
import com.beusalons.android.R;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import okhttp3.MediaType;

/**
 * Created by Ashish Sharma on 3/25/2017.
 */

public class Utility {
    public static final MediaType MEDIA_TYPE = MediaType.parse("application/json; charset=utf-8");
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }



    public static void getContactData(Context context){
        Cursor cursor ;
        List<ContactModel> list=new LinkedList<>();
         HashSet<String> mNumber=new HashSet<String>();

        cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" ASC");

        while (cursor.moveToNext()) {
            String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));

            String      name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

            String   phonenumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            ContactModel contactModel=new ContactModel();

            phonenumber = phonenumber.replaceAll(" ","");

            phonenumber = phonenumber.replaceAll("[()-]","");


            if (phonenumber.startsWith("+91")) {

                phonenumber=addCountryCodeRemove(phonenumber,

                       "+91");
                if (!mNumber.contains(phonenumber.trim())) {
                    contactModel.setMobileNumber(phonenumber);

                    long phId = cursor.getLong(cursor
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));


                    Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, phId);
                    Uri pURI = Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

                    contactModel.setPhotoURI(pURI);
                    contactModel.setName(name);

                    //contactModel.setMobileNumber(phonenumber);
                    contactModel.setId(phId);

                    list.add(contactModel);
                    mNumber.add(phonenumber.trim());
                }
            }
            else if (phonenumber.startsWith("0")) {
                phonenumber=phonenumber.substring(1);


                if (!mNumber.contains(phonenumber.trim())) {
                    contactModel.setMobileNumber(phonenumber);

                    mNumber.add(phonenumber.trim());
                    long phId = cursor.getLong(cursor
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));


                    Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, phId);
                    Uri pURI = Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

                    contactModel.setPhotoURI(pURI);
                    contactModel.setName(name);

                    //contactModel.setMobileNumber(phonenumber);
                    contactModel.setId(phId);

                    list.add(contactModel);
                }
            }
            else if (!mNumber.contains(phonenumber.trim())) {
                contactModel.setMobileNumber(phonenumber);

                mNumber.add(phonenumber.trim());
                long phId = cursor.getLong(cursor
                        .getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));


                Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, phId);
                Uri pURI = Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

                contactModel.setPhotoURI(pURI);
                contactModel.setName(name);

                //contactModel.setMobileNumber(phonenumber);
                contactModel.setId(phId);

                list.add(contactModel);
            }











        }
        SelectContactDailog.list.addAll(list);


        cursor.close();
    }
    public static String addCountryCodeRemove(String tempString, String countryCode) {

        if (tempString.contains(countryCode)) {
            tempString = tempString.replace(countryCode, "");
        }
        return tempString;
    }

    public static void applyFontForToolbarTitle(Activity context){
        Toolbar toolbar = (Toolbar) context.findViewById(R.id.toolbar);
        for(int i = 0; i < toolbar.getChildCount(); i++){
            View view = toolbar.getChildAt(i);
            if(view instanceof TextView){
                TextView tv = (TextView) view;
                Typeface titleFont = Typeface.
                        createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
                if(tv.getText().equals(toolbar.getTitle())){
                    tv.setTypeface(titleFont);
                    break;
                }
            }
        }
    }


    public static String faqHtmlData="<html>\n" +
            "<head>\n" +
            "</head>\n" +
            "<body>\n" +
            "<div>\n" +
            "<h3>FAQ</h3>\n" +
            "<p style=\"font-weight:700\">1. How to find branch phone number, address, direction of any salon?</p>\n" +
            "<p >Steps to follow -</p>\n" +
            "<p>Home Page >  Search Salon > Select your desired salon > Salon detail page with Contact No., Address and Directions of the salon.</p>\n" +
            "<p style=\"font-weight:700\">2. When do I receive my 100% or 50% cashback?</p>\n" +
            "<p>100% or 50% cashback is credited once your appointment is completed at the salon.</p>\n" +
            "<p style=\"font-weight:700\"> 3. How to check my booked appointments?</p>\n" +
            "<p>Steps to follow- <br>Home Page > Select 'My Profile' in bottom bar > Select 'My Appointments' > View your Booked or Completed Appointments</p>\n" +
            "<p style=\"font-weight:700\">4. Do I receive my cashback in my bank account ?</p>\n" +
            "<p>No, the cashback is not credited in your bank account. The points are credited to the Freebie Account in Freebies Section in the bottom bar.</p>\n" +
            "<p style=\"font-weight:700\">5. Whom to contact for queries related to services & appointments ?</p>\n" +
            "<p>You can email us at <a href=\"mailto:customercare@beusalons.com\" target=\"_blank\">customercare@beusalons.com</a>  or call on our customer care number <a href='tel:8690291910'>8690291910</a>.</p>\n" +
            "<p style=\"font-weight:700\"> 6. How can I tie up with Be U Salons ?</p>\n" +
            "<p>You can email us at <a href=\"mailto:customercare@beusalons.com\" target=\"_blank\">customercare@beusalons.com</a>  or can call us on <a href='tel:9821886522'>9821886522</a>.</p>\n" +
            "</div>\n" +
            "</body>\n" +
            "</html>";

    public static boolean isAppRunning(final Context context, final String packageName) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        if (procInfos != null)
        {
            for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                if (processInfo.processName.equals(packageName)) {
                    return true;
                }
            }
        }
        return false;
    }
}
