package com.beusalons.android.Model.AboutUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 17-Feb-18.
 */

public class Response {

    private Boolean success;
    private List<Datum> data = new ArrayList<>();


    public class Datum{

        private int questionCategoryId;
        private String questionCategory;
        private List<Question> questions = new ArrayList<>();

        public class Question{

            private String questionId;
            private String question;
            private List<Answer> answers = new ArrayList<>();
            private String type;

            public class Answer{

                private String answer;
                private String _id;
                private boolean isSelected;

                public String getAnswer() {
                    return answer;
                }

                public void setAnswer(String answer) {
                    this.answer = answer;
                }

                public String get_id() {
                    return _id;
                }

                public void set_id(String _id) {
                    this._id = _id;
                }

                public boolean isSelected() {
                    return isSelected;
                }

                public void setSelected(boolean selected) {
                    isSelected = selected;
                }
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getQuestionId() {
                return questionId;
            }

            public void setQuestionId(String questionId) {
                this.questionId = questionId;
            }

            public String getQuestion() {
                return question;
            }

            public void setQuestion(String question) {
                this.question = question;
            }

            public List<Answer> getAnswers() {
                return answers;
            }

            public void setAnswers(List<Answer> answers) {
                this.answers = answers;
            }
        }


        public int getQuestionCategoryId() {
            return questionCategoryId;
        }

        public void setQuestionCategoryId(int questionCategoryId) {
            this.questionCategoryId = questionCategoryId;
        }

        public String getQuestionCategory() {
            return questionCategory;
        }

        public void setQuestionCategory(String questionCategory) {
            this.questionCategory = questionCategory;
        }

        public List<Question> getQuestions() {
            return questions;
        }

        public void setQuestions(List<Question> questions) {
            this.questions = questions;
        }
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
}
