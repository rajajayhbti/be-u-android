package com.beusalons.android.Model.ServiceAvailable;

/**
 * Created by Ajay on 10/9/2017.
 */

public class ServiceAvailable_post {
    private String accessToken;
    private String userId;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
