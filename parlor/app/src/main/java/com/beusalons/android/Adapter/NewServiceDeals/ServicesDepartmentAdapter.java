package com.beusalons.android.Adapter.NewServiceDeals;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.newServiceDeals.Department;
import com.beusalons.android.R;
import com.bumptech.glide.Glide;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

/**
 * Created by myMachine on 5/25/2017.
 */

public class ServicesDepartmentAdapter extends RecyclerView.Adapter<ServicesDepartmentAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Department> list;
    private String gender;
    private String home_membership;
    private UserCart userCart;
    AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;


    public ServicesDepartmentAdapter(Context context, ArrayList<Department> list, String gender, UserCart userCart,
                                     String home_membership){

        this.context= context;
        this.list= list;
        this.gender= gender;
        this.userCart= userCart;
        this.home_membership= home_membership;
        logger = AppEventsLogger.newLogger(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    public void setData(ArrayList<Department> list, String gender){

        this.list= list;
        this.gender= gender;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Department department= list.get(position);

        holder.txt_name.setText(department.getName());
        holder.txt_info.setText(department.getDescription());

        if(department.isSelected()){
            holder.img_animation.setImageResource(R.drawable.ic_arrow_down);
            holder.rec_services.setVisibility(View.VISIBLE);
//            holder.LlContainer.animate().translationY( holder.LlContainer.getHeight());
//            holder.LlContainer.setBackgroundColor(context.getResources().getColor(R.color.yellow_fedded));
            if (position % 2==0){
                holder.LlContainer.setBackgroundColor(context.getResources().getColor(R.color.yellow_fedded));

            }else {
                holder.LlContainer.setBackgroundColor(context.getResources().getColor(R.color.blue_DARK));
            }
            GridLayoutManager layoutManager= new GridLayoutManager(context, 3);
            holder.rec_services.setLayoutManager(layoutManager);
            DepartmentServicesAdapter adapter= new DepartmentServicesAdapter(context, department.getCategories(),
                    gender, userCart, department.getDepartmentId(), department.getName(), home_membership);

            holder.rec_services.setAdapter(adapter);
        }else{
//            holder.LlContainer.animate().translationY(0);
            holder.LlContainer.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.img_animation.setImageResource(R.drawable.ic_right);
            holder.rec_services.setVisibility(View.GONE);
        }


        //final int pos= position;
        holder.linear_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(department.isSelected()){
                    holder.img_animation.setImageResource(R.drawable.ic_right);
//                    holder.LlContainer.animate().translationY(0);
                    holder.rec_services.setVisibility(View.GONE);
                    holder.LlContainer.setBackgroundColor(context.getResources().getColor(R.color.white));
                    department.setSelected(false);

                }else{
//                    holder.LlContainer.animate().translationY( holder.LlContainer.getHeight());
                    logServiceDepertmentEvent(department.getName(),gender);
                    logServiceDepertmentFireBaseEvent(department.getName(),gender);
                    if (position % 2==0){
                        holder.LlContainer.setBackgroundColor(context.getResources().getColor(R.color.yellow_fedded));

                    }else {
                        holder.LlContainer.setBackgroundColor(context.getResources().getColor(R.color.blue_DARK));
                    }
                  //  holder.LlContainer.setBackgroundColor(context.getResources().getColor(R.color.yellow_fedded));
                    Log.e("in services","test");

                    holder.img_animation.setImageResource(R.drawable.ic_arrow_down);
                    holder.rec_services.setVisibility(View.VISIBLE);
                    GridLayoutManager layoutManager= new GridLayoutManager(context, 3);
                    holder.rec_services.setLayoutManager(layoutManager);
                    DepartmentServicesAdapter adapter= new DepartmentServicesAdapter(context, department.getCategories(),
                            gender, userCart, department.getDepartmentId(), department.getName(), home_membership);
                    holder.rec_services.setAdapter(adapter);

                    department.setSelected(true);
                    updateView(position);


                }


            }
        });


        Glide.with(context)
                .load(department.getImage())
//                .centerCrop()
                .into(holder.img_);


    }


    public void updateView(int pos){
        for(int i=0;i<list.size();i++){
            if(i!=pos){
                list.get(i).setSelected(false);

            }
        }
        notifyDataSetChanged();
    }


    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logServiceDepertmentEvent (String name,String gender) {
        Log.e("deapartment","fine");
        Bundle params = new Bundle();
        params.putString("Name", name+"-"+gender);
       // params.putString(AppConstant.Gender,gender);
        logger.logEvent(AppConstant.ServiceDepertment, params);
    }

    public void logServiceDepertmentFireBaseEvent (String name,String gender) {
        Log.e("deapartmentfirebase","fine");
        Bundle params = new Bundle();
        params.putString("Name", name+"-"+gender);
        mFirebaseAnalytics.logEvent(AppConstant.ServiceDepertment, params);
    }
    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_name, txt_info;
        private ImageView img_, img_animation;
        private RelativeLayout linear_title;
        LinearLayout LlContainer;
        private RecyclerView rec_services;

        public ViewHolder(View itemView) {
            super(itemView);

            txt_name= (TextView)itemView.findViewById(R.id.txt_name);
            txt_info= (TextView)itemView.findViewById(R.id.txt_info);
            img_= (ImageView) itemView.findViewById(R.id.img_);
            img_animation= (ImageView)itemView.findViewById(R.id.img_animation);
            linear_title= (RelativeLayout) itemView.findViewById(R.id.linear_title);
            LlContainer= (LinearLayout) itemView.findViewById(R.id.ll_container_category_list);
            rec_services= (RecyclerView) itemView.findViewById(R.id.rec_services);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.services_department_list, parent, false);
        return new ViewHolder(view);
    }
}
