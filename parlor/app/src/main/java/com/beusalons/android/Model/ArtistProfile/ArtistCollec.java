package com.beusalons.android.Model.ArtistProfile;

/**
 * Created by Ajay on 1/29/2018.
 */

public class ArtistCollec {
    private String collecId;
    private String collectionName;

    public String getCollecId() {
        return collecId;
    }

    public void setCollecId(String collecId) {
        this.collecId = collecId;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }
}
