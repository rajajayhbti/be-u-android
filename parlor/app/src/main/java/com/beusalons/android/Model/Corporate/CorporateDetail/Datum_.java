package com.beusalons.android.Model.Corporate.CorporateDetail;

/**
 * Created by myMachine on 8/21/2017.
 */

public class Datum_ {

    private String start;
    private String middle;
    private String end;



    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getMiddle() {
        return middle;
    }

    public void setMiddle(String middle) {
        this.middle = middle;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
}
