package com.beusalons.android.Model.GeoCode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bhrigu on 2/13/17.
 */

public class GeoCodingResponse {

    public List<Result> results = new ArrayList<>();


    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }
}
