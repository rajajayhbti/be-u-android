package com.beusalons.android.Model.Corporate.CorporateDetail;

/**
 * Created by myMachine on 7/31/2017.
 */

public class CorporateDetailPost {

    private String userId;
    private String accessToken;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
