package com.beusalons.android.Model.selectArtist;

import java.util.List;

/**
 * Created by myMachine on 15-Mar-18.
 */

public class SelectedPost {

    private String appointmentId;
    private List<Service> services= null;

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

}
