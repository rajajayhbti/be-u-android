package com.beusalons.android.Model.MymembershipDetails;

/**
 * Created by Ajay on 11/9/2017.
 */

public class CreditHistory {

    private String parlorName;
    private String parlorAddress;
    private String name;
    private String phoneNumber;
    private String creditUsed;

    public String getParlorName() {
        return parlorName;
    }

    public void setParlorName(String parlorName) {
        this.parlorName = parlorName;
    }

    public String getParlorAddress() {
        return parlorAddress;
    }

    public void setParlorAddress(String parlorAddress) {
        this.parlorAddress = parlorAddress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCreditUsed() {
        return creditUsed;
    }

    public void setCreditUsed(String creditUsed) {
        this.creditUsed = creditUsed;
    }
}
