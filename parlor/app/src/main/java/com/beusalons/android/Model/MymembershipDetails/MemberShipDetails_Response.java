package com.beusalons.android.Model.MymembershipDetails;

/**
 * Created by Ajay on 11/9/2017.
 */

public class MemberShipDetails_Response {
    private Boolean success;
    private MemberShipData data;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }


    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public MemberShipData getData() {
        return data;
    }

    public void setData(MemberShipData data) {
        this.data = data;
    }
}
