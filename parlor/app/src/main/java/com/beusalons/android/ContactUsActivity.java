package com.beusalons.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;


import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;


import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ContactUsActivity extends AppCompatActivity {

    LinearLayout linearLayoutCall,linearLayoutEmail,linearLayoutLocation;
    protected static final int REQUEST_CHECK_SETTINGS = 1;
    private static final String TAG= ContactUsActivity.class.getSimpleName();
    GoogleApiClient mGoogleApiClient= null;
    private static final int GPS_PERMISSION = 6;
    private WebView webView;
    private String htmlData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        // new ToolbarHelper(getSupportActionBar(), getLayoutInflater(), true, this);
        setToolBar();

       /* ZopimChat.init("5HNceLCOU0EQ6SMq8zEuMKaqbYLK2kzH")
                .emailTranscript(EmailTranscript.DISABLED);
        VisitorInfo visitorData = new VisitorInfo.Builder()
                .name(BeuSalonsSharedPrefrence.getUserName())
                .phoneNumber(BeuSalonsSharedPrefrence.getPhoneNumber())
                .email(BeuSalonsSharedPrefrence.getUserEmail())
                .build();
        ZopimChat.setVisitorInfo(visitorData);*/

        linearLayoutCall=(LinearLayout)findViewById(R.id.linearLayout_call);
        linearLayoutEmail=(LinearLayout)findViewById(R.id.linearLayout_mail);
        linearLayoutLocation=(LinearLayout)findViewById(R.id.linearLayout_location);
        linearLayoutCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                startActivity(new Intent(getApplicationContext(), ZopimChatActivity.class));

            }
        });
        webView= (WebView) findViewById(R.id.web_faq);

        webView.loadData(Utility.faqHtmlData, "text/html; charset=utf-8", "UTF-8");
        webView.setWebViewClient(new WebViewClient(){

        });
        linearLayoutEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {


                    AlertDialog.Builder builder1 = new AlertDialog.Builder(ContactUsActivity.this);
                    builder1.setMessage("Would you like to send mail ?");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" + getResources().getString(R.string.email)));
                                    startActivity(intent);
                                    dialog.cancel();
                                }
                            });

                    builder1.setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.getWindow().setBackgroundDrawableResource(R.color.white);
                    alert11.show();


                }catch (Exception e){

                }
            }
        });

        linearLayoutLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("http://maps.google.com/maps?q=" +getResources().getString(R.string.latitude)+ "," + getResources().getString(R.string.longitude) + "(" + getResources().getString(R.string.address)+ ")&iwloc=A&hl=es");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
                if(Build.VERSION.SDK_INT < 23){

                    checkLocation();
                }else {
                    requestContactPermission();
                }
            }
        });

    }

    private void requestContactPermission() {

        int hasContactPermission = ActivityCompat.checkSelfPermission(this , android.Manifest.permission.ACCESS_FINE_LOCATION);

        if(hasContactPermission != PackageManager.PERMISSION_GRANTED ) {
            Log.i("permissions", "i'm in not onReqest permis granterd");
            ActivityCompat.requestPermissions(this, new String[]   {android.Manifest.permission.ACCESS_FINE_LOCATION}, GPS_PERMISSION);
        }else {
            Log.i("permissions", "i'm in already onReqest permis granterd");
            checkLocation();

//            Toast.makeText(this, "Contact Permission is already granted", Toast.LENGTH_LONG).show();
        }
    }
    private void checkLocation(){

        if(mGoogleApiClient==null){
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        final LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.
                checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(TAG, "All location settings are satisfied.");

                        Bundle bundle= new Bundle();
                        bundle.putDouble("latitude", Double.valueOf(getResources().getString(R.string.latitude)));
                        bundle.putDouble("longitude", Double.valueOf(getResources().getString(R.string.longitude)));
                        bundle.putString("parlorAddress", getResources().getString(R.string.address));
                        Intent intent= new Intent(ContactUsActivity.this, Activity_GoogleMaps.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {

                            status.startResolutionForResult(ContactUsActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {

                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(getResources().getString(R.string.contact_us));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);

        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onResume() {
        super.onResume();
//        mLoadingView.setVisibility(View.GONE);
//        mContentView.setVisibility(View.VISIBLE);

        if(mGoogleApiClient!=null){
            mGoogleApiClient.connect();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mGoogleApiClient!=null){
            mGoogleApiClient.disconnect();
        }

    }

    @Override
    protected void onStop() {

        if(mGoogleApiClient!=null){
            mGoogleApiClient.disconnect();
        }

        super.onStop();
    }

}
