package com.beusalons.android;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Adapter.AboutUserAdapter;
import com.beusalons.android.Helper.CheckConnection;
import com.beusalons.android.Model.UserIDAccessTokenPost;
import com.beusalons.android.Model.AboutUser.QuestionAns;
import com.beusalons.android.Model.AboutUser.Response;
import com.beusalons.android.Model.AboutUser.SendPost;
import com.beusalons.android.Model.AboutUser.SendResponse;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class AboutUserActivity extends AppCompatActivity {

    private RecyclerView recycler_;
    private LinearLayout linear_container;

    private int data_index;
    private int data_size;

    private List<RelativeLayout> list_relative;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_user);

        recycler_= findViewById(R.id.recycler_);
        recycler_.setLayoutManager(new LinearLayoutManager(this));
        linear_container= findViewById(R.id.linear_container);

        TextView txt_skip= findViewById(R.id.txt_skip);
        txt_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if(CheckConnection.isConnected(this)){

            fetchData(this);
        }else
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();

    }

    private void sendData(Response response, final Context context){

        SendPost post= new SendPost();
//        post.setUserId("59c642311d15f54c476aa0b2");
//        post.setAccessToken("784d1c1e82ede2225010ab2c2ed660af5aa4fcf5");
        post.setUserId(BeuSalonsSharedPrefrence.getUserId());
        post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());

        List<QuestionAns> list= new ArrayList<>();
        for(int i=0;i<response.getData().size();i++){
            for(int j=0;j<response.getData().get(i).getQuestions().size();j++){
                QuestionAns question= new QuestionAns();
                question.setQuestionId(response.getData().get(i).getQuestions().get(j).getQuestionId());
                List<String> answers= new ArrayList<>();
                for(int k=0;k<response.getData().get(i).getQuestions().get(j).getAnswers().size();k++){
                    if(response.getData().get(i).getQuestions().get(j).getAnswers().get(k).isSelected())
                        answers.add(response.getData().get(i).getQuestions().get(j).getAnswers().get(k).get_id());
                }
                question.setAnswers(answers);
                list.add(question);
            }
        }
        post.setQuestionAnswers(list);

        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface= retrofit.create(ApiInterface.class);
        Call<SendResponse> call= apiInterface.sendData(post);
        call.enqueue(new Callback<SendResponse>() {
            @Override
            public void onResponse(Call<SendResponse> call, retrofit2.Response<SendResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().isSuccess()){
                        Log.i("sendUserData", "in the success");
                        Toast toast =
                                Toast.makeText(context,
                                        "Profile Completed. \n 15% Discount Coupon Will Be Added To Your Coupons Shortly",
                                        Toast.LENGTH_LONG);
                        TextView v =  toast.getView().findViewById(android.R.id.message);
                        if( v != null) v.setGravity(Gravity.CENTER);
                        toast.show();



                    }else{
                        Log.i("sendUserData", "in the not success");
                    }

                }else{
                    Log.i("sendUserData", "in the else");
                }


            }

            @Override
            public void onFailure(Call<SendResponse> call, Throwable t) {
                Log.i("sendUserData", "in the failure: "+ t.getStackTrace()+ " " +t.getCause());
            }
        });

    }

    private void fetchData(final Context context){

        UserIDAccessTokenPost userIDAccessTokenPost = new UserIDAccessTokenPost();
        userIDAccessTokenPost.setUserId(BeuSalonsSharedPrefrence.getUserId());
        userIDAccessTokenPost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
//        userIDAccessTokenPost.setUserId("59c642311d15f54c476aa0b2");
//        userIDAccessTokenPost.setAccessToken("784d1c1e82ede2225010ab2c2ed660af5aa4fcf5");

        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface= retrofit.create(ApiInterface.class);
        Call<Response> call= apiInterface.getData(userIDAccessTokenPost);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, final retrofit2.Response<Response> response) {

                if(response.isSuccessful()){
                    if(response.body().getSuccess()){
                        Log.i("aboutUser", "in the success: ");

//                        tabLayout.addTab(tabLayout.newTab().setIcon(tab_icon_white_[0]).setText(tab_name_[0]));


                        linear_container.removeAllViews();
                        list_relative= new ArrayList<>();

                        data_size= response.body().getData().size();
                        for(int i=0;i<data_size;i++){
//                            tab_.addTab(tab_.newTab().setText(response.body().getData()
//                                    .get(i).getQuestionCategory().toUpperCase()));


                            RelativeLayout relative_column= (RelativeLayout) LayoutInflater.from(context)
                                    .inflate(R.layout.tab_custom_view, null);

                            TextView txt_name= relative_column.findViewById(R.id.txt_name);
                            txt_name.setText(response.body().getData()
                                    .get(i).getQuestionCategory().toUpperCase());

                            ImageView img_= relative_column.findViewById(R.id.img_);
                            img_.setVisibility(View.INVISIBLE);

                            LinearLayout linear_= relative_column.findViewById(R.id.linear_);
                            linear_.setVisibility(View.INVISIBLE);
                            if(i==0){
                                txt_name.setTextColor(ContextCompat.getColor(context, R.color.black));
                                linear_.setVisibility(View.VISIBLE);
                            }

                            list_relative.add(relative_column);

                            linear_container.addView(relative_column);
                        }




                        AboutUserAdapter adapter= new AboutUserAdapter(response.body()
                                .getData().get(data_index).getQuestions());
//                        adapter.setListener(new AboutUserAdapter.Listener() {
//                            @Override
//                            public void onClick(int count) {
//
//                            }
//                        });
                        recycler_.setAdapter(adapter);

                        final TextView txt_next= findViewById(R.id.txt_next);
                        txt_next.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                int count=0;
                                int size= response.body().getData()
                                        .get(data_index).getQuestions().size();
                                for(int i=0;i<size;i++){
                                    for(int j=0;j<response.body().getData()
                                            .get(data_index).getQuestions().get(i).getAnswers().size();j++){

                                        if(response.body().getData()
                                                .get(data_index).getQuestions().get(i).getAnswers().get(j).isSelected()){
                                            count++;
                                            break;
                                        }
                                    }
                                }
                                Log.i("cardClick", "value: "+ size+ " "+ count);
                                if(count== size){   //next page

                                    data_index++;

                                    if(data_index<data_size){
                                        if(data_index==data_size-1)
                                            txt_next.setText("Finish");

                                        //last index ka stuff
                                        TextView txt= list_relative.get(data_index-1).findViewById(R.id.txt_name);
                                        txt.setTextColor(ContextCompat.getColor(context, R.color.fontColor));
                                        ImageView img= list_relative.get(data_index-1).findViewById(R.id.img_);
                                        img.setVisibility(View.VISIBLE);
                                        LinearLayout linear= list_relative.get(data_index-1).findViewById(R.id.linear_);
                                        linear.setVisibility(View.INVISIBLE);

                                        //current index ka stuff
                                        TextView txt_= list_relative.get(data_index).findViewById(R.id.txt_name);
                                        txt_.setTextColor(ContextCompat.getColor(context, R.color.black));
                                        ImageView img_= list_relative.get(data_index).findViewById(R.id.img_);
                                        img_.setVisibility(View.INVISIBLE);
                                        LinearLayout linear_= list_relative.get(data_index).findViewById(R.id.linear_);
                                        linear_.setVisibility(View.VISIBLE);

                                        AboutUserAdapter adapter= new AboutUserAdapter(response.body()
                                                .getData().get(data_index).getQuestions());
                                        recycler_.setAdapter(adapter);
                                    }else if(data_index==data_size){

                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                sendData(response.body(), context);
                                            }
                                        }).start();
                                        finish();

                                    }
                                }else{
                                    Toast.makeText(view.getContext(), "Please Answer All Questions", Toast.LENGTH_SHORT).show();
                                }




                            }
                        });


                    }else{
                        Log.i("aboutUser", "in the not success: ");
                    }



                }else{
                    Log.i("aboutUser", "in the else: ");
                }


            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Log.i("aboutUser", "in the failure: "+ t.getCause()+ " "+ t.getStackTrace());
            }
        });



    }


}
