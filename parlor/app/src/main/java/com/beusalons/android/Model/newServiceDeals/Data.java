package com.beusalons.android.Model.newServiceDeals;

import java.util.List;

public class Data {

    private List<Service> services = null;

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

}