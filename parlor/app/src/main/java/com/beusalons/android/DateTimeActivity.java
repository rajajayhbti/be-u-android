package com.beusalons.android;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Adapter.DateTimeAdapter;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.DateTimeModel;
import com.beusalons.android.Model.SalonHome.HomeResponse;
import com.beusalons.android.Model.SalonHome.salonDepartments.SalonDeparttmentsData;
import com.beusalons.android.Utility.Utility;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class DateTimeActivity extends AppCompatActivity implements View.OnClickListener {

    public final static String TAG = DateTimeActivity.class.getSimpleName();

    public static final String APPOINTMENT_ID= "appointment_id";
    public static final String CURRENT_TIME= "current_time";
    public static final String DATA_HOME_RESPONSE= "homeresponse";
    public static final String DATA_DEPARTMENT_RESPONSE= "salonsdepartmentsdata";
    public static final String OPENING_TIME= "opening_time";
    public static final String CLOSING_TIME= "closing_time";
    public static final String DAY_CLOSED= "day_closed";

    public static final String REORDER= "com.beusalons.date.reorder";

    Context context = this;            //context

    static List<DateTimeModel> details= new ArrayList<>();

    //recyclerview
    private static RecyclerView recyclerView;
    private static DateTimeAdapter adapter;
    private static TextView txt_date;


    //current server time
    private static Calendar cal_server_time= Calendar.getInstance();
    private static Date date;
    private static String server_time="";

    static Calendar cal_interval= Calendar.getInstance();    //time slots
    static Calendar cal_btn= Calendar.getInstance();           //date change


    TextView txtViewActionBarName;

    ImageView imgViewBack;

    // stuff to send to the parlor service activity
    private String serviceListAsString="", apptId= "", home_response="";


    private static String openingTime, closingTime;
    private static int startTimeHr, startTimeMin ;
    private static int closeTimeHr, closeTimeMin;
    private static int dayClosed=0;

    private static LinearLayout linear_day_closed, linear_left, linear_right;

    private boolean isReschedule;
    private AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;
    String departmentId,departmentNeme,gender;
    int index;
    HomeResponse homeResponse;
    //reorder
    private boolean isReorder= false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_time);
        setToolBar();
        logger = AppEventsLogger.newLogger(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //test purpose...
//            server_time= "2017-11-10T03:45:00.000Z";
        Bundle bundle= getIntent().getExtras();
        if(bundle!=null &&
                bundle.containsKey(DATA_HOME_RESPONSE)){

            isReschedule= false;
            home_response= bundle.getString(DATA_HOME_RESPONSE);
            homeResponse= new Gson().fromJson(home_response,
                    HomeResponse.class);
            openingTime= bundle.getString(OPENING_TIME);
            closingTime= bundle.getString(CLOSING_TIME);
            dayClosed= bundle.getInt(DAY_CLOSED);
            server_time= bundle.getString(CURRENT_TIME);


        }else if(bundle!=null &&
                bundle.containsKey(REORDER)){

            isReorder= true;
            apptId= bundle.getString(APPOINTMENT_ID);
            openingTime= bundle.getString(OPENING_TIME);
            closingTime= bundle.getString(CLOSING_TIME);
            dayClosed= bundle.getInt(DAY_CLOSED, 0);
            server_time= bundle.getString(CURRENT_TIME);
        }else if(bundle!=null &&
                bundle.containsKey(APPOINTMENT_ID)){

            isReschedule= true;                 //reschedule ka case hai yeh

            apptId= bundle.getString(APPOINTMENT_ID);

            openingTime= bundle.getString(OPENING_TIME);
            closingTime= bundle.getString(CLOSING_TIME);
            dayClosed= bundle.getInt(DAY_CLOSED, 0);
            server_time= bundle.getString(CURRENT_TIME);
        }


        if (bundle!=null && bundle.containsKey("departmentId")){


            departmentId=bundle.getString("departmentId",null);
            departmentNeme=bundle.getString("departmentName",null);
            gender=bundle.getString("gender",null);
            index=bundle.getInt("index",-1);
        }

        SimpleDateFormat simpleDateFormat=
                new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date date_= null;
        try{
            //"2017-11-09T09:59:37.962Z"

            date_= simpleDateFormat.parse(server_time);

            cal_server_time.setTime(date_);
            cal_server_time.add(Calendar.HOUR, +5);
            cal_server_time.add(Calendar.MINUTE, +30);

            cal_btn.setTime(date_);
            cal_btn.add(Calendar.HOUR, +5);
            cal_btn.add(Calendar.MINUTE, +30);

            cal_interval.setTime(date_);
            cal_interval.add(Calendar.HOUR, +5);
            cal_interval.add(Calendar.MINUTE, +30);

            date= cal_server_time.getTime();

        }catch (ParseException e){
            e.printStackTrace();
        }

        txt_date= (TextView)findViewById(R.id.txt_date_time_date);
        txtViewActionBarName=(TextView)findViewById(R.id.txt_location);
        imgViewBack=(ImageView)findViewById(R.id.imgView_back);
        linear_left=(LinearLayout)findViewById(R.id.linear_left);
        linear_right=(LinearLayout)findViewById(R.id.linear_right);
        linear_right.setOnClickListener(this);
        linear_left.setOnClickListener(this);
        txt_date.setOnClickListener(this);
        txt_date.setText(dateFormat(date));
        recyclerView = (RecyclerView)findViewById(R.id.rcy_time_slot);

        linear_day_closed= (LinearLayout) findViewById(R.id.linear_day_closed);
        linear_day_closed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cal_btn.add(Calendar.DAY_OF_YEAR, 1);
                date= cal_btn.getTime();
                txt_date.setText(dateFormat(date));
                setTime();
            }
        });

        GridLayoutManager layoutManager= new GridLayoutManager(this, 4);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        if (departmentId!=null && departmentId.trim().length()>0){
            adapter = new DateTimeAdapter(this, details, apptId, isReschedule, home_response,departmentId,departmentNeme,index,gender);
        }else adapter = new DateTimeAdapter(this, details, apptId, isReschedule, home_response, isReorder);
        recyclerView.setAdapter(adapter);

        //setting gridlayoutmanager row span
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch(adapter.getItemViewType(position)){

                    case 0:
                        return 4;
                    case 1:
                        return 1;
                    default:
                        return -1;
                }
            }
        });
        setTime();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void ongetDepartments(SalonDeparttmentsData data){
        // this.salonDeparttmentsData=data;
       /* response_.getData().setDepartments(data.getDepartments());
        response_.getData().setSubscriptions(data.getSubscriptions());
        response_.getData().setWelcomeOffer(data.getWelcomeOffer());
        response_.getData().setTax(data.getTax());*/
        Log.e("in DateTimeActivity",data.getParlorId());


        if (homeResponse.getData().getDepartments()!=null && homeResponse.getData().getDepartments().size()>0){

        }else{
            homeResponse.getData().setDepartments(data.getDepartments());
            homeResponse.getData().setSubscriptions(data.getSubscriptions());
            homeResponse.getData().setWelcomeOffer(data.getWelcomeOffer());
            homeResponse.getData().setTax(data.getTax());
        }



        adapter.setHomeData(new Gson().toJson(homeResponse, HomeResponse.class));


    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){

            getSupportActionBar().setTitle(getResources().getString(R.string.txt_timeSlot_title));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }
    }

    public static void setTime(){

        boolean inAnCase;

        try{
            //back press pe issue hai
            startTimeHr= Integer.parseInt(openingTime==null? "10": openingTime.substring(0,2));//hour
            startTimeMin= Integer.parseInt(openingTime==null? "00": openingTime.substring(3,5));//minutes

            closeTimeHr= Integer.parseInt(closingTime==null? "19": closingTime.substring(0, 2)); //hr
            closeTimeMin= Integer.parseInt(closingTime==null? "45": closingTime.substring(3,5)); //min
        }catch (Exception e){
            e.printStackTrace();
        }

        int[] image_limit= new int[3];      //set the image limit
        image_limit[0]=1;
        image_limit[1]=1;
        image_limit[2]=1;

        if(!details.isEmpty()){

            details.clear();
        }

        Log.i("wtfisthis", "value ca-: "+ cal_btn.get(Calendar.YEAR)+  " "+
                cal_btn.get(Calendar.DAY_OF_YEAR)+"   "+ cal_server_time.get(Calendar.YEAR)+ " " +
                cal_server_time.get(Calendar.DAY_OF_YEAR));

        Date time_date;
        if ((cal_btn.get(Calendar.YEAR) == cal_server_time.get(Calendar.YEAR)) &&
                (cal_btn.get(Calendar.DAY_OF_YEAR) == cal_server_time.get(Calendar.DAY_OF_YEAR)) &&
                (cal_btn.get(Calendar.DAY_OF_WEEK)!=dayClosed)) {

            inAnCase= true;

            SimpleDateFormat simpleDateFormat=
                    new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            Date date_= null;
            try{
                //"2017-11-09T09:59:37.962Z"

                date_= simpleDateFormat.parse(server_time);
                cal_interval.setTime(date_);
                cal_interval.add(Calendar.HOUR, +5);
                cal_interval.add(Calendar.MINUTE, +30);
            }catch (ParseException e){
                e.printStackTrace();
            }

            if(cal_interval.get(Calendar.HOUR_OF_DAY)<startTimeHr-1){

                cal_interval.set(Calendar.HOUR_OF_DAY, startTimeHr);          //setting start time--earlier set was--8
                cal_interval.set(Calendar.MINUTE, startTimeMin);
                cal_interval.set(Calendar.SECOND, 0);
                cal_interval.set(Calendar.MILLISECOND, 0);
            }else{

                Calendar cal_closing= Calendar.getInstance();
                Date date_closing= null;
                try{
                    //"2017-11-09T09:59:37.962Z"
                    date_closing= simpleDateFormat.parse(server_time);
                    cal_closing.setTime(date_closing);
                    cal_closing.set(Calendar.HOUR_OF_DAY, closeTimeHr);
                    cal_closing.set(Calendar.MINUTE, closeTimeMin);
                    cal_closing.set(Calendar.SECOND, 0);
                    cal_closing.set(Calendar.MILLISECOND, 0);
                }catch (ParseException e){
                    e.printStackTrace();
                }


                if((cal_closing.getTimeInMillis()-4500000) <= cal_interval.getTimeInMillis()){
                    Log.i("wtfisthis", "value ca-: "+ cal_btn.get(Calendar.DAY_OF_YEAR)+  " "+
                            cal_btn.get(Calendar.MINUTE));

                    cal_btn.add(Calendar.DAY_OF_YEAR, 1);
                    date= cal_btn.getTime();
                    txt_date.setText(dateFormat(date));

                    Log.i("wtfisthis", "value ca-: "+ cal_btn.get(Calendar.DAY_OF_YEAR)+  " "+
                            cal_btn.get(Calendar.MINUTE));

                    setTime();
                    return;
                }

                //after one hour ka time dikhao

                cal_interval.add(Calendar.HOUR, 1);
            }



            /*else if(((cal_interval.get(Calendar.HOUR_OF_DAY)>=closeTimeHr-2) &&
                    (cal_interval.get(Calendar.MINUTE)>45)) ||
                    cal_interval.get(Calendar.HOUR_OF_DAY)>=closeTimeHr-1){

                cal_btn.add(Calendar.DAY_OF_YEAR, 1);
                date= cal_btn.getTime();
                txt_date.setText(dateFormat(date));

                setTime();
                return;
            }*//*else if(cal_interval.get(Calendar.HOUR_OF_DAY)>=closeTimeHr-1){


                cal_btn.add(Calendar.DAY_OF_YEAR, 1);
                date= cal_btn.getTime();
                txt_date.setText(dateFormat(date));
                Log.i("i'mdate", "I'm in the same date pe 3 "+ cal_interval.get(Calendar.HOUR_OF_DAY)+ "   "+closeTimeHr);
                setTime();
                return;
            }*/

            int unRoundedMinutes = cal_interval.get(Calendar.MINUTE);
            int mod = unRoundedMinutes % 15;
            cal_interval.add(Calendar.MINUTE, mod < 8 ? -mod : (15 - mod));
            while(cal_interval.get(Calendar.HOUR_OF_DAY)<12){

                if(image_limit[0]==1){
                    DateTimeModel img= new DateTimeModel();
                    img.setImage_id(R.drawable.morning);
                    img.setType(0);
                    img.setTxt_image("Morning before 12:00 PM");
                    details.add(img);
                    image_limit[0]++;
                }

                DateTimeModel modal= new DateTimeModel();
                modal.setDisplay_time(getTimeInMillis(cal_interval));
                modal.setDate(jsonDate(date));
                time_date = cal_interval.getTime();
                modal.setTime(jsonTime(time_date));
                modal.setType(1);
                details.add(modal);
                cal_interval.add(Calendar.MINUTE, 15);
            }

            while(cal_interval.get(Calendar.HOUR_OF_DAY)<16){

                if(image_limit[1]==1){
                    DateTimeModel img= new DateTimeModel();
                    img.setImage_id(R.drawable.afternoon);
                    img.setType(0);
                    img.setTxt_image("Afternoon after 12:00 PM");
                    details.add(img);
                    image_limit[1]++;
                }

                DateTimeModel modal= new DateTimeModel();

                modal.setDisplay_time(getTimeInMillis(cal_interval));
                modal.setDate(jsonDate(date));
                time_date = cal_interval.getTime();
                modal.setTime(jsonTime(time_date));
                modal.setType(1);
                details.add(modal);

                cal_interval.add(Calendar.MINUTE, 15);
            }

            while(cal_interval.get(Calendar.HOUR_OF_DAY)<closeTimeHr){                //setting close time

                if(image_limit[2]==1){
                    DateTimeModel img= new DateTimeModel();
                    img.setImage_id(R.drawable.evening);
                    img.setType(0);
                    img.setTxt_image("Evening after 16:00 PM");
                    details.add(img);
                    image_limit[2]++;
                }

                DateTimeModel modal= new DateTimeModel();
                cal_interval.add(Calendar.MINUTE, 15);      //before

                modal.setDisplay_time(getTimeInMillis(cal_interval));
                modal.setDate(jsonDate(date));
                time_date = cal_interval.getTime();
                modal.setTime(jsonTime(time_date));
                modal.setType(1);
                details.add(modal);
            }

            Calendar cal_closing= Calendar.getInstance();
            Date date_closing= null;
            try{
                //"2017-11-09T09:59:37.962Z"
                date_closing= simpleDateFormat.parse(server_time);
                cal_closing.setTime(date_closing);
                cal_closing.set(Calendar.HOUR_OF_DAY, closeTimeHr);
                cal_closing.set(Calendar.MINUTE, closeTimeMin);
                cal_closing.set(Calendar.SECOND, 0);
                cal_closing.set(Calendar.MILLISECOND, 0);
            }catch (ParseException e){
                e.printStackTrace();
            }


//            if((cal_closing.getTimeInMillis()-3300000) <= cal_server_time.getTimeInMillis()){
//                cal_btn.add(Calendar.DAY_OF_YEAR, 1);
//                date= cal_btn.getTime();
//                txt_date.setText(dateFormat(date));
//
//                setTime();
//                return;
//            }

//            while(cal_interval.get(Calendar.HOUR_OF_DAY)==closeTimeHr &&
//                    cal_interval.get(Calendar.MINUTE)<closeTimeMin){

            while(cal_interval.getTimeInMillis() <= (cal_closing.getTimeInMillis()-600000)){

                if(image_limit[2]==1){
                    DateTimeModel img= new DateTimeModel();
                    img.setImage_id(R.drawable.evening);
                    img.setType(0);
                    img.setTxt_image("Evening after 16:00 PM");
                    details.add(img);
                    image_limit[2]++;
                }

                DateTimeModel modal= new DateTimeModel();
                cal_interval.add(Calendar.MINUTE, 15);      //before

                modal.setDisplay_time(getTimeInMillis(cal_interval));
                modal.setDate(jsonDate(date));
                time_date = cal_interval.getTime();
                modal.setTime(jsonTime(time_date));
                modal.setType(1);
                details.add(modal);
            }

        }else if((cal_btn.get(Calendar.DAY_OF_WEEK)!=dayClosed)){

//            date= cal_btn.getTime();
//            txt_date.setText(dateFormat(date));

            Log.i("wtfisthis", "value kyahuaterawada: "+ cal_btn.get(Calendar.DAY_OF_YEAR)+  " "+
                    cal_btn.get(Calendar.MINUTE));

            inAnCase=true;
            cal_interval.set(Calendar.HOUR_OF_DAY, startTimeHr);
            cal_interval.set(Calendar.MINUTE, startTimeMin);
            cal_interval.set(Calendar.SECOND, 0);
            cal_interval.set(Calendar.MILLISECOND, 0);

            while(cal_interval.get(Calendar.HOUR_OF_DAY)<12){

                if(image_limit[0]==1){
                    DateTimeModel img= new DateTimeModel();
                    img.setImage_id(R.drawable.morning);
                    img.setTxt_image("Morning before 12:00 PM");
                    img.setType(0);
                    details.add(img);
                    image_limit[0]++;
                }

                DateTimeModel modal= new DateTimeModel();

                modal.setDisplay_time(getTimeInMillis(cal_interval));
                modal.setDate(jsonDate(date));
                time_date = cal_interval.getTime();
                modal.setTime(jsonTime(time_date));
                modal.setType(1);
                details.add(modal);
                cal_interval.add(Calendar.MINUTE, 15);
            }

            while(cal_interval.get(Calendar.HOUR_OF_DAY)<16){

                if(image_limit[1]==1){
                    DateTimeModel img= new DateTimeModel();
                    img.setImage_id(R.drawable.afternoon);
                    img.setTxt_image("Afternoon after 12:00 PM");
                    img.setType(0);
                    details.add(img);
                    image_limit[1]++;
                }

                DateTimeModel modal= new DateTimeModel();


                modal.setDisplay_time(getTimeInMillis(cal_interval));
                modal.setDate(jsonDate(date));
                time_date = cal_interval.getTime();
                modal.setTime(jsonTime(time_date));
                modal.setType(1);
                details.add(modal);
                cal_interval.add(Calendar.MINUTE, 15);
            }

            while(cal_interval.get(Calendar.HOUR_OF_DAY)<closeTimeHr){

                if(image_limit[2]==1){
                    DateTimeModel img= new DateTimeModel();
                    img.setImage_id(R.drawable.evening);
                    img.setTxt_image("Evening after 16:00 PM");
                    img.setType(0);
                    details.add(img);
                    image_limit[2]++;
                }

                DateTimeModel modal= new DateTimeModel();

                cal_interval.add(Calendar.MINUTE, 15);
                modal.setDisplay_time(getTimeInMillis(cal_interval));
                modal.setDate(jsonDate(date));
                time_date = cal_interval.getTime();
                modal.setTime(jsonTime(time_date));
                modal.setType(1);
                details.add(modal);
            }

            //closing time hr- and minutes
            while(cal_interval.get(Calendar.HOUR_OF_DAY)==closeTimeHr && cal_interval.get(Calendar.MINUTE)<closeTimeMin){
                DateTimeModel modal= new DateTimeModel();
                cal_interval.add(Calendar.MINUTE, 15);      //before

                modal.setDisplay_time(getTimeInMillis(cal_interval));
                modal.setDate(jsonDate(date));
                time_date = cal_interval.getTime();
                modal.setTime(jsonTime(time_date));
                modal.setType(1);
                details.add(modal);
            }
        }else{

            inAnCase= false;
        }

        if(inAnCase){                                       // salon is open

            linear_day_closed.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter.notifyDataSetChanged();

        }else{

            recyclerView.setVisibility(View.GONE);
            linear_day_closed.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.linear_left:

                logPreviousDayButtonEvent();
                logPreviousDayFireBaseEvent();
                if((cal_btn.get(Calendar.YEAR) == cal_server_time.get(Calendar.YEAR)) &&
                        (cal_btn.get(Calendar.DAY_OF_YEAR) > cal_server_time.get(Calendar.DAY_OF_YEAR))){
                    cal_btn.add(Calendar.DAY_OF_YEAR, -1);
                    date= cal_btn.getTime();
                    txt_date.setText(dateFormat(date));
                }else if(cal_btn.get(Calendar.YEAR)> cal_server_time.get(Calendar.YEAR)){
                    cal_btn.add(Calendar.DAY_OF_YEAR, -1);
                    date= cal_btn.getTime();
                    txt_date.setText(dateFormat(date));

                }else{
                    date= cal_btn.getTime();
                    txt_date.setText(dateFormat(date));
                }
                setTime();

                break;

            case R.id.linear_right:
                logNextDayButtonEvent();
                logNextDayFireBaseEvent();
                cal_btn.add(Calendar.DAY_OF_YEAR, 1);
                date= cal_btn.getTime();
                txt_date.setText(dateFormat(date));
                setTime();
                break;

            case R.id.txt_date_time_date:

                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");
                break;
        }
    }


    //date picker class
    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker

            int year = cal_btn.get(Calendar.YEAR);
            int month = cal_btn.get(Calendar.MONTH);
            int day = cal_btn.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dialog= new DatePickerDialog(getActivity(), this, year, month, day);

            dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {

            cal_btn.set(Calendar.YEAR, year);
            cal_btn.set(Calendar.MONTH, month);
            cal_btn.set(Calendar.DAY_OF_MONTH, day);
            date= cal_btn.getTime();
            txt_date.setText(dateFormat(date));
            setTime();
            Log.i("sothetimee", "day of week: "+ cal_btn.get(Calendar.DAY_OF_WEEK));
           /* if(year>cal_server_time.get(Calendar.YEAR)){
                btn_calendar.set(Calendar.YEAR, year);
                btn_calendar.set(Calendar.MONTH, month);
                btn_calendar.set(Calendar.DAY_OF_MONTH, day);
                date = btn_calendar.getTime();
                btn_date.setText(dateFormat(date));

            }else if(year==cal_server_time.get(Calendar.YEAR)){
                if(month>=cal_server_time.get(Calendar.DAY_OF_YEAR)){

                    btn_calendar.set(Calendar.YEAR, year);
                    btn_calendar.set(Calendar.MONTH, month);
                    btn_calendar.set(Calendar.DAY_OF_MONTH, day);
                    date = btn_calendar.getTime();
                    btn_date.setText(dateFormat(date));

                }
            }*/


        }
    }

    public static String getTimeInMillis(Calendar calendar){

        return new SimpleDateFormat("HH:mm aa").format(new Date(calendar.getTimeInMillis()));
    }

    public static String dateFormat(Date date){

        return new SimpleDateFormat("EEE, dd MMM").format(date);
    }

    public static String jsonDate(Date date){

        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public static String jsonTime(Date date){

        return new SimpleDateFormat("HH:mm").format(date);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        logTimePageBackButtonEvent();
        logTimePageBackButtonFireBaseEvent();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void logPreviousDayButtonEvent () {
        Log.e("PreviousDay","fine");
        logger.logEvent(AppConstant.PreviousDay);
    }
    private void logNextDayButtonEvent () {
        Log.e("NextDay","fine");
        logger.logEvent(AppConstant.NextDay);
    }
    private void logNextDayFireBaseEvent () {
        Log.e("NextDayfire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.NextDay,bundle);
    }
    private void logPreviousDayFireBaseEvent () {
        Log.e("PreviousDayfire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.PreviousDay,bundle);
    }


    private void logTimePageBackButtonEvent () {
        Log.e("TimePageBackButton","fine");
        logger.logEvent(AppConstant.TimePageBackButton);
    }
    private void logTimePageBackButtonFireBaseEvent () {
        Log.e("TimePageBackButtonfire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.TimePageBackButton,bundle);
    }


//    //update list
//    @org.greenrobot.eventbus.Subscribe(threadMode = ThreadMode.MAIN)
//    public void onMessageEvent3(DateTimeEvent event) {
//
//        Log.i("investi", "i'm in on recieve evnet.djfskdajflksjdkfjksa  bussssssssssssssssss");
//        mContentView.setVisibility(View.GONE);
//        mLoadingView.setVisibility(View.VISIBLE);
//    }
}
