package com.beusalons.android.Model.Send_Otp;

import java.io.Serializable;

/**
 * Created by myMachine on 11/1/2016.
 */

public class Send_Otp_Response implements Serializable{

    public Send_Otp_Response(){

    }

    private Boolean success;
    private String message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
