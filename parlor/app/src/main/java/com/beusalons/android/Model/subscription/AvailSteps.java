package com.beusalons.android.Model.subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AvailSteps {

@SerializedName("heading")
@Expose
private String heading;
@SerializedName("steps")
@Expose
private List<Step> steps = null;

public String getHeading() {
return heading;
}

public void setHeading(String heading) {
this.heading = heading;
}

public List<Step> getSteps() {
return steps;
}

public void setSteps(List<Step> steps) {
this.steps = steps;
}

}