package com.beusalons.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Adapter.AdapterCreditHistory;
import com.beusalons.android.Adapter.AdapterMembershipFreeService;
import com.beusalons.android.Adapter.AdapterSelectedContact;
import com.beusalons.android.Fragment.SelectContactDailog;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.MymembershipDetails.AddContact_response;
import com.beusalons.android.Model.MymembershipDetails.AddUserContactNumber_post;
import com.beusalons.android.Model.MymembershipDetails.ContactModel;
import com.beusalons.android.Model.MymembershipDetails.CreditHistory;
import com.beusalons.android.Model.MymembershipDetails.FreeService;
import com.beusalons.android.Model.MymembershipDetails.Member;
import com.beusalons.android.Model.MymembershipDetails.MemberShipDetails_Response;
import com.beusalons.android.Model.MymembershipDetails.MembershipDetailsPost;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Ajay on 11/7/2017.
 */

public class MyMembershipDetails extends AppCompatActivity implements DialogInterface.OnDismissListener {
    private static final String TAG= MyMembershipDetails.class.getSimpleName();

    private TextView txtViewAddContact,txtCreditLeft,txtMembershipName,txtViewExpirydate,txtViewMain,txtViewDeal,txtCreditUsed,txtViewMembershipCount;
    private RecyclerView recyclerView,recyclerViewCreditHistory,recyclerViewFreeService;
    private List<ContactModel> selectedList=new ArrayList<>();
    private LinearLayout linearLayoutCom,linear_membershipCredit,linear_free_service;
    private  List<FreeService> serviceList;
    private String memberShipSellId;
    private List<String> addContactList;
    private  List<CreditHistory> creditHistoryList;
    private TextView txt_topup_membership;
    AdapterCreditHistory adapterMembershipCredit;
    AdapterSelectedContact adapterSelectedContact;
    AdapterMembershipFreeService adapter;
    private List<Member> memberList;
    public static final int REQUEST_READ_CONTACTS = 79;
    private View loading_for_;
    public  static boolean isDismiss=false;
    public   int remainingAddUser=0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mymembership_details);

        setToolBar();
        inItView();
        requestLocationPermission();
    }

    private  void inItView(){
        serviceList=new ArrayList<>();
        addContactList=new ArrayList<>();
        creditHistoryList=new ArrayList<>();
        memberList=new ArrayList<>();
        loading_for_=(View)findViewById(R.id.loading_for_);
        txtCreditUsed=(TextView)findViewById(R.id.txt_credit_used);
        recyclerView=(RecyclerView)findViewById(R.id.recycler_view_select_contact) ;
        txtViewMembershipCount=(TextView)findViewById(R.id.txtView_membership_count);
        txtCreditLeft=(TextView)findViewById(R.id.txt_credit_left);
        txtMembershipName=(TextView)findViewById(R.id.txt_membership_name);
        txtViewMain=(TextView) findViewById(R.id.txtView_main);
        txt_topup_membership=(TextView) findViewById(R.id.txt_topup_membership);
        txtViewDeal=(TextView)findViewById(R.id.txtView_deal);
        linearLayoutCom=(LinearLayout)findViewById(R.id.linearLayout_com);
        linear_membershipCredit=(LinearLayout)findViewById(R.id.linear_membershipCredit);
        linear_free_service=(LinearLayout)findViewById(R.id.linear_membershipCredit);
        recyclerViewFreeService=(RecyclerView)findViewById(R.id.recycler_view_free_service) ;
        recyclerViewCreditHistory=(RecyclerView)findViewById(R.id.recycler_view_credit_history) ;
        txtViewExpirydate=(TextView)findViewById(R.id.txtViewExpirydate);
        txtViewAddContact=(TextView)findViewById(R.id.txtView_add_contact);

        txtViewAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ActivityCompat.checkSelfPermission(MyMembershipDetails.this, android.Manifest.permission.READ_CONTACTS)
                        == PackageManager.PERMISSION_GRANTED) {

                    try{

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(MyMembershipDetails.this);
                        builder1.setMessage("A family member once added to the family card cannot be removed.\n" +
                                "A request will be sent to the members not registered on Be U App and existing registered members will be notified regarding Family Wallet benefits.");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {


                                        SelectContactDailog fragment= new SelectContactDailog();
                                        Bundle bundle = new Bundle();
                                        bundle.putInt("totolUser", remainingAddUser);;
                                        fragment.setArguments(bundle);
                                        FragmentManager fm = getSupportFragmentManager();
                                        fragment.show(fm, TAG);

                                    }
                                });

                        builder1.setNegativeButton(
                                "No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.getWindow().setBackgroundDrawableResource(R.color.white);
                        alert11.show();



                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
                else{
                    requestLocationPermission();
                }
            }
        });
        txt_topup_membership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MyMembershipDetails.this,MemberShipCardAcitvity.class);
                intent.putExtra("from_home","from_home");
                startActivity(intent);
            }
        });
        fetchData();

        LinearLayoutManager layout= new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layout);
        adapterSelectedContact= new AdapterSelectedContact(this,memberList );
        recyclerView.setAdapter(adapterSelectedContact);

        LinearLayoutManager layoutCredit= new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewCreditHistory.setLayoutManager(layoutCredit);
        adapterMembershipCredit= new AdapterCreditHistory(this,creditHistoryList);
        recyclerViewCreditHistory.setAdapter(adapterMembershipCredit);
        adapterMembershipCredit.notifyDataSetChanged();


        LinearLayoutManager layoutFreeService= new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewFreeService.setLayoutManager(layoutFreeService);
        adapter= new AdapterMembershipFreeService(this,serviceList);
        recyclerViewFreeService.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    protected void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.READ_CONTACTS)) {
            // show UI part if you want here to show some rationale !!!
            Log.e("show","ui");
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_CONTACTS},
                    REQUEST_READ_CONTACTS);

        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_CONTACTS},
                    REQUEST_READ_CONTACTS);
            Log.e("show","succ");
        }

    }

    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(getResources().getString(R.string.my_family_wallet));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                  /*  SelectContactDailog fragment= new SelectContactDailog();
//
                    FragmentManager fm = getSupportFragmentManager();
                    fragment.show(fm, TAG);*/

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("result","result");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("onResume","onResume"+SelectContactDailog.contactSelectedList.size());

    }

    private void addUserContactNumber(){
        setContactList();
        AddUserContactNumber_post addUserContactNumberPost=new AddUserContactNumber_post();
        addUserContactNumberPost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        addUserContactNumberPost.setUserId(BeuSalonsSharedPrefrence.getUserId());
        addUserContactNumberPost.setMembershipSaleId(memberShipSellId);
        addUserContactNumberPost.setPhoneNumber(addContactList);
        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Log.e("contact",addContactList.toString());

        Call<AddContact_response> call=service.addMemberShipContact(addUserContactNumberPost);

        call.enqueue(new Callback<AddContact_response>() {
            @Override
            public void onResponse(Call<AddContact_response> call, Response<AddContact_response> response) {
                if (response.body().getSuccess()){
                    Toast.makeText(MyMembershipDetails.this,response.body().getData(),Toast.LENGTH_SHORT).show();
                    SelectContactDailog.contactSelectedList.clear();
                    fetchData();
                }
            }

            @Override
            public void onFailure(Call<AddContact_response> call, Throwable t) {

            }
        });
    }
    private void setContactList(){
        if (addContactList.size()>0){
            addContactList.clear();
        }
        for (int i=0;i<selectedList.size();i++){
            addContactList.add(selectedList.get(i).getMobileNumber());
        }
    }
    private void fetchData(){
        loading_for_.setVisibility(View.VISIBLE);
        linearLayoutCom.setVisibility(View.GONE);
        MembershipDetailsPost membershipDetailsPost=new MembershipDetailsPost();
        membershipDetailsPost.setUserId(BeuSalonsSharedPrefrence.getUserId());
        membershipDetailsPost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Log.e("userid",BeuSalonsSharedPrefrence.getAccessToken()+"+   userId"+BeuSalonsSharedPrefrence.getUserId());


        Call<MemberShipDetails_Response> call=service.getMemberShipDetails(membershipDetailsPost);

        call.enqueue(new Callback<MemberShipDetails_Response>() {
            @Override
            public void onResponse(Call<MemberShipDetails_Response> call, Response<MemberShipDetails_Response> response) {

                if (response.body() != null) {
                    if (response.body().getSuccess()) {

                        loading_for_.setVisibility(View.GONE);
                        linearLayoutCom.setVisibility(View.VISIBLE);
                        if (creditHistoryList.size() > 0) {
                            creditHistoryList.clear();
                        }
                        if (memberList.size() > 0) {
                            memberList.clear();
                        }
                        if (serviceList.size() > 0) {
                            serviceList.clear();
                        }
                        remainingAddUser=0;
                        txtMembershipName.setText(response.body().getData().getName());
                        txtViewDeal.setText(response.body().getData().getDealDiscountString());
                        txtViewMain.setText(response.body().getData().getMenuDiscountString());
                        txtCreditLeft.setText("Wallet Credit Left: " + response.body().getData().getCreditsLeft());
                        String[] expDate = response.body().getData().getExpiryDate().split("T");
                        String splitExpDate = expDate[0]; //
                        txtViewExpirydate.setText("Expiry: " + splitExpDate);


                        memberShipSellId = response.body().getData().getMembershipSaleId();

                        for (int i = 0; i < response.body().getData().getFreeServices().size(); i++) {
                            serviceList.add(response.body().getData().getFreeServices().get(i));
                        }

                        for (int i = 0; i < response.body().getData().getMembers().size(); i++) {
                            memberList.add(response.body().getData().getMembers().get(i));
                        }
                        if (response.body().getData().getNoOfMembersAllowed() > 0) {
                            txtViewAddContact.setVisibility(View.VISIBLE);
                        } else txtViewAddContact.setVisibility(View.GONE);

                        int ccreditAmount = 0;
                        for (int i = 0; i < response.body().getData().getHistory().size(); i++) {
                            creditHistoryList.add(response.body().getData().getHistory().get(i));
                            ccreditAmount += Integer.valueOf(response.body().getData().getHistory().get(i).getCreditUsed());
                        }


                        txtCreditUsed.setText("Credit Used: " + AppConstant.CURRENCY + ccreditAmount);


                        if ((response.body().getData().getNoOfMembersAllowed() - response.body().getData().getMembers().size() > 0)) {
                            remainingAddUser=response.body().getData().getNoOfMembersAllowed() - response.body().getData().getMembers().size();
                            txtViewAddContact.setVisibility(View.VISIBLE);
                            txtViewMembershipCount.setVisibility(View.VISIBLE);
                            if ((response.body().getData().getNoOfMembersAllowed() - response.body().getData().getMembers().size())==1){
                                txtViewMembershipCount.setText("Add " +
                                        (response.body().getData().getNoOfMembersAllowed() - response.body().getData().getMembers().size())+" Member");
                            }else{
                                txtViewMembershipCount.setText("Add " +
                                        (response.body().getData().getNoOfMembersAllowed() - response.body().getData().getMembers().size())+" Members");
                            }

                        } else {

                            txtViewAddContact.setVisibility(View.GONE);
                            txtViewMembershipCount.setVisibility(View.GONE);

                        }
                        adapter.notifyDataSetChanged();
                        adapterMembershipCredit.notifyDataSetChanged();
                        adapterSelectedContact.notifyDataSetChanged();

                        if (serviceList.size() > 0) {
                            linear_free_service.setVisibility(View.VISIBLE);
                        } else linear_free_service.setVisibility(View.GONE);

                        if (creditHistoryList.size() > 0) {
                            linear_membershipCredit.setVisibility(View.VISIBLE);
                        } else linear_membershipCredit.setVisibility(View.GONE);

                    } else if (!response.body().getSuccess()) {
                        loading_for_.setVisibility(View.GONE);
                        linearLayoutCom.setVisibility(View.GONE);
                        Toast.makeText(MyMembershipDetails.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<MemberShipDetails_Response> call, Throwable t) {

            }
        });
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        Log.e("dis event","dismiss");
        if (selectedList.size()>0){
            selectedList.clear();
        }
        selectedList.addAll(SelectContactDailog.contactSelectedList);
//        LinearLayoutManager layout= new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
//        recyclerView.setLayoutManager(layout);
//        AdapterSelectedContact adapterSelectedContact= new AdapterSelectedContact(this,selectedList);
//        recyclerView.setAdapter(adapterSelectedContact);
//        adapterSelectedContact.notifyDataSetChanged();
        Log.e("contact sise",""+selectedList.size()   +"cont"+SelectContactDailog.contactSelectedList.size());
        if (selectedList.size()>0 && isDismiss){
            addUserContactNumber();

        }
    }
}
