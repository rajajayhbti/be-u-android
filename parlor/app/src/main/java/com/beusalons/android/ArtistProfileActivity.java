package com.beusalons.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Fragment.ArtistCollectionFragment;
import com.beusalons.android.Fragment.ArtistProjectFragment;
import com.beusalons.android.Model.ArtistProfile.ArtistProfile_post;
import com.beusalons.android.Model.ArtistProfile.Artist_response;
import com.beusalons.android.Model.ArtistProfile.FollowArtist_post;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Ajay on 2/21/2018.
 */

public class ArtistProfileActivity extends AppCompatActivity{
    private String artistId="";
    TabLayout tabLayout;
    private List<String> tabs ;
    private MyAdapter adapter;
    ViewPager viewPager;
    ImageView img_ham;
    private ImageView img_user;
    private TextView txt_rating,txt_post,txt_post_name,txt_like,txt_name,txt_location,txt_like_name,txtFollowing;

    private boolean isFollow=false;
    private LinearLayout linearLayoutTop;
    private View progress_bar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.artist_profile_activity);


        setToolBar();
        tabLayout=findViewById(R.id.tabs);
        img_user=findViewById(R.id.img_user);
        viewPager = findViewById(R.id.viewpager);
        txt_name=findViewById(R.id.txt_name);
        txt_location=findViewById(R.id.txt_location);
        txt_post=findViewById(R.id.txt_post);
        txt_rating=findViewById(R.id.txt_rating);
        txt_like=findViewById(R.id.txt_like);
        txt_post_name=findViewById(R.id.txt_post_name);
        txt_like_name=findViewById(R.id.txt_like_name);
        txtFollowing=findViewById(R.id.txtFollowing);

        linearLayoutTop=findViewById(R.id.linear_top);
        progress_bar=findViewById(R.id.progress_bar);
        Intent intent = getIntent();
         artistId = intent.getStringExtra("artistId");
        fetchData();
    }


    private void inItView(){





        tabs=new ArrayList<>();
        tabs.clear();
        // tabs.add("Billing");
        tabs.add(getResources().getString(R.string.project));
        tabs.add(getResources().getString(R.string.collection));

        int position= 0;

        adapter=new MyAdapter(this.getSupportFragmentManager(),this,tabs);
        viewPager.setAdapter(adapter);

        viewPager.setCurrentItem(position);
        tabLayout.setupWithViewPager(viewPager);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(adapter.getTabView(i));

        }


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                //viewPager.reMeasureCurrentPage(viewPager.getCurrentItem());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                TextView txt= (TextView) tab.getCustomView().findViewById(R.id.txt_tab_name);
                txt.setTextColor(getResources().getColor(R.color.colorPrimary));


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {


                TextView txt= (TextView) tab.getCustomView().findViewById(R.id.txt_tab_name);
                txt.setTextColor(getResources()
                        .getColor(R.color.colorPrimaryText));


            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }



    private void fetchData(){
        linearLayoutTop.setVisibility(View.GONE);
        progress_bar.setVisibility(View.VISIBLE);
        ArtistProfile_post artistProfilePost=new ArtistProfile_post();

        artistProfilePost.setArtistId(artistId);
        Retrofit retrofit= ServiceGenerator.getPortfolioClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<Artist_response> call=apiInterface.getArtistProfile(artistProfilePost);

        call.enqueue(new Callback<Artist_response>() {
            @Override
            public void onResponse(Call<Artist_response> call, Response<Artist_response> response) {
                if (response.isSuccessful()){

                    if (response.body().getSuccess()){

                        linearLayoutTop.setVisibility(View.VISIBLE);
                        progress_bar.setVisibility(View.GONE);


                        txt_name.setText(response.body().getData().getFirstName());
                        txt_like.setText(""+response.body().getData().getPostsLikes());
                        txt_post.setText(""+response.body().getData().getPostsCount());

                        if(response.body().getData().getPostsCount()==0 ||
                                response.body().getData().getPostsCount()==1)
                            txt_post_name.setText("Post");
                        else
                            txt_post_name.setText("Posts");

                        txt_like.setText(""+response.body().getData().getPostsLikes());
                        if(response.body().getData().getPostsLikes()==0 ||
                                response.body().getData().getPostsLikes()==1)
                            txt_like_name.setText("Like");
                        else
                            txt_like_name.setText("Likes");

                        try{
                            Glide.with(ArtistProfileActivity.this).load(response.body().getData())
                                    .apply(new RequestOptions().placeholder(R.drawable.about_us_1))
                                    .apply(RequestOptions.circleCropTransform())

                                    .into(img_user);


                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        if (response.body().getData().getFollowedByMe()){
                            isFollow=true;
                            txtFollowing.setBackground(getResources().getDrawable(R.drawable.following_border));
                            txtFollowing.setText(getResources().getString(R.string.following));
                            txtFollowing.setTextColor(getResources().getColor(R.color.white));


                        }else if (!response.body().getData().getFollowedByMe()){
                            isFollow=false;
                            txtFollowing.setBackground(getResources().getDrawable(R.drawable.follow_border));
                            txtFollowing.setText(getResources().getString(R.string.follow));
                            txtFollowing.setTextColor(getResources().getColor(R.color.colorPrimary));
                        }


                        inItView();


                    }else if (!response.body().getSuccess()){
                        Toast.makeText(ArtistProfileActivity.this, "Looks Like The Server Is Taking To Long To Respond\n" +
                                " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
                        Log.i("homestuff", "i'm in failure: "+"getSuccess  false");
                    }


                }else if(!response.isSuccessful()){
                    Toast.makeText(ArtistProfileActivity.this, "Looks Like The Server Is Taking To Long To Respond\n" +
                            " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
                    Log.i("homestuff", "i'm in failure: "+"isSuccessful  false");

                }
            }

            @Override
            public void onFailure(Call<Artist_response> call, Throwable t) {
                Toast.makeText(ArtistProfileActivity.this, "Looks Like The Server Is Taking To Long To Respond\n" +
                        " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
                Log.i("homestuff", "i'm in failure: "+t.getMessage()+ " " + t.getCause()+ "   "+ t.getStackTrace());
            }
        });

    }

    private void artist_follow(String artistId){
        FollowArtist_post followArtist_post=new FollowArtist_post();
        followArtist_post.setArtistId(artistId);
        if (BeuSalonsSharedPrefrence.getUserId()!=null)
            followArtist_post.setUserId(BeuSalonsSharedPrefrence.getUserId());
        Retrofit retrofit= ServiceGenerator.getPortfolioClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<Artist_response> call=apiInterface.followArtist(followArtist_post);

        call.enqueue(new Callback<Artist_response>() {
            @Override
            public void onResponse(Call<Artist_response> call, Response<Artist_response> response) {

                if (response.isSuccessful()){

                    if (response.body().getSuccess()){











                        if (isFollow){

                            isFollow=false;
                            txtFollowing.setBackground(getResources().getDrawable(R.drawable.follow_border));
                            txtFollowing.setText(getResources().getString(R.string.follow));
                            txtFollowing.setTextColor(getResources().getColor(R.color.colorPrimary));
                            Toast.makeText(ArtistProfileActivity.this, "Successfully Follow", Toast.LENGTH_SHORT).show();



                        }else if (!isFollow){

                            isFollow=true;
                            txtFollowing.setBackground(getResources().getDrawable(R.drawable.following_border));
                            txtFollowing.setText(getResources().getString(R.string.following));
                            txtFollowing.setTextColor(getResources().getColor(R.color.white));
                            Toast.makeText(ArtistProfileActivity.this, "Successfully Following", Toast.LENGTH_SHORT).show();

                        }



                    }else if (!response.body().getSuccess()){

                        Toast.makeText(ArtistProfileActivity.this, "Looks Like The Server Is Taking To Long To Respond\n" +
                                " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
                        Log.i("homestuff", "i'm in failure: "+"getSuccess  false");
                    }
                }else if (!response.isSuccessful()){
                    Toast.makeText(ArtistProfileActivity.this, "Looks Like The Server Is Taking To Long To Respond\n" +
                            " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
                    Log.i("homestuff", "i'm in failure: "+"isSuccessful  false");
                }

            }

            @Override
            public void onFailure(Call<Artist_response> call, Throwable t) {
                Toast.makeText(ArtistProfileActivity.this, "Looks Like The Server Is Taking To Long To Respond\n" +
                        " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
                Log.i("homestuff", "i'm in failure: "+t.getMessage()+ " " + t.getCause()+ "   "+ t.getStackTrace());

            }
        });

    }


    private class MyAdapter extends FragmentStatePagerAdapter {

        private Context context;
        private List<String> str_tab_name;

        private MyAdapter(FragmentManager fm, Context context, List<String> str_tab_name){
            super(fm);
            this.context= context;
            this.str_tab_name= str_tab_name;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {

                case 0:
                    //return  ArtistProjectFragment.newInstance(artistProjectList);
                    return   ArtistProjectFragment.newInstance(artistId);
                case 1:
                    // return  ArtistCollectionFragment.newInstance(groupByCollectionList);
                    return      ArtistCollectionFragment.newInstance(artistId);

            }
            return null;

        }

        @Override
        public int getCount() {
            return tabs.size();
        }

        private View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(context).inflate(R.layout.custom_tab_layout, null);

            TextView textView= (TextView)v.findViewById(R.id.txt_tab_name);
            if(position==0){
                textView.setTextColor(getResources().getColor( R.color.colorPrimary));
            }else{
                textView.setTextColor(getResources().getColor( R.color.colorPrimaryText));
            }
            textView.setText(str_tab_name.get(position));

            return v;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){

            getSupportActionBar().setTitle(getResources().getString(R.string.artist_profile));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }
    }

}
