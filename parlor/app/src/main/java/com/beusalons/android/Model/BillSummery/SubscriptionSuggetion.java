package com.beusalons.android.Model.BillSummery;

/**
 * Created by Ashish Sharma on 1/25/2018.
 */

public class SubscriptionSuggetion {
    private String title;
    private String heading1;
    private String heading2;
    private int subscriptionId;
    private double payableAmount;
    private double redemableAmount;
    private String tnC;
    private String suggestion;
    private double amount;
    private double redemption;
    private String userSubscriptionOfferTerms;

    public String getUserSubscriptionOfferTerms() {
        return userSubscriptionOfferTerms;
    }

    public void setUserSubscriptionOfferTerms(String userSubscriptionOfferTerms) {
        this.userSubscriptionOfferTerms = userSubscriptionOfferTerms;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHeading1() {
        return heading1;
    }

    public void setHeading1(String heading1) {
        this.heading1 = heading1;
    }

    public String getHeading2() {
        return heading2;
    }

    public void setHeading2(String heading2) {
        this.heading2 = heading2;
    }

    public int getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(int subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getTnC() {
        return tnC;
    }

    public void setTnC(String tnC) {
        this.tnC = tnC;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getRedemption() {
        return redemption;
    }

    public void setRedemption(double redemption) {
        this.redemption = redemption;
    }

    public double getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(double payableAmount) {
        this.payableAmount = payableAmount;
    }

    public double getRedemableAmount() {
        return redemableAmount;
    }

    public void setRedemableAmount(double redemableAmount) {
        this.redemableAmount = redemableAmount;
    }
}
