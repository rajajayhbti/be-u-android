package com.beusalons.android.Adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Model.AboutUser.QuestionAns;
import com.beusalons.android.Model.AboutUser.Response.Datum.Question;
import com.beusalons.android.R;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayout;

import java.util.List;

/**
 * Created by myMachine on 17-Feb-18.
 */

public class AboutUserAdapter extends RecyclerView.Adapter<AboutUserAdapter.ViewHolder> {

//    public interface Listener{
//        void onClick(int count);
//    }
//    private Listener listener;
//    public void setListener(Listener listener){
//        this.listener= listener;
//    }

    private List<Question> list_;
    public AboutUserAdapter(List<Question> list_){
        this.list_= list_;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LinearLayout linear_= (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_aboutuser, parent,false);
        return new ViewHolder(linear_);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final int pos= position;

        final LinearLayout linear_= holder.linear_;

        TextView txt_question= linear_.findViewById(R.id.txt_question);
        txt_question.setText(list_.get(pos).getQuestion());

        TextView txt_option_type= linear_.findViewById(R.id.txt_option_type);
        if(list_.get(pos).getType().equalsIgnoreCase("singleSelect")){
            txt_option_type.setText("Select One ");
        }else{
            txt_option_type.setText("Select Multiple");
        }


        FlexboxLayout flex_= linear_.findViewById(R.id.flex_);
        flex_.removeAllViews();
        final int answer_size= list_.get(pos).getAnswers().size();
        for(int i=0;i<answer_size;i++){

            final int index= i;

            final CardView card_= (CardView) LayoutInflater.from(linear_.getContext())
                    .inflate(R.layout.flex_aboutuser, flex_, false);
            final TextView txt_answer= card_.findViewById(R.id.txt_answer);
            txt_answer.setText(list_.get(pos).getAnswers().get(index).getAnswer());

            if(list_.get(pos).getAnswers().get(index).isSelected()){
                card_.setCardBackgroundColor(ContextCompat.getColor(linear_.getContext(),
                        R.color.card_selected));
                card_.setCardElevation(6f);
                card_.setAlpha(.7f);
                txt_answer.setTextColor(ContextCompat.getColor(linear_.getContext(),
                        R.color.white));
            }else{
                card_.setCardBackgroundColor(ContextCompat.getColor(linear_.getContext(),
                        R.color.card_unselected));
                card_.setCardElevation(0f);
                card_.setAlpha(1);
                txt_answer.setTextColor(ContextCompat.getColor(linear_.getContext(),
                        R.color.textLight));
            }

            card_.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(list_.get(pos).getType().equalsIgnoreCase("singleSelect")){
                        Log.i("cardClick", "in the single");

                        if(list_.get(pos).getAnswers().get(index).isSelected())
                            list_.get(pos).getAnswers().get(index).setSelected(false);
                        else
                            list_.get(pos).getAnswers().get(index).setSelected(true);


                        int selected_size=0;
                        for(int a=0; a<answer_size;a++)
                            if(!list_.get(pos).getAnswers().get(a).isSelected())
                                selected_size++;

                        if(selected_size==answer_size)
                            list_.get(pos).getAnswers().get(index).setSelected(true);

                        for(int b=0;b<answer_size;b++)
                            if(b!=index)
                                list_.get(pos).getAnswers().get(b).setSelected(false);


                    }else/*else if(list_.get(pos).getType().equalsIgnoreCase("multiSelect"))*/{
                        Log.i("cardClick", "in the multi");

                        if(list_.get(pos).getAnswers().get(index).isSelected())
                            list_.get(pos).getAnswers().get(index).setSelected(false);
                        else
                            list_.get(pos).getAnswers().get(index).setSelected(true);

                    }

                    notifyDataSetChanged();



                }
            });

            flex_.addView(card_);
        }





    }


    @Override
    public int getItemCount() {
        if(list_!=null &&
                list_.size()>0)
            return list_.size();
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private LinearLayout linear_;
        public ViewHolder(View itemView) {
            super(itemView);
            linear_= (LinearLayout)itemView;
        }
    }


}
