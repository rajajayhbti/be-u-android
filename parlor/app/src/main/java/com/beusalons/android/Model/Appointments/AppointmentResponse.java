package com.beusalons.android.Model.Appointments;

/**
 * Created by myMachine on 12/12/2016.
 */

public class AppointmentResponse {

    private Boolean success;
    private String message;
    private AppointmentDataResponse data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public AppointmentDataResponse getData() {
        return data;
    }

    public void setData(AppointmentDataResponse data) {
        this.data = data;
    }
}
