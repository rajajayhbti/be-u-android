package com.beusalons.android.Model.AllParlors;

import java.util.List;

/**
 * Created by myMachine on 2/9/2017.
 */

public class ParlorData {


    public String name;
    public String parlorId;
    public Integer parlorType;
    public String link;
    public List<Object> images = null;
    public String address1;
    public String address2;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }

    public Integer getParlorType() {
        return parlorType;
    }

    public void setParlorType(Integer parlorType) {
        this.parlorType = parlorType;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public List<Object> getImages() {
        return images;
    }

    public void setImages(List<Object> images) {
        this.images = images;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }
}
