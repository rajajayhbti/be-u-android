package com.beusalons.android.Adapter.NewServiceDeals;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beusalons.android.Event.NewServicesEvent.UpgradeEvent;
import com.beusalons.android.Fragment.ServiceFragments.UpgradeBottomSheet;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Upgrade;
import com.beusalons.android.R;
import com.beusalons.android.ServiceSpecificActivity;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by myMachine on 5/25/2017.
 */

public class UpgradeListAdapter extends RecyclerView.Adapter<UpgradeListAdapter.ViewHolder> {

    private Context context;
    private List<Upgrade> list;
    private Dialog dialog;
    private AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String service_name;


    public UpgradeListAdapter(Context context, List<Upgrade> list, Dialog dialog,
                              String service_name){

        this.context= context;
        this.list= list;
        this.dialog= dialog;
        this.service_name= service_name;

        logger = AppEventsLogger.newLogger(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final Upgrade upgrade= list.get(position);
        holder.txt_name.setText(upgrade.getName());
        holder.txt_description.setText(upgrade.getDetail());

        //with tax
        int price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*upgrade.getPrice());
        holder.txt_price.setText(AppConstant.CURRENCY+price_);

        int menuprice=(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*upgrade.getMenuPrice());

        if (upgrade.getSave()>0){
            holder.txt_menu_price.setVisibility(View.VISIBLE);
            holder.txt_save_per.setVisibility(View.VISIBLE);
            holder.txt_menu_price.setText(AppConstant.CURRENCY+menuprice);
            holder.txt_save_per.setText("Save "+upgrade.getSave()+"%");
            holder.txt_save_per.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
            holder.txt_save_per.setBackgroundResource(R.drawable.discount_seletor);
            holder.txt_menu_price.setPaintFlags(holder.txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else{
            holder.txt_menu_price.setVisibility(View.GONE);
            holder.txt_save_per.setVisibility(View.GONE);
        }

        holder.txt_upgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logEditServiceBuyUpgradeEvent();
                logEditServiceBuyUpgradeFireBaseEvent();

                if(upgrade.getServiceId()!=null){           //toh page khulega

                    EventBus.getDefault().post(new UpgradeEvent(upgrade.getServiceId()));
                    dialog.dismiss();
                }else{                      //bottom sheet open hoga

                    UpgradeBottomSheet bottomSheet= new UpgradeBottomSheet();
                    Bundle bundle= new Bundle();
                    bundle.putString(UpgradeBottomSheet.SERVICE_NAME, service_name);
                    bundle.putString("upgrade", new Gson().toJson(upgrade, Upgrade.class));
                    bottomSheet.setArguments(bundle);
                    bottomSheet.show((((ServiceSpecificActivity)context)).getSupportFragmentManager(), "upgrade_bottom_sheet");
                }
            }
        });




    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logEditServiceBuyUpgradeEvent () {
        Log.e("EditServiceBuyUpgrade","fine");
        logger.logEvent(AppConstant.EditServiceBuyUpgrade);
    }

    public void logEditServiceBuyUpgradeFireBaseEvent () {
        Log.e("EditBuyFireBaseUpgrade","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.EditServiceBuyUpgrade,bundle);
    }
    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_name, txt_description, txt_price, txt_save_per, txt_upgrade,txt_menu_price;

        public ViewHolder(View itemView) {
            super(itemView);

            txt_name= (TextView)itemView.findViewById(R.id.txt_name);
            txt_description= (TextView)itemView.findViewById(R.id.txt_description);
            txt_price= (TextView)itemView.findViewById(R.id.txt_price);
            txt_save_per= (TextView)itemView.findViewById(R.id.txt_save_per);
            txt_upgrade= (TextView)itemView.findViewById(R.id.txt_upgrade);
            txt_menu_price= (TextView)itemView.findViewById(R.id.txt_menu_price);
        }
    }
    @Override
    public int getItemCount() {
        return list.size();
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.fragment_services_specific_dialog_upgrade, parent, false);
        return new ViewHolder(view);
    }
}
