package com.beusalons.android.Model.ArtistProfile;

import java.util.List;

/**
 * Created by Ajay on 1/29/2018.
 */

public class ArtistData {
    private String firstName;
    private String description;
    private Integer postsCount;
    private Integer postsLikes;
    private Boolean followedByMe=false;
    private List<ArtistProject_> projects = null;
    private List<GroupByCollection> groupByCollection = null;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPostsCount() {
        return postsCount;
    }

    public void setPostsCount(Integer postsCount) {
        this.postsCount = postsCount;
    }

    public Integer getPostsLikes() {
        return postsLikes;
    }

    public void setPostsLikes(Integer postsLikes) {
        this.postsLikes = postsLikes;
    }

    public Boolean getFollowedByMe() {
        return followedByMe;
    }

    public void setFollowedByMe(Boolean followedByMe) {
        this.followedByMe = followedByMe;
    }

    public List<ArtistProject_> getProjects() {
        return projects;
    }

    public void setProjects(List<ArtistProject_> projects) {
        this.projects = projects;
    }

    public List<GroupByCollection> getGroupByCollection() {
        return groupByCollection;
    }

    public void setGroupByCollection(List<GroupByCollection> groupByCollection) {
        this.groupByCollection = groupByCollection;
    }
}
