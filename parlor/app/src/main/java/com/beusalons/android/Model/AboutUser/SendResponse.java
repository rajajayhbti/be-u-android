package com.beusalons.android.Model.AboutUser;

/**
 * Created by myMachine on 17-Feb-18.
 */

public class SendResponse {

    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
