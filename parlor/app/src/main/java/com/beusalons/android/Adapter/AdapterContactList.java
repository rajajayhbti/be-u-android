package com.beusalons.android.Adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Model.MymembershipDetails.ContactModel;
import com.beusalons.android.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ajay on 11/7/2017.
 */

public class AdapterContactList  extends RecyclerView.Adapter<AdapterContactList.MyViewHolder>{

    private List<ContactModel> list=new LinkedList<>();
    Activity activity;
    private ArrayList<ContactModel> myArrayList=new ArrayList<>();
    int totalUser=0;
    public AdapterContactList(Activity myActivity, List<ContactModel> cusData,int total){
        this.list=cusData;
        this.activity=myActivity;
        myArrayList.addAll(cusData);
        this.totalUser=total;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_list_row, parent, false);

        return new AdapterContactList.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final  MyViewHolder holder, final int position) {

        holder.txtViewCustomerNumer.setText(list.get(position).getMobileNumber().trim().toString());
        holder.txtViewName.setText(list.get(position).getName());




//        try{
//
//            Glide.with(linear_.getContext())
//                    .load(list.get(position).getUserImage()).asBitmap().centerCrop()
//                    .into(new BitmapImageViewTarget(img_user){
//                        @Override
//                        protected void setResource(Bitmap resource) {
//                            RoundedBitmapDrawable circularBitmapDrawable =
//                                    RoundedBitmapDrawableFactory.create(linear_.getContext().getResources(), resource);
//                            circularBitmapDrawable.setCircular(true);
//                            img_user.setImageDrawable(circularBitmapDrawable);
//                        }
//                    });
//        }catch (Exception e){
//            e.printStackTrace();
//        }



        try{
            Glide.with(activity).load(list.get(position).getPhotoURI()).apply(RequestOptions.circleCropTransform()).into(holder.imgUserPro);


          /*  Glide.with(activity).load(list.get(position).getPhotoURI()).asBitmap().centerCrop().into(new BitmapImageViewTarget(holder.imgUserPro) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(activity.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    holder.imgUserPro.setImageDrawable(circularBitmapDrawable);
                }
            });*/

        }catch (Exception e){
            e.printStackTrace();
        }

        holder.checkBox.setChecked(list.get(position).isSelected());

        holder.linearSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (totalUser>=0){
                    if (list.get(position).isSelected() ){
                        list.get(position).setSelected(false);
                        totalUser+=1;
                        holder.checkBox.setChecked(list.get(position).isSelected());
                        Log.e("selected",list.get(position).getMobileNumber()+"true hona chai"+ totalUser+list.get(position).isSelected());

                    }
                    else{
                        if (totalUser>0){
                            list.get(position).setSelected(true);
                            totalUser-=1;
                            holder.checkBox.setChecked(list.get(position).isSelected());
                            Log.e("selected",list.get(position).getMobileNumber()+"false hona chai"+totalUser+list.get(position).isSelected());

                        }else{
                            list.get(position).setSelected(false);
                            holder.checkBox.setChecked(false);
                            Toast.makeText(activity,"You can not add now",Toast.LENGTH_SHORT).show();
                        }
                    }
                }else {
                    list.get(position).setSelected(false);
                    holder.checkBox.setChecked(false);
                    Toast.makeText(activity,"You can not add now",Toast.LENGTH_SHORT).show();
                }

            }
        });


        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (totalUser>=0){
                    if (list.get(position).isSelected() ){
                        list.get(position).setSelected(false);
                        totalUser+=1;
                        holder.checkBox.setChecked(list.get(position).isSelected());
                        Log.e("selected",list.get(position).getMobileNumber()+"true hona chai"+ totalUser+list.get(position).isSelected());

                    }
                    else{
                       if (totalUser>0){
                           list.get(position).setSelected(true);
                           totalUser-=1;
                           holder.checkBox.setChecked(list.get(position).isSelected());
                           Log.e("selected",list.get(position).getMobileNumber()+"false hona chai"+totalUser+list.get(position).isSelected());

                       }else{
                           list.get(position).setSelected(false);
                           holder.checkBox.setChecked(false);
                           Toast.makeText(activity,"You can not add now",Toast.LENGTH_SHORT).show();
                       }
                    }
                }else {
                    list.get(position).setSelected(false);
                    holder.checkBox.setChecked(false);
                    Toast.makeText(activity,"You can not add now",Toast.LENGTH_SHORT).show();
                }


            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(myArrayList);
        } else {
            System.out.println("pubStocks size" + myArrayList.size());
            for (ContactModel cs : myArrayList) {
                if (cs.getName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    System.out.println("cs.getName()" + cs.getName());
                    list.add(cs);
                } else if (cs.getMobileNumber().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    list.add(cs);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class MyViewHolder extends  RecyclerView.ViewHolder{
        @BindView(R.id.contact_name)
        TextView txtViewName;
        @BindView(R.id.contact_number)  TextView txtViewCustomerNumer;

        @BindView(R.id.img_user_profile)
        ImageView imgUserPro;

        @BindView(R.id.checkBox_selected)
        CheckBox checkBox;
        @BindView(R.id.linear_Select)
        LinearLayout linearSelect;
        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}