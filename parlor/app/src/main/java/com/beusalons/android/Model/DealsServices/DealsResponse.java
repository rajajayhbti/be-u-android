package com.beusalons.android.Model.DealsServices;

/**
 * Created by Ajay on 6/9/2017.
 */

public class DealsResponse {

    private Boolean success;
    private Data data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
