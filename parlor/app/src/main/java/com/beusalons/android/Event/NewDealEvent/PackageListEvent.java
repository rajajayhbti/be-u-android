package com.beusalons.android.Event.NewDealEvent;

import com.beusalons.android.Model.UserCart.UserServices;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 9/20/2017.
 */

public class PackageListEvent {

    private String name;
    private boolean isAlter;
    private List<UserServices> list= new ArrayList<>();

    public PackageListEvent(List<UserServices> list, String name, boolean isAlter){
        this.name= name;
        this.isAlter= isAlter;
        this.list= list;
    }


    public boolean isAlter() {
        return isAlter;
    }

    public String getName() {
        return name;
    }

    public void setList(List<UserServices> list) {
        this.list = list;
    }


    public List<UserServices> getList() {
        return list;
    }

}
