package com.beusalons.android.Model.HomeFragmentModel;

import java.util.List;

/**
 * Created by Ashish sharma on 3/2/2017.
 */

public class HomePageData {

    public Boolean success;
    public Data data;


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {


        private List<PopularSalon> popularSalons = null;
        private List<NearBySalon> nearBySalons = null;
        private List<PopularDeals> deals=null;
        private List<FreeBees> freebies=null;
        private List<NewPackages> packages = null;
        private List<Carousel> carousels= null;
        private List<DiscountRules> discountRules=null;
        private int notificationCount;
        private String referCode;

        public List<DiscountRules> getDiscountRules() {
            return discountRules;
        }

        public void setDiscountRules(List<DiscountRules> discountRules) {
            this.discountRules = discountRules;
        }

        public List<Carousel> getCarousels() {
            return carousels;
        }

        public void setCarousels(List<Carousel> carousels) {
            this.carousels = carousels;
        }

        public List<PopularSalon> getPopularSalons() {
            return popularSalons;
        }

        public void setPopularSalons(List<PopularSalon> popularSalons) {
            this.popularSalons = popularSalons;
        }

        public List<NearBySalon> getNearBySalons() {
            return nearBySalons;
        }

        public void setNearBySalons(List<NearBySalon> nearBySalons) {
            this.nearBySalons = nearBySalons;
        }

        public List<PopularDeals> getDeals() {
            return deals;
        }

        public void setDeals(List<PopularDeals> deals) {
            this.deals = deals;
        }

        public List<FreeBees> getFreebies() {
            return freebies;
        }

        public void setFreebies(List<FreeBees> freebies) {
            this.freebies = freebies;
        }

        public List<NewPackages> getPackages() {
            return packages;
        }

        public void setPackages(List<NewPackages> packages) {
            this.packages = packages;
        }

        public int getNotificationCount() {
            return notificationCount;
        }

        public void setNotificationCount(int notificationCount) {
            this.notificationCount = notificationCount;
        }

        public String getReferCode() {
            return referCode;
        }

        public void setReferCode(String referCode) {
            this.referCode = referCode;
        }
    }
}
