package com.beusalons.android.Adapter.FreeBies;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beusalons.android.AboutUserActivity;
import com.beusalons.android.Model.Loyalty.ArrayInfo;
import com.beusalons.android.Model.Loyalty.NewFreeBy;
import com.beusalons.android.ParlorListActivity;
import com.beusalons.android.R;
import com.beusalons.android.WebViewActivity;
import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Random;

/**
 * Created by Ashish Sharma on 9/7/2017.
 */

public class FreebiesCategoryAdapterBackUp extends RecyclerView.Adapter<FreebiesCategoryAdapterBackUp.ViewHolder> {
    private Context context;
    private List<NewFreeBy > freeByList;

    public FreebiesCategoryAdapterBackUp(Context context, List<NewFreeBy > freeByList) {
        this.context=context;
        this.freeByList=freeByList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.row_new_freebie_backup, parent, false);
        return new FreebiesCategoryAdapterBackUp.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        final  InnerFreebyGrid adapter = null;
        final InnerFreebyGrid adapter;
        final NewFreeBy freeBy=freeByList.get(position);

        holder.txt_name.setText(freeBy.getTitle());
        holder.txt_info.setText(freeBy.getMessage());
        final int pos=position;
        Glide.with(context)
                .load(freeBy.getImage())
//                .centerCrop()
                .into(holder.img_);
        /*holder.img_2.setVisibility(View.INVISIBLE);
        holder.img_3.setVisibility(View.INVISIBLE);
        holder.txt_title2.setVisibility(View.INVISIBLE);
        holder.txt_title3.setVisibility(View.INVISIBLE);*/
      /*  holder.linear_click.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
        holder.Linear_share.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
        holder.view1.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGreen));
        holder.txt_name_outer.setText(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoTitle());
        holder.txt_info_outer.setText(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(0));*/
        View.OnClickListener innerClick  =new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ArrayInfo arrayInfo=(ArrayInfo)v.getTag();
                Test test=(Test)v.getTag();
                Log.e("testt",test.getArrayInfo().getInfoTitle());
                holder.txt_name_outer.setText(test.getArrayInfo().getInfoTitle());
//                holder.txt_info_outer.setText(test.getArrayInfo().getInfoMessage().get(0));
//test.getArrayInfo().
                Glide.with(context)
                        .load(test.getArrayInfo().getInfoIcon())
//                .centerCrop()
                        .into(holder.img_outer);
                if (test.getArrayInfo().getInfoMessage().size()>1){

                    String txt="";
                    for(int t=0;t<test.getArrayInfo().getInfoMessage().size();t++){

                        txt= txt+ "\u2022 " + test.getArrayInfo().getInfoMessage().get(t).toString() + "\n";

                    }
                    holder.txt_info_outer.setText(txt);



                }else holder.txt_info_outer.setText(test.getArrayInfo().getInfoMessage().get(0));

                if (position % 2==0){

                    holder.Linear_share.setBackgroundColor(context.getResources().getColor(R.color.blue_DARK));
                }else {
                    holder.Linear_share.setBackgroundColor(context.getResources().getColor(R.color.yellow_fedded));
                }
                if (test.getArrayInfo().getTnc().size()>0){
                    holder.txt_tnc_outer.setVisibility(View.VISIBLE);
                    String tAndc="";
                    for (int t=0;t<test.getArrayInfo().getTnc().size();t++){
                        tAndc= tAndc+ "\u2022 " + test.getArrayInfo().getTnc().get(t).toString() + "\n";
                    }
                    holder.txt_tnc_outer.setText(tAndc);
                }else holder.txt_tnc_outer.setVisibility(View.GONE);


                test.getInnerFreebyGrid().notifydata(test.getArrayInfo().getIndideArrayPosition());
                test.getInnerFreebyGrid().notifyDataSetChanged();

                createButttons(test.getArrayInfo(),holder);

            }
        };
        if (freeByList.get(pos).isSelected()) {
//            final InnerFreebyGrid finalAdapter = adapter;

            holder.img_animation.setImageResource(R.drawable.ic_arrow_down);
            // holder.inside_array.setVisibility(View.GONE);
            holder.rec_inner_freeby.setVisibility(View.VISIBLE);
            holder.Linear_share.setVisibility(View.VISIBLE);
//            GridLayoutManager layoutManager= new GridLayoutManager(context, 3);
            LinearLayoutManager layoutManager=new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            holder.rec_inner_freeby.setLayoutManager(layoutManager);
            for(int i=0;i<freeByList.get(pos).getInsideArray().size();i++){

                ///if(i!=pos){
                freeByList.get(pos).getInsideArray().get(i).setSelected(false);

                // }
            }
            freeByList.get(pos).getInsideArray().get(0).setSelected(true);

            adapter=new InnerFreebyGrid(freeByList.get(pos).getInsideArray(),context,innerClick, position);
            holder.rec_inner_freeby.setAdapter(adapter);


            Glide.with(context)
                    .load(freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getInfoIcon())
//                .centerCrop()
                    .into(holder.img_outer);

            holder.txt_name_outer.setText(freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getInfoTitle());
//            holder.txt_info_outer.setText(freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(0));

            if (freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().size()>1){

                String txt="";
                for(int t=0;t<freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().size();t++){

                    txt= txt+ "\u2022 " + freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(t).toString() + "\n";

                }
                holder.txt_info_outer.setText(txt);
                holder.txt_info_outer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent in = new Intent(context,WebViewActivity.class);
                        in.putExtra("url", "http://beusalons.com/appTermsConditions");
                        in.putExtra("title", "Terms & Conditions");
                        context.startActivity(in);
                    }
                });
            }else holder.txt_info_outer.setText(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(0));
//            holder.Linear_share.setBackgroundColor(context.getResources().getColor(R.color.blue_DARK));

            if (freeBy.getInsideArray().get(0).getArrayInfo().get(0).getTnc().size()>0){
                holder.txt_tnc_outer.setVisibility(View.VISIBLE);
                String tAndc="";
                for (int t=0;t<freeBy.getInsideArray().get(0).getArrayInfo().get(0).getTnc().size();t++){
                    tAndc= tAndc+ "\u2022 " + freeBy.getInsideArray().get(0).getArrayInfo().get(0).getTnc().get(t).toString() + "\n";
                }
                holder.txt_tnc_outer.setText(tAndc);
            }else holder.txt_tnc_outer.setVisibility(View.GONE);
            if (position % 2==0){

                holder.Linear_share.setBackgroundColor(context.getResources().getColor(R.color.blue_DARK));
            }else {
                holder.Linear_share.setBackgroundColor(context.getResources().getColor(R.color.yellow_fedded));
            }
            createButttons(freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0),holder);
            freeByList.get(pos).setSelected(true);
            if (position % 2==0){
                holder.linear_title.setBackgroundColor(context.getResources().getColor(R.color.yellow_fedded));
                holder.ll_full_freeby.setBackgroundColor(context.getResources().getColor(R.color.yellow_fedded));

            }else {
                holder.linear_title.setBackgroundColor(context.getResources().getColor(R.color.blue_DARK));
                holder.ll_full_freeby.setBackgroundColor(context.getResources().getColor(R.color.blue_DARK));

            }
            /*if (position%4==2){
                holder.linear_title.setBackgroundColor(context.getResources().getColor(R.color.blue_DARK));
                //holder.linear_add_.setBackgroundResource(R.drawable.drawable_blue);

            }else  holder.linear_title.setBackgroundColor(context.getResources().getColor(R.color.blue_DARK));*/
//            freeByList.get(pos).setSelected(false);
        } else {
//                    final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,1);

//            freeByList.get(pos).setSelected(true);
            holder.rec_inner_freeby.setVisibility(View.GONE);
            holder.Linear_share.setVisibility(View.GONE);
            holder.ll_btn_container.setVisibility(View.GONE);
            holder.linear_title.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.ll_full_freeby.setBackgroundColor(context.getResources().getColor(R.color.white));

            holder.img_animation.setImageResource(R.drawable.ic_right);
            for(int i=0;i<freeByList.get(pos).getInsideArray().size();i++){

                ///if(i!=pos){
                freeByList.get(pos).getInsideArray().get(i).setSelected(false);

                // }
            }

        }

        holder.linear_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {




                View.OnClickListener innerClick  =new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                ArrayInfo arrayInfo=(ArrayInfo)v.getTag();
                        Test test=(Test)v.getTag();
                        Log.e("testt22",test.getArrayInfo().getInfoTitle());
                        holder.txt_name_outer.setText(test.getArrayInfo().getInfoTitle());
//                        holder.txt_info_outer.setText(test.getArrayInfo().getInfoMessage().get(0));
//test.getArrayInfo().
                        if (test.getArrayInfo().getInfoMessage().size()>1){

                            String txt="";
                            for(int t=0;t<test.getArrayInfo().getInfoMessage().size();t++){

                                txt= txt+ "\u2022 " + test.getArrayInfo().getInfoMessage().get(t).toString() + "\n";

                            }
                            holder.txt_info_outer.setText(txt);

                        }else holder.txt_info_outer.setText(test.getArrayInfo().getInfoMessage().get(0));
                        if (test.getArrayInfo().getIndideArrayPosition() % 2==0){

                            holder.Linear_share.setBackgroundColor(context.getResources().getColor(R.color.blue_DARK));
                        }else {
                            holder.Linear_share.setBackgroundColor(context.getResources().getColor(R.color.yellow_fedded));
                        }
                        createButttons(test.getArrayInfo(),holder);
                        test.getInnerFreebyGrid().notifydata(test.getArrayInfo().getIndideArrayPosition());
                        test.getInnerFreebyGrid().notifyDataSetChanged();

                        createButttons(test.getArrayInfo(),holder);

                    }
                };
                if (freeByList.get(pos).isSelected()) {

                    holder.img_animation.setImageResource(R.drawable.ic_right);
                    // holder.inside_array.setVisibility(View.GONE);
                    holder.rec_inner_freeby.setVisibility(View.GONE);
                    holder.ll_btn_container.setVisibility(View.GONE);
                    holder.Linear_share.setVisibility(View.GONE);
                    freeByList.get(pos).setSelected(false);
                    holder.linear_title.setBackgroundColor(context.getResources().getColor(R.color.white));
                    holder.ll_full_freeby.setBackgroundColor(context.getResources().getColor(R.color.white));
                } else {

                    holder.rec_inner_freeby.setVisibility(View.VISIBLE);
                    holder.ll_btn_container.setVisibility(View.GONE);
                    holder.Linear_share.setVisibility(View.VISIBLE);
//                    final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,1);
                    LinearLayoutManager layoutManager=new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
//                    GridLayoutManager layoutManager= new GridLayoutManager(context, 3);
                    holder.rec_inner_freeby.setLayoutManager(layoutManager);

                    InnerFreebyGrid adapter=new InnerFreebyGrid(freeByList.get(pos).getInsideArray(),context,innerClick,position);
                    holder.rec_inner_freeby.setAdapter(adapter);
                    freeByList.get(pos).setSelected(true);
                    holder.img_animation.setImageResource(R.drawable.ic_arrow_down);



                    holder.Linear_share.setBackgroundColor(context.getResources().getColor(R.color.blue_DARK));


                    holder.txt_name_outer.setText(freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getInfoTitle());
//                    holder.txt_info_outer.setText(freeByList.get(pos).getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(0));
                    if (freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().size()>1){

                        String txt="";
                        for(int t=0;t<freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().size();t++){

                            txt= txt+ "\u2022 " + freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(t).toString() + "\n";

                        }
                        holder.txt_info_outer.setText(txt);

                    }else holder.txt_info_outer.setText(freeBy.getInsideArray().get(0).getArrayInfo().get(0).getInfoMessage().get(0));

                    updateView(pos);


                }



            }
        });

    }






    private void updateView(int pos){


        for(int i=0;i<freeByList.size();i++){

            if(i!=pos){
                freeByList.get(i).setSelected(false);

            }
        }
        notifyDataSetChanged();
    }
    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }
    private void createButttons( ArrayInfo arrayInfo,ViewHolder holder){
        if (arrayInfo.getButtons().size()>0){
            holder.ll_btn_container.setVisibility(View.VISIBLE);
            holder.ll_btn_container.removeAllViews();
            for (int i=0;i<arrayInfo.getButtons().size();i++){
                TextView vi1=new TextView(context);
                Random rand = new Random();
                int value = rand.nextInt(1150);
                LinearLayout.LayoutParams textViewLayoutParams =  new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                textViewLayoutParams.setMargins(0,0,30,15);
                vi1.setLayoutParams(textViewLayoutParams);
                if (arrayInfo.getButtons().get(i).getButtonColor().equalsIgnoreCase("red")){

                    vi1.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    vi1.setBackground(ContextCompat.getDrawable(context, R.drawable.shape_freebies_red));

                }else {
                    vi1.setTextColor(ContextCompat.getColor(context, R.color.colorGreen));

                    vi1.setBackground(ContextCompat.getDrawable(context, R.drawable.shape_freebies_green));
                }
                vi1.setId(arrayInfo.getButtons().size()+value);
                vi1.setText(arrayInfo.getButtons().get(i).getButtonTitle());
                vi1.setTextSize(10);
                vi1.setPadding(16,8,16,8);

                final int finalI = i;
                final ArrayInfo arrayInfo1=arrayInfo;
                vi1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (arrayInfo1.getButtons().get(finalI).getButtonAction().equalsIgnoreCase("book")){
                            Intent intent= new Intent(context, ParlorListActivity.class);
                            intent.putExtra("isService",true);
                            context.startActivity(intent);

                        }else if (arrayInfo1.getButtons().get(finalI).getButtonAction().equalsIgnoreCase("refer")){
                            Intent sendIntent= new Intent();
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.setType("text/plain");

                            //have to set title and content
                            sendIntent.putExtra(Intent.EXTRA_TEXT ,arrayInfo1.getButtonMessage().get(0));
//                sendIntent.putExtra(Intent.EXTRA_TEXT, "Share this link with your friends. Avail yourself a free haircut at any of the Be U salons by downloading Be U app.");
                            context.startActivity(Intent.createChooser(sendIntent, "Share code:"));
                        }else if (arrayInfo1.getButtons().get(finalI).getButtonAction().equalsIgnoreCase("preference")){
                            context.startActivity(new Intent(v.getContext(), AboutUserActivity.class));
                        }else if (arrayInfo1.getButtons().get(finalI).getButtonAction().equalsIgnoreCase("share")){
                            Intent sendIntent= new Intent();
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.setType("text/plain");

                            //have to set title and content
                            sendIntent.putExtra(Intent.EXTRA_TEXT ,arrayInfo1.getButtonMessage().get(0));
//                sendIntent.putExtra(Intent.EXTRA_TEXT, "Share this link with your friends. Avail yourself a free haircut at any of the Be U salons by downloading Be U app.");
                            context.startActivity(Intent.createChooser(sendIntent, "Share code:"));
                        }else if (arrayInfo1.getButtons().get(finalI).getButtonAction().equalsIgnoreCase("corporateRefer")){
                            Intent sendIntent= new Intent();
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.setType("text/plain");

                            //have to set title and content
                            sendIntent.putExtra(Intent.EXTRA_TEXT ,arrayInfo1.getButtonMessage().get(0));
//                sendIntent.putExtra(Intent.EXTRA_TEXT, "Share this link with your friends. Avail yourself a free haircut at any of the Be U salons by downloading Be U app.");
                            context.startActivity(Intent.createChooser(sendIntent, "Share code:"));
                        }
                    }
                });
//                container_card.addView(vi1);
                holder.ll_btn_container.addView(vi1);
            }
        }
        else holder.ll_btn_container.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return freeByList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private RecyclerView rec_inner_freeby;

        private TextView txt_name, txt_info,txt_title,txt_info_outer,txt_name_outer,txt_title2,txt_title3,btnFirst,btnSecond,txt_tnc_outer;
        private ImageView img_, img_animation,img_title,img_outer,img_2,img_3;
        private RelativeLayout linear_title;
        LinearLayout inside_array,Linear_share,linear_click,linear_click2,linear_click3,ll_full_freeby;
        private RecyclerView rec_services;
        LinearLayout ll_btn_container;
        private View view1,view2,view3;

        public ViewHolder(View itemView) {
            super(itemView);

            txt_name= (TextView)itemView.findViewById(R.id.txt_name);
            txt_info= (TextView)itemView.findViewById(R.id.txt_info);
            txt_title= (TextView)itemView.findViewById(R.id.txt_title);
            txt_info_outer= (TextView)itemView.findViewById(R.id.txt_info_outer);
            txt_name_outer= (TextView)itemView.findViewById(R.id.txt_name_outer);
            txt_title2= (TextView)itemView.findViewById(R.id.txt_title2);
            txt_title3= (TextView)itemView.findViewById(R.id.txt_title3);
            img_= (ImageView) itemView.findViewById(R.id.img_);
            img_title= (ImageView) itemView.findViewById(R.id.img_title);
            img_2= (ImageView) itemView.findViewById(R.id.img_2);
            img_3= (ImageView) itemView.findViewById(R.id.img_3);
            img_outer= (ImageView) itemView.findViewById(R.id.img_outer);
            img_animation= (ImageView)itemView.findViewById(R.id.img_animation);
            linear_title= (RelativeLayout) itemView.findViewById(R.id.linear_title);
            inside_array= (LinearLayout) itemView.findViewById(R.id.inside_array);
            Linear_share= (LinearLayout) itemView.findViewById(R.id.Linear_share);
            ll_full_freeby= (LinearLayout) itemView.findViewById(R.id.ll_full_freeby);
            linear_click= (LinearLayout) itemView.findViewById(R.id.linear_click);
            linear_click2= (LinearLayout) itemView.findViewById(R.id.linear_click2);
            linear_click3= (LinearLayout) itemView.findViewById(R.id.linear_click3);
            view1=(View) itemView.findViewById(R.id.view1);
            view2=(View) itemView.findViewById(R.id.view2);
            view3=(View) itemView.findViewById(R.id.view3);
            btnFirst= (TextView) itemView.findViewById(R.id.btn_freeby_first);
            btnSecond= (TextView) itemView.findViewById(R.id.btn_freeby_second);
            txt_tnc_outer= (TextView) itemView.findViewById(R.id.txt_tnc_outer);
            rec_inner_freeby= (RecyclerView) itemView.findViewById(R.id.rec_inner_freeby);
            ll_btn_container=(LinearLayout) itemView.findViewById(R.id.ll_btn_container);
//            rec_services= (RecyclerView) itemView.findViewById(R.id.rec_services);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }
}
