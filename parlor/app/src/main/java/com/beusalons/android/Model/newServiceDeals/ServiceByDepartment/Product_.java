package com.beusalons.android.Model.newServiceDeals.ServiceByDepartment;

/**
 * Created by myMachine on 5/30/2017.
 */

public class Product_ {

    private String id;
    private double ratio;
    private String name;
    private String productId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getRatio() {
        return ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
