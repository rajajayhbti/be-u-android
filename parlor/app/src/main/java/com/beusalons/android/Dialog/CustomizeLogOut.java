package com.beusalons.android.Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.LoginActivity;
import com.beusalons.android.Model.Logout.LogoutModel;
import com.beusalons.android.Model.StatusModel;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Ajay on 1/27/2017.
 */

public class CustomizeLogOut {
     Dialog dialog;
    Button btnYes,btnNo;
    LogoutModel logoutModel;
    String accessToken="",userId="";
    SharedPreferences sharedPref, shared_pref_location;
    UserCart userCart=new UserCart();
    //initialize the imageview in left navigation drawer
    public CustomizeLogOut(final Activity activity) {

        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logout);

        btnYes=(Button)dialog.findViewById(R.id.btnyes);
        btnNo=(Button)dialog.findViewById(R.id.btnNo);
        sharedPref= activity.getSharedPreferences
                ("userDetails", Context.MODE_PRIVATE);
        shared_pref_location=activity.getSharedPreferences      //location ka hai yeh
                ("beusalons_preferences", Context.MODE_PRIVATE);

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Runnable runnable= new Runnable() {
                    @Override
                    public void run() {

                        getUserData();
                        postLogOutDeatil();
                        DB snappyDB = null;

                        /*
                        * clear snappy db only user cart notification db not clear
                        * */
                        try {
                            snappyDB = DBFactory.open(activity);
                            if(snappyDB.exists(AppConstant.USER_CART_DB)) {
                                Log.i("mainyahatukaha", " existing hai");
                                snappyDB.del(AppConstant.USER_CART_DB);
                            }
                            snappyDB.close();
                        } catch (SnappydbException e) {
                            e.printStackTrace();
                        }

                        BeuSalonsSharedPrefrence.clearData();
                    }
                };
                new Thread(runnable).start();

                activity.startActivity(new Intent(activity, LoginActivity.class));
                dialog.dismiss();               //dismiss the dialog then call the finish activity
                activity.finish();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();



    }


    public void   getUserData(){

        accessToken=sharedPref.getString("accessToken",null);
        userId=sharedPref.getString("userId",null);
        logoutModel=new LogoutModel();
        logoutModel.setUserId(userId);
        logoutModel.setAccessToken(accessToken);
    }

    public void postLogOutDeatil(){

        Retrofit retrofit1 = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit1.create(ApiInterface.class);
        Call<StatusModel> call= apiInterface.logout(logoutModel);
        call.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                if (response!=null){
                    Log.i("response log out",response.message());
//                    SharedPreferences sharedPref= activity.getSharedPreferences
//                            ("userDetails", Context.MODE_PRIVATE);
                    if (response.message().equals("OK")){

                        Log.i("response log out1",response.message());

                    }

                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {

            }
        });

    }
}
