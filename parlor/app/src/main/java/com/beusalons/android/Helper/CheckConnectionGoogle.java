package com.beusalons.android.Helper;

import android.content.Context;

import java.io.IOException;

/**
 * Created by myMachine on 5/5/2017.
 */

public class CheckConnectionGoogle {

    public static boolean isConnected(Context context) {

        String command = "ping -c 1 google.com";

        try {
            return (Runtime.getRuntime().exec (command).waitFor() == 0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;

    }
}
