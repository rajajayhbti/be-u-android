package com.beusalons.android.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Model.selectArtist.Employee;
import com.beusalons.android.Model.selectArtist.Services;
import com.beusalons.android.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.support.v4.content.ContextCompat.getColor;

/**
 * Created by Ashish Sharma on 12/30/2017.
 */

public class SelectEmployeeAdapter extends RecyclerView.Adapter<SelectEmployeeAdapter.MyViewHolder>
{
    private Context context;
    private ArrayList<Services> services;

    public SelectEmployeeAdapter(Context context, ArrayList<Services> services) {
        this.context = context;
        this.services = services;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_select_stylist,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final int pos=position;
        holder.serviceName.setText("Stylist Available For "+services.get(position).getName());

        if (services.get(position).getEmployees().size()>0){
            for (int i=0;i<services.get(position).getEmployees().size();i++){
                LayoutInflater inflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View myView = inflater.inflate(R.layout.row_select_emp, null);
                final ImageView imageView=myView.findViewById(R.id.img_employee);
                final TextView empName=myView.findViewById(R.id.txt_emp_name);
                TextView txt_client_served=myView.findViewById(R.id.txt_client_served);
                TextView txt_rating=myView.findViewById(R.id.txt_rating);
                empName.setText(services.get(position).getEmployees().get(i).getName());
                txt_client_served.setText(""+services.get(position).getEmployees().get(i).getClientServed());
                txt_rating.setText(""+services.get(position).getEmployees().get(i).getRating());
                myView.setTag(services.get(position).getEmployees().get(i));
                try{
                    Glide.with(context).load(services.get(position).getEmployees().get(i).getImage()).apply(RequestOptions.circleCropTransform()).into(imageView);
                 /* Glide.with(context).load(services.get(position).getEmployees().get(i).getImage()).asBitmap().centerCrop().into(new BitmapImageViewTarget(imageView) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            imageView.setImageDrawable(circularBitmapDrawable);
                        }
                    });*/
                }catch (Exception e){
                    e.printStackTrace();
                }
                myView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        Employee employee=(Employee)view.getTag();
                        employee.setSelected(true);
                        for (int j=0;j<services.get(pos).getEmployees().size();j++){

                            if (employee.getEmployeeId().equalsIgnoreCase(services.get(pos).getEmployees().get(j).getEmployeeId())){
                                myView.setAlpha(0.5f);
                                empName.setTextColor(getColor(context,R.color.colorPrimary));
                                services.get(pos).getEmployees().get(j).setSelected(true);
                            }else {
                                services.get(pos).getEmployees().get(j).setSelected(false);
                                myView.setAlpha(1f);
                            }
                        }


//                        notifyItemChanged(pos);
                    }
                });

                holder.mContainer.addView(myView);
            }
        }

    }

    @Override
    public int getItemCount() {
        return services.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_service_name) TextView serviceName;
        @BindView(R.id.ll_container_employee) LinearLayout mContainer;


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
