package com.beusalons.android.Fragment.ServiceFragments;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.beusalons.android.Event.NewServicesEvent.EditPackageListEvent;
import com.beusalons.android.Event.NewServicesEvent.PackageListEvent;
import com.beusalons.android.Event.NewServicesEvent.ServiceComboEvent;
import com.beusalons.android.Event.NewServicesEvent.UpgradeEvent_;
import com.beusalons.android.Fragment.DialogFragmentServices;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.UserCart.PackageService;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.Model.newServiceDeals.NewCombo.Data;
import com.beusalons.android.Model.newServiceDeals.NewCombo.NewComboResponse;
import com.beusalons.android.Model.newServiceDeals.NewCombo.Selector;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.ServiceSpecificActivity;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.michael.easydialog.EasyDialog;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by myMachine on 6/17/2017.
 */

public class NewComboBottomSheet extends BottomSheetDialogFragment{

    private String deal_id, parlor_id;
    //    private Slab slab;
    private boolean pop_up;
    private String description="", service_name, short_description="";

    private ViewAnimator view_animator;
    private TextView txt_customize, txt_select, txt_done_apply, txt_save_per, txt_menu_price, txt_price, txt_cancel,
            txt_name_, txt_description_, txt_validity_, txt_discount_;
    private LinearLayout linear_done, linear_, linear_items, linear_price, linear_cancel;        //linear_ and linear_item se khelenge

    private List<PackageService> package_services_list= new ArrayList<>();
    private List<Integer> service_index= new ArrayList<>();
    private List<Integer> brand_index= new ArrayList<>();
    private List<Integer> product_index= new ArrayList<>();

    private List<Boolean> has_brand= new ArrayList<>();
    private List<Boolean> has_product= new ArrayList<>();

    private Data data;
    private AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;

    private int menu_price=0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle= getArguments();
        if(bundle!=null && bundle.containsKey("deal_id") && bundle.containsKey("parlor_id")){

            deal_id= bundle.getString("deal_id");
            parlor_id= bundle.getString("parlor_id");
//            slab = new Gson().fromJson(bundle.getString("slab"), Slab.class);
            pop_up= bundle.getBoolean("pop_up", false);
            description= bundle.getString("description");
            short_description= bundle.getString("short_description");
            service_name= bundle.getString("service_name");
            menu_price= bundle.getInt("menu_price", 0);
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_services_specific_ncombo_bottomsheet, container, false);

        logger = AppEventsLogger.newLogger(view.getContext());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(view.getContext());

        view_animator= (ViewAnimator)view.findViewById(R.id.view_animator);

        linear_= (LinearLayout)view.findViewById(R.id.linear_);
        linear_items= (LinearLayout)view.findViewById(R.id.linear_items);
        linear_done= (LinearLayout)view.findViewById(R.id.linear_done);     //yehi apply bhi hai
        linear_price= (LinearLayout)view.findViewById(R.id.linear_price);
        linear_cancel= (LinearLayout)view.findViewById(R.id.linear_cancel);

        txt_name_= (TextView)view.findViewById(R.id.txt_name_);
        txt_description_= (TextView)view.findViewById(R.id.txt_description_);

        txt_name_.setText(service_name);
        txt_description_.setText(short_description);

        txt_customize= (TextView)view.findViewById(R.id.txt_customize);
        txt_select= (TextView)view.findViewById(R.id.txt_select);

        txt_customize.setText("Customize Your Package");
        txt_select.setText("Please Select");

        txt_menu_price= (TextView)view.findViewById(R.id.txt_menu_price);
        txt_save_per= (TextView)view.findViewById(R.id.txt_save_per);

        txt_done_apply= (TextView)view.findViewById(R.id.txt_done_apply);
        txt_cancel= (TextView)view.findViewById(R.id.txt_cancel);

        txt_validity_= (TextView)view.findViewById(R.id.txt_validity_);
        txt_discount_= (TextView)view.findViewById(R.id.txt_discount_);

        txt_price= (TextView)view.findViewById(R.id.txt_price);     //hahah
        fetchData(deal_id, parlor_id, view);

        return view;
    }

    public void fetchData(String deal_id, String parlor_id, final View view){

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<NewComboResponse> call = apiInterface.getData(deal_id, parlor_id);
        call.enqueue(new Callback<NewComboResponse>() {
            @Override
            public void onResponse(Call<NewComboResponse> call, Response<NewComboResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().getSuccess()){

                        //setting data stuff
                        data= response.body().getData();

                        try{

                            dothejob(data, view);
                        }catch (Exception e){
                            e.printStackTrace();
                        }


                    }else{
                        dismiss();
                        Log.i("newcombo", "i'm in not success");
                    }


                }else{
                    dismiss();
                    Log.i("newcombo", "i'm in retrofit failure");
                }

            }

            @Override
            public void onFailure(Call<NewComboResponse> call, Throwable t) {
                dismiss();
                Log.i("newcombo", "i'm in failure: "+ t.getMessage()+  "  "+ t.getCause()+ " " + t.getStackTrace());
            }
        });

    }

    public void dothejob(final Data data, final View view){

        linear_.removeAllViews();
        for(int i=0;i<data.getSelectors().size();i++){

            final int selector_index= i;

            boolean isService= false, isBrand= false, isProduct= false;

            has_brand.add(false);
            has_product.add(false);

            final PackageService package_service= new PackageService();

            if(data.getSelectors().get(i).getType().equalsIgnoreCase("service") ||
                    data.getSelectors().get(i).getType().equalsIgnoreCase("category")){

//                View view_= LayoutInflater.from(getActivity()).
//                        inflate(R.layout.fragment_services_specific_bottomsheet_package_combo, null, false);
//
//                final TextView txt_service, txt_brand, txt_product, txt_service_name
//                        , txt_price_, txt_menu_price_, txt_save_per_;
//                final LinearLayout linear_service, linear_brand, linear_product, view_service,view_brand,
//                        linear_brand_, linear_product_;

//                linear_service= (LinearLayout)view_.findViewById(R.id.linear_service);
//                linear_brand= (LinearLayout)view_.findViewById(R.id.linear_brand);
//                linear_product= (LinearLayout)view_.findViewById(R.id.linear_product);
//
//                linear_brand_= (LinearLayout)view_.findViewById(R.id.linear_brand_);
//                linear_product_= (LinearLayout)view_.findViewById(R.id.linear_product_);
//
//                txt_service= (TextView)view_.findViewById(R.id.txt_service);
//                txt_brand= (TextView)view_.findViewById(R.id.txt_brand);
//                txt_product= (TextView)view_.findViewById(R.id.txt_product);
//                txt_service_name= (TextView)view_.findViewById(R.id.txt_service_name);
//                view_service=(LinearLayout) view_.findViewById(R.id.view_service);
//                view_brand=(LinearLayout)view_.findViewById(R.id.view_brand);
//
//                txt_price_= (TextView)view_.findViewById(R.id.txt_price);
//                txt_menu_price_= (TextView)view_.findViewById(R.id.txt_menu_price);
//                txt_save_per_= (TextView)view_.findViewById(R.id.txt_save_per);
//
//                txt_service_name.setText(data.getSelectors().get(selector_index).getServiceTitle());

                //service-------------------------------------------
                service_index.add(selector_index, 0);
                brand_index.add(selector_index, 0);
                product_index.add(selector_index, 0);

                data.getSelectors().get(i).getServices().get(0).setCheck(true);
//                txt_service.setText(data.getSelectors().get(selector_index).getServices().
//                        get(0).getName());

                package_service.setService_code(data.getSelectors().get(selector_index)
                        .getServices().get(0).getServiceCode());
                package_service.setService_id(data.getSelectors().get(selector_index).getServices().
                        get(0).getServiceId());
                package_service.setService_name(data.getSelectors().get(selector_index).getServices().
                        get(0).getName());

                Log.i("priceskalocha", "price: "+ data.getSelectors().get(selector_index).getServices().
                        get(0).getPrice()+ "    menu price: " +data.getSelectors().get(selector_index).getServices().
                        get(0).getMenuPrice());


                //whether to show service or not
                if(data.getSelectors().get(i).getServices().size()>1){      //size greater than 1 then show
                    isService= true;
//                    linear_service.setVisibility(View.VISIBLE);
                }else{
                    isService= false;
//                    linear_service.setVisibility(View.GONE);
                }


                if(data.getSelectors().get(i).getServices().get(0).getBrands()==null ||
                        data.getSelectors().get(i).getServices().get(0).getBrands().size()==0 ){

                    isBrand=false;
                    isProduct= false;
//                    linear_brand.setVisibility(View.GONE);
//                    linear_product.setVisibility(View.GONE);
                    package_service.setPrice((int)data.getSelectors().get(selector_index).getServices().
                            get(0).getPrice());
                    package_service.setMenu_price((int)data.getSelectors().get(selector_index).getServices().
                            get(0).getMenuPrice());

                    has_brand.set(selector_index, false);
                    has_product.set(selector_index, false);
                }else{

//                    view_service.setPadding(0,10,195,0);
//                    linear_brand.setVisibility(View.VISIBLE);
                    has_brand.set(selector_index, true);

                    package_service.setBrand_id(data.getSelectors().get(selector_index).getServices().
                            get(0).getBrands().get(0).getBrandId());
                    package_service.setBrand_name(data.getSelectors().get(selector_index).getServices().
                            get(0).getBrands().get(0).getBrandName());

                    data.getSelectors().get(selector_index).getServices().get(0).getBrands().get(0).setCheck(true);
//                    txt_brand.setText(data.getSelectors().get(selector_index).getServices().
//                            get(0).getBrands().get(0).getBrandName());

                    if(data.getSelectors().get(selector_index).getServices().get(0).getBrands().size()>1){

                        isBrand= true;

//                        linear_brand_.setVisibility(View.VISIBLE);
//                        linear_brand.setEnabled(true);
                    }else{

                        isBrand= false;

//                        linear_brand_.setVisibility(View.GONE);
//                        linear_brand.setEnabled(false);
                    }

                    if(data.getSelectors().get(selector_index).getServices().get(0).getBrands().get(0).
                            getProducts()==null ||
                            data.getSelectors().get(selector_index).getServices().get(0).getBrands().get(0).
                                    getProducts().size()==0){

                        isProduct= false;
                        has_product.set(selector_index, false);
//                        linear_product.setVisibility(View.GONE);

                        package_service.setPrice((int)data.getSelectors().get(selector_index).getServices().
                                get(0).getBrands().get(0).getPrice());
                        package_service.setMenu_price((int)data.getSelectors().get(selector_index).getServices().
                                get(0).getBrands().get(0).getMenuPrice());
                    }else{


                        has_product.set(selector_index, true);
//                        linear_product.setVisibility(View.VISIBLE);
//                        view_brand.setPadding(0,10,195,0);
                        data.getSelectors().get(selector_index).getServices().get(0).getBrands().get(0).
                                getProducts().get(0).setCheck(true);
//                        txt_product.setText(data.getSelectors().get(selector_index).getServices().
//                                get(0).getBrands().get(0).getProducts().get(0).getProductName());

                        package_service.setProduct_id(data.getSelectors().get(selector_index).getServices().
                                get(0).getBrands().get(0).getProducts().get(0).getProductId());
                        package_service.setProduct_name(data.getSelectors().get(selector_index).getServices().
                                get(0).getBrands().get(0).getProducts().get(0).getProductName());
                        package_service.setPrice((int)data.getSelectors().get(selector_index).getServices().
                                get(0).getBrands().get(0).getProducts().get(0).getPrice());
                        package_service.setMenu_price((int)data.getSelectors().get(selector_index).getServices().
                                get(0).getBrands().get(0).getProducts().get(0).getMenuPrice());

                        if(data.getSelectors().get(selector_index).getServices()
                                .get(0).getBrands().get(0).getProducts().size()>1){
                            isProduct= true;
//                            linear_product_.setVisibility(View.VISIBLE);
//                            linear_product.setEnabled(true);

                        }else{

                            isProduct= false;
//                            linear_product_.setVisibility(View.GONE);
//                            linear_product.setEnabled(false);
                        }
                    }
                }
                package_services_list.add(package_service);

//                txt_price_.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                        package_service.getPrice()));
//                if(package_service.getPrice()<package_service.getMenu_price()){
//
//                    txt_menu_price_.setVisibility(View.VISIBLE);
//                    txt_save_per_.setVisibility(View.VISIBLE);
//
//                    //with tax
//                    int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                            package_service.getMenu_price());
//
//                    txt_menu_price_.setText(AppConstant.CURRENCY+menu_price_tax);
//                    txt_menu_price_.setPaintFlags(txt_menu_price_.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                    txt_save_per_.setText(AppConstant.SAVE+" "+
//                            (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                    package_service.getPrice())*100)/menu_price_tax))+"%");
//                    txt_save_per_.setBackgroundResource(R.drawable.discount_seletor);
//                }else{
//
//                    txt_menu_price_.setVisibility(View.GONE);
//                    txt_save_per_.setVisibility(View.GONE);
//                }


//                linear_service.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        if (!pop_up){
//                            logPackageBottomSheetChangeEvent();
//                            logPackageBottomSheetChangeFireBaseEvent();
//                        }
//
//
//                        //view animator type hai yeh
//                        view_animator.setInAnimation(getActivity(), R.anim.slide_from_right);
//
//                        txt_customize.setText("Customize Your Service");
//                        txt_select.setText("Please Select Any One Option");
//
//                        txt_done_apply.setText(getResources().getString(R.string.apply));
//
//                        txt_cancel.setText("Cancel");
//                        txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cancel_));
//
//                        linear_cancel.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                view_animator.setInAnimation(getActivity(), R.anim.slide_from_left);
//
//                                txt_customize.setText("Customize Your Package");
//                                txt_select.setText("Please Select");
//
//                                txt_cancel.setText("View Cart");
//                                txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.alter_));
//
//                                cancel();
//
//                                txt_done_apply.setText(getResources().getString(R.string.done));
//                                linear_done.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        addServices(false);
//                                        if (pop_up){
//                                            logEditServicePackageDoneEvent();
//
//                                            logEditServicePackageDoneFirBaseEvent();
//                                        }
//                                        else{
//                                            logPackageBottomSheetDoneFireBaseEvent();
//                                            logPackageBottomSheetDoneEvent();
//                                        }
//                                        dismiss();
//                                    }
//                                });
//
//
//                                txt_service.setText(data.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getName());
//
//                                if(data.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands()!=null &&
//                                        data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().size()>0){
//                                    has_brand.set(selector_index, true);
//
//                                    if(data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(0).getProducts()!=null &&
//                                            data.getSelectors().get(selector_index).getServices()
//                                                    .get(service_index.get(selector_index)).getBrands().get(0).getProducts().size()>0){
//
//                                        has_product.set(selector_index, true);
//                                    }else{
//                                        has_product.set(selector_index, false);
//                                    }
//
//                                }else{
//                                    has_brand.set(selector_index, false);
//                                    has_product.set(selector_index, false);
//                                }
//
//
//                                if(has_brand.get(selector_index)){
//
//                                    //brand ka case handle kar raha hoon-----------------------------------------------
//                                    brand_index.set(selector_index, 0);
//                                    for(int a=0;a<data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().size();a++){
//                                        data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(a).setCheck(false);
//                                    }
//                                    data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(0).setCheck(true);
//                                    txt_brand.setText(data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(0).getBrandName());
//
//                                    package_service.setBrand_name(data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(0).getBrandName());
//                                    package_service.setBrand_id(data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(0).getBrandId());
//                                    package_service.setPrice((int)data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(0).getPrice());
//                                    package_service.setMenu_price((int)data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(0).getMenuPrice());
//
//                                    if(data.getSelectors().get(selector_index)
//                                            .getServices().get(service_index.get(selector_index)).getBrands().size()>1){
//
//                                        linear_brand_.setVisibility(View.VISIBLE);
//                                        linear_brand.setEnabled(true);
//                                    }else{
//
//                                        linear_brand_.setVisibility(View.GONE);
//                                        linear_brand.setEnabled(false);
//                                    }
//
//                                    //product ka case handle kara hai-----------------------------------------
//
//                                    if(has_product.get(selector_index)){
//                                        product_index.set(selector_index, 0);
//                                        for(int b=0;b<data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().size();b++){
//
//                                            data.getSelectors().get(selector_index).getServices()
//                                                    .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().get(b).setCheck(false);
//                                        }
//                                        data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(0).setCheck(true);
//                                        txt_product.setText(data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands()
//                                                .get(brand_index.get(selector_index)).getProducts().get(0).getProductName());
//
//
//                                        package_service.setProduct_name(data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(0).getProductName());
//                                        package_service.setProduct_id(data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(0).getProductId());
//                                        package_service.setPrice((int)data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(0).getPrice());
//                                        package_service.setMenu_price((int)data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(0).getMenuPrice());
//
//
//                                        if(data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands()
//                                                .get(brand_index.get(selector_index)).getProducts().size()>1){
//
//                                            linear_product_.setVisibility(View.VISIBLE);
//                                            linear_product.setEnabled(true);
//
//                                        }else{
//
//                                            linear_product_.setVisibility(View.GONE);
//                                            linear_product.setEnabled(false);
//                                        }
//
//                                    }
//                                    package_services_list.set(selector_index, package_service);
//                                }
//
//                                updatePrice(view);
//
//                                txt_price_.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                        package_service.getPrice()));
//                                if(package_service.getPrice()<package_service.getMenu_price()){
//
//                                    txt_menu_price_.setVisibility(View.VISIBLE);
//                                    txt_save_per_.setVisibility(View.VISIBLE);
//
//                                    //with tax
//                                    int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                            package_service.getMenu_price());
//
//                                    txt_menu_price_.setText(AppConstant.CURRENCY+menu_price_tax);
//                                    txt_menu_price_.setPaintFlags(txt_menu_price_.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                                    txt_save_per_.setText(AppConstant.SAVE+" "+
//                                            (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                    package_service.getPrice())*100)/menu_price_tax))+"%");
//                                    txt_save_per_.setBackgroundResource(R.drawable.discount_seletor);
//                                }else{
//
//                                    txt_menu_price_.setVisibility(View.GONE);
//                                    txt_save_per_.setVisibility(View.GONE);
//                                }
//
//                                view_animator.showPrevious();
//                            }
//                        });
//
//                        linear_done.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                view_animator.setInAnimation(getActivity(), R.anim.slide_from_left);
//
//                                txt_customize.setText("Customize Your Package");
//                                txt_select.setText("Please Select");
//
//                                txt_done_apply.setText(getResources().getString(R.string.done));
//
//                                txt_service.setText(data.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getName());
//
//                                linear_done.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        if (pop_up){
//                                            logEditServicePackageDoneEvent();
//                                            logEditServicePackageDoneFirBaseEvent();
//                                        }
//                                        else{
//                                            logPackageBottomSheetDoneEvent();
//                                            logPackageBottomSheetDoneFireBaseEvent();
//                                        }
//                                        addServices(false);
//                                        dismiss();
//                                    }
//                                });
//
//                                txt_cancel.setText("View Cart");
//                                txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.alter_));
//
//                                cancel();   //cancel click
//
//                                if(data.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands()!=null &&
//                                        data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().size()>0){
//                                    has_brand.set(selector_index, true);
//
//                                    if(data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(0).getProducts()!=null &&
//                                            data.getSelectors().get(selector_index).getServices()
//                                                    .get(service_index.get(selector_index)).getBrands().get(0).getProducts().size()>0){
//
//                                        has_product.set(selector_index, true);
//                                    }else{
//                                        has_product.set(selector_index, false);
//                                    }
//
//                                }else{
//                                    has_brand.set(selector_index, false);
//                                    has_product.set(selector_index, false);
//                                }
//
//
//                                if(has_brand.get(selector_index)){
//
//                                    //brand ka case handle kar raha hoon-----------------------------------------------
//                                    brand_index.set(selector_index, 0);
//                                    for(int a=0;a<data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().size();a++){
//                                        data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(a).setCheck(false);
//                                    }
//                                    data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(0).setCheck(true);
//                                    txt_brand.setText(data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(0).getBrandName());
//
//                                    package_service.setBrand_name(data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(0).getBrandName());
//                                    package_service.setBrand_id(data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(0).getBrandId());
//                                    package_service.setPrice((int)data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(0).getPrice());
//                                    package_service.setMenu_price((int)data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(0).getMenuPrice());
//
//                                    if(data.getSelectors().get(selector_index)
//                                            .getServices().get(service_index.get(selector_index)).getBrands().size()>1){
//
//                                        linear_brand_.setVisibility(View.VISIBLE);
//                                        linear_brand.setEnabled(true);
//                                    }else{
//
//                                        linear_brand_.setVisibility(View.GONE);
//                                        linear_brand.setEnabled(false);
//                                    }
//
//                                    //product ka case handle kara hai-----------------------------------------
//
//                                    if(has_product.get(selector_index)){
//                                        product_index.set(selector_index, 0);
//                                        for(int b=0;b<data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().size();b++){
//
//                                            data.getSelectors().get(selector_index).getServices()
//                                                    .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().get(b).setCheck(false);
//                                        }
//                                        data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(0).setCheck(true);
//                                        txt_product.setText(data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands()
//                                                .get(brand_index.get(selector_index)).getProducts().get(0).getProductName());
//
//
//                                        package_service.setProduct_name(data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(0).getProductName());
//                                        package_service.setProduct_id(data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(0).getProductId());
//                                        package_service.setPrice((int)data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(0).getPrice());
//                                        package_service.setMenu_price((int)data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(0).getMenuPrice());
//
//
//                                        if(data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands()
//                                                .get(brand_index.get(selector_index)).getProducts().size()>1){
//
//                                            linear_product_.setVisibility(View.VISIBLE);
//                                            linear_product.setEnabled(true);
//
//                                        }else{
//
//                                            linear_product_.setVisibility(View.GONE);
//                                            linear_product.setEnabled(false);
//                                        }
//
//                                    }
//                                    package_services_list.set(selector_index, package_service);
//                                }
//
//                                updatePrice(view);
//
//
//                                txt_price_.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                        package_service.getPrice()));
//                                if(package_service.getPrice()<package_service.getMenu_price()){
//
//                                    txt_menu_price_.setVisibility(View.VISIBLE);
//                                    txt_save_per_.setVisibility(View.VISIBLE);
//
//                                    //with tax
//                                    int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                            package_service.getMenu_price());
//
//                                    txt_menu_price_.setText(AppConstant.CURRENCY+menu_price_tax );
//                                    txt_menu_price_.setPaintFlags(txt_menu_price_.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                                    txt_save_per_.setText(AppConstant.SAVE+" "+
//                                            (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                    package_service.getPrice())*100)/menu_price_tax))+"%");
//                                    txt_save_per_.setBackgroundResource(R.drawable.discount_seletor);
//                                }else{
//
//                                    txt_menu_price_.setVisibility(View.GONE);
//                                    txt_save_per_.setVisibility(View.GONE);
//                                }
//
//                                view_animator.showPrevious();
//                            }
//
//                        });
//
//                        linear_items.removeAllViews();                  //removing the views first...no duplication
//                        LinearLayout customize_= (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.edit_service, null);
//                        TextView txt_customize_= (TextView)customize_.findViewById(R.id.txt_customize_);
//                        txt_customize_.setText("Service");
//                        linear_items.addView(customize_);
//
//
//                        final List<RadioButton> list_radio_= new ArrayList<>();
//
//                        for(int j=0;j<data.getSelectors().get(selector_index).getServices().size();j++){
//
//                            final int index= j;
//                            View view_= LayoutInflater.from(getActivity()).
//                                    inflate(R.layout.bottomsheet_items, null, false);
//
//                            LinearLayout linear_click= (LinearLayout)view_.findViewById(R.id.linear_click);
//                            TextView txt_name= (TextView)view_.findViewById(R.id.txt_name);
//                            TextView txt_price= (TextView)view_.findViewById(R.id.txt_price);
//                            TextView txt_menu_price= (TextView)view_.findViewById(R.id.txt_menu_price);
//                            TextView txt_save_per= (TextView)view_.findViewById(R.id.txt_save_per);
//
//                            txt_name.setText(data.getSelectors().get(selector_index).getServices().get(j).getName());
//                            txt_price.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                    data.getSelectors().get(selector_index)
//                                            .getServices().get(j).getPrice()));
//
//                            if(data.getSelectors().get(selector_index).getServices().get(j).getMenuPrice()>
//                                    data.getSelectors().get(selector_index).getServices().get(j).getPrice()){
//
//                                txt_menu_price.setVisibility(View.VISIBLE);
//                                txt_save_per.setVisibility(View.VISIBLE);
//
//                                int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                        data.getSelectors().get(selector_index)
//                                                .getServices().get(j).getMenuPrice());
//
//                                txt_menu_price.setText(AppConstant.CURRENCY+menu_price_tax);
//                                txt_menu_price.setPaintFlags(txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                                txt_save_per.setText(AppConstant.SAVE+" "+
//                                        (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                data.getSelectors().get(selector_index)
//                                                        .getServices().get(j).getPrice())*100)/
//                                                menu_price_tax))+"%");
//                                txt_save_per.setBackgroundResource(R.drawable.discount_seletor);
//
//                            }else{
//                                txt_menu_price.setVisibility(View.GONE);
//                                txt_save_per.setVisibility(View.GONE);
//                            }
//
//                            final RadioButton radio_= (RadioButton)view_.findViewById(R.id.radio_);
//                            radio_.setClickable(false);
//                            list_radio_.add(radio_);            //adding the radio button in radio list
//
//                            if(data.getSelectors().get(selector_index).getServices().get(j).isCheck()){
//
//                                radio_.setChecked(true);
//                                list_radio_.get(j).setChecked(true);
//                            }else{
//
//                                radio_.setChecked(false);
//                                list_radio_.get(j).setChecked(false);
//                            }
//
//
//                            linear_click.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//
//                                    if(list_radio_.get(index).isChecked()){
//
//                                        list_radio_.get(index).setChecked(false);
//                                    }else{
//
//                                        list_radio_.get(index).setChecked(true);
//                                    }
//
//                                    //updating view
//                                    int list_radio_size=0;
//                                    for(int k=0;k<list_radio_.size();k++){
//
//                                        if(k==index && list_radio_.get(index).isChecked()){     //conditions yaha likh
//
//                                            service_index.set(selector_index, index);
//
//                                            data.getSelectors().get(selector_index).getServices().get(index).setCheck(true);
//
//                                            package_service.setService_name(data.getSelectors().get(selector_index).getServices().
//                                                    get(index).getName());
//                                            package_service.setService_id(data.getSelectors().get(selector_index).getServices().
//                                                    get(index).getServiceId());
//                                            package_service.setService_code(data.getSelectors()
//                                                    .get(selector_index).getServices().get(index).getServiceCode());
//                                            if(!has_brand.get(selector_index)){
//                                                package_service.setPrice((int)data.getSelectors()
//                                                        .get(selector_index).getServices().get(index).getPrice());
//                                                package_service.setMenu_price((int)data.getSelectors()
//                                                        .get(selector_index).getServices().get(index).getMenuPrice());
//                                            }
//                                            package_services_list.set( selector_index,package_service);
//
//                                            updatePrice(view);
//
//                                        }else{              //baki sare un check
//
//                                            list_radio_size++;
//                                            list_radio_.get(k).setChecked(false);
//                                            data.getSelectors().get(selector_index).getServices().get(k).setCheck(false);
//                                        }
//                                    }
//
//                                    //agar koi bhi select nai kara toh current wale ko select karao
//                                    if(list_radio_size==list_radio_.size()){                            //aur conditions yaha likh
//
//                                        service_index.set(selector_index, index);
//
//                                        //json types saving to cart
//                                        package_service.setService_name(data.getSelectors().get(selector_index).getServices().
//                                                get(index).getName());
//                                        package_service.setService_id(data.getSelectors().get(selector_index).getServices().
//                                                get(index).getServiceId());
//                                        package_service.setService_code(data.getSelectors()
//                                                .get(selector_index).getServices().get(index).getServiceCode());
//                                        if(!has_brand.get(selector_index)){
//                                            package_service.setPrice((int)data.getSelectors()
//                                                    .get(selector_index).getServices().get(index).getPrice());
//                                            package_service.setMenu_price((int)data.getSelectors()
//                                                    .get(selector_index).getServices().get(index).getMenuPrice());
//                                        }
//                                        package_services_list.set( selector_index,package_service);
//
//                                        data.getSelectors().get(selector_index).getServices().get(index).setCheck(true);
//                                        list_radio_.get(index).setChecked(true);
//                                    }
//                                }
//                            });
//
//                            linear_items.addView(view_);
//                        }
//
//                        view_animator.showNext();
//                    }
//                });
//
//                linear_brand.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//
//                        if (!pop_up){
//                            logPackageBottomSheetChangeEvent();
//                            logPackageBottomSheetChangeFireBaseEvent();
//                        }
//                        //view animator type hai yeh
//                        view_animator.setInAnimation(getActivity(), R.anim.slide_from_right);
//
//                        txt_customize.setText("Customize Your Service");
//                        txt_select.setText("Please Select Any One Option");
//
//                        txt_done_apply.setText(getResources().getString(R.string.apply));
//
//                        txt_cancel.setText("Cancel");
//                        txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cancel_));
//
//                        linear_cancel.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                view_animator.setInAnimation(getActivity(), R.anim.slide_from_left);
//
//
//                                txt_customize.setText("Customize Your Package");
//                                txt_select.setText("Please Select");
//
//                                txt_cancel.setText("View Cart");
//                                txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.alter_));
//
//                                cancel();
//
//                                txt_done_apply.setText(getResources().getString(R.string.done));
//                                linear_done.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//
//                                        if (pop_up){
//                                            logEditServicePackageDoneEvent();
//                                            logEditServicePackageDoneFirBaseEvent();
//                                        }
//                                        else{
//                                            logPackageBottomSheetDoneEvent();
//                                            logPackageBottomSheetDoneFireBaseEvent();
//                                        }
//
//                                        addServices(false);
//                                        dismiss();
//                                    }
//                                });
//
//
//                                txt_brand.setText(data.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getBrandName());
//
//                                if(data.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().
//                                                get(brand_index.get(selector_index)).getProducts()!=null &&
//                                        data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).
//                                                getBrands().get(brand_index.get(selector_index)).getProducts().size()>0){
//
//                                    has_product.set(selector_index, true);
//                                }else{
//                                    has_product.set(selector_index, false);
//                                }
//
//
//                                if(has_product.get(selector_index)) {
//                                    //product ka case handle kara hai
//                                    product_index.set(selector_index, 0);
//                                    for (int b = 0; b < data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().size(); b++) {
//
//                                        data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().get(b).setCheck(false);
//                                    }
//                                    data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().get(0).setCheck(true);
//                                    txt_product.setText(data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands()
//                                            .get(brand_index.get(selector_index)).getProducts().get(0).getProductName());
//
//                                    package_service.setProduct_name(data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(0).getProductName());
//                                    package_service.setProduct_id(data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(0).getProductId());
//                                    package_service.setPrice((int)data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(0).getPrice());
//                                    package_service.setMenu_price((int)data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(0).getMenuPrice());
//
//                                    package_services_list.set(selector_index, package_service);
//
//                                    if(data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands()
//                                            .get(brand_index.get(selector_index)).getProducts().size()>1){
//
//                                        linear_product_.setVisibility(View.VISIBLE);
//                                        linear_product.setEnabled(true);
//                                    }else{
//
//                                        linear_product_.setVisibility(View.GONE);
//                                        linear_product.setEnabled(false);
//                                    }
//
//                                }
//
//                                updatePrice(view);
//
//                                txt_price_.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                        package_service.getPrice()));
//                                if(package_service.getPrice()<package_service.getMenu_price()){
//
//                                    txt_menu_price_.setVisibility(View.VISIBLE);
//                                    txt_save_per_.setVisibility(View.VISIBLE);
//
//                                    int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                            package_service.getMenu_price());
//
//                                    txt_menu_price_.setText(AppConstant.CURRENCY+menu_price_tax);
//                                    txt_menu_price_.setPaintFlags(txt_menu_price_.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                                    txt_save_per_.setText(AppConstant.SAVE+" "+
//                                            (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                    package_service.getPrice())*100)/menu_price_tax))+"%");
//                                    txt_save_per_.setBackgroundResource(R.drawable.discount_seletor);
//                                }else{
//
//                                    txt_menu_price_.setVisibility(View.GONE);
//                                    txt_save_per_.setVisibility(View.GONE);
//                                }
//                                view_animator.showPrevious();
//                            }
//                        });
//
//
//                        linear_done.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//
//                                view_animator.setInAnimation(getActivity(), R.anim.slide_from_left);
//
//                                txt_customize.setText("Customize Your Package");
//                                txt_select.setText("Please Select");
//
//                                txt_done_apply.setText(getResources().getString(R.string.done));
//
//                                txt_brand.setText(data.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getBrandName());
//
//                                linear_done.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        if (pop_up){
//                                            logEditServicePackageDoneEvent();
//                                            logEditServicePackageDoneFirBaseEvent();
//                                        }
//                                        else{
//                                            logPackageBottomSheetDoneEvent();
//                                            logPackageBottomSheetDoneFireBaseEvent();
//                                        }
//                                        addServices(false);
//                                        dismiss();
//                                    }
//                                });
//
//                                txt_cancel.setText("View Cart");
//                                txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.alter_));
//
//                                cancel();
//
//                                if(data.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().
//                                                get(brand_index.get(selector_index)).getProducts()!=null &&
//                                        data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).
//                                                getBrands().get(brand_index.get(selector_index)).getProducts().size()>0){
//
//                                    has_product.set(selector_index, true);
//                                }else{
//                                    has_product.set(selector_index, false);
//                                }
//
//
//                                if(has_product.get(selector_index)) {
//                                    //product ka case handle kara hai
//                                    product_index.set(selector_index, 0);
//                                    for (int b = 0; b < data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().size(); b++) {
//
//                                        data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().get(b).setCheck(false);
//                                    }
//                                    data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index)).getProducts().get(0).setCheck(true);
//                                    txt_product.setText(data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands()
//                                            .get(brand_index.get(selector_index)).getProducts().get(0).getProductName());
//
//                                    package_service.setProduct_name(data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(0).getProductName());
//                                    package_service.setProduct_id(data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(0).getProductId());
//                                    package_service.setPrice((int)data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(0).getPrice());
//                                    package_service.setMenu_price((int)data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(0).getMenuPrice());
//
//                                    package_services_list.set(selector_index, package_service);
//
//                                    if(data.getSelectors().get(selector_index).getServices()
//                                            .get(service_index.get(selector_index)).getBrands()
//                                            .get(brand_index.get(selector_index)).getProducts().size()>1){
//
//                                        linear_product_.setVisibility(View.VISIBLE);
//                                        linear_product.setEnabled(true);
//                                    }else{
//
//                                        linear_product_.setVisibility(View.GONE);
//                                        linear_product.setEnabled(false);
//                                    }
//
//                                }
//
//                                updatePrice(view);
//
//                                txt_price_.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                        package_service.getPrice()));
//                                if(package_service.getPrice()<package_service.getMenu_price()){
//
//                                    txt_menu_price_.setVisibility(View.VISIBLE);
//                                    txt_save_per_.setVisibility(View.VISIBLE);
//
//                                    int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                            package_service.getMenu_price());
//
//                                    txt_menu_price_.setText(AppConstant.CURRENCY+menu_price_tax);
//                                    txt_menu_price_.setPaintFlags(txt_menu_price_.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                                    txt_save_per_.setText(AppConstant.SAVE+" "+
//                                            (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                    package_service.getPrice())*100)/menu_price_tax))+"%");
//                                    txt_save_per_.setBackgroundResource(R.drawable.discount_seletor);
//                                }else{
//
//                                    txt_menu_price_.setVisibility(View.GONE);
//                                    txt_save_per_.setVisibility(View.GONE);
//                                }
//
//                                view_animator.showPrevious();
//                            }
//                        });
//
//                        linear_items.removeAllViews();                  //removing the views first...no duplication
//
//                        LinearLayout customize_= (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.edit_service, null);
//                        TextView txt_customize_= (TextView)customize_.findViewById(R.id.txt_customize_);
//                        txt_customize_.setText("Brand");
//                        linear_items.addView(customize_);
//
//                        final List<RadioButton> list_radio_= new ArrayList<>();
//
//                        for(int j=0;j<data.getSelectors().get(selector_index).getServices().
//                                get(service_index.get(selector_index)).getBrands().size();j++){
//
//                            final int index= j;
//
//                            View view_= LayoutInflater.from(getActivity()).
//                                    inflate(R.layout.bottomsheet_items, null, false);
//
//                            LinearLayout linear_click= (LinearLayout)view_.findViewById(R.id.linear_click);
//                            TextView txt_name= (TextView)view_.findViewById(R.id.txt_name);
//                            TextView txt_price= (TextView)view_.findViewById(R.id.txt_price);
//                            TextView txt_menu_price= (TextView)view_.findViewById(R.id.txt_menu_price);
//                            TextView txt_save_per= (TextView)view_.findViewById(R.id.txt_save_per);
//
//                            txt_name.setText(data.getSelectors().get(selector_index).getServices().
//                                    get(service_index.get(selector_index)).getBrands().get(j).getBrandName());
//                            txt_price.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                    data.getSelectors().get(selector_index).getServices().
//                                            get(service_index.get(selector_index)).getBrands().get(j).getPrice()));
//
//
//                            if(data.getSelectors().get(selector_index).getServices().
//                                    get(service_index.get(selector_index)).getBrands().get(j).getMenuPrice()>
//                                    data.getSelectors().get(selector_index).getServices().
//                                            get(service_index.get(selector_index)).getBrands().get(j).getPrice()){
//
//                                txt_menu_price.setVisibility(View.VISIBLE);
//                                txt_save_per.setVisibility(View.VISIBLE);
//
//                                int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                        data.getSelectors().get(selector_index).getServices().
//                                                get(service_index.get(selector_index)).getBrands().get(j).getMenuPrice());
//
//                                txt_menu_price.setText(AppConstant.CURRENCY+menu_price_tax);
//                                txt_menu_price.setPaintFlags(txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                                txt_save_per.setText(AppConstant.SAVE+" "+
//                                        (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                data.getSelectors().get(selector_index).getServices().
//                                                        get(service_index.get(selector_index)).getBrands().get(j).getPrice())*100)/
//                                                menu_price_tax))+"%");
//                                txt_save_per.setBackgroundResource(R.drawable.discount_seletor);
//
//                            }else{
//
//                                txt_menu_price.setVisibility(View.GONE);
//                                txt_save_per.setVisibility(View.GONE);
//                            }
//
//
//                            final RadioButton radio_= (RadioButton)view_.findViewById(R.id.radio_);
//                            radio_.setClickable(false);
//                            list_radio_.add(radio_);            //adding the radio button in radio list
//
//                            if(data.getSelectors().get(selector_index).getServices().get(service_index.get(selector_index))
//                                    .getBrands().get(j).isCheck()){
//
//                                radio_.setChecked(true);
//                                list_radio_.get(j).setChecked(true);
//                            }else{
//
//                                radio_.setChecked(false);
//                                list_radio_.get(j).setChecked(false);
//                            }
//
//                            linear_click.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//
//                                    if(list_radio_.get(index).isChecked()){
//
//                                        list_radio_.get(index).setChecked(false);
//                                    }else{
//
//                                        list_radio_.get(index).setChecked(true);
//                                    }
//
//                                    //updating view
//                                    int list_radio_size=0;
//                                    for(int k=0;k<list_radio_.size();k++){
//
//                                        if(k==index && list_radio_.get(index).isChecked()){     //conditions yaha likh
//
//                                            brand_index.set(selector_index, index);
//                                            data.getSelectors().get(selector_index).getServices().
//                                                    get(service_index.get(selector_index)).getBrands().get(index).setCheck(true);
//
//                                            package_service.setBrand_id(data.getSelectors().get(selector_index).getServices().
//                                                    get(service_index.get(selector_index)).getBrands().get(index).getBrandId());
//                                            package_service.setBrand_name(data.getSelectors().get(selector_index).getServices().
//                                                    get(service_index.get(selector_index)).getBrands().get(index).getBrandName());
//                                            if(!has_product.get(selector_index)){
//                                                package_service.setPrice((int)data.getSelectors().get(selector_index).getServices().
//                                                        get(service_index.get(selector_index)).getBrands().get(index).getPrice());
//                                                package_service.setMenu_price((int)data.getSelectors().get(selector_index).getServices().
//                                                        get(service_index.get(selector_index)).getBrands().get(index).getMenuPrice());
//                                            }
//                                            package_services_list.set(selector_index, package_service);
//
//                                            updatePrice(view);
//                                        }else{              //baki sare un check
//
//                                            list_radio_size++;
//                                            list_radio_.get(k).setChecked(false);
//                                            data.getSelectors().get(selector_index).getServices().
//                                                    get(service_index.get(selector_index)).getBrands().get(k).setCheck(false);
//                                        }
//                                    }
//
//                                    //agar koi bhi select nai kara toh current wale ko select karao
//                                    if(list_radio_size==list_radio_.size()){                            //aur conditions yaha likh
//
//                                        data.getSelectors().get(selector_index).getServices().
//                                                get(service_index.get(selector_index)).getBrands().get(index).setCheck(true);
//                                        list_radio_.get(index).setChecked(true);
//                                        brand_index.set(selector_index, index);
//
//                                        package_service.setBrand_id(data.getSelectors().get(selector_index).getServices().
//                                                get(service_index.get(selector_index)).getBrands().get(index).getBrandId());
//                                        package_service.setBrand_name(data.getSelectors().get(selector_index).getServices().
//                                                get(service_index.get(selector_index)).getBrands().get(index).getBrandName());
//                                        if(!has_product.get(selector_index)){
//                                            package_service.setPrice((int)data.getSelectors().get(selector_index).getServices().
//                                                    get(service_index.get(selector_index)).getBrands().get(index).getPrice());
//                                            package_service.setMenu_price((int)data.getSelectors().get(selector_index).getServices().
//                                                    get(service_index.get(selector_index)).getBrands().get(index).getMenuPrice());
//                                        }
//                                        package_services_list.set(selector_index, package_service);
//
//                                    }
//                                }
//                            });
//
//                            linear_items.addView(view_);
//                        }
//
//                        view_animator.showNext();
//                    }
//                });
//
//                //products ka click hai yeh
//                linear_product.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//
//                        if (!pop_up){
//                            logPackageBottomSheetChangeEvent();
//                            logPackageBottomSheetChangeFireBaseEvent();
//                        }
//                        view_animator.setInAnimation(getActivity(), R.anim.slide_from_right);
//
//                        txt_customize.setText("Customize Your Service");
//                        txt_select.setText("Please Select Any One Option");
//
//
//                        txt_done_apply.setText(getResources().getString(R.string.apply));
//
//                        txt_cancel.setText("Cancel");
//                        txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cancel_));
//
//                        linear_cancel.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                view_animator.setInAnimation(getActivity(), R.anim.slide_from_left);
//
//
//                                txt_cancel.setText("View Cart");
//                                txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.alter_));
//
//                                cancel();
//
//                                txt_customize.setText("Customize Your Package");
//                                txt_select.setText("Please Select");
//
//                                txt_done_apply.setText(getResources().getString(R.string.done));
//                                linear_done.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//
//                                        if (pop_up){
//                                            logEditServicePackageDoneEvent();
//                                            logEditServicePackageDoneFirBaseEvent();
//                                        }
//                                        else{
//
//                                            logPackageBottomSheetDoneEvent();
//                                            logPackageBottomSheetDoneFireBaseEvent();
//                                        }
//                                        addServices(false);
//                                        dismiss();
//                                    }
//                                });
//
//                                txt_product.setText(data.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getProductName());
//
//                                updatePrice(view);
//
//                                txt_price_.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                        package_service.getPrice()));
//                                if(package_service.getPrice()<package_service.getMenu_price()){
//
//                                    txt_menu_price_.setVisibility(View.VISIBLE);
//                                    txt_save_per_.setVisibility(View.VISIBLE);
//
//                                    int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                            package_service.getMenu_price());
//
//                                    txt_menu_price_.setText(AppConstant.CURRENCY+menu_price_tax);
//                                    txt_menu_price_.setPaintFlags(txt_menu_price_.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                                    txt_save_per_.setText(AppConstant.SAVE+" "+
//                                            (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                    package_service.getPrice())*100)/menu_price_tax))+"%");
//                                    txt_save_per_.setBackgroundResource(R.drawable.discount_seletor);
//                                }else{
//
//                                    txt_menu_price_.setVisibility(View.GONE);
//                                    txt_save_per_.setVisibility(View.GONE);
//                                }
//                                view_animator.showPrevious();
//                            }
//                        });
//
//                        linear_done.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                view_animator.setInAnimation(getActivity(), R.anim.slide_from_left);
//
//                                txt_customize.setText("Customize Your Package");
//                                txt_select.setText("Please Select");
//
//                                txt_done_apply.setText(getResources().getString(R.string.done));
//
//                                txt_product.setText(data.getSelectors().get(selector_index).getServices()
//                                        .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getProductName());
//
//                                linear_done.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        if (pop_up){
//                                            logEditServicePackageDoneEvent();
//                                            logEditServicePackageDoneFirBaseEvent();
//                                        }
//                                        else{
//                                            logPackageBottomSheetDoneEvent();
//                                            logPackageBottomSheetDoneFireBaseEvent();
//                                        }
//                                        addServices(false);
//                                        dismiss();
//                                    }
//                                });
//
//                                txt_cancel.setText("View Cart");
//                                txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.alter_));
//
//                                cancel();
//
//                                updatePrice(view);
//
//                                txt_price_.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                        package_service.getPrice()));
//                                if(package_service.getPrice()<package_service.getMenu_price()){
//
//                                    txt_menu_price_.setVisibility(View.VISIBLE);
//                                    txt_save_per_.setVisibility(View.VISIBLE);
//
//                                    int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                            package_service.getMenu_price());
//
//                                    txt_menu_price_.setText(AppConstant.CURRENCY+menu_price_tax);
//                                    txt_menu_price_.setPaintFlags(txt_menu_price_.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                                    txt_save_per_.setText(AppConstant.SAVE+" "+
//                                            (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                    package_service.getPrice())*100)/menu_price_tax))+"%");
//                                    txt_save_per_.setBackgroundResource(R.drawable.discount_seletor);
//                                }else{
//
//                                    txt_menu_price_.setVisibility(View.GONE);
//                                    txt_save_per_.setVisibility(View.GONE);
//                                }
//
//                                view_animator.showPrevious();
//                            }
//                        });
//
//                        linear_items.removeAllViews();                  //removing the views first...no duplication
//
//                        LinearLayout customize_= (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.edit_service, null);
//                        TextView txt_customize_= (TextView)customize_.findViewById(R.id.txt_customize_);
//                        txt_customize_.setText("Product");
//                        linear_items.addView(customize_);
//
//                        final List<RadioButton> list_radio_= new ArrayList<>();
//
//                        for(int j=0;j<data.getSelectors().get(selector_index).getServices().
//                                get(service_index.get(selector_index)).getBrands()
//                                .get(brand_index.get(selector_index)).getProducts().size();j++){
//                            final int index= j;
//
//                            View view_= LayoutInflater.from(getActivity()).
//                                    inflate(R.layout.bottomsheet_items, null, false);
//
//                            LinearLayout linear_click= (LinearLayout)view_.findViewById(R.id.linear_click);
//                            TextView txt_name= (TextView)view_.findViewById(R.id.txt_name);
//                            TextView txt_price= (TextView)view_.findViewById(R.id.txt_price);
//                            TextView txt_menu_price= (TextView)view_.findViewById(R.id.txt_menu_price);
//                            TextView txt_save_per= (TextView)view_.findViewById(R.id.txt_save_per);
//
//                            txt_name.setText(data.getSelectors().get(selector_index).getServices().
//                                    get(service_index.get(selector_index)).
//                                    getBrands().get(brand_index.get(selector_index)).getProducts().get(index).getProductName());
//                            txt_price.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                    data.getSelectors().get(selector_index).getServices().
//                                            get(service_index.get(selector_index)).
//                                            getBrands().get(brand_index.get(selector_index)).getProducts().get(index).getPrice()));
//
//                            if(data.getSelectors().get(selector_index).getServices().
//                                    get(service_index.get(selector_index)).
//                                    getBrands().get(brand_index.get(selector_index)).getProducts().get(index).getMenuPrice()>
//                                    data.getSelectors().get(selector_index).getServices().
//                                            get(service_index.get(selector_index)).
//                                            getBrands().get(brand_index.get(selector_index)).getProducts().get(index).getPrice()){
//
//                                txt_menu_price.setVisibility(View.VISIBLE);
//                                txt_save_per.setVisibility(View.VISIBLE);
//
//                                int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                        data.getSelectors().get(selector_index).getServices().
//                                                get(service_index.get(selector_index)).
//                                                getBrands().get(brand_index.get(selector_index)).getProducts().get(index).getMenuPrice());
//
//                                txt_menu_price.setText(AppConstant.CURRENCY+menu_price_tax);
//                                txt_menu_price.setTextColor(Color.parseColor("#a7a9ac"));
//                                txt_menu_price.setPaintFlags(txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                                txt_save_per.setText(AppConstant.SAVE+" "+
//                                        (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                data.getSelectors().get(selector_index).getServices().
//                                                        get(service_index.get(selector_index)).
//                                                        getBrands().get(brand_index.get(selector_index)).getProducts().get(index).getPrice())*100)/
//                                                menu_price_tax))+"%");
//                                txt_save_per.setBackgroundResource(R.drawable.discount_seletor);
//
//                            }else{
//
//                                txt_menu_price.setVisibility(View.GONE);
//                                txt_save_per.setVisibility(View.GONE);
//                            }
//
//
//                            final RadioButton radio_= (RadioButton)view_.findViewById(R.id.radio_);
//                            radio_.setClickable(false);
//                            list_radio_.add(radio_);            //adding the radio button in radio list
//
//                            if(data.getSelectors().get(selector_index).getServices().get(service_index.get(selector_index))
//                                    .getBrands().get(brand_index.get(selector_index)).getProducts().get(j).isCheck()){
//
//                                radio_.setChecked(true);
//                                list_radio_.get(j).setChecked(true);
//                            }else{
//
//                                radio_.setChecked(false);
//                                list_radio_.get(j).setChecked(false);
//                            }
//
//                            linear_click.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//
//                                    if(list_radio_.get(index).isChecked()){
//
//                                        list_radio_.get(index).setChecked(false);
//                                    }else{
//
//                                        list_radio_.get(index).setChecked(true);
//                                    }
//
//                                    //updating view
//                                    int list_radio_size=0;
//                                    for(int k=0;k<list_radio_.size();k++){
//
//                                        if(k==index && list_radio_.get(index).isChecked()){     //conditions yaha likh
//
//                                            product_index.set(selector_index, index);
//
//                                            package_service.setProduct_name(data.getSelectors().get(selector_index).getServices()
//                                                    .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                    .getProducts().get(index).getProductName());
//                                            package_service.setProduct_id(data.getSelectors().get(selector_index).getServices()
//                                                    .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                    .getProducts().get(index).getProductId());
//                                            package_service.setPrice((int)data.getSelectors().get(selector_index).getServices()
//                                                    .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                    .getProducts().get(index).getPrice());
//                                            package_service.setMenu_price((int)data.getSelectors().get(selector_index).getServices()
//                                                    .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                    .getProducts().get(index).getMenuPrice());
//
//                                            package_services_list.set(selector_index, package_service);
//
//                                            data.getSelectors().get(selector_index).getServices().get(service_index.get(selector_index))
//                                                    .getBrands().get(brand_index.get(selector_index)).getProducts().get(index).setCheck(true);
//
//                                            updatePrice(view);
//
//                                        }else{              //baki sare un check
//
//                                            list_radio_size++;
//                                            list_radio_.get(k).setChecked(false);
//                                            data.getSelectors().get(selector_index).getServices().get(service_index.get(selector_index))
//                                                    .getBrands().get(brand_index.get(selector_index)).getProducts().get(k).setCheck(false);
//                                        }
//                                    }
//
//                                    //agar koi bhi select nai kara toh current wale ko select karao
//                                    if(list_radio_size==list_radio_.size()){                            //aur conditions yaha likh
//
//                                        product_index.set(selector_index, index);
//
//                                        package_service.setProduct_name(data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(index).getProductName());
//                                        package_service.setProduct_id(data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(index).getProductId());
//                                        package_service.setPrice((int)data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(index).getPrice());
//                                        package_service.setMenu_price((int)data.getSelectors().get(selector_index).getServices()
//                                                .get(service_index.get(selector_index)).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(index).getMenuPrice());
//
//                                        package_services_list.set(selector_index, package_service);
//
//                                        data.getSelectors().get(selector_index).getServices().get(service_index.get(selector_index))
//                                                .getBrands().get(brand_index.get(selector_index)).getProducts().get(index).setCheck(true);
//                                        list_radio_.get(index).setChecked(true);
//                                    }
//                                }
//                            });
//
//                            linear_items.addView(view_);
//                        }
//
//                        view_animator.showNext();
//                    }
//                });

                Log.i("booleananother","values:" + isService+ isBrand+isProduct);
//                if(!data.getSelectors().get(i).isShowToUser() ||
//                        (!isService && !isBrand && !isProduct)){

//                LinearLayout linear_stuff= (LinearLayout) LayoutInflater.from(view.getContext()).
//                        inflate(R.layout.bottom_sheet_services, null, false);
//                TextView txt_name= linear_stuff.findViewById(R.id.txt_name);
//                final TextView txt_des= linear_stuff.findViewById(R.id.txt_description);
//                TextView txt_menu_price_= linear_stuff.findViewById(R.id.txt_menu_price);
//                TextView txt_save_per_= linear_stuff.findViewById(R.id.txt_save_per);
//                TextView txt_price_= linear_stuff.findViewById(R.id.txt_price);
//                LinearLayout linear_change= linear_stuff.findViewById(R.id.linear_change);
//                LinearLayout linear_show= linear_stuff.findViewById(R.id.linear_show);
//
//                txt_name.setText(data.getSelectors().get(selector_index).getServiceTitle());
//                txt_price_.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                        package_service.getPrice()));
//
//                if(package_service.getPrice()<package_service.getMenu_price()){
//
//                    txt_menu_price_.setVisibility(View.VISIBLE);
//                    txt_save_per_.setVisibility(View.VISIBLE);
//
//                    int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                            package_service.getMenu_price());
//
//                    txt_menu_price_.setText(AppConstant.CURRENCY+menu_price_tax);
//                    txt_menu_price_.setPaintFlags( txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//
//                    txt_save_per_.setText(AppConstant.SAVE+" "+
//                            (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                    package_service.getPrice())*100)/
//                                    menu_price_tax))+"%");
//                    txt_save_per_.setBackgroundResource(R.drawable.discount_seletor);
//                }else{
//
//                    txt_menu_price_.setVisibility(View.GONE);
//                    txt_save_per_.setVisibility(View.GONE);
//                }
//
//                String brand_name= package_service.getBrand_name()==null ||
//                        package_service.getBrand_name().equalsIgnoreCase("")?"":package_service.getBrand_name();
//                String product_name= package_service.getProduct_name()==null ||
//                        package_service.getProduct_name().equalsIgnoreCase("")?"":package_service.getProduct_name();
//
//                Log.i("pricekalocha", "values: "+ data.getSelectors().get(selector_index).getServiceTitle()
//                        +  "   "+ brand_name+" : "+ product_name + " ::: "+
//                        package_service.getBrand_name()+ " "+ package_service.getProduct_name());
//
//                if(brand_name.equalsIgnoreCase("") &&
//                        product_name.equalsIgnoreCase("")){
//
////                    linear_des.setVisibility(View.GONE);
//                    txt_des.setText("");
//                }
//                else{
//
//                    if(!product_name.equalsIgnoreCase(""))
//                        brand_name= brand_name + " - ";
//
////                    linear_des.setVisibility(View.VISIBLE);
//                    txt_des.setText(brand_name+ product_name);
//                }
//
//                if(!data.getSelectors().get(i).isShowToUser() ||
//                        (!isService && !isBrand && !isProduct)) {
//                    linear_change.setVisibility(View.INVISIBLE);
//                    linear_change.setEnabled(false);
//                }else{
//                    linear_change.setVisibility(View.VISIBLE);
//                    linear_change.setEnabled(true);
//                    linear_change.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(final View view) {
//
//                            package_service.setName(data.getSelectors().get(selector_index).getServiceTitle());
////                        package_service.setDescription(package_.getSelectors().get(selector_index).getD);
//
//                            DialogFragmentServices fragment= new DialogFragmentServices();
//                            Bundle bundle= new Bundle();
//                            bundle.putString(DialogFragmentServices.SERVICE_DATA,
//                                    new Gson().toJson(data.getSelectors().get(selector_index), Selector.class));
//                            bundle.putString(DialogFragmentServices.DATA,
//                                    new Gson().toJson(package_service, PackageService.class));
//
//                            fragment.setArguments(bundle);
//                            fragment.show(getActivity().getSupportFragmentManager(),
//                                    DialogFragmentServices.DIALOG);
//                            fragment.setListener(new DialogFragmentServices.FragmentListener() {
//                                @Override
//                                public void onClick(PackageService packageService) {
//                                    Log.i("humkahahai", "int the gharh");
//                                    package_services_list.set(selector_index, packageService);
//
//                                    String brand_name= packageService.getBrand_name()==null ||
//                                            packageService.getBrand_name().equalsIgnoreCase("")?"":packageService.getBrand_name();
//                                    String product_name= packageService.getProduct_name()==null ||
//                                            packageService.getProduct_name().equalsIgnoreCase("")?"":packageService.getProduct_name();
//
//                                    if(brand_name.equalsIgnoreCase("") &&
//                                            product_name.equalsIgnoreCase(""))
//                                        txt_des.setText("");
//                                    else
//                                    if(!product_name.equalsIgnoreCase(""))
//                                        brand_name= brand_name + " - ";
//
//                                    txt_des.setText(brand_name+ product_name);
//                                    updatePrice(view);
//
//                                }
//                            });
//                        }
//                    });
//                }
//                linear_.addView(linear_stuff);          //show sirf service name :D
//                }else
//                    linear_.addView(view_);

            }else if(data.getSelectors().get(i).getType().equalsIgnoreCase("subCategory") ){

//                View view_= LayoutInflater.from(getActivity()).
//                        inflate(R.layout.fragment_services_specific_bottomsheet_new_combo, null, false);
//
//                final TextView txt_service, txt_brand, txt_product, txt_service_name
//                        , txt_price_, txt_menu_price_, txt_save_per_;
//                final LinearLayout linear_service, linear_brand, linear_product,
//                        linear_brand_, linear_product_, linear_service_;

//                linear_service= (LinearLayout)view_.findViewById(R.id.linear_service);
//                linear_brand= (LinearLayout)view_.findViewById(R.id.linear_brand);
//                linear_product= (LinearLayout)view_.findViewById(R.id.linear_product);
//
//
//                linear_service_= (LinearLayout)view_.findViewById(R.id.linear_service_);
//                linear_brand_= (LinearLayout)view_.findViewById(R.id.linear_brand_);
//                linear_product_= (LinearLayout)view_.findViewById(R.id.linear_product_);
//
//                txt_service= (TextView)view_.findViewById(R.id.txt_service);
//                txt_brand= (TextView)view_.findViewById(R.id.txt_brand);
//                txt_product= (TextView)view_.findViewById(R.id.txt_product);
//
//                txt_service_name= (TextView)view_.findViewById(R.id.txt_service_name);
//
//                txt_price_= (TextView)view_.findViewById(R.id.txt_price);
//                txt_menu_price_= (TextView)view_.findViewById(R.id.txt_menu_price);
//                txt_save_per_= (TextView)view_.findViewById(R.id.txt_save_per);
//
//                txt_service_name.setText(data.getSelectors().get(selector_index).getServiceTitle());

                //brand ka stuff-------------------------------------------------
                brand_index.add(selector_index, 0);

                //---------------------------package service-------------------
                if (data.getSelectors().get(selector_index).getBrands().size()>0) {
                    package_service.setBrand_id(data.getSelectors().get(selector_index).getBrands().get(0).getBrandId());
                    package_service.setBrand_name(data.getSelectors().get(selector_index).getBrands().get(0).getBrandName());
                    package_service.setProduct_id(data.getSelectors().get(selector_index).getBrands().get(0).
                            getProducts().get(0).getProductId());
                    package_service.setProduct_name(data.getSelectors().get(selector_index).getBrands().get(0).
                            getProducts().get(0).getProductName());
                    package_service.setService_code(data.getSelectors().get(selector_index).getBrands().get(0).
                            getProducts().get(0).getServices().get(0).getServiceCode());
                    package_service.setService_id(data.getSelectors().get(selector_index).getBrands().get(0).
                            getProducts().get(0).getServices().get(0).getServiceId());
                    package_service.setService_name(data.getSelectors().get(selector_index).getBrands().get(0).
                            getProducts().get(0).getServices().get(0).getName());
                    package_service.setPrice((int)data.getSelectors().get(selector_index).getBrands().get(0).
                            getProducts().get(0).getServices().get(0).getPrice());
                    package_service.setMenu_price((int)data.getSelectors().get(selector_index).getBrands().get(0).
                            getProducts().get(0).getServices().get(0).getMenuPrice());

                    package_services_list.add(package_service);
                }



                data.getSelectors().get(selector_index).getBrands().get(0).setCheck(true);
//                txt_brand.setText(data.getSelectors().get(selector_index).getBrands().get(0).getBrandName());

                if(data.getSelectors().get(selector_index).getBrands().size()>1){

                    isBrand= true;
//                    linear_brand.setEnabled(true);
//                    linear_brand_.setVisibility(View.VISIBLE);
                }else{

                    isBrand= false;
//                    linear_brand.setEnabled(false);
//                    linear_brand_.setVisibility(View.GONE);
                }


                //product ka stuff-----------------------------------------------------------
                product_index.add(selector_index, 0);

                if(data.getSelectors().get(selector_index).getBrands().
                        get(brand_index.get(selector_index)).getProducts().get(0).getProductName()==null){

                    Log.i("upgrademai", "main product name == null mai");
//                    txt_product.setText("-");
//                    linear_product.setEnabled(false);
//                    linear_product_.setVisibility(View.GONE);

                    isProduct= false;
//                    linear_product.setVisibility(View.GONE);

                }else{
                    Log.i("upgrademai", "main product name == null naiiiiii");


//                    linear_product.setVisibility(View.VISIBLE);

//                    linear_product.setEnabled(true);

                    data.getSelectors().get(selector_index).getBrands().get(0).
                            getProducts().get(0).setCheck(true);
//                    txt_product.setText(data.getSelectors().get(selector_index).getBrands().get(0).
//                            getProducts().get(0).getProductName());

                    if(data.getSelectors().get(selector_index).getBrands().get(0).getProducts().size()>1){

                        isProduct= true;
//                        linear_product.setEnabled(true);
//                        linear_product_.setVisibility(View.VISIBLE);

                    }else{

                        isProduct= false;
//                        linear_product.setEnabled(false);
//                        linear_product_.setVisibility(View.GONE);
                    }

                }

                //service-------------------------------------------
                service_index.add(selector_index, 0);

                data.getSelectors().get(selector_index).getBrands().get(0).
                        getProducts().get(0).getServices().get(0).setCheck(true);
//                txt_service.setText(data.getSelectors().get(selector_index).getBrands().get(0).
//                        getProducts().get(0).getServices().get(0).getName());

                if(data.getSelectors().get(selector_index).getBrands().get(0).getProducts().get(0).getServices().size()>1){
                    isService= true;
//                    linear_service.setEnabled(true);
//                    linear_service_.setVisibility(View.VISIBLE);
                }else{
                    isService= false;
//                    linear_service.setEnabled(false);
//                    linear_service_.setVisibility(View.GONE);
                }

//                txt_price_.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                        package_service.getPrice()));
//                if(package_service.getPrice()<package_service.getMenu_price()){
//
//                    txt_menu_price_.setVisibility(View.VISIBLE);
//                    txt_save_per_.setVisibility(View.VISIBLE);
//
//                    int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                            package_service.getMenu_price());
//
//                    txt_menu_price_.setText(AppConstant.CURRENCY+ menu_price_tax);
//                    txt_menu_price_.setPaintFlags(txt_menu_price_.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                    txt_save_per_.setText(AppConstant.SAVE+" "+
//                            (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                    package_service.getPrice())*100)/menu_price_tax))+"%");
//                    txt_save_per_.setBackgroundResource(R.drawable.discount_seletor);
//
//                }else{
//                    txt_menu_price_.setVisibility(View.GONE);
//                    txt_save_per_.setVisibility(View.GONE);
//                }



                //--------------------------------------------------++++++++++++++++++++++++++++++++++++++++++

//                linear_brand.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        if (!pop_up){
//                            logPackageBottomSheetChangeEvent();
//                            logPackageBottomSheetChangeFireBaseEvent();
//                        }
//                        //view animator type hai yeh
//                        view_animator.setInAnimation(getActivity(), R.anim.slide_from_right);
//
//                        txt_customize.setText("Customize Your Service");
//                        txt_select.setText("Please Select Any One Option");
//
//
//                        txt_done_apply.setText(getResources().getString(R.string.apply));
//
//                        txt_cancel.setText("Cancel");
//                        txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cancel_));
//
//                        linear_cancel.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                view_animator.setInAnimation(getActivity(), R.anim.slide_from_left);
//
//                                txt_cancel.setText("View Cart");
//                                txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.alter_));
//
//                                cancel();
//
//
//                                txt_customize.setText("Customize Your Package");
//                                txt_select.setText("Please Select");
//
//                                txt_done_apply.setText(getResources().getString(R.string.done));
//                                linear_done.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        if (pop_up){
//                                            logEditServicePackageDoneEvent();
//                                            logEditServicePackageDoneFirBaseEvent();
//                                        }
//                                        else{
//                                            logPackageBottomSheetDoneEvent();
//                                            logPackageBottomSheetDoneFireBaseEvent();
//                                        }
//                                        addServices(false);
//                                        dismiss();
//                                    }
//                                });
//
//                                //brand ka hai yeh
//                                txt_brand.setText(data.getSelectors().get(selector_index).getBrands()
//                                        .get(brand_index.get(selector_index)).getBrandName());
//
//
//                                //products ka case handle kara maine
//                                for(int i=0;i<data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().size();i++){
//
//                                    data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(i).setCheck(false);
//                                }
//
//                                if(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(0).getProductName()==null){
//                                    Log.i("upgrademai", "main product name == null mai");
////                                    linear_product.setEnabled(false);
////                                    txt_product.setText("-");
//
//                                    linear_product.setVisibility(View.GONE);
//                                }else{
//                                    Log.i("upgrademai", "main product name == null naiiiiii");
//
//                                    linear_product.setVisibility(View.VISIBLE);
//
//                                    data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(0).setCheck(true);
//                                    txt_product.setText(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(0).getProductName());
//
//                                    if(data.getSelectors().get(selector_index).getBrands()
//                                            .get(brand_index.get(selector_index)).getProducts().size()>1){
//
//                                        linear_product.setEnabled(true);
//                                        linear_product_.setVisibility(View.VISIBLE);
//
//                                    }else{
//
//                                        linear_product.setEnabled(false);
//                                        linear_product_.setVisibility(View.GONE);
//                                    }
//
//                                }
//
//                                product_index.set(selector_index, 0);
//                                package_service.setProduct_id(data.getSelectors().get(selector_index).getBrands()
//                                        .get(brand_index.get(selector_index))
//                                        .getProducts().get(0).getProductId());
//                                package_service.setProduct_id(data.getSelectors().get(selector_index).getBrands()
//                                        .get(brand_index.get(selector_index))
//                                        .getProducts().get(0).getProductName());
//
//
//                                //service ka setting
//                                for(int i=0;i<data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getServices().size();i++){
//
//                                    data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getServices().get(i).setCheck(false);
//                                }
//                                data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getServices().get(0).setCheck(true);
//                                txt_service.setText(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getServices().get(0).getName());
//
//                                service_index.set(selector_index, 0);
//                                package_service.setService_id(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getServices().get(0).getServiceId());
//                                package_service.setService_name(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getServices().get(0).getName());
//                                package_service.setService_code(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getServices().get(0).getServiceCode());
//                                package_service.setPrice((int)data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getServices().get(0).getPrice());
//                                package_service.setMenu_price((int)data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getServices().get(0).getMenuPrice());
//
//
//                                package_services_list.set(selector_index, package_service);
//
//                                if(data.getSelectors().get(selector_index).getBrands()
//                                        .get(brand_index.get(selector_index)).getProducts()
//                                        .get(product_index.get(selector_index)).getServices().size()>1){
//
//                                    linear_service.setEnabled(true);
//                                    linear_service_.setVisibility(View.VISIBLE);
//                                }else{
//
//                                    linear_service.setEnabled(false);
//                                    linear_service_.setVisibility(View.GONE);
//                                }
//
//                                //----------------------------
//
//                                updatePrice(view);
//
//                                txt_price_.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                        package_service.getPrice()));
//                                if(package_service.getPrice()<package_service.getMenu_price()){
//
//                                    txt_menu_price_.setVisibility(View.VISIBLE);
//                                    txt_save_per_.setVisibility(View.VISIBLE);
//
//                                    int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                            package_service.getMenu_price());
//
//                                    txt_menu_price_.setText(AppConstant.CURRENCY+menu_price_tax);
//                                    txt_menu_price_.setPaintFlags(txt_menu_price_.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                                    txt_save_per_.setText(AppConstant.SAVE+" "+
//                                            (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                    package_service.getPrice())*100)/menu_price_tax))+"%");
//                                    txt_save_per_.setBackgroundResource(R.drawable.discount_seletor);
//                                }else{
//
//                                    txt_menu_price_.setVisibility(View.GONE);
//                                    txt_save_per_.setVisibility(View.GONE);
//                                }
//                                view_animator.showPrevious();
//                            }
//                        });
//
//
//                        linear_done.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                view_animator.setInAnimation(getActivity(), R.anim.slide_from_left);
//
//
//                                txt_customize.setText("Customize Your Package");
//                                txt_select.setText("Please Select");
//
//                                txt_done_apply.setText(getResources().getString(R.string.done));
//
//                                //restoring the done functionality
//                                linear_done.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        if (pop_up){
//                                            logEditServicePackageDoneEvent();
//                                            logEditServicePackageDoneFirBaseEvent();
//                                        }
//                                        else{
//                                            logPackageBottomSheetDoneEvent();
//                                            logPackageBottomSheetDoneFireBaseEvent();
//                                        }
//                                        addServices(false);
//                                        dismiss();
//                                    }
//                                });
//
//                                txt_cancel.setText("View Cart");
//                                txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.alter_));
//
//                                cancel();
//
//                                //brand ka hai yeh
//                                txt_brand.setText(data.getSelectors().get(selector_index).getBrands()
//                                        .get(brand_index.get(selector_index)).getBrandName());
//
//
//                                //products ka case handle kara maine
//                                for(int i=0;i<data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().size();i++){
//
//                                    data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(i).setCheck(false);
//                                }
//
//                                if(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(0).getProductName()==null){
//                                    Log.i("upgrademai", "main product name == null mai");
////                                    linear_product.setEnabled(false);
////                                    txt_product.setText("-");
//
//                                    linear_product.setVisibility(View.GONE);
//                                }else{
//                                    Log.i("upgrademai", "main product name == null naiiiiii");
//
//                                    linear_product.setVisibility(View.VISIBLE);
//
//                                    data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(0).setCheck(true);
//                                    txt_product.setText(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(0).getProductName());
//
//                                    if(data.getSelectors().get(selector_index).getBrands()
//                                            .get(brand_index.get(selector_index)).getProducts().size()>1){
//
//                                        linear_product.setEnabled(true);
//                                        linear_product_.setVisibility(View.VISIBLE);
//
//                                    }else{
//
//                                        linear_product.setEnabled(false);
//                                        linear_product_.setVisibility(View.GONE);
//                                    }
//
//                                }
//
//                                product_index.set(selector_index, 0);
//                                package_service.setProduct_id(data.getSelectors().get(selector_index).getBrands()
//                                        .get(brand_index.get(selector_index))
//                                        .getProducts().get(0).getProductId());
//                                package_service.setProduct_id(data.getSelectors().get(selector_index).getBrands()
//                                        .get(brand_index.get(selector_index))
//                                        .getProducts().get(0).getProductName());
//
//
//                                //service ka setting
//                                for(int i=0;i<data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getServices().size();i++){
//
//                                    data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getServices().get(i).setCheck(false);
//                                }
//                                data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getServices().get(0).setCheck(true);
//                                txt_service.setText(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getServices().get(0).getName());
//
//                                service_index.set(selector_index, 0);
//                                package_service.setService_id(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getServices().get(0).getServiceId());
//                                package_service.setService_name(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getServices().get(0).getName());
//                                package_service.setService_code(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getServices().get(0).getServiceCode());
//                                package_service.setPrice(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getServices().get(0).getPrice());
//                                package_service.setMenu_price(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getServices().get(0).getMenuPrice());
//
//                                package_services_list.set(selector_index, package_service);
//
//                                if(data.getSelectors().get(selector_index).getBrands()
//                                        .get(brand_index.get(selector_index)).getProducts()
//                                        .get(product_index.get(selector_index)).getServices().size()>1){
//
//                                    linear_service.setEnabled(true);
//                                    linear_service_.setVisibility(View.VISIBLE);
//                                }else{
//
//                                    linear_service.setEnabled(false);
//                                    linear_service_.setVisibility(View.GONE);
//                                }
//
//                                //----------------------------
//
//                                updatePrice(view);
//
//                                txt_price_.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                        package_service.getPrice()));
//                                if(package_service.getPrice()<package_service.getMenu_price()){
//
//                                    txt_menu_price_.setVisibility(View.VISIBLE);
//                                    txt_save_per_.setVisibility(View.VISIBLE);
//
//                                    int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                            package_service.getMenu_price());
//
//                                    txt_menu_price_.setText(AppConstant.CURRENCY+menu_price_tax);
//                                    txt_menu_price_.setPaintFlags(txt_menu_price_.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                                    txt_save_per_.setText(AppConstant.SAVE+" "+
//                                            (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                    package_service.getPrice())*100)/menu_price_tax))+"%");
//                                    txt_save_per_.setBackgroundResource(R.drawable.discount_seletor);
//                                }else{
//
//                                    txt_menu_price_.setVisibility(View.GONE);
//                                    txt_save_per_.setVisibility(View.GONE);
//                                }
//
//                                view_animator.showPrevious();
//                            }
//                        });
//
//                        linear_items.removeAllViews();                  //removing the views first...no duplication
//
//                        LinearLayout customize_= (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.edit_service, null);
//                        TextView txt_customize_= (TextView)customize_.findViewById(R.id.txt_customize_);
//                        txt_customize_.setText("Brand");
//                        linear_items.addView(customize_);
//
//                        final List<RadioButton> list_radio_= new ArrayList<>();
//
//                        for(int i=0;i<data.getSelectors().get(selector_index).getBrands().size();i++){
//
//                            final int index =i;             //click ke andar ke liye yeh final kiya
//
//                            View view_= LayoutInflater.from(getActivity()).
//                                    inflate(R.layout.bottomsheet_items, null, false);
//
//                            LinearLayout linear_click= (LinearLayout)view_.findViewById(R.id.linear_click);
//                            TextView txt_name= (TextView)view_.findViewById(R.id.txt_name);
//                            TextView txt_price= (TextView)view_.findViewById(R.id.txt_price);
//                            TextView txt_menu_price= (TextView)view_.findViewById(R.id.txt_menu_price);
//                            TextView txt_save_per= (TextView)view_.findViewById(R.id.txt_save_per);
//
//                            final RadioButton radio_= (RadioButton)view_.findViewById(R.id.radio_);
//                            radio_.setClickable(false);
//
//                            list_radio_.add(radio_);            //adding the radio button in radio list
//
//                            if(data.getSelectors().get(selector_index).getBrands().get(i).isCheck()){
//                                radio_.setChecked(true);
//                                list_radio_.get(i).setChecked(true);
//                            }else{
//                                radio_.setChecked(false);
//                                list_radio_.get(i).setChecked(false);
//                            }
//
//                            txt_name.setText(data.getSelectors().get(selector_index).getBrands().get(i).getBrandName());
//                            txt_price.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                    (int)data.getSelectors().get(selector_index).getBrands().get(i).getPrice()));
//
//                            if(data.getSelectors().get(selector_index).getBrands().get(i).getMenuPrice()>
//                                    data.getSelectors().get(selector_index).getBrands().get(i).getPrice()){
//
//                                txt_menu_price.setVisibility(View.VISIBLE);
//                                txt_save_per.setVisibility(View.VISIBLE);
//
//                                int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                        data.getSelectors().get(selector_index).getBrands().get(i).getMenuPrice());
//
//                                txt_menu_price.setText(AppConstant.CURRENCY+menu_price_tax);
//                                txt_menu_price.setTextColor(Color.parseColor("#a7a9ac"));
//                                txt_menu_price.setPaintFlags(txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                                txt_save_per.setText(AppConstant.SAVE+" "+
//                                        (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                (int)data.getSelectors().get(selector_index).getBrands().get(i).getPrice())*100)/
//                                                menu_price_tax))+"%");
//                                txt_save_per.setBackgroundResource(R.drawable.discount_seletor);
//
//                            }else{
//
//                                txt_menu_price.setVisibility(View.GONE);
//                                txt_save_per.setVisibility(View.GONE);
//                            }
//
//
//                            linear_click.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//
//                                    if(list_radio_.get(index).isChecked()){
//
//                                        list_radio_.get(index).setChecked(false);
//                                    }else{
//
//                                        list_radio_.get(index).setChecked(true);
//                                    }
//
//                                    //updating view
//                                    int list_radio_size=0;
//                                    for(int j=0;j<list_radio_.size();j++){
//
//                                        if(j==index && list_radio_.get(index).isChecked()){     //conditions yaha likh
//
//                                            brand_index.set(selector_index, index);
//                                            data.getSelectors().get(selector_index).getBrands().get(index).setCheck(true);
//
//                                            package_service.setBrand_id(data.getSelectors().get(selector_index).
//                                                    getBrands().get(index).getBrandId());
//                                            package_service.setBrand_name(data.getSelectors().get(selector_index).getBrands()
//                                                    .get(index).getBrandName());
//                                            package_services_list.set(selector_index, package_service);
//
//                                            updatePrice(view);
//
//                                        }else{              //baki sare un check
//
//                                            list_radio_size++;
//                                            list_radio_.get(j).setChecked(false);
//                                            data.getSelectors().get(selector_index).getBrands().get(j).setCheck(false);
//                                        }
//                                    }
//
//                                    //agar koi bhi select nai kara toh current wale ko select karao
//                                    if(list_radio_size==list_radio_.size()){                            //aur conditions yaha likh
//
//                                        brand_index.set(selector_index, index);
//                                        data.getSelectors().get(selector_index).getBrands().get(index).setCheck(true);
//                                        list_radio_.get(index).setChecked(true);
//
//                                        package_service.setBrand_id(data.getSelectors().get(selector_index).
//                                                getBrands().get(index).getBrandId());
//                                        package_service.setBrand_name(data.getSelectors().get(selector_index).getBrands()
//                                                .get(index).getBrandName());
//                                        package_services_list.set(selector_index, package_service);
//                                    }
//                                }
//                            });
//
//
//                            linear_items.addView(view_);
//                        }
//                        view_animator.showNext();
//                    }
//                });
//
//                linear_product.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (!pop_up){
//                            logPackageBottomSheetChangeEvent();
//                            logPackageBottomSheetChangeFireBaseEvent();
//                        }
//
//                        if(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                .getProducts().get(0).getProductName()==null){
//
//                            txt_product.setText("-");
//                            linear_product.setEnabled(false);
//                            linear_product_.setVisibility(View.GONE);
//                        }else{
//                            //view animator type hai yeh
//                            view_animator.setInAnimation(getActivity(), R.anim.slide_from_right);
//
//                            txt_customize.setText("Customize Your Service");
//                            txt_select.setText("Please Select Any One Option");
//
//                            txt_done_apply.setText(getResources().getString(R.string.apply));
//
//                            txt_cancel.setText("Cancel");
//                            txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cancel_));
//
//                            linear_cancel.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    view_animator.setInAnimation(getActivity(), R.anim.slide_from_left);
//
//
//                                    txt_cancel.setText("View Cart");
//                                    txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.alter_));
//
//                                    cancel();
//
//                                    txt_customize.setText("Customize Your Package");
//                                    txt_select.setText("Please Select");
//
//                                    txt_done_apply.setText(getResources().getString(R.string.done));
//                                    linear_done.setOnClickListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View v) {
//
//                                            if (pop_up){
//                                                logEditServicePackageDoneEvent();
//                                                logEditServicePackageDoneFirBaseEvent();
//                                            }
//                                            else{
//                                                logPackageBottomSheetDoneEvent();
//                                                logPackageBottomSheetDoneFireBaseEvent();
//                                            }
//                                            addServices(false);
//                                            dismiss();
//                                        }
//                                    });
//
//                                    //select products ka naam se kiya
//                                    txt_product.setText(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getProductName());
//
//                                    //service ka stuff yaha set karunga
//                                    for(int i=0;i<data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getServices().size();i++){
//
//                                        data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(product_index.get(selector_index)).getServices().get(i).setCheck(false);
//                                    }
//                                    data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getServices().get(0).setCheck(true);
//                                    txt_service.setText(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getServices().get(0).getName());
//
//                                    service_index.set(selector_index, 0);
//                                    package_service.setService_id(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getServices().get(0).getServiceId());
//                                    package_service.setService_name(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getServices().get(0).getName());
//                                    package_service.setService_code(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getServices().get(0).getServiceCode());
//                                    package_service.setPrice(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getServices().get(0).getPrice());
//                                    package_service.setMenu_price(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getServices().get(0).getMenuPrice());
//
//                                    package_services_list.set(selector_index, package_service);
//
//                                    if(data.getSelectors().get(selector_index).getBrands()
//                                            .get(brand_index.get(selector_index)).getProducts()
//                                            .get(product_index.get(selector_index)).getServices().size()>1){
//
//                                        linear_service.setEnabled(true);
//                                        linear_service_.setVisibility(View.VISIBLE);
//                                    }else{
//
//                                        linear_service.setEnabled(false);
//                                        linear_service_.setVisibility(View.GONE);
//                                    }
//
//                                    //---------------------
//
//                                    updatePrice(view);
//
//                                    txt_price_.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                            package_service.getPrice()));
//                                    if(package_service.getPrice()<package_service.getMenu_price()){
//
//                                        txt_menu_price_.setVisibility(View.VISIBLE);
//                                        txt_save_per_.setVisibility(View.VISIBLE);
//
//                                        int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                package_service.getMenu_price());
//
//                                        txt_menu_price_.setText(AppConstant.CURRENCY+menu_price_tax);
//                                        txt_menu_price_.setPaintFlags(txt_menu_price_.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                                        txt_save_per_.setText(AppConstant.SAVE+" "+
//                                                (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                        package_service.getPrice())*100)/menu_price_tax))+"%");
//                                        txt_save_per_.setBackgroundResource(R.drawable.discount_seletor);
//                                    }else{
//
//                                        txt_menu_price_.setVisibility(View.GONE);
//                                        txt_save_per_.setVisibility(View.GONE);
//                                    }
//                                    view_animator.showPrevious();
//                                }
//                            });
//
//                            linear_done.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    view_animator.setInAnimation(getActivity(), R.anim.slide_from_left);
//
//                                    txt_customize.setText("Customize Your Package");
//                                    txt_select.setText("Please Select");
//
//                                    txt_done_apply.setText(getResources().getString(R.string.done));
//
//
//
//                                    //select products ka naam se kiya
//                                    txt_product.setText(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getProductName());
//
//                                    //service ka stuff yaha set karunga
//                                    for(int i=0;i<data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getServices().size();i++){
//
//                                        data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(product_index.get(selector_index)).getServices().get(i).setCheck(false);
//                                    }
//                                    data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getServices().get(0).setCheck(true);
//                                    txt_service.setText(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getServices().get(0).getName());
//
//                                    service_index.set(selector_index, 0);
//                                    package_service.setService_id(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getServices().get(0).getServiceId());
//                                    package_service.setService_name(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getServices().get(0).getName());
//                                    package_service.setService_code(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getServices().get(0).getServiceCode());
//                                    package_service.setPrice(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getServices().get(0).getPrice());
//                                    package_service.setMenu_price(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).getServices().get(0).getMenuPrice());
//
//                                    package_services_list.set(selector_index, package_service);
//
//                                    if(data.getSelectors().get(selector_index).getBrands()
//                                            .get(brand_index.get(selector_index)).getProducts()
//                                            .get(product_index.get(selector_index)).getServices().size()>1){
//
//                                        linear_service.setEnabled(true);
//                                        linear_service_.setVisibility(View.VISIBLE);
//                                    }else{
//
//                                        linear_service.setEnabled(false);
//                                        linear_service_.setVisibility(View.GONE);
//                                    }
//
//                                    //---------------------
//
//                                    linear_done.setOnClickListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View v) {
//                                            if (pop_up){
//                                                logEditServicePackageDoneEvent();
//                                                logEditServicePackageDoneFirBaseEvent();
//                                            }
//                                            else{
//                                                logPackageBottomSheetDoneEvent();
//                                                logPackageBottomSheetDoneFireBaseEvent();
//                                            }
//                                            addServices(false);
//                                            dismiss();
//                                        }
//                                    });
//
//                                    txt_cancel.setText("View Cart");
//                                    txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.alter_));
//
//                                    cancel();
//
//                                    updatePrice(view);
//
//                                    txt_price_.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                            package_service.getPrice()));
//                                    if(package_service.getPrice()<package_service.getMenu_price()){
//
//                                        txt_menu_price_.setVisibility(View.VISIBLE);
//                                        txt_save_per_.setVisibility(View.VISIBLE);
//
//                                        int menu_price= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                package_service.getMenu_price());
//
//                                        txt_menu_price_.setText(AppConstant.CURRENCY+menu_price);
//                                        txt_menu_price_.setPaintFlags(txt_menu_price_.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                                        txt_save_per_.setText(AppConstant.SAVE+" "+
//                                                (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                        package_service.getPrice())*100)/menu_price))+"%");
//                                        txt_save_per_.setBackgroundResource(R.drawable.discount_seletor);
//                                    }else{
//
//                                        txt_menu_price_.setVisibility(View.GONE);
//                                        txt_save_per_.setVisibility(View.GONE);
//                                    }
//
//                                    view_animator.showPrevious();
//                                }
//                            });
//
//                            linear_items.removeAllViews();                  //removing the views first...no duplication
//
//                            LinearLayout customize_= (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.edit_service, null);
//                            TextView txt_customize_= (TextView)customize_.findViewById(R.id.txt_customize_);
//                            txt_customize_.setText("Product");
//                            linear_items.addView(customize_);
//
//                            final List<RadioButton> list_radio_= new ArrayList<>();
//
//                            for(int i=0; i<data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                    .getProducts().get(product_index.get(selector_index)).getServices().size();i++){
//
//                                final int index =i;             //click ke andar ke liye yeh final kiya
//
//                                View view_= LayoutInflater.from(getActivity()).
//                                        inflate(R.layout.bottomsheet_items, null, false);
//
//                                LinearLayout linear_click= (LinearLayout)view_.findViewById(R.id.linear_click);
//                                TextView txt_name= (TextView)view_.findViewById(R.id.txt_name);
//                                TextView txt_price= (TextView)view_.findViewById(R.id.txt_price);
//                                TextView txt_menu_price= (TextView)view_.findViewById(R.id.txt_menu_price);
//                                TextView txt_save_per= (TextView)view_.findViewById(R.id.txt_save_per);
//
//                                final RadioButton radio_= (RadioButton)view_.findViewById(R.id.radio_);
//                                radio_.setClickable(false);
//
//                                list_radio_.add(radio_);            //adding the radio button in radio list
//
//                                //setting check at first... radio button mai
//                                if(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).getServices().get(i).isCheck()){
//
//                                    radio_.setChecked(true);
//                                    list_radio_.get(i).setChecked(true);
//                                }else{
//
//                                    radio_.setChecked(false);
//                                    list_radio_.get(i).setChecked(false);
//                                }
//
//
//                                //yaha name set kara hai
//                                txt_name.setText(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(i).getProductName());
//                                txt_price.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                        (int)data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(i).getPrice()));
//
//                                if(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(i).getMenuPrice()>
//                                        data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(i).getPrice()){
//
//                                    txt_menu_price.setVisibility(View.VISIBLE);
//                                    txt_save_per.setVisibility(View.VISIBLE);
//
//                                    int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                            (int)data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                    .getProducts().get(i).getMenuPrice());
//
//                                    txt_menu_price.setText(AppConstant.CURRENCY+menu_price_tax);
//                                    txt_menu_price.setTextColor(Color.parseColor("#a7a9ac"));
//                                    txt_menu_price.setPaintFlags(txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                                    txt_save_per.setText(AppConstant.SAVE+" "+
//                                            (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                    (int)data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                            .getProducts().get(i).getPrice())*100)/
//                                                    menu_price_tax))+"%");
//                                    txt_save_per.setBackgroundResource(R.drawable.discount_seletor);
//
//                                }else{
//
//                                    txt_menu_price.setVisibility(View.GONE);
//                                    txt_save_per.setVisibility(View.GONE);
//                                }
//
//                                linear_click.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//
//                                        if(list_radio_.get(index).isChecked()){
//
//                                            list_radio_.get(index).setChecked(false);
//                                        }else{
//
//                                            list_radio_.get(index).setChecked(true);
//                                        }
//
//                                        //updating view
//                                        int list_radio_size=0;
//                                        for(int j=0;j<list_radio_.size();j++){
//
//                                            if(j==index && list_radio_.get(index).isChecked()){     //conditions yaha likh
//
//                                                product_index.set(selector_index, index);
//                                                data.getSelectors().get(selector_index).getBrands().get(brand_index.
//                                                        get(selector_index)).getProducts().get(index).setCheck(true);
//
//                                                package_service.setProduct_id(data.getSelectors().get(selector_index).getBrands().get(brand_index.
//                                                        get(selector_index)).getProducts().get(index).getProductId());
//                                                package_service.setProduct_name(data.getSelectors().get(selector_index).getBrands().get(brand_index.
//                                                        get(selector_index)).getProducts().get(index).getProductName());
//                                                package_services_list.set(selector_index, package_service);
//
//                                                updatePrice(view);
//                                            }else{              //baki sare un check
//
//                                                list_radio_size++;
//                                                list_radio_.get(j).setChecked(false);
//                                                data.getSelectors().get(selector_index).getBrands().get(brand_index.
//                                                        get(selector_index)).getProducts().get(j).setCheck(false);
//                                            }
//                                        }
//                                        //agar koi bhi select nai kara toh current wale ko select karao
//                                        if(list_radio_size==list_radio_.size()){                            //aur conditions yaha likh
//
//                                            product_index.set(selector_index, index);
//                                            data.getSelectors().get(selector_index).getBrands().get(brand_index.
//                                                    get(selector_index)).getProducts().get(index).setCheck(true);
//                                            list_radio_.get(index).setChecked(true);
//
//                                            package_service.setProduct_id(data.getSelectors().get(selector_index).getBrands().get(brand_index.
//                                                    get(selector_index)).getProducts().get(index).getProductId());
//                                            package_service.setProduct_name(data.getSelectors().get(selector_index).getBrands().get(brand_index.
//                                                    get(selector_index)).getProducts().get(index).getProductName());
//                                            package_services_list.set(selector_index, package_service);
//                                        }
//                                    }
//                                });
//
//                                linear_items.addView(view_);
//                            }
//
//                            view_animator.showNext();
//                        }
//
//
//                    }
//                });
//
//                //service ka case hai yeh
//                linear_service.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        if (!pop_up){
//                            logPackageBottomSheetChangeEvent();
//                            logPackageBottomSheetChangeFireBaseEvent();
//                        }
//                        //view animator type hai yeh
//                        view_animator.setInAnimation(getActivity(), R.anim.slide_from_right);
//
//                        txt_customize.setText("Customize Your Service");
//                        txt_select.setText("Please Select Any One Option");
//
//                        txt_done_apply.setText(getResources().getString(R.string.apply));
//
//                        txt_cancel.setText("Cancel");
//                        txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cancel_));
//
//                        linear_cancel.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                view_animator.setInAnimation(getActivity(), R.anim.slide_from_left);
//
//
//                                txt_cancel.setText("View Cart");
//                                txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.alter_));
//
//                                cancel();
//
//                                txt_customize.setText("Customize Your Package");
//                                txt_select.setText("Please Select");
//
//                                txt_done_apply.setText(getResources().getString(R.string.done));
//                                linear_done.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        if (pop_up){
//                                            logEditServicePackageDoneEvent();
//                                            logEditServicePackageDoneFirBaseEvent();
//                                        }
//                                        else{
//                                            logPackageBottomSheetDoneEvent();
//                                            logPackageBottomSheetDoneFireBaseEvent();
//                                        }
//                                        addServices(false);
//                                        dismiss();
//                                    }
//                                });
//
//                                txt_service.setText(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).
//                                                getServices().get(service_index.get(selector_index)).getName());
//
//                                updatePrice(view);
//
//                                txt_price_.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                        package_service.getPrice()));
//                                if(package_service.getPrice()<package_service.getMenu_price()){
//
//                                    txt_menu_price_.setVisibility(View.VISIBLE);
//                                    txt_save_per_.setVisibility(View.VISIBLE);
//
//                                    int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                            package_service.getMenu_price());
//
//                                    txt_menu_price_.setText(AppConstant.CURRENCY+menu_price_tax);
//                                    txt_menu_price_.setPaintFlags(txt_menu_price_.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                                    txt_save_per_.setText(AppConstant.SAVE+" "+
//                                            (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                    package_service.getPrice())*100)/menu_price_tax))+"%");
//                                    txt_save_per_.setBackgroundResource(R.drawable.discount_seletor);
//                                }else{
//
//                                    txt_menu_price_.setVisibility(View.GONE);
//                                    txt_save_per_.setVisibility(View.GONE);
//                                }
//                                view_animator.showPrevious();
//                            }
//                        });
//
//                        linear_done.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                view_animator.setInAnimation(getActivity(), R.anim.slide_from_left);
//
//                                txt_customize.setText("Customize Your Package");
//                                txt_select.setText("Please Select");
//
//                                txt_done_apply.setText(getResources().getString(R.string.done));
//
//                                txt_service.setText(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                        .getProducts().get(product_index.get(selector_index)).
//                                                getServices().get(service_index.get(selector_index)).getName());
//
//                                linear_done.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        if (pop_up){
//                                            logEditServicePackageDoneEvent();
//                                            logEditServicePackageDoneFirBaseEvent();
//                                        }
//                                        else{
//                                            logPackageBottomSheetDoneFireBaseEvent();
//                                            logPackageBottomSheetDoneEvent();
//                                        }
//                                        addServices(false);
//                                        dismiss();
//                                    }
//                                });
//
//                                txt_cancel.setText("View Cart");
//                                txt_cancel.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.alter_));
//
//                                cancel();
//
//                                updatePrice(view);
//
//                                txt_price_.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                        package_service.getPrice()));
//                                if(package_service.getPrice()<package_service.getMenu_price()){
//
//                                    txt_menu_price_.setVisibility(View.VISIBLE);
//                                    txt_save_per_.setVisibility(View.VISIBLE);
//
//                                    int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                            package_service.getMenu_price());
//
//                                    txt_menu_price_.setText(AppConstant.CURRENCY+menu_price_tax);
//                                    txt_menu_price_.setPaintFlags(txt_menu_price_.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                                    txt_save_per_.setText(AppConstant.SAVE+" "+
//                                            (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                    package_service.getPrice())*100)/menu_price_tax))+"%");
//                                    txt_save_per_.setBackgroundResource(R.drawable.discount_seletor);
//                                }else{
//
//                                    txt_menu_price_.setVisibility(View.GONE);
//                                    txt_save_per_.setVisibility(View.GONE);
//                                }
//
//                                view_animator.showPrevious();
//                            }
//                        });
//
//                        linear_items.removeAllViews();                  //removing the views first...no duplication
//
//                        LinearLayout customize_= (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.edit_service, null);
//                        TextView txt_customize_= (TextView)customize_.findViewById(R.id.txt_customize_);
//                        txt_customize_.setText("Service ");
//                        linear_items.addView(customize_);
//
//                        final List<RadioButton> list_radio_= new ArrayList<>();
//
//                        for(int i=0;i<data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                .getProducts().get(product_index.get(selector_index)).
//                                        getServices().size();i++){
//
//                            View view_= LayoutInflater.from(getActivity()).
//                                    inflate(R.layout.bottomsheet_items, null, false);
//
//                            LinearLayout linear_click= (LinearLayout)view_.findViewById(R.id.linear_click);
//                            TextView txt_name= (TextView)view_.findViewById(R.id.txt_name);
//                            TextView txt_price= (TextView)view_.findViewById(R.id.txt_price);
//                            TextView txt_menu_price= (TextView)view_.findViewById(R.id.txt_menu_price);
//                            TextView txt_save_per= (TextView)view_.findViewById(R.id.txt_save_per);
//
//                            final RadioButton radio_= (RadioButton)view_.findViewById(R.id.radio_);
//                            radio_.setClickable(false);
//
//                            list_radio_.add(radio_);            //adding the radio button in radio list
//
//                            //setting check at first... radio button mai
//                            if(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                    .getProducts().get(product_index.get(selector_index)).
//                                            getServices().get(i).isCheck()){
//
//                                radio_.setChecked(true);
//                                list_radio_.get(i).setChecked(true);
//                            }else{
//
//                                radio_.setChecked(false);
//                                list_radio_.get(i).setChecked(false);
//                            }
//
//                            //yaha name set kara hai
//                            txt_name.setText(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                    .getProducts().get(product_index.get(selector_index)).
//                                            getServices().get(i).getName());
//                            txt_price.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                    (int)data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).
//                                                    getServices().get(i).getPrice()));
//
//
//                            if(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                    .getProducts().get(product_index.get(selector_index)).
//                                            getServices().get(i).getMenuPrice()>
//                                    data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                            .getProducts().get(product_index.get(selector_index)).
//                                            getServices().get(i).getPrice()){
//
//                                txt_menu_price.setVisibility(View.VISIBLE);
//                                txt_save_per.setVisibility(View.VISIBLE);
//
//                                int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                        data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(product_index.get(selector_index)).
//                                                getServices().get(i).getMenuPrice());
//
//                                txt_menu_price.setText(AppConstant.CURRENCY+menu_price_tax);
//                                txt_menu_price.setTextColor(Color.parseColor("#a7a9ac"));
//                                txt_menu_price.setPaintFlags(txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//                                txt_save_per.setText(AppConstant.SAVE+" "+
//                                        (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                                (int)data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                        .getProducts().get(product_index.get(selector_index)).
//                                                                getServices().get(i).getPrice())*100)/
//                                                menu_price_tax))+"%");
//                                txt_save_per.setBackgroundResource(R.drawable.discount_seletor);
//
//                            }else{
//
//                                txt_menu_price.setVisibility(View.GONE);
//                                txt_save_per.setVisibility(View.GONE);
//                            }
//
//                            final int index =i;             //click ke andar ke liye yeh final kiya
//
//                            linear_click.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//
//                                    if(list_radio_.get(index).isChecked()){
//
//                                        list_radio_.get(index).setChecked(false);
//                                    }else{
//
//                                        list_radio_.get(index).setChecked(true);
//                                    }
//
//                                    //updating view
//                                    int list_radio_size=0;
//                                    for(int j=0;j<list_radio_.size();j++){
//
//                                        if(j==index && list_radio_.get(index).isChecked()){     //conditions yaha likh
//
//                                            service_index.set(selector_index, index);
//                                            data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                    .getProducts().get(product_index.get(selector_index)).
//                                                    getServices().get(index).setCheck(true);
//
//                                            package_service.setService_id(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                    .getProducts().get(product_index.get(selector_index)).
//                                                            getServices().get(index).getServiceId());
//                                            package_service.setService_name(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                    .getProducts().get(product_index.get(selector_index)).
//                                                            getServices().get(index).getName());
//                                            package_service.setService_code(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                    .getProducts().get(product_index.get(selector_index)).
//                                                            getServices().get(index).getServiceCode());
//                                            package_service.setPrice(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                    .getProducts().get(product_index.get(selector_index)).
//                                                            getServices().get(index).getPrice());
//                                            package_service.setMenu_price(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                    .getProducts().get(product_index.get(selector_index)).
//                                                            getServices().get(index).getMenuPrice());
//
//                                            package_services_list.set(selector_index, package_service);
//
//                                        }else{              //baki sare un check
//
//                                            list_radio_size++;
//                                            list_radio_.get(j).setChecked(false);
//                                            data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                    .getProducts().get(product_index.get(selector_index)).
//                                                    getServices().get(j).setCheck(false);
//                                        }
//                                    }
//                                    //agar koi bhi select nai kara toh current wale ko select karao
//                                    if(list_radio_size==list_radio_.size()){                            //aur conditions yaha likh
//
//                                        service_index.set(selector_index, index);
//                                        list_radio_.get(index).setChecked(true);
//                                        data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(product_index.get(selector_index)).
//                                                getServices().get(index).setCheck(true);
//
//                                        package_service.setService_id(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(product_index.get(selector_index)).
//                                                        getServices().get(index).getServiceId());
//                                        package_service.setService_name(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(product_index.get(selector_index)).
//                                                        getServices().get(index).getName());
//                                        package_service.setService_code(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(product_index.get(selector_index)).
//                                                        getServices().get(index).getServiceCode());
//                                        package_service.setPrice(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(product_index.get(selector_index)).
//                                                        getServices().get(index).getPrice());
//                                        package_service.setMenu_price(data.getSelectors().get(selector_index).getBrands().get(brand_index.get(selector_index))
//                                                .getProducts().get(product_index.get(selector_index)).
//                                                        getServices().get(index).getMenuPrice());
//
//                                        package_services_list.set(selector_index, package_service);
//                                    }
//                                }
//                            });
//
//                            linear_items.addView(view_);
//                        }
//
//                        view_animator.showNext();
//                    }
//                });

                Log.i("booleananother","values another: " + isService+ isBrand+isProduct);
//                if(!data.getSelectors().get(i).isShowToUser() ||
//                        (!isService && !isBrand && !isProduct)){

//                LinearLayout linear_stuff= (LinearLayout) LayoutInflater.from(view.getContext()).
//                        inflate(R.layout.bottom_sheet_services, null, false);
//                TextView txt_name= linear_stuff.findViewById(R.id.txt_name);
//                TextView txt_des= linear_stuff.findViewById(R.id.txt_description);
//                TextView txt_menu_price_= linear_stuff.findViewById(R.id.txt_menu_price);
//                TextView txt_save_per_= linear_stuff.findViewById(R.id.txt_save_per);
//                TextView txt_price_= linear_stuff.findViewById(R.id.txt_price);
//                LinearLayout linear_change= linear_stuff.findViewById(R.id.linear_change);
//                LinearLayout linear_show= linear_stuff.findViewById(R.id.linear_show);
//
//
//                txt_name.setText(data.getSelectors().get(selector_index).getServiceTitle());
//                txt_price_.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                        package_service.getPrice()));
//
//                if(package_service.getPrice()<package_service.getMenu_price()){
//
//                    txt_menu_price_.setVisibility(View.VISIBLE);
//                    txt_save_per_.setVisibility(View.VISIBLE);
//
//                    int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                            package_service.getMenu_price());
//
//                    txt_menu_price_.setText(AppConstant.CURRENCY+menu_price_tax);
//                    txt_menu_price_.setPaintFlags( txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//
//                    txt_save_per_.setText(AppConstant.SAVE+" "+
//                            (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
//                                    package_service.getPrice())*100)/
//                                    menu_price_tax))+"%");
//                    txt_save_per_.setBackgroundResource(R.drawable.discount_seletor);
//                }else{
//
//                    txt_menu_price_.setVisibility(View.GONE);
//                    txt_save_per_.setVisibility(View.GONE);
//                }
//
//                String brand_name= package_service.getBrand_name()==null ||
//                        package_service.getBrand_name().equalsIgnoreCase("")?"":package_service.getBrand_name();
//                String product_name= package_service.getProduct_name()==null ||
//                        package_service.getProduct_name().equalsIgnoreCase("")?"":package_service.getProduct_name();
//
//                Log.i("pricekalocha", "values another: "+ data.getSelectors().get(selector_index).getServiceTitle()
//                        +  "   "+ brand_name+" : "+ product_name + " ::: "+
//                        package_service.getBrand_name()+ " "+ package_service.getProduct_name());
//
//                if(brand_name.equalsIgnoreCase("") &&
//                        product_name.equalsIgnoreCase("")){
//
////                        linear_des.setVisibility(View.GONE);
//                }
//                else{
//
//                    if(!product_name.equalsIgnoreCase(""))
//                        brand_name= brand_name + " - ";
//
////                        linear_des.setVisibility(View.VISIBLE);
//                    txt_des.setText(brand_name+ product_name);
//                }
//
//                if(!data.getSelectors().get(i).isShowToUser() ||
//                        (!isService && !isBrand && !isProduct)){
//
//                    linear_change.setVisibility(View.INVISIBLE);
//                    linear_change.setEnabled(false);
//                }else{
//
//                    linear_change.setVisibility(View.VISIBLE);
//                    linear_change.setEnabled(true);
//                }
//
//                linear_.addView(linear_stuff);          //show sirf service name :D
//                }else
//                    linear_.addView(view_);


            }

            LinearLayout linear_stuff= (LinearLayout) LayoutInflater.from(view.getContext()).
                    inflate(R.layout.bottom_sheet_services, null, false);
            TextView txt_name= linear_stuff.findViewById(R.id.txt_name);
            final TextView txt_des= linear_stuff.findViewById(R.id.txt_description);
            TextView txt_menu_price_= linear_stuff.findViewById(R.id.txt_menu_price);
            TextView txt_save_per_= linear_stuff.findViewById(R.id.txt_save_per);
            TextView txt_price_= linear_stuff.findViewById(R.id.txt_price);
            LinearLayout linear_change= linear_stuff.findViewById(R.id.linear_change);
            LinearLayout linear_show= linear_stuff.findViewById(R.id.linear_show);

            txt_name.setText(data.getSelectors().get(selector_index).getServiceTitle());
            txt_price_.setText(AppConstant.CURRENCY+(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                    package_service.getPrice()));

            if(package_service.getPrice()<package_service.getMenu_price()){

                txt_menu_price_.setVisibility(View.VISIBLE);
                txt_save_per_.setVisibility(View.VISIBLE);

                int menu_price_tax= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                        package_service.getMenu_price());

                txt_menu_price_.setText(AppConstant.CURRENCY+menu_price_tax);
                txt_menu_price_.setPaintFlags( txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


                txt_save_per_.setText(AppConstant.SAVE+" "+
                        (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                                package_service.getPrice())*100)/
                                menu_price_tax))+"%");
                txt_save_per_.setBackgroundResource(R.drawable.discount_seletor);
            }else{

                txt_menu_price_.setVisibility(View.GONE);
                txt_save_per_.setVisibility(View.GONE);
            }

            String brand_name= package_service.getBrand_name()==null ||
                    package_service.getBrand_name().equalsIgnoreCase("")?"":package_service.getBrand_name();
            String product_name= package_service.getProduct_name()==null ||
                    package_service.getProduct_name().equalsIgnoreCase("")?"":package_service.getProduct_name();

            Log.i("pricekalocha", "values: "+ data.getSelectors().get(selector_index).getServiceTitle()
                    +  "   "+ brand_name+" : "+ product_name + " ::: "+
                    package_service.getBrand_name()+ " "+ package_service.getProduct_name());

            if(brand_name.equalsIgnoreCase("") &&
                    product_name.equalsIgnoreCase("")){

//                    linear_des.setVisibility(View.GONE);
                txt_des.setText("");
            }
            else{

                if(!product_name.equalsIgnoreCase(""))
                    brand_name= brand_name + " - ";

//                    linear_des.setVisibility(View.VISIBLE);
                txt_des.setText(brand_name+ product_name);
            }

            if(!data.getSelectors().get(i).isShowToUser() ||
                    (!isService && !isBrand && !isProduct)) {
                linear_show.setVisibility(View.GONE);
            }else{

                linear_show.setVisibility(View.VISIBLE);
                linear_change.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {

                        package_service.setName(data.getSelectors().get(selector_index).getServiceTitle());
                        package_service.setDescription(data.getSelectors().get(selector_index).getDescription());

                        DialogFragmentServices fragment= new DialogFragmentServices();
                        Bundle bundle= new Bundle();
                        bundle.putString(DialogFragmentServices.SERVICE_DATA,
                                new Gson().toJson(data.getSelectors().get(selector_index), Selector.class));
                        bundle.putString(DialogFragmentServices.DATA,
                                new Gson().toJson(package_service, PackageService.class));

                        fragment.setArguments(bundle);
                        fragment.show(getActivity().getFragmentManager(),
                                DialogFragmentServices.DIALOG);
                        fragment.setListener(new DialogFragmentServices.FragmentListener() {
                            @Override
                            public void onClick(PackageService packageService) {
                                Log.i("humkahahai", "int the gharh");
                                package_services_list.set(selector_index, packageService);

                                String brand_name= packageService.getBrand_name()==null ||
                                        packageService.getBrand_name().equalsIgnoreCase("")?"":packageService.getBrand_name();
                                String product_name= packageService.getProduct_name()==null ||
                                        packageService.getProduct_name().equalsIgnoreCase("")?"":packageService.getProduct_name();

                                if(brand_name.equalsIgnoreCase("") &&
                                        product_name.equalsIgnoreCase(""))
                                    txt_des.setText("");
                                else
                                if(!product_name.equalsIgnoreCase(""))
                                    brand_name= brand_name + " - ";

                                txt_des.setText(brand_name+ product_name);
                                updatePrice(view);

                            }
                        });
                    }
                });
            }
            linear_.addView(linear_stuff);          //show sirf service name :D


        }

        linear_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pop_up){
                    logEditServicePackageDoneEvent();
                    logEditServicePackageDoneFirBaseEvent();
                }
                else{
                    logPackageBottomSheetDoneEvent();
                    logPackageBottomSheetDoneFireBaseEvent();
                }

                addServices(false);
                dismiss();
            }
        });

        cancel();

        updatePrice(view);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(getActivity()!=null)
                    setToolTip(txt_cancel);
            }
        }, 500);

        view_animator.showNext();
    }

    public void cancel(){

        linear_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pop_up){
                    logPackageBottomSheetCancelEvent();
                    logPackageBottomSheetCancelFireBaseEvent();
                }

                addServices(true);

                dismiss();
            }

        });


    }

    private void setToolTip(TextView txt_cancel){
        View view = getActivity().getLayoutInflater().inflate(R.layout.tooltip_bottomsheet, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT );
        view.setLayoutParams(params);
        new EasyDialog(view.getContext())
                // .setLayoutResourceId(R.layout.layout_tip_content_horizontal)//layout resource id
                .setLayout(view)
                .setBackgroundColor(view.getContext().getResources().getColor(R.color.tooltip_bg))
                // .setLocation(new location[])//point in screen
                .setLocationByAttachedView(txt_cancel)
                .setGravity(EasyDialog.GRAVITY_BOTTOM)
                .setAnimationTranslationShow(EasyDialog.DIRECTION_X, 1000, -600, 100, -50, 50, 0)
                .setAnimationAlphaShow(100, 0.3f, 1.0f)
                .setAnimationTranslationDismiss(EasyDialog.DIRECTION_X, 500, -50, 800)
                .setAnimationAlphaDismiss(200, 1.0f, 0.0f)
                .setTouchOutsideDismiss(true)
                .setOutsideColor(view.getContext().getResources().getColor(android.R.color.transparent))
                .setMatchParent(true)
                .setMarginLeftAndRight(24, 24)
                .show();
    }


    public void saveToCart(){

        if(pop_up){

            UpgradeEvent_ event= new UpgradeEvent_();

            event.setService_name(service_name);

            event.setService_deal_id(""+data.getDealId());
            event.setDealId(Integer.parseInt(deal_id));

            event.setType("newCombo");

            String primary_key="";
            int payable_price=0;
            for(int i=0;i<package_services_list.size();i++){

                int service_code= package_services_list.get(i).getService_code();
                String service_id= package_services_list.get(i).getService_id();
                String brand_id= package_services_list.get(i).getBrand_id()==null?
                        "":package_services_list.get(i).getBrand_id();
                String product_id=package_services_list.get(i).getProduct_id()==null?
                        "":package_services_list.get(i).getProduct_id();
                primary_key+= ""+service_code+ service_id+ brand_id+product_id;

                payable_price+= package_services_list.get(i).getPrice();

                Log.i("primary_keyey", "value in: "+ service_code+
                        "  " + service_id+  " "+ brand_id+  " "+ product_id);
            }

            Log.i("primary_key", "values: "+ primary_key);

            event.setDescription(description);
            event.setPrimary_key(primary_key);
            event.setPackage_services_list(package_services_list);

//            if(slab !=null){
//                for(int i = 0; i< slab.getRanges().size(); i++){
//
//                    if(payable_price>= slab.getRanges().get(i).getRange1() && payable_price<= slab.getRanges().get(i).getRange2()){
//
//                        int discount= (int) ((double)((slab.getRanges().get(i).getDiscount()/100)*payable_price));
//                        payable_price= payable_price - discount;
//                    }
//                }
//            }

            event.setPrice(payable_price);
            event.setMenu_price(menu_price);         //abhi price set kiya

            EventBus.getDefault().post(event);

        }else{

            ServiceComboEvent event= new ServiceComboEvent();

            event.setService_name(service_name);

            event.setService_deal_id(""+data.getDealId());

            event.setDealId(Integer.parseInt(deal_id));

            event.setType("newCombo");

            String primary_key="";
            int payable_price=0;
            for(int i=0;i<package_services_list.size();i++){

                int service_code= package_services_list.get(i).getService_code();
                String service_id= package_services_list.get(i).getService_id();
                String brand_id= package_services_list.get(i).getBrand_id()==null?
                        "":package_services_list.get(i).getBrand_id();
                String product_id=package_services_list.get(i).getProduct_id()==null?
                        "":package_services_list.get(i).getProduct_id();

                primary_key+= ""+service_code+ service_id+ brand_id+product_id;

                payable_price+= package_services_list.get(i).getPrice();

                Log.i("primary_keyey", "value in: "+package_services_list.get(i).getPrice() + " "+ service_code+
                        "  " + service_id+  " "+ brand_id+  " "+ product_id);
            }
            Log.i("primary_keyey", "value in primary key: "+ primary_key);

            event.setDescription(description);
            event.setPrimary_key(primary_key);
            event.setPackage_services_list(package_services_list);

//            if(slab !=null){
//                for(int i = 0; i< slab.getRanges().size(); i++){
//
//                    if(payable_price>= slab.getRanges().get(i).getRange1() && payable_price<= slab.getRanges().get(i).getRange2()){
//
//                        int discount= (int) ((double)((slab.getRanges().get(i).getDiscount()/100)*payable_price));
//                        payable_price= payable_price - discount;
//                    }
//                }
//            }

            event.setPrice(payable_price);
            event.setMenu_price(menu_price);         //abhi price set kiya

            EventBus.getDefault().post(event);
        }
    }

    //updating price
    public void updatePrice(View view){

        int payable_price=0;
        for(int i=0;i<package_services_list.size();i++){

            payable_price+= package_services_list.get(i).getPrice();
        }

//        if(slab !=null){
//            for(int i = 0; i< slab.getRanges().size(); i++){
//
//                if(payable_price>= slab.getRanges().get(i).getRange1() && payable_price<= slab.getRanges().get(i).getRange2()){
//
//                    int discount= (int) ((double)((slab.getRanges().get(i).getDiscount()/100)*payable_price));
//                    payable_price= payable_price - discount;
//                }
//            }
//        }

        int payable_price_=(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                payable_price);

        txt_price.setText(AppConstant.CURRENCY+payable_price_);

        txt_menu_price.setText(AppConstant.CURRENCY+ (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                menu_price));
        txt_menu_price.setPaintFlags(txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        if (menu_price == 0)
            menu_price= 1;


        txt_save_per.setText(AppConstant.SAVE+" "+
                (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                        payable_price)*100)/(int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                        menu_price))+"%"));
        txt_save_per.setTextColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimaryDark));
        txt_save_per.setBackgroundResource(R.drawable.discount_seletor);


        //additional discount ka stuff
        if(BeuSalonsSharedPrefrence.getDiscount(view.getContext())!=null &&
                BeuSalonsSharedPrefrence.getDiscount(view.getContext()).size()>0){

            //price_ tax included hai
            if((payable_price_ >= BeuSalonsSharedPrefrence.getDiscount(view.getContext()).get(0).getMin()) &&
                    payable_price_ < BeuSalonsSharedPrefrence.getDiscount(view.getContext()).get(0).getMax()){

                txt_validity_.setVisibility(View.VISIBLE);
                txt_discount_.setVisibility(View.VISIBLE);
                txt_validity_.setText("Validity: "+ BeuSalonsSharedPrefrence.getDiscount(view.getContext()).get(0).getValidity());
                txt_discount_.setText("Additional "+
                        BeuSalonsSharedPrefrence.getDiscount(view.getContext()).get(0).getDiscountPercent()+" % Package Discount " +
                        "Will Be Applied To Your Cart");

            }else if((payable_price_ >= BeuSalonsSharedPrefrence.getDiscount(view.getContext()).get(1).getMin()) &&
                    payable_price_ < BeuSalonsSharedPrefrence.getDiscount(view.getContext()).get(1).getMax()){

                txt_validity_.setVisibility(View.VISIBLE);
                txt_discount_.setVisibility(View.VISIBLE);
                txt_validity_.setText("Validity: "+ BeuSalonsSharedPrefrence.getDiscount(view.getContext()).get(1).getValidity());
                txt_discount_.setText("Additional "+
                        BeuSalonsSharedPrefrence.getDiscount(view.getContext()).get(1).getDiscountPercent()+" % Package Discount " +
                        "Will Be Applied To Your Cart");

            }else if((payable_price_ >= BeuSalonsSharedPrefrence.getDiscount(view.getContext()).get(2).getMin()) &&
                    payable_price_ <= BeuSalonsSharedPrefrence.getDiscount(view.getContext()).get(2).getMax()){

                txt_validity_.setVisibility(View.VISIBLE);
                txt_discount_.setVisibility(View.VISIBLE);
                txt_validity_.setText("Validity: "+ BeuSalonsSharedPrefrence.getDiscount(view.getContext()).get(2).getValidity());
                txt_discount_.setText("Additional "+
                        BeuSalonsSharedPrefrence.getDiscount(view.getContext()).get(2).getDiscountPercent()+" % Package Discount " +
                        "Will Be Applied To Your Cart");
            }else{

                txt_validity_.setVisibility(View.GONE);
                txt_discount_.setVisibility(View.GONE);
            }


        }else{
            txt_validity_.setVisibility(View.GONE);
            txt_discount_.setVisibility(View.GONE);
        }

    }

    private void addServices(boolean isAlter){

        List<UserServices> list= new ArrayList<>();
        for(int i=0;i<package_services_list.size();i++){

            int service_code= package_services_list.get(i).getService_code();
            String service_id= package_services_list.get(i).getService_id();
            String brand_id= package_services_list.get(i).getBrand_id()==null?
                    "":package_services_list.get(i).getBrand_id();
            String product_id=package_services_list.get(i).getProduct_id()==null?
                    "":package_services_list.get(i).getProduct_id();

            String brand_name= package_services_list.get(i).getBrand_name()==null || package_services_list.get(i).getBrand_name()
                    .equalsIgnoreCase("")?"":package_services_list.get(i).getBrand_name();
            String product_name= package_services_list.get(i).getProduct_name()==null|| package_services_list.get(i).getProduct_name()
                    .equalsIgnoreCase("")?"":package_services_list.get(i).getProduct_name();

            String primary_key= ""+service_code+ service_id+ brand_id+product_id;

            int price= package_services_list.get(i).getPrice();
            int menu_price= package_services_list.get(i).getMenu_price();

            //with tax
            int price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*price);
            int menu_price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()* menu_price);


            Log.i("primary_keyey", "value in: "+ service_code+
                    "  " + service_id+  " "+ brand_id+  " "+ product_id);

            UserServices service= new UserServices();
            service.setName(package_services_list.get(i).getService_name());
            service.setService_code(service_code);
            service.setPrice_id(service_code);


            service.setPrice(price_);
            service.setMenu_price(menu_price_);

            service.setService_id(package_services_list.get(i).getService_id());
            service.setService_deal_id(data.getDealId());

            service.setType("newCombo");

            service.setBrand_name(package_services_list.get(i).getBrand_name());
            service.setProduct_name(package_services_list.get(i).getProduct_name());
            service.setBrand_id(package_services_list.get(i).getBrand_id());
            service.setProduct_id(package_services_list.get(i).getProduct_id());

            service.setDescription(brand_name+" "+product_name);
            service.setPrimary_key(primary_key);

            list.add(service);
            if (!isAlter)
                logAddedToCartEvent(package_services_list.get(i).getService_name(),"INR",price_);

        }

        //pop_up true matab yeh edit service page ka event hai
        if(pop_up)
            EventBus.getDefault().post(new EditPackageListEvent(list, service_name, isAlter));
        else                        //yeh bahar ka event hai
            EventBus.getDefault().post(new PackageListEvent(list, service_name, isAlter));

        dismiss();
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */

    public void logAddedToCartEvent (String contentType, String currency, double price) {
        Log.e("prefine","add  type"+contentType+ " price"+price);

        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        logger.logEvent(AppEventsConstants.EVENT_NAME_ADDED_TO_CART, price, params);
    }


    public void logPackageBottomSheetChangeEvent () {
        Log.e("PackageBottomtChange","fine");

        logger.logEvent(AppConstant.PackageBottomSheetChange);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logPackageBottomSheetDoneEvent () {
        Log.e("PackageBottomSheetDone","fine");

        logger.logEvent(AppConstant.PackageBottomSheetDone);
    }
    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logPackageBottomSheetCancelEvent () {
        Log.e("PackageBtmSheetCancel","fine");
        logger.logEvent(AppConstant.PackageBottomSheetCancel);
    }
    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logEditServicePackageDoneEvent () {
        Log.e("EditServicePackageDone","fine");

        logger.logEvent(AppConstant.EditServicePackageDone);
    }

    public void logEditServicePackageDoneFirBaseEvent () {
        Bundle bundle=new Bundle();

        Log.e("EditPackageDonefirebase","fine");

        mFirebaseAnalytics.logEvent(AppConstant.EditServicePackageDone,bundle);
    }
    public void logPackageBottomSheetCancelFireBaseEvent () {
        Bundle bundle=new Bundle();

        Log.e("packageCancelfirebase","fine");
        mFirebaseAnalytics.logEvent(AppConstant.PackageBottomSheetCancel,bundle);
    }
    public void logPackageBottomSheetChangeFireBaseEvent () {
        Log.e("packageChangefirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.PackageBottomSheetChange,bundle);
    }

    public void logPackageBottomSheetDoneFireBaseEvent () {
        Log.e("packageDoneFirbase","fine");
        Bundle bundle=new Bundle();

        mFirebaseAnalytics.logEvent(AppConstant.PackageBottomSheetDone,bundle);
    }



}
