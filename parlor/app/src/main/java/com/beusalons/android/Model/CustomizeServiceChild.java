package com.beusalons.android.Model;

import java.util.Date;

/**
 * Created by Robbin Singh on 18/11/2016.
 */

public class CustomizeServiceChild {

    private Date date;
    private boolean check;
    private String name;
    private String parentName;
    private int serviceCode;
    //this is price
    private Integer price;
    public CustomizeServiceChild(){

    }

    public CustomizeServiceChild(String name, boolean check, Integer price, String parent) {

        this.name = name;
        this.price = price;
        this.check = check;
        this.parentName = parent;
    }

    public int getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(int serviceCode) {
        this.serviceCode = serviceCode;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}