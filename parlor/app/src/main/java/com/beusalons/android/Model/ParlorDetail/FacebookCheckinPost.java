package com.beusalons.android.Model.ParlorDetail;

/**
 * Created by Ashish Sharma on 10/13/2017.
 */

public class FacebookCheckinPost {
    private String accessToken;
    private String userId;
    private String fbAccessToken;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFbAccessToken() {
        return fbAccessToken;
    }

    public void setFbAccessToken(String fbAccessToken) {
        this.fbAccessToken = fbAccessToken;
    }
}
