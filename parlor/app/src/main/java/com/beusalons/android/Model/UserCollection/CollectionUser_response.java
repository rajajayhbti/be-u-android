package com.beusalons.android.Model.UserCollection;

import com.beusalons.android.Model.Profile.CollectionWisePost;

import java.util.List;


/**
 * Created by Ajay on 2/8/2018.
 */

public class CollectionUser_response {

    private Boolean success;
    private List<CollectionWisePost> data = null;


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<CollectionWisePost> getData() {
        return data;
    }

    public void setData(List<CollectionWisePost> data) {
        this.data = data;
    }
}
