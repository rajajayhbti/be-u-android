package com.beusalons.android.Adapter.NewServiceDeals;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.MemberShipCardAcitvity;
import com.beusalons.android.Model.SalonHome.Membership;
import com.beusalons.android.R;
import com.bumptech.glide.Glide;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.List;

/**
 * Created by myMachine on 5/27/2017.
 */

public class ServiceMembershipAdapter extends RecyclerView.Adapter<ServiceMembershipAdapter.ViewHolder>{

    private Context context;
    private List<Membership> list;
    AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;


    public ServiceMembershipAdapter(Context context, List<Membership> list){

        this.context= context;
        this.list= list;
        logger = AppEventsLogger.newLogger(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Membership membership= list.get(position);
//        String strTxtView5 = " Save <font color=#d2232a><b>"+membership.getDealPercentage()+"%</b></font> on Deal Menu";
//        String strTxtView20 = " Save <font color=#d2232a><b>"+membership.getNormalPercentage()+"%</b></font> on Normal Menu";
//        String strMemberShip="<font color=#d2232a><b>"+membership.getTitle()+"</b></font>";
        holder.txt_title.setText(fromHtml(membership.getTitle()));
//        holder.txt_line_1.setText(fromHtml(membership.getLine1()));
        holder.txt_line_2.setText(fromHtml(membership.getTitle()));
        holder.txt_line_3.setText(fromHtml(membership.getLine3()));
        holder.txt_member.setText("*Applicable To "+membership.getNoOfMembersAllowed()+" Registered Family Wallet Member");
        if(membership.getValidity()!=null &&
                membership.getValidity().equalsIgnoreCase("1")){
            holder.txt_validity.setText("*Validity: "+membership.getValidity() +" Month");
        }else
            holder.txt_validity.setText("*Validity: "+membership.getValidity()+ " Months");


        holder.txt_count_1.setText(fromHtml(membership.getCountDown1()));
        holder.txt_count_2.setText(fromHtml(membership.getCountDown2()));
        if(position==0)
            holder.img_count.setBackgroundResource(R.drawable.ic_family_yellow);
        else if(position==1)
            holder.img_count.setBackgroundResource(R.drawable.ic_family_red);
        else if(position==2)
            holder.img_count.setBackgroundResource(R.drawable.ic_family_green);



        Glide.with(context)
                .load(membership.getCardImage())
//                .centerCrop()
                .into(holder.imgView_servicememberShip);

//        holder.txt_static.setText("*Validity 1 year");
        holder.txt_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logMemberShipViewMoreEvent();
                logMemberShipViewMoreFireBaseEvent();
                context.startActivity(new Intent(context, MemberShipCardAcitvity.class));
            }
        });

        holder.linear_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logMemberShipViewMoreEvent();
                logMemberShipViewMoreFireBaseEvent();
                context.startActivity(new Intent(context, MemberShipCardAcitvity.class));
            }
        });


    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_title,txt_line_1,txt_line_2, txt_line_3, txt_member, txt_validity, txt_count_1, txt_count_2,
                txt_view_more;
        private ImageView imgView_servicememberShip, img_count;
        private LinearLayout linear_view_more, linear_buy;

        public ViewHolder(View itemView) {
            super(itemView);

            txt_member= (TextView)itemView.findViewById(R.id.txt_member);
            txt_validity= (TextView)itemView.findViewById(R.id.txt_validity);
            txt_title= (TextView)itemView.findViewById(R.id.txt_title);
            txt_line_1=(TextView)itemView.findViewById(R.id.txt_line_1);
            txt_line_2=(TextView)itemView.findViewById(R.id.txt_line_2);
            txt_line_3= (TextView)itemView.findViewById(R.id.txt_line_3);
            txt_count_1= (TextView)itemView.findViewById(R.id.txt_count_1);
            txt_count_2= (TextView)itemView.findViewById(R.id.txt_count_2);
            img_count= (ImageView)itemView.findViewById(R.id.img_count);
            txt_view_more= (TextView)itemView.findViewById(R.id.txt_view_more);

            imgView_servicememberShip= (ImageView)itemView.findViewById(R.id.imgView_servicememberShip);

            linear_view_more= (LinearLayout)itemView.findViewById(R.id.linear_view_more);
            linear_buy= (LinearLayout)itemView.findViewById(R.id.linear_buy);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.services_membership_adapter, parent, false);
        return new ViewHolder(view);
    }


    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logMemberShipViewMoreEvent () {
        Log.e("membership from service","fine");
        logger.logEvent(AppConstant.MemberShipViewMore);
    }
    public void logMemberShipViewMoreFireBaseEvent () {
        Log.e("membership fir service","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.MemberShipViewMore,bundle);
    }
    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }
}
