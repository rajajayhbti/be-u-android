package com.beusalons.android.Model.Offers;

import java.util.List;

/**
 * Created by myMachine on 1/21/2017.
 */

public class ProfileData {

    private String firstName;
    private String email;
    private String gender;
    private String phoneNumber;
    private List<ProfileFreeService> freeServices = null;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<ProfileFreeService> getProfileFreeServices() {
        return freeServices;
    }

    public void setProfileFreeServices(List<ProfileFreeService> freeServices) {
        this.freeServices = freeServices;
    }
}
