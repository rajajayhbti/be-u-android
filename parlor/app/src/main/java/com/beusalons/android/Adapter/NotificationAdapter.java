package com.beusalons.android.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.beusalons.android.MainActivity;
import com.beusalons.android.Model.Notifications.NotiFicationData;
import com.beusalons.android.Model.Notifications.NotificationDetail;
import com.beusalons.android.NotificationsActivity;
import com.beusalons.android.OnlinePaymentActivity;
import com.beusalons.android.R;
import com.beusalons.android.Task.SaveNotificationTask;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Ashish Sharma on 3/29/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    Activity context;
    List<NotificationDetail> listData= new ArrayList<>();
    private String userId;
    private String accessToken;
    private int retry_submit;
    private View.OnClickListener clickListener;

    public NotificationAdapter(Activity context, List<NotificationDetail> mlistData, View.OnClickListener mclickListener) {
        this.clickListener=mclickListener;
        this.context = context;
        this.listData=mlistData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.row_notification_offer,parent,false);
        return new ViewHolder(view);
    }
    String parlorId;
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final NotificationDetail notiFicationData=listData.get(position);
        holder.txtUseCode.setVisibility(View.GONE);
        if (notiFicationData.getAction().equalsIgnoreCase("offer")){

            holder.imgIconNotification.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_notification_offer));
            holder.txtTime.setText(notiFicationData.getTime());
            holder.txtTitle.setText(notiFicationData.getTitle());
            holder.txtDiscription.setText(notiFicationData.getBody());
            holder.txtUseCode.setText("Use Code: "+BeuSalonsSharedPrefrence.getReferCode());

            if(notiFicationData.isSeen()){
                holder.txtButton.setBackgroundResource(R.drawable.txt_border_line);
                holder.txtButton.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtButton.setText("CLAIM");

                holder.txtTime.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtTitle.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtDiscription.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtUseCode.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
            }else{
                holder.txtButton.setBackgroundResource(R.drawable.txt_border_line);
                holder.txtButton.setTextColor(ContextCompat.getColor(context, R.color.notification_green));
                holder.txtButton.setText("CLAIM");

                holder.txtTime.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
                holder.txtTitle.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
                holder.txtDiscription.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
                holder.txtUseCode.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
            }

            holder.txtButton.setTag(notiFicationData);
            holder.txtButton.setOnClickListener(clickListener);


        }else if(notiFicationData.getAction().equalsIgnoreCase("review")){

            holder.imgIconNotification.setImageDrawable(context.getResources().getDrawable(R.drawable.notification_icon_review));
            holder.txtTime.setText(notiFicationData.getTime());
            holder.txtTitle.setText(notiFicationData.getTitle());
            holder.txtDiscription.setText(notiFicationData.getBody());
            holder.txtUseCode.setText("Use Code: "+BeuSalonsSharedPrefrence.getReferCode());


            if(notiFicationData.isSeen()){
                holder.txtButton.setBackgroundResource(R.drawable.txt_border_line);
                holder.txtButton.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtButton.setText("REVIEW");

                holder.txtTime.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtTitle.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtDiscription.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtUseCode.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));


            }else{
                holder.txtButton.setBackgroundResource(R.drawable.txt_border_line);
                holder.txtButton.setTextColor(ContextCompat.getColor(context, R.color.notification_red));
                holder.txtButton.setText("REVIEW");

                holder.txtTime.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
                holder.txtTitle.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
                holder.txtDiscription.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
                holder.txtUseCode.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));

            }


            holder.txtButton.setTag(notiFicationData);
            holder.txtButton.setOnClickListener(clickListener);



        }else if(notiFicationData.getAction().equalsIgnoreCase("started")){

            holder.imgIconNotification.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_notification_pay));
            holder.txtTitle.setText(notiFicationData.getTitle());
            holder.txtDiscription.setText(notiFicationData.getBody());
            holder.txtTime.setText(notiFicationData.getTime());
            holder.txtUseCode.setText("Use Code: "+BeuSalonsSharedPrefrence.getReferCode());


            if(notiFicationData.isSeen()){
                holder.txtButton.setBackgroundResource(R.drawable.txt_border_line);
                holder.txtButton.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtButton.setText("PAY");

                holder.txtTime.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtTitle.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtDiscription.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtUseCode.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));

            }else{
                holder.txtButton.setBackgroundResource(R.drawable.txt_border_line);
                holder.txtButton.setTextColor(ContextCompat.getColor(context, R.color.notification_green));
                holder.txtButton.setText("PAY");

                holder.txtTime.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
                holder.txtTitle.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
                holder.txtDiscription.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
                holder.txtUseCode.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));

            }

            holder.txtButton.setTag(notiFicationData.getAppointmentId());
            holder.txtButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notiFicationData.setSeen(true);
                    new Thread(new SaveNotificationTask(context, notiFicationData)).start();

                    String appointmentId=(String) v.getTag();
                    Intent  intent1= new Intent(context, OnlinePaymentActivity.class);
                    intent1.putExtra("apptId",appointmentId);
                    context.startActivity(intent1);
                }
            });




//            holder.txtTime.setText(notiFicationData.getTime());
           /* holder.linearLayout.setVisibility(View.GONE);
            holder.imgNotificatio.setVisibility(View.VISIBLE);*/
        }else if(notiFicationData.getAction().equalsIgnoreCase("recommendation")){

            holder.imgIconNotification.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_notification_offer));
            holder.txtTitle.setText(notiFicationData.getTitle());
            holder.txtDiscription.setText(notiFicationData.getBody());
            holder.txtTime.setText(notiFicationData.getTime());
            holder.txtUseCode.setText("Use Code: "+BeuSalonsSharedPrefrence.getReferCode());


            if(notiFicationData.isSeen()){
                holder.txtButton.setBackgroundResource(R.drawable.txt_border_line);
                holder.txtButton.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtButton.setText("CLAIM");

                holder.txtTime.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtTitle.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtDiscription.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtUseCode.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));

            }else{
                holder.txtButton.setBackgroundResource(R.drawable.txt_border_line);
                holder.txtButton.setTextColor(ContextCompat.getColor(context, R.color.notification_green));
                holder.txtButton.setText("CLAIM");

                holder.txtTime.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
                holder.txtTitle.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
                holder.txtDiscription.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
                holder.txtUseCode.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));

            }

            holder.txtButton.setTag(notiFicationData);
            holder.txtButton.setOnClickListener(clickListener);




        }else if(notiFicationData.getAction().equalsIgnoreCase("cartBased")){

            holder.imgIconNotification.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_notification_offer));
            holder.txtTitle.setText(notiFicationData.getTitle());
            holder.txtDiscription.setText(notiFicationData.getBody());
            holder.txtTime.setText(notiFicationData.getTime());
            holder.txtUseCode.setText("Use Code: "+BeuSalonsSharedPrefrence.getReferCode());


            if(notiFicationData.isSeen()){
                holder.txtButton.setBackgroundResource(R.drawable.txt_border_line);
                holder.txtButton.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtButton.setText("CLAIM");

                holder.txtTime.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtTitle.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtDiscription.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtUseCode.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));

            }else{
                holder.txtButton.setBackgroundResource(R.drawable.txt_border_line);
                holder.txtButton.setTextColor(ContextCompat.getColor(context, R.color.notification_green));
                holder.txtButton.setText("CLAIM");

                holder.txtTime.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
                holder.txtTitle.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
                holder.txtDiscription.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
                holder.txtUseCode.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));

            }

            holder.txtButton.setTag(notiFicationData);
            holder.txtButton.setOnClickListener(clickListener);




        }else if(notiFicationData.getAction().equalsIgnoreCase("profile")){

            holder.imgIconNotification.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_notification_offer));
            holder.txtTitle.setText(notiFicationData.getTitle());
            holder.txtDiscription.setText(notiFicationData.getBody());
            holder.txtTime.setText(notiFicationData.getTime());
            holder.txtUseCode.setText("Use Code: "+BeuSalonsSharedPrefrence.getReferCode());



                holder.txtButton.setBackgroundResource(R.drawable.txt_border_line);
                holder.txtButton.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtButton.setText("CLAIM");

                holder.txtTime.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtTitle.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtDiscription.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtUseCode.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));



            holder.txtButton.setTag(notiFicationData);
            holder.txtButton.setOnClickListener(clickListener);




        }else {

            holder.imgIconNotification.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_notification_offer));
            holder.txtTitle.setText(notiFicationData.getTitle());
            holder.txtDiscription.setText(notiFicationData.getBody());
            holder.txtTime.setText(notiFicationData.getTime());
            holder.txtUseCode.setText("Use Code: "+BeuSalonsSharedPrefrence.getReferCode());


            if(notiFicationData.isSeen()){
                holder.txtButton.setBackgroundResource(R.drawable.txt_border_line);
                holder.txtButton.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtButton.setText("CLAIM");

                holder.txtTime.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtTitle.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtDiscription.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));
                holder.txtUseCode.setTextColor(ContextCompat.getColor(context, R.color.font_light_salons));

            }else{
                holder.txtButton.setBackgroundResource(R.drawable.txt_border_line);
                holder.txtButton.setTextColor(ContextCompat.getColor(context, R.color.notification_green));
                holder.txtButton.setText("CLAIM");

                holder.txtTime.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
                holder.txtTitle.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
                holder.txtDiscription.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));
                holder.txtUseCode.setTextColor(ContextCompat.getColor(context, R.color.black_ninety));

            }

            holder.txtButton.setTag(notiFicationData);
            holder.txtButton.setOnClickListener(clickListener);




        }



    }



    @Override
    public int getItemCount() {

        if(listData.size()>0){

            return listData.size();
        }
        return 0;
    }

    public void setData(List<NotificationDetail> mlistData) {
        this.listData=mlistData;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtButton,txtTitle,txtDiscription,txtUseCode,txtTime;
        ImageView imgNotificatio,imgIconNotification;
        LinearLayout linearLayout;
        public ViewHolder(View itemView) {

            super(itemView);
            imgNotificatio= (ImageView) itemView.findViewById(R.id.img_notificaion);
            imgIconNotification= (ImageView) itemView.findViewById(R.id.img_icon_notification);
            txtButton= (TextView) itemView.findViewById(R.id.txt_button_notification);
            txtTitle= (TextView) itemView.findViewById(R.id.txt_title_notification);
            txtDiscription= (TextView) itemView.findViewById(R.id.txt_discription);
            txtUseCode= (TextView) itemView.findViewById(R.id.txt_use_code);
            txtTime= (TextView) itemView.findViewById(R.id.txt_time_notification);
            linearLayout= (LinearLayout) itemView.findViewById(R.id.ll_notification);

        }
    }
}
