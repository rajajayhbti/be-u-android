package com.beusalons.android.Model.DealsServices;

/**
 * Created by Ashish Sharma on 12/8/2017.
 */

public class ParlorTypes {
    private int type;
    private int startAt;
    private int save;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStartAt() {
        return startAt;
    }

    public void setStartAt(int startAt) {
        this.startAt = startAt;
    }

    public int getSave() {
        return save;
    }

    public void setSave(int save) {
        this.save = save;
    }
}
