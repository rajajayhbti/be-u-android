package com.beusalons.android.Model.Appointments;

/**
 * Created by myMachine on 5/3/2017.
 */

public class CancelAppointmentResponse {

    private boolean success;
    private String data;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
