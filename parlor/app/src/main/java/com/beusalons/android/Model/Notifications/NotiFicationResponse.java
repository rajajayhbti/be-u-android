package com.beusalons.android.Model.Notifications;

import java.util.List;

/**
 * Created by Ashish Sharma on 3/30/2017.
 */

public class NotiFicationResponse {


    private Boolean success;
    private List<NotificationDetail> data;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<NotificationDetail> getData() {
        return data;
    }

    public void setData(List<NotificationDetail> data) {
        this.data = data;
    }
}
