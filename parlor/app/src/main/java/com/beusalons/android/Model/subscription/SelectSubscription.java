package com.beusalons.android.Model.subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SelectSubscription {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("tile")
    @Expose
    private List<Tile> tile = null;
    @SerializedName("subscriptionCount")
    @Expose
    private String subscriptionCount;
    private boolean isSubscribed;
    private  SubscribedData  subscribedData;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Tile> getTile() {
        return tile;
    }

    public void setTile(List<Tile> tile) {
        this.tile = tile;
    }

    public String getSubscriptionCount() {
        return subscriptionCount;
    }

    public void setSubscriptionCount(String subscriptionCount) {
        this.subscriptionCount = subscriptionCount;
    }

    public boolean isSubscribed() {
        return isSubscribed;
    }

    public void setSubscribed(boolean subscribed) {
        isSubscribed = subscribed;
    }

    public SubscribedData getSubscribedData() {
        return subscribedData;
    }

    public void setSubscribedData(SubscribedData subscribedData) {
        this.subscribedData = subscribedData;
    }
}