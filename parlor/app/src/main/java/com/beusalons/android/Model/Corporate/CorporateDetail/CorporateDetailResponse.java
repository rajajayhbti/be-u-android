package com.beusalons.android.Model.Corporate.CorporateDetail;

/**
 * Created by myMachine on 7/31/2017.
 */

public class CorporateDetailResponse {

    private Boolean success;
    private Data data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
