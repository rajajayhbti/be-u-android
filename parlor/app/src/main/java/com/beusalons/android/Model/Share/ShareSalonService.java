package com.beusalons.android.Model.Share;

/**
 * Created by Ashish Sharma on 12/20/2017.
 */

public class ShareSalonService {
    private int index;
    private int serviceCode;


    public ShareSalonService(int index, int serviceCode) {
        this.index = index;
        this.serviceCode = serviceCode;
    }

    public int getIndex() {
        return index;
    }


    public int getServiceCode() {
        return serviceCode;
    }


}
