package com.beusalons.android;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.beusalons.android.Model.DistanceMatrixApi.DistanceModel;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.Utility;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class Activity_GoogleMaps extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    private static final String TAG = Activity_GoogleMaps.class.getSimpleName();

    private GoogleApiClient mGoogleApiClient = null;
    private Location location;

//    LatLng delhi = new LatLng(28.7041, 77.1025);

    String dist;

    private GoogleMap mMap;

    Button btnDistance;

    String parlorName, parlorAddress;
    Double latitude, longitude;
    LatLng parlorLatLong;
    TextView txtViewActionBarName;
    ImageView imgViewBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_maps);
        setToolBar();
        //new ToolbarHelper(getSupportActionBar(), getLayoutInflater(), true, this);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            latitude = bundle.getDouble("latitude");
            longitude = bundle.getDouble("longitude");
            parlorName = bundle.getString("parlorName");
            parlorAddress = bundle.getString("parlorAddress");
        }


        parlorLatLong = new LatLng(latitude, longitude);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }

        btnDistance = (Button) findViewById(R.id.btnDistance);

        btnDistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Uri uri = Uri.parse("http://maps.google.com/maps?q=" + latitude + "," + longitude + "(" + parlorName + ")&iwloc=A&hl=es");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);

            }
        });

    }

    private void setToolBar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getResources().getString(R.string.direction));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);

        }
    }

    public void getTheDistance(String userLat, String userLong) {


        Retrofit distance = ServiceGenerator.getDistance();
        ApiInterface service = distance.create(ApiInterface.class);


        Call<DistanceModel> call = service.distanceMatrix("https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" + userLat + "," + userLong + "&destinations=" + latitude + "," + longitude +
                "&departure_time=now&traffic_model=best_guess&key=" + getString(R.string.distanceMatrixApiKey));

//        Call<DistanceModel> call= service.distanceMatrix(userLat, userLong, String.valueOf(latitude), String.valueOf(longitude));

        call.enqueue(new Callback<DistanceModel>() {
            @Override
            public void onResponse(Call<DistanceModel> call, Response<DistanceModel> response) {

                if (response != null) {
                    try {
                        String distance = response.body().getRows().get(0).getElements().get(0).getDistance().getText();
                        if (distance != null) {
                            btnDistance.setText("Get Direction (" + distance + ")");

                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<DistanceModel> call, Throwable t) {

            }
        });


    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera

//        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(false);

//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(parlorLatLong, 16));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(parlorLatLong, 16));
        mMap.addMarker(new MarkerOptions().position(parlorLatLong).title(parlorName).snippet(parlorAddress));

    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        return false;

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {


        //get user fused location
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        location = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (location != null) {

            getTheDistance(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
        }
//
//
//            /*Log.i("tagThis", "latitude: "+ location.getLatitude());
//            Log.i("tagThis", "Longitude: "+location.getLongitude());*/
////            getDistance(new LatLng(location.getLatitude(), location.getLongitude()), delhi);
//        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
