package com.beusalons.android.Model.Loyalty;

import java.util.ArrayList;

/**
 * Created by Ashish Sharma on 9/6/2017.
 */

public class ArrayInfo {
    private String infoTitle;
    private String infoIcon;
    private ArrayList<String> tnc;
    private ArrayList<String> infoMessage;
    private ArrayList<String> buttonMessage;
    private ArrayList<Buttons> buttons;
    private int indideArrayPosition;

    public String getInfoTitle() {
        return infoTitle;
    }

    public void setInfoTitle(String infoTitle) {
        this.infoTitle = infoTitle;
    }

    public String getInfoIcon() {
        return infoIcon;
    }

    public void setInfoIcon(String infoIcon) {
        this.infoIcon = infoIcon;
    }

    public ArrayList<String> getTnc() {
        return tnc;
    }

    public void setTnc(ArrayList<String> tnc) {
        this.tnc = tnc;
    }

    public ArrayList<String> getInfoMessage() {
        return infoMessage;
    }

    public void setInfoMessage(ArrayList<String> infoMessage) {
        this.infoMessage = infoMessage;
    }

    public ArrayList<Buttons> getButtons() {
        return buttons;
    }

    public void setButtons(ArrayList<Buttons> buttons) {
        this.buttons = buttons;
    }

    public int getIndideArrayPosition() {
        return indideArrayPosition;
    }

    public void setIndideArrayPosition(int indideArrayPosition) {
        this.indideArrayPosition = indideArrayPosition;
    }

    public ArrayList<String> getButtonMessage() {
        return buttonMessage;
    }

    public void setButtonMessage(ArrayList<String> buttonMessage) {
        this.buttonMessage = buttonMessage;
    }
}
