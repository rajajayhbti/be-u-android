package com.beusalons.android.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beusalons.android.Model.OnlinePayment.OnlinePaymentModel;
import com.beusalons.android.R;

import java.util.List;

/**
 * Created by myMachine on 2/23/2017.
 */

public class OnlinePaymentAdapter extends RecyclerView.Adapter<OnlinePaymentAdapter.MyViewHolder> {

    private Activity activity;
    private List<OnlinePaymentModel> list;

    public OnlinePaymentAdapter(Activity activity, List<OnlinePaymentModel> list){
        this.activity= activity;
        this.list= list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(activity).inflate(R.layout.activity_online_payment_recy, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.txt_name.setText(list.get(position).getName());
        holder.txt_price.setText(""+list.get(position).getPrice());

    }

    @Override
    public int getItemCount() {
        if(!list.isEmpty()){

            return list.size();
        }
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_name, txt_price;

        public MyViewHolder(View itemView) {
            super(itemView);

            txt_name= (TextView)itemView.findViewById(R.id.txt_name);
            txt_price= (TextView)itemView.findViewById(R.id.txt_price);

        }
    }



}
