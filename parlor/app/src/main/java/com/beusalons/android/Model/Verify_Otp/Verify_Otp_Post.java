package com.beusalons.android.Model.Verify_Otp;

import java.io.Serializable;

/**
 * Created by myMachine on 11/1/2016.
 */

public class Verify_Otp_Post implements Serializable{

    private String phoneNumber;
    private String otp;
    private String newPassword;

    private String accessToken;
    private String gender;
    private int socialLoginType;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getSocialLoginType() {
        return socialLoginType;
    }

    public void setSocialLoginType(int socialLoginType) {
        this.socialLoginType = socialLoginType;
    }

    public Verify_Otp_Post(){

    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
