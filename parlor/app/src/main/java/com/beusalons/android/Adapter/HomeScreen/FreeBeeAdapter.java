package com.beusalons.android.Adapter.HomeScreen;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.MainActivity;
import com.beusalons.android.Model.HomeFragmentModel.FreeBees;
import com.beusalons.android.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashish sharma on 3/15/2017.
 */

public class FreeBeeAdapter  extends RecyclerView.Adapter<FreeBeeAdapter.ViewHolder> {
    private Activity context;
    List<FreeBees> freeBees;
    AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;

    public FreeBeeAdapter(Activity context, List<FreeBees> mfreeBees){
        this.freeBees=mfreeBees;
        this.context=context;

        if(context!=null){

            logger = AppEventsLogger.newLogger(context);
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_freebie_home,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.freeBeeTitle.setText(freeBees.get(position).getNewHead());
        holder.discription.setText(""+freeBees.get(position).getNewPoints());
        holder.txtViewdiscription.setText(freeBees.get(position).getDescription());
        Glide.with(context).load(freeBees.get(position).getImage()).into(holder.imgFreebee);
      /*  Glide.with(context)
                .load(freeBees.get(position).getImage())
                .fitCenter()
//                                       .placeholder(R.drawable.loading_spinner)
                .crossFade()
                .into(holder.imgFreebee);*/

        holder.linear_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logFreebiesHomeButtonEvent();
                logFreebiesHomeButtonFireBaseEvent();
                MainActivity.isFreebieHome = true;
                AHBottomNavigation bottomNavigation= MainActivity.getBottomNav();
                bottomNavigation.setCurrentItem(2);
            }
        });


    }

    public void setdata(ArrayList<FreeBees> mfreeBees){
        this.freeBees=mfreeBees;
        notifyDataSetChanged();
    }

    public void logFreebiesHomeButtonEvent () {
        Log.e("freebies","fine");
        logger.logEvent(AppConstant.FreebiesHomeButton);
    }
    public void logFreebiesHomeButtonFireBaseEvent () {
        Log.e("freebiesfirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.FreebiesHomeButton,bundle);
    }

    @Override
    public int getItemCount() {
        return freeBees.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView freeBeeTitle,discription,txtViewdiscription;
        private ImageView imgFreebee;
        private LinearLayout linear_click;

        public ViewHolder(View itemView) {
            super(itemView);
            freeBeeTitle= (TextView) itemView.findViewById(R.id.tv_freebe_title);
            discription= (TextView) itemView.findViewById(R.id.tv_freebe_deal);
            imgFreebee = (ImageView) itemView.findViewById(R.id.iv_freebe_img);
            txtViewdiscription=(TextView)itemView.findViewById(R.id.txtViewdiscription);
            linear_click= (LinearLayout)itemView.findViewById(R.id.linear_click);
        }
    }
}
