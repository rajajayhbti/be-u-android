package com.beusalons.android.Model.HomePage;

import java.util.List;

/**
 * Created by bhrigu on 1/24/2018.
 */

public class List_{

    private String title;
    private String heading1;
    private String heading2;
    private List<String> points = null;
    private String recommended;
    private String imageUrl;
    private String action;
    private String heading1HTML;
    private String heading2HTML;
    private double amount;

    private int subscriptionId;

    public int getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(int subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHeading1() {
        return heading1;
    }

    public void setHeading1(String heading1) {
        this.heading1 = heading1;
    }

    public String getHeading2() {
        return heading2;
    }

    public void setHeading2(String heading2) {
        this.heading2 = heading2;
    }

    public List<String> getPoints() {
        return points;
    }

    public void setPoints(List<String> points) {
        this.points = points;
    }

    public String getRecommended() {
        return recommended;
    }

    public void setRecommended(String recommended) {
        this.recommended = recommended;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getHeading1HTML() {
        return heading1HTML;
    }

    public void setHeading1HTML(String heading1HTML) {
        this.heading1HTML = heading1HTML;
    }

    public String getHeading2HTML() {
        return heading2HTML;
    }

    public void setHeading2HTML(String heading2HTML) {
        this.heading2HTML = heading2HTML;
    }
}