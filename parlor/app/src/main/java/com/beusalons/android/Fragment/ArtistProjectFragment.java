package com.beusalons.android.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.beusalons.android.Adapter.Artist.ProfileProjectAdapter;
import com.beusalons.android.Model.Profile.Project;
import com.beusalons.android.Model.UserProject.ProjectData_post;
import com.beusalons.android.Model.UserProject.Project_response;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.PaginationScrollListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Ajay on 2/21/2018.
 */

public class ArtistProjectFragment  extends Fragment{
    private int PAGE_SIZE = 0;

    private List<Project> list;
    RecyclerView recycler_;
    ProfileProjectAdapter adapter;
    private boolean isLoading= false;
    private String artistId;
    public static ArtistProjectFragment newInstance(String artistId){

        ArtistProjectFragment fragment= new ArtistProjectFragment();
        fragment.artistId=artistId;

        return fragment;
    }

//    public static interface UploadTabListener{
//        void onClick();
//    }
//
//    private UploadTabListener tabListener;
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//
//            this.tabListener= (UploadTabListener)context;
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_profile_projects, container, false);

        recycler_= (RecyclerView)view.findViewById(R.id.recycler_);



//            btn_.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    if(tabListener!=null)
//                        tabListener.onClick();
//
//                }
//            });


        inItView();



        Log.e("project","done");

        return view;
    }


    private void inItView(){

        LinearLayoutManager manager=new LinearLayoutManager(getActivity());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler_.setLayoutManager(manager);
        adapter= new ProfileProjectAdapter(list,getActivity());
        recycler_.setAdapter(adapter);
        recycler_.addOnScrollListener(new PaginationScrollListener(manager) {
            @Override
            protected void loadMoreItems() {
                if (!isLoading){
                    isLoading= true;

                    adapter.addProgress();
                    PAGE_SIZE++;
                    fetchData();

                }

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });

        fetchData();

    }


    private  void fetchData(){
        Retrofit retrofit = ServiceGenerator.getPortfolioClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        ProjectData_post projectData_post=new ProjectData_post();

        if (artistId!=null)
            projectData_post.setArtistId(artistId);
        projectData_post.setPage(PAGE_SIZE);
        Call<Project_response> call= apiInterface.getProfileProject(projectData_post);

        call.enqueue(new Callback<Project_response>() {
            @Override
            public void onResponse(Call<Project_response> call, Response<Project_response> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){




                        Log.i("homestuff", " isSuccess");
                        if(response.body().getData()!=null &&
                                response.body().getData().size()>0){
                            Log.i("homestuff", " data hai isme");
                            //progress_bar.setVisibility(View.GONE);

                            if(PAGE_SIZE==0){
                                list=new ArrayList<>();
                                list.addAll(response.body().getData());
                                adapter.setInitialData(list);

                            }
                            else{
                                adapter.removeProgress();
                                adapter.addData(response.body().getData());

                                isLoading= false;
                            }


                            recycler_.setVisibility(View.VISIBLE);
                            //   progress_bar.setVisibility(View.GONE);

                        }else{
                            Log.i("homestuff", "no data");


                            if (adapter.getItemCount()>0){

                            }else {
                                recycler_.setVisibility(View.GONE);
                            }
                            // progress_bar.setVisibility(View.GONE);

                            //Toast.makeText(coordinator_.getContext(), "No Posts To Show", Toast.LENGTH_SHORT).show();
                            //size 0 do something else
                            adapter.removeProgress();

                        }




                    }else if (!response.body().getSuccess()){

                        if (adapter.getItemCount()>0){
                            adapter.removeProgress();
                            //       progress_bar.setVisibility(View.GONE);

                        }else{


                            recycler_.setVisibility(View.GONE);
                            //   progress_bar.setVisibility(View.GONE);
//                            Toast.makeText(getActivity(), "Looks Like The Server Is Taking To Long To Respond\n" +
//                                    " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();

                        }


//                        Toast.makeText(getActivity(), "Looks Like The Server Is Taking To Long To Respond\n" +
//                                " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
                        Log.i("viewprofile", "not success");
                    }

                }else{
                    Toast.makeText(getActivity(), "Looks Like The Server Is Taking To Long To Respond\n" +
                            " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
                    Log.i("viewprofile", "response not successful");
                }

            }

            @Override
            public void onFailure(Call<Project_response> call, Throwable t) {
                if (getActivity()!=null)
                    Toast.makeText(getActivity(), "Looks Like The Server Is Taking To Long To Respond\n" +
                            " Please Try Again In Sometime", Toast.LENGTH_SHORT).show();
                Log.i("viewprofile", "failure: "+ t.getLocalizedMessage()+ "  "+t.getMessage()+"  "+ t.getCause());
            }
        });

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(List<Project> event) {

        /* Do something */
        Log.e("projectlistsie",""+event.size());
        if (list.size()>0){
            list.clear();
        }
        list.addAll(event);
        if(list!=null &&
                list.size()==0){

            recycler_.setVisibility(View.GONE);
        }else{

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    recycler_.setVisibility(View.VISIBLE);


                    LinearLayoutManager layoutManager= new LinearLayoutManager(getActivity());
                    recycler_.setLayoutManager(layoutManager);
                    recycler_.setAdapter(adapter);
                }
            });

        }
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        EventBus.getDefault().register(this);
//
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//
//        EventBus.getDefault().unregister(this);
//
//    }
}
