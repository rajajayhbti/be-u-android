package com.beusalons.android.Model.AppointmentDetail;

import com.beusalons.android.Model.BillSummery.MemberShipSuggetion;
import com.beusalons.android.Model.BillSummery.SubscriptionSuggetion;

import java.util.List;

/**
 * Created by myMachine on 12/13/2016.
 */

public class AppointmentDetailDataResponse {

    private double otherCharges;
    private double subtotal;
    private double discount;
    private double appointmentType;
    private List<Object> allPaymentMethods = null;
    private double membershipDiscount;
    private double payableAmount;
    private double payableAmountForOnlineDiscount;
    private String appointmentId;
    private double parlorAppointmentId;
    private double creditUsed;
    private double tax;
    private String startsAt;
    private double membershipAmount;
    private double estimatedTime;
    private List<Service> services = null;
    private List<Object> products = null;
    private String parlorName;
    private String parlorAddress;
    private Review review;
    private String parlorId;
    private double totalSaved;
    private double loyalityPoints;
    private Boolean isPaid;
    private double latitude;
    private double longitude;
    private String date;
    private double onlineDiscount;
    private double onlineTax;
    private String discountMessage;
    private boolean threadingDiscountAvailable;
    private String alertMessage;
    private String freebiesTerms;
    private String freebiesUsed;
    private double membershipTax;
    private double packageDiscount;
    private double couponLoyalityPoints;
    private double loyalitySubscription;
    private double redeemableSubscriptionLoyality;
    private long subscriptionAmount;
    private double parlorLatitude;
    private double parlorLongitude;
    private SubscriptionSuggetion subscriptionSuggestion;


    private String usableLoyalityPoints;
    private List<UsableMembership> usableMembership = null;
    private MemberShipSuggetion  membershipSuggestion=null;

    private String suggestion;
    private String couponError;
    private String subscriptionPopUpText;
    private int serviceCode;



    public List<UsableMembership> getUsableMembership() {
        return usableMembership;
    }

    public void setUsableMembership(List<UsableMembership> usableMembership) {
        this.usableMembership = usableMembership;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    public String getUsableLoyalityPoints() {
        return usableLoyalityPoints;
    }

    public void setUsableLoyalityPoints(String usableLoyalityPoints) {
        this.usableLoyalityPoints = usableLoyalityPoints;
    }

    public double getOnlineDiscount() {
        return onlineDiscount;
    }

    public void setOnlineDiscount(double onlineDiscount) {
        this.onlineDiscount = onlineDiscount;
    }

    public double getPayableAmountForOnlineDiscount() {
        return payableAmountForOnlineDiscount;
    }

    public void setPayableAmountForOnlineDiscount(double payableAmountForOnlineDiscount) {
        this.payableAmountForOnlineDiscount = payableAmountForOnlineDiscount;
    }

    private boolean onlinePaymentDiscountAvailable;     //100 rupiye ka discount dena hai ki nai

    public boolean isOnlinePaymentDiscountAvailable() {
        return onlinePaymentDiscountAvailable;
    }

    public void setOnlinePaymentDiscountAvailable(boolean onlinePaymentDiscountAvailable) {
        this.onlinePaymentDiscountAvailable = onlinePaymentDiscountAvailable;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getOtherCharges() {
        return otherCharges;
    }

    public void setOtherCharges(double otherCharges) {
        this.otherCharges = otherCharges;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(double appointmentType) {
        this.appointmentType = appointmentType;
    }

    public List<Object> getAllPaymentMethods() {
        return allPaymentMethods;
    }

    public void setAllPaymentMethods(List<Object> allPaymentMethods) {
        this.allPaymentMethods = allPaymentMethods;
    }

    public double getMembershipDiscount() {
        return membershipDiscount;
    }

    public void setMembershipDiscount(double membershipDiscount) {
        this.membershipDiscount = membershipDiscount;
    }

    public double getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(double payableAmount) {
        this.payableAmount = payableAmount;
    }

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public double getParlorAppointmentId() {
        return parlorAppointmentId;
    }

    public void setParlorAppointmentId(double parlorAppointmentId) {
        this.parlorAppointmentId = parlorAppointmentId;
    }

    public double getCreditUsed() {
        return creditUsed;
    }

    public void setCreditUsed(double creditUsed) {
        this.creditUsed = creditUsed;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public String getStartsAt() {
        return startsAt;
    }

    public void setStartsAt(String startsAt) {
        this.startsAt = startsAt;
    }

    public double getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(double estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public List<Object> getProducts() {
        return products;
    }

    public void setProducts(List<Object> products) {
        this.products = products;
    }

    public String getParlorName() {
        return parlorName;
    }

    public void setParlorName(String parlorName) {
        this.parlorName = parlorName;
    }

    public String getParlorAddress() {
        return parlorAddress;
    }

    public void setParlorAddress(String parlorAddress) {
        this.parlorAddress = parlorAddress;
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }

    public double getTotalSaved() {
        return totalSaved;
    }

    public void setTotalSaved(double totalSaved) {
        this.totalSaved = totalSaved;
    }

    public double getLoyalityPoints() {
        return loyalityPoints;
    }

    public void setLoyalityPoints(double loyalityPoints) {
        this.loyalityPoints = loyalityPoints;
    }

    public Boolean getPaid() {
        return isPaid;
    }

    public void setPaid(Boolean paid) {
        isPaid = paid;
    }

    public double getOnlineTax() {
        return onlineTax;
    }

    public void setOnlineTax(double onlineTax) {
        this.onlineTax = onlineTax;
    }

    public String getDiscountMessage() {
        return discountMessage;
    }

    public void setDiscountMessage(String discountMessage) {
        this.discountMessage = discountMessage;
    }

    public boolean isThreadingDiscountAvailable() {
        return threadingDiscountAvailable;
    }


    public String getAlertMessage() {
        return alertMessage;
    }

    public void setAlertMessage(String alertMessage) {
        this.alertMessage = alertMessage;
    }

    public void setThreadingDiscountAvailable(boolean threadingDiscountAvailable) {
        this.threadingDiscountAvailable = threadingDiscountAvailable;
    }


    public double getMembershipAmount() {
        return membershipAmount;
    }

    public void setMembershipAmount(double membershipAmount) {
        this.membershipAmount = membershipAmount;
    }

    public double getMembershipTax() {
        return membershipTax;
    }

    public void setMembershipTax(double membershipTax) {
        this.membershipTax = membershipTax;
    }

    public double getPackageDiscount() {
        return packageDiscount;
    }

    public void setPackageDiscount(double packageDiscount) {
        this.packageDiscount = packageDiscount;
    }

    public class Review {


    }

    public MemberShipSuggetion getMembershipSuggestion() {
        return membershipSuggestion;
    }

    public void setMembershipSuggestion(MemberShipSuggetion membershipSuggestion) {
        this.membershipSuggestion = membershipSuggestion;
    }
    public String getSubscriptionPopUpText() {
        return subscriptionPopUpText;
    }

    public void setSubscriptionPopUpText(String subscriptionPopUpText) {
        this.subscriptionPopUpText = subscriptionPopUpText;
    }
    public class Service {

        public String name;
        public double price;
        public List<Object> employees = null;
        public double estimatedTime;
        public double additions;
        public int quantity;
        public double count;
        public int code;
        public double tax;
        public Boolean dealPriceUsed;
        private double actualPrice;
        private String dealId;
        private String type;
        private String serviceId;
        private int typeIndex;
        private String brandId;
        private String productId;
        private int serviceCode;

        public int getServiceCode() {
            return serviceCode;
        }

        public void setServiceCode(int serviceCode) {
            this.serviceCode = serviceCode;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getBrandId() {
            return brandId;
        }

        public void setBrandId(String brandId) {
            this.brandId = brandId;
        }
        public int getTypeIndex() {
            return typeIndex;
        }

        public void setTypeIndex(int typeIndex) {
            this.typeIndex = typeIndex;
        }

        public String getServiceId() {
            return serviceId;
        }

        public void setServiceId(String serviceId) {
            this.serviceId = serviceId;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDealId() {
            return dealId;
        }

        public void setDealId(String dealId) {
            this.dealId = dealId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public List<Object> getEmployees() {
            return employees;
        }

        public void setEmployees(List<Object> employees) {
            this.employees = employees;
        }

        public double getEstimatedTime() {
            return estimatedTime;
        }

        public void setEstimatedTime(double estimatedTime) {
            this.estimatedTime = estimatedTime;
        }

        public double getAdditions() {
            return additions;
        }

        public void setAdditions(double additions) {
            this.additions = additions;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public double getCount() {
            return count;
        }

        public void setCount(double count) {
            this.count = count;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public double getTax() {
            return tax;
        }

        public void setTax(double tax) {
            this.tax = tax;
        }

        public Boolean getDealPriceUsed() {
            return dealPriceUsed;
        }

        public void setDealPriceUsed(Boolean dealPriceUsed) {
            this.dealPriceUsed = dealPriceUsed;
        }

        public double getActualPrice() {
            return actualPrice;
        }

        public void setActualPrice(double actualPrice) {
            this.actualPrice = actualPrice;
        }
    }


    public String getFreebiesTerms() {
        return freebiesTerms;
    }

    public void setFreebiesTerms(String freebiesTerms) {
        this.freebiesTerms = freebiesTerms;
    }

    public String getFreebiesUsed() {
        return freebiesUsed;
    }

    public void setFreebiesUsed(String freebiesUsed) {
        this.freebiesUsed = freebiesUsed;
    }

    public double getCouponLoyalityPoints() {
        return couponLoyalityPoints;
    }

    public void setCouponLoyalityPoints(double couponLoyalityPoints) {
        this.couponLoyalityPoints = couponLoyalityPoints;
    }

    public String getCouponError() {
        return couponError;
    }

    public void setCouponError(String couponError) {
        this.couponError = couponError;
    }

    public long getSubscriptionAmount() {
        return subscriptionAmount;
    }

    public void setSubscriptionAmount(long subscriptionAmount) {
        this.subscriptionAmount = subscriptionAmount;
    }

    public double getLoyalitySubscription() {
        return loyalitySubscription;
    }

    public void setLoyalitySubscription(double loyalitySubscription) {
        this.loyalitySubscription = loyalitySubscription;
    }

    public double getRedeemableSubscriptionLoyality() {
        return redeemableSubscriptionLoyality;
    }

    public void setRedeemableSubscriptionLoyality(double redeemableSubscriptionLoyality) {
        this.redeemableSubscriptionLoyality = redeemableSubscriptionLoyality;
    }

    public SubscriptionSuggetion getSubscriptionSuggestion() {
        return subscriptionSuggestion;
    }

    public void setSubscriptionSuggestion(SubscriptionSuggetion subscriptionSuggestion) {
        this.subscriptionSuggestion = subscriptionSuggestion;
    }

    public int getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(int serviceCode) {
        this.serviceCode = serviceCode;
    }

    public double getParlorLatitude() {
        return parlorLatitude;
    }

    public void setParlorLatitude(double parlorLatitude) {
        this.parlorLatitude = parlorLatitude;
    }

    public double getParlorLongitude() {
        return parlorLongitude;
    }

    public void setParlorLongitude(double parlorLongitude) {
        this.parlorLongitude = parlorLongitude;
    }
}
