package com.beusalons.android.Model.newServiceDeals.NewCombo;

import java.util.List;

/**
 * Created by myMachine on 6/17/2017.
 */

public class Data {

    private String dealId;
    private List<Selector> selectors = null;

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public List<Selector> getSelectors() {
        return selectors;
    }

    public void setSelectors(List<Selector> selectors) {
        this.selectors = selectors;
    }
}
