package com.beusalons.android.Model.MymembershipDetails;

import java.util.List;

/**
 * Created by Ajay on 11/9/2017.
 */

public class AddUserContactNumber_post {
    private String userId;
    private String accessToken;
    private String membershipSaleId;
    private List<String>  phoneNumber;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getMembershipSaleId() {
        return membershipSaleId;
    }

    public void setMembershipSaleId(String membershipSaleId) {
        this.membershipSaleId = membershipSaleId;
    }

    public List<String> getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(List<String> phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
