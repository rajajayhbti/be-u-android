package com.beusalons.android.Model.Profile;

import java.util.List;

/**
 * Created by Ajay on 1/23/2018.
 */

public class CollectionWisePost {

    private String name;
    private int type;

    private Boolean followingCollection=false;

    public Boolean getFollowingCollection() {
        return followingCollection;
    }

    public void setFollowingCollection(Boolean followingCollection) {
        this.followingCollection = followingCollection;
    }

    private List<Project_> projects = null;

    public CollectionWisePost(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Project_> getProjects() {
        return projects;
    }

    public void setProjects(List<Project_> projects) {
        this.projects = projects;
    }
}
