package com.beusalons.android.Fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Adapter.AdapterRemainingServiceChild;
import com.beusalons.android.Adapter.UserCartAdapter;
import com.beusalons.android.BookingSummaryActivity;
import com.beusalons.android.DealsServicesActivity;
import com.beusalons.android.Event.RemainingServicesEvent.RemainingServiceIndex;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.SalonHome.HomeResponse;
import com.beusalons.android.Model.ServiceAvailable.Service;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.ParlorListActivity;
import com.beusalons.android.R;
import com.beusalons.android.SalonPageActivity;
import com.beusalons.android.Task.UserCartTask;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.michael.easydialog.EasyDialog;
import com.snappydb.DB;
import com.snappydb.DBFactory;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

/**
 * Created by myMachine on 5/31/2017.
 */

public class UserCartFragment extends DialogFragment{

    public static final String FROM_SERVICE= "service_";
    public static final String CART_DATA= "cart_data";
    public static final String HOME_RESPONSE_DATA= "home_data";
    public static final String SHOW_PROCEED="show_proceed";
    public static final String BTN_PROCEED= "btn_proceed";

    public static final String FROM_HOME_HAS_SALONS= "com.beusalons.fromhome.salon";


    private UserCart user_cart;
    AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;
//    public static final String FROM_DEAL= "deal_";
//    public static final String FROM_MEMBERSHIP= "membership_";

    public static boolean show_popup= false;

    private List<UserServices> list= new ArrayList<>();
    private RecyclerView recyclerView;
    private LinearLayout linear_one, linear_back, linear_no_item,linearLayout_price, linear_,linearLayout_top,ll_remaining_sevice_cart,linear_detail;
    private Button btn_proceed;
    private TextView txt_view_more,txt_view_more1;
    private TextView txtText,txtViewValidity,txtGetMore,txtViewPrice,txt_menu_price,txt_save_per;
    TextView txt_parlor_name,  txt_rating,txt_address1,txt_address2;
    TextView txt_timeDate,txtPackageDiscount ;

    private boolean fromHome;
    private boolean firstTime=true;
    private boolean show_proceed= false;
    UserCart saved_cart= null;
    private TextView txt_checkout, txt_proceed;

    private SimpleTooltip tooltip;
    private boolean from_service= false/*, from_deal= false, from_membership= false*/;
    private String cart_data, home_response;
    private HomeResponse homeResponseData=null;
    private RecyclerView recycler_view_remaining_services;
    boolean isSelectedRemaining=false;
    AdapterRemainingServiceChild adapter;
    private  List<Service> serviceList;
    private ImageView img_animation_remain;
    private View view;

    private boolean btn_procee= false;
    private boolean from_home_salon= false;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        try{

            getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);          //no action bar space
        }catch (Exception e){

            Log.i("dialogcrash", "yaha crash hua, aur isko ignore karo");
        }

        view= inflater.inflate(R.layout.fragment_user_cart_, container, false);
        img_animation_remain=(ImageView)view.findViewById(R.id.img_animation_remain);

        txtGetMore=(TextView)view.findViewById(R.id.txtViewGetMore);
        txtText=(TextView)view.findViewById(R.id.txtViewtext);
        txtText=(TextView)view.findViewById(R.id.txtViewtext);
        txt_view_more=(TextView)view.findViewById(R.id.txt_View_nxt_validity);
        txt_view_more1=(TextView)view.findViewById(R.id.txt_view_more);
        txtViewValidity=(TextView)view.findViewById(R.id.txtViewValidity);
        linear_one= (LinearLayout)view.findViewById(R.id.linear_one);           //hide or show the custom toolbar
        linear_back= (LinearLayout)view.findViewById(R.id.linear_back);
        linear_no_item= (LinearLayout)view.findViewById(R.id.linear_no_item);
        txtViewPrice=(TextView)view.findViewById(R.id.txtViewPrice);
        txtPackageDiscount=(TextView)view.findViewById(R.id.txt_package_discount);
        btn_proceed = (Button) view.findViewById(R.id.btn_proceed);
        txt_menu_price=(TextView)view.findViewById(R.id.txt_menu_price);
        txt_save_per=(TextView)view.findViewById(R.id.txt_save_per);
        linearLayout_price=(LinearLayout) view.findViewById(R.id.linearLayout_price);
        ll_remaining_sevice_cart=(LinearLayout) view.findViewById(R.id.ll_remaining_sevice_cart);
        recycler_view_remaining_services= (RecyclerView) view.findViewById(R.id.recycler_view_remaining_services);

        linear_= (LinearLayout)view.findViewById(R.id.linear_);
        linear_detail=(LinearLayout)view.findViewById(R.id.linear_detail);
        linearLayout_top=(LinearLayout)view.findViewById(R.id.linearLayout_top);
        linear_.setVisibility(View.GONE);
        txt_checkout= (TextView)view.findViewById(R.id.txt_checkout);
        txt_proceed= (TextView)view.findViewById(R.id.txt_proceed);

        // top parlor name time address
        txt_timeDate = (TextView)view. findViewById(R.id.txt_booking_summary_parlor_booking_time);
        txt_parlor_name   = (TextView)view. findViewById(R.id.txt_booking_summary_parlor_name);
        txt_rating = (TextView)view. findViewById(R.id.txt_booking_summary_parlor_rating);
        txt_address1 = (TextView)view. findViewById(R.id.txt_booking_summary_parlor_address_1);
        txt_address2 = (TextView)view. findViewById(R.id.txt_booking_summary_parlor_address_2);
        serviceList=new ArrayList<>();
        logger = AppEventsLogger.newLogger(getActivity());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
      /*  tooltip=new SimpleTooltip.Builder(getActivity())
                .anchorView(txt_checkout)
                .text("Texto do Tooltip kjhkjhkhjkhkkjhk hkjhkjhkjhkjhkjhkhjkjhkjhkjhk jhkjhkkhjkhjkjhk hk k k  kjhkjhkhk jhkhkh")
                .gravity(Gravity.TOP)
                .animated(true)
                .transparentOverlay(true)
                .build();

        tooltip .show();*/

        fromHome= true;
        Bundle bundle= getArguments();
        if(bundle!=null && bundle.containsKey("has_data")){

            fromHome= false;
            linear_one.setVisibility(View.VISIBLE);                 //not from home so toolbar dikhana hai

            from_home_salon= bundle.getBoolean(FROM_HOME_HAS_SALONS, false);

            from_service= bundle.getBoolean(FROM_SERVICE, false);
            cart_data= bundle.getString(CART_DATA, "");
            user_cart=  new Gson().fromJson(bundle.getString("cart_data", null), UserCart.class);
            show_proceed= bundle.getBoolean(SHOW_PROCEED, false);
            btn_procee= bundle.getBoolean(BTN_PROCEED, false);

            home_response= bundle.getString(HOME_RESPONSE_DATA, "");
            homeResponseData=new Gson().fromJson(home_response,HomeResponse.class);

//            from_deal= Fbundle.getBoolean(FROM_DEAL, false);
//            from_membership= bundle.getBoolean(FROM_MEMBERSHIP, false);

            if (homeResponseData==null){
                linear_detail.setVisibility(View.GONE);
            }else
                linear_detail.setVisibility(View.VISIBLE);

            if (homeResponseData!=null&&homeResponseData.getData()!=null && homeResponseData.getData().getServicesAvailable()!=null && homeResponseData.getData().getServicesAvailable().size()>0){
                linear_detail.setVisibility(View.VISIBLE);
                if (serviceList.size()>0){
                    serviceList.clear();
                }

                serviceList.addAll(homeResponseData.getData().getServicesAvailable().get(0).getServices());
               if (serviceList.size()>0){
                   ll_remaining_sevice_cart.setVisibility(View.VISIBLE);

               }else                 ll_remaining_sevice_cart.setVisibility(View. GONE);

                if (isSelectedRemaining){
                    recycler_view_remaining_services.setVisibility(View.VISIBLE);
                    recycler_view_remaining_services.setFocusable(false);
                    img_animation_remain.setImageResource(R.drawable.ic_arrow_down);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    recycler_view_remaining_services.setLayoutManager(mLayoutManager);
                    adapter = new AdapterRemainingServiceChild(getActivity(),
                            homeResponseData.getData().getServicesAvailable().get(0).getServices() , 0,from_service);
                    recycler_view_remaining_services.setAdapter(adapter);
                }else{
                    recycler_view_remaining_services.setVisibility(View.GONE);
                    img_animation_remain.setImageResource(R.drawable.ic_right);



                }

                ll_remaining_sevice_cart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isSelectedRemaining){
                            img_animation_remain.setImageResource(R.drawable.ic_arrow_down);
                            recycler_view_remaining_services.setVisibility(View.VISIBLE);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            recycler_view_remaining_services.setLayoutManager(mLayoutManager);
                            adapter = new AdapterRemainingServiceChild(getActivity(),
                                    homeResponseData.getData().getServicesAvailable().get(0).getServices() , 0,from_service);
                            recycler_view_remaining_services.setAdapter(adapter);
                            isSelectedRemaining=false;
                        }else{
                            isSelectedRemaining=true;
                            img_animation_remain.setImageResource(R.drawable.ic_right);

                            recycler_view_remaining_services.setVisibility(View.GONE);
                        }

                    }
                });


            }else ll_remaining_sevice_cart.setVisibility(View.GONE);


        }else{

            fromHome= true;
            linear_one.setVisibility(View.GONE);
            ll_remaining_sevice_cart.setVisibility(View.GONE);

        }





        recyclerView= (RecyclerView)view.findViewById(R.id.rec_user_cart);
        LinearLayoutManager layoutManager= new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setFocusable(false);

        return view;
    }

    private void setToolTip(){

        View view = getActivity().getLayoutInflater().inflate(R.layout.tooltip_cart, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT );
        view.setLayoutParams(params);
        new EasyDialog(getActivity())
                // .setLayoutResourceId(R.layout.layout_tip_content_horizontal)//layout resource id
                .setLayout(view)
                .setBackgroundColor(getActivity().getResources().getColor(R.color.tooltip_bg))
                // .setLocation(new location[])//point in screen
                .setLocationByAttachedView(txt_view_more1)
                .setGravity(EasyDialog.GRAVITY_BOTTOM)
                .setAnimationTranslationShow(EasyDialog.DIRECTION_X, 1000, -600, 100, -50, 50, 0)
                .setAnimationAlphaShow(100, 0.3f, 1.0f)
                .setAnimationTranslationDismiss(EasyDialog.DIRECTION_X, 500, -50, 800)
                .setAnimationAlphaDismiss(200, 1.0f, 0.0f)
                .setTouchOutsideDismiss(true)
                .setOutsideColor(getActivity().getResources().getColor(android.R.color.transparent))
                .setMatchParent(true)
                .setMarginLeftAndRight(24, 24)
                .show();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(RemainingServiceIndex event) {

        Log.i("remainingservice", "i'm in the event");
        if(getActivity()!=null)
            Toast.makeText(getActivity(), "Service Added To Cart!", Toast.LENGTH_SHORT).show();

        //service kara
        UserServices userServices=new UserServices();

        userServices.setName(homeResponseData.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position()).getName());


        userServices.setService_code(homeResponseData.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position()).getServiceCode());
        userServices.setPrice_id(homeResponseData.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position()).getServiceCode());


        if (homeResponseData.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position()).getType()!=null)
        userServices.setMemberShipRemaingType(homeResponseData.getData().getServicesAvailable().get(0).getServices()
        .get(event.getService_position()).getType());


        String brand_id= homeResponseData.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position()).getBrandId()==null?"":homeResponseData.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position()).getBrandId();
        String product_id= homeResponseData.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position())==null?"":homeResponseData.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position()).getProductId();
        userServices.setBrand_id(brand_id);
        userServices.setProduct_id(product_id);
        userServices.setRemainingTotalQuantity(homeResponseData.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position()).getQuantity());
        String primary_key= homeResponseData.getData().getServicesAvailable().get(0).getServices()
                .get(event.getService_position()).getServiceCode()+brand_id+product_id;
        userServices.setPrimary_key(primary_key);

        if (homeResponseData.getData().getServicesAvailable().get(0).getServices().get(event.getService_position()).getType()!=null &&
                homeResponseData.getData().getServicesAvailable().get(0).getServices().get(event.getService_position()).getType()
                        .equals("membership")){
            userServices.setMyMembershipFreeService(true);
            userServices.setType("membership");

        }
        else {
            if (homeResponseData.getData().getServicesAvailable().get(0).getServices()
                    .get(event.getService_position()).getPrice()!=0)
                userServices.setPrice(homeResponseData.getData().getServicesAvailable().get(0).getServices()
                        .get(event.getService_position()).getPrice());


            if (homeResponseData.getData().getServicesAvailable().get(0).getServices()
                    .get(event.getService_position()).getActualPrice()!=null)
                userServices.setMenu_price(homeResponseData.getData().getServicesAvailable().get(0).getServices()
                        .get(event.getService_position()).getActualPrice());
            userServices.setRemainingService(true);
            userServices.setType("serviceAvailable");
        }
        if(getActivity()!=null)
            new Thread(new UserCartTask(getActivity(), user_cart, userServices, false, false)).start();

        try {
            updateAddData();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    private void updateAddData(){
        Thread  thread = new Thread(){
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(200);

                        getActivity(). runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                getCardData();
                            }
                        });

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                /*Intent mainActivity = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(mainActivity);*/
            }
        };
        thread.start();
    }


    /*@Override
    public void onDismiss(DialogInterface dialog) {
        if (getActivity() != null) {
            txt_name.performClick();
        }
        super.onDismiss(dialog);
    }*/

    //    @Override
//    public void onPause() {
//        super.onPause();
//        if (tooltip != null && tooltip.isShowing()) {
//            tooltip.dismiss();
//        }
//    }
    @Override
    public void onDestroyView() {
        Dialog dialog = getDialog();
        // handles https://code.google.com/p/android/issues/detail?id=17423
        if (dialog != null && getRetainInstance()) {
            dialog.setDismissMessage(null);
            if (tooltip != null && tooltip.isShowing()) {
                tooltip.dismiss();
            }
        }
        super.onDestroyView();
    }






    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        Log.e("start","onStart");
    }
    public void logContinueShoppingClickedEvent () {
        Log.e("ContinueShoCliedClicked","fine");

        logger.logEvent(AppConstant.ContinueShoppingClicked);
    }

    public void logContinueShoppingClickedFireBaseEvent () {
        Log.e("ContieSgClfirebase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.ContinueShoppingClicked,bundle);
    }

    @Override
    public void onResume() {
        super.onResume();

        getCardData();               //update card here
        linear_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    dismiss();

            }
        });
        txt_view_more1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setToolTip();
            }
        });

        //proceed ke button ka code hai
        if(fromHome){               //home page se(bahar se)

            final String cart_type= saved_cart==null?AppConstant.SERVICE_TYPE:saved_cart.getCartType();
            btn_proceed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (saved_cart.getServicesList().size()==1 &&
                            saved_cart.getServicesList().get(0).isMembership()){
                        Intent intent= new Intent(getActivity(), BookingSummaryActivity.class);
                        intent.putExtra("user_cart" ,cart_data);
                        intent.putExtra(BookingSummaryActivity.HOME_RESPONSE, home_response);
                        startActivity(intent);
                    } else {

                        try {
                            if(saved_cart.getServicesList().size()>0 &&
                                    saved_cart.getParlorId()!=null &&
                                    !saved_cart.getParlorId().equalsIgnoreCase("")){

                                Intent intent = new Intent(getContext(), SalonPageActivity.class);
                                intent.putExtra("parlorId", saved_cart.getParlorId());
                                getContext().startActivity(intent);
                            }else{

                                Intent intent= new Intent(getActivity(), ParlorListActivity.class);
                                intent.putExtra(cart_type.equalsIgnoreCase(AppConstant.SERVICE_TYPE)?
                                        "isService":"isDeal",true);
                                startActivity(intent);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }


                    }

                }
            });
        }else if(from_service){

            btn_proceed.setVisibility(View.GONE);
            linear_.setVisibility(View.VISIBLE);

            txt_checkout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent= new Intent(getActivity(), BookingSummaryActivity.class);
                    intent.putExtra("user_cart" ,cart_data);
                    intent.putExtra(BookingSummaryActivity.HOME_RESPONSE, home_response);
                    startActivity(intent);
                }
            });

            txt_proceed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    logContinueShoppingClickedEvent();
                    logContinueShoppingClickedFireBaseEvent();
                    dismiss();
                }
            });


        }


        if(btn_procee){

            btn_proceed.setVisibility(View.VISIBLE);
            btn_proceed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent= new Intent(getActivity(), BookingSummaryActivity.class);
                    intent.putExtra("user_cart" ,cart_data);
                    intent.putExtra(BookingSummaryActivity.HOME_RESPONSE, home_response);
                    startActivity(intent);

                }
            });
        }

        if(show_proceed) {
            btn_proceed.setVisibility(View.VISIBLE);
        }

        if(saved_cart==null ||
                (saved_cart!=null &&
                        saved_cart.getServicesList()!=null &&
                        saved_cart.getServicesList().size()==0))
            btn_proceed.setVisibility(View.GONE);
        else if(saved_cart!=null &&
                saved_cart.getCartType()!=null &&
                saved_cart.getCartType().equalsIgnoreCase(AppConstant.DEAL_TYPE)){
            btn_proceed.setVisibility(View.VISIBLE);
            btn_proceed.setText("Select Salon");
            btn_proceed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent= new Intent(view.getContext(), ParlorListActivity.class);
                    intent.putExtra("isDeal", true);
                    startActivity(intent);
                }
            });
        }else if(saved_cart!=null &&
                saved_cart.getCartType()!=null &&
                saved_cart.getCartType().equalsIgnoreCase(AppConstant.SERVICE_TYPE) &&
                from_home_salon){
            btn_proceed.setVisibility(View.VISIBLE);
            btn_proceed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(view.getContext(), SalonPageActivity.class);
                    intent.putExtra("parlorId", saved_cart.getParlorId());
                    startActivity(intent);
                }
            });

        }

    }



    private void setParlorDetailFromCart(){


        if (saved_cart!=null ){
            if (saved_cart.getCartType()!=null &&
                    saved_cart.getCartType().equals(AppConstant.DEAL_TYPE)){
                linear_detail.setVisibility(View.GONE);
            }else if (saved_cart.getCartType().equals(AppConstant.OTHER_TYPE)){
                linear_detail.setVisibility(View.GONE);
            }

            else {
                    txt_parlor_name.setText(saved_cart.getParlorName());
                    txt_rating.setText(String.valueOf(saved_cart.getRating()));
                    txt_address1.setText(saved_cart.getAddress1());
                    txt_address2.setText(saved_cart.getAddress2());

                if (saved_cart.getDate_time()!=null){
                    txt_timeDate.setVisibility(View.VISIBLE);
                    txt_timeDate.setText(formatDateTime(saved_cart.getDate_time()));           //format the json date-time

                } else txt_timeDate.setVisibility(View.GONE);

            }

        }
        else{
            linear_detail.setVisibility(View.GONE);
        }

//    txt_parlor_name.setText(saved_cart.getParlorName());
//    txt_address1.setText(saved_cart.getAddress1());
//    txt_address2.setText(saved_cart.getAddress2());
//    txt_rating.setText(""+saved_cart.getRating());
//    txt_timeDate.setText(saved_cart.getDate_time());
    }

    private void getCardData(){
        try {

            DB snappyDB= DBFactory.open(getActivity());
            saved_cart= null;               //db mai jo saved cart hai
            if(snappyDB.exists(AppConstant.USER_CART_DB)){

                saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
                setParlorDetailFromCart();  //update parlor details in card here
                snappyDB.close();
                if(saved_cart.getServicesList().size()>0){

                    linear_no_item.setVisibility(View.GONE);

                    if (fromHome){
                        btn_proceed.setVisibility(View.VISIBLE);

                    }else
                    {
                        btn_proceed.setVisibility(View.GONE);
                    }

                    if (show_proceed)
                        btn_proceed.setVisibility(View.VISIBLE);
                    list.clear();
                    list= saved_cart.getServicesList();
                    UserCartAdapter adapter = new UserCartAdapter(getActivity(), saved_cart, list, btn_proceed, fromHome, linear_no_item);

                    recyclerView.setAdapter(adapter);
                    if (homeResponseData!=null && homeResponseData.getData().getServicesAvailable()!=null && homeResponseData.getData().getServicesAvailable().size()>0){
                        updateQuantity(list,homeResponseData.getData().getServicesAvailable().get(0).getServices());

                    }
                    setData(list);
                    adapter.notifyDataSetChanged();

                    if (firstTime && from_service){
                        Thread  thread = new Thread(){
                            @Override
                            public void run() {
                                try {
                                    synchronized (this) {
                                        wait(500);
                                        if(getActivity()!=null){
                                            getActivity(). runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {

                                                    setToolTip();
                                                    firstTime=false;
                                                }
                                            });
                                        }
                                    }
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                /*Intent mainActivity = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(mainActivity);*/
                            }
                        };
                        thread.start();

                    }

                }else{

                    linear_no_item.setVisibility(View.VISIBLE);
                    list.clear();
                    setData(list);
                    if(fromHome)
                        btn_proceed.setVisibility(View.GONE);
                    if (show_proceed)
                        btn_proceed.setVisibility(View.VISIBLE);
                }
            }else{

                snappyDB.close();
                list.clear();
                setData(list);
                setParlorDetailFromCart();  //update parlor details in card here
                linear_no_item.setVisibility(View.VISIBLE);
                if(fromHome)
                    btn_proceed.setVisibility(View.GONE);
                if (show_proceed)
                    btn_proceed.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){

            if(fromHome)
                btn_proceed.setVisibility(View.GONE);
            if (show_proceed)
                btn_proceed.setVisibility(View.VISIBLE);

            e.printStackTrace();
        }

        if(saved_cart!=null &&
                saved_cart.getCartType()!=null &&
                saved_cart.getCartType().equalsIgnoreCase(AppConstant.SERVICE_TYPE) &&
                from_home_salon &&
                saved_cart.getServicesList()!=null &&
                saved_cart.getServicesList().size()==0) {
            btn_proceed.setVisibility(View.GONE);
        }

    }


    private void updateQuantity(List<UserServices> list ,List<Service> serviceAvailableData){
        for (int i=0;i<list.size();i++){
            for (int j=0;j<serviceAvailableData.size();j++){
                if (list.get(i).getService_code()==serviceAvailableData.get(j).getServiceCode() ){
                    serviceAvailableData.remove(j);
                }
            }
        }
        if (list.size()==0){
            homeResponseData.getData().getServicesAvailable().get(0).setServices(serviceList);

        }

        if (homeResponseData.getData().getServicesAvailable().get(0).getServices().size()==0){
            ll_remaining_sevice_cart.setVisibility(View.GONE);
        }else              ll_remaining_sevice_cart.setVisibility(View.VISIBLE);

        if (adapter!=null)
            adapter.notifyDataSetChanged();

    }
    private void addServiceData(List<UserServices> list,List<Service> serviceList){
        ArrayList<Integer> serviceCode=new ArrayList<>();
        if (serviceCode.size()>0){
            serviceCode.clear();
        }
        for (int i=0;i<list.size();i++){
            serviceCode.add(list.get(i).getService_code());
        }
        List<Service> service=new ArrayList<>();
        for(int j=0;j<serviceList.size();j++){
            if (!serviceCode.contains(serviceList.get(j).getServiceCode())){

                service.add(serviceList.get(j));
            }
        }

        if (homeResponseData.getData()!=null){
            homeResponseData.getData().getServicesAvailable().get(0).setServices(service);
            adapter = new AdapterRemainingServiceChild(getActivity(),
                    homeResponseData.getData().getServicesAvailable().get(0).getServices() , 0,from_service);
            recycler_view_remaining_services.setAdapter(adapter);
            Log.e("listSize",""+homeResponseData.getData().getServicesAvailable().get(0).getServices().size());

            if (homeResponseData.getData().getServicesAvailable().get(0).getServices().size()==0){
                ll_remaining_sevice_cart.setVisibility(View.GONE);
            }else              ll_remaining_sevice_cart.setVisibility(View.VISIBLE);

            if (adapter!=null)
                adapter.notifyDataSetChanged();
        }

    }

    private void setData(List<UserServices> list){
        double totalPrice=0,menuTotalPrice=0,memberShipPrice=0;
        for (int i=0;i<list.size();i++){
            int q=list.get(i).getQuantity();
            double price=list.get(i).getPrice()*q;
            double menuPrice=list.get(i).getMenu_price()*q;
            if (list.get(i).isFree_service() || list.get(i).isRemainingService() || list.get(i).isMembership() ){
                totalPrice+=0;
                menuTotalPrice+=0;
                if (list.get(i).isMembership()){
                    memberShipPrice+=price;
                }
            }else{

                totalPrice+= price;
                if (menuPrice==0){
                    menuTotalPrice+=price;

                }else{
                    menuTotalPrice+=menuPrice;
                }
            }
        }
        txt_menu_price.setVisibility(View.VISIBLE);
        txt_menu_price.setText(AppConstant.CURRENCY+(int)(menuTotalPrice+memberShipPrice));
        txt_menu_price.setPaintFlags(txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        txt_save_per.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        txt_save_per.setBackgroundResource(R.drawable.discount_seletor);
        if (BeuSalonsSharedPrefrence.getDiscount(getActivity())!=null) {

            for (int i = 0; i < BeuSalonsSharedPrefrence.getDiscount(getActivity()).size(); i++) {

                if (totalPrice >= BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getMin() &&
                        totalPrice < BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getMax()) {
                    txtText.setVisibility(View.VISIBLE);
                    txtText.setText(BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getText());
                    txtPackageDiscount.setVisibility(View.VISIBLE);
                    txtPackageDiscount.setText(BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getDiscountPercent()+"% Package Discount");
                    txtViewValidity.setVisibility(View.VISIBLE);
                    double discountPrice = 0;
                    discountPrice = (totalPrice * BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getDiscountPercent()) / 100;
                    txtViewPrice.setText(AppConstant.CURRENCY +(int) (totalPrice - discountPrice+memberShipPrice));
                    int pos = i + 1;
                    if (BeuSalonsSharedPrefrence.getDiscount(getActivity()).size() > pos) {
                        txtGetMore.setVisibility(View.VISIBLE);
                        String val=BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i + 1).getValidity();

                        txt_view_more.setText("Validity: "+val);
                        double price = (BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i + 1).getMin() - totalPrice);
                        txtGetMore.setText("Upgrade to " + BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i + 1).getDiscountPercent() + "% Package Discount On Adding Services Worth Rs." + AppConstant.CURRENCY + (int)price);

                    } else {
                        txtGetMore.setVisibility(View.GONE);
                        txt_view_more.setVisibility(View.GONE);
                    }
                    double discountPercentage=(1-((totalPrice-discountPrice)/menuTotalPrice))*100;
                    Log.e("discount",""+discountPercentage);
                    txt_save_per.setText(AppConstant.SAVE +" "+ (int)Math.round(discountPercentage)+"%");
                    // txt_save_per.setText(AppConstant.SAVE + " Extra "  + BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getDiscountPercent()+"%");
                    txtViewValidity.setText("Validity: " + BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getValidity());
                    updateDiscountPercentage(i, totalPrice,menuTotalPrice,memberShipPrice);
                } else if (totalPrice < BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(0).getMin()) {
                    txtText.setVisibility(View.VISIBLE);
                    txtViewValidity.setVisibility(View.VISIBLE);
                    txtPackageDiscount.setVisibility(View.GONE);
                    txtText.setText(BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(0).getText());
                    updateDiscountPercentage(0, totalPrice,menuTotalPrice,memberShipPrice);
                }

            }

        }

        setEmptyCart(list);

    }
    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }


    private void setEmptyCart(List<UserServices> list){
        if (list.size()==0){
            linearLayout_price.setVisibility(View.GONE);
            linear_detail.setVisibility(View.GONE);
            linearLayout_top.setVisibility(View.GONE);
            linear_no_item.setVisibility(View.VISIBLE);

        }
        else{
            linearLayout_price.setVisibility(View.VISIBLE);
            linearLayout_top.setVisibility(View.VISIBLE);
            linear_no_item.setVisibility(View.GONE);
        }

    }
    private void updateDiscountPercentage(int i,double totalPrice,double menuTotalPrice,double membershipPrice){
        int q=0;
        for (int j=0;j<list.size();j++){
            q+=list.get(j).getQuantity();
        }
        if ((q<=BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getMinNoOfService())
                && (totalPrice>=BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(0).getExcludeNoOfServiceAmount())){
            linearLayout_price.setVisibility(View.VISIBLE);
            txtText.setVisibility(View.VISIBLE);
            txtViewValidity.setVisibility(View.VISIBLE);
            double discountPrice = 0;
            discountPrice = (totalPrice * BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getDiscountPercent()) / 100;
            txtViewPrice.setText(AppConstant.CURRENCY + (int)(totalPrice - discountPrice+membershipPrice));
            double discountPercentage=(1-((totalPrice-discountPrice)/menuTotalPrice))*100;
            txt_save_per.setText(AppConstant.SAVE +" "+ (int)Math.round(discountPercentage)+"%");

            Log.e("discount",""+discountPercentage);

            // txt_save_per.setText(AppConstant.SAVE + " Extra " +BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getDiscountPercent() + "%");
        }
        else if (q>=BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getMinNoOfService()){
            if (totalPrice<BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getMin()){
                txtText.setVisibility(View.GONE);
                txtViewValidity.setVisibility(View.GONE);

                txtViewPrice.setText(AppConstant.CURRENCY+(int)(totalPrice+membershipPrice));
                double discountPrice = 0;
                discountPrice = (totalPrice * BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getDiscountPercent()) / 100;
                double discountPercentage=(1-((totalPrice-0)/menuTotalPrice))*100;
                txt_save_per.setText(AppConstant.SAVE +" "+ (int)Math.round(discountPercentage)+"%");

               // txt_save_per.setText(AppConstant.SAVE + " 0"+"%");
                double  price =BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getMin()-totalPrice;
                txtGetMore.setText("Upgrade to "+BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getDiscountPercent()+"% Package Discount On Adding Services Worth Rs."+AppConstant.CURRENCY+(int)price);
                String val=BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i ).getValidity();

                txt_view_more.setText("Validity: "+fromHtml(val));

            }
        }
        else{
            txtViewValidity.setText("Validity: " + BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(0).getValidity());
            txtGetMore.setVisibility(View.VISIBLE);
            if (totalPrice>=BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getMin()){
                if (q<BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getMinNoOfService()){
                    String val=BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i ).getValidity();

                    txt_view_more.setText("Validity: "+fromHtml(val));
                    txtGetMore.setText("Upgrade to "+BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getDiscountPercent()+"% Package Discount On Adding "+(BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getMinNoOfService()-q)+" More Service ");
                    txtViewPrice.setText(AppConstant.CURRENCY+(int)(totalPrice+membershipPrice));
                    double discountPrice = 0;
                    discountPrice = (totalPrice * BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getDiscountPercent()) / 100;
                    double discountPercentage=(1-((totalPrice-0)/menuTotalPrice))*100;
                    txt_save_per.setText(AppConstant.SAVE +" "+ (int)Math.round(discountPercentage)+"%");
                 //   txt_save_per.setText(AppConstant.SAVE + " 0"  + "%");
                    txtText.setVisibility(View.GONE);
                    txtViewValidity.setVisibility(View.GONE);

                }



            }
            else {
                if (q<BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getMinNoOfService()){
                    String val=BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i ).getValidity();

                    txt_view_more.setText("Validity: "+fromHtml(val));

                    double  price =BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getMin()-totalPrice;
                    txtGetMore.setText("Upgrade to "+BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getDiscountPercent()+"% Package Discount On Adding "+(BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getMinNoOfService()-q)+" More Service ");
                    txtViewPrice.setText(AppConstant.CURRENCY+(int)(totalPrice+membershipPrice));
                    double discountPercentage=(1-((totalPrice-0)/menuTotalPrice))*100;
                    txt_save_per.setText(AppConstant.SAVE +" "+ (int)Math.round(discountPercentage)+"%");
                   // txt_save_per.setText(AppConstant.SAVE + " 0" + "%");
                    txtText.setVisibility(View.GONE);
                    txtViewValidity.setVisibility(View.GONE);


                }
                else if (totalPrice<BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getMin()){
                    txtText.setVisibility(View.GONE);
                    txtViewValidity.setVisibility(View.GONE);
                    txtViewPrice.setText(AppConstant.CURRENCY+(int)(totalPrice+membershipPrice));

                    double discountPercentage=(1-((totalPrice-0)/menuTotalPrice))*100;
                    txt_save_per.setText(AppConstant.SAVE +" "+ (int)Math.round(discountPercentage)+"%");

                    String val=BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i ).getValidity();
                    txt_view_more.setText("Validity: "+fromHtml(val));
                    double  price =BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getMin()-totalPrice;
                    txtGetMore.setText("Upgrade to "+BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(i).getDiscountPercent()+"% Package Discount On Adding Services Worth Rs."+AppConstant.CURRENCY+(int)price);
                }

            }

        }

        if (totalPrice==0){
            txtViewValidity.setText("Validity: " + BeuSalonsSharedPrefrence.getDiscount(getActivity()).get(0).getValidity());
            txtText.setVisibility(View.GONE);
            txtViewValidity.setVisibility(View.GONE);

            txtGetMore.setVisibility(View.GONE);
            txt_view_more.setVisibility(View.GONE);

            txtViewPrice.setText(AppConstant.CURRENCY+(totalPrice+membershipPrice));
            txt_save_per.setText(AppConstant.SAVE + " 0" + "%");

            //in case of price is 0
            txtViewPrice.setVisibility(View.GONE);
            txt_save_per.setVisibility(View.GONE);
            txt_menu_price.setVisibility(View.GONE);
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            Window window = getDialog().getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f1f2f2")));
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final List<UserServices> event) {
        /* Do something */

        try {
            updateAddData();
        }catch (Exception e){
            e.printStackTrace();
        }
        //setData(event);


        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
               if (serviceList!=null && serviceList.size()>0)
                addServiceData(event,serviceList);

            }
        });

    }
    public String formatDateTime(String str_date){

        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss");
        Date date_format=null;
        try {
            date_format = sdf.parse(str_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("MMM dd yyyy, EEEE HH:mm aa").format(date_format);
    }
    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        Log.e("onStop","onStop");
       /* if (tooltip != null && tooltip.isShowing()) {
            tooltip.dismiss();
        }*/
    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if(view!=null && isVisibleToUser){
//            tooltip=new SimpleTooltip.Builder(getActivity())
//                    .anchorView(txt_view_more)
//                    .gravity(Gravity.BOTTOM)
//                    .animated(true)
//                    .transparentOverlay(true)
//                    .dismissOnInsideTouch(true)
//                    .dismissOnOutsideTouch(true)
//                    .contentView(R.layout.tooltip_cart)
//                    .build();
//            tooltip .show();
//        }else{
//            if (tooltip != null && tooltip.isShowing()) {
//                tooltip.dismiss();
//            }
//        }
//
//
//    }

    //    public void clearCart(){
//        try{
//            DB snappyDB = DBFactory.open(getActivity()); //create or open an existing database using the default name
//            if(snappyDB.exists(AppConstant.USER_CART_DB)){
//
//                snappyDB.destroy();
//                list.clear();
//                adapter.setList(list);
//            }else{
//                snappyDB.close();
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }

//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setHasOptionsMenu(true);
//    }
//
//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.delete, menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if(id == R.id.action_delete){
//
//            //clearing the cart
//            clearCart();
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
}
