package com.beusalons.android.Model.subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Tile {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("heading1")
    @Expose
    private String heading1;
    private String heading1HTML;
    private String heading2HTML;
    @SerializedName("heading2")
    @Expose
    private String heading2;
    @SerializedName("points")
    @Expose
    private List<String> points = null;
    @SerializedName("recommended")
    @Expose
    private String recommended;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    private boolean isSelect;
    private int subscriptionId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHeading1() {
        return heading1;
    }

    public void setHeading1(String heading1) {
        this.heading1 = heading1;
    }

    public String getHeading2() {
        return heading2;
    }

    public void setHeading2(String heading2) {
        this.heading2 = heading2;
    }

    public List<String> getPoints() {
        return points;
    }

    public void setPoints(List<String> points) {
        this.points = points;
    }

    public String getRecommended() {
        return recommended;
    }

    public void setRecommended(String recommended) {
        this.recommended = recommended;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public int getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(int subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getHeading1HTML() {
        return heading1HTML;
    }

    public void setHeading1HTML(String heading1HTML) {
        this.heading1HTML = heading1HTML;
    }

    public String getHeading2HTML() {
        return heading2HTML;
    }

    public void setHeading2HTML(String heading2HTML) {
        this.heading2HTML = heading2HTML;
    }
}

