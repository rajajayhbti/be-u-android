package com.beusalons.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.beusalons.android.Adapter.HomePageAdapter;
import com.beusalons.android.Dialog.CustomizeLogOut;
import com.beusalons.android.Event.CartNotificationEvent;
import com.beusalons.android.Event.FiltersEvent;
import com.beusalons.android.Event.HomeAddressChangedEvent;
import com.beusalons.android.Event.SortFilterEvent;
import com.beusalons.android.Fragment.SalonFilterFragment;
import com.beusalons.android.Fragment.UpdateAppScreen.UpdateAppScreenFragment;
import com.beusalons.android.Fragment.UserCartFragment;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.FireBaseResponse;
import com.beusalons.android.Model.FirebaseModel.FireBaseIdPost;
import com.beusalons.android.Model.HomeFragmentModel.DiscountRules;
import com.beusalons.android.Model.HomePageBottomSheetModel;
import com.beusalons.android.Model.Loyalty.LoyaltyPointsRequest;
import com.beusalons.android.Model.Loyalty.LoyaltyPointsResponse;
import com.beusalons.android.Model.Notifications.NotiFicationResponse;
import com.beusalons.android.Model.Notifications.NotificationPost;
import com.beusalons.android.Model.Notifications.Notifications;
import com.beusalons.android.Model.Reviews.WriteReviewPost;
import com.beusalons.android.Model.Reviews.WriteReviewResponse;
import com.beusalons.android.Model.SalonFilter.FilterResponse;
import com.beusalons.android.Model.UpdateAppResponse;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserIDAccessTokenPost;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Service.FetchMembership;
import com.beusalons.android.Service.LocationUpdateService;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import net.steamcrafted.materialiconlib.MaterialDrawableBuilder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    protected com.beusalons.android.Application nMyApplication;
    private final String UPDATE_SCREEN_FRAGMENT="com.beusalons.updatescreenstuff";
    private final String BOTTOMSHEET_DIALOG_FRAGMENT= "com.beusalons.bottomsheet.home";

    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String LOCATION_ACTION="com.beusalons.android.MainActivity";
    private IntentFilter locationFilter;
    private LocationReciver locationReciver;

    public static boolean isFreebieHome= false,isDealsHome=false;
    //left nav views instantiation
    private ImageView img_navHeader;
    private TextView txt_navName;
    private String regId= "";
    private FireBaseIdPost fireBaseIdPost;
    private HomePageAdapter adapter;
    public List<DiscountRules>   discountArrayList=new ArrayList<>();

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    //    ImageView img_shoping;
    private TextView txt_filter;

    public static AHBottomNavigation bottomNavigation;

    private boolean doubleBackToExitPressedOnce = false;

    private UserCart user_cart;

    private ImageView imgHeaderTitle;
    private static int retry = 0;
    public static AHBottomNavigation getBottomNav(){
        return bottomNavigation;
    }

    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 200;

    private TextView txt_location_name;

    private String parlorId;
    private int user_rating=5;
    private AppEventsLogger logger;
    private int retry_submit=0;
    private DB snappyDB = null;
    private FirebaseAnalytics mFirebaseAnalytics;

    private Runnable runnable;
    private Handler handler;
    private String filters_rating= null, filters_price= null, filters_gender= null, filters_sort= "0";

    public List<DiscountRules> getDiscountArrayList() {
        return discountArrayList;
    }

    public void setDiscountArrayList(List<DiscountRules> discountArrayList) {
        this.discountArrayList = discountArrayList;
    }

    //yeh filters ka stuff hai
    private static final String FILTERS="filters";
    private boolean isMale= false, isFemale= false, isUnisex= false;
    private boolean isPopularity= false, isPriceLow= false, isPriceHigh= false, isNearest= true;
    private boolean isRating_1= false, isRating_2= false, isRating_3= false, isRating_4= false;
    private boolean isPrice_red= false, isPrice_blue= false, isPrice_green=false, isPrice_all=false;

    private CardView card_container;

    private FilterResponse filterResponse;
    private LinearLayout linearSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nMyApplication = (com.beusalons.android.Application) getApplication();
        nMyApplication.onActivityCreated(this, savedInstanceState);
//        getApplication().registerActivityLifecycleCallbacks(new MyActivityLifecycleCallbacks(this));
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(MainActivity.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        if(getSupportActionBar()!=null)
            getSupportActionBar().setTitle(null);
        bottomNavigation= (AHBottomNavigation)findViewById(R.id.bottom_navigation);

        viewPager= findViewById(R.id.view_pager);
//        img_shoping=findViewById(R.id.img_shoping);

        logger = AppEventsLogger.newLogger(MainActivity.this);
        bottomNavigation.setForceTint(true);
        //icons active color
        bottomNavigation.setAccentColor(Color.parseColor("#d2232A"));
        initBottomBar();
        imgHeaderTitle= (ImageView) findViewById(R.id.img_down_arrow);
        txt_location_name= (TextView)findViewById(R.id.txt_location);
        linearSearch=findViewById(R.id.linear_search);

        linearSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openLocationIntent();

            }
        });
        txt_location_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openLocationIntent();
            }
        });
        if (BeuSalonsSharedPrefrence.getAddressLocalty()!=null)
            txt_location_name.setText(BeuSalonsSharedPrefrence.getAddressLocalty());

        card_container= findViewById(R.id.card_container);

        fetchNotificationData();
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        View navView_header =  navigationView.getHeaderView(0);  //get navigation header
        img_navHeader= (ImageView) navView_header.findViewById(R.id.img_navHeader);
        img_navHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                drawer.closeDrawer(Gravity.LEFT);
                bottomNavigation.setCurrentItem(4);
            }
        });
        Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/Lato-Medium.ttf");

        txt_navName= (TextView)navView_header.findViewById(R.id.txt_navName);
        txt_navName.setTypeface(typeFace);

        navigationView.setNavigationItemSelectedListener(this);


        adapter = new HomePageAdapter(getSupportFragmentManager(), MainActivity.this);


        viewPager.post(new Runnable() {
            public void run() {
                viewPager.setAdapter(adapter);
            }
        });
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        // printKeyHash(activity);


//        img_shoping.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                UserCartFragment fragment= new UserCartFragment();
//                Bundle bundle= new Bundle();
//                bundle.putBoolean("has_data", true);
//                bundle.putBoolean(UserCartFragment.SHOW_PROCEED, true);
//                fragment.setArguments(bundle);
//                fragment.show(getSupportFragmentManager(), "user_cart");
//            }
//        });


        //updateapp ke images



        fetchFilterData(false);


        txt_filter= findViewById(R.id.txt_filter);
        txt_filter.setVisibility(View.VISIBLE);
        txt_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(filterResponse!=null &&
                        filterResponse.getData()!=null &&
                        filterResponse.getData().getBrands()!=null &&
                        filterResponse.getData().getBrands().size()>0){


                    SalonFilterFragment fragment= new SalonFilterFragment();
                    Bundle bundle= new Bundle();
                    bundle.putString(SalonFilterFragment.DATA, new Gson().toJson(filterResponse));
                    bundle.putString(SalonFilterFragment.SORT, filters_sort);
                    bundle.putString(SalonFilterFragment.CATEGORY, filters_price);
                    bundle.putString(SalonFilterFragment.RATING, filters_rating);
                    fragment.setArguments(bundle);
                    fragment.show(getSupportFragmentManager(),FILTERS );

                }else{
                    fetchFilterData(true);

                }




//                FilterFragment filterFragment= new FilterFragment();
//                Bundle bundle= new Bundle();
//
//                //------------------sort----------------------------------
//                bundle.putBoolean("isPopularity", isPopularity);
//                bundle.putBoolean("isPriceLow", isPriceLow);
//                bundle.putBoolean("isPriceHigh", isPriceHigh);
//                bundle.putBoolean("isNearest", isNearest);
//
//                //--------------------------filters-----------------------
//                bundle.putBoolean("isMale", isMale);
//                bundle.putBoolean("isFemale", isFemale);
//                bundle.putBoolean("isUnisex", isUnisex);
//
//                bundle.putBoolean("isRating_1", isRating_1);
//                bundle.putBoolean("isRating_2", isRating_2);
//                bundle.putBoolean("isRating_3", isRating_3);
//                bundle.putBoolean("isRating_4", isRating_4);
//
//                bundle.putBoolean("isPrice_red", isPrice_red);
//                bundle.putBoolean("isPrice_blue", isPrice_blue);
//                bundle.putBoolean("isPrice_green", isPrice_green);
//                bundle.putBoolean("isPrice_all", isPrice_all);
//
//                filterFragment.setArguments(bundle);
//                filterFragment.show(getSupportFragmentManager(),FILTERS );
            }
        });


        /**
         * BroadcastReciver
         */
        locationFilter=new IntentFilter(LOCATION_ACTION);
        locationReciver=new LocationReciver();
        startService(new Intent(MainActivity.this, FetchMembership.class));
        //initialize the appsflyers
        //  AppsFlyerLib.getInstance().startTracking(this.getApplication(), getResources().getString(R.string.devKey));

        //initialize the imageview in left navigation drawer
//        if(!BeuSalonsSharedPrefrence.getFirebaseSuccess())
        postFirBaseRegId();
//        int versionCode=0;
//        try {
//            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
//            String version = pInfo.versionName;
//             versionCode=pInfo.versionCode;
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
        if (BuildConfig.VERSION_CODE>BeuSalonsSharedPrefrence.getVersionCode()){
//            fetchBottomSheetData(this);
            fetchUpdateScreenData();

        }else{
            if (BeuSalonsSharedPrefrence.getIsSubscribed()){
                setSubscriptionPopUp();
            }else{
                setSmartPopUp();
            }
        }
        fetchBottomSheetData(MainActivity.this);
        Bundle bundle= getIntent().getExtras();

        if(bundle!=null){

            if(!bundle.isEmpty()){

                Log.i("trackthis", "i'm i n bundle mai now main activity ke");

                final String appt_id= bundle.getString("apptId");
                int type= bundle.getInt("type");

                if(type==5){            //review ka case hai yeh

                    Bundle bundle_= new Bundle();
                    bundle_.putString(CustomerReviewActivity.PARLOR_NAME, "");
                    bundle_.putString(CustomerReviewActivity.PARLOR_ID, "");
                    bundle_.putString(CustomerReviewActivity.APPOINTMENT_ID, appt_id);
                    Intent intent= new Intent(MainActivity.this, CustomerReviewActivity.class);
                    intent.putExtras(bundle_);
                    startActivity(intent);

                }else if(type==3){                 //freebie ka case hai yeh

                    Log.i("mainyahahoon", "type 3 pe ");

                    Runnable runnable= new Runnable() {
                        @Override
                        public void run() {
                            bottomNavigation.setCurrentItem(2);
                        }
                    };
                    new Handler().postDelayed(runnable, 1000);

                }else if (type==2){
                    Runnable runnable= new Runnable() {
                        @Override
                        public void run() {
                            bottomNavigation.setCurrentItem(0);
                        }
                    };
                    new Handler().postDelayed(runnable, 1000);
                }else if (type==4){

                    Runnable runnable= new Runnable() {
                        @Override
                        public void run() {
                            UserCartFragment fragment= new UserCartFragment();
                            Bundle bundle= new Bundle();
                            bundle.putBoolean("has_data", true);
                            bundle.putBoolean(UserCartFragment.SHOW_PROCEED, true);
                            bundle.putBoolean(UserCartFragment.FROM_HOME_HAS_SALONS, true);
                            fragment.setArguments(bundle);
                            fragment.show(getSupportFragmentManager(), "user_cart");
                            //  bottomNavigation.setCurrentItem(3);
                        }
                    };
                    new Handler().postDelayed(runnable, 1000);
                }else if (type==1){

                    Runnable runnable= new Runnable() {
                        @Override
                        public void run() {
                            bottomNavigation.setCurrentItem(1);
                        }
                    };
                    new Handler().postDelayed(runnable, 1000);
                }else if (type==6){

                    Runnable runnable= new Runnable() {
                        @Override
                        public void run() {
                            bottomNavigation.setCurrentItem(4);
                        }
                    };
                    new Handler().postDelayed(runnable, 1000);
                }else if (type==7){

                    Runnable runnable= new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.beusalons.android"));
                            startActivity(i);

                            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        }
                    };
                    new Handler().postDelayed(runnable, 1000);
                }else if (type==8){
                    final String parlorID= bundle.getString("parlorId");
                    Runnable runnable= new Runnable() {
                        @Override
                        public void run() {
                            Intent intent=new Intent(MainActivity.this,SalonPageActivity.class);
                            intent.putExtra("parlorId",parlorID);
                            startActivity(intent);
                        }
                    };
                    new Handler().postDelayed(runnable, 1000);
                }
                else{
                    Runnable runnable= new Runnable() {
                        @Override
                        public void run() {
                            bottomNavigation.setCurrentItem(0);
                        }
                    };
                    new Handler().postDelayed(runnable, 1000);
                }
            }

        }

        SharedPreferences preferences= getSharedPreferences("userDetails", Context.MODE_PRIVATE);

        if(preferences!=null) {
            String name= preferences.getString("name", "Name");
            String profilePic= preferences.getString("profilePic", "?");
            txt_navName.setText(name);

            try{
                Glide.with(this).load(profilePic).apply(RequestOptions.circleCropTransform()).into(img_navHeader);

              /*  Glide.with(this)
                        .load(profilePic).asBitmap().centerCrop()
                        .into(new BitmapImageViewTarget(img_navHeader){
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                img_navHeader.setImageDrawable(circularBitmapDrawable);
                            }
                        });*/
            }catch (Exception e){
                e.printStackTrace();
            }

        }

        Log.e("macAddress",""+getMacAddr());
        if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            startService(new Intent(MainActivity.this, LocationUpdateService.class));
          /*  */
            return;
        }

    }



    public void setBottomFromOutSide(int pos){
        bottomNavigation.setCurrentItem(pos);
    }


    private int filter_retry=0;
    private void fetchFilterData(final boolean openPage){

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<FilterResponse> call= apiInterface.getFilterData();
        call.enqueue(new Callback<FilterResponse>() {
            @Override
            public void onResponse(Call<FilterResponse> call, Response<FilterResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().getSuccess()){
                        Log.i("filterstuff", "on success");

                        filter_retry=0;
                        filterResponse= response.body();

                        if(openPage)
                            txt_filter.performClick();


                    }else{
                        Log.i("filterstuff", "on not successful");
                        if(filter_retry<3){
                            filter_retry++;
                            fetchFilterData(openPage);
                        }
                    }


                }else{
                    Log.i("filterstuff", "on else");
                    if(filter_retry<3){
                        filter_retry++;
                        fetchFilterData(openPage);
                    }
                }
            }

            @Override
            public void onFailure(Call<FilterResponse> call, Throwable t) {
                Log.i("filterstuff", "on failure: "+ t.getCause()+ " "+t.getStackTrace());
                if(filter_retry<3){
                    filter_retry++;
                    fetchFilterData(openPage);
                }

            }
        });


    }


    private void fetchBottomSheetData(final Context context){

        UserIDAccessTokenPost post= new UserIDAccessTokenPost();
        post.setUserId(BeuSalonsSharedPrefrence.getUserId()) ;
        post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
//        post.setType(1);
        post.setVersionAndroid(BuildConfig.VERSION_NAME);

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<HomePageBottomSheetModel> call= apiInterface.getHomeBottomSheetData(post);
        call.enqueue(new Callback<HomePageBottomSheetModel>() {
            @Override
            public void onResponse(Call<HomePageBottomSheetModel> call, final Response<HomePageBottomSheetModel> response) {

                if(response.isSuccessful()){

                    if(response.body().isSuccess()){
                        Log.i("homepagebottomsheet", "in the success");


                        card_container.removeAllViews();

                        LinearLayout linear_ = (LinearLayout) LayoutInflater.from(context)
                                .inflate(R.layout.home_bottomsheet_update, null);

                        TextView txt_title= linear_.findViewById(R.id.txt_title);
                        txt_title.setText(response.body().getData().getHeading1());
                        LinearLayout linear_cancel= linear_.findViewById(R.id.linear_cancel);
                        linear_cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                card_container.setVisibility(View.GONE);
                            }
                        });

                        if(response.body().getData().getType()!=null &&
                                response.body().getData().getType().equalsIgnoreCase("review")){

                            LinearLayout linear_review= linear_.findViewById(R.id.linear_review);
                            linear_review.setVisibility(View.VISIBLE);
                            linear_review.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    card_container.setVisibility(View.GONE);
                                    Bundle bundle_= new Bundle();
                                    bundle_.putString(CustomerReviewActivity.PARLOR_NAME, "");
                                    bundle_.putString(CustomerReviewActivity.PARLOR_ID,
                                            response.body().getData().getParlorId());
                                    bundle_.putString(CustomerReviewActivity.APPOINTMENT_ID,
                                            response.body().getData().getAppointmentId());
                                    Intent intent= new Intent(MainActivity.this, CustomerReviewActivity.class);
                                    intent.putExtras(bundle_);
                                    startActivity(intent);
                                }
                            });


                        }else{

                            RelativeLayout relative_= linear_.findViewById(R.id.relative_);
                            relative_.setVisibility(View.VISIBLE);

                            TextView txt_description= linear_.findViewById(R.id.txt_description);
                            txt_description.setText(response.body().getData().getDescription());
                            TextView txt_action= linear_.findViewById(R.id.txt_action);
                            txt_action.setText(response.body().getData().getButtonText());
                            txt_action.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    if(response.body().getData().getType()!=null &&
                                            response.body().getData().getType().equalsIgnoreCase("update")){
                                        card_container.setVisibility(View.GONE);
                                        Intent i = new Intent(Intent.ACTION_VIEW);
                                        i.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.beusalons.android"));
                                        startActivity(i);

                                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                        try {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                        } catch (android.content.ActivityNotFoundException anfe) {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                        }

                                    }else if(response.body().getData().getType()!=null &&
                                            response.body().getData().getType().equalsIgnoreCase("showFeatures")){

                                        fetchUpdateScreenData();
                                        card_container.setVisibility(View.GONE);

                                    }else if(response.body().getData().getType()!=null &&
                                            response.body().getData().getType().equalsIgnoreCase("goToUrl")){
                                        card_container.setVisibility(View.GONE);
                                        Intent in = new Intent(view.getContext(),WebViewActivity.class);
                                        in.putExtra("url",
                                                response.body().getData().getUrl());
                                        in.putExtra("title",
                                                response.body().getData().getHeading1());
                                        startActivity(in);

                                    }else if(response.body().getData().getType()!=null &&
                                            response.body().getData().getType().equalsIgnoreCase("preference")){
                                        card_container.setVisibility(View.GONE);
                                        startActivity(new Intent(view.getContext(), AboutUserActivity.class));
                                    }else if(response.body().getData().getType()!=null &&
                                            response.body().getData().getType().equalsIgnoreCase("earnCoupon")){
                                        card_container.setVisibility(View.GONE);
                                        bottomNavigation.setCurrentItem(2);
                                    }

                                    else{
                                        card_container.setVisibility(View.GONE);
                                    }


                                }
                            });

                        }

                        card_container.setVisibility(View.VISIBLE);
                        card_container.addView(linear_);







                    }else{
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                if (!MainActivity.this.isFinishing())
//                                    setToolTip(bottomNavigation.getViewAtPosition(1));
//                            }
//                        },100);
                        Log.i("homepagebottomsheet", "in the not success");
                    }
                }else{
                    Log.i("homepagebottomsheet", "in the else");
                }

            }

            @Override
            public void onFailure(Call<HomePageBottomSheetModel> call, Throwable t) {
                Log.i("homepagebottomsheet", "in the failure: " + t.getStackTrace()+  " "+ t.getCause());
            }
        });

    }



    public void fetchUpdateScreenData(){

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        if (!isFinishing()) {
            Call<UpdateAppResponse> call = apiInterface.getData();

            call.enqueue(new Callback<UpdateAppResponse>() {
                @Override
                public void onResponse(Call<UpdateAppResponse> call, Response<UpdateAppResponse> response) {

                    if (response.isSuccessful()) {
                        if (response.body().isSuccess()) {
                            Log.i("updateAppImages", "in the success");
                            BeuSalonsSharedPrefrence.setVersionCode(BuildConfig.VERSION_CODE);

                            try {
                                UpdateAppScreenFragment fragment = new UpdateAppScreenFragment();
                                Bundle bundle = new Bundle();
                                bundle.putString(UpdateAppScreenFragment.DATA, new Gson().toJson(response.body(), UpdateAppResponse.class));
                                fragment.setArguments(bundle);
                                fragment.show(getSupportFragmentManager(), UPDATE_SCREEN_FRAGMENT);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } else {
                            Log.i("updateAppImages", "in the not success");
                        }
                    } else {
                        Log.i("updateAppImages", "in the else");
                    }


                }

                @Override
                public void onFailure(Call<UpdateAppResponse> call, Throwable t) {
                    Log.i("updateAppImages", "in the failure: " + t.getCause() + " " + t.getStackTrace());
                }
            });
        }
    }



//    private void setToolTip(View tip){
//        View view = getLayoutInflater().inflate(R.layout.deal_tooltip, null);
//        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT );
//        view.setLayoutParams(params);
//        new EasyDialog(MainActivity.this)
//                // .setLayoutResourceId(R.layout.layout_tip_content_horizontal)//layout resource id
//                .setLayout(view)
//                .setBackgroundColor(getResources().getColor(R.color.tooltip_bg))
//                // .setLocation(new location[])//point in screen
//                .setLocationByAttachedView(tip)
//                .setGravity(EasyDialog.GRAVITY_TOP)
//
//                .setTouchOutsideDismiss(true)
//                .setOutsideColor(getResources().getColor(android.R.color.transparent))
//                .setMatchParent(true)
//                .setMarginLeftAndRight(64, 96)
//                .show();
//
//    }
    private void submitReview(final String userId, final String accessToken, final String parlorId, final String appointmentId, final String txt,
                              final int rating){

        WriteReviewPost writeReviewPost= new WriteReviewPost();

        writeReviewPost.setRating(rating);
        writeReviewPost.setUserId(userId);
        writeReviewPost.setAccessToken(accessToken);
        writeReviewPost.setParlorId(parlorId);
        writeReviewPost.setAppointmentId(appointmentId);
        writeReviewPost.setText(txt);

        Log.i("writereview", "value in stuff:--rating "+writeReviewPost.getRating()+ "user id: "+ writeReviewPost.getUserId()+
                " accesstoken : " + writeReviewPost.getAccessToken()+ " parlor id: "+ writeReviewPost.getParlorId()+
                " appt id: "+writeReviewPost.getAppointmentId()+ "  get text: "+ writeReviewPost.getText());

        Retrofit retrofit2 = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit2.create(ApiInterface.class);
        Call<WriteReviewResponse> call= apiInterface.writeReview(writeReviewPost);
        call.enqueue(new Callback<WriteReviewResponse>() {
            @Override
            public void onResponse(Call<WriteReviewResponse> call, Response<WriteReviewResponse> response) {

                try{

                    if(response.isSuccessful()){

                        if(response.body().getSuccess()){

                            WriteReviewResponse reviewResponse= response.body();

                            Log.i(TAG, "I'm in onResponse submit issuccesful ");
                            if(reviewResponse.getSuccess()){

                                Log.i(TAG, "I'm in onResponse submit: yeh to chal parah");

                                Toast.makeText(MainActivity.this, "Review Posted Successfully", Toast.LENGTH_SHORT).show();

                            }

                        }else{

                            Log.i(TAG, "I'm in onResponse failure submit pe");

                        }


                    }else{
                        Log.i(TAG, "I'm in retrofit failure submit pe");
                    }


                }catch (Exception e){
                    Log.i(TAG, "I'm in catch pe submit response pe");

                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WriteReviewResponse> call, Throwable t) {

                if(retry_submit<2){

                    submitReview(userId, accessToken, parlorId, appointmentId, txt, rating);
                    retry_submit++;
                }

                Log.i(TAG, "I'm in onFailure submit pe");
            }
        });
    }

    /**
     *  to set Header title text fron any where in activity or fragments
     * @param title
     */

    public void setTitleHeader(String title){
//       txt_location_name.setText(title);

        if(title.equalsIgnoreCase("home") || title.equalsIgnoreCase("deals")){
            imgHeaderTitle.setVisibility(View.VISIBLE);
            txt_location_name.setText(BeuSalonsSharedPrefrence.getAddressLocalty());
        }else{
            imgHeaderTitle.setVisibility(View.GONE);
            txt_location_name.setText(title);
        }


    }

    private void initBottomBar() {

        // Create items
        Drawable homeIcon = getBottomIcon(MaterialDrawableBuilder.IconValue.HOME);
        Drawable coupon = getResources().getDrawable( R.drawable.coupon_unselected );
        Drawable cartIcon = getBottomIcon(MaterialDrawableBuilder.IconValue.CART);
        Drawable profileIcon = getBottomIcon(MaterialDrawableBuilder.IconValue.ACCOUNT_CIRCLE);
        Drawable loyaltyIcon = getBottomIcon(MaterialDrawableBuilder.IconValue.DATABASE);
        Drawable dealsIcon = getBottomIcon(MaterialDrawableBuilder.IconValue.TAG);

        AHBottomNavigationItem home = new AHBottomNavigationItem(getResources().getString(R.string.home_tab), homeIcon);
        AHBottomNavigationItem deals = new AHBottomNavigationItem(getResources().getString(R.string.deals_tab), dealsIcon);
        AHBottomNavigationItem loyalty = new AHBottomNavigationItem(getResources().getString(R.string.loyalty_tab), loyaltyIcon);
        AHBottomNavigationItem cart = new AHBottomNavigationItem(getResources().getString(R.string.cart_tab), coupon);
        AHBottomNavigationItem profile = new AHBottomNavigationItem(getResources().getString(R.string.profile_tab), profileIcon);

        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        bottomNavigation.addItem(home);
        bottomNavigation.addItem(deals);
        bottomNavigation.addItem(loyalty);
        bottomNavigation.addItem(cart);
        bottomNavigation.addItem(profile);

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {

                switch (position){
                    case 0:

                        Log.i("fadlkjfldasjk", "i'm in  0");
                        setTitleHeader("home");
                        txt_filter.setVisibility(View.VISIBLE);

                        break;
                    case 1:

                        Log.i("fadlkjfldasjk", "i'm in  1");
                        if (!isDealsHome){
                            logDealsPageFireBaseEvent();
                            logDealsPageEvent();
                            Log.i("fadlkjfldasjk", "ab toh yeh masst chal raha hai");
                        }

                        setTitleHeader("Deals Across Salons");
                        isDealsHome=false;
                        txt_filter.setVisibility(View.GONE);
                        break;
                    case 2:

                        Log.i("fadlkjfldasjk", "i'm in  2");
                        setTitleHeader("My Freebies");
                        if(!isFreebieHome){
                            logFreebiesPageFireBaseEvent();
                            logFreebiesPageEvent();
                            Log.i("fadlkjfldasjk", "ab toh yeh masst chal raha hai");
                        }
//                        else{
//                            Log.i("fadlkjfldasjk", "yeh ab user ne niche click kara hai");
//
//
//                        }
                        isFreebieHome= false;
                        txt_filter.setVisibility(View.GONE);
                        break;
                    case 3:
                        Log.i("fadlkjfldasjk", "i'm in  3");
                        setTitleHeader("Coupons");
                        UserCartFragment.show_popup= true;
                        txt_filter.setVisibility(View.GONE);
                        break;
                    case 4:

                        Log.i("fadlkjfldasjk", "i'm in  4");
                        setTitleHeader("My Profile");
                        txt_filter.setVisibility(View.GONE);
                        break;
                }
                Log.i("margayamai", "on viewpager selected");

                try{

                    viewPager.setCurrentItem(position, true);
                }catch (Exception e){
                    e.printStackTrace();
                }

                return true;
            }
        });



    }
    public void logFreebiesPageEvent () {
        Log.e("FreebiesPageFooter","fine");
        logger.logEvent(AppConstant.FreebiesPage);
    }

    public void logDealsPageEvent () {
        Log.e("DealsPageFooter","fine");
        logger.logEvent(AppConstant.DealsPage);
    }
    public void logFreebiesPageFireBaseEvent () {
        Log.e("FreebiesPageFooterFire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.FreebiesPage,bundle);
    }
    public void logDealsPageFireBaseEvent () {
        Log.e("DealsPageFooterfirbase","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics .logEvent(AppConstant.DealsPage,bundle);
    }
    @Override
    public void onBackPressed() {
        if(this.bottomNavigation.getCurrentItem() != 0)
        {
            this.bottomNavigation.setCurrentItem(0);
        }else {

            if (doubleBackToExitPressedOnce) {
                finish();
                return;
            }
            doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 3000);
        }
    }

    public  void refreshCartNotification(){

        user_cart=new UserCart();
        try {
            snappyDB = DBFactory.open(this);
            if (snappyDB.exists(AppConstant.USER_CART_DB))
                user_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
            else
                bottomNavigation.setNotification("",3);
            snappyDB.close();

            if(user_cart!=null && user_cart.getServicesList().size()>0){

                int quantity=0;
                if(user_cart.getServicesList().size()==1){

                    quantity= user_cart.getServicesList().get(0).getQuantity();
                    //       if(quantity==1)
                    //         bottomNavigation.setNotification(""+quantity, 3);
                    //        else
                    //   //        bottomNavigation.setNotification(""+quantity, 3);
                } else if(user_cart.getServicesList().size()>1){

                    for(int i=0;i<user_cart.getServicesList().size();i++){

                        quantity+= user_cart.getServicesList().get(i).getQuantity();
                    }

                    //        bottomNavigation.setNotification(""+quantity, 3);
                }                                                   //0 nai dikhana
            }                                                           //0 nai dikhana

        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        /*if (id == R.id.nav_referEarn) {


        }*/ /*if (id == R.id.nav_rateReview) {

            Intent i = new Intent(android.content.Intent.ACTION_VIEW);
            i.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.beusalons.android"));
            startActivity(i);

            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }


        } else*/ if (id == R.id.nav_aboutBeU) {

            startActivity(new Intent(this, AboutUs.class));

        }else if(id == R.id.nav_cart){

            UserCartFragment fragment= new UserCartFragment();
            Bundle bundle= new Bundle();
            bundle.putBoolean("has_data", true);
            bundle.putBoolean(UserCartFragment.SHOW_PROCEED, true);
            bundle.putBoolean(UserCartFragment.FROM_HOME_HAS_SALONS, true);
            fragment.setArguments(bundle);
            fragment.show(getSupportFragmentManager(), "user_cart");
        } else if (id == R.id.nav_contactUs) {

            startActivity(new Intent(this, ContactUsActivity.class));

        } else if (id == R.id.nav_fav_salons) {

            Intent in= new  Intent(this, ParlorListActivity.class);
            in.putExtra("isFavouriteSalon", true);
            startActivity(in);

        }
        else if(id==R.id.nav_family_wallet){
            Intent in=new Intent(this, MemberShipCardAcitvity.class);
            in.putExtra("from_home", true);
            startActivity(in);
        }
        else if (id == R.id.nav_refer_friend) {

            Intent in=new  Intent(this, ReferAFriendActivity.class);
            startActivity(in);

        }else if (id == R.id.nav_notification) {
//
            Intent in=new  Intent(this, NotificationsActivity.class);
            startActivity(in);


        }else if(id == R.id.nav_signOut){

            Log.i(TAG, "i'm in signOout");

            new CustomizeLogOut(MainActivity.this);
        }
        else if(id == R.id.nav_t_n_c){

            Log.i(TAG, "i'm in signOout");
            Intent in = new Intent(this,WebViewActivity.class);
            in.putExtra("url", "http://beusalons.com/appTermsConditions");
            in.putExtra("title", "Terms & Conditions");
            startActivity(in);

        }else if (id == R.id.nav_faq) {

            Intent in = new Intent(this,WebViewActivity.class);
            in.putExtra("url", "http://beusalons.com/appfaq");
            in.putExtra("title", "Frequently Asked Questions");
            startActivity(in);

        }else if (id == R.id.nav_subsription) {

            Intent in = new Intent(this,SubscriptionActivity.class);

            startActivity(in);

        }
//        else if (id==R.id.nav_fav_membership){
//
//            Intent intent=new  Intent(this, MemberShipCardAcitvity.class);
//            intent.putExtra("from_home", true);
//            startActivity(intent);
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public Drawable getBottomIcon(MaterialDrawableBuilder.IconValue icon) {
        Drawable d =  MaterialDrawableBuilder.with(getApplicationContext()) // provide a context
                .setIcon(icon) // provide an icon
                .setColor(Color.parseColor("#58595b")) // set the icon color
                .setSizeDp(R.dimen.bottom_button_height)
                .setToActionbarSize() // set the icon size
                .build(); // Finally call build;
        return d;
    }


    public void removeNotification(){
        bottomNavigation.setNotification("",3);
    }

    protected void openLocationIntent() {
        try {
            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                    .setTypeFilter(AutocompleteFilter.TYPE_FILTER_REGIONS)
                    .build();

            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(typeFilter)
//                             .setBoundsBias(PLACE_BOUNDS_DELHI)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                Place place = PlaceAutocomplete.getPlace(this, data);
//                new ConstantHelper().createLocation(activity, String.valueOf(place.getLatLng().latitude),
//                        String.valueOf(place.getLatLng().longitude), place.getAddress().toString());
//                new ConstantHelper().initLocation(activity);

                SharedPreferences sharedPref= getSharedPreferences
                        ("location", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor= sharedPref.edit();
                editor.putString("name", ""+ place.getName());
                editor.putString("address", ""+place.getAddress());
                editor.putString("lat", ""+place.getLatLng().latitude);
                editor.putString("long", ""+place.getLatLng().longitude);
                editor.apply();

                BeuSalonsSharedPrefrence.saveAddress(this,place.getName().toString(),place.getAddress().toString(),place.getLatLng().latitude+"",place.getLatLng().longitude+"");

                txt_location_name.setText(place.getName());

                EventBus.getDefault().post(new HomeAddressChangedEvent());


//                int pos = viewPager.getCurrentItem();
//               Fragment activeFragment = adapter.getItem(pos);
//                if(pos == 0)
//                    ((HomeFragmentNew)activeFragment).fetchHomeScreenData();
//             /*  *//**//* HomeFragmentNew fragment = (HomeFragmentNew) getSupportFragmentManager().findFragmentById(R.id.view_pager);
//
//                fragment.fetchHomeScreenData();*//**//*
//                Fragment fragment=getSupportFragmentManager().findFragmentById(R.id.view_pager);*/
//               /* if (fragment instanceof HomeFragmentNew){
//                    HomeFragmentNew fragment1 = (HomeFragmentNew) getSupportFragmentManager().findFragmentById(R.id.view_pager);
//
//                    fragment1.fetchHomeScreenData();
//                }*/

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);

                // TODO: Handle the error.
                Log.e("investigatingg", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                Log.e("investigatingg", "so the user cancelled the autocomplete acitivty");
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(UserCart event) {

        Log.i("mainactivity", "i'm in cartmodel event bus notification");

       /* if (event.getServicesList()!=null && event.getServicesList().size()>0){
            int cartsize=event.getServicesList().size();
            bottomNavigation.setNotification(""+cartsize,3);
        }else{
            bottomNavigation.setNotification("",3);
        }*/

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNotificationEvent(Notifications event) {
        Log.i("mainactivity", "i'm in event bus notification :D");
        fetchNotificationData();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CartNotificationEvent event) {

        //   if(bottomNavigation!=null)
        //       bottomNavigation.setNotification("",3);

    }

    private void fetchNotificationData() {

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface service = retrofit.create(ApiInterface.class);

        NotificationPost post = new NotificationPost();
        post.setUserId(BeuSalonsSharedPrefrence.getUserId());
        post.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());

        Call<NotiFicationResponse> call = service.getNotifications(post);

        call.enqueue(new Callback<NotiFicationResponse>() {

            @Override
            public void onResponse(Call<NotiFicationResponse> call, Response<NotiFicationResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().getSuccess()){

                        int count=0;
                        int noti_count=0;
                        Log.i("notifika", "i'm in notification success"+ noti_count);

                        try {
                            noti_count= response.body().getData().size()>0?response.body().getData().size():0;
                            Notifications notifications;
                            snappyDB = DBFactory.open(MainActivity.this);
                            if (snappyDB.exists(AppConstant.NOTIFICATION_DB)) {
                                notifications = snappyDB.getObject(AppConstant.NOTIFICATION_DB, Notifications.class);
                                snappyDB.close();

                                for(int i=0;i<notifications.getList().size();i++){

                                    for(int j=0; j<response.body().getData().size();j++){

                                        if(response.body().getData().get(j).getNotificationId()
                                                .equals(notifications.getList().get(i).getNotificationId())){
                                            if (notifications.getList().get(i).isSeen()){
                                                count++;
                                            }else{

                                            }


                                        }

                                    }
                                }

                                noti_count-=  count;

                                Log.i("notifika", "values: "+ count+  "   "+ noti_count);

                                if (noti_count>0){

                                    bottomNavigation.setNotification(""+noti_count, 4);
                                }else{

                                    bottomNavigation.setNotification("",4);
                                }
                            }else{

                                snappyDB.close();

                                Log.i("notifika", "values here: "+ count+  "   "+ noti_count);

                                if (noti_count>0){

                                    bottomNavigation.setNotification(""+noti_count, 4);

                                }else{
                                    bottomNavigation.setNotification("",4);
                                }


                            }
                        } catch (SnappydbException e) {
                            e.printStackTrace();
                        }


                    }else{
                        Log.i("notifika", "i'm in notification unsuccessful");

                    }


                } else{

                    Log.i("notifika", "i'm in retrofit notification not successful");
                }


            }

            @Override
            public void onFailure(Call<NotiFicationResponse> call, Throwable t) {
//                Log.e("asddas", t.getMessage());

                Log.i("notifika", "i'm in notification failure");

            }
        });
    }




    @Override
    protected void onResume() {
        super.onResume();

        txt_location_name.setText(BeuSalonsSharedPrefrence.getAddressLocalty());
        //     refreshCartNotification();                          //app constant item in cart ispe dependent hai
        switch (bottomNavigation.getCurrentItem()){
            case 0:
                setTitleHeader("home");
                break;
            case 1:
                setTitleHeader("deals");
                break;
            case 2:
                setTitleHeader("My Freebies");
                break;
            case 3:
                setTitleHeader("Coupon");
                break;
            case 4:
                setTitleHeader("My Profile");
                break;
        }

    }









    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }

    public void fetchLoyaltyData(final String lat, final String lon) {


        final LoyaltyPointsRequest loyaltyPointsRequest = new LoyaltyPointsRequest();
        loyaltyPointsRequest.setUserId(BeuSalonsSharedPrefrence.getUserId());
        loyaltyPointsRequest.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        loyaltyPointsRequest.setLatitude(lat);
        loyaltyPointsRequest.setLongitude(lon);
        loyaltyPointsRequest.setVersionAndroid(BuildConfig.VERSION_NAME);
        loyaltyPointsRequest.setMacAddress(getMacAddr());


        Retrofit   retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<LoyaltyPointsResponse> call = apiInterface.loyaltyPoints(loyaltyPointsRequest);
        call.enqueue(new Callback<LoyaltyPointsResponse>() {
            @Override
            public void onResponse(Call<LoyaltyPointsResponse> call, final Response<LoyaltyPointsResponse> response) {

                Log.i(TAG, "I'm in loyalty response success");
                if (response.isSuccessful()) {


                    if(response.body().getSuccess()){


                        Log.i("notificka", "value in loyalty: "+response.body().getData().getCode());

                        BeuSalonsSharedPrefrence.setReferCode(MainActivity.this, response.body().getData().getCode());
                        BeuSalonsSharedPrefrence.setMyLoyaltyPoints(response.body().getData().getPoints());
                        BeuSalonsSharedPrefrence.setFreeHeircutBar(response.body().getData().getFreeHairCutBar());
                        BeuSalonsSharedPrefrence.setNormalMessage(response.body().getData().getNoramlReferalMessage().getMessage());

                        Float amt;
                        String name="";
                        if(response.body().getData().getActiveMemberships()!=null &&
                                response.body().getData().getActiveMemberships().size()>0){

                            amt= (float)response.body().getData().getActiveMemberships().get(0).getCredits();
                            name= response.body().getData().getActiveMemberships().get(0).getName();
                        }
                        else
                            amt=(float)0;

                        BeuSalonsSharedPrefrence.setMembershipName(name);
                        BeuSalonsSharedPrefrence.setMembershipPoints(amt);

                        if (response.body().getData().isSubscribed()){
                            BeuSalonsSharedPrefrence.setisSubscribe(true);
                            BeuSalonsSharedPrefrence.setSubsReferMsg(response.body().getData().getSubscriptionReferMessage());
                            BeuSalonsSharedPrefrence.setSubsBalance(response.body().getData().getRemainingSubscriptionAmount());

                        }
                        //setting this to false, so no more free popup

                        //wifi
                        /*if (response.body().getData().getWifiName().length()>0){

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    WifiManager wifiManager = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                                    try{                             //   if ssid will not available it will throw exception
                                        if(!wifiManager.isWifiEnabled())
                                            wifiManager.setWifiEnabled(true);
                                        else
                                            wifiManager.disconnect();

                                        WifiConfiguration wifiConfig = new WifiConfiguration();
                                        wifiConfig.SSID = String.format("\"%s\"", response.body().getData().getWifiName());
                                        wifiConfig.preSharedKey = String.format("\"%s\"", response.body().getData().getWifiPassword());

                                        wifiManager.enableNetwork(wifiManager.addNetwork(wifiConfig), true);
                                        wifiManager.reconnect();
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }

                                    final CorporateSuccess success= new CorporateSuccess();
                                    Bundle bundle= new Bundle();
                                    bundle.putBoolean("wifi_success", true);
                                    success.setArguments(bundle);

                                    final WifiInfo info = wifiManager.getConnectionInfo ();
                                    String ssid  = info.getSSID();
                                    runnable= new Runnable() {
                                        @Override
                                        public void run() {

                                            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                            NetworkInfo mWifi = connManager.getActiveNetworkInfo();

                                            try{
                                                if(mWifi!=null)
                                                    if (info.getSSID().equalsIgnoreCase(response.body().getData().getWifiName())){
                                                        success.show(getSupportFragmentManager(), "wifi_popup");
                                                    }

                                                // if(mWifi.getType()== ConnectivityManager.TYPE_WIFI)           //success

                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }

                                        }
                                    };
                                    handler= new Handler();
                                    handler.postDelayed(runnable, 5000);
                                }
                            }, 9000);
                        }*/

                       /* try {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            View view = inflater.inflate(R.layout.dialog_free_service_welcome, null);
                            Button btn_free_okay, btn_skip;
                            final TextView txt_freebie,txt_dialogue_header;
                            ImageView img_dialog_;
                            img_dialog_= (ImageView) view.findViewById(R.id.img_dialog_);
                            txt_freebie= (TextView)view.findViewById(R.id.txt_freebie);
                            txt_dialogue_header= (TextView)view.findViewById(R.id.txt_dialogue_header);
                            btn_free_okay= (Button) view.findViewById(R.id.btn_welcome);
                            btn_skip= (Button)view.findViewById(R.id.btn_skip);
                            builder.setView(view).setCancelable(true);
                            final AlertDialog dialog = builder.create();
                            if (BeuSalonsSharedPrefrence.getAppOpenCount()<1 && count > 0) {
                                txt_freebie.setText("Congrats! Thank You For Signing Up With Be U Salons. Get 100% Cashback On Your 1st Digital Payment Or 50% Cashback On Your 1st Cash Payment Through The App. *T&C Apply");

                                btn_skip.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                    }
                                });

                                btn_free_okay.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                         *//*   Runnable runnable= new Runnable() {
                                                @Override
                                                public void run() {

*//**//*
                                                    CartModel cartModel= new CartModel();
                                                    cartModel.setParlorId("serviceDb");

                                                    CartServiceModel modelOffers= new CartServiceModel();

                                                    modelOffers.setName(response.body().getData().getFreeServices().get(0).getName());

                                                    String theCode= String.valueOf(response.body().getData().getFreeServices().get(0).getCode())
                                                            +13579;
                                                    Log.i(TAG, "the value in code: "+ theCode);

                                                    modelOffers.setServiceCode(Integer.parseInt(theCode));
                                                    modelOffers.setActualServiceCode(response.body().getData().getFreeServices().get(0).getCode());
                                                    modelOffers.setServiceId(response.body().getData().getFreeServices().get(0).getServiceId());
                                                    modelOffers.setQuantity(1);
                                                    modelOffers.setPriceId(response.body().getData().getFreeServices().get(0).getCode());
                                                    modelOffers.setMenuPrice(response.body().getData().getFreeServices().get(0).getPrice());

//                        modelOffers.setOffer_price(profileResponse.getData().getProfileFreeServices().get(i).getPrice());
                                                    modelOffers.setPrice(0);
                                                    modelOffers.setType("deal");
                                                    modelOffers.setOffer_check(false);          //setting the free service check to false by default
                                                    modelOffers.setTax(0);
                                                    modelOffers.setAdditions("Free Service!");
                                                    modelOffers.setPriceAdditions(0);
                                                    modelOffers.setIndex(0);

                                                    //yeh variable service ke case mai parlor list ke liye hai
                                                    modelOffers.setService_serviceCode(response.body().getData().getFreeServices().get(0).getCode());



                                                    CartModel recent_cart= new CartModel();
                                                    DB snappyDB = null;
                                                    try {
                                                        snappyDB = DBFactory.open(MainActivity.this);
                                                        if (snappyDB.exists(AppConstant.CART_COLLECTION_NAME)) {
                                                            recent_cart = snappyDB.getObject(AppConstant.CART_COLLECTION_NAME, CartModel.class);
                                                            snappyDB.close();
                                                        }
                                                    } catch (SnappydbException e) {
                                                        e.printStackTrace();
                                                    }
                                                    Log.i("investi", "on check");
                                                    boolean flag= false;
                                                    for(int i=0; i<recent_cart.getCartServiceList().size();i++){

                                                        if(recent_cart.getCartServiceList().get(i).getServiceCode()==
                                                                Integer.parseInt(theCode)){

                                                            flag=true;
                                                        }
                                                    }

                                                    if(flag){
                                                        Log.i("investi", "on check flag= true");
                                                        // do not add the free stuff
                                                    }else{

                                                        modelOffers.setOffer_check(true);

                                                        runOnUiThread(new Runnable() {

                                                            @Override
                                                            public void run() {
                                                                Toast.makeText(MainActivity.this, "Package added to cart!", Toast.LENGTH_SHORT).show();
                                                            }
                                                        });


                                                        new Thread(new SaveToCartTask(MainActivity.this, modelOffers, cartModel , false)).start();

                                                    }*//**//*
                                                    bottomNavigation.setCurrentItem(2);

                                                }
                                            };*//*
                                        Runnable runnable= new Runnable() {
                                            @Override
                                            public void run() {
                                                bottomNavigation.setCurrentItem(2);
                                            }
                                        };
                                        new Handler().postDelayed(runnable, 1000);

                                        dialog.dismiss();

                                        // new Thread(runnable).start();



                                           *//* Intent intent= new Intent(MainActivity.this, ParlorListActivity.class);
                                            intent.putExtra("service","serviceList");
                                            intent.putExtra("parlorFreeStuff", true);
                                            startActivity(intent);*//*
                                    }
                                });


                            }else{
                                int index=BeuSalonsSharedPrefrence.getFreebieCount();
                                if (index < response.body().getData().getFreebies().size()){
                                    txt_dialogue_header.setText(response.body().getData().getFreebies().get(index).getHead());
//                                    img_dialog_
                                    Glide.with(activity)
                                            .load(response.body().getData().getFreebies().get(index).getImage())
                                            .fitCenter()
                                            .placeholder(R.drawable.ic_freebie)
                                            .crossFade()
                                            .into(img_dialog_);
                                    txt_freebie.setText(response.body().getData().getFreebies().get(index).getMessage()+" *T&C Apply");
                                    btn_free_okay.setText(response.body().getData().getFreebies().get(index).getAction());
                                    btn_free_okay.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Runnable runnable= new Runnable() {
                                                @Override
                                                public void run() {
                                                    bottomNavigation.setCurrentItem(2);
                                                }
                                            };
                                            new Handler().postDelayed(runnable, 1000);

                                            dialog.dismiss();

                                        }
                                    });
                                    index++;
                                    BeuSalonsSharedPrefrence.setFreebieCount(index);
                                }else{
                                       *//* txt_freebie.setText("Thank you for downloading Be U. You just earned a free "+
                                                response.body().getData().getFreeServices().get(0).getName()+".");*//*
                                    BeuSalonsSharedPrefrence.setFreebieCount(0);
                                    BeuSalonsSharedPrefrence.setAppOpenCount(0);
                                    txt_dialogue_header.setText(response.body().getData().getFreebies().get(index).getHead());
//                                    img_dialog_
                                    Glide.with(activity)
                                            .load(response.body().getData().getFreebies().get(index).getImage())
                                            .fitCenter()
                                            .placeholder(R.drawable.ic_freebie)
                                            .crossFade()
                                            .into(img_dialog_);
                                    txt_freebie.setText(response.body().getData().getFreebies().get(index).getMessage());
                                    btn_free_okay.setText(response.body().getData().getFreebies().get(index).getAction());
                                    btn_free_okay.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Runnable runnable= new Runnable() {
                                                @Override
                                                public void run() {
                                                    bottomNavigation.setCurrentItem(2);
                                                }
                                            };
                                            new Handler().postDelayed(runnable, 1000);

                                            dialog.dismiss();

                                        }
                                    });
                                    index++;
                                    BeuSalonsSharedPrefrence.setFreebieCount(index);
                                      *//*  AccessToken token = AccessToken.getCurrentAccessToken();
                                        GraphRequest request = GraphRequest.newMeRequest(
                                                token,
                                                new GraphRequest.GraphJSONObjectCallback() {
                                                    @Override
                                                    public void onCompleted(
                                                            JSONObject object,
                                                            GraphResponse response) {
                                                        // Application code
                                                    }
                                                });
                                        Bundle parameters = new Bundle();
                                        parameters.putString("fields", "id,name,user_posts");
                                        request.setParameters(parameters);
                                        request.executeAsync();*//*
                                       *//* Bundle params = new Bundle();
                                        params.putString("with", "location");
*//**//* make the API call *//**//*
                                        new GraphRequest(
                                                AccessToken.getCurrentAccessToken(),
                                                "/me/feed",
                                                params,
                                                HttpMethod.GET,
                                                new GraphRequest.Callback() {
                                                    public void onCompleted(GraphResponse response) {
            *//**//* handle the result *//**//*
                                                    }
                                                }
                                        ).executeAsync();*//*

                                      *//*  Bundle params = new Bundle();
                                        params.putString("message", "This is a test message");
*//**//* make the API call *//**//*
                                        new GraphRequest(
                                                AccessToken.getCurrentAccessToken(),
                                                "/me/feed",
                                                params,
                                                HttpMethod.POST,
                                                new GraphRequest.Callback() {
                                                    public void onCompleted(GraphResponse response) {
            *//**//* handle the result *//**//*
                                                    }
                                                }
                                        ).executeAsync();*//*

                                }



                            }








                            dialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                            // }
                        }*/



                    }else{
                        if(retry<3){
                            fetchLoyaltyData(lat,lon);
                            retry++;
                        }
                    }

                }

            }

            @Override
            public void onFailure(Call<LoyaltyPointsResponse> call, Throwable t) {

                //try again
                if(retry<3){
                    fetchLoyaltyData(lat,lon);
                    retry++;
                }

                Log.i(TAG, "Loyalty request failure");
            }
        });
    }




    /**
     * Broadcast reciver class for location update
     */
    public class LocationReciver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            Bundle bundle = intent.getExtras();
            if (bundle != null && bundle.getString("local") != null) {

                final String response = bundle.getString("local");
                final Double lat =bundle.getDouble("lat");
                final Double lon =bundle.getDouble("long");
                if (!response.equalsIgnoreCase(BeuSalonsSharedPrefrence.getAddressLocalty())){

                    // Toast.makeText(MainActivity.this,"We have found new Location "+response,Toast.LENGTH_LONG).show();
                    AlertDialog.Builder builder;
                    /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light);
                    } else {
                        builder = new AlertDialog.Builder(context);
                    }*/
                    builder = new AlertDialog.Builder(context);
                    builder.setMessage("We have found new Location "+response +" do you want to update this location")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    BeuSalonsSharedPrefrence.saveAddress(MainActivity.this,
                                            response,response,String.valueOf(lat),String.valueOf(lon));
                                    txt_location_name.setText(response);

                                    EventBus.getDefault().post(new HomeAddressChangedEvent());
                                    fetchLoyaltyData(String.valueOf(lat),String.valueOf(lon));
                                    dialog.dismiss();

                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                }else{

                    fetchLoyaltyData(String.valueOf(lat),String.valueOf(lon));
                }

            }
        }
    }


    private  void setSmartPopUp(){

        if (BeuSalonsSharedPrefrence.getOfferDialog()){




            //    new Handler().postDelayed(new Runnable() {
            //         @Override
            //          public void run() {
            final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.smart_popup, null);



            builder.setView(view).setCancelable(true);
            final AlertDialog dialog = builder.create();
            Window window = dialog.getWindow();
            assert window != null;
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            /*dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!MainActivity.this.isFinishing())

                            setToolTip(bottomNavigation.getViewAtPosition(1));
                        }
                    },1000);

                }
            });*/

            dialog.show();

            BeuSalonsSharedPrefrence.setOfferDialog(MainActivity.this, false);
            //            }
            //       },1000);
        }
    }

    private  void setSubscriptionPopUp(){

        if (BeuSalonsSharedPrefrence.getOfferDialog()){




            //    new Handler().postDelayed(new Runnable() {
            //         @Override
            //          public void run() {
            final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.smart_subscription, null);
            LinearLayout layout=view.findViewById(R.id.ll_smartPop);
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    String msg=BeuSalonsSharedPrefrence.getSubsReferMsg()+"https://beusalons.app.link/hjrjss9mZJ?page=subscription&subscriptionCode="+BeuSalonsSharedPrefrence.getReferCode();
                    Intent sendIntent = new Intent(Intent.ACTION_SEND);
                    msg=msg.replaceAll(" @@ "," "+BeuSalonsSharedPrefrence.getReferCode() +" ");
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
                    sendIntent.setType("text/plain");
                    startActivity(Intent.createChooser(sendIntent,"Refer Subscription"));
                }
            });



            builder.setView(view).setCancelable(true);
            final AlertDialog dialog = builder.create();
//            Window window = dialog.getWindow();
//            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
         /*   dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!MainActivity.this.isFinishing())

                            setToolTip(bottomNavigation.getViewAtPosition(1));
                        }
                    },1000);

                }
            });*/

            dialog.show();

            BeuSalonsSharedPrefrence.setOfferDialog(MainActivity.this, false);
            //            }
            //       },1000);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFilterEvent3(FiltersEvent event) {

        isMale= event.getFilters_hashmap().get("isMale")==null?false:event.getFilters_hashmap().get("isMale");
        isFemale= event.getFilters_hashmap().get("isFemale")==null?false:event.getFilters_hashmap().get("isFemale");
        isUnisex= event.getFilters_hashmap().get("isUnisex")==null?false:event.getFilters_hashmap().get("isUnisex");

        isPopularity= event.getFilters_hashmap().get("isPopularity")==null?false:event.getFilters_hashmap().get("isPopularity");
        isPriceLow= event.getFilters_hashmap().get("isPriceLow")==null?false:event.getFilters_hashmap().get("isPriceLow");
        isPriceHigh= event.getFilters_hashmap().get("isPriceHigh")==null?false:event.getFilters_hashmap().get("isPriceHigh");
        isNearest= event.getFilters_hashmap().get("isNearest")==null?false:event.getFilters_hashmap().get("isNearest");

        isRating_1= event.getFilters_hashmap().get("isRating_1")==null?false:event.getFilters_hashmap().get("isRating_1");
        isRating_2= event.getFilters_hashmap().get("isRating_2")==null?false:event.getFilters_hashmap().get("isRating_2");
        isRating_3= event.getFilters_hashmap().get("isRating_3")==null?false:event.getFilters_hashmap().get("isRating_3");
        isRating_4= event.getFilters_hashmap().get("isRating_4")==null?false:event.getFilters_hashmap().get("isRating_4");

        isPrice_red= event.getFilters_hashmap().get("isPrice_red")==null?false:event.getFilters_hashmap().get("isPrice_red");
        isPrice_blue= event.getFilters_hashmap().get("isPrice_blue")==null?false:event.getFilters_hashmap().get("isPrice_blue");
        isPrice_green= event.getFilters_hashmap().get("isPrice_green")==null?false:event.getFilters_hashmap().get("isPrice_green");
        isPrice_all= event.getFilters_hashmap().get("isPrice_all")==null?false:event.getFilters_hashmap().get("isPrice_all");


        //----------------------------------------------filters------------------------

        int fil_count=0;

        if(isRating_1){
            fil_count++;
        }else if(isRating_2){
            fil_count++;
        }else if(isRating_3){
            fil_count++;
        }else if(isRating_4){
            fil_count++;
        }

        if(isPrice_red){
            fil_count++;
        }else if(isPrice_blue){
            fil_count++;
        }else if(isPrice_green){
            fil_count++;
        }else if(isPrice_all){
            fil_count++;
        }

        if(isMale){
            fil_count++;
        }else if (isFemale) {
            fil_count++;
        }else if(isUnisex){
            fil_count++;
        }

        String filters_count = "";
        if(fil_count==0){
            filters_count ="";
        }else{

            filters_count = "("+fil_count+")";
        }

        txt_filter.setText("Sort | Filter "+ filters_count);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void filterEvent(SortFilterEvent event) {

        Log.i("filtereventtt", "sort: "+ event.getSort()+ " category: "+event.getCategory()+
                " rating: "+event.getRating()+ " brands: "+event.getBrands());


        filters_sort= event.getSort();
        filters_price= event.getCategory();     //price hai yeh
        filters_rating= event.getRating();

    }


    public void postFirBaseRegId(){

        //get token
        String token = FirebaseInstanceId.getInstance().getToken();

        FireBaseIdPost fireBaseIdPost=new FireBaseIdPost();
        fireBaseIdPost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        fireBaseIdPost.setUserId(BeuSalonsSharedPrefrence.getUserId());
        fireBaseIdPost.setFirebaseId(token);

        Retrofit retrofit1 = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit1.create(ApiInterface.class);
        Call<FireBaseResponse> call= apiInterface.fireBaseIdsPost(fireBaseIdPost);
        call.enqueue(new Callback<FireBaseResponse>() {
            @Override
            public void onResponse(Call<FireBaseResponse> call, Response<FireBaseResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().isSuccess()){

                        Log.i("reponsefirebaseclass", "success pe hoon");
//                        BeuSalonsSharedPrefrence.setFirebaseSuccess(true);
                    }else
                        Log.i("reponsefirebaseclass", "else pe hoon");

                }else
                    Log.i("reponsefirebaseclass", "double else pe hoon");
            }

            @Override
            public void onFailure(Call<FireBaseResponse> call, Throwable t) {
                Log.i("reponsefirebaseclass", "failure pe hoon: "+t.getMessage()+  " " +t.getCause());
            }
        });
    }


    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        nMyApplication.onActivityPaused(this);
        if(runnable!=null && handler!=null)
            handler.removeCallbacks(runnable);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        nMyApplication.onActivityDestroyed(this);
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        nMyApplication.onActivityStarted(this);
        EventBus.getDefault().register(this);
        registerReceiver(locationReciver,locationFilter);
        super.onStart();
    }

    @Override
    protected void onStop() {
        nMyApplication.onActivityStopped(this);
        EventBus.getDefault().unregister(this);
        unregisterReceiver(locationReciver);
        super.onStop();
    }

}
