package com.beusalons.android.Model.ServiceAvailable;

import java.util.List;

/**
 * Created by Ajay on 10/9/2017.
 */

public class ServiceAvailableData {
    private String expiryDate;
    private String parlorName;
    private boolean isSelected= false;     //check if the stuff is open or not

    private List<Service> services = null;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getParlorName() {
        return parlorName;
    }

    public void setParlorName(String parlorName) {
        this.parlorName = parlorName;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }
}
