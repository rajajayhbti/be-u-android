package com.beusalons.android.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.ArtistProfileActivity;
import com.beusalons.android.Model.selectArtist.Services;
import com.beusalons.android.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.michael.easydialog.EasyDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 15-Mar-18.
 */

public class SelectEmpAdapter extends RecyclerView.Adapter<SelectEmpAdapter.ViewHolder> {

    private List<Services> list;
    private boolean show_tooltip= true;
    private Activity activity;

    public SelectEmpAdapter(List<Services> list, Activity activity){

        this.list= list;
        this.activity= activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LinearLayout linear_= (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_select_emp, parent, false);
        return new ViewHolder(linear_);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final int pos= position;

        final LinearLayout linear_= holder.linear_;
        TextView txt_= linear_.findViewById(R.id.txt_);
        txt_.setText("Stylist Available For "+list.get(pos).getName());

        LinearLayout linear_container= linear_.findViewById(R.id.linear_container);
        linear_container.removeAllViews();

        final List<FrameLayout> frame_list= new ArrayList<>();
        for(int i=0;i<list.get(pos).getEmployees().size();i++){
            final int index= i;
            LinearLayout linear_row= (LinearLayout) LayoutInflater.from(linear_.getContext())
                    .inflate(R.layout.linear_select_emp_row, linear_container, false);

            ImageView img_= linear_row.findViewById(R.id.img_);
            Glide.with(linear_.getContext())
                    .load(list.get(pos).getEmployees().get(index).getImage())
                    .apply(RequestOptions.circleCropTransform())
                    .into(img_);

            TextView txt_rating= linear_row.findViewById(R.id.txt_rating);
            txt_rating.setText(""+list.get(pos).getEmployees().get(index).getRating());

            final TextView txt_name= linear_row.findViewById(R.id.txt_name);
            txt_name.setText(list.get(pos).getEmployees().get(index).getName());
            txt_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent= new Intent(view.getContext(), ArtistProfileActivity.class);
                    intent.putExtra("artistId", list.get(pos).getEmployees().get(index).getArtistId());
                    activity.startActivity(intent);

                }
            });

            if(list.get(pos).getEmployees().get(index).getArtistId()==null ||
                    list.get(pos).getEmployees().get(index).getArtistId().equalsIgnoreCase("")){
                txt_name.setTextColor(ContextCompat.getColor(linear_.getContext(), R.color.fontColor));
                txt_name.setEnabled(false);
            }else{
                txt_name.setTextColor(ContextCompat.getColor(linear_.getContext(), R.color.show_detail_color));
                txt_name.setEnabled(true);
            }

            TextView txt_detail= linear_row.findViewById(R.id.txt_detail);
            txt_detail.setText("Clients Served\n"+list.get(pos).getEmployees().get(index).getClientServed());

            final ImageView img_check= linear_row.findViewById(R.id.img_check);

            final FrameLayout frame_= linear_row.findViewById(R.id.frame_);
            frame_.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(list.get(pos).getEmployees().get(index).isSelected()){
                        frame_.setAlpha(1);
                        list.get(pos).getEmployees().get(index).setSelected(false);
                        img_check.setVisibility(View.GONE);
                    }else{
                        frame_.setAlpha(.6f);
                        list.get(pos).getEmployees().get(index).setSelected(true);
                        img_check.setVisibility(View.VISIBLE);
                    }

                    for(int a=0;a<list.get(pos).getEmployees().size();a++){
                        if(a!=index){
                            list.get(pos).getEmployees().get(a).setSelected(false);
                            frame_list.get(a).setAlpha(1);
                            frame_list.get(a).findViewById(R.id.img_check).setVisibility(View.GONE);
                        }
                    }
                }
            });
            frame_list.add(frame_);
            if(list.get(pos).getEmployees().get(index).isSelected()){
                frame_.setAlpha(.6f);
                img_check.setVisibility(View.VISIBLE);
            }else{
                frame_.setAlpha(1);
                img_check.setVisibility(View.GONE);
            }

            linear_container.addView(linear_row);

            if(show_tooltip){
                if(pos==0 &&
                        index==1){
                    show_tooltip= false;

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setToolTip(linear_.getContext(), txt_name);
                        }
                    }, 1200);

                }
            }
        }
    }

    private void setToolTip(Context context, TextView txt_name){

        View view = LayoutInflater.from(context).inflate(R.layout.tooltip_selectemp,
                null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT );
        view.setLayoutParams(params);
        new EasyDialog(activity)
                // .setLayoutResourceId(R.layout.layout_tip_content_horizontal)//layout resource id
                .setLayout(view)
                .setBackgroundColor(context.getResources().getColor(R.color.tooltip_bg))
                // .setLocation(new location[])//point in screen
                .setLocationByAttachedView(txt_name)
                .setGravity(EasyDialog.GRAVITY_BOTTOM)
                .setTouchOutsideDismiss(true)
                .setOutsideColor(context.getResources().getColor(android.R.color.transparent))
                .setMatchParent(true)
                .setMarginLeftAndRight(64, 120)
                .show();
    }

    @Override
    public int getItemCount() {

        if(list!=null &&
                list.size()>0)
            return list.size();
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout linear_;
        public ViewHolder(View itemView) {
            super(itemView);
            linear_= (LinearLayout)itemView;
        }
    }

}
