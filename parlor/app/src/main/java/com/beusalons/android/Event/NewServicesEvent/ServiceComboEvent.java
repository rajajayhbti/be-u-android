package com.beusalons.android.Event.NewServicesEvent;

import com.beusalons.android.Model.UserCart.PackageService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 6/17/2017.
 */

public class ServiceComboEvent {

    public ServiceComboEvent(){

    }

    private String service_name;
    private int service_code;
    private int price_id;

    private String service_deal_id;
    private String service_id;

    private int dealId;

    private String type;                //service ya deal type

    private int price;
    private int menu_price;
    private String description;
    private String brand_name;
    private String brand_id;
    private String product_name;
    private String product_id;
    private String primary_key;

    //-----------------------------package ka stuff-------------------------

    private List<PackageService> package_services_list= new ArrayList<>();


    //-------------------------------------------------------------------------


    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public int getDealId() {
        return dealId;
    }

    public void setDealId(int dealId) {
        this.dealId = dealId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public int getService_code() {
        return service_code;
    }

    public void setService_code(int service_code) {
        this.service_code = service_code;
    }

    public int getPrice_id() {
        return price_id;
    }

    public void setPrice_id(int price_id) {
        this.price_id = price_id;
    }

    public String getService_deal_id() {
        return service_deal_id;
    }

    public void setService_deal_id(String service_deal_id) {
        this.service_deal_id = service_deal_id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getMenu_price() {
        return menu_price;
    }

    public void setMenu_price(int menu_price) {
        this.menu_price = menu_price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getPrimary_key() {
        return primary_key;
    }

    public void setPrimary_key(String primary_key) {
        this.primary_key = primary_key;
    }

    public List<PackageService> getPackage_services_list() {
        return package_services_list;
    }

    public void setPackage_services_list(List<PackageService> package_services_list) {
        this.package_services_list = package_services_list;
    }
}
