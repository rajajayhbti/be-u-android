package com.beusalons.android.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beusalons.android.Model.MymembershipDetails.Member;
import com.beusalons.android.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ajay on 11/7/2017.
 */

public class AdapterSelectedContact extends  RecyclerView.Adapter<AdapterSelectedContact.MyViewHolder> {
    Activity activity;
    private List<Member> myArrayList;
    public AdapterSelectedContact(Activity myActivity, List<Member> cusData){
        this.myArrayList=cusData;
        this.activity=myActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_selected_contact_, parent, false);

        return new AdapterSelectedContact.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (!myArrayList.get(position).getName().equals("")){
            holder.txtViewName.setText(myArrayList.get(position).getName());
        }
        else{
            holder.txtViewName.setText(myArrayList.get(position).getStatus());
        }
        holder.txtViewCustomerNumer.setText(myArrayList.get(position).getPhoneNumber().trim().toString());
    }

    @Override
    public int getItemCount() {
        return myArrayList.size();
    }

    public class MyViewHolder extends  RecyclerView.ViewHolder{
        @BindView(R.id.contact_name_)
        TextView txtViewName;
        @BindView(R.id.contact_number_)
        TextView txtViewCustomerNumer;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
