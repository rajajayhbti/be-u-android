package com.beusalons.android.Retrofit;

import com.beusalons.android.Model.AboutUser.SendPost;
import com.beusalons.android.Model.AboutUser.SendResponse;
import com.beusalons.android.Model.AddServiceUserCart.AddService_response;
import com.beusalons.android.Model.AddServiceUserCart.UserCart_post;
import com.beusalons.android.Model.AllParlors.AllParlorResponse;
import com.beusalons.android.Model.AppCloseResponse;
import com.beusalons.android.Model.AppointmentDetail.AppointmentDetailPost;
import com.beusalons.android.Model.AppointmentDetail.AppointmentDetailResponse;
import com.beusalons.android.Model.Appointments.AppointmentPost;
import com.beusalons.android.Model.Appointments.AppointmentResponse;
import com.beusalons.android.Model.Appointments.CancelAppointMentsPost;
import com.beusalons.android.Model.Appointments.CancelAppointmentResponse;
import com.beusalons.android.Model.Appointments.PastAppointmentsPost;
import com.beusalons.android.Model.Appointments.PastAppointmentsResponse;
import com.beusalons.android.Model.Appointments.UpcomingAppointmentsPost;
import com.beusalons.android.Model.Appointments.UpcomingAppointmentsResponse;
import com.beusalons.android.Model.ArtistProfile.ArtistProfile_post;
import com.beusalons.android.Model.ArtistProfile.Artist_response;
import com.beusalons.android.Model.ArtistProfile.FollowArtist_post;
import com.beusalons.android.Model.BillSummery.ApplyPromoRequest;
import com.beusalons.android.Model.BillSummery.CouponAppliedResponse;
import com.beusalons.android.Model.Corporate.CorporateDetail.CorporateDetailPost;
import com.beusalons.android.Model.Corporate.CorporateDetail.CorporateDetailResponse;
import com.beusalons.android.Model.Corporate.CorporateRequest.CorporateRequestPost;
import com.beusalons.android.Model.Corporate.CorporateRequest.CorporateRequestResponse;
import com.beusalons.android.Model.Corporate.CorporateSendOtp.CorporateSendOtp;
import com.beusalons.android.Model.Corporate.CorporateSendOtp.CorporateSendResponse;
import com.beusalons.android.Model.Corporate.CorporateVerifyOtp.CorporateVerifyPost;
import com.beusalons.android.Model.Corporate.CorporateVerifyOtp.CorporateVerifyResponse;
import com.beusalons.android.Model.Coupon.Coupon_Response;
import com.beusalons.android.Model.Coupon.Coupon_post;
import com.beusalons.android.Model.Deal.DealDetailsResponse;
import com.beusalons.android.Model.Deal.DealsData;
import com.beusalons.android.Model.Deal.DealsResponseModel;
import com.beusalons.android.Model.DealDeatilsPost;
import com.beusalons.android.Model.DealsData.DealsDepartmentResponse;
import com.beusalons.android.Model.DealsSalonList.DealSalonListResponse;
import com.beusalons.android.Model.DealsServices.DealDetail.Deal_Detail_Post;
import com.beusalons.android.Model.DealsServices.DealsResponse;
import com.beusalons.android.Model.DistanceMatrixApi.DistanceModel;
import com.beusalons.android.Model.Favourites.SalonFavResponse;
import com.beusalons.android.Model.Favourites.SalonFavpost;
import com.beusalons.android.Model.FireBaseResponse;
import com.beusalons.android.Model.FirebaseModel.FireBaseIdPost;
import com.beusalons.android.Model.GeoCode.GeoCodingResponse;
import com.beusalons.android.Model.HomeFragmentModel.HomePageData;
import com.beusalons.android.Model.HomeFragmentModel.HomePost;
import com.beusalons.android.Model.HomeFragmentModel.ImageResponse;
import com.beusalons.android.Model.HomePage.ApiResponse;
import com.beusalons.android.Model.HomePageBottomSheetModel;
import com.beusalons.android.Model.Login.Login_Post;
import com.beusalons.android.Model.Login.Login_Response;
import com.beusalons.android.Model.Logout.LogoutModel;
import com.beusalons.android.Model.Loyalty.LoyaltyPointsRequest;
import com.beusalons.android.Model.Loyalty.LoyaltyPointsResponse;
import com.beusalons.android.Model.Loyalty.UpgradePost;
import com.beusalons.android.Model.Loyalty.UpgradeResponse;
import com.beusalons.android.Model.MemberShip.MemberShip_Response;
import com.beusalons.android.Model.MymembershipDetails.AddContact_response;
import com.beusalons.android.Model.MymembershipDetails.AddUserContactNumber_post;
import com.beusalons.android.Model.MymembershipDetails.MemberShipDetails_Response;
import com.beusalons.android.Model.MymembershipDetails.MembershipDetailsPost;
import com.beusalons.android.Model.Notifications.NotiFicationResponse;
import com.beusalons.android.Model.Notifications.NotificationPost;
import com.beusalons.android.Model.Offers.ProfilePost;
import com.beusalons.android.Model.Offers.ProfileResponse;
import com.beusalons.android.Model.OnlinePay.UseFreeBiesPost;
import com.beusalons.android.Model.ParlorDetail.FacebookCheckinPost;
import com.beusalons.android.Model.ParlorDetail.FacebookCheckinResponse;
import com.beusalons.android.Model.ParlorDetail.ParlorDetailResponse;
import com.beusalons.android.Model.PaymentSuccessPost;
import com.beusalons.android.Model.PaymentSuccessResponse;
import com.beusalons.android.Model.PostLike;
import com.beusalons.android.Model.Registration.Registration_Post;
import com.beusalons.android.Model.Registration.Registration_Response;
import com.beusalons.android.Model.RescheduleAppointmentPost;
import com.beusalons.android.Model.RescheduleAppointmentResponse;
import com.beusalons.android.Model.ResponseLike;
import com.beusalons.android.Model.Reviews.ParlorReviewResponse;
import com.beusalons.android.Model.Reviews.UserReviewsPost;
import com.beusalons.android.Model.Reviews.UserReviewsResponse;
import com.beusalons.android.Model.Reviews.WriteReviewPost;
import com.beusalons.android.Model.Reviews.WriteReviewResponse;
import com.beusalons.android.Model.SalonFilter.FilterResponse;
import com.beusalons.android.Model.SalonHome.HomeResponse;
import com.beusalons.android.Model.SalonHome.salonDepartments.SalonsDepartmentsResponse;
import com.beusalons.android.Model.SalonReview.HelpFull_Response;
import com.beusalons.android.Model.SalonReview.Helpfull_post;
import com.beusalons.android.Model.Send_Otp.Send_Otp_Post;
import com.beusalons.android.Model.Send_Otp.Send_Otp_Response;
import com.beusalons.android.Model.ServerTime;
import com.beusalons.android.Model.ServiceAvailable.ServiceAvailable_post;
import com.beusalons.android.Model.ServiceAvailable.ServiceAvailable_response;
import com.beusalons.android.Model.SocialLogin.LoginPost;
import com.beusalons.android.Model.SocialLogin.LoginResponse;
import com.beusalons.android.Model.Songs.AllSongs;
import com.beusalons.android.Model.StatusModel;
import com.beusalons.android.Model.SubscriptionHistory.Subscription_response;
import com.beusalons.android.Model.Subscription_post;
import com.beusalons.android.Model.UpdateAppResponse;
import com.beusalons.android.Model.UserCollection.CollectionUser_response;
import com.beusalons.android.Model.UserIDAccessTokenPost;
import com.beusalons.android.Model.UserProject.ProjectData_post;
import com.beusalons.android.Model.UserProject.Project_response;
import com.beusalons.android.Model.Verify_Otp.Verify_Otp_Post;
import com.beusalons.android.Model.VersionCheck.VersionCheckResponse;
import com.beusalons.android.Model.newServiceDeals.DepartmentResponse;
import com.beusalons.android.Model.newServiceDeals.NewCombo.NewComboResponse;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.ServiceResponse;
import com.beusalons.android.Model.selectArtist.SelectEmployeePost;
import com.beusalons.android.Model.selectArtist.SelectEmployeeResponse;
import com.beusalons.android.Model.selectArtist.SelectedPost;
import com.beusalons.android.Model.selectArtist.SelectedResponse;
import com.beusalons.android.Model.subscription.SubscriptionResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by myMachine on 10/31/2016.
 */

public interface    ApiInterface {


//    @GET("user")
//    Call<ParlorUser> getSuccessDetails();

    @GET("api/parlors")
    Call<ParlorResponseModel> parlorsList(@Query("latitude") String latitude, @Query("longitude") String longitude,
                                          @Query("userId") String userId,
                                          @Query("rating") String rating,@Query("price") String price,@Query("gender") String gender, @Query("sort") String sort ,@Query("page") int page);
    @GET("api/parlors")
    Call<ParlorResponseModel> getFavouriteSalons(@Query("latitude") String latitude, @Query("longitude") String longitude,
                                                 @Query("userId") String userId,  @Query("favourite") int favourite);
    //yeh search salon se hai
    @GET("api/parlors")
    Call<ParlorResponseModel> searchServiceSalonList(@Query("latitude") String latitude, @Query("longitude") String longitude,
                                                     @Query("userId") String userId, @Query("serviceCodes") List<String> serviceCodes
                                          /*,@Query("rating") String rating,@Query("price")
                                                       String price,@Query("gender") String gender, @Query("sort") String sort*/);

    @GET("api/deals")
    Call<DealsResponseModel> dealDetail(@Query("parlorId") String parlorId);

    @GET("api/services")
    Call<ServicesResponseModel> servicesList(@Query("latitude") String latitude, @Query("longitude") String longitude);

    @GET("api/dealsDetail")
    Call<DealsResponse>  dealsData(@Query("dealIds") List<DealsData> dealsData, @Query("latitude") String latitude, @Query("longitude") String longitude);


    //yeh mera upcoming appointments ke liye hai, kripya na chere iseeh! dhanyawaad
    @GET("api/parlorDetail")
    Call<ParlorDetailResponse> getDetail(@Query("parlorId") String parlorId, @Query("userId") String userId);

    @POST("api/user")
    Call<Registration_Response> registerUser(@Body Registration_Post registrationPost);

    @POST("api/login")
    Call<Login_Response> checkUser(@Body Login_Post loginPost);

    @POST("api/sendOtp")
    Call<Send_Otp_Response> sendOtpRequest(@Body Send_Otp_Post send_otp_request);

    @POST("api/verifyOtp")
    Call<LoginResponse> verifyOtpRequest(@Body Verify_Otp_Post verify_otp_post);

    @POST("loggedapi/loyalityPoints")
    Call<LoyaltyPointsResponse> loyaltyPoints(@Body LoyaltyPointsRequest loyaltyPointsRequest);

    @POST("loggedapi/upcomingAppointments")
    Call<UpcomingAppointmentsResponse> upcomingAppts(@Body UpcomingAppointmentsPost upcomingAppointmentsPost);

    @GET("api/serverTime")
    Call<ServerTime> getServerTime();

    @POST("loggedapi/pastAppointments")
    Call<PastAppointmentsResponse> pastAppts(@Body PastAppointmentsPost pastAppointmentsPost);

    @POST("loggedapi/addReview")
    Call<WriteReviewResponse> writeReview(@Body WriteReviewPost writeReviewPost);

    @POST("loggedapi/appointment")
    Call<AppointmentResponse> createAppts(@Body AppointmentPost appointmentPost);

    @POST("loggedapi/appointmentv3")
    Call<AppointmentDetailResponse> createAppointment(@Body AppointmentPost appointmentPost);

   /* @POST("loggedapi/appointmentv2")
    Call<AppointmentDetailResponse> createAppointment(@Body AppointmentPost appointmentPost);*/

//    @POST("loggedapi/initPaytmPayment")
//    Call<AppointmentDetailResponse> paytm(@Body AppointmentDetailPost paytmPost);

//    // for booking appointments
//    @POST("loggedapi/appointmentDetail")
//    Call<AppointmentDetailResponse> paytm(@Body AppointmentDetailPost appointmentDetailPost);

    @POST("loggedapi/userReviewedSalons")
    Call<UserReviewsResponse> userReviews(@Body UserReviewsPost post);

    @POST("api/dealsDetail")
    Call<DealDetailsResponse> dealsDetails(@Body DealDeatilsPost post);

    @POST("api/newDealsDetail")
    Call<DealSalonListResponse> getNewDealData(@Body Deal_Detail_Post post);

    @GET
    Call<ParlorResponseModel> geServiceParlorList(@Url String url);
    @GET
    Call<DistanceModel> distanceMatrix(@Url String url);

    @GET("api/parlorReviews")
    Call<ParlorReviewResponse> getParlorReviews(@Query("parlorId") String parlorId,
                                                @Query("sort") int sort,@Query("page") int page);

    @GET
    Call<DealsResponseModel> allDealDetail(@Url String url);

    @POST("loggedapi/bookAndCapturePayment")
    Call<PaymentSuccessResponse> bookCapturePayment(@Body PaymentSuccessPost post);

    @POST("loggedapi/appointmentDetail")
    Call<AppointmentDetailResponse> apptDetail(@Body AppointmentDetailPost appointmentDetailPost);

    @POST("loggedapi/profile")
    Call<ProfileResponse> profilePost(@Body ProfilePost post);


    @POST("loggedapi/updateFirebaseId")
    Call<FireBaseResponse> fireBaseIdsPost(@Body FireBaseIdPost post);

    @POST("loggedapi/logout")
    Call<StatusModel> logout(@Body LogoutModel post);

    @GET
    Call<ImageResponse> getHomeImages(@Url String url);

    @POST("loggedapi/homePage")
    Call<HomePageData> getHomeScreenData(@Body HomePost post);

//    @GET("json?units=metric&origins={userLat},{userLong}&destinations={latitude},{longitude}&departure_time=now&traffic_model=best_guess&key=AIzaSyCDVdJ8kE0RcbrsjJrb0b186PZkIDIm5ak")
//    Call<DistanceModel> distanceMatrix(@Query("userLat") String userLat, @Query("userLong") String userLong,
//                                       @Query("latitude") String latitude, @Query("longitude") String longitude);

    @GET
    Call<AllParlorResponse> getAllParlors(@Url String url); //get all parlor list

    @GET                                                    //this is to get address by lat long
    Call<GeoCodingResponse> getAddress(@Url String url);

    @POST("api/socialLogin")
    Call<LoginResponse> getSocialResponse(@Body LoginPost post);

    @POST("loggedapi/notification")
    Call<NotiFicationResponse> getNotifications(@Body NotificationPost post);


    @POST("loggedapi/favourite")
    Call<SalonFavResponse> postSalonFavourite(@Body SalonFavpost post);

    @POST("loggedapi/availCoupon ")
    Call<CouponAppliedResponse> postApplyCoupon(@Body ApplyPromoRequest post);

    @HTTP(method = "DELETE", hasBody = true, path = "/loggedapi/favourite")
    Call<SalonFavResponse> deleteSalonFavourite(@Body SalonFavpost post);



    @POST("loggedapi/cancelAppointment")
    Call<CancelAppointmentResponse> cancelAppointMent(@Body CancelAppointMentsPost post);



    @GET
    Call<com.beusalons.android.Model.AllDeals.Response> getAllDeals(@Url String url);

    @POST("loggedapi/changeAppointmentTime")
    Call<RescheduleAppointmentResponse> rescheduleAppt(@Body RescheduleAppointmentPost post);

    @GET("api/versionCheck")
    Call<VersionCheckResponse> checkVersion(@Query("version") String version);


    @GET
    Call<DepartmentResponse> getDepartmentServices(@Url String url);

    @GET
    Call<DealsDepartmentResponse> getDeals(@Url String url);
    @GET("api/parlorServiceByDepartment")
    Call<ServiceResponse> getService(@Query("gender") String gender, @Query("departmentId") String departmentId,
                                     @Query("parlorId") String parlorId,@Query("latitude") String latitude,
                                     @Query("longitude") String longitude);

    @GET("api/dealsDepartmentWise")
    Call<DealsResponse> getDealsDepartmentWise(@Query("gender") String gender,
                                               @Query("departmentId") String departmentId,
                                               @Query("package") int package_,
                                               @Query("brandType") int brandType,
                                               @Query("latitude") String latitude,
                                               @Query("longitude") String longitude);

    @GET("api/parlorHomeDepartmentv21")
    Call<HomeResponse> getSalonStuff(@Query("parlorId") String parlorId, @Query("userId") String userId);


  @GET("api/parlorHomeDepartmentv22")
    Call<SalonsDepartmentsResponse> getSalonsDepartments(@Query("parlorId") String parlorId, @Query("userId") String userId);

    @GET
    Call<MemberShip_Response> getMemberShipData(@Url String url);


    @POST("loggedapi/useLoyalityPoints")
    Call<AppointmentDetailResponse> useLoyaltyPoints(@Body UseFreeBiesPost post);

    //new combo service ka aur deal ka, deal pe parlor id mat bhejo
    @GET("api/dealDetailById")
    Call<NewComboResponse> getData(@Query("dealId") String deal_id, @Query("parlorId") String parlor_id);

    @GET("api/homeDealDetailById")
    Call<NewComboResponse> getData(@Query("dealId") String deal_id,@Query("latitude") String latitude,
                                   @Query("longitude") String longitude);
    @POST("loggedapi/getCorporateDetail")
    Call<CorporateDetailResponse> corporateGetDetail(@Body CorporateDetailPost post);

    @POST("loggedapi/sendCorporateOtp")
    Call<CorporateSendResponse> corporateSendOtp(@Body CorporateSendOtp post);

    @POST("loggedapi/verifyCorporateOtp")
    Call<CorporateVerifyResponse> corporateVerifyOtp(@Body CorporateVerifyPost post);

    @POST("loggedapi/requestCorporateAccount")
    Call<CorporateRequestResponse> corporateRequest(@Body CorporateRequestPost post);

    @POST("loggedapi/updateCorporateServices")
    Call<UpgradeResponse> corporateUpgrade(@Body UpgradePost post);

    @GET("api/parlorRatings")
    Call<com.beusalons.android.Model.SalonReview.Response> getResponse(@Query("parlorId") String parlorId);

    @POST("loggedapi/employeeForReview")
    Call<com.beusalons.android.Model.CustomerReview.Response> getResponse(@Body com.beusalons.android.Model.CustomerReview.Post post);


    @POST("loggedapi/userServiceAvailable")
    Call<ServiceAvailable_response> getRemainingService(@Body ServiceAvailable_post post);

    @POST("loggedapi/fbCheckIn")
    Call<FacebookCheckinResponse> reqCheckin(@Body FacebookCheckinPost post);

    @POST("loggedapi/membershipDetail")
    Call<MemberShipDetails_Response> getMemberShipDetails(@Body MembershipDetailsPost membershipDetailsPost);

    @POST("loggedapi/addUserToMembership")
    Call<AddContact_response> addMemberShipContact(@Body AddUserContactNumber_post addUserContactNumber_post);


    @GET("api/parlors")
    Call<DealSalonListResponse> getMembershipParlor(@Query("latitude") String latitude,
                                                    @Query("longitude") String longitude,
                                                    @Query("serviceCodes") String serviceCode);


    @POST("loggedapi/updateHelpfulRating")
    Call<HelpFull_Response> helpFulPost(@Body Helpfull_post helpfull_post);

    @POST("loggedapi/couponCodes")
    Call<Coupon_Response> couponGetData(@Body Coupon_post coupon_post);
    @POST("loggedapi/selectEmployee")
    Call<SelectEmployeeResponse> getArtist(@Body SelectEmployeePost selectEmployeePost);
    @POST("loggedapi/updateEmployee")
    Call<SelectedResponse> sendData(@Body SelectedPost post);

    //all songs
    @GET("/mp3/songs")
    Call<AllSongs> getAllSongs();

    @GET("api/getUserDetailByPhoneNumber")
    Call<LoginResponse> getOtherUserDetail(@Query("phoneNumber") String phoneNumber);
    //    salon mai baz rahe songs
//    @GET("/mp3/playlist")
//    Call<> getSongs();
    @POST("loggedapi/showSubscription")
    Call<SubscriptionResponse> getSubscription(@Body Coupon_post post);

    @GET("api/newHomePagev2")
    Call<ApiResponse> getData(@Query("latitude") String latitude,
                              @Query("longitude") String longitude,
                              @Query("userId") String userId,
                              @Query("page") int page,
                              @Query("rating") String rating,
                              @Query("price") String price,
                              @Query("gender") String gender,
                              @Query("sort") String sort,
                              @Query("brands") String brands);

    @POST("loggedapi/getQuestionAnswers")
    Call<com.beusalons.android.Model.AboutUser.Response> getData(@Body UserIDAccessTokenPost post);

    @POST("loggedapi/submitQuestionAnswers")
    Call<SendResponse> sendData(@Body SendPost post);

    @POST("/loggedapi/getHomePageBottomSheet")
    Call<HomePageBottomSheetModel> getHomeBottomSheetData(@Body UserIDAccessTokenPost post);

    @GET("api/getImagesOfUpdate")
    Call<UpdateAppResponse> getData();

    @GET("api/getSalonFilter")
    Call<FilterResponse> getFilterData();
    @POST("/loggedapi/addServicesToUserCart")
    Call<AddService_response> addServicetoCart(@Body UserCart_post post);

    @POST("/loggedapi/subscriptionHistory")
    Call<Subscription_response> getSubscriptionHistory(@Body Subscription_post post);


    @POST("/api/onAppClose")
    Call<AppCloseResponse> onAppClose(@Body Subscription_post post);


//artist api

    @POST("portfolio/getArtistProfile")
    Call<Artist_response> getArtistProfile(@Body ArtistProfile_post artistProfilePost);

    @POST("portfolio/followArtist")
    Call<Artist_response> followArtist(@Body FollowArtist_post artistProfilePost);

    @POST("portfolio/viewProfileProjects")
    Call<Project_response> getProfileProject(@Body ProjectData_post request);


    @POST("portfolio/likePost")
    Call<ResponseLike> postLike(@Body PostLike post);


    @POST("portfolio/viewProfileCollection")
    Call<CollectionUser_response> getCollection(@Body ProjectData_post request);
}
