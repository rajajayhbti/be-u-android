package com.beusalons.android.Model.newServiceDeals.NewCombo;

import com.beusalons.android.Model.DealsServices.ParlorTypes;

import java.util.List;

/**
 * Created by myMachine on 6/17/2017.
 */

//--
public class Service_ {

    private String name;
    private int menuPrice;
    private int price;
    private int serviceCode;
    private String serviceId;

    private double tax;
    private int estimatedTime;
    private int priceId;
    private String dealId;
    private String dealType;

    private String brandTitle;
    private List<Brand> brands = null;

    private boolean maxSaving;
    private boolean popularChoice;
    private String lowest;
    private String saveUpto;
    private List<ParlorTypes> parlorTypes;

    public List<ParlorTypes> getParlorTypes() {
        return parlorTypes;
    }

    public void setParlorTypes(List<ParlorTypes> parlorTypes) {
        this.parlorTypes = parlorTypes;
    }

    public String getLowest() {
        return lowest;
    }

    public void setLowest(String lowest) {
        this.lowest = lowest;
    }

    public String getSaveUpto() {
        return saveUpto;
    }

    public void setSaveUpto(String saveUpto) {
        this.saveUpto = saveUpto;
    }

    private boolean isCheck= false;

    public boolean isMaxSaving() {
        return maxSaving;
    }

    public void setMaxSaving(boolean maxSaving) {
        this.maxSaving = maxSaving;
    }

    public boolean isPopularChoice() {
        return popularChoice;
    }

    public void setPopularChoice(boolean popularChoice) {
        this.popularChoice = popularChoice;
    }

    public String getBrandTitle() {
        return brandTitle;
    }

    public void setBrandTitle(String brandTitle) {
        this.brandTitle = brandTitle;
    }

    public List<Brand> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(int menuPrice) {
        this.menuPrice = menuPrice;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(int serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public int getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(int estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public int getPriceId() {
        return priceId;
    }

    public void setPriceId(int priceId) {
        this.priceId = priceId;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }
}
