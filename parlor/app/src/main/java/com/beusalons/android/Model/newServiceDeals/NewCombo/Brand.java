package com.beusalons.android.Model.newServiceDeals.NewCombo;

import com.beusalons.android.Model.DealsServices.ParlorTypes;

import java.util.List;

/**
 * Created by myMachine on 6/17/2017.
 */

//--
public class Brand {

    private String brandId;
    private String brandName;
    private int price;
    private String productTitle;
    private List<Product> products = null;
    private double ratio;
    private int menuPrice;
    private boolean maxSaving;
    private boolean popularChoice;
    private String lowest;
    private String saveUpto;
    private List<ParlorTypes> parlorTypes;

    private boolean isCheck= false;

    public List<ParlorTypes> getParlorTypes() {
        return parlorTypes;
    }

    public void setParlorTypes(List<ParlorTypes> parlorTypes) {
        this.parlorTypes = parlorTypes;
    }

    public String getLowest() {
        return lowest;
    }

    public void setLowest(String lowest) {
        this.lowest = lowest;
    }

    public String getSaveUpto() {
        return saveUpto;
    }

    public void setSaveUpto(String saveUpto) {
        this.saveUpto = saveUpto;
    }

    public boolean isMaxSaving() {
        return maxSaving;
    }

    public void setMaxSaving(boolean maxSaving) {
        this.maxSaving = maxSaving;
    }

    public boolean isPopularChoice() {
        return popularChoice;
    }

    public void setPopularChoice(boolean popularChoice) {
        this.popularChoice = popularChoice;
    }

    public double getRatio() {
        return ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

    public int getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(int menuPrice) {
        this.menuPrice = menuPrice;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
