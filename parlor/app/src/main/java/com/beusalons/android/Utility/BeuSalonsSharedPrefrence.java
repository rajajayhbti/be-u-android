package com.beusalons.android.Utility;

import android.content.Context;
import android.content.SharedPreferences;

import com.beusalons.android.Model.HomeFragmentModel.DiscountRules;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by Ashish on 03/08/2017.
 */
public class BeuSalonsSharedPrefrence {
    private static final String VERSION_CODE = "versioncode";
    private static SharedPreferences sharedPref, user_details_preference,discountSh,version;

    private static final String PREF_NAME = "beusalons_preferences2";
    private static final String PREF_USER = "userDetails";
    private static final String PREF_VERSION = "versioncode";

    public static final String LOGIN_DETAILS = "login";
    public static final String SETTINGS = "settings";
    public static final String LOGINPREF = "loginPrefs";
    public static final String NUMBER = "mobile";
    public static final String PASSWORD = "password";
    public static final String ADDRESS_LOCALTY = "local_address";
    public static final String FULL_ADDRESS = "full_address";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    public static final String IsLogin = "isLoginV5";               //earlier v4

    public static final String ISFIRST_SEEN_LIST = "isFirstSalonList";
    public static final String MY_LOYALTY_POINTS="myLoyaltyPoints";
    public static final String APP_OPEN_COUNT="openCount";
    public static final String FREEBIES_COUNT="freebiecount";
    public static final String FREE_HEIRCUT_BAR="freeHairCutBar";

    public static final String NOTIFICATION_ID= "notification_id";

    public static final String VERIFY_CORPORATE="verify_corporate";
    public static final String CORPORATE_ID= "corporate_id";

    public static final String PREFS_DIS = "DISCOUNT_TEST";
    public static final String DISCOUNT_PER = "Discont_per";
    public static final String CHECKED_PARLOR = "checkedin_parlor";
    public static final String PROFILE_PIC = "profilePic";
    public static final String SUBSCRIPTION_REFER = "referCode";
    public static final String IS_SUBSCRIBED = "issubscribe";
    public static final String REFER_MSG = "refermsg";
    public static final String SUBS_BALANCE = "subsbalance";

    // This  is use for user home page discount array.
    public static void saveDiscount(Context context, List<DiscountRules> favorites) {

        SharedPreferences.Editor editor;


        editor = discountSh.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);

        editor.putString(DISCOUNT_PER, jsonFavorites);

        editor.commit();
    }


    public static List<DiscountRules> getDiscount(Context context) {
        List<DiscountRules> favorites;

        discountSh = context.getSharedPreferences(PREFS_DIS,
                Context.MODE_PRIVATE);

        if (discountSh.contains(DISCOUNT_PER)) {
            String jsonDis = discountSh.getString(DISCOUNT_PER, null);
            Gson gson = new Gson();
            DiscountRules[] favoriteItems = gson.fromJson(jsonDis,
                    DiscountRules[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<DiscountRules>(favorites);
        } else
            return null;

        return (List<DiscountRules>) favorites;
    }

    public static void clearDiscountData() {
         discountSh.edit().clear().apply();
    }


    public static void init(Context context) {
        discountSh = context.getSharedPreferences(PREFS_DIS,
                Context.MODE_PRIVATE);
        sharedPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        user_details_preference= context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        version= context.getSharedPreferences(PREF_VERSION, Context.MODE_PRIVATE);
    }

    public static void clearData() {

        sharedPref.edit().clear().apply();
        user_details_preference.edit().clear().apply();
    }



    public static  void saveAddress(Context context,String localAddress,String address,String lat,String longitude){

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(FULL_ADDRESS,address);
        editor.putString(ADDRESS_LOCALTY,localAddress);
        editor.putString(LATITUDE,lat);
        editor.putString(LONGITUDE,longitude);
        editor.apply();
    }


    public static void setNotificationId(String id){

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(NOTIFICATION_ID, id);
        editor.apply();
    }

    public static String getNotificationId(){
        return sharedPref.getString(NOTIFICATION_ID, null);
    }

    public static String getUserName(){
        return user_details_preference.getString("name", "");
    }

    public static double getServiceTax(){
        return user_details_preference.getFloat("service_tax", 1.18f);
    }

    public static void setServiceTax(float value){

        SharedPreferences.Editor editor = user_details_preference.edit();
        editor.putFloat("service_tax", value);
        editor.apply();
    }


    public static void saveLatLong(Context context, String lat, String longitude){

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(LATITUDE,lat);
        editor.putString(LONGITUDE,longitude);
        editor.apply();
    }

    public static void setReferCode(Context context, String code){
        SharedPreferences.Editor editor = user_details_preference.edit();
        editor.putString("referCode", code);
        editor.apply();
    }

    public static void setNormalMessage(String code){
        SharedPreferences.Editor editor = user_details_preference.edit();
        editor.putString("normalMessage", code);
        editor.apply();
    }

    public static String getNormalMessage(){
        return user_details_preference.getString("normalMessage", null);
    }


    public static void setFirebaseSuccess(boolean value){
        SharedPreferences.Editor editor = user_details_preference.edit();
        editor.putBoolean("firebase_", value);
        editor.apply();
    }

    public static boolean getFirebaseSuccess(){
        return user_details_preference.getBoolean("firebase_", false);
    }
    public static boolean getIsSubscribed(){
        return sharedPref.getBoolean(IS_SUBSCRIBED,false);
    }
    public static void setisSubscribe(boolean issubs){
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(IS_SUBSCRIBED, issubs);
        editor.apply();
    }

    public static void setCorporateReferCode(String code){

        SharedPreferences.Editor editor = user_details_preference.edit();
        editor.putString("corporateReferCode", code);
        editor.apply();
    }

    public static String getCorporateReferCode(){

        return user_details_preference.getString("corporateReferCode", null);
    }



    public static void saveSettings(Context context, int Settings) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(SETTINGS, Settings);
        editor.apply();
    }

    public static int getSettings(Context context) {

        if (sharedPref.contains(SETTINGS)) {
            return sharedPref.getInt(SETTINGS, 0);
        }
        return 0;
    }

    public static void saveCredentials(Context context, String mobile, String password) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(NUMBER, mobile);
        editor.putString(PASSWORD, password);
        editor.apply();
    }

    public static String getMobile(Context context) {

        return sharedPref.getString(NUMBER, null);
    }

    public static String getPassword(Context context) {

        return sharedPref.getString(PASSWORD, null);

    }
    public static  String getFullAddress(){
        return  sharedPref.getString(FULL_ADDRESS,null);
    }
    public static String getAddressLocalty(){
        return  sharedPref.getString(ADDRESS_LOCALTY,null);
    }


    public static String getLatitude(){
        return sharedPref.getString(LATITUDE,null);
    }
    public static String getLongitude(){
        return  sharedPref.getString(LONGITUDE,null);
    }

    public static void setLogin(boolean isLogin){
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(IsLogin, isLogin);
        editor.apply();

    }

    public static String getReferCode(){

        return user_details_preference.getString("referCode", null);
    }

    public static String getUserId(){

        return user_details_preference.getString("userId", null);
    }

    public static String getAccessToken(){

        return user_details_preference.getString("accessToken", null);
    }

    public static Boolean getOfferDialog(){

        return user_details_preference.getBoolean("offerDialog", false);
    }

    public static void setOfferDialog(Context context, Boolean value){

        SharedPreferences.Editor editor = user_details_preference.edit();
        editor.putBoolean("offerDialog", value );
        editor.apply();
    }

    public static void setCorporateId(String value){

        SharedPreferences.Editor editor = user_details_preference.edit();
        editor.putString(CORPORATE_ID, value );
        editor.apply();
    }

    public static String getCorporateId(){

        return user_details_preference.getString(CORPORATE_ID, null);
    }

    public static void setVerifyCorporate(Boolean value){

        SharedPreferences.Editor editor = user_details_preference.edit();
        editor.putBoolean(VERIFY_CORPORATE, value );
        editor.apply();
    }

    public static Boolean getVerifyCorporate(){

        return user_details_preference.getBoolean(VERIFY_CORPORATE, false);
    }


    public static void setUserType(Context context, Boolean value){

        SharedPreferences.Editor editor = user_details_preference.edit();
        editor.putBoolean("userType", value );
        editor.apply();

    }


    public static String getUserEmail(){

        return user_details_preference.getString("emailId", "");
    }

    public static String getUserPhone(){

        return user_details_preference.getString("phoneNumber", "");
    }

    public static boolean getUserType(){

        return user_details_preference.getBoolean("userType", false);
    }


    public static boolean isLogin(){
        return  sharedPref.getBoolean(IsLogin,false);
    }

    public static String getGender(){
        return user_details_preference.getString("gender", "");
    }

    public static String getPhoneNumber(){
        return user_details_preference.getString("phoneNumber", "");
    }


    public static void setIsfirstSeenList(boolean isfirstSeenList){
        SharedPreferences.Editor editor=sharedPref.edit();
        editor.putBoolean(ISFIRST_SEEN_LIST,isfirstSeenList);
        editor.apply();
    }
    public static  boolean isfirstSeenList(){
        return  sharedPref.getBoolean(ISFIRST_SEEN_LIST,false);
    }
    public static void setMyLoyaltyPoints(int myLoyaltyPoints){
        SharedPreferences.Editor editor=sharedPref.edit();
        editor.putInt(MY_LOYALTY_POINTS,myLoyaltyPoints);
        editor.apply();

    }

    public static void setMembershipName(String membership_name){

        SharedPreferences.Editor editor=sharedPref.edit();
        editor.putString("membership_name" , membership_name);
        editor.apply();
    }

    public static String getMembershipName(){

        return sharedPref.getString("membership_name", "");
    }


    public static void setMembershipPoints(float membershipPoints){

        SharedPreferences.Editor editor=sharedPref.edit();
        editor.putFloat("membership_points" , membershipPoints);
        editor.apply();
    }

    public static float getMembershipPoints(){

        return sharedPref.getFloat("membership_points", 0);
    }


    public static int getMyLoyaltyPoints(){
        return sharedPref.getInt(MY_LOYALTY_POINTS,0);
    }


    public static void setAppOpenCount (int appOpenCount){
        SharedPreferences.Editor editor=sharedPref.edit();
        editor.putInt(APP_OPEN_COUNT,appOpenCount);
        editor.apply();

    }
    public static int getAppOpenCount(){
        return sharedPref.getInt(APP_OPEN_COUNT,-1);
    }


    public static void setFreebieCount(int freebieCount){

        SharedPreferences.Editor editor=sharedPref.edit();
        editor.putInt(FREEBIES_COUNT,freebieCount);
        editor.apply();
    }
    public static int getFreebieCount(){
       return sharedPref.getInt(FREEBIES_COUNT,0);
    }


    public static void  setFreeHeircutBar(int minimumprice){
        SharedPreferences.Editor editor=sharedPref.edit();
        editor.putInt(FREE_HEIRCUT_BAR,minimumprice);
        editor.apply();

    }
    public static int getFreeHeircutBar(){
        return sharedPref.getInt(FREE_HEIRCUT_BAR,0);
    }

    public static void  setProfilePic(String profilePic){
        SharedPreferences.Editor editor=sharedPref.edit();
        editor.putString(PROFILE_PIC,profilePic);
        editor.apply();

    }
    public static String getProfilePic(){
        return sharedPref.getString(PROFILE_PIC,"");
    }
    public static void  setSubsRefer(String refer){
        SharedPreferences.Editor editor=sharedPref.edit();
        editor.putString(SUBSCRIPTION_REFER,refer);
        editor.apply();

    }
    public static String getSubsRefer(){
        return sharedPref.getString(SUBSCRIPTION_REFER,"");
    }
   /* public static void setCheckedParlor(String parlorId){

    }*/
   public static void setSubsReferMsg(String msg){
       SharedPreferences.Editor editor=sharedPref.edit();
       editor.putString(REFER_MSG,msg);
       editor.apply();
   }
   public static String getSubsReferMsg(){
       return sharedPref.getString(REFER_MSG,"");
   }

   public static void setSubsBalance(int balance){
       SharedPreferences.Editor editor=sharedPref.edit();
       editor.putInt(SUBS_BALANCE,balance);
       editor.apply();
   }
   public static int getSubsBalance(){
       return sharedPref.getInt(SUBS_BALANCE,0);
   }

   public static void setVersionCode(int balance){
       SharedPreferences.Editor editor=version.edit();
       editor.putInt(VERSION_CODE,balance);
       editor.apply();
   }
   public static int getVersionCode(){
       return version.getInt(VERSION_CODE,0);
   }

}
