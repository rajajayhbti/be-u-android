package com.beusalons.android.Model.SalonReview;

/**
 * Created by Ajay on 12/4/2017.
 */

public class HelpFull_Response {

   private boolean success;
    private  String data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
