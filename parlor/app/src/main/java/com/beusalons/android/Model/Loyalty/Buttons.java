package com.beusalons.android.Model.Loyalty;

/**
 * Created by Ashish Sharma on 9/11/2017.
 */

public class Buttons {
    private String buttonTitle;
    private String buttonAction;
    private String buttonColor;

    public String getButtonTitle() {
        return buttonTitle;
    }

    public void setButtonTitle(String buttonTitle) {
        this.buttonTitle = buttonTitle;
    }

    public String getButtonAction() {
        return buttonAction;
    }

    public void setButtonAction(String buttonAction) {
        this.buttonAction = buttonAction;
    }

    public String getButtonColor() {
        return buttonColor;
    }

    public void setButtonColor(String buttonColor) {
        this.buttonColor = buttonColor;
    }
}
