package com.beusalons.android.Model.Appointments;

/**
 * Created by myMachine on 11/28/2016.
 */

public class UpcomingAppointmentsPost {

    private String userId;
    private String accessToken;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
