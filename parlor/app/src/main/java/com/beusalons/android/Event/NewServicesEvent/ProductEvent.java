package com.beusalons.android.Event.NewServicesEvent;

/**
 * Created by myMachine on 6/6/2017.
 */

public class ProductEvent {

    private int product_index;
    private int brand_index;


    public ProductEvent(int product_index, int brand_index){

        this.product_index= product_index;
        this.brand_index= brand_index;
    }

    public int getProduct_index() {
        return product_index;
    }

    public int getBrand_index() {
        return brand_index;
    }
}
