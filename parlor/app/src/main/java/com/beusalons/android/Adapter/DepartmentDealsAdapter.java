package com.beusalons.android.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.DealsServicesActivity;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.DealsData.DealsData;
import com.beusalons.android.Model.newServiceDeals.Category;
import com.beusalons.android.Model.newServiceDeals.Department;
import com.beusalons.android.R;
import com.bumptech.glide.Glide;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ajay on 6/8/2017.
 */

public class DepartmentDealsAdapter extends RecyclerView.Adapter<DepartmentDealsAdapter.ViewHolder> {


    private Context context;
    private List<Category> list;
    private String gender, departmentId, department_name;
    AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;
    ArrayList<Department> departments;

private DealsData dealsData;
    public DepartmentDealsAdapter(Context context, List<Category> list, String gender,
                                  String departmentId, String department_name, ArrayList<Department> departments,DealsData dealData){

        this.context= context;
        this.list= list;
        this.gender= gender;
        this.departmentId= departmentId;
        this.department_name= department_name;
        this.departments=departments;
        logger = AppEventsLogger.newLogger(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        this.dealsData=dealData;

    }


    @Override
    public void onBindViewHolder(DepartmentDealsAdapter.ViewHolder holder, final int position) {
        if (list.size() > position) {
            final Category category = list.get(position);

            holder.txt_name.setText(category.getName());


            Glide.with(context)
                    .load(category.getImage())
//                .centerCrop()
                    .into(holder.img_);

            holder.linear_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    logDealCategoryEvent(department_name+"- "+list.get(position).getName()+"- "+gender);
                    logDealCategoryFireBaseEvent(department_name+"- "+list.get(position).getName()+"- "+gender);
                    Intent intent = new Intent(context, DealsServicesActivity.class);
                    intent.putExtra("department_id", departmentId);
                    intent.putExtra("department_name", department_name);
                    intent.putExtra("gender", gender);
                    intent.putExtra("position", position);              //viewpager ka position
                    intent.putExtra("dealsData",new Gson().toJson(dealsData));
                    intent.putExtra("departments",new Gson().toJson(departments));
                    context.startActivity(intent);
                }
            });
        } else {

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            if (list.size() == 4) {
                if (position == 5) {
                    params.setMargins(-1, 1, 1, 1);
                    holder.linear_click.setLayoutParams(params);
                }
            } else if (list.size() == 1 && position == 2) {
                params.setMargins(-1, 1, 1, 1);
                holder.linear_click.setLayoutParams(params);
            } else if (list.size() == 7 && position == 7) {
                holder.linear_click.setLayoutParams(params);
            }


        }
    }



    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_name;
        private ImageView img_;
        private LinearLayout linear_click;


        public ViewHolder(View itemView) {
            super(itemView);
            txt_name= (TextView)itemView.findViewById(R.id.txt_name);
            img_= (ImageView)itemView.findViewById(R.id.img_);
            linear_click= (LinearLayout)itemView.findViewById(R.id.linear_click);

        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    @Override
    public int getItemCount() {
        if (list.size()==1 || list.size()==2){

            return 3;
        }else if (list.size()==4 || list.size()==5){
            return 6;
        }else if (list.size()==7 || list.size()==8){
            return 9;
        }else
            return list.size();
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logDealCategoryEvent (String name) {
        Log.e("servicecategory","fine"+name);
        Bundle params = new Bundle();
        params.putString("Name", name);
        logger.logEvent(AppConstant.DealCategory, params);
    }

    public void logDealCategoryFireBaseEvent (String name) {
        Log.e("servicecategoryFirbase","fine");
        Bundle params = new Bundle();
        params.putString("Name", name);
        mFirebaseAnalytics.logEvent(AppConstant.DealCategory, params);
    }

    @Override
    public DepartmentDealsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.deals_department_service_grid, parent, false);
        return new DepartmentDealsAdapter.ViewHolder(view);
    }

}
