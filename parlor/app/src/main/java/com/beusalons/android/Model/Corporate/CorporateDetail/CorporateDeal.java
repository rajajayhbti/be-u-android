package com.beusalons.android.Model.Corporate.CorporateDetail;

/**
 * Created by myMachine on 7/31/2017.
 */

public class CorporateDeal {

    private String start;
    private String end;
    private String image;

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
