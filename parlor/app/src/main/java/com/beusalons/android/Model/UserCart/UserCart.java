package com.beusalons.android.Model.UserCart;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 5/30/2017.
 */

public class UserCart {

    private String cartType;
    //parlor stuff
    private String parlorId;                              //parlor id
    private String parlorName;                //parlor name
    private int parlorType;

    private String gender;
    private double rating;                      //parlor rating

    private String openingTime;
    private String closingTime;

    private String address1;                             //parlor address
    private String address2;                         //parlor locality address

    //---------------------------------------------------------------------------------------------------------------

    private String date_time;               //date time.. user selected or any other

    private int total;              //total services amount

    private List<UserServices> servicesList= new ArrayList<>();                //save the deal-- services-- membership

    //quantity ke liye hai yeh-- contains service id's, parlordealid....stuff
    private String deal_id=null;                 //package ke case ke liye
    private String service_id= null;
    private int int_deal_id;

    public int getInt_deal_id() {
        return int_deal_id;
    }

    public void setInt_deal_id(int int_deal_id) {
        this.int_deal_id = int_deal_id;
    }

    public String getDeal_id() {
        return deal_id;
    }

    public void setDeal_id(String deal_id) {
        this.deal_id = deal_id;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getParlorType() {
        return parlorType;
    }

    public void setParlorType(int parlorType) {
        this.parlorType = parlorType;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public String getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(String closingTime) {
        this.closingTime = closingTime;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getCartType() {
        return cartType;
    }

    public void setCartType(String cartType) {
        this.cartType = cartType;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }

    public String getParlorName() {
        return parlorName;
    }

    public void setParlorName(String parlorName) {
        this.parlorName = parlorName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<UserServices> getServicesList() {
        return servicesList;
    }

    public void setServicesList(List<UserServices> servicesList) {
        this.servicesList = servicesList;
    }
}
