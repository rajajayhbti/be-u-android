package com.beusalons.android.Model.selectArtist;

/**
 * Created by Ashish Sharma on 12/29/2017.
 */

public class Employee {
    private String employeeId;
    private String name;
    private String image;
    private float rating;
    private int clientServed;
    private  boolean isSelected;
    private String artistId= null;

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getClientServed() {
        return clientServed;
    }

    public void setClientServed(int clientServed) {
        this.clientServed = clientServed;
    }
}
