package com.beusalons.android.Model.Verify_Otp;

/**
 * Created by Ajay on 12/30/2016.
 */

public class CustomizeDealsChild {
    private String name;

    public CustomizeDealsChild(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
