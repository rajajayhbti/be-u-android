package com.beusalons.android.Model.Appointments;

import java.util.List;

/**
 * Created by myMachine on 11/28/2016.
 */

public class UpcomingData {


    private double otherCharges;
    private double subtotal;
    private double discount;
    private int appointmentType;
    private int paymentMethod;
    private double membershipDiscount;
    private double payableAmount;
    private String appointmentId;
    private int parlorAppointmentId;
    private String appointmentStatus;
    private double creditUsed;
    private double tax;
    private String startsAt;
    private double estimatedTime;
    private double loyalityPoints;
    private List<Services> services = null;
    private List<Products> products = null;
    private String parlorName;
    private String parlorAddress;
    private UpcomingReview review;
    private String parlorId;
    private double parlorLatitude;
    private double parlorLongitude;

    //custom variables
    private String date;
    private String time;

    private String openingTime;
    private String closingTime;
    private String currentTime;

    public double getOtherCharges() {
        return otherCharges;
    }

    public void setOtherCharges(double otherCharges) {
        this.otherCharges = otherCharges;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(int appointmentType) {
        this.appointmentType = appointmentType;
    }

    public int getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(int paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public double getMembershipDiscount() {
        return membershipDiscount;
    }

    public void setMembershipDiscount(double membershipDiscount) {
        this.membershipDiscount = membershipDiscount;
    }

    public double getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(double payableAmount) {
        this.payableAmount = payableAmount;
    }

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public int getParlorAppointmentId() {
        return parlorAppointmentId;
    }

    public void setParlorAppointmentId(int parlorAppointmentId) {
        this.parlorAppointmentId = parlorAppointmentId;
    }

    public String getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(String appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }

    public double getCreditUsed() {
        return creditUsed;
    }

    public void setCreditUsed(double creditUsed) {
        this.creditUsed = creditUsed;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public String getStartsAt() {
        return startsAt;
    }

    public void setStartsAt(String startsAt) {
        this.startsAt = startsAt;
    }

    public double getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(double estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public double getLoyalityPoints() {
        return loyalityPoints;
    }

    public void setLoyalityPoints(double loyalityPoints) {
        this.loyalityPoints = loyalityPoints;
    }

    public List<Services> getServices() {
        return services;
    }

    public void setServices(List<Services> services) {
        this.services = services;
    }

    public List<Products> getProducts() {
        return products;
    }

    public void setProducts(List<Products> products) {
        this.products = products;
    }

    public String getParlorName() {
        return parlorName;
    }

    public void setParlorName(String parlorName) {
        this.parlorName = parlorName;
    }

    public String getParlorAddress() {
        return parlorAddress;
    }

    public void setParlorAddress(String parlorAddress) {
        this.parlorAddress = parlorAddress;
    }

    public UpcomingReview getReview() {
        return review;
    }

    public void setReview(UpcomingReview review) {
        this.review = review;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }

    public double getParlorLatitude() {
        return parlorLatitude;
    }

    public void setParlorLatitude(double parlorLatitude) {
        this.parlorLatitude = parlorLatitude;
    }

    public double getParlorLongitude() {
        return parlorLongitude;
    }

    public void setParlorLongitude(double parlorLongitude) {
        this.parlorLongitude = parlorLongitude;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public String getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(String closingTime) {
        this.closingTime = closingTime;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }
}
