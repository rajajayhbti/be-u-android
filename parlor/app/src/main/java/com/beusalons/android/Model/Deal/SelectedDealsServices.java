package com.beusalons.android.Model.Deal;

import java.io.Serializable;

/**
 * Created by Ajay on 1/5/2017.
 */

public class SelectedDealsServices implements Serializable {


    private String dealName;
    private  Integer menuPrice;
    private Integer dealPrice;
    private String parlorId;

    private Integer dealIdParlor;
    private String categoryName;
    private Integer serviceCodeId;
    private String serviceName;
    private Integer quantity;
    private  String weekDay;


    public String getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(String weekDay) {
        this.weekDay = weekDay;
    }

    public String getDealName() {
        return dealName;
    }

    public void setDealName(String dealName) {
        this.dealName = dealName;
    }

    public Integer getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(Integer menuPrice) {
        this.menuPrice = menuPrice;
    }

    public Integer getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(Integer dealPrice) {
        this.dealPrice = dealPrice;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }

    public Integer getDealIdParlor() {
        return dealIdParlor;
    }

    public void setDealIdParlor(Integer dealIdParlor) {
        this.dealIdParlor = dealIdParlor;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Integer getServiceCodeId() {
        return serviceCodeId;
    }

    public void setServiceCodeId(Integer serviceCodeId) {
        this.serviceCodeId = serviceCodeId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
