package com.beusalons.android.Model;

import java.util.List;

/**
 * Created by Ajay on 1/20/2017.
 */

public class DealDeatilsPost {

    private List<DealsIdPost> dealIds;
    private double latitude;
    private double longitude;

    private String userId;

    public DealDeatilsPost(List<DealsIdPost> dealIds, double latitude, double longitude,String userId) {
        this.dealIds = dealIds;
        this.latitude = latitude;
        this.longitude = longitude;
        this.userId=userId;
    }

    public DealDeatilsPost() {
    }

    public List<DealsIdPost> getDealIds() {
        return dealIds;
    }

    public void setDealIds(List<DealsIdPost> dealIds) {
        this.dealIds = dealIds;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


}
