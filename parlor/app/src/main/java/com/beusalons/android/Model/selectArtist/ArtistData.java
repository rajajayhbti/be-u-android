package com.beusalons.android.Model.selectArtist;

import java.util.ArrayList;

/**
 * Created by Ashish Sharma on 12/29/2017.
 */

public class ArtistData {
    private ArrayList<Services> services;

    public ArrayList<Services> getServices() {
        return services;
    }

    public void setServices(ArrayList<Services> services) {
        this.services = services;
    }
}
