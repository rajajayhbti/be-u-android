package com.beusalons.android.Model.Loyalty;

/**
 * Created by myMachine on 8/24/2017.
 */

public class CorporateReferalMessage {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
