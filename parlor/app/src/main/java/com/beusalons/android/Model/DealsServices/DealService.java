package com.beusalons.android.Model.DealsServices;

import com.beusalons.android.Model.newServiceDeals.NewCombo.Selector;

import java.util.List;

/**
 * Created by Ajay on 6/9/2017.
 */

public class DealService {

    private int dealId;
    private String parlorDealId;

    private String name;
    private String description;
    private String shortDescription;
    private int menuPrice;
    private int price;
    private int dealPrice;
    private String dealType;
    private List<Selector> selectors = null;
    private List<ParlorTypes> parlorTypes;
    private boolean allHairLength=false;

    private int quantity=0;

    private String startAt;
    private String save;

    public String getStartAt() {
        return startAt;
    }

    public void setStartAt(String startAt) {
        this.startAt = startAt;
    }

    public String getSave() {
        return save;
    }

    public void setSave(String save) {
        this.save = save;
    }

    public List<ParlorTypes> getParlorTypes() {
        return parlorTypes;
    }

    public void setParlorTypes(List<ParlorTypes> parlorTypes) {
        this.parlorTypes = parlorTypes;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getDealId() {
        return dealId;
    }

    public void setDealId(int dealId) {
        this.dealId = dealId;
    }

    public String getParlorDealId() {
        return parlorDealId;
    }

    public void setParlorDealId(String parlorDealId) {
        this.parlorDealId = parlorDealId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public int getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(int menuPrice) {
        this.menuPrice = menuPrice;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(int dealPrice) {
        this.dealPrice = dealPrice;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public List<Selector> getSelectors() {
        return selectors;
    }

    public void setSelectors(List<Selector> selectors) {
        this.selectors = selectors;
    }

    public boolean isAllHairLength() {
        return allHairLength;
    }

    public void setAllHairLength(boolean allHairLength) {
        this.allHairLength = allHairLength;
    }
}
