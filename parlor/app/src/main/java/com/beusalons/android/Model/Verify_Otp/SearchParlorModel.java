package com.beusalons.android.Model.Verify_Otp;

/**
 * Created by Robbin Singh on 15/11/2016.
 */

public class SearchParlorModel {

    private String parlorId;                // parlor id set hai isme
    private String title;                   //isme location name set kiya hai
    private String placeId;                         // place id google places api ke liye use ki gayi hai
    private String parlor_location;                     //if parlor id toh parlor_location set karna hai

    public String getParlor_location() {
        return parlor_location;
    }

    public void setParlor_location(String parlor_location) {
        this.parlor_location = parlor_location;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String location) {
        this.title = location;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }
}
