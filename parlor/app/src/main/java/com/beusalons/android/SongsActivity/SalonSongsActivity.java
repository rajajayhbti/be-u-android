package com.beusalons.android.SongsActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Adapter.Songs.SalonSongsAdapter;
import com.beusalons.android.BuildConfig;
import com.beusalons.android.Model.SongSendData;
import com.beusalons.android.Model.Songs.SalonSongs;
import com.beusalons.android.R;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.net.URISyntaxException;


public class SalonSongsActivity extends AppCompatActivity {

    public static final String SALON_NAME= "com.beusalons.salonsongactivity.salonname";
    public static final String SALON_ID= "com.beusalons.salonsongactivity.salonid";
    public static final int REQUESTCODE= 100;

    private Socket mSocket;
    {
        try {

            if(BuildConfig.DEBUG){

                mSocket = IO.socket("http://13.126.45.78:1337");
            }
            else
                mSocket = IO.socket("http://beusalons.com");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private String salon_id="", salon_name="";
    private RecyclerView recycler_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon_songs);
        Intent intent= getIntent();
        if(intent!=null){
            salon_id= intent.getStringExtra(SALON_ID);
            salon_name= intent.getStringExtra(SALON_NAME);
        }
        setToolBar();

        String id= salon_id+"music";
        mSocket.emit("musicroom", id);
        mSocket.on("musicRoomJoined", onConnect);
        mSocket.on("listForApp", songList);
        mSocket.on("userRequest", onUserRequest);

        mSocket.connect();
        Log.i("inthestuff", "on create pe");
        recycler_= findViewById(R.id.recycler_);
        LinearLayoutManager manager= new LinearLayoutManager(this);
        recycler_.setLayoutManager(manager);

        TextView txt_click= findViewById(R.id.txt_click);
        txt_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent= new Intent(view.getContext(), AllSongsActivity.class);
                intent.putExtra(AllSongsActivity.SALON_NAME, salon_name);
                startActivityForResult(intent, REQUESTCODE);
            }
        });
    }

    private Emitter.Listener onUserRequest = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            //connect hua
            Log.i("socketkastuff", "on user request");

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Toast.makeText(SalonSongsActivity.this, "Request Successful", Toast.LENGTH_SHORT).show();
                }
            });

        }
    };

    //joining the music room
    private Emitter.Listener onConnect = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            //connect hua
            Log.i("socketkastuff", "everthing is fine");
        }
    };

    //getting the song list
    private Emitter.Listener songList = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            try{
                Log.i("socketkastuff", "in the songlist listener: " +
                        args[0].toString());
                JSONObject jsonObject= (JSONObject)args[0];

                final SalonSongs songs= new Gson().fromJson(jsonObject.toString(), SalonSongs.class);
                if(songs!=null &&
                        songs.getData().size()>0){

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            SalonSongsAdapter adapter= new SalonSongsAdapter(songs.getData());
                            recycler_.setAdapter(adapter);
                        }
                    });

                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode== REQUESTCODE)
            if(resultCode== Activity.RESULT_OK){

                SongSendData data_= new SongSendData();
                data_.setUserId(BeuSalonsSharedPrefrence.getUserId());
                data_.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
                data_.setParlorId(salon_id);
                data_.setSongId(data.getIntExtra("id", 0));
                Log.i("inthesongevent", "in the onactivity result: "+salon_id);
                mSocket.emit("songRequest", new Gson().toJson(data_, SongSendData.class));

            }

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("inthestuff", "on stop pe");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("inthestuff", "on destroy pe");
        mSocket.disconnect();
        mSocket.off("musicRoomJoined", onConnect);
        mSocket.off("listForApp", songList);
        mSocket.off("userRequest", onUserRequest);
    }


    private void setToolBar(){

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(salon_name+ " Playlist");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
