package com.beusalons.android.Model.Corporate.CorporateVerifyOtp;

/**
 * Created by myMachine on 8/1/2017.
 */

public class CorporateVerifyResponse {

    private boolean success;
    private String message;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
