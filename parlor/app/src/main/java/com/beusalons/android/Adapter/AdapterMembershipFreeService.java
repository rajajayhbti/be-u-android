package com.beusalons.android.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.MymembershipDetails.FreeService;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.ParlorListActivity;
import com.beusalons.android.R;
import com.beusalons.android.Task.UserCartTask;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import java.util.List;

/**
 * Created by Ajay on 11/8/2017.
 */

public class AdapterMembershipFreeService extends RecyclerView.Adapter<AdapterMembershipFreeService.MyViewHolder> {
    private Activity activity;
    private List<FreeService> serviceList;
    UserCart saved_cart= null;

    public AdapterMembershipFreeService(Activity context, List<FreeService> list){

        this.activity= context;
        this.serviceList=list;
        saved_cart=new UserCart();

    }


    @Override
    public AdapterMembershipFreeService.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(activity).inflate(R.layout.row_free_service_, parent, false);
        return new AdapterMembershipFreeService.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
            holder.txt_service_name.setText(serviceList.get(position).getName());
           holder.txt_quantity_remaining.setText("Quantity Remaining: "+serviceList.get(position).getQuantityRemaining());

        holder.txt_free_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clearCartData();

                UserCart userCart= new UserCart();
                userCart.setCartType(AppConstant.DEAL_TYPE);

                UserServices userServices=new UserServices();


                userServices.setName(serviceList.get(position).getName());
                if (serviceList.get(position).getServiceCode()!=null)
                userServices.setService_code(Integer.valueOf(serviceList.get(position).getServiceCode().trim()));

                if (serviceList.get(position).getServiceCode()!=null)
                userServices.setPrice_id(Integer.valueOf(serviceList.get(position).getServiceCode().trim()));

                if (serviceList.get(position).getServiceId()!=null)
                userServices.setService_id(serviceList.get(position).getServiceId());
                String primary_key="";
                if (serviceList.get(position).getServiceId()!=null){
                    primary_key = serviceList.get(position).getServiceCode()+serviceList.get(position).getServiceId();

                } else primary_key = serviceList.get(position).getServiceCode();


                userServices.setPrimary_key(primary_key);
                userServices.setType(serviceList.get(position).getType());
                userServices.setMyMembershipFreeService(true);
                userServices.setRemainingTotalQuantity(serviceList.get(position).getQuantityRemaining());
                //userServices.setRemainingService(true);
                new Thread(new UserCartTask(activity, userCart, userServices, false, false)).start();
                Toast.makeText(activity,"Service Added to Cart",Toast.LENGTH_SHORT).show();





                Intent intent=new Intent(activity, ParlorListActivity.class);
                intent.putExtra("isDeal",true);
                activity.startActivity(intent);
                activity.finish();

            }
        });

    }

    private void clearCartData(){

        DB snappyDB = null;

                        /*
                        * clear snappy db only user cart notification db not clear
                        * */
        try {
            snappyDB = DBFactory.open(activity);
            if(snappyDB.exists(AppConstant.USER_CART_DB)) {
                Log.i("mainyahatukaha", " existing hai");
                saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);

            }

            for (int i=0;i<saved_cart.getServicesList().size();i++){
                if (!saved_cart.getServicesList().get(i).isMyMembershipFreeService()){
                    saved_cart.getServicesList().remove(i);
                }
            }
            snappyDB.put(AppConstant.USER_CART_DB, saved_cart);
            snappyDB.close();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return serviceList.size();
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout linearLayout_description,linearLayout_availService;
        private TextView txt_quantity_remaining,txt_service_name,txt_free_click;

        public MyViewHolder(View itemView) {
            super(itemView);
            linearLayout_description=(LinearLayout)itemView.findViewById(R.id.linearLayout_description);
            txt_quantity_remaining=(TextView)itemView.findViewById(R.id.txt_quantity_remaining);
            txt_free_click=(TextView)itemView.findViewById(R.id.txt_free_click);
            txt_service_name=(TextView)itemView.findViewById(R.id.txt_service_name);



        }
    }
}
