package com.beusalons.android.Event;

/**
 * Created by Ajay on 12/6/2017.
 */

public class CouponCodeApply {
       private  String couponCode;
       private String couponId;

    public CouponCodeApply(String couponCode,String couponId) {
        this.couponCode = couponCode;
        this.couponId=couponId;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }
}
