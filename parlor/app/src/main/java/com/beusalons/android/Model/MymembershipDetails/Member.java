package com.beusalons.android.Model.MymembershipDetails;

/**
 * Created by Ajay on 11/9/2017.
 */

public class Member {

    private String name;
    private String phoneNumber;
    private String status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
