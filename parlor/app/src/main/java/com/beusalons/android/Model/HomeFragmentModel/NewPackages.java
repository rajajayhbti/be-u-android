package com.beusalons.android.Model.HomeFragmentModel;

import java.util.List;

/**
 * Created by Ajay on 3/21/2017.
 */

public class NewPackages {


    private Integer categoryId;

    private Integer type;

    private String name;

    private List<PackageService> services = null;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PackageService> getServices() {
        return services;
    }

    public void setServices(List<PackageService> services) {
        this.services = services;
    }
}
