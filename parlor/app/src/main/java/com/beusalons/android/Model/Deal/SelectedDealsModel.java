package com.beusalons.android.Model.Deal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ajay on 1/8/2017.
 */

public class SelectedDealsModel implements Serializable {


    @SerializedName("menuPrice")
    @Expose
    private Integer menuPrice;
    @SerializedName("dealPrice")
    @Expose
    private Integer dealPrice;
    @SerializedName("parlorId")
    @Expose
    private String parlorId;

    @SerializedName("parlorName")
    @Expose
    private String parlorName;
    @SerializedName("parlorAddress1")
    @Expose
    private String parlorAddress1;
    @SerializedName("parlorAddress2")
    @Expose
    private String parlorAddress2;
    @SerializedName("distance")
    @Expose
    private String distance;

    public Integer getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(Integer menuPrice) {
        this.menuPrice = menuPrice;
    }

    public Integer getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(Integer dealPrice) {
        this.dealPrice = dealPrice;
    }

    public String getParlorName() {
        return parlorName;
    }

    public void setParlorName(String parlorName) {
        this.parlorName = parlorName;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }

    public String getParlorAddress1() {
        return parlorAddress1;
    }

    public void setParlorAddress1(String parlorAddress1) {
        this.parlorAddress1 = parlorAddress1;
    }

    public String getParlorAddress2() {
        return parlorAddress2;
    }

    public void setParlorAddress2(String parlorAddress2) {
        this.parlorAddress2 = parlorAddress2;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
