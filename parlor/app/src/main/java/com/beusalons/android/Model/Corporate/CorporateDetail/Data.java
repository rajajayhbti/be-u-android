package com.beusalons.android.Model.Corporate.CorporateDetail;

import com.beusalons.android.Model.Loyalty.CorporateReferalMessage;

import java.util.List;

/**
 * Created by myMachine on 7/31/2017.
 */

public class Data {

    private List<Company> companies = null;
    private List<CorporateDeal> corporateDeals = null;
    private CorporateReferalsGender corporateReferalsMale;
    private CorporateReferalsGender corporateReferalsFemale;
    private List<CorporateRegister> corporateRegister = null;
    private CorporateTnC corporateTnC;

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }

    public List<CorporateDeal> getCorporateDeals() {
        return corporateDeals;
    }

    public void setCorporateDeals(List<CorporateDeal> corporateDeals) {
        this.corporateDeals = corporateDeals;
    }

    public CorporateReferalsGender getCorporateReferalsMale() {
        return corporateReferalsMale;
    }

    public void setCorporateReferalsMale(CorporateReferalsGender corporateReferalsMale) {
        this.corporateReferalsMale = corporateReferalsMale;
    }

    public CorporateReferalsGender getCorporateReferalsFemale() {
        return corporateReferalsFemale;
    }

    public void setCorporateReferalsFemale(CorporateReferalsGender corporateReferalsFemale) {
        this.corporateReferalsFemale = corporateReferalsFemale;
    }

    public List<CorporateRegister> getCorporateRegister() {
        return corporateRegister;
    }

    public void setCorporateRegister(List<CorporateRegister> corporateRegister) {
        this.corporateRegister = corporateRegister;
    }

    public CorporateTnC getCorporateTnC() {
        return corporateTnC;
    }

    public void setCorporateTnC(CorporateTnC corporateTnC) {
        this.corporateTnC = corporateTnC;
    }
}
