package com.beusalons.android.Event.MembershipEvent;

/**
 * Created by myMachine on 6/29/2017.
 */

public class Event {

    private String name;
    private boolean same_service= false;

    public Event(String name, boolean same_service){

        this.name= name;
        this.same_service= same_service;
    }

    public String getName() {
        return name;
    }

    public boolean isSame_service() {
        return same_service;
    }
}
