package com.beusalons.android.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beusalons.android.R;
import com.beusalons.android.Utility.SquareImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

/**
 * Created by Ajay on 2/6/2018.
 */

public class ImageSlideSearchFragment extends Fragment {
    private boolean open;
    private String img_url;
    private String postId,artistId,artistPic,artistName;
    private int pos;

    public ImageSlideSearchFragment newInstance(String img_url, String postId, String artistId, boolean isOpen, int pos, String artistName, String artistPic){

        ImageSlideSearchFragment fragment= new ImageSlideSearchFragment();
        fragment.img_url= img_url;
        fragment.artistId=artistId;
        fragment.postId=postId;
        fragment.open=isOpen;
        fragment.pos=pos;
        fragment.artistName=artistName;
        fragment.artistPic=artistPic;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view= LayoutInflater.from(getContext()).inflate(R.layout.fragment_image_, container ,false);

        final SquareImageView img_= (SquareImageView) view.findViewById(R.id.img_full);
        Glide.with(view.getContext())
                .applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.about_us_3))
                .load(img_url)
                .into(img_);


        if (!open){
            img_.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    viewedByMe(postId);
//                    Intent intent=new Intent(getActivity(),PostDetailActivity.class);
//                    intent.putExtra(PostDetailActivity.POST_ID, postId);
//                    intent.putExtra(PostDetailActivity.ARTIST_ID, artistId);
//                    intent.putExtra(PostDetailActivity.POSITION,pos);
//                    intent.putExtra(PostDetailActivity.ARTISTPIC,artistPic);
//                    intent.putExtra(PostDetailActivity.ARTISTNAME,artistName);
//                    getActivity().startActivity(intent);
                }
            });
        }


        Log.i("thisthat", "single image url: "+ img_url);


        return view;
    }


//    private void viewedByMe(String postId){
//        Retrofit retrofit= ServiceGenerator.getPortfolioClient();
//        ApiInterface apiInterface= retrofit.create(ApiInterface.class);
//        ViewByMe_post viewByMe_post=new ViewByMe_post();
//        viewByMe_post.setPostId(postId);
//        Call<ViewByMe_response> call=apiInterface.viewedByMe(viewByMe_post);
//
//        call.enqueue(new Callback<ViewByMe_response>() {
//            @Override
//            public void onResponse(Call<ViewByMe_response> call, Response<ViewByMe_response> response) {
//                if (response.isSuccessful()){
//                    if (response.body()!=null){
//
//                        if (response.body().getSuccess()){
//                            Log.i("homestuff", "i'm in : isSuccess trrue");
//
//                        }else{
//                            Log.i("homestuff", "i'm in : isSuccess false ");
//
//                        }
//                    }
//                }else{
//                    Log.i("homestuff", "i'm in : isSuccessful false");
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ViewByMe_response> call, Throwable t) {
//                Log.i("homestuff", "i'm in failure: view by me "+t.getMessage()+ " " + t.getCause()+ "   "+ t.getStackTrace());
//
//            }
//        });
//
//    }

}

