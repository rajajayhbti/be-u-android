package com.beusalons.android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Model.SocialLogin.LoginPost;
import com.beusalons.android.Model.SocialLogin.LoginResponse;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.LoggingBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity {

    private CarouselView carouselView;
    private FirebaseAnalytics mFirebaseAnalytics;
    private int[] images= {R.drawable.c1, R.drawable.c2,R.drawable.c3,R.drawable.c4, R.drawable.c5};

    private ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {

            imageView.setScaleType(ImageView.ScaleType.CENTER);
            imageView.setImageResource(images[position]);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    };

    private CallbackManager callbackManager;        //facebook ka hai yeh
    private ProgressBar progressBar;
    private GoogleApiClient mGoogleApiClient;
    private static final int GOOGLE_SIGN_IN= 1;

    private Button btn_facebook, signInButton,btn_switch_user;

    private TextView txt_terms_condition;
    private CheckBox checkBox_term_condition;
    private LinearLayout linear_signin;

    private ImageView img_logo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_about_carousel);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        img_logo= (ImageView)findViewById(R.id.img_logo);
        img_logo.setImageResource(R.drawable.beu_logo);
        img_logo.setColorFilter(ContextCompat.getColor(this,R.color.colorPrimaryBackgroundText));

        callbackManager = CallbackManager.Factory.create();     //facebook ka hai
        if (BuildConfig.DEBUG) {
            FacebookSdk.setIsDebugEnabled(true);
            FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        }
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestIdToken("741142594415-u38s375phq85fd3g5m0129ms5pb9rdou.apps.googleusercontent.com")     //server client id hai autocreated google api-credential auth 2.0 kk
                .requestProfile()
                .build();
        progressBar= (ProgressBar)findViewById(R.id.progress_bar);
        btn_switch_user= findViewById(R.id.btn_switch_user);
        progressBar.setVisibility(View.GONE);
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .build();

        mGoogleApiClient.connect();

        carouselView = (CarouselView)findViewById(R.id.carouselView);
        carouselView.setImageListener(imageListener);
        carouselView.setPageCount(images.length);

        linear_signin= (LinearLayout)findViewById(R.id.linear_signin);
        linear_signin.setClickable(false);
        linear_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(LoginActivity.this, "Please accept our T & C", Toast.LENGTH_SHORT).show();
            }
        });


        txt_terms_condition= (TextView) findViewById(R.id.txt_terms_condition);
        txt_terms_condition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("http://beusalons.com/appTermsConditions"));
                startActivity(viewIntent);
            }
        });

        checkBox_term_condition= (CheckBox)findViewById(R.id.checkbox_term_condition);
        checkBox_term_condition.setChecked(true);
        checkBox_term_condition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){
                    linear_signin.setClickable(false);
                    btn_facebook.setClickable(true);
                    ViewCompat.setElevation(btn_facebook, 2);

                    signInButton.setClickable(true);
                    ViewCompat.setElevation(signInButton, 2);


                }else{
                    linear_signin.setClickable(true);
                    btn_facebook.setClickable(false);
                    ViewCompat.setElevation(btn_facebook, 0);

                    signInButton.setClickable(false);
                    ViewCompat.setElevation(signInButton, 0);


//                    checkBox_term_condition.setFocusableInTouchMode(true);
//                    checkBox_term_condition.requestFocus();
//                    txt_terms_condition.setFocusableInTouchMode(true);
//                    txt_terms_condition.requestFocus();

                }

            }
        });

        signInButton = (Button) findViewById(R.id.btn_google);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, GOOGLE_SIGN_IN);
            }
        });

        //facebook code hai yeh
        btn_facebook= (Button)findViewById(R.id.btn_facebook);
        btn_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LoginManager.getInstance().logOut();

                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("user_friends,user_tagged_places"));
                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(LoginResult loginResult) {
                                Log.i("investiga", "I'm in onSuccess");
                                String str= loginResult.getAccessToken().getToken();

                                fetchData(str, 1);
                                logEventForSocialLogin();
                            }

                            @Override
                            public void onCancel() {
                                Log.i("investiga", "I'm in onCancel");
                            }

                            @Override
                            public void onError(FacebookException exception) {
                                Log.i("investiga", "I'm in onError: "+ exception.getMessage()+ " "+exception.getCause());
                            }
                        });
            }
        });


        btn_facebook.setClickable(true);
        signInButton.setClickable(true);
        ViewCompat.setElevation(signInButton, 2);
        ViewCompat.setElevation(btn_facebook, 2);
        if (BuildConfig.DEBUG) {
            btn_switch_user.setVisibility(View.VISIBLE);
        }else{
            btn_switch_user.setVisibility(View.GONE);
        }
        btn_switch_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LoginActivity.this,SwitchUserActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i("investig", "I'm in the on activity result");
        //neeche ka google sign in ka code hai
        if(requestCode==GOOGLE_SIGN_IN){

            Log.i("investiga", "value in result: "+ resultCode);

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);


//                int statusCode = result.getStatus().getStatusCode();
//                Log.i("investig", "I'm in the on result okay now"+ result.getStatus().toString()+ "  "+statusCode);
            if (result.isSuccess()) {
                logEventForSocialLogin();
                GoogleSignInAccount acct = result.getSignInAccount();
                Log.i("investiga", "name: "+acct.getDisplayName()+ "  "+ "email :"+ acct.getEmail()+
                        "   "+ acct.getIdToken()+ acct);

                fetchData(acct.getIdToken(), 2);

            } else {
                Log.i("investig", "I'm in the google+ activity result failed");
                // Signed out, show unauthenticated UI.

            }

        }
        callbackManager.onActivityResult(requestCode, resultCode, data);            //facebook ka hai yeh

    }

    private void logEventForSocialLogin(){

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SIGN_UP_METHOD,"Social Login");

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle);
    }

    private void fetchData(final String accessToken, final int loginType){
        View view = getCurrentFocus();
        progressBar.setVisibility(View.VISIBLE);
        progressBar.bringToFront();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        LoginPost post= new LoginPost();
        post.setAccessToken(accessToken);
        post.setSocialLoginType(loginType);

        Call<LoginResponse> call= apiInterface.getSocialResponse(post);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                Log.i("loginactivity", "I'm in onResponse");

                try{

                    if(response.isSuccessful()){

                        if(response.body().isSuccess()){

                            if(response.body().getData().getPhoneNumber() == null){
                                // if user registered first time

                                SharedPreferences globalPreference= getSharedPreferences
                                        ("globalPreference", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editorr= globalPreference.edit();
                                editorr.putBoolean("firstLogin", true).apply();

                                Log.i("loginactivity", "I'm in onResponse phonenumber null: "+
                                        response.body().getData().getPhoneNumber());

                                LoginResponse loginResponse= response.body();

                                Intent intent= new Intent(LoginActivity.this, LoginPhoneActivity.class);
                                Bundle bundle= new Bundle();
                                bundle.putSerializable("loginResponse", loginResponse);
                                bundle.putString("socialToken", accessToken);
                                bundle.putInt("socialType", loginType);             //fb ka type 1 hai
                                intent.putExtras(bundle);
                                progressBar.setVisibility(View.GONE);
                                startActivity(intent);


                            }else if(response.body().getData().getPhoneVerification()==0){

                                // otp verify activity

                                SharedPreferences globalPreference= getSharedPreferences
                                        ("globalPreference", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editorr= globalPreference.edit();
                                editorr.putBoolean("firstLogin", true).apply();

                                Log.i("loginactivity", "I'm in onResponse phone verification: "+
                                        response.body().getData().getPhoneVerification());
                                Intent intent= new Intent(LoginActivity.this, OtpVerificationActivity.class);
                                intent.putExtra("phoneNumber", response.body().getData().getPhoneNumber());
                                intent.putExtra("gender", "");
                                intent.putExtra("socialType", loginType);
                                intent.putExtra("socialToken", accessToken);
                                progressBar.setVisibility(View.GONE);
                                startActivity(intent);

                            }else if(response.body().getData().getPhoneVerification()==1 &&
                                    response.body().getData().getPhoneNumber()!=null){

                                //existing facebook and gmail user
                                SharedPreferences globalPreference= getSharedPreferences
                                        ("globalPreference", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editorr= globalPreference.edit();
                                editorr.putBoolean("firstLogin", false).apply();

                                Log.i("loginactivity", "I'm in onResponse phone verification: "+
                                        response.body().getData().getPhoneVerification());
                                SharedPreferences sharedPref= getSharedPreferences
                                        ("userDetails", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor= sharedPref.edit();
                                editor.putString("name", response.body().getData().getName());
                                editor.putString("gender", response.body().getData().getGender());
                                editor.putString("emailId", response.body().getData().getEmailId());
                                editor.putString("phoneNumber", response.body().getData().getPhoneNumber());
                                editor.putString("userId", response.body().getData().getUserId());
                                editor.putString("accessToken", response.body().getData().getAccessToken());
                                editor.putString("profilePic", response.body().getData().getProfilePic());
                                editor.putBoolean("firebase_", false);              //whether firebase id posted to server

                                editor.putBoolean(BeuSalonsSharedPrefrence.VERIFY_CORPORATE,
                                        response.body().getData().isCorporateUser());           //whether corporate is added

                                editor.putBoolean("isLoggedIn", true);

                                editor.putInt("loyaltyPoints", 0);
                                editor.putString("promoCode", "BEU");
                                editor.putBoolean("offerDialog", true);

                                editor.putBoolean("userType", response.body().getData().isUserType());      //yeh apne logo ke liye hai
                                editor.apply();

                                Log.i("imhere","value :D: "+ response.body().getData().isUserType()+ response.toString());
                                BeuSalonsSharedPrefrence.setLogin(true);
                                progressBar.setVisibility(View.GONE);
                                Intent intent= new Intent(LoginActivity.this, FetchingLocationActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }else{
                            Log.i("loginactivity", "yeh kaise ho gaya kldj klasd fsdafh sa jdfajj saj");
                            //Todo: handle karo iseh
                            progressBar.setVisibility(View.GONE);
                        }

                    }else{
                        //Todo: handle karo iseh
                        Log.i("loginactivity", "I'm in onResponse not succes pe hai");
                        progressBar.setVisibility(View.GONE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                View view = getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                Log.i("loginactivity", "I'm in onFailure"+ t.getMessage()+ "  "+t.getCause());
                progressBar.setVisibility(View.GONE);
            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
