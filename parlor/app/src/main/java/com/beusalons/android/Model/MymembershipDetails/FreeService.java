package com.beusalons.android.Model.MymembershipDetails;

import java.util.List;

/**
 * Created by Ajay on 11/9/2017.
 */

public class FreeService {
    private List<Service> services = null;


    private String serviceId;
    private String serviceCode;
    private String name;
    private Integer quantityAvailed;
    private Integer quantityRemaining;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQuantityAvailed() {
        return quantityAvailed;
    }

    public void setQuantityAvailed(Integer quantityAvailed) {
        this.quantityAvailed = quantityAvailed;
    }

    public Integer getQuantityRemaining() {
        return quantityRemaining;
    }

    public void setQuantityRemaining(Integer quantityRemaining) {
        this.quantityRemaining = quantityRemaining;
    }
}
