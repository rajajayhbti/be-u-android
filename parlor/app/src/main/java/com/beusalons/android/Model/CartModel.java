package com.beusalons.android.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robbin Singh on 17/11/2016.
 */

public class CartModel implements Serializable {
    private String parlorName;
    private String parlorId;
    private int totalCostWithTax;
    private int subTotal;
    private int tax;
    private String address1;
    private String address2;
    private Double rating;

    private String cartType;          //dealType or servicesType or salonType


    public String getCartType() {
        return cartType;
    }

    public void setCartType(String cartType) {
        this.cartType = cartType;
    }

    //this is cart service ka hai
    private List<CartServiceModel> cartServiceList = new ArrayList<CartServiceModel>();


    public String getAddress1() {
        return address1;
    }

    public int getTax() {
        return tax;
    }

    public void setTax(int tax) {
        this.tax = tax;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getParlorName() {
        return parlorName;
    }

    public void setParlorName(String parlorName) {
        this.parlorName = parlorName;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }

    public int getTotalCostWithTax() {
        return totalCostWithTax;
    }

    public void setTotalCostWithTax(int totalCostWithTax) {
        this.totalCostWithTax = totalCostWithTax;
    }

    public int getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(int subTotal) {
        this.subTotal = subTotal;
    }

    public List<CartServiceModel> getCartServiceList() {
        return cartServiceList;
    }

    public void setCartServiceList(List<CartServiceModel> cartServiceList) {
        this.cartServiceList = cartServiceList;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }
}
