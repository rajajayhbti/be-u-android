package com.beusalons.android.Service;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.beusalons.android.FetchingLocationActivity;
import com.beusalons.android.MainActivity;
import com.beusalons.android.Model.GeoCode.GeoCodingResponse;
import com.beusalons.android.ParlorListActivity;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.util.Log.e;

/**
 * Created by Ashish Sharma on 8/3/2017.
 */
public class LocationUpdateService extends  Service {

    Service service;
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 5; // 5 minutes
    LocationManager locationManager;
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        service = this;

        getLocation();
    }


    private void getLocation() {
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        e("locario", "Getting location");

        LocationListener locationListener = new LocationListener() {

            public void onLocationChanged(Location location) {
//                new ConstantHelper().createLocation(service, String.valueOf(location.getLatitude()),
//                        String.valueOf(location.getLongitude()), "");




//              Log.i("shityshit", "sfkldfj: "+ Arrays.deepToString(addresses.toArray()));

//                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()


             /*   BeuSalonsSharedPrefrence.saveLatLong(service, String.valueOf(location.getLatitude()),
                        String.valueOf(location.getLongitude()));*/
                //  Log.e("save", "location found" + location.getLatitude() + "DFft" + location.getLongitude()+ ":"+addresses.get(0).getThoroughfare());
                handleNewLocation(location);
                Log.e("locario", "location found" + location.getLatitude() + "DFft" + location.getLongitude());
                if (ActivityCompat.checkSelfPermission(service, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(service, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                locationManager.removeUpdates(this);
                stopService(new Intent(service, LocationUpdateService.class));

                //   EventBus.getDefault().post(location);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };
//        Log.e("locario", "Listener");
        // Register the listener with the Location Manager to receive location updates

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
//        Log.e("locario", "Listener 2");

    }


    public void handleNewLocation(Location lastLocation) {
        try {

            Log.e("investigatingg", "in handle new location");
            Log.e("investigatingg", "value in last location string handle new location" + lastLocation.toString());
            final double lat = lastLocation.getLatitude(), lon = lastLocation.getLongitude();
            Retrofit retrofit;
            retrofit = ServiceGenerator.getClient();
            ApiInterface apiInterface = retrofit.create(ApiInterface.class);

            Call<GeoCodingResponse> call = apiInterface.getAddress(
                    "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon + "&sensor=true"+"&key=AIzaSyDwsi-O7uCC9IgO6Jx8m8wMWoLbdfyOLT4");
            //http://maps.googleapis.com/maps/api/geocode/json?latlng=28.617498,77.068499&sensor=true
            call.enqueue(new Callback<GeoCodingResponse>() {
                @Override
                public void onResponse(Call<GeoCodingResponse> call, Response<GeoCodingResponse> response) {

                    if(response.isSuccessful()) {

                        if (response.body() != null &&
                                response.body().getResults() != null &&
                                response.body().getResults().size() > 0) {

                       //     Log.i("investigatingg", "value in on repsonse: " + response.body().getResults().get(0).getFormatted_address());

                            String address = response.body().getResults().get(0).getFormatted_address();
                           // Log.e("ASh", response.body().getResults().get(0).getAddress_components().get(1).getLong_name());
                            String localtyAddress = response.body().getResults().get(0).getAddress_components().get(1).getLong_name();
                            if (address != null && address != "") {
//                            new ConstantHelper().createLocation(activity, String.valueOf(lat),
//                                    String.valueOf(lon), address);
//                            new ConstantHelper().initLocation(activity);
                                Intent intent = new Intent(MainActivity.LOCATION_ACTION);
                                intent.putExtra("lat", lat);
                                intent.putExtra("long", lon);
                                intent.putExtra("local", localtyAddress);
                                sendBroadcast(intent);

                                Intent forParlorList = new Intent(ParlorListActivity.LOCATION_ACTION);
                                forParlorList.putExtra("lat", lat);
                                forParlorList.putExtra("long", lon);
                                forParlorList.putExtra("local", localtyAddress);
                                sendBroadcast(forParlorList);

                                //stopService(new Intent(service, LocationService.class));
                                //    BeuSalonsSharedPrefrence.saveAddress(getApplicationContext(),localtyAddress,address,String.valueOf(lat),String.valueOf(lon));

                            }

                        } else {
                            Toast.makeText(getApplicationContext(), "Unable to get location", Toast.LENGTH_SHORT).show();
                        }

                    }
                }

                @Override
                public void onFailure(Call<GeoCodingResponse> call, Throwable t) {
                    Log.i("investigatingg", "value in on failure: "+ t.getMessage()+ " "+t.getCause()+  " "+ t.getStackTrace());

                }
            });

//            new GetAddressTask(activity, new GetAddressTask.OnTaskCompleted() {
//                @Override
//                public void onTaskCompleted(String address) {
//
//                    Log.i("investigatingg", "vlaue in address: "+address);
//                    new ConstantHelper().createLocation(activity, String.valueOf(lat),
//                            String.valueOf(lon), address);
//                    new ConstantHelper().initLocation(activity);
//                    activity.startActivity(new Intent(activity, MainActivity.class));
//                    activity.finish();
//                }
//            }).execute(String.valueOf(lat), String.valueOf(lon));
        }catch (Exception e){
            Log.e("investigatingg", e.getMessage());
        }

    }
}