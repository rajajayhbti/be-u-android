package com.beusalons.android.Model.DealsServices.DealDetail;

/**
 * Created by Ashish Sharma on 6/16/2017.
 */

public class ServicesList {

    private Integer serviceCode;
    private String brandId;
    private String productId;

    public Integer getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(Integer serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
