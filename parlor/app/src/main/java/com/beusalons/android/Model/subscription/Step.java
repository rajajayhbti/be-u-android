package com.beusalons.android.Model.subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Step {

@SerializedName("title")
@Expose
private String title;
private String icon;
@SerializedName("content")
@Expose
private String content;

public String getTitle() {
return title;
}

public void setTitle(String title) {
this.title = title;
}

public String getContent() {
return content;
}

public void setContent(String content) {
this.content = content;
}

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}