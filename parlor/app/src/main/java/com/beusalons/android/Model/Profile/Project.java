package com.beusalons.android.Model.Profile;


import com.beusalons.android.Model.ArtistProfile.CreativeField;

import java.util.List;


/**
 * Created by myMachine on 10/27/2017.
 */

public class Project {

    private List<Object> comments = null;
    private String id;
    private List<Object> likes = null;
    private String date;
    private String coverImage;
    private String postTitle;
    private String postDescription;
    private List<String> images = null;
    private String collectionName;
    private List<Tag> tags = null;
    private Boolean likedStatus;
    private String artistName;
    private String totalLikes;
    private String   artistId;
    private List<CreativeField>  creativeFields;
    private int postLikes;

    private int type;

    public int getPostLikes() {
        return postLikes;
    }

    public void setPostLikes(int postLikes) {
        this.postLikes = postLikes;
    }

    public Project(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<CreativeField> getCreativeFields() {
        return creativeFields;
    }

    public void setCreativeFields(List<CreativeField> creativeFields) {
        this.creativeFields = creativeFields;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public List<Object> getComments() {
        return comments;
    }

    public void setComments(List<Object> comments) {
        this.comments = comments;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Object> getLikes() {
        return likes;
    }

    public void setLikes(List<Object> likes) {
        this.likes = likes;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostDescription() {
        return postDescription;
    }

    public void setPostDescription(String postDescription) {
        this.postDescription = postDescription;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public Boolean getLikedStatus() {
        return likedStatus;
    }

    public void setLikedStatus(Boolean likedStatus) {
        this.likedStatus = likedStatus;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        this.totalLikes = totalLikes;
    }
}
