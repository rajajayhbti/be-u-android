package com.beusalons.android.Model.MymembershipDetails;

/**
 * Created by Ajay on 11/8/2017.
 */

public class MembershipDetailsPost {
    private  String userId;
    private  String accessToken;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
