package com.beusalons.android.Model.DealsServices.DealDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ajay on 6/22/2017.
 */

public class Deal_Detail_Post {

    private List<PostDealDetail> selectedDeals= new ArrayList<>();
    private double latitude;
    private double longitude;
    private int page;
    private String parlorId;

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public List<PostDealDetail> getSelectedDeals() {
        return selectedDeals;
    }

    public void setSelectedDeals(List<PostDealDetail> selectedDeals) {
        this.selectedDeals = selectedDeals;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
