package com.beusalons.android.Service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.R;
import com.beusalons.android.SplashScreenActivity;
import com.beusalons.android.Utility.Utility;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Ajay on 12/15/2016.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private String value = "";
    private String strAppointmentId, type_string;          //yaha type ka matlab- started hai ya review
    String parlorId;
    private int type_;          //multiple types ho toh isliye use kiya maine integer
    private AppEventsLogger logger;

    NotificationManager mNotifyManager;
    NotificationCompat.Builder mBuilder;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        logger = AppEventsLogger.newLogger(this);

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage.getData().size() > 0) {

//            String type = remoteMessage.getData().get("notification_type");
//            body = remoteMessage.getData().get("body");
            strAppointmentId = remoteMessage.getData().get("appointmentId");
            type_string= remoteMessage.getData().get("type");

            Log.i("investig", "vlaue id appt and type: "+ strAppointmentId+ "   "+ type_string+ " "+ remoteMessage.getData().get("body")+ "  "+
                    remoteMessage.getData().get("title"));

            String msg="", title="";

            if(type_string.equalsIgnoreCase("started")){
                msg= remoteMessage.getData().get("body");
                title= remoteMessage.getData().get("title");
                type_=0;                                            // appointment started

            }else if(type_string.equalsIgnoreCase("review")){
                msg= remoteMessage.getData().get("body");
                title= remoteMessage.getData().get("title");
                type_=5;                                                        //appointment review

            }else if(type_string.equalsIgnoreCase("offer")){

                msg= remoteMessage.getData().get("body");
                title= remoteMessage.getData().get("title");
                type_=1;                                                        //offers ka notification

            }else if(type_string.equalsIgnoreCase("freebie")){              //freebie-> main activity/freebie page

                msg= remoteMessage.getData().get("body");
                title= remoteMessage.getData().get("title");
                type_=3;

            }else if(type_string.equalsIgnoreCase("recommendation")){              //freebie-> main activity/freebie page

                msg= remoteMessage.getData().get("body");
                title= remoteMessage.getData().get("title");
                type_=1;

            }else if(type_string.equalsIgnoreCase("loginBased")){              //freebie-> main activity/freebie page

                msg= remoteMessage.getData().get("body");
                title= remoteMessage.getData().get("title");
                type_=2;

            }else if(type_string.equalsIgnoreCase("cartBased")){              //freebie-> main activity/freebie page

                msg= remoteMessage.getData().get("body");
                title= remoteMessage.getData().get("title");
                type_=4;

            }else if(type_string.equalsIgnoreCase("update")){              //freebie-> main activity/freebie page

                msg= remoteMessage.getData().get("body");
                title= remoteMessage.getData().get("title");
                type_=7;

            }else if(type_string.equalsIgnoreCase("profile")){              //freebie-> main activity/freebie page

                msg= remoteMessage.getData().get("body");
                title= remoteMessage.getData().get("title");
                type_=6;

            }else if(type_string.equalsIgnoreCase("checkIn")){              //chekin-> parlor detail

                msg= remoteMessage.getData().get("body");
                title= remoteMessage.getData().get("title");
                parlorId=remoteMessage.getData().get("parlorId");
                type_=8;

            }else if(type_string.equalsIgnoreCase("subscription")){              //subscription

                msg= remoteMessage.getData().get("body");
                title= remoteMessage.getData().get("title");
                type_=18;

            }else{
                msg= remoteMessage.getData().get("body");
                title= remoteMessage.getData().get("title");
                type_=2;

            }
            String url="";
            try{
                 url=remoteMessage.getData().get("lImage");
            }catch (Exception e){
                e.printStackTrace();
            }



            if (type_==1){

                if (url!=null && url.toString().trim().length()>0){
                    showPicture(title,msg,url,type_,type_string);
                }else{
                    showBigText(title,msg,type_,type_string);
                }

                logNotificationReceivedEvent(type_string,currentTime());
            }else if(type_==3 || type_==7){
                if (url!=null && url.toString().trim().length()>0){
                    showPicture(title,msg,url,type_,type_string);
                }else{
                    showBigText(title,msg,type_,type_string);
                }
                logNotificationReceivedEvent(type_string,currentTime());

            }
            else {
//    showNotification(strAppointmentId,title,msg, type_);
                if (url!=null && url.toString().trim().length()>0){
                    showPicture(title,msg,url,type_,type_string);
                }else{
                    showBigText(title,msg,type_,type_string);
                }
                logNotificationReceivedEvent(type_string,currentTime());

            }
            //type_  ye integer hai
        }
    }


   /* public void showNotification(String strAppointmentId,String strTitle,String strMesage, int type) {

        Log.d(TAG, "i'm in show notification");
        Intent intent;
        intent =  new Intent(this, SplashScreenActivity.class);

        Log.i("trackthis", "i'm i n bundle mai now messageing service ke: "+ type+  "  "+ strAppointmentId);

        intent.putExtra("appointmentId",strAppointmentId);
        intent.putExtra("type", type);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK  | Intent.FLAG_ACTIVITY_NEW_TASK );
//        Intent notificationIntent = new Intent(ctx, YourClass.class);

        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(),
                0, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationManager nm = (NotificationManager) getApplicationContext()
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Resources res = getApplicationContext().getResources();
        Notification.Builder builder = new Notification.Builder(getApplicationContext());
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.ic_status)
                .setLargeIcon(BitmapFactory.decodeResource(res,R.mipmap.ic_launcher))
                .setTicker(strTitle)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentTitle(strTitle)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_HIGH)

                .setContentText(strMesage);

        Notification n = builder.build();

        long time = new Date().getTime();
        String tmpStr = String.valueOf(time);
        String last4Str = tmpStr.substring(tmpStr.length() - 5);
        int notificationId = Integer.valueOf(last4Str);


        nm.notify(notificationId, n);

    }*/
    public void showPicture(String title,String text,String Lurl,int type,String type_string){

        Intent intent;
        intent =  new Intent(this, SplashScreenActivity.class);

        Log.i("trackthis", "i'm i n bundle mai now messageing service ke: "+ type+  "  "+ strAppointmentId);

        intent.putExtra("appointmentId",strAppointmentId);
        intent.putExtra("type", type);
        intent.putExtra("type_String",type_string);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK  | Intent.FLAG_ACTIVITY_NEW_TASK );
//        Intent notificationIntent = new Intent(ctx, YourClass.class);
        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(),
                0, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        Bitmap image = null;
        try {
            URL url = new URL(Lurl);
            image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch(IOException e) {
            System.out.println(e);
        }
        Resources res = getApplicationContext().getResources();
        NotificationManager nm = (NotificationManager) getApplicationContext()
                .getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){

            String id = "my_package_channel_1";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = nm.getNotificationChannel(id);
            if (mChannel == null) {
                mChannel = new NotificationChannel(id, title, importance);
                mChannel.setDescription(text);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                nm.createNotificationChannel(mChannel);
            }
            builder = new NotificationCompat.Builder(this, id);
            builder.setContentTitle(title)  // required
                    .setSmallIcon(R.drawable.ic_status) // required
                    .setContentText(text)  // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(contentIntent)
                    .setTicker(title)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

        }else{
            builder = new NotificationCompat.Builder(this);
            builder.setSmallIcon(R.drawable.ic_status)
                    .setLargeIcon(BitmapFactory.decodeResource(res,R.mipmap.ic_launcher))
                    .setColor(getResources().getColor(R.color.colorPrimary))
                    .setContentTitle(title)
                    .setContentText(text)
                    .setAutoCancel(true)
                    .setContentIntent(contentIntent)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(Notification.PRIORITY_DEFAULT);
        }


        NotificationCompat.BigPictureStyle s = new NotificationCompat.BigPictureStyle().bigPicture(image);
        s.setSummaryText(text);
        builder.setStyle(s);
        Notification n = builder.build();


        long time = new Date().getTime();
        String tmpStr = String.valueOf(time);
        String last4Str = tmpStr.substring(tmpStr.length() - 5);
        int notificationId = Integer.valueOf(last4Str);


        nm.notify(notificationId, n);
    }


    public void showBigText(String title,String msg,int type,String type_string) {
        if (Utility.isAppIsInBackground(this)) {
            Intent intent;
            intent = new Intent(this, SplashScreenActivity.class);

            Log.i("trackthis", "i'm i n bundle mai now messageing service ke: " + type + "  " + strAppointmentId);
            if (type == 8) {

                intent.putExtra("parlorId", parlorId);
            }

            intent.putExtra("appointmentId", strAppointmentId);
            intent.putExtra("type", type);
            intent.putExtra("type_String", type_string);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//        Intent notificationIntent = new Intent(ctx, YourClass.class);
            PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(),
                    0, intent,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            NotificationCompat.Builder builder;
            Resources res = getApplicationContext().getResources();
            NotificationManager nm = (NotificationManager) getApplicationContext()
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                String id = "my_package_channel_1";
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = nm.getNotificationChannel(id);
                if (mChannel == null) {
                    mChannel = new NotificationChannel(id, title, importance);
                    mChannel.setDescription(msg);
                    mChannel.enableVibration(true);
                    mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    nm.createNotificationChannel(mChannel);
                }
                builder = new NotificationCompat.Builder(this, id);
                builder.setContentTitle(title)  // required
                        .setSmallIcon(R.drawable.ic_status) // required
                        .setContentText(msg)  // required
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setAutoCancel(true)
                        .setContentIntent(contentIntent)
                        .setTicker(title)
                        .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

            } else {
                builder = new NotificationCompat.Builder(this);
                builder.setSmallIcon(R.drawable.ic_status)
                        .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher))
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setContentTitle(title)
                        .setContentIntent(contentIntent)
                        .setAutoCancel(true)
                        .setContentText(msg)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setPriority(Notification.PRIORITY_DEFAULT);

                NotificationCompat.BigTextStyle s = new NotificationCompat.BigTextStyle().bigText(msg);
                builder.setStyle(s);

            }


            long time = new Date().getTime();
            String tmpStr = String.valueOf(time);
            String last4Str = tmpStr.substring(tmpStr.length() - 5);
            int notificationId = Integer.valueOf(last4Str);
            Notification notification = builder.build();

            nm.notify(notificationId, notification);
        }
    }

        /**
         * This function assumes logger is an instance of AppEventsLogger and has been
         * created using AppEventsLogger.newLogger() call.
         */
    public void logNotificationReceivedEvent (String type, String notificationDate) {
        Bundle params = new Bundle();
        params.putString("type", type+"-"+notificationDate);
        //params.putString(AppConstant.NotificationDate, notificationDate);
        logger.logEvent(AppConstant.NotificationReceived, params);
    }

    public String currentTime(){

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }
}