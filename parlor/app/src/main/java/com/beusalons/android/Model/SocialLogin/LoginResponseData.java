package com.beusalons.android.Model.SocialLogin;

import java.io.Serializable;

/**
 * Created by myMachine on 3/2/2017.
 */

public class LoginResponseData implements Serializable{

    private String name;
    private String emailId;
    private String phoneNumber;         //if null then open ask phone number in the next screen
    private String profilePic;
    private String gender;
    private int phoneVerification;                         //send 1 in case of app      -- verify

    private String userId;              //yaha mujhe user id milta hai aur accesstoken
    private String accessToken;

    private boolean userType;           //yeh apne logo ke liye hai jo customers ke liye appt book karenge
    private boolean isCorporateUser;

    public boolean isCorporateUser() {
        return isCorporateUser;
    }

    public void setCorporateUser(boolean corporateUser) {
        isCorporateUser = corporateUser;
    }

    public boolean isUserType() {
        return userType;
    }

    public void setUserType(boolean userType) {
        this.userType = userType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getPhoneVerification() {
        return phoneVerification;
    }

    public void setPhoneVerification(int phoneVerification) {
        this.phoneVerification = phoneVerification;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
