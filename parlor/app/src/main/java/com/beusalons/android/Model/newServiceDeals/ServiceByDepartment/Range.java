package com.beusalons.android.Model.newServiceDeals.ServiceByDepartment;

/**
 * Created by myMachine on 6/19/2017.
 */

public class Range {

    private int range1;
    private int range2;
    private double discount;

    public int getRange1() {
        return range1;
    }

    public void setRange1(int range1) {
        this.range1 = range1;
    }

    public int getRange2() {
        return range2;
    }

    public void setRange2(int range2) {
        this.range2 = range2;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }
}
