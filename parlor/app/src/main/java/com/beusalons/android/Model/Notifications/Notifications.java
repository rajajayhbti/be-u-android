package com.beusalons.android.Model.Notifications;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 4/4/2017.
 */

public class Notifications {

    private List<NotificationDetail> list= new ArrayList<>();

    public List<NotificationDetail> getList() {
        return list;
    }

    public void setList(List<NotificationDetail> list) {
        this.list = list;
    }

}
