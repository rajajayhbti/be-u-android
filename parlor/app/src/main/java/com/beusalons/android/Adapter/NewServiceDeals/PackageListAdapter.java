package com.beusalons.android.Adapter.NewServiceDeals;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beusalons.android.Dialog.ShowDetailsServiceDialog;
import com.beusalons.android.Fragment.ServiceFragments.ComboBottomSheet;
import com.beusalons.android.Fragment.ServiceFragments.NewComboBottomSheet;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Service;
import com.beusalons.android.R;
import com.beusalons.android.ServiceSpecificActivity;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by myMachine on 6/20/2017.
 */

public class PackageListAdapter extends RecyclerView.Adapter<PackageListAdapter.ViewHolder> {

    private Context context;
    private List<Service> list;
//    private Slabs slabs;
    private UserCart user_cart;
    AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;

    public PackageListAdapter(Context context, List<Service> list, /*Slabs slabs,*/ UserCart user_cart){

        this.context= context;
        this.list= list;
//        this.slabs= slabs;
        this.user_cart= user_cart;
        logger = AppEventsLogger.newLogger(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final Service service= list.get(position);

        holder.txt_name.setText(service.getName());

        //with tax
        int price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*service.getPrice());

        holder.txt_price.setText(AppConstant.CURRENCY+ price_);
        if (service.getDescription().length()>45){
            holder.txt_description.setText(service.getDescription().substring(0,45)+"+...see more");
            holder.txt_description.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new ShowDetailsServiceDialog((Activity) context,service.getName(),service.getDescription());

                }
            });
        }
        holder.txt_package.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                logEditServiceBuyPackageEvent();
                logEditServiceBuyPackageFireBaseEvent();

                if(service.getDealType().equalsIgnoreCase("combo")){

//                    Slab slab= null;
//                    for(int i = 0; i< slabs.getSlabs().size(); i++){
//
//                        if (slabs.getSlabs().get(i).getSlabId().equalsIgnoreCase(service.getSlabId())){
//                            slab = slabs.getSlabs().get(i);
//                        }
//                    }


                    ComboBottomSheet bottomSheet= new ComboBottomSheet();
                    Bundle bundle= new Bundle();
                    bundle.putString("package", new Gson().toJson(service, Service.class));
//                    bundle.putString("slab", new Gson().toJson(slab, Slab.class));
                    bundle.putBoolean("pop_up", true);
                    bottomSheet.setArguments(bundle);
                    bottomSheet.show((((ServiceSpecificActivity)context)).getSupportFragmentManager(), "package_combo");
                }else if(service.getDealType().equalsIgnoreCase("newCombo")){

//                    Slab slab= null;
//                    for(int i = 0; i< slabs.getSlabs().size(); i++){
//
//                        if (slabs.getSlabs().get(i).getSlabId().equalsIgnoreCase(service.getSlabId())){
//                            slab = slabs.getSlabs().get(i);
//                        }
//                    }

                    try{
                        NewComboBottomSheet bottomSheet= new NewComboBottomSheet();
                        Bundle bundle= new Bundle();
                        bundle.putString("deal_id", service.getDealId());
                        bundle.putString("parlor_id", user_cart.getParlorId());         //api call karne ke liye
//                    bundle.putString("slab", new Gson().toJson(slab, Slab.class));
                        bundle.putString("description", service.getDescription());
                        bundle.putString("short_description", service.getShortDescription());
                        bundle.putString("service_name", service.getName());
                        bundle.putBoolean("pop_up", true);
                        bundle.putInt("menu_price", service.getMenuPrice());
                        bottomSheet.setArguments(bundle);
                        bottomSheet.show((((ServiceSpecificActivity)context)).getSupportFragmentManager(), "package_new_combo");

                    }catch (Exception e){
                        e.printStackTrace();
                    }


                }

            }
        });

    }


    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */



    public void logEditServiceBuyPackageEvent () {
        Log.e("EditServiceBuyPackage","fine");

        logger.logEvent(AppConstant.EditServiceBuyPackage);
    }
    public void logEditServiceBuyPackageFireBaseEvent () {
        Log.e("EditServiceBuyPackage","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.EditServiceBuyPackage,bundle);
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_name, txt_description, txt_more, txt_price, txt_save_per, txt_package;


        public ViewHolder(View itemView) {
            super(itemView);

            txt_name= (TextView)itemView.findViewById(R.id.txt_name);
            txt_description= (TextView)itemView.findViewById(R.id.txt_description);
            txt_price= (TextView)itemView.findViewById(R.id.txt_price);
            txt_save_per= (TextView)itemView.findViewById(R.id.txt_save_per);
            //txt_more= (TextView)itemView.findViewById(R.id.txt_more);
            txt_package= (TextView)itemView.findViewById(R.id.txt_package);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.fragment_services_specific_dialog_package, parent, false);
        return new ViewHolder(view);
    }

}
