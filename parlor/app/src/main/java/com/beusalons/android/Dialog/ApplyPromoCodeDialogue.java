package com.beusalons.android.Dialog;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Event.BookingSummaryPromoEvent;
import com.beusalons.android.Model.BillSummery.ApplyPromoRequest;
import com.beusalons.android.Model.BillSummery.CouponAppliedResponse;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


/**
 * Created by Ashish on 19/04/2017.
 */

public class ApplyPromoCodeDialogue {
    final Dialog dialog;
    RelativeLayout linearLayoutManager;
    TextView applyPromo;
    Context activity;
    EditText edPromoCode;
    public ApplyPromoCodeDialogue(Context activity, final String userId, final String accessToken,final String appointmentId){

        this.activity=activity;
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogue_apply_promo_code);
        /*WindowManager.LayoutParams lp = new WindowManager.LayoutParams(); Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes()); //This makes the dialog take up the full width
         lp.width = WindowManager.LayoutParams.MATCH_PARENT; lp.height = WindowManager.LayoutParams.WRAP_CONTENT; window.setAttributes(lp);*/
//
        linearLayoutManager= (RelativeLayout) dialog.findViewById(R.id.ll_show_promo_dialogue);
        edPromoCode= (EditText) dialog.findViewById(R.id.ed_promocode);
        applyPromo= (TextView) dialog.findViewById(R.id.applyPromo);
        applyPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edPromoCode.getText().toString().length()>0){
                    ApplyPromoRequest applyPromoRequest=new ApplyPromoRequest();
                    applyPromoRequest.setAccessToken(accessToken);
                    applyPromoRequest.setUserId(userId);
                    applyPromoRequest.setApppointment(appointmentId);
                    applyPromoRequest.setCouponCode(edPromoCode.getText().toString());
                    postPromoCode(applyPromoRequest);

                }
                else{
                    edPromoCode.setError("Enter promocode");
                }

            }
        });


       /* linearLayoutManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });*/

        dialog.show();
    }


    public void postPromoCode(ApplyPromoRequest applyPromoRequest){

        Retrofit retrofit1 = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit1.create(ApiInterface.class);
        Call<CouponAppliedResponse> call= apiInterface.postApplyCoupon(applyPromoRequest);
        call.enqueue(new Callback<CouponAppliedResponse>() {
            @Override
            public void onResponse(Call<CouponAppliedResponse> call, Response<CouponAppliedResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().isSuccess()){

//                        EventBus.getDefault().post(new BookingSummaryPromoEvent(response.body().getData() .getLoyalityPoints()));


                        Toast.makeText(activity,response.body().getData().getMessage(),Toast.LENGTH_LONG).show();
                        dialog.dismiss();

                    }else{
                        Toast.makeText(activity,response.body().getMessage(),Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }




                }else{

                    Toast.makeText(activity,response.message(),Toast.LENGTH_LONG).show();
                    dialog.dismiss();

                }



            }

            @Override
            public void onFailure(Call<CouponAppliedResponse> call, Throwable t) {

            }
        });

    }
}
