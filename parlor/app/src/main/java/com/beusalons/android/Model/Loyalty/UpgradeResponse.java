package com.beusalons.android.Model.Loyalty;

/**
 * Created by myMachine on 8/18/2017.
 */

public class UpgradeResponse {

    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
