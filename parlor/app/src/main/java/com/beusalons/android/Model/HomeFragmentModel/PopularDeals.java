package com.beusalons.android.Model.HomeFragmentModel;

import java.util.List;

/**
 * Created by Beu salons on 3/6/2017.
 */

public class PopularDeals {
    String image;
    String category;
    int price;
    int discount;
    int dealId;
    String brandId;
    String productId;
    private String dealType;
    private List<Integer> serviceCodes = null;



    public PopularDeals(int dealId) {
        this.dealId = dealId;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public PopularDeals(String image, String category, int price, int discount) {
        this.image = image;
        this.category = category;
        this.price = price;
        this.discount = discount;
    }

    public List<Integer> getServiceCodes() {
        return serviceCodes;
    }

    public void setServiceCodes(List<Integer> serviceCodes) {
        this.serviceCodes = serviceCodes;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getDealId() {
        return dealId;
    }

    public void setDealId(int dealId) {
        this.dealId = dealId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }
}
