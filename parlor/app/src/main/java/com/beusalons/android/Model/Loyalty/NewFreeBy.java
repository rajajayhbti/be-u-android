package com.beusalons.android.Model.Loyalty;

import java.util.ArrayList;

/**
 * Created by Ashish Sharma on 9/6/2017.
 */

public class NewFreeBy {

    private String title;
    private String message;
    private String image;
    private ArrayList<InsideArray> insideArray;
    private boolean isSelected= false;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<InsideArray> getInsideArray() {
        return insideArray;
    }

    public void setInsideArray(ArrayList<InsideArray> insideArray) {
        this.insideArray = insideArray;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
