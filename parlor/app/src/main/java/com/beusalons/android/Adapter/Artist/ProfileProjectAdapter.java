package com.beusalons.android.Adapter.Artist;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beusalons.android.Fragment.ImageSlideSearchFragment;
import com.beusalons.android.Model.ArtistProfile.CreativeField;
import com.beusalons.android.Model.PostLike;
import com.beusalons.android.Model.Profile.Project;
import com.beusalons.android.Model.ResponseLike;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.SquareImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.like.LikeButton;
import com.like.OnLikeListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


/**
 * Created by myMachine on 10/27/2017.
 */

public class ProfileProjectAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int PROTFOLIO_TYPE = 0;
    private static final int PROGRESS_TYPE = 1;
    private List<Project> list;
    private Activity act ;
    public ProfileProjectAdapter(List<Project> list, Activity activity){
        this.list= list;
        this.act=activity;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        LinearLayout linear_;
        CardView cardview_;
        switch (viewType){
            case PROTFOLIO_TYPE:
                cardview_= (CardView) LayoutInflater.
                        from(parent.getContext()).inflate(R.layout.home_fragment_list, parent, false);
                return new ViewHolder1(cardview_);
            case PROGRESS_TYPE:
                linear_= (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_progress, parent, false);
                return new ViewHolder2(linear_);
        }
        return null;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        switch (list.get(position).getType()) {
            case PROTFOLIO_TYPE:


                final ProfileProjectAdapter.ViewHolder1 temp= (ProfileProjectAdapter.ViewHolder1) holder;



                final int pos= position;
                temp.card_.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        viewedByMe(list.get(position).getId());
//                        Intent intent=new Intent(act,PostDetailActivity.class);
//                        intent.putExtra(PostDetailActivity.POST_ID, list.get(pos).getId());
//                        intent.putExtra(PostDetailActivity.ARTISTNAME,list.get(pos).getArtistName());
//                        intent.putExtra(PostDetailActivity.ARTISTPIC,BeuSalonsSharedPrefrence.getProfilePic());
//                        act.startActivity(intent);
                    }
                });



             temp.txt_name.setText("");

             temp.txt_collection_name.setText(list.get(pos).getCollectionName());
                if(list.get(position).getPostLikes()==0
                        )
                    temp.txt_likes.setText("");
                else if(list.get(position).getPostLikes()==1)
                    temp.txt_likes.setText(""+list.get(position).getPostLikes()+" Like");
                else
                    temp.txt_likes.setText(""+list.get(position).getPostLikes()+ " Likes");


                Glide.with(act)
                        .applyDefaultRequestOptions(new RequestOptions()
                                .placeholder(R.drawable.about_us_3))
                        .load(list.get(position).getCoverImage())
                        .into(temp.img_profile_pic);



                 temp.linear_tag.setVisibility(View.GONE);
                List<CreativeField> creativeFieldList=new ArrayList<>();
                if (list.get(position).getCreativeFields()!=null &&
                        list.get(position).getCreativeFields().size()>0){
                    for (int i=0;i<list.get(position).getCreativeFields().size();i++){
                        creativeFieldList.add(list.get(position).getCreativeFields().get(i));

                    }
                }
                temp.recycler_creative.setLayoutManager(new FlexboxLayoutManager(act));

                AdapterCommentTags adapterCommentTags=new AdapterCommentTags(creativeFieldList,true);
                temp.recycler_creative.setAdapter(adapterCommentTags);
                adapterCommentTags.setList(creativeFieldList);

                adapterCommentTags.notifyDataSetChanged();

                temp.img_love.setUnlikeDrawableRes(R.drawable.ic_love_);

                if(list.get(position).getLikedStatus())
                    temp.img_love.setLiked(true);
                else
                    temp.img_love.setLiked(false);



               temp.img_love.setOnLikeListener(new OnLikeListener() {
                    @Override
                    public void liked(LikeButton likeButton) {
                        if (BeuSalonsSharedPrefrence.getUserId()!=null){
                            postlike(act,list.get(pos).getId(),pos);
                            if(list.get(pos).getLikedStatus())
                                temp.txt_likes.setText(""+(list.get(pos).getPostLikes())+ " Likes");

                            else  temp.txt_likes.setText(""+(Integer.valueOf(list.get(pos).getPostLikes())+1)+ " Likes");
                        }

                        else {
                           // Util.createLoginAlert(act);
                           temp.img_love.setLiked(false);
                        }


                    }

                    @Override
                    public void unLiked(LikeButton likeButton) {
                        postlike(act,list.get(pos).getId(),pos);
                            postlike(act,list.get(pos).getId(),pos);
                            if(list.get(pos).getLikedStatus())
                                temp.txt_likes.setText(""+(Integer.valueOf(list.get(pos).getPostLikes())-1)+ " Likes");
                            else temp.txt_likes.setText(""+(list.get(pos).getPostLikes())+ " Likes");

                    }
                });

                temp.img_comment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

//                        if (BeuSalonsSharedPrefrence.getUserId()!=null){
//
//                            Intent intent=new Intent(view.getContext(),CommentActivity.class);
//                            Bundle bundle = new Bundle();
//                            intent.putExtra(POST_ID,list.get(pos).getId());
////                                    intent.putExtra(CREATIVE_FIELD, new Gson().toJson(postDetailData));
////                                    intent.putExtra(COLLECTIONNAME,collectionName);
//                            intent.putExtras(bundle);
//
//                            view.getContext().startActivity(intent);
//                        }else Util.createLoginAlert(view.getContext());
                    }
                });


                break;
            case PROGRESS_TYPE:
                break;
            }
        }


    @Override
    public int getItemCount() {
        if(list!=null &&
                list.size()>0)
            return list.size();
        return 0;
    }



    public void setInitialData(List<Project> list){

        this.list=list;
        notifyDataSetChanged();
    }

    public void addData(List<Project> list){

        int size= this.list.size();
        Log.i("sizeing", "size of new: "+ list.size()+ " "+ "old: "+ this.list.size());
        this.list.addAll(list);             //add kara
        Log.i("sizeing", "size: "+ this.list.size());
        notifyItemRangeInserted(size, this.list.size());        //existing size, aur new size
    }

    public void addProgress(){

        if(list!=null && list.size()>0){

            list.add(new Project(1));
            if (list.size()>1){
                Handler handler = new Handler();

                final Runnable r = new Runnable() {
                    public void run() {
                        notifyItemInserted(list.size()-1);
                    }
                };

                handler.post(r);
            }

        }
    }

    public void removeProgress(){

        int size= list!=null && list.size()>0?list.size():0;

        if(size>0){

            list.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }
    @Override
    public int getItemViewType(int position) {

        if(list!=null && list.size()>0)
            return list.get(position).getType();
        return 0;
    }

    public class ViewHolder1 extends RecyclerView.ViewHolder{

        //private LinearLayout linear_;
//        ViewPager pager;
//        PageIndicatorView indicator;
//        //love button hai
//        LikeButton img_love;
//        ImageView img_comment;
//        TextView txt_like;
//        TextView txt_comment;
//        LinearLayout linear_tag;
//        RecyclerView recycler_tags;
//        public ViewHolder1(View itemView) {
//            super(itemView);
//            pager = (ViewPager) itemView.findViewById(R.id.pager);
//            indicator = (PageIndicatorView) itemView.findViewById(R.id.pageIndicatorView);
//            img_love= (LikeButton)itemView.findViewById(R.id.img_love);
//            img_comment= (ImageView)itemView.findViewById(R.id.img_comment);
//            txt_like= (TextView)itemView.findViewById(R.id.txt_like);
//            txt_comment= (TextView)itemView.findViewById(R.id.txt_comment);
//            linear_tag= (LinearLayout)itemView.findViewById(R.id.linear_tag);
//            recycler_tags=itemView.findViewById(R.id.recycler_tags);
//        }

        public TextView txt_name, txt_rating, txt_collection_name, txt_likes, txt_location,txtFollowing;
        public ImageView img_user_, img_comment;
        SquareImageView img_profile_pic;
        public LikeButton img_love;
        public LinearLayout linear_tag;
        public LinearLayout card_;
        public RecyclerView recycler_creative;
        public ViewHolder1(View view) {
            super(view);

            txt_name= (TextView)view.findViewById(R.id.txt_artist_name);
            txt_rating= (TextView)view.findViewById(R.id.txt_rating);
            txt_collection_name= (TextView)view.findViewById(R.id.txt_collection_name);
            txt_likes= (TextView)view.findViewById(R.id.txt_likes);
            txt_location= (TextView)view.findViewById(R.id.txt_location);
            img_profile_pic= (SquareImageView)view.findViewById(R.id.img_profile_pic);
            img_user_= (ImageView)view.findViewById(R.id.img_user_);
            img_love= (LikeButton)view.findViewById(R.id.img_love);

            linear_tag= (LinearLayout)view.findViewById(R.id.linear_tag);
            card_= view.findViewById(R.id.card_);
            txtFollowing=view.findViewById(R.id.txtFollowing);
            recycler_creative=view.findViewById(R.id.recycler_creative);
            img_comment= view.findViewById(R.id.img_comment);

        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        private Project project;
        int pos;
        public ScreenSlidePagerAdapter(FragmentManager fm, Project project, int pos) {
            super(fm);
            this.project= project;
            this.pos=pos;
        }

        @Override
        public Fragment getItem(int position) {
            return new ImageSlideSearchFragment().newInstance(project.getImages().get(position),project.getId(),project.getArtistId(),false,pos,project.getArtistName(),"");
        }

        @Override
        public int getCount() {
            if(project.getImages()!=null &&
                    project.getImages().size()>0)
                return project.getImages().size();
            return 0;
        }
    }
    public class ViewHolder2 extends RecyclerView.ViewHolder{

        private ProgressBar progress_;
        public ViewHolder2(View itemView) {
            super(itemView);
            progress_= (ProgressBar)itemView.findViewById(R.id.progress_);
        }
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

//    private void viewedByMe(String postId){
//        Retrofit retrofit= ServiceGenerator.getPortfolioClient();
//        ApiInterface apiInterface= retrofit.create(ApiInterface.class);
//        ViewByMe_post viewByMe_post=new ViewByMe_post();
//        viewByMe_post.setPostId(postId);
//        Call<ViewByMe_response> call=apiInterface.viewedByMe(viewByMe_post);
//
//        call.enqueue(new Callback<ViewByMe_response>() {
//            @Override
//            public void onResponse(Call<ViewByMe_response> call, Response<ViewByMe_response> response) {
//                if (response.isSuccessful()){
//                    if (response.body()!=null){
//
//                        if (response.body().getSuccess()){
//                            Log.i("homestuff", "i'm in : isSuccess trrue");
//
//                        }else{
//                            Log.i("homestuff", "i'm in : isSuccess false ");
//
//                        }
//                    }
//                }else{
//                    Log.i("homestuff", "i'm in : isSuccessful false");
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ViewByMe_response> call, Throwable t) {
//                Log.i("homestuff", "i'm in failure: view by me "+t.getMessage()+ " " + t.getCause()+ "   "+ t.getStackTrace());
//
//            }
//        });
//
//    }



    private void postlike(final Context context, String post_id,int pos){

        PostLike post= new PostLike();
        if (BeuSalonsSharedPrefrence.getUserId()!=null)
            post.setUserId(BeuSalonsSharedPrefrence.getUserId());
        post.setArtistId(list.get(pos).getArtistId());
        post.setName(BeuSalonsSharedPrefrence.getUserName());
        post.setPostId(post_id);

        Retrofit retrofit= ServiceGenerator.getPortfolioClient();
        final ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<ResponseLike> call= apiInterface.postLike(post);
        call.enqueue(new Callback<ResponseLike>() {
            @Override
            public void onResponse(Call<ResponseLike> call, Response<ResponseLike> response) {

                if(response.isSuccessful()){

                    if(response.body().isSuccess()){

//                        Toast.makeText(context ,response.body().getData(),Toast.LENGTH_SHORT).show();


                    }else{
                        Log.i("likepostka", "not success pe hai");
                    }


                }else{

                    Log.i("likepostka", "else pe hai");
                }


            }

            @Override
            public void onFailure(Call<ResponseLike> call, Throwable t) {
                Log.i("likepostka", "failure: "+ t.getMessage()+ " " + t.getStackTrace());
            }
        });

    }

}
