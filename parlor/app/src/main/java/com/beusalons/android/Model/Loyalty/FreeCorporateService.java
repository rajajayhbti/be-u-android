package com.beusalons.android.Model.Loyalty;

import java.util.List;

/**
 * Created by myMachine on 8/18/2017.
 */

public class FreeCorporateService {

    private String id;
    private String expiresAt;
    private int discount;
    private String name;
    private int price;
    private int noOfService;
    private String parlorId;
    private int dealId;

    private int priceId;
    private int serviceCode;
    private String serviceId;

    private String brandId;
    private String productId;

    private String dealType;
    private String description;
    private String categoryId;
    private String createdAt;
    private String source;
    private List<Upgrade> upgrade = null;
    private Boolean enableUpgrade;
    private boolean availed;




    public int getPriceId() {
        return priceId;
    }

    public void setPriceId(int priceId) {
        this.priceId = priceId;
    }

    public boolean isAvailed() {
        return availed;
    }

    public void setAvailed(boolean availed) {
        this.availed = availed;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(String expiresAt) {
        this.expiresAt = expiresAt;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getNoOfService() {
        return noOfService;
    }

    public void setNoOfService(int noOfService) {
        this.noOfService = noOfService;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }

    public int getDealId() {
        return dealId;
    }

    public void setDealId(int dealId) {
        this.dealId = dealId;
    }

    public int getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(int serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public List<Upgrade> getUpgrade() {
        return upgrade;
    }

    public void setUpgrade(List<Upgrade> upgrade) {
        this.upgrade = upgrade;
    }

    public Boolean getEnableUpgrade() {
        return enableUpgrade;
    }

    public void setEnableUpgrade(Boolean enableUpgrade) {
        this.enableUpgrade = enableUpgrade;
    }
}
