package com.beusalons.android.Model.newServiceDeals.ServiceByDepartment;

import com.beusalons.android.Model.newServiceDeals.NewCombo.Selector;

import java.util.List;

/**
 * Created by myMachine on 5/30/2017.
 */

public class Package {

    private String dealId;
    private String parlorDealId;
    private String name;
    private String description;
    private String shortDescription;
    private int menuPrice;
    private int price;
    private int dealPrice;
    private String slabId;
    private String dealType;
    private List<Object> newCombo = null;
    private List<Selector> selectors = null;

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getParlorDealId() {
        return parlorDealId;
    }

    public void setParlorDealId(String parlorDealId) {
        this.parlorDealId = parlorDealId;
    }

    public String getSlabId() {
        return slabId;
    }

    public void setSlabId(String slabId) {
        this.slabId = slabId;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(int menuPrice) {
        this.menuPrice = menuPrice;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(int dealPrice) {
        this.dealPrice = dealPrice;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public List<Object> getNewCombo() {
        return newCombo;
    }

    public void setNewCombo(List<Object> newCombo) {
        this.newCombo = newCombo;
    }

    public List<Selector> getSelectors() {
        return selectors;
    }

    public void setSelectors(List<Selector> selectors) {
        this.selectors = selectors;
    }
}
