package com.beusalons.android.Model;

/**
 * Created by myMachine on 1/22/2017.
 */

public class BookingSummaryServiceModel {

    private String name;
    private double price;
    private int quantity;
    private int serviceCode;


    private int booking_service_price;          //booking service individual service price

    public int getBooking_service_price() {
        return booking_service_price;
    }

    public void setBooking_service_price(int booking_service_price) {
        this.booking_service_price = booking_service_price;
    }

    // the below static int is for multiple viewholder in the booking summary activity page
    public static final int TYPE_SERVICE=0;
    public static final int TYPE_OFFERS= 1;

    private int view_types;
    private Boolean check;

    public int getView_types() {
        return view_types;
    }

    public void setView_types(int view_types) {
        this.view_types = view_types;
    }

    public Boolean getCheck() {
        return check;
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(int serviceCode) {
        this.serviceCode = serviceCode;
    }
}
