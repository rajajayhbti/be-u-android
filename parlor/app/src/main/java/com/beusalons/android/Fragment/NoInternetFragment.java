package com.beusalons.android.Fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beusalons.android.Event.RetryEvent;
import com.beusalons.android.R;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by myMachine on 5/5/2017.
 */

public class NoInternetFragment extends DialogFragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.fragment_no_internet, container);


        final ProgressBar spinner_no_internet= (ProgressBar)view.findViewById(R.id.spinner_no_internet);
        spinner_no_internet.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getActivity(),
                R.color.colorPrimary),
                android.graphics.PorterDuff.Mode.MULTIPLY);
        spinner_no_internet.setVisibility(View.GONE);

        final MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.progress_retry);

        TextView txt_retry= (TextView) view.findViewById(R.id.txt_retry);
        txt_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                spinner_no_internet.setVisibility(View.VISIBLE);
                countDown(spinner_no_internet, mp);
                EventBus.getDefault().post(new RetryEvent());

            }
        });

        TextView txt_exit= (TextView)view.findViewById(R.id.txt_exit);
        txt_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finishAffinity();
            }
        });


        return view;
    }

    private void countDown(final ProgressBar progressBar, final MediaPlayer mp){
        new CountDownTimer(5000, 1000) {

            public void onTick(long millisUntilFinished) {


            }

            public void onFinish() {

                progressBar.setVisibility(View.GONE);
                mp.start();
            }
        }.start();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        Window window = getDialog().getWindow();
//        ViewGroup.LayoutParams attributes = window.getAttributes();
        //must setBackgroundDrawable(TRANSPARENT) in onActivityCreated()
        window.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFFFFF")));

        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }


}
