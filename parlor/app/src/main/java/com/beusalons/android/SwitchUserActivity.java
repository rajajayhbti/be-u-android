package com.beusalons.android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.beusalons.android.Model.Registration.Registration_Post;
import com.beusalons.android.Model.Registration.Registration_Response;
import com.beusalons.android.Model.SocialLogin.LoginResponse;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Ashish Sharma on 1/17/2018.
 */

public class SwitchUserActivity extends AppCompatActivity {
    private ProgressBar progressBar;
    private EditText etxt_phone, etxt_refferal;
    private Button btn_next_active, btn_next_inactive;
    private LinearLayout linear_number;
    private int retry= 0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_phone);


        progressBar= (ProgressBar)findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);

        linear_number= (LinearLayout)findViewById(R.id.linear_number);
        linear_number.setVisibility(View.VISIBLE);

        btn_next_inactive= (Button)findViewById(R.id.btn_next_inactive);
        btn_next_active= (Button)findViewById(R.id.btn_next_active);
        btn_next_inactive.setVisibility(View.VISIBLE);
        btn_next_inactive.setClickable(false);
        setToolBar();

        etxt_phone= (EditText)findViewById(R.id.etxt_phone);
        etxt_phone.setTransformationMethod(new NumericKeyBoardTransformationMethod());      //method to convert password dot to digit
        etxt_phone.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        imm.showSoftInput(etxt_phone, InputMethodManager.SHOW_FORCED);


        etxt_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length()>=10){
                    btn_next_active.setVisibility(View.VISIBLE);
                    btn_next_inactive.setVisibility(View.GONE);
                }else{
                    btn_next_inactive.setVisibility(View.VISIBLE);
                    btn_next_inactive.setClickable(false);
                    btn_next_active.setVisibility(View.GONE);
                }

            }
        });

        btn_next_active.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fetchData();
            }
        });
    }
    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle("Switch User");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);

        }

    }
    private class NumericKeyBoardTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return source;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    public void fetchData(){
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        linear_number.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        etxt_phone.setClickable(false);
        btn_next_active.setClickable(false);


        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface= retrofit.create(ApiInterface.class);

        Call<LoginResponse> call= apiInterface.getOtherUserDetail(etxt_phone.getText().toString());
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                Log.i("loginphoneacti", "i'm in onResponse");
                try{
                    if(response.isSuccessful()){


                        if(response.body().isSuccess()){
                            linear_number.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            etxt_phone.setClickable(true);
                            btn_next_active.setClickable(true);
                            etxt_phone.requestFocus();
                            Intent intent= new Intent(SwitchUserActivity.this, FetchingLocationActivity.class);
                            SharedPreferences globalPreference= getSharedPreferences
                                    ("globalPreference", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editorr= globalPreference.edit();
                            editorr.putBoolean("firstLogin", false).apply();

                            SharedPreferences sharedPref= getSharedPreferences
                                    ("userDetails", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor= sharedPref.edit();
                            editor.putString("name", response.body().getData().getName());
                            editor.putString("gender", response.body().getData().getGender());
                            editor.putString("emailId", response.body().getData().getEmailId());
                            editor.putString("phoneNumber", response.body().getData().getPhoneNumber());
                            editor.putString("userId", response.body().getData().getUserId());
                            editor.putString("accessToken", response.body().getData().getAccessToken());
                            editor.putString("profilePic", response.body().getData().getProfilePic());
                            editor.putBoolean("isLoggedIn", true);
                            editor.putInt("loyaltyPoints", 0);
                            editor.putBoolean("offerDialog", true);
                            editor.putString("promoCode", "BEU");
                            editor.putBoolean("userType", response.body().getData().isUserType());  //yeh apne logo ke liye hai
                            editor.putBoolean("firebase_", false);

                            editor.putBoolean(BeuSalonsSharedPrefrence.VERIFY_CORPORATE, false);           //corporate walo ka hai yeh

                            editor.apply();
                            BeuSalonsSharedPrefrence.setLogin(true);
                            startActivity(intent);
                            finishAffinity();

                        }else{
                            linear_number.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            etxt_phone.setClickable(true);
                            btn_next_active.setClickable(true);
                            etxt_phone.requestFocus();

                            Intent intent= new Intent(SwitchUserActivity.this, OtpVerificationActivity.class);
                            intent.putExtra("phoneNumber", ""+etxt_phone.getText().toString());

                            startActivity(intent);
                            Log.i("loginphoneacti", "i'm in onResponse else");
                        }
                    }else{

                        //// TODO: 3/25/2017 toast dikhao
                        Log.i("loginphoneacti", "i'm in onResponse else else");
                        btn_next_active.setClickable(true);
                        Toast.makeText(SwitchUserActivity.this, "Please try again", Toast.LENGTH_SHORT).show();

                        // finish();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                    Log.i("loginphoneacti", "i'm in onResponse");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

                if(retry<2){

                    fetchData();
                    retry++;
                }else{
                    linear_number.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    etxt_phone.setClickable(true);
                    btn_next_active.setClickable(true);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                    etxt_phone.requestFocus();
                    imm.showSoftInput(etxt_phone, InputMethodManager.SHOW_FORCED);
                    Toast.makeText(SwitchUserActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

                Log.i("loginphoneacti", "i'm in onFailure"+ t.getMessage()+ "  "+ t.getCause());

            }
        });

    }
}
