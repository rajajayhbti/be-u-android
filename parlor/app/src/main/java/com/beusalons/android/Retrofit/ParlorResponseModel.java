package com.beusalons.android.Retrofit;

import com.beusalons.android.Model.ParlorModel;
import com.beusalons.android.Model.StatusModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robbin Singh on 03/11/2016.
 */
public class ParlorResponseModel{

    private Boolean success;
    private String message;
    private Data data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data{
        @SerializedName("parlors")
        @Expose
        private List<ParlorModel> parlors = new ArrayList<ParlorModel>();

        public List<ParlorModel> getParlors() {
            return parlors;
        }

        public void setParlors(List<ParlorModel> parlors) {
            this.parlors = parlors;
        }
    }

}

