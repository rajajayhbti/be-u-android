package com.beusalons.android.Model.HomeFragmentModel;

import java.util.List;

/**
 * Created by myMachine on 9/4/2017.
 */

public class Carousel{

    private String imageUrl;
    private String action;
    private String code;
    private List<SelectedDeals> selectedDeals;

    public List<SelectedDeals> getSelectedDeals() {
        return selectedDeals;
    }

    public void setSelectedDeals(List<SelectedDeals> selectedDeals) {
        this.selectedDeals = selectedDeals;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
