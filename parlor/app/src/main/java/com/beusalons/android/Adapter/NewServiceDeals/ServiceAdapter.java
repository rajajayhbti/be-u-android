package com.beusalons.android.Adapter.NewServiceDeals;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Dialog.ShowDetailsServiceDialog;
import com.beusalons.android.Event.NewServicesEvent.AddServiceEvent;
import com.beusalons.android.Fragment.ServiceDesciptionFragment;
import com.beusalons.android.Fragment.ServiceFragments.ComboBottomSheet;
import com.beusalons.android.Fragment.ServiceFragments.NewComboBottomSheet;
import com.beusalons.android.Fragment.ServiceFragments.ServiceSpecificDialogFragment;
import com.beusalons.android.Fragment.UserCartFragment;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.SalonHome.HomeResponse;
import com.beusalons.android.Model.Share.ShareSalonService;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Service;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Slabs;
import com.beusalons.android.R;
import com.beusalons.android.ServiceSpecificActivity;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

/**
 * Created by myMachine on 5/23/2017.
 */

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder> {

    private static final String TAG="service_dialog";

    private Context context;
    private List<Service> list;
    private Slabs slabs;
    private UserCart user_cart;

    private AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;
    private  HomeResponse home_response;

    public ServiceAdapter(Context context, List<Service> list, Slabs slabs, UserCart user_cart, HomeResponse home_response){

        EventBus.getDefault().register(this);

        this.context= context;
        this.list= list;
        this.slabs = slabs;
        this.user_cart= user_cart;
        this.home_response=home_response;
        logger = AppEventsLogger.newLogger(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {


        final int pos= position;
//        txt_quantity_= holder.txt_quantity;

        if (position%4==2){
            holder.linearLayoutTop.setBackgroundColor(context.getResources().getColor(R.color.blue_DARK));
            holder.linear_add_.setBackgroundResource(R.drawable.drawable_blue);
        }
        else if (position%4==0){
            holder.linearLayoutTop.setBackgroundColor(context.getResources().getColor(R.color.pink_red));
            holder.linear_add_.setBackgroundResource(R.drawable.drawable_pink);
        }
        else if (position%4==1){
            holder.linearLayoutTop.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.linear_add_.setBackgroundResource(R.drawable.linear_add_btn);
        }
        else if (position%4==3){
            holder.linearLayoutTop.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.linear_add_.setBackgroundResource(R.drawable.linear_add_btn);
        }


        holder.txt_name.setText(list.get(pos).getName());

        if(list.get(pos).getQuantity()>0){

            holder.txt_quantity.setText(""+list.get(pos).getQuantity());
            holder.linear_add_.setVisibility(View.GONE);
            holder.linear_add_remove.setVisibility(View.VISIBLE);
        }else{

            holder.linear_add_.setVisibility(View.VISIBLE);
            holder.linear_add_remove.setVisibility(View.GONE);
        }


        //img for share item

        holder.img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                logSeviceSharedEvent(list.get(pos).getName());
                EventBus.getDefault().post(new ShareSalonService(list.get(pos).getServiceCode(), pos));
            }
        });




        //check if service ya package
        boolean isPackage;
        if(list.get(pos).getCategory().equalsIgnoreCase("service")){

            isPackage= false;

//            holder.linearLayoutTop.setPadding(8,45,8,45);
//            holder.linearlayoutForPrice.setPadding(0,15,0,0);



            if(list.get(pos).getDescription()!=null &&
                    list.get(pos).getDescription().length()>0){

                holder.linear_description.setVisibility(View.VISIBLE);
                String des="";
                if(list.get(pos).getDescription().length()<=25){

                    holder.txt_description.setClickable(false);
                    des= list.get(pos).getDescription();
                    holder.txt_description.setText(des);
                }
                else{

                    holder.txt_description.setClickable(true);

                    des=list.get(pos).getDescription().substring(0, 25)+ "<font color='#00B8CA'> ... more</font>";
                    holder.txt_description.setText(fromHtml(des));
                }
            }else
                holder.linear_description.setVisibility(View.INVISIBLE);

            holder.linear_description.setVisibility(View.INVISIBLE);
            holder.txt_description.setVisibility(View.GONE);
            holder.imgshowdetails.setVisibility(View.VISIBLE);
            //agar subtitle hai toh ....
            if(list.get(pos).isFirstSubtitleElement()){
                holder.ll_package_header.setVisibility(View.GONE);
                holder.txt_package_header.setVisibility(View.VISIBLE);
                holder.txt_package_header.setText(list.get(pos).getSubTitle());
                holder.txt_package_header.setPadding(40,25,8,8);
                holder.txt_package_header.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
                holder.txt_package_header.setVisibility(View.VISIBLE);
                holder.txt_package_header.setTypeface(Typeface.DEFAULT_BOLD);
            }else{

                holder.txt_package_header.setVisibility(View.GONE);
                holder.ll_package_header.setVisibility(View.GONE);
            }

            //paisa bolta hai
            if (list.get(pos).getDealId()!=null){

                holder.img_.setImageResource(R.drawable.ic_deal_icon);
                holder.txt_save_per.setVisibility(View.VISIBLE);
                holder.txt_menu_price.setVisibility(View.VISIBLE);

                if(list.get(pos).getDealType().equalsIgnoreCase("chooseOnePer")){

                    int price= list.get(pos).getMenuPrice()- (int)
                            (((double)list.get(pos).getDealPrice()/100)*list.get(pos).getMenuPrice());
                    //price with tax
                    int price_= (int) Math.round(BeuSalonsSharedPrefrence.getServiceTax()*price);
                    holder.txt_price.setText(AppConstant.CURRENCY+price_);
                    holder.txt_save_per.setText(AppConstant.SAVE+" "+
                            list.get(pos).getDealPrice()+"%");

                }else{

                    int price= list.get(pos).getDealPrice();
                    //price with tax
                    int price_= (int) Math.round(BeuSalonsSharedPrefrence.getServiceTax()*price);
                    int menu_price= list.get(pos).getPrices().get(0).getPrice();
                    //menuprice with tax
                    int menu_price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*menu_price);

                    holder.txt_price.setText(AppConstant.CURRENCY+price_);
                    holder.txt_save_per.setText(AppConstant.SAVE+" "+
                            (100 -((price_*100)/menu_price_)+"%"));
                }

                if(list.get(pos).getPrices().get(0).getAdditions()!=null &&
                        list.get(pos).getPrices().get(0).getAdditions().size()>0 &&
                        list.get(pos).getPrices().get(0).getAdditions().get(0).getTypes()!=null &&
                        list.get(pos).getPrices().get(0).getAdditions().get(0).getTypes().size()>0){

                    int size= list.get(pos).getPrices().get(0).getAdditions().get(0).getTypes().size();

                    int price= list.get(pos).getDealPrice();
                    //with tax
                    int price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*price);

                    int menu_price= list.get(pos).getPrices().get(0).getPrice()+
                            list.get(pos).getPrices().get(0).getAdditions().get(0).getTypes().get(size-1).getAdditions();
                    //menuprice plus tax
                    int menu_price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*menu_price);

                    holder.txt_menu_price.setText(AppConstant.CURRENCY+menu_price_);
                    holder.txt_save_per.setText(AppConstant.SAVE+" "+
                            (100 -((price_*100)/menu_price_)+"%"));
                }else{

                    int menu_price= list.get(pos).getPrices().get(0).getPrice();
                    //menu price with tax
                    int menu_price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*menu_price);

                    holder.txt_menu_price.setText(AppConstant.CURRENCY+menu_price_);
                }
                holder.txt_menu_price.setPaintFlags(holder.txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


            }else{

                holder.img_.setImageResource(R.drawable.ic_radio_empty_small);

                int price= list.get(pos).getPrices().get(0).getPrice();
                //price with tax
                int price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*price);
                holder.txt_price.setText(AppConstant.CURRENCY+price_);

                holder.txt_save_per.setVisibility(View.GONE);
                holder.txt_menu_price.setVisibility(View.GONE);
            }



            holder.linear_header_show_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    logShowDetailPackageEvent();
                    logShowDetailPackageFireBaseEvent();
                    ServiceDesciptionFragment fragment= new ServiceDesciptionFragment();
                    Bundle bundle= new Bundle();
                    bundle.putString(ServiceDesciptionFragment.NAME, list.get(pos).getName());
                    bundle.putString(ServiceDesciptionFragment.DESCRIPTION, list.get(pos).getDescription());
                    bundle.putString(ServiceDesciptionFragment.TIME, list.get(pos).getEstimatedTime());

                    fragment.setArguments(bundle);
                    fragment.show(((ServiceSpecificActivity)view.getContext()).getFragmentManager(),
                            ServiceDesciptionFragment.THIS_FRAGMENT);
                }
            });

        }else{              //services mai packages ka case hai yeh




            holder.imgshowdetails.setVisibility(View.GONE);

            isPackage= true;

            //agar packages ka first element hai toh package naam ka header dikhayo
            if(list.get(pos).isFirstPackageElement()){
                holder.ll_package_header.setVisibility(View.VISIBLE);
                holder.txt_package_header.setVisibility(View.GONE);
                //holder.txt_package_header.setText("PACKAGES");
            }else{
                holder.ll_package_header.setVisibility(View.GONE);
                holder.txt_package_header.setVisibility(View.GONE);
            }


            if (list.get(pos).getDealId()!=null){

                int price= list.get(pos).getDealPrice();
                int price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*price);

                int menu_price= list.get(pos).getMenuPrice();
                //with tax
                int menu_price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*menu_price);

                holder.txt_menu_price.setText(AppConstant.CURRENCY+menu_price_);
                Paint paint=new Paint();
                paint.setColor(context.getResources().getColor(R.color.fontColor));
                holder.txt_menu_price.setPaintFlags(  holder.txt_menu_price.getPaintFlags() | paint.STRIKE_THRU_TEXT_FLAG);

                holder.txt_save_per.setText(AppConstant.SAVE+" "+
                        (100 -((price_*100)/menu_price_)+"%"));

                holder.txt_save_per.setVisibility(View.VISIBLE);
                holder.txt_menu_price.setVisibility(View.VISIBLE);
            }
            else{
                holder.txt_save_per.setVisibility(View.GONE);
                holder.txt_menu_price.setVisibility(View.GONE);
            }
            holder.img_.setImageResource(R.drawable.ic_package);

            int price= list.get(pos).getDealPrice();
            //price with tax
            int price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*price);
            holder.txt_price.setText(AppConstant.CURRENCY+price_);

            if(list.get(pos).getDescription()!=null &&
                    list.get(pos).getDescription().length()>0){

                holder.linear_description.setVisibility(View.VISIBLE);
                String des="";
                if(list.get(pos).getDescription().length()<=25){

                    holder.txt_description.setClickable(false);
                    des= list.get(pos).getDescription();
                    holder.txt_description.setText(des);
                }
                else{

                    holder.txt_description.setClickable(true);

                    des=list.get(pos).getDescription().substring(0, 25)+ "<font color='#00B8CA'> ... more</font>";
                    holder.txt_description.setText(fromHtml(des));
                }
            }else
                holder.linear_description.setVisibility(View.INVISIBLE);




            if(BeuSalonsSharedPrefrence.getDiscount(context)!=null &&
                    BeuSalonsSharedPrefrence.getDiscount(context).size()>0){

                //price_ tax included hai
                if((price_ >= BeuSalonsSharedPrefrence.getDiscount(context).get(0).getMin()) &&
                        price_ < BeuSalonsSharedPrefrence.getDiscount(context).get(0).getMax()){
                    holder.linear_validity_.setVisibility(View.VISIBLE);

                    holder.txt_validity_.setText("Validity: "+ BeuSalonsSharedPrefrence.getDiscount(context).get(0).getValidity());
                    holder.txt_discount_.setText(BeuSalonsSharedPrefrence.getDiscount(context).get(0).getDiscountPercent()+
                            "% Package Discount Included");

                }else if((price_ >= BeuSalonsSharedPrefrence.getDiscount(context).get(1).getMin()) &&
                        price_ < BeuSalonsSharedPrefrence.getDiscount(context).get(1).getMax()){
                    holder.linear_validity_.setVisibility(View.VISIBLE);

                    holder.txt_validity_.setText("Validity: "+ BeuSalonsSharedPrefrence.getDiscount(context).get(1).getValidity());
                    holder.txt_discount_.setText(BeuSalonsSharedPrefrence.getDiscount(context).get(1).getDiscountPercent()+
                            "% Package Discount Included");

                }else if((price_ >= BeuSalonsSharedPrefrence.getDiscount(context).get(2).getMin()) &&
                        price_ <= BeuSalonsSharedPrefrence.getDiscount(context).get(2).getMax()){
                    holder.linear_validity_.setVisibility(View.VISIBLE);
                    holder.txt_validity_.setText("Validity: "+ BeuSalonsSharedPrefrence.getDiscount(context).get(2).getValidity());
                    holder.txt_discount_.setText(BeuSalonsSharedPrefrence.getDiscount(context).get(2).getDiscountPercent()+
                            "% Package Discount Included");
                }else
                    holder.linear_validity_.setVisibility(View.GONE);

            }else
                holder.linear_validity_.setVisibility(View.GONE);


            holder.linear_description.setVisibility(View.VISIBLE);
            holder.txt_description.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    logShowDetailPackageEvent();
                    logShowDetailPackageFireBaseEvent();
//                    ServiceDesciptionFragment fragment= new ServiceDesciptionFragment();
//                    Bundle bundle= new Bundle();
//                    bundle.putString(ServiceDesciptionFragment.NAME, list.get(pos).getName());
//                    bundle.putString(ServiceDesciptionFragment.DESCRIPTION, list.get(pos).getDescription());
//                    if (list.get(pos).getEstimatedTime()!=null){
//                        bundle.putString(ServiceDesciptionFragment.TIME, list.get(pos).getEstimatedTime());
//
//                    }else{
//                        bundle.putString(ServiceDesciptionFragment.TIME,"");
//                    }
//
//                    fragment.setArguments(bundle);
//                    fragment.show(((ServiceSpecificActivity)v.getContext()).getFragmentManager(),
//                            ServiceDesciptionFragment.THIS_FRAGMENT );

                    new ShowDetailsServiceDialog((Activity) context,list.get(pos).getName(),list.get(pos).getDescription());
                }
            });
//            holder.linear_layout_show_detail.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    logShowDetailPackageEvent();
//                    logShowDetailPackageFireBaseEvent();
////                    new ShowDetailsServiceDialog((Activity) context,list.get(pos).getName(),list.get(pos).getDescription());
//                }
//            });
        }

        final boolean is_package= isPackage;

        holder.linear_add_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addStuff(pos, is_package);
                String price=holder.txt_price.getText().toString();

                logViewedContentEvent(list.get(pos).getName(),"service","INR",
                        Double.valueOf(price.replaceFirst(AppConstant.CURRENCY,"")));
            }
        });

        holder.linear_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addStuff(pos, is_package);
            }
        });

        holder.linear_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UserCartFragment fragment= new UserCartFragment();
                Bundle bundle= new Bundle();
                bundle.putBoolean("has_data", true);
                bundle.putString(UserCartFragment.HOME_RESPONSE_DATA,new Gson().toJson(home_response, HomeResponse.class));
                fragment.setArguments(bundle);
                fragment.show(((ServiceSpecificActivity)context).getSupportFragmentManager(), "user_cart");
            }
        });


    }



//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onMessageEvent(UserCart event) {
//
//        Log.i("abemaitoh", "in the quanity event: ");
//
//        for(int i=0;i<event.getServicesList().size();i++){
//            for(int j=0;j<services.size();j++){
//
//                if(event.getServicesList().get(i).getService_id().
//                        equalsIgnoreCase(services.get(j).getServiceId())){
//                    Log.i("abemaitoh", "in the quanity eventtt: "+ event.getServicesList().get(i).getQuantity());
//                    services.get(j).setQuantity(event.getServicesList().get(i).getQuantity());
//                }
//            }
//        }
//
//        notifyDataSetChanged();
//    }


    private void addStuff(int pos, boolean is_package){

        if(!is_package){         //service hai toh window

            if((list.get(pos).getPrices()!=null && list.get(pos).getPrices().size()>0) &&
                    (list.get(pos).getPrices().get(0).getBrand()==null ||
                            list.get(pos).getPrices().get(0).getBrand().getBrands().size()<1) &&
                    (list.get(pos).getPrices().get(0).getAdditions() ==null ||
                            list.get(pos).getPrices().get(0).getAdditions().size()<1) &&
                    (list.get(pos).getUpgrades()==null || list.get(pos).getUpgrades().size()<1) &&
                    (list.get(pos).getPackages() ==null || list.get(pos).getPackages().size()<1)){

                Log.i("ispeha", "i'm in 1");
                //add normal service here
                addService(list.get(pos));

//                list.get(pos).setQuantity(list.get(pos).getQuantity()+1);
//                updateQuantity(list.get(pos).getQuantity(), txt_quantity);

                logServiceSelectedEvent(list.get(pos).getName());
                logServiceSelectedFireBaseEvent(list.get(pos).getName());



                // use event for add to card direct without edit service page open
                int price= list.get(pos).getMenuPrice()- (int)
                        (((double)list.get(pos).getDealPrice()/100)*list.get(pos).getMenuPrice());
                //price with tax
                int price_= (int) Math.round(BeuSalonsSharedPrefrence.getServiceTax()*price);

            }else if((list.get(pos).getPrices()!=null && list.get(pos).getPrices().size()>0) &&
                    ((list.get(pos).getPrices().get(0).getBrand()!= null &&
                            list.get(pos).getPrices().get(0).getBrand().getBrands()!=null &&
                            list.get(pos).getPrices().get(0).getBrand().getBrands().size()>1) ||
                            (list.get(pos).getPrices().get(0).getAdditions()!=null &&
                                    list.get(pos).getPrices().get(0).getAdditions().size()>0))){

                //yaha i'm not checking upgrade or package, because on need motherfucker
                //open window don't add anything,
                Log.i("ispeha", "i'm in 2");
                logEditServicePageOpenEvent();
                logEditServicePageOpenFireBaseEvent();
                ServiceSpecificDialogFragment fragment= new ServiceSpecificDialogFragment();
                Bundle bundle= new Bundle();
                bundle.putString("service", new Gson().toJson(list.get(pos), Service.class));
                bundle.putString("user_cart", new Gson().toJson(user_cart, UserCart.class));
                bundle.putString(ServiceSpecificDialogFragment.HOME_RESPONSE, new Gson().toJson(home_response, HomeResponse.class));
                bundle.putBoolean("add_service", false);
                bundle.putBoolean("add_", false);

                fragment.setArguments(bundle);
                fragment.show(((ServiceSpecificActivity)context).getSupportFragmentManager(), TAG);

            }else if((list.get(pos).getPrices()!=null && list.get(pos).getPrices().size()>0) &&
                    (list.get(pos).getPrices().get(0).getBrand()== null ||
                            list.get(pos).getPrices().get(0).getBrand().getBrands()==null ||
                            list.get(pos).getPrices().get(0).getBrand().getBrands().size()<1) &&
                    (list.get(pos).getPrices().get(0).getAdditions()==null ||
                            list.get(pos).getPrices().get(0).getAdditions().size()<1) &&
                    (list.get(pos).getUpgrades()!=null && list.get(pos).getUpgrades().size()>0 ||
                            list.get(pos).getPackages()!=null && list.get(pos).getPackages().size()>0)){

                //no brand  aur addition but package or upgrade, so add simple service inside

                Log.i("ispeha", "i'm in 3");
                logEditServicePageOpenEvent();
                logEditServicePageOpenFireBaseEvent();
                ServiceSpecificDialogFragment fragment= new ServiceSpecificDialogFragment();
                Bundle bundle= new Bundle();
                bundle.putString("service", new Gson().toJson(list.get(pos), Service.class));
                bundle.putString("user_cart", new Gson().toJson(user_cart, UserCart.class));
                bundle.putString(ServiceSpecificDialogFragment.HOME_RESPONSE, new Gson().toJson(home_response, HomeResponse.class));
                bundle.putBoolean("add_service", true);
                bundle.putBoolean("add_", false);

                fragment.setArguments(bundle);
                fragment.show(((ServiceSpecificActivity)context).getSupportFragmentManager(), TAG);

            }else if((list.get(pos).getPrices()!=null && list.get(pos).getPrices().size()>0) &&
                    (list.get(pos).getPrices().get(0).getBrand()!= null ||
                            list.get(pos).getPrices().get(0).getBrand().getBrands()!=null ||
                            list.get(pos).getPrices().get(0).getBrand().getBrands().size()==1) &&
                    (list.get(pos).getPrices().get(0).getBrand().getBrands().get(0).getProducts()==null ||
                            (list.get(pos).getPrices().get(0).getBrand().getBrands().get(0).getProducts()!=null &&
                                    list.get(pos).getPrices().get(0).getBrand().getBrands().get(0).getProducts().size()<=1)) &&
                    (list.get(pos).getPrices().get(0).getAdditions() ==null ||
                            list.get(pos).getPrices().get(0).getAdditions().size()<1) &&
                    (list.get(pos).getUpgrades() ==null || list.get(pos).getUpgrades().size() ==0) &&
                    (list.get(pos).getPackages() ==null || list.get(pos).getPackages().size() ==0)){

                //add brand service here
                Log.i("ispeha", "i'm in 4");
                singleBrandProduct(list.get(pos));

                logServiceSelectedEvent(list.get(pos).getName());
                logServiceSelectedFireBaseEvent(list.get(pos).getName());

                // use event for add to card direct without edit service page open
                int price= list.get(pos).getMenuPrice()- (int)
                        (((double)list.get(pos).getDealPrice()/100)*list.get(pos).getMenuPrice());
                //price with tax
                int price_= (int) Math.round(BeuSalonsSharedPrefrence.getServiceTax()*price);
               // logAddedToCartEvent(list.get(pos).getName(),"INR",price_);

            }else if((list.get(pos).getPrices()!=null && list.get(pos).getPrices().size()>0) &&
                    (list.get(pos).getPrices().get(0).getBrand()!= null ||
                            list.get(pos).getPrices().get(0).getBrand().getBrands()!=null ||
                            list.get(pos).getPrices().get(0).getBrand().getBrands().size()==1) &&
                    (list.get(pos).getPrices().get(0).getBrand().getBrands().get(0).getProducts()==null ||
                            (list.get(pos).getPrices().get(0).getBrand().getBrands().get(0).getProducts()!=null &&
                                    list.get(pos).getPrices().get(0).getBrand().getBrands().get(0).getProducts().size()<=1)) &&
                    (list.get(pos).getPrices().get(0).getAdditions() ==null ||
                            list.get(pos).getPrices().get(0).getAdditions().size()<1) &&
                    (list.get(pos).getUpgrades() !=null && list.get(pos).getUpgrades().size()>0) ||
                    (list.get(pos).getPackages() !=null && list.get(pos).getPackages().size()>0)){

                Log.i("ispeha", "i'm in 5: "+ list.get(pos).getPrices().get(0).getBrand().getBrands().size());
                logEditServicePageOpenEvent();
                logEditServicePageOpenFireBaseEvent();
                // add brand wali service inside
                ServiceSpecificDialogFragment fragment= new ServiceSpecificDialogFragment();
                Bundle bundle= new Bundle();
                bundle.putString("service", new Gson().toJson(list.get(pos), Service.class));
                bundle.putString("user_cart", new Gson().toJson(user_cart, UserCart.class));
                bundle.putString(ServiceSpecificDialogFragment.HOME_RESPONSE, new Gson().toJson(home_response, HomeResponse.class));
                bundle.putBoolean("add_service", false);
                bundle.putBoolean("add_", true);
//                bundle.putBoolean("add_", false);

                fragment.setArguments(bundle);
                fragment.show(((ServiceSpecificActivity)context).getSupportFragmentManager(), TAG);
            }else if((list.get(pos).getPrices()!=null && list.get(pos).getPrices().size()>0) &&
                    (list.get(pos).getPrices().get(0).getBrand()!= null ||
                            list.get(pos).getPrices().get(0).getBrand().getBrands()!=null ||
                            list.get(pos).getPrices().get(0).getBrand().getBrands().size()==1) &&
                    (list.get(pos).getPrices().get(0).getBrand().getBrands().get(0).getProducts()!=null &&
                            list.get(pos).getPrices().get(0).getBrand().getBrands().get(0).getProducts().size()>1)){

                //single brand but multiple products
                Log.i("ispeha", "i'm in 6 mai hoon");
                logEditServicePageOpenEvent();
                logEditServicePageOpenFireBaseEvent();
                ServiceSpecificDialogFragment fragment= new ServiceSpecificDialogFragment();
                Bundle bundle= new Bundle();
                bundle.putString("service", new Gson().toJson(list.get(pos), Service.class));
                bundle.putString("user_cart", new Gson().toJson(user_cart, UserCart.class));
                bundle.putString(ServiceSpecificDialogFragment.HOME_RESPONSE, new Gson().toJson(home_response, HomeResponse.class));
                bundle.putBoolean("add_service", false);
                bundle.putBoolean("add_", false);

                fragment.setArguments(bundle);
                fragment.show(((ServiceSpecificActivity)context).getSupportFragmentManager(), TAG);
            }else{

                Log.i("ispeha", "i'm in 7, kisi mai nai");
            }

        }else{
            logPackageBottomSheetOpenEvent();
            logPackageBottomSheetOpenFireBaseEvent();
            if(list.get(pos).getDealType().equalsIgnoreCase("combo")){

//                Slab slab= null;
//
//                if(slabs!=null){
//
//                    for(int i = 0; i< slabs.getSlabs().size(); i++){
//
//                        if (slabs.getSlabs().get(i).getSlabId().equalsIgnoreCase(list.get(pos).getSlabId())){
//
//                            slab = slabs.getSlabs().get(i);
//                        }
//                    }
//                }






                ComboBottomSheet bottomSheet= new ComboBottomSheet();
                Bundle bundle= new Bundle();
                bundle.putString("package", new Gson().toJson(list.get(pos), Service.class));
//                bundle.putString("slab", new Gson().toJson(slab, Slab.class));
                bottomSheet.setArguments(bundle);
                bottomSheet.show((((ServiceSpecificActivity)context)).getSupportFragmentManager(), "package_combo");

            }else if(list.get(pos).getDealType().equalsIgnoreCase("newCombo")){

//                Slab slab= null;
//                for(int i = 0; i< slabs.getSlabs().size(); i++){
//
//                    if (slabs.getSlabs().get(i).getSlabId().equalsIgnoreCase(list.get(pos).getSlabId())){
//                        slab = slabs.getSlabs().get(i);
//                    }
//                }

                try{

                    NewComboBottomSheet bottomSheet= new NewComboBottomSheet();
                    Bundle bundle= new Bundle();
                    bundle.putString("deal_id", list.get(pos).getDealId());
                    bundle.putString("parlor_id", user_cart.getParlorId());
//                bundle.putString("slab", new Gson().toJson(slab, Slab.class));
                    bundle.putString("description", list.get(pos).getDescription());
                    bundle.putString("short_description", list.get(pos).getShortDescription());
                    bundle.putString("service_name", list.get(pos).getName());
                    bundle.putInt("menu_price", list.get(pos).getMenuPrice());
                    bottomSheet.setArguments(bundle);
                    bottomSheet.show((((ServiceSpecificActivity)context)).getSupportFragmentManager(), "package_new_combo");
                }catch (Exception e){

                }

            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAddEvent(UserCart cart) {

        String deal_id= cart.getDeal_id()==null?"":cart.getDeal_id();
        String service_id= cart.getService_id()==null?"":cart.getService_id();
        Log.i("quantities", "value: "+ deal_id+ " -- "+service_id);
        int quantity= 0;

        for(int j=0; j<list.size();j++){

            /*if(list.get(j).getParlorDealId()!=null &&
                    list.get(j).getParlorDealId().equalsIgnoreCase(deal_id)){

                Log.i("makemytrip", "in the if mai deal: "+list.get(j).getParlorDealId()+ " 1-- "+
                        list.get(j).getDealId()+ " 2-- "+list.get(j).getServiceId()+  " ----- "+ deal_id);

                for(int i=0;i<cart.getServicesList().size();i++){

                    if(cart.getServicesList().get(i).getService_deal_id()
                            .equalsIgnoreCase(deal_id)){

                        quantity+=cart.getServicesList().get(i).getQuantity();
                        Log.i("lodatrip", "int the deal mai: "+ cart.getServicesList().get(i).getQuantity()+ " "+
                                quantity+ " "+ cart.getServicesList().get(i).getService_id());
                    }
                }

                list.get(j).setQuantity(quantity);

            }else */if(list.get(j).getServiceId()!=null &&
                    list.get(j).getServiceId().equalsIgnoreCase(service_id)){

                Log.i("makemytrip", "in the if mai service: "+list.get(j).getParlorDealId()+ " 1-- "+
                        list.get(j).getDealId()+ " 2-- "+list.get(j).getServiceId()+ " ------- "+ service_id);

                for(int i=0;i<cart.getServicesList().size();i++){

                    Log.i("kyabachodi", "vlaue: "+ cart.getServicesList().get(i).getService_id());
                    if(cart.getServicesList().get(i).getService_id().equalsIgnoreCase(service_id)){
                        Log.i("quanitykimaki", "quantity: "+ quantity);
                        quantity+=cart.getServicesList().get(i).getQuantity();
                        Log.i("lodatrip", "int the service mai: "+ cart.getServicesList().get(i).getQuantity()+ " "+
                                quantity+ " "+ cart.getServicesList().get(i).getService_id());
                    }
                }
                Log.i("quanitykimaki", "quantity---------->: "+ quantity);
                list.get(j).setQuantity(quantity);
            }
        }

        notifyDataSetChanged();
    }

    public void logAddedToCartEvent (String contentType, String currency, double price) {
        Log.e("prefine","add  type"+contentType+ " price"+price);

        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        logger.logEvent(AppEventsConstants.EVENT_NAME_ADDED_TO_CART, price, params);
    }

    public void logViewedContentEvent (String contentType, String contentId, String currency, double price) {
        Log.e("prefine","content view  type"+contentType+ " price"+price);

        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, contentId);
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        logger.logEvent(AppEventsConstants.EVENT_NAME_VIEWED_CONTENT, price, params);
    }


    public void logSeviceSharedEvent (String serviceName) {
        Bundle params = new Bundle();
        params.putString(AppConstant.ServiceName, serviceName);
        logger.logEvent(AppConstant.SeviceShared, params);
    }

    public void logPackageBottomSheetOpenEvent () {
        Log.e("PackageBottomSheetOpen","fine");
        logger.logEvent(AppConstant.PackageBottomSheetOpen);
    }

    public void logPackageBottomSheetOpenFireBaseEvent () {
        Log.e("PackagSheetOpenfire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.PackageBottomSheetOpen,bundle);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logEditServicePageOpenEvent () {
        Log.e("EditServicePageOpen","fine");

        logger.logEvent(AppConstant.EditServicePageOpen);
    }

    public void logEditServicePageOpenFireBaseEvent () {
        Log.e("EditServfirePageOpen","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.EditServicePageOpen,bundle);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logShowDetailPackageEvent () {
        Log.e("ShowDetailPackage","fine");

        logger.logEvent(AppConstant.ShowDetailPackage);
    }
    public void logShowDetailPackageFireBaseEvent () {
        Log.e("ShowDetailPackage","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.ShowDetailPackage,bundle);
    }
    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logServiceSelectedEvent (String name) {
        Log.e("singleServiceSelect","fine");
        Bundle params = new Bundle();
        params.putString(AppConstant.ServiceName, name);
        logger.logEvent(AppConstant.ServiceSelected, params);
    }

    public void logServiceSelectedFireBaseEvent (String name) {
        Log.e("singleServiceSelect","fine");
        Bundle params = new Bundle();
        params.putString(AppConstant.ServiceName, name);
        mFirebaseAnalytics.logEvent(AppConstant.ServiceSelected, params);
    }



    private void addService(Service service){

        AddServiceEvent event= new AddServiceEvent();

        event.setName(service.getName());

        String service_deal_id= service.getDealId()==null? service.getServiceId():service.getDealId();
        event.setService_deal_id(service_deal_id);       //service ya deal id hogi isme

        event.setService_id(service.getServiceId());            //quanity ke liye

        event.setType(service.getDealId()==null?"service":service.getDealType());

        event.setService_code(service.getServiceCode());
        event.setPrice_id(service.getPrices().get(0).getPriceId());

        if(event.getType().equalsIgnoreCase("chooseOnePer")){

            int price= service.getMenuPrice()- (int) (((double)service.getDealPrice()/100)*service.getMenuPrice());
            event.setPrice(price);
        }else
            event.setPrice(service.getDealId()==null?service.getPrices().get(0).getPrice():service.getDealPrice());


        event.setMenu_price(service.getDealId()==null?0:service.getMenuPrice());

        event.setDescription(service.getDescription()==null?
                "":service.getDescription());

        String primary_key= ""+service.getServiceCode()+service_deal_id;
        event.setPrimary_key(primary_key);

        Log.i("promary_key", "value to be sent from adapter: " + event.getName()+ "  "+ event.getService_code()
                + "  "+event.getService_deal_id()+ "  "+event.getPrice_id()
                + "  "+ event.getType()+ "  "+event.getPrice()+ "  "+event.getMenu_price()+ "  "+
                event.getDescription()+ "  "+ event.getPrimary_key()+ "  "+event.getService_id());

        EventBus.getDefault().post(event);
    }

    private int setDiscountData(int totalPrice){
        int pos=0;
        if (BeuSalonsSharedPrefrence.getDiscount(context)!=null) {

            for (int i = 0; i < BeuSalonsSharedPrefrence.getDiscount(context).size(); i++) {

                if (totalPrice>=BeuSalonsSharedPrefrence.getDiscount(context).get(0).getExcludeNoOfServiceAmount()
                        && totalPrice<BeuSalonsSharedPrefrence.getDiscount(context).get(0).getMax()){
                    pos=i;
                }
                else {
                    pos=i;
                }

            }

        }

        return   pos;
    }

    private void singleBrandProduct(Service service){

        //yaha wale pe brand name aur product name explicitly add nai kara hai, service activity pe kiya hai
        AddServiceEvent event= new AddServiceEvent();

        event.setName(service.getName());

        event.setService_id(service.getServiceId());            //yeh service id hai quantity ke liye

        event.setService_code(service.getServiceCode());
        event.setPrice_id(service.getPrices().get(0).getPriceId());

        String description= "";

        Boolean has_product= false;
        if( service.getPrices()!=null &&
                service.getPrices().size()>0 &&
                service.getPrices().get(0).getBrand()!=null &&
                service.getPrices().get(0).getBrand().getBrands().size()>0) {


            has_product = false;
            if (service.getPrices().get(0).getBrand().getBrands().get(0).getProducts() != null &&
                    service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().size() > 0) {

                has_product = true;
            }

        }

        String brand_id="", product_id="";
        String brand_name="", product_name="";

        if(has_product){

            description= service.getPrices().get(0).getBrand().getBrands().get(0).getName()+ ", "+
                    service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().get(0).getName();

            brand_id= service.getPrices().get(0).getBrand().getBrands().get(0).getBrandId();
            product_id= service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().get(0).getProductId();

            event.setBrand_id(brand_id);
            event.setProduct_id(product_id);


            if(service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().get(0).getDealRatio()!=null){

                event.setPrice((int)(service.getPrices().get(0).getBrand().getBrands().get(0)
                        .getProducts().get(0).getDealRatio() * service.getDealPrice()));
                event.setMenu_price(service.getMenuPrice());

                event.setService_deal_id(service.getDealId());
                event.setType(service.getDealType());
            }else{

                event.setPrice((int)(service.getPrices().get(0).getBrand().getBrands().get(0)
                        .getProducts().get(0).getRatio() *
                        service.getPrices().get(0).getPrice()));

                event.setService_deal_id(service.getServiceId());
                event.setType("service");
            }
        }else {

            description= service.getPrices().get(0).getBrand().getBrands().get(0).getName();
            brand_id= service.getPrices().get(0).getBrand().getBrands().get(0).getBrandId();

            event.setBrand_id(brand_id);

            if(service.getPrices().get(0).getBrand().getBrands().get(0).getDealRatio()!=null){

                event.setPrice((int)
                        (service.getPrices().get(0).getBrand().getBrands()
                                .get(0).getDealRatio()*service.getDealPrice()));
                event.setMenu_price(service.getMenuPrice());

                event.setService_deal_id(service.getDealId());
                event.setType(service.getDealType());

            }else{

                event.setPrice((int)
                        service.getPrices().get(0).getBrand().getBrands().get(0).getRatio()
                        *service.getPrices().get(0).getPrice());

                event.setService_deal_id(service.getServiceId());
                event.setType("service");
            }
        }
        Log.i("singleservice", "value: "+ service.getServiceCode()+ " "+ event.getService_deal_id()+ " "+
                brand_id+ " "+ product_id);
        event.setPrimary_key(""+service.getServiceCode()+event.getService_deal_id()+brand_id+product_id);
        event.setDescription(description);

        EventBus.getDefault().post(event);
    }



    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView  txt_validity_, txt_discount_;
        private TextView txt_name, txt_save_per, txt_menu_price, txt_price,txt_quantity, txt_description,txt_package_header;
        private LinearLayout linear_add, linear_add_, linear_remove,linearLayoutTop,
                linear_description,linearlayoutForPrice, linear_header_show_details,linear_add_remove,linear_layout_show_detail,
                linear_validity_,ll_package_header;
        private ImageView img_,img_share,imgshowdetails;

        public ViewHolder(View itemView) {
            super(itemView);

            txt_package_header= (TextView) itemView.findViewById(R.id.txt_package_header);
            ll_package_header= (LinearLayout)itemView.findViewById(R.id.ll_package_header);
            // title_icon=(TextView)itemView.findViewById(R.id.title_icon);
            txt_description= (TextView)itemView.findViewById(R.id.txt_description);
            txt_quantity= (TextView)itemView.findViewById(R.id.txt_quantity);
            txt_name= (TextView)itemView.findViewById(R.id.txt_name);
            txt_save_per= (TextView)itemView.findViewById(R.id.txt_save_per);
            txt_menu_price= (TextView)itemView.findViewById(R.id.txt_menu_price);
            txt_price= (TextView)itemView.findViewById(R.id.txt_price);
            linear_add= (LinearLayout) itemView.findViewById(R.id.linear_add);      //add button
            linear_add_= (LinearLayout)itemView.findViewById(R.id.linear_add_);             //add items
            linear_remove= (LinearLayout)itemView.findViewById(R.id.linear_remove);         //remove items
            img_= (ImageView)itemView.findViewById(R.id.img_);
            linear_description= (LinearLayout)itemView.findViewById(R.id.linear_description);
            linearLayoutTop=(LinearLayout)itemView.findViewById(R.id.linearLayoutTop);
            linearlayoutForPrice=(LinearLayout)itemView.findViewById(R.id.linearlayout_for_price);
            linear_add_remove= (LinearLayout)itemView.findViewById(R.id.linear_add_remove);
            linear_layout_show_detail=(LinearLayout)itemView.findViewById(R.id.linear_layout_show_detail);
            linear_header_show_details=(LinearLayout)itemView.findViewById(R.id.linear_header_show_details);
            linear_validity_= (LinearLayout)itemView.findViewById(R.id.linear_validity_);
            txt_validity_= (TextView)itemView.findViewById(R.id.txt_validity_);
            txt_discount_= (TextView)itemView.findViewById(R.id.txt_discount_);
            img_share=(ImageView)itemView.findViewById(R.id.img_share);
            imgshowdetails=(ImageView)itemView.findViewById(R.id.imgshowdetails);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.fragment_services_specific_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }



}
