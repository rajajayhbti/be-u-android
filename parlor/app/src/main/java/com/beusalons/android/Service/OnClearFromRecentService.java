package com.beusalons.android.Service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.beusalons.android.Model.AppCloseResponse;
import com.beusalons.android.Model.Subscription_post;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Ajay on 2/28/2018.
 */

public class OnClearFromRecentService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("ClearFromRecentService", "Service Started");
        return START_NOT_STICKY;
    }




    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("test", "Service Destroyed");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e("ClearFromRecentService", "END");
        //Code here
        killAppPost();
    }

    private void killAppPost(){

        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface=retrofit.create(ApiInterface.class);
        Subscription_post userCartPost=new Subscription_post();
        userCartPost.setUserId(BeuSalonsSharedPrefrence.getUserId());

        Call<AppCloseResponse> call=apiInterface.onAppClose(userCartPost);

        call.enqueue(new Callback<AppCloseResponse>() {
            @Override
            public void onResponse(Call<AppCloseResponse> call, Response<AppCloseResponse> response) {
                if (response.isSuccessful()){
                    stopSelf();

                }
            }

            @Override
            public void onFailure(Call<AppCloseResponse> call, Throwable t) {
                stopSelf();


            }
        });

    }
}
