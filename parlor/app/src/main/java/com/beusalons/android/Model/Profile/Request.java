package com.beusalons.android.Model.Profile;

/**
 * Created by myMachine on 10/26/2017.
 */

public class Request {

    private String userId;

    public Request(String artistId){
        this.userId= artistId;
    }

    public String getArtistId() {
        return userId;
    }

}
