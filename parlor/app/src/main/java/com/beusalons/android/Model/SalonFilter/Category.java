package com.beusalons.android.Model.SalonFilter;

/**
 * Created by myMachine on 22-Feb-18.
 */

public class Category{
    private int parlorType;
    private String text;
    private boolean isCheck;

    public Category(int parlorType, String text, boolean isCheck){
        this.parlorType= parlorType;
        this.text= text;
        this.isCheck= isCheck;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public int getParlorType() {
        return parlorType;
    }

    public void setParlorType(int parlorType) {
        this.parlorType = parlorType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}