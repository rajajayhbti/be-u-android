package com.beusalons.android.Model.ServiceDialog;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 4/14/2017.
 */

public class ServiceSelection{

    private String type;                    //service type
    private String service_name;
    private int service_code;                        //yaha service code save kar raha hoon kyunki api/parlors mai service code jayega
    private List<ServiceSelectionList> list= new ArrayList<>();


    public int getService_code() {
        return service_code;
    }

    public void setService_code(int service_code) {
        this.service_code = service_code;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<ServiceSelectionList> getList() {
        return list;
    }

    public void setList(List<ServiceSelectionList> list) {
        this.list = list;
    }
}
