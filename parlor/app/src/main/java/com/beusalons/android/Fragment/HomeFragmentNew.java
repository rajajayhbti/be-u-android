package com.beusalons.android.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.beusalons.android.Adapter.HomeScreen.FreeBeeAdapter;
import com.beusalons.android.Adapter.HomeScreen.NearBySalonsAdapter;
import com.beusalons.android.Adapter.HomeScreen.PopularDealsAdapter;
import com.beusalons.android.Adapter.HomeScreen.PopularSalonsAdapter;
import com.beusalons.android.CorporateLoginActivity;
import com.beusalons.android.Event.HomeAddressChangedEvent;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.MainActivity;
import com.beusalons.android.MemberShipCardAcitvity;
import com.beusalons.android.Model.FireBaseResponse;
import com.beusalons.android.Model.FirebaseModel.FireBaseIdPost;
import com.beusalons.android.Model.HomeFragmentModel.Carousel;
import com.beusalons.android.Model.HomeFragmentModel.DiscountRules;
import com.beusalons.android.Model.HomeFragmentModel.HomePageData;
import com.beusalons.android.Model.HomeFragmentModel.HomePost;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.ParlorListActivity;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.SalonPageActivity;
import com.beusalons.android.Task.MultipleServicesTask;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.bumptech.glide.Glide;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.share.ShareApi;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.iid.FirebaseInstanceId;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Ashish sharma on 02/03/2017.
 */

public class HomeFragmentNew extends Fragment
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    private final String CAROUSEL_OPEN_PARLOR= "parlors";
    private final String CAROUSEL_OPEN_DEALS= "deals";
    private final String CAROUSEL_OPEN_DEAL_PAGE= "dealPage";
    private final String CAROUSEL_OPEN_FREEBIES= "freebies";
    private final String CAROUSEL_OPEN_CORPORATE= "corporate";
    private final String CAROUSEL_OPEN_MEMBERSHIP= "membership";

    private static final String TAG = HomeFragmentNew.class.getSimpleName();

    private CoordinatorLayout coordinator_;
    private RecyclerView recycler_freebies, recycler_nearby,
            recycler_deals, recycler_popular;
    private LinearLayout linear_c;
    private HorizontalScrollView horizontal_;

    private ProgressBar progress_;
    private Button btn_retry;
    private ScrollView scroll_;
    AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;
    private  List<DiscountRules> discountRulesList=new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_homepage, container, false);

//        try {
//            FirebaseCrash.logcat(1, "", "Activity created");
//            Bundle bundle = new Bundle();
//            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, BeuSalonsSharedPrefrence.getUserId());
//            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, BeuSalonsSharedPrefrence.getUserName());
//            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

      /*  ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse("https://beusalons.com"))
                .build();
        ShareDialog shareDialog = new ShareDialog(getActivity());
      //  shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
       ShareApi.share(content, null);*/
        coordinator_= (CoordinatorLayout)view.findViewById(R.id.coordinator_);
        progress_= (ProgressBar)view.findViewById(R.id.progress_) ;
        btn_retry= (Button)view.findViewById(R.id.btn_retry);
        scroll_= (ScrollView)view.findViewById(R.id.scroll_);
        linear_c= (LinearLayout)view.findViewById(R.id.linear_c);
        horizontal_= (HorizontalScrollView)view.findViewById(R.id.horizontal_);

        recycler_freebies= (RecyclerView)view.findViewById(R.id.recycler_freebies);
        recycler_nearby= (RecyclerView)view.findViewById(R.id.recycler_nearby);
        recycler_deals= (RecyclerView)view.findViewById(R.id.recycler_deals);
        recycler_popular= (RecyclerView)view.findViewById(R.id.recycler_popular);


        logger = AppEventsLogger.newLogger(getActivity());
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());

        //firebase enable
        FirebaseCrash.logcat(1, "", "Activity created");
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, BeuSalonsSharedPrefrence.getUserId());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, BeuSalonsSharedPrefrence.getUserName());
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);



        LinearLayout linear_= (LinearLayout)view.findViewById(R.id.linear_);
        linear_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logSearchSalonButtonFirbaseEvent();
                logSearchSalonButtonEvent();
                openSalonList();
            }
        });

        LinearLayout linear_more_1= (LinearLayout)view.findViewById(R.id.linear_more_1);
        linear_more_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity.isFreebieHome = true;
                AHBottomNavigation bottomNavigation= MainActivity.getBottomNav();
                bottomNavigation.setCurrentItem(2);
            }
        });

        LinearLayout linear_more_2= (LinearLayout)view.findViewById(R.id.linear_more_2);
        linear_more_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSalonList();
            }
        });

        LinearLayout linear_more_3= (LinearLayout)view.findViewById(R.id.linear_more_3);
        linear_more_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSalonList();
            }
        });

        LinearLayout linear_more_4= (LinearLayout)view.findViewById(R.id.linear_more_4);
        linear_more_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSalonList();
            }
        });

        btn_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fetchData();
            }
        });
        btn_retry.performClick();




        return view;
    }

    private void openSalonList(){

        Intent intent= new Intent(getActivity(), ParlorListActivity.class);
        intent.putExtra("isService",true);
        startActivity(intent);
    }

    private void fetchData(){
        Log.i("homedata", "I'm in the runnable");

        progress_.setVisibility(View.VISIBLE);
        scroll_.setVisibility(View.GONE);
        btn_retry.setVisibility(View.GONE);

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        HomePost homePost = new HomePost();

        if (BeuSalonsSharedPrefrence.getLatitude()!=null&& BeuSalonsSharedPrefrence.getLongitude()!=null){
            homePost.setLatitude(Double.valueOf(BeuSalonsSharedPrefrence.getLatitude()));
            homePost.setLongitude(Double.valueOf(BeuSalonsSharedPrefrence.getLongitude()));
            homePost.setUserId(BeuSalonsSharedPrefrence.getUserId());
            homePost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        }else{
            homePost.setLatitude(28.5603);
            homePost.setLongitude(77.2913);

//            homePost.setVersionAndroid();
            homePost.setUserId(BeuSalonsSharedPrefrence.getUserId());
            homePost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        }

        String version="";
        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        homePost.setVersionAndroid(version);
//        Toast.makeText(getContext(),""+version,Toast.LENGTH_LONG).show();

        Call<HomePageData> call = apiInterface.getHomeScreenData(homePost);
        call.enqueue(new Callback<HomePageData>() {
            @Override
            public void onResponse(Call<HomePageData> call, final Response<HomePageData> response) {


                if (response.isSuccessful()) {

                    if (response.body().getSuccess()) {

                        Log.e(TAG, "i'm in on response New Home ");

                        linear_c.removeAllViews();
                        if(response.body().getData().getCarousels()!=null &&
                                response.body().getData().getCarousels().size()>0){

                            int size= response.body().getData().getCarousels().size();
                            for(int i=0; i<size;i++){
                                final int pos= i;
                                if(getActivity()!=null){
                                    CardView cardView= (CardView) LayoutInflater.from(getActivity()).inflate(R.layout.img_carousel, null, false);
                                    final ImageView img_= (ImageView)cardView.findViewById(R.id.img_);
                                    Glide.with(getActivity())
                                            .load(response.body().getData().getCarousels().get(pos).getImageUrl())
                                            .into(img_);


                                    img_.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            logHomePageBannerSliderEvent(response.body().getData().getCarousels().get(pos).getAction());
                                            logHomePageBannerSliderFireBase(response.body().getData().getCarousels().get(pos).getAction());
                                            if(response.body().getData().getCarousels().get(pos).getAction()
                                                    .equalsIgnoreCase(CAROUSEL_OPEN_PARLOR)){

                                                if(getActivity()!=null){

                                                    Intent intent= new Intent(getActivity(), ParlorListActivity.class);
                                                    intent.putExtra("isDeal", true);
                                                    startActivity(intent);
                                                }

                                            }else if(response.body().getData().getCarousels().get(pos).getAction()
                                                    .equalsIgnoreCase(CAROUSEL_OPEN_DEALS)){


                                                addDeal(response.body().getData().getCarousels().get(pos));

                                            }else if(response.body().getData().getCarousels().get(pos).getAction()
                                                    .equalsIgnoreCase(CAROUSEL_OPEN_FREEBIES)){

                                                if(getActivity()!=null){
                                                    MainActivity.isFreebieHome = true;
                                                    AHBottomNavigation bottomNavigation= MainActivity.getBottomNav();
                                                    bottomNavigation.setCurrentItem(2);
                                                }

                                            }else if(response.body().getData().getCarousels().get(pos).getAction()
                                                    .equalsIgnoreCase(CAROUSEL_OPEN_CORPORATE)){

                                                startActivity(new Intent(getActivity(), CorporateLoginActivity.class));

                                            }else if(response.body().getData().getCarousels().get(pos).getAction()
                                                    .equalsIgnoreCase(CAROUSEL_OPEN_MEMBERSHIP)){

                                                Intent in=new Intent(getActivity(), MemberShipCardAcitvity.class);
                                                in.putExtra("from_home", true);
                                                startActivity(in);

                                            }else if(response.body().getData().getCarousels().get(pos).getAction()
                                                    .equalsIgnoreCase(CAROUSEL_OPEN_DEAL_PAGE)){

                                                if(getActivity()!=null){
                                                    MainActivity.isFreebieHome = true;
                                                    AHBottomNavigation bottomNavigation= MainActivity.getBottomNav();
                                                    bottomNavigation.setCurrentItem(1);
                                                }
                                            }


                                        }
                                    });


                                    linear_c.addView(cardView);
                                }
                            }
                        }

                        if(response.body().getData().getFreebies()!=null &&
                                response.body().getData().getFreebies().size()>0){
                            LinearLayoutManager layout= new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                            recycler_freebies.setLayoutManager(layout);
                            FreeBeeAdapter freeBeeAdapter= new FreeBeeAdapter(getActivity(), response.body().getData().getFreebies());
                            recycler_freebies.setAdapter(freeBeeAdapter);
                        }

                        if(response.body().getData().getNearBySalons()!=null &&
                                response.body().getData().getNearBySalons().size()>0){

                            LinearLayoutManager layout= new
                                    LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                            recycler_nearby.setLayoutManager(layout);
                            NearBySalonsAdapter nearBySalons= new NearBySalonsAdapter(getActivity(), response.body().getData().getNearBySalons());
                            recycler_nearby.setAdapter(nearBySalons);
                        }

                        if(response.body().getData().getDeals()!=null &&
                                response.body().getData().getDeals().size()>0){

                            LinearLayoutManager layout= new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                            recycler_deals.setLayoutManager(layout);

                            PopularDealsAdapter popularDealsAdapter= new PopularDealsAdapter(getActivity(),
                                    response.body().getData().getDeals());
                            recycler_deals.setAdapter(popularDealsAdapter);
                        }

                        if(response.body().getData().getPopularSalons()!=null &&
                                response.body().getData().getPopularSalons().size()>0){

                            LinearLayoutManager layout= new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                            recycler_popular.setLayoutManager(layout);

                            PopularSalonsAdapter popularSalonsAdapter= new PopularSalonsAdapter(getActivity(),
                                    response.body().getData().getPopularSalons());
                            recycler_popular.setAdapter(popularSalonsAdapter);
                        }


                        if (response.body().getData().getDiscountRules()!=null &&
                                response.body().getData().getDiscountRules().size()>0){
                            if (discountRulesList.size()>0){
                                discountRulesList.clear();
                            }
                            for (int i=0;i<response.body().getData().getDiscountRules().size();i++){
                                discountRulesList.add(response.body().getData().getDiscountRules().get(i));
                            }
                            BeuSalonsSharedPrefrence.clearDiscountData();
                            BeuSalonsSharedPrefrence.saveDiscount(getActivity(),discountRulesList);

                        }

                        scroll_.setVisibility(View.VISIBLE);
                        progress_.setVisibility(View.GONE);
                        btn_retry.setVisibility(View.GONE);

                    } else {
                        Log.e(TAG, "i'm in on response New Home else ");
                        showSnackbar("Server Not Responding");
                        scroll_.setVisibility(View.GONE);
                        progress_.setVisibility(View.GONE);
                        btn_retry.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e(TAG, "i'm in on response New Home else else");
                    showSnackbar("Server Not Responding");
                    scroll_.setVisibility(View.GONE);
                    progress_.setVisibility(View.GONE);
                    btn_retry.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<HomePageData> call, Throwable t) {

                Log.e(TAG, "i'm in on failure New Home" + t.getMessage() + t.getCause());
                showSnackbar("Server Not Responding");
                scroll_.setVisibility(View.GONE);
                progress_.setVisibility(View.GONE);
                btn_retry.setVisibility(View.VISIBLE);
            }
        });
    }

    private void addDeal(Carousel data){

        UserCart user_cart= new UserCart();
        user_cart.setCartType(AppConstant.DEAL_TYPE);

        List<UserServices> list= new ArrayList<>();
        for(int i=0;i<data.getSelectedDeals().size();i++){

            UserServices service= new UserServices();
            service.setName(data.getSelectedDeals().get(i).getName());

            int service_code= data.getSelectedDeals().get(i).getServiceCodes()!=null &&
                    data.getSelectedDeals().get(i).getServiceCodes().size()>0?
                    data.getSelectedDeals().get(i).getServiceCodes().get(0):0;
            service.setService_code(service_code);
            service.setPrice_id(service_code);

            service.setService_id("");
            service.setService_deal_id("");

            service.setDealId(data.getSelectedDeals().get(i).getDealId());
            service.setType(data.getSelectedDeals().get(i).getDealType());

            service.setBrand_id(data.getSelectedDeals().get(i).getBrandId());
            service.setProduct_id(data.getSelectedDeals().get(i).getProductId());

            String des= data.getSelectedDeals().get(i).getDescription()==null?"":
                    data.getSelectedDeals().get(i).getDescription();
            service.setDescription(des);

            service.setPrimary_key(service_code+data.getSelectedDeals().get(i).getBrandId()+
                    data.getSelectedDeals().get(i).getProductId());

            list.add(service);
        }

        if(getActivity()!=null){

            new Thread(new MultipleServicesTask(getActivity(), user_cart, list)).start();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    Intent intent= new Intent(getActivity(), ParlorListActivity.class);
                    intent.putExtra("isDeal", true);
                    getActivity().startActivity(intent);
                }
            }, 400);
            Toast.makeText(getContext(), "Services Added To Cart!", Toast.LENGTH_SHORT).show();
        }

    }





    private void showSnackbar(String txt){
        Snackbar snackbar = Snackbar.make( coordinator_,
                txt, 4500);

        //setting the snackbar action button text size
        View view = snackbar.getView();
        TextView txt_text = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txt_text.setTextSize(13);
        txt_text.setAllCaps(false);
        snackbar.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(HomeAddressChangedEvent event) {

        fetchData();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(BeuSalonsSharedPrefrence.ADDRESS_LOCALTY))
            fetchData();
    }




    public void logHomePageBannerSliderEvent (String bannerName) {
        Bundle params = new Bundle();
        Log.e("HomePageBannerSlider","fine"+bannerName);

        params.putString(AppConstant.BannerName, bannerName);
        logger.logEvent(AppConstant.HomePageBannerSlider, params);
    }

    public void logHomePageBannerSliderFireBase (String bannerName) {
        Log.e("HomePageBannerfire","fine"+bannerName);
        Bundle params = new Bundle();
        params.putString(AppConstant.BannerName, bannerName);
        logger.logEvent(AppConstant.HomePageBannerSlider, params);
    }
    public void logSearchSalonButtonEvent () {
        Log.e("search salon","fine");
        logger.logEvent(AppConstant.SearchSalonButton);
    }


    public void logSearchSalonButtonFirbaseEvent(){
        Log.e("search fire salon","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.SearchSalonButton,bundle);
    }
}
