package com.beusalons.android.Model.SalonReview;

/**
 * Created by Ajay on 12/4/2017.
 */

public class Helpfull_post {
  private String   appId;
    private String   userId;
    private boolean  isHelpful;
    private boolean   notHelpful;
    private String accessToken;


    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isHelpful() {
        return isHelpful;
    }

    public void setHelpful(boolean helpful) {
        isHelpful = helpful;
    }

    public boolean isNotHelpful() {
        return notHelpful;
    }

    public void setNotHelpful(boolean notHelpful) {
        this.notHelpful = notHelpful;
    }
}
