package com.beusalons.android.Model.ArtistProfile;

import java.util.List;

/**
 * Created by Ajay on 1/31/2018.
 */

public class ArtistProject_ {

    private String _id;
    private String artistId;
    private String artistName;
    private String coverImage;
    private String postTitle;
    private Double postLatitude;
    private Double postLongitude;
    private String collectionName;
    private Integer __v;
    private String artistPic;
    private List<CreativeField> creativeFields = null;
    private ArtistCollec_ collec;
    private List<ArtistTag_> tags = null;
    private String createdAt;
    private Boolean followedByMe;
    private Integer postLikes;
    private List<String> images = null;
    private String totalLikes;

    public String getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        this.totalLikes = totalLikes;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public Double getPostLatitude() {
        return postLatitude;
    }

    public void setPostLatitude(Double postLatitude) {
        this.postLatitude = postLatitude;
    }

    public Double getPostLongitude() {
        return postLongitude;
    }

    public void setPostLongitude(Double postLongitude) {
        this.postLongitude = postLongitude;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public Integer get__v() {
        return __v;
    }

    public void set__v(Integer __v) {
        this.__v = __v;
    }

    public String getArtistPic() {
        return artistPic;
    }

    public void setArtistPic(String artistPic) {
        this.artistPic = artistPic;
    }

    public List<CreativeField> getCreativeFields() {
        return creativeFields;
    }

    public void setCreativeFields(List<CreativeField> creativeFields) {
        this.creativeFields = creativeFields;
    }

    public ArtistCollec_ getCollec() {
        return collec;
    }

    public void setCollec(ArtistCollec_ collec) {
        this.collec = collec;
    }

    public List<ArtistTag_> getTags() {
        return tags;
    }

    public void setTags(List<ArtistTag_> tags) {
        this.tags = tags;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getFollowedByMe() {
        return followedByMe;
    }

    public void setFollowedByMe(Boolean followedByMe) {
        this.followedByMe = followedByMe;
    }

    public Integer getPostLikes() {
        return postLikes;
    }

    public void setPostLikes(Integer postLikes) {
        this.postLikes = postLikes;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
