package com.beusalons.android.Model.newServiceDeals.ServiceByDepartment;

import java.util.List;

/**
 * Created by myMachine on 6/9/2017.
 */

public class ProductSelector {

    private String title;
    private String name;
    private String productId;
    private List<ServiceSelector> services = null;
    private int price;
    private int menuPrice;

    private boolean isCheck;                //check ya uncheck :D

    public int getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(int menuPrice) {
        this.menuPrice = menuPrice;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ServiceSelector> getServices() {
        return services;
    }

    public void setServices(List<ServiceSelector> services) {
        this.services = services;
    }
}
