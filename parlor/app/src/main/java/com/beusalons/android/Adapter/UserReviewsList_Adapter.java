package com.beusalons.android.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.beusalons.android.Model.Reviews.UserReviewsData;
import com.beusalons.android.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 11/18/2016.
 */

public class UserReviewsList_Adapter extends RecyclerView.Adapter<UserReviewsList_Adapter.MyReviewViewHolder> {


    Context context;
    List<UserReviewsData> details= new ArrayList<>();
    private Boolean isCheck= true;

    public UserReviewsList_Adapter(List<UserReviewsData> details, Context context){
        this.details= details;
        this.context= context;
    }


    public static class MyReviewViewHolder extends RecyclerView.ViewHolder{

        private TextView salonName, salonAddress, reviewDate, rating, review;
        private CardView cardView;
        private ImageView img_parlor_image;

        public MyReviewViewHolder(View view) {
            super(view);

            salonName= (TextView)view.findViewById(R.id.txt_myReviews_salonName);
            salonAddress= (TextView)view.findViewById(R.id.txt_myReviews_salonAddress);
            reviewDate= (TextView)view.findViewById(R.id.txt_myReviews_reviewDate);
            rating= (TextView)view.findViewById(R.id.txt_myReviews_rating);
            review= (TextView)view.findViewById(R.id.txt_user_review_review);
            cardView= (CardView)view.findViewById(R.id.card_user_reviews);
            img_parlor_image= (ImageView)view.findViewById(R.id.img_myReviews_salonImage);
        }
    }

    @Override
    public MyReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        context= parent.getContext();
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_user_review_cardview, parent, false);
        return new MyReviewViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final MyReviewViewHolder holder, int position) {

        UserReviewsData data= details.get(position);


        holder.salonName.setText(data.getName());
        holder.salonAddress.setText(data.getAddress());
        holder.reviewDate.setText(data.getReviewedTime());
        holder.rating.setText(String.valueOf(data.getRatingByUser()));
//        holder.review.setText("Most organizations recognize that being a successful, data-driven company requires skilled developers and analysts. Fewer grasp how to use data to tell a meaningful story that resonates both intellectually and emotionally with an audience. Marketers are responsible for this story; as such, they're often the bridge between the data and those who need to learn something from it, or make decisions based on its analysis. As marketers, we can tailor the story to the audience and effectively use data visualization to complement our narrative. We know that data is powerful. But with a good story, it's unforgettable.");
        holder.review.setText(data.getReviewContent());
//        holder.cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if(isCheck){
//                    holder.review.setMaxLines(10);
//                    isCheck= false;
//                }else{
//                    holder.review.setMaxLines(2);
//                    isCheck= true;
//                }
//
//
//            }
//        });

        holder.img_parlor_image.setImageResource(R.drawable.master_salon);
    }

    @Override
    public int getItemCount() {
        return details.size();
    }

}
