package com.beusalons.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beusalons.android.Adapter.DealsDetailsParlorListAdapter;
import com.beusalons.android.Adapter.ParlorsListAdapter;
import com.beusalons.android.Event.FiltersEvent;
import com.beusalons.android.Fragment.FilterFragment;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.DealsSalonList.DealSalonListResponse;
import com.beusalons.android.Model.DealsSalonList.Parlor;
import com.beusalons.android.Model.DealsServices.DealDetail.Deal_Detail_Post;
import com.beusalons.android.Model.DealsServices.DealDetail.PostDealDetail;
import com.beusalons.android.Model.DealsServices.DealDetail.ServicesList;
import com.beusalons.android.Model.ParlorModel;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ParlorResponseModel;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Service.LocationUpdateService;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.PaginationScrollListener;
import com.beusalons.android.Utility.Utility;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.snappydb.DB;
import com.snappydb.DBFactory;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.beusalons.android.Helper.AppConstant.PLACE_BOUNDS_DELHI;

public class ParlorListActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    public static final String LOCATION_ACTION="com.beusalons.android.ParlorListActivity";
    final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 100;
    private GoogleApiClient googleApiClient;

    private ParlorsListAdapter mGoogleCardsAdapter;
    private Deal_Detail_Post deal_post= null;

    private static final String FILTERS="filters";
    private UserCart savedCart = null;
    //yeh filters ka stuff hai
    private boolean isMale= false, isFemale= false, isUnisex= false;
    private boolean isPopularity= false, isPriceLow= false, isPriceHigh= false, isNearest= true;
    private boolean isRating_1= false, isRating_2= false, isRating_3= false, isRating_4= false;
    private boolean isPrice_red= false, isPrice_blue= false, isPrice_green=false, isPrice_all=false;
    private String filters_rating= null, filters_price= null, filters_gender= null, filters_sort= "0";

    private boolean isFilter= false;

    //navigation ka setting boolean se
    private boolean isService= false;
    private boolean isDeal= false;
    private boolean isFavouriteSalon= false;
    private static final int GPS_PERMISSION = 100;                  //marshmallow permission

    private List<ParlorModel> salon_list= new ArrayList<>();
    private List<Parlor> parlors= new ArrayList<>();

    private LinearLayout linear_;
    private RecyclerView recycler_;
    private TextView txt_location, txt_empty, txt_filter,txtView_mail_id,txt_ok;
    private HorizontalScrollView horizontal_location;

    private int PAGE_SIZE = 1;
    private boolean isLoading= false;

    private ProgressBar progress_;
    private Button btn_retry;

    private CardView card_all;
    private CardView card_premium;
    private CardView card_standard;
    private CardView card_budget;
    private AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;

    private IntentFilter locationFilter;
    private LocationReciver locationReciver;

    private ImageView img_no_salon,img_no_salon_deals;

    private  View view;
    private  String noSalonCity= "Seems like we don\'t serve in your city yet!\n write to us about your city at";
    private String  noSalonDeals="Seems like you have selected services \n from multiple salon categories. Please \n " +
            "select from one salon category";
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parlor_list);

        linear_= (LinearLayout)findViewById(R.id.linear_);      //filters ki visibility
        linear_.setVisibility(View.GONE);
        view=(View)findViewById(R.id.include_no_salon);
        recycler_= (RecyclerView)findViewById(R.id.recycler_);
        txt_location= (TextView)findViewById(R.id.txt_location);
        txt_empty= (TextView)findViewById(R.id.txt_empty);
        img_no_salon=findViewById(R.id.img_no_salon);
        img_no_salon_deals=findViewById(R.id.img_no_salon_deal);
        txt_ok=(TextView)findViewById(R.id.txt_ok) ;
        horizontal_location= (HorizontalScrollView)findViewById(R.id.horizontal_location);

        txtView_mail_id=(TextView)findViewById(R.id.txtView_mail_id);

        progress_= (ProgressBar)findViewById(R.id.progress_);
        btn_retry= (Button)findViewById(R.id.btn_retry);
        progress_.setVisibility(View.VISIBLE);
        btn_retry.setVisibility(View.GONE);
        logger = AppEventsLogger.newLogger(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        googleApiClient = new GoogleApiClient
                .Builder( this )
                .enableAutoManage( this, 0, this )
                .addApi( Places.GEO_DATA_API )
                .addApi( Places.PLACE_DETECTION_API )
                .addConnectionCallbacks( this )
                .addOnConnectionFailedListener( this )
                .build();

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null && (bundle.containsKey("isService") ||
                bundle.containsKey("isDeal") ||
                bundle.containsKey("isFavouriteSalon"))){

            isService= bundle.getBoolean("isService", false);
            isDeal= bundle.getBoolean("isDeal", false);
            isFavouriteSalon= bundle.getBoolean("isFavouriteSalon", false);
        }else
            finish();





        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler_.setLayoutManager(mLayoutManager);
        recycler_.addOnScrollListener(new PaginationScrollListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){


                    logSalonScrollEvent();
                    logSalonScrollFireBaseEvent();
                    Log.i("loadinghua", "im here now");
                    isLoading= true;

                    mGoogleCardsAdapter.addProgress();
                    PAGE_SIZE++;

                    fetchServiceSalons();
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });

        locationFilter=new IntentFilter(LOCATION_ACTION);
        locationReciver=new LocationReciver();
        mGoogleCardsAdapter = new ParlorsListAdapter(salon_list, ParlorListActivity.this);
        recycler_.setAdapter(mGoogleCardsAdapter);

        updateList();
        locationGesture();

        txt_filter= (TextView)findViewById(R.id.txt_filter);
        if(isService)
            txt_filter.setVisibility(View.VISIBLE);
        else
            txt_filter.setVisibility(View.GONE);
        txt_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FilterFragment filterFragment= new FilterFragment();
                Bundle bundle= new Bundle();

                //------------------sort----------------------------------
                bundle.putBoolean("isPopularity", isPopularity);
                bundle.putBoolean("isPriceLow", isPriceLow);
                bundle.putBoolean("isPriceHigh", isPriceHigh);
                bundle.putBoolean("isNearest", isNearest);

                //--------------------------filters-----------------------
                bundle.putBoolean("isMale", isMale);
                bundle.putBoolean("isFemale", isFemale);
                bundle.putBoolean("isUnisex", isUnisex);

                bundle.putBoolean("isRating_1", isRating_1);
                bundle.putBoolean("isRating_2", isRating_2);
                bundle.putBoolean("isRating_3", isRating_3);
                bundle.putBoolean("isRating_4", isRating_4);

                bundle.putBoolean("isPrice_red", isPrice_red);
                bundle.putBoolean("isPrice_blue", isPrice_blue);
                bundle.putBoolean("isPrice_green", isPrice_green);
                bundle.putBoolean("isPrice_all", isPrice_all);

                filterFragment.setArguments(bundle);
                filterFragment.show(getSupportFragmentManager(),FILTERS );
            }
        });

        TextView txt_premium= (TextView)findViewById(R.id.txt_premium);
        TextView txt_standard= (TextView)findViewById(R.id.txt_standard);
        TextView txt_budget= (TextView)findViewById(R.id.txt_budget);

//
//        String cost="";
//        int price=0;
//        if(price==1)
//            cost= "<font color='#3e780a'>₹</font>"+" "+"₹"+" "+"₹"+" "+"₹"+" "+"₹";
//        else if(price==2)
//            cost= "<font color='#3e780a'>₹ ₹</font>"+" "+"₹"+" "+"₹"+" "+"₹";
//        else if(price==3)
//            cost= "<font color='#3e780a'>₹ ₹ ₹</font>"+" "+"₹"+" "+"₹";
//        else if(price==4)
//            cost= "<font color='#3e780a'>₹ ₹ ₹ ₹</font>"+" "+"₹";
//        else
//            cost= "<font color='#3e780a'>₹ ₹ ₹ ₹ ₹</font>";

        String str_premium= "<font color='#3e780a'>₹ ₹ ₹ ₹</font>"+" "+"₹ - "+
                "<font color='#3e780a'>₹ ₹ ₹ ₹ ₹</font>";
        txt_premium.setText(fromHtml(str_premium));

        String str_standard= "<font color='#3e780a'>₹ ₹ ₹</font>"+" "+"₹"+" "+"₹ - "+
                "<font color='#3e780a'>₹ ₹ ₹ ₹</font>"+" "+"₹";
        txt_standard.setText(fromHtml(str_standard));

        String str_budget= "<font color='#3e780a'>₹ ₹</font>"+" "+"₹"+" "+"₹"+" "+"₹ - "+
                "<font color='#3e780a'>₹ ₹ ₹</font>"+" "+"₹"+" "+"₹";
        txt_budget.setText(fromHtml(str_budget));

        card_all= (CardView)findViewById(R.id.card_all);
        card_premium= (CardView)findViewById(R.id.card_premium);
        card_standard= (CardView)findViewById(R.id.card_standard);
        card_budget= (CardView)findViewById(R.id.card_budget);

        if(Build.VERSION.SDK_INT>=21)
            card_all.setCardElevation(4f);
        card_all.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        card_premium.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBackground));
        card_standard.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBackground));
        card_budget.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBackground));

        // use for default  "AllCategories" fire this event
        logSalonCategoriesEvent("AllCategories");
        logSalonCategoriesFireBaseEvent("AllCategories");
        card_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logSalonCategoriesEvent("AllCategories");
                logSalonCategoriesFireBaseEvent("AllCategories");
                if(Build.VERSION.SDK_INT>=21){
                    card_all.setCardElevation(4f);
                    card_premium.setCardElevation(2f);
                    card_standard.setCardElevation(2f);
                    card_budget.setCardElevation(2f);
                }

                card_all.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.white));
                card_premium.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
                card_standard.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
                card_budget.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));

                filters_price= "3";

                txt_filter.setText("Sort | Filter");
                filters_rating= null;
                filters_gender= null;
                filters_sort= "0";

                PAGE_SIZE= 1;
                salon_list.clear();
                fetchServiceSalons();
            }
        });
        card_premium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logSalonCategoriesEvent("BeUPremium");
                logSalonCategoriesFireBaseEvent("BeUPremium");
                if(Build.VERSION.SDK_INT>=21){

                    card_all.setCardElevation(2f);
                    card_premium.setCardElevation(4f);
                    card_standard.setCardElevation(2f);
                    card_budget.setCardElevation(2f);
                }

                card_all.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
                card_premium.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.white));
                card_standard.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
                card_budget.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));

                filters_price= "0";

                txt_filter.setText("Sort | Filter");
                filters_rating= null;
                filters_gender= null;
                filters_sort= "0";

                PAGE_SIZE= 1;
                salon_list.clear();
                fetchServiceSalons();
            }
        });
        card_standard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logSalonCategoriesEvent("BeUStandard");
                logSalonCategoriesFireBaseEvent("BeUStandard");
                if(Build.VERSION.SDK_INT>=21){
                    card_all.setCardElevation(2f);
                    card_premium.setCardElevation(2f);
                    card_standard.setCardElevation(4f);
                    card_budget.setCardElevation(2f);
                }

                card_all.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
                card_premium.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
                card_standard.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.white));
                card_budget.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));

                filters_price= "1";

                txt_filter.setText("Sort | Filter");
                filters_rating= null;
                filters_gender= null;
                filters_sort= "0";

                PAGE_SIZE= 1;
                salon_list.clear();
                fetchServiceSalons();
            }
        });
        card_budget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logSalonCategoriesEvent("BeUBuget");
                logSalonCategoriesFireBaseEvent("BeUBuget");
                if(Build.VERSION.SDK_INT>=21){
                    card_all.setCardElevation(2f);
                    card_premium.setCardElevation(2f);
                    card_standard.setCardElevation(2f);
                    card_budget.setCardElevation(4f);
                }

                card_all.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
                card_premium.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
                card_standard.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
                card_budget.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.white));

                filters_price= "2";

                txt_filter.setText("Sort | Filter");
                filters_rating= null;
                filters_gender= null;
                filters_sort= "0";

                PAGE_SIZE= 1;
                salon_list.clear();
                fetchServiceSalons();
            }
        });

    }

    private void updateList() {

        txt_location.setText(BeuSalonsSharedPrefrence.getAddressLocalty());
        if(isService || isDeal){

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    openDealDB();
                }
            }, 250);

        }else if(isFavouriteSalon){
            isDeal= false;
            setToolBar();
            fetchFavSalons();
        }
                                  //favourite salon

    }

    public void openDealDB(){

        if(deal_post==null){

            try {

                DB snappyDB = DBFactory.open(this);
                //db mai jo saved cart hai
                if (snappyDB.exists(AppConstant.USER_CART_DB))
                    savedCart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);

                snappyDB.close();

                int size= savedCart==null?0:savedCart.getServicesList().size();

                if(size>0 &&
                        savedCart.getCartType()!=null &&
                        savedCart.getCartType().equalsIgnoreCase(AppConstant.DEAL_TYPE)){


                    deal_post= new Deal_Detail_Post();
                    List<PostDealDetail> list= new ArrayList<>();
//                    for(int i=0; i<size; i++){
//
//                        PostDealDetail detail= new PostDealDetail();
//
//                        //yaha int bhej raha hoon, string nai
//                        detail.setDealId(savedCart.getServicesList().get(i).getDealId());
//
//                        List<ServicesList> services= new ArrayList<>();
//                        ServicesList service= new ServicesList();
//                        service.setServiceCode(savedCart.getServicesList().get(i).getService_code());
//                        service.setBrandId(savedCart.getServicesList().get(i).getBrand_id());
//                        service.setProductId(savedCart.getServicesList().get(i).getProduct_id());
//                        services.add(service);
//                        detail.setServices(services);
//
//                        list.add(detail);
//                    }
//                    deal_post.setSelectedDeals(list);               //finally updating the stuff
                    List<Integer> serviceCodes=new ArrayList();

                    boolean isMemberShip=false;
                    for(int i=0;i<size;i++){

                        PostDealDetail detail= new PostDealDetail();
                        detail.setDealId(savedCart.getServicesList().get(i).getDealId());
                        detail.setQuantity(savedCart.getServicesList().get(i).getQuantity());

                        if(savedCart.getServicesList().get(i).getPackageServices().size()>0){

                            List<ServicesList> services= new ArrayList<>();
                            for(int j=0; j<savedCart.getServicesList().get(i).getPackageServices().size(); j++){

                                ServicesList service= new ServicesList();
                                service.setServiceCode(savedCart.getServicesList().get(i)
                                        .getPackageServices().get(j).getService_code());
                                service.setBrandId(savedCart.getServicesList().get(i)
                                        .getPackageServices().get(j).getBrand_id());

                                service.setProductId(savedCart.getServicesList().get(i)
                                        .getPackageServices().get(j).getProduct_id());

                                services.add(service);
                            }
                            detail.setServices(services);

                        }
                        else if (savedCart.getServicesList().get(i).isMyMembershipFreeService()){

                            serviceCodes.add(Integer.parseInt(String.valueOf(savedCart.getServicesList().get(i).getService_code()).toString().trim()));
                            isMemberShip=true;
                        }
                        else{

                            List<ServicesList> services= new ArrayList<>();
                            ServicesList service= new ServicesList();
                            service.setServiceCode(savedCart.getServicesList().get(i).getService_code());
                            service.setBrandId(savedCart.getServicesList().get(i).getBrand_id());
                            service.setProductId(savedCart.getServicesList().get(i).getProduct_id());
                            services.add(service);

                            detail.setServices(services);
                        }


                        list.add(detail);
                    }
                    deal_post.setSelectedDeals(list);               //finally updating the stuff

                    //
                    if (isMemberShip){
                        isDeal= false;
                        setToolBar();
                        fetchParlorsMembership(serviceCodes);
                    } else {
                        isDeal= true;
                        setToolBar();
                        fetchDealSalons();
                    }
                }

                else{

                    linear_.setVisibility(View.VISIBLE);
                    isDeal= false;
                    setToolBar();
                    fetchServiceSalons();               //no service toh yeh set kiya
                }

            }catch (Exception e){

                e.printStackTrace();
            }
        }else{
            isDeal= true;
            setToolBar();
            //  fetchParlorsMembership();
            fetchDealSalons();             //data already hai isme toh why update one more time...! yes motherfucker
        }
    }

    private void fetchParlorsMembership(List serviceCode){
        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface service = retrofit.create(ApiInterface.class);


//        String url=service.getMembershipParlor(BeuSalonsSharedPrefrence.getLatitude(),
//                BeuSalonsSharedPrefrence.getLongitude(),serviceCode);
        Call<DealSalonListResponse> call=service.getMembershipParlor(BeuSalonsSharedPrefrence.getLatitude(),
                BeuSalonsSharedPrefrence.getLongitude(), serviceCode.toString().trim());
        Log.e("serverlist",""+serviceCode);
        call.enqueue(new Callback<DealSalonListResponse>() {
            @Override
            public void onResponse(Call<DealSalonListResponse> call, Response<DealSalonListResponse> response) {
                if(response.isSuccessful()){

                    if(response.body().getSuccess()){

                        Log.i("investigatingggg", "i'm in on response successful");

                        if(response.body().getData().getParlors().size()==0){


                            txt_empty.setText("No Salons With The Selected Deals");

                            txt_empty.setVisibility(View.GONE);
                            recycler_.setVisibility(View.GONE);

                        }else{
                            parlors.addAll(response.body().getData().getParlors());
                            DealsDetailsParlorListAdapter dealsSalonAdapter=new DealsDetailsParlorListAdapter(parlors,savedCart.getServicesList()
                                    ,ParlorListActivity.this);
                            recycler_.setAdapter(dealsSalonAdapter);


                            txt_empty.setVisibility(View.GONE);
                            recycler_.setVisibility(View.VISIBLE);
                            isFilter=false;
                        }

                        progress_.setVisibility(View.GONE);
                        btn_retry.setVisibility(View.GONE);

                    }else{
                        Log.i("investigatingggg", "i'm in on response not successful");
                        recycler_.setVisibility(View.GONE);
                        progress_.setVisibility(View.GONE);
                        btn_retry.setVisibility(View.VISIBLE);


                    }
                }else{
                    Log.i("investigatingggg", "i'm in else else");
                    recycler_.setVisibility(View.GONE);
                    progress_.setVisibility(View.GONE);
                    btn_retry.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onFailure(Call<DealSalonListResponse> call, Throwable t) {

                Log.e("investigatingggg", "i'm in on falure: "+ t.getMessage()+ "  "+t.getStackTrace());
                recycler_.setVisibility(View.GONE);
                progress_.setVisibility(View.GONE);
                btn_retry.setVisibility(View.VISIBLE);
            }
        });


    }


    private void fetchServiceSalons() {

        if(isFilter){

            PAGE_SIZE=1;
            isFilter= false;
            salon_list.clear();

            if(filters_price==null || filters_price.equalsIgnoreCase("3")){

                if(Build.VERSION.SDK_INT>=21){
                    card_all.setCardElevation(4f);
                    card_premium.setCardElevation(2f);
                    card_standard.setCardElevation(2f);
                    card_budget.setCardElevation(2f);
                }

                card_all.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.white));
                card_premium.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
                card_standard.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
                card_budget.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));

            }else if(filters_price.equalsIgnoreCase("0")){

                if(Build.VERSION.SDK_INT>=21){
                    card_all.setCardElevation(2f);
                    card_premium.setCardElevation(4f);
                    card_standard.setCardElevation(2f);
                    card_budget.setCardElevation(2f);
                }

                card_all.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
                card_premium.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.white));
                card_standard.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
                card_budget.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
            }else if(filters_price.equalsIgnoreCase("1")){

                if(Build.VERSION.SDK_INT>=21){
                    card_all.setCardElevation(2f);
                    card_premium.setCardElevation(2f);
                    card_standard.setCardElevation(4f);
                    card_budget.setCardElevation(2f);
                }

                card_all.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
                card_premium.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
                card_standard.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.white));
                card_budget.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
            }else if(filters_price.equalsIgnoreCase("2")){

                if(Build.VERSION.SDK_INT>=21){
                    card_all.setCardElevation(2f);
                    card_premium.setCardElevation(2f);
                    card_standard.setCardElevation(2f);
                    card_budget.setCardElevation(4f);
                }

                card_all.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
                card_premium.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
                card_standard.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.colorBackground));
                card_budget.setBackgroundColor(ContextCompat.getColor(ParlorListActivity.this, R.color.white));
            }


        }

        if(PAGE_SIZE==1){

            isLoading= false;
            recycler_.setVisibility(View.GONE);
            progress_.setVisibility(View.VISIBLE);
            btn_retry.setVisibility(View.GONE);
        }


        btn_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PAGE_SIZE=1;
                fetchServiceSalons();
            }
        });


        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<ParlorResponseModel> call = service.parlorsList(BeuSalonsSharedPrefrence.getLatitude(), BeuSalonsSharedPrefrence.getLongitude(),
                BeuSalonsSharedPrefrence.getUserId(),
                filters_rating, filters_price, filters_gender, filters_sort, PAGE_SIZE);
        Log.e("PAGE 408 ",""+PAGE_SIZE);
        Log.i("fuckingSe", "value in stuff: "+ BeuSalonsSharedPrefrence.getLatitude() +  " " + BeuSalonsSharedPrefrence.getLongitude()+  " " +
                filters_rating+  " " + filters_price +  " " + filters_gender+  " " + filters_sort);
        call.enqueue(new Callback<ParlorResponseModel>() {
            @Override
            public void onResponse(Call<ParlorResponseModel> call, Response<ParlorResponseModel> response) {

                if(response.isSuccessful()){
                    if(response.body().getSuccess()){


                        if (response.body().getData().getParlors().size()>0){

                            Log.i("salonlist", "i'm in success");


                            if(PAGE_SIZE==1){

                                mGoogleCardsAdapter.setInitialData(response.body().getData().getParlors());

                                recycler_.setVisibility(View.VISIBLE);

                                view.setVisibility(View.GONE);
                                progress_.setVisibility(View.GONE);
                                btn_retry.setVisibility(View.GONE);
                            }
                            else{
                                view.setVisibility(View.GONE);
                                mGoogleCardsAdapter.removeProgress();
                                mGoogleCardsAdapter.addData(response.body().getData().getParlors());

                                isLoading= false;
                            }



                        }else {
                            view.setVisibility(View.VISIBLE);
                            SpannableString ss1=  new SpannableString(noSalonCity);
                            ss1.setSpan(new StyleSpan(Typeface.BOLD), 0, ss1.length(), 0);
                            txtView_mail_id.append(ss1);
                            SpannableString ss=  new SpannableString(" info@beusalons.com");
                            ss1.setSpan(new StyleSpan(Typeface.NORMAL), 0, ss1.length(), 0);
                            txtView_mail_id.append(ss);
                            img_no_salon.setVisibility(View.VISIBLE);
                            img_no_salon_deals.setVisibility(View.GONE);
                            mGoogleCardsAdapter.removeProgress();
                        }



                    }else{
                        Log.i("salonlist", "i'm in  else");

                        recycler_.setVisibility(View.GONE);
                        progress_.setVisibility(View.GONE);
                        btn_retry.setVisibility(View.VISIBLE);
                    }


                } else{
                    Log.i("salonlist", "i'm in else else");
                    recycler_.setVisibility(View.GONE);
                    progress_.setVisibility(View.GONE);
                    btn_retry.setVisibility(View.VISIBLE);
                }

            }
            @Override
            public void onFailure(Call<ParlorResponseModel> call, Throwable t) {
                Log.i("salonlist", "i'm in failure: "+ t.getMessage()+ " "+ t.getCause()+ " "+t.getStackTrace());
                mGoogleCardsAdapter.removeProgress();
                recycler_.setVisibility(View.GONE);
                progress_.setVisibility(View.GONE);
                btn_retry.setVisibility(View.VISIBLE);
                view.setVisibility(View.GONE);
            }
        });
    }


    private void fetchDealSalons() {

        recycler_.setVisibility(View.GONE);
        progress_.setVisibility(View.VISIBLE);
        btn_retry.setVisibility(View.GONE);

        btn_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fetchDealSalons();
            }
        });

        deal_post.setLatitude(Double.parseDouble(BeuSalonsSharedPrefrence.getLatitude()));
        deal_post.setLongitude(Double.parseDouble(BeuSalonsSharedPrefrence.getLongitude()));

        Retrofit retrofit3 = ServiceGenerator.getClient();
        ApiInterface service = retrofit3.create(ApiInterface.class);
        Call<DealSalonListResponse> call = service.getNewDealData(deal_post);

        call.enqueue(new Callback<DealSalonListResponse>() {

            @Override
            public void onResponse(Call<DealSalonListResponse> call, Response<DealSalonListResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().getSuccess()){

                        Log.i("investigatingggg", "i'm in on response successful");

                        if(response.body().getData().getParlors().size()==0){


                            txt_empty.setText("No Salons With The Selected Deals");

                            txt_empty.setVisibility(View.GONE);

                            recycler_.setVisibility(View.GONE);
                            view.setVisibility(View.VISIBLE);
                            img_no_salon.setVisibility(View.GONE);
                            img_no_salon_deals.setVisibility(View.VISIBLE);
                            SpannableString ss1=  new SpannableString(noSalonDeals);
                            ss1.setSpan(new StyleSpan(Typeface.BOLD), 0, ss1.length(), 0);
                            txtView_mail_id.append(ss1);
                        }else{
                            parlors.addAll(response.body().getData().getParlors());
                            DealsDetailsParlorListAdapter dealsSalonAdapter=new DealsDetailsParlorListAdapter(parlors,savedCart.getServicesList()
                                    ,ParlorListActivity.this);
                            recycler_.setAdapter(dealsSalonAdapter);


                            txt_empty.setVisibility(View.GONE);
                            recycler_.setVisibility(View.VISIBLE);
                            view.setVisibility(View.GONE);

                            isFilter=false;
                        }

                        progress_.setVisibility(View.GONE);
                        btn_retry.setVisibility(View.GONE);

                    }else{
                        Log.i("investigatingggg", "i'm in on response not successful");
                        recycler_.setVisibility(View.GONE);
                        progress_.setVisibility(View.GONE);
                        btn_retry.setVisibility(View.VISIBLE);


                    }
                }else{
                    Log.i("investigatingggg", "i'm in else else");
                    recycler_.setVisibility(View.GONE);
                    progress_.setVisibility(View.GONE);
                    btn_retry.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onFailure(Call<DealSalonListResponse> call, Throwable t) {


                Log.e("investigatingggg", "i'm in on falure: "+ t.getMessage()+ "  "+t.getStackTrace());
                recycler_.setVisibility(View.GONE);
                progress_.setVisibility(View.GONE);
                btn_retry.setVisibility(View.VISIBLE);
            }
        });
    }



    public void fetchFavSalons(){

        recycler_.setVisibility(View.GONE);
        progress_.setVisibility(View.VISIBLE);
        btn_retry.setVisibility(View.GONE);

        btn_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fetchFavSalons();
            }
        });

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<ParlorResponseModel> call = service.getFavouriteSalons(BeuSalonsSharedPrefrence.getLatitude(),
                BeuSalonsSharedPrefrence.getLongitude(), BeuSalonsSharedPrefrence.getUserId(), 1);

        Log.i("i'mfavour", "value in above stuff: "+ BeuSalonsSharedPrefrence.getLatitude()+
                "   "+BeuSalonsSharedPrefrence.getLongitude() +"   "+BeuSalonsSharedPrefrence.getUserId());
        call.enqueue(new Callback<ParlorResponseModel>() {
            @Override
            public void onResponse(Call<ParlorResponseModel> call, Response<ParlorResponseModel> response) {

                if(response.isSuccessful()){

                    if(response.body().getSuccess()){

                        Log.i("favsalons", "success pe");

                        if(response.body().getData().getParlors().size() == 0){

                            txt_empty.setText("You don't have any favourite salons yet");
                            txt_empty.setVisibility(View.GONE);
                            recycler_.setVisibility(View.GONE);
                            view.setVisibility(View.VISIBLE);
                            SpannableString ss1=  new SpannableString(noSalonCity);
                            ss1.setSpan(new StyleSpan(Typeface.BOLD), 0, ss1.length(), 0);
                            txtView_mail_id.append(ss1);
                            txtView_mail_id.append(" info@beusalons.com");
                            img_no_salon.setVisibility(View.VISIBLE);
                            img_no_salon_deals.setVisibility(View.GONE);

                        }else{


                            mGoogleCardsAdapter = new ParlorsListAdapter(response.body().getData().getParlors(), ParlorListActivity.this);
                            recycler_.setAdapter(mGoogleCardsAdapter);

                            txt_empty.setVisibility(View.GONE);
                            view.setVisibility(View.GONE);
                            recycler_.setVisibility(View.VISIBLE);
                        }

                        progress_.setVisibility(View.GONE);
                        btn_retry.setVisibility(View.GONE);

                    }else{
                        Log.i("favsalons", "else pe");
                        recycler_.setVisibility(View.GONE);
                        progress_.setVisibility(View.GONE);
                        btn_retry.setVisibility(View.VISIBLE);

                    }


                }else{
                    Log.i("favsalons", "else else pe");
                    recycler_.setVisibility(View.GONE);
                    progress_.setVisibility(View.GONE);
                    btn_retry.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<ParlorResponseModel> call, Throwable t) {
                Log.i("favsalons", "failure: "+ t.getMessage()+ " "+t.getCause()+ " "+t.getStackTrace());

                recycler_.setVisibility(View.GONE);
                progress_.setVisibility(View.GONE);
                btn_retry.setVisibility(View.VISIBLE);
            }
        });
    }

    public void locationGesture(){

        final GestureDetector detector = new GestureDetector(this, new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(MotionEvent e) {
                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {

            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                openLocationIntent();
                return false;
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                return false;
            }

            @Override
            public void onLongPress(MotionEvent e) {

            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                return false;
            }
        });

        horizontal_location.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                detector.onTouchEvent(event);
                return false;
            }
        });
    }

    protected void openLocationIntent() {

        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
//                            .setBoundsBias(PLACE_BOUNDS_DELHI)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                BeuSalonsSharedPrefrence.saveAddress(this, place.getName().toString(),
                        place.getAddress().toString(),String.valueOf(place.getLatLng().latitude),
                        String.valueOf(place.getLatLng().longitude));

                PAGE_SIZE = 1;
                updateList();                   //update the salon list respective of service, deal or fav salon

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {

            } else if (resultCode == RESULT_CANCELED) {

            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFilterEvent3(FiltersEvent event) {

        isMale= event.getFilters_hashmap().get("isMale")==null?false:event.getFilters_hashmap().get("isMale");
        isFemale= event.getFilters_hashmap().get("isFemale")==null?false:event.getFilters_hashmap().get("isFemale");
        isUnisex= event.getFilters_hashmap().get("isUnisex")==null?false:event.getFilters_hashmap().get("isUnisex");

        isPopularity= event.getFilters_hashmap().get("isPopularity")==null?false:event.getFilters_hashmap().get("isPopularity");
        isPriceLow= event.getFilters_hashmap().get("isPriceLow")==null?false:event.getFilters_hashmap().get("isPriceLow");
        isPriceHigh= event.getFilters_hashmap().get("isPriceHigh")==null?false:event.getFilters_hashmap().get("isPriceHigh");
        isNearest= event.getFilters_hashmap().get("isNearest")==null?false:event.getFilters_hashmap().get("isNearest");

        isRating_1= event.getFilters_hashmap().get("isRating_1")==null?false:event.getFilters_hashmap().get("isRating_1");
        isRating_2= event.getFilters_hashmap().get("isRating_2")==null?false:event.getFilters_hashmap().get("isRating_2");
        isRating_3= event.getFilters_hashmap().get("isRating_3")==null?false:event.getFilters_hashmap().get("isRating_3");
        isRating_4= event.getFilters_hashmap().get("isRating_4")==null?false:event.getFilters_hashmap().get("isRating_4");

        isPrice_red= event.getFilters_hashmap().get("isPrice_red")==null?false:event.getFilters_hashmap().get("isPrice_red");
        isPrice_blue= event.getFilters_hashmap().get("isPrice_blue")==null?false:event.getFilters_hashmap().get("isPrice_blue");
        isPrice_green= event.getFilters_hashmap().get("isPrice_green")==null?false:event.getFilters_hashmap().get("isPrice_green");
        isPrice_all= event.getFilters_hashmap().get("isPrice_all")==null?false:event.getFilters_hashmap().get("isPrice_all");


        //--------------------------------------------sort------------------------------
        if(isPopularity){
            filters_sort="1";
        }else if(isPriceLow){
            filters_sort="2";
        }else if(isPriceHigh){
            filters_sort="3";
        }else if(isNearest){
            filters_sort="0";           //distance ka hai yeh
        }else{
            filters_sort=null;
        }

        //----------------------------------------------filters------------------------

        int fil_count=0;

        if(isRating_1){
            filters_rating="1";
            fil_count++;
        }else if(isRating_2){
            filters_rating="2";
            fil_count++;
        }else if(isRating_3){
            filters_rating="3";
            fil_count++;
        }else if(isRating_4){
            filters_rating="4";
            fil_count++;
        }else{
            filters_rating=null;
        }

        if(isPrice_red){
            filters_price= "0";
            fil_count++;
        }else if(isPrice_blue){
            filters_price= "1";
            fil_count++;
        }else if(isPrice_green){
            filters_price= "2";
            fil_count++;
        }else if(isPrice_all){
            filters_price= "3";
            fil_count++;
        }else{
            filters_price= null;
        }

        if(isMale){
            filters_gender="2";
            fil_count++;
        }else if (isFemale) {
            filters_gender="3";
            fil_count++;
        }else if(isUnisex){
            filters_gender="1";
            fil_count++;
        }else{
            filters_gender=null;
        }

        String filters_count = "";
        if(fil_count==0){
            filters_count ="";
        }else{

            filters_count = "("+fil_count+")";
        }

        txt_filter.setText("Sort | Filter "+ filters_count);

        isFilter= true;
        updateList();

        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isMale"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isFemale"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isUnisex"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isPopularity"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isPriceLow"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isPriceHigh"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isNearest"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isRating_1"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isRating_2"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isRating_3"));
        Log.i("filterbhai", "value in boolean: "+ event.getFilters_hashmap().get("isRating_4"));
    }


    //overridden methods hai ..
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Build.VERSION.SDK_INT < 23){

            startService(new Intent(ParlorListActivity.this, LocationUpdateService.class));
        }else {
            requestContactPermission();
        }

    }

    private void requestContactPermission() {

        int hasContactPermission = ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);

        if (hasContactPermission != PackageManager.PERMISSION_GRANTED) {
            Log.i("permissions", "i'm in not onReqest permis granterd");
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, GPS_PERMISSION);
        } else {
            Log.i("permissions", "i'm in already onReqest permis granterd");
            startService(new Intent(ParlorListActivity.this, LocationUpdateService.class));

//            Toast.makeText(this, "Contact Permission is already granted", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case GPS_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("permissions", "i'm in onReqest permis granterd");
                    startService(new Intent(ParlorListActivity.this, LocationUpdateService.class));

                }
            }
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){

            if(isDeal)
                getSupportActionBar().setTitle("Compare Price of Selected Deals");
            else
                getSupportActionBar().setTitle(getResources().getString(R.string.salon_list));

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
        EventBus.getDefault().register(this);
        registerReceiver(locationReciver,locationFilter);
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        EventBus.getDefault().unregister(this);
        unregisterReceiver(locationReciver);
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        logSalonBackButtonEvent();
        logSalonBackButtonFireBaseEvent();
    }

    private void logSalonCategoriesEvent (String categoriesName) {
        Log.e("SalonCategories","fine");
        Bundle params = new Bundle();
        params.putString(AppConstant.CategoriesName, categoriesName);
        logger.logEvent(AppConstant.SalonCategories,params);
    }
    private void logSalonCategoriesFireBaseEvent (String categoriesName) {
        Log.e("SalonCategoriesfire","fine");
        Bundle bundle=new Bundle();
        bundle.putString(AppConstant.CategoriesName,categoriesName);
        mFirebaseAnalytics.logEvent(AppConstant.SalonCategories,bundle);
    }

    private void logSalonBackButtonEvent () {
        Log.e("SalonBackButton","fine");
        logger.logEvent(AppConstant.SalonBackButton);
    }
    private void logSalonBackButtonFireBaseEvent () {
        Log.e("SalonBackButtonfire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.SalonBackButton,bundle);
    }
    private void logSalonScrollEvent () {
        Log.e("SalonScroll","fine");
        logger.logEvent(AppConstant.SalonScroll);
    }
    private void logSalonScrollFireBaseEvent () {
        Log.e("SalonScrollfire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.SalonScroll,bundle);
    }
    /**
     * Broadcast reciver class for location update
     */
    public class LocationReciver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            Bundle bundle = intent.getExtras();
            if (bundle != null && bundle.getString("local") != null) {

                final String response = bundle.getString("local");
                final Double lat =bundle.getDouble("lat");
                final Double lon =bundle.getDouble("long");
                if (!response.equalsIgnoreCase(BeuSalonsSharedPrefrence.getAddressLocalty())){

                    // Toast.makeText(MainActivity.this,"We have found new Location "+response,Toast.LENGTH_LONG).show();
                    AlertDialog.Builder builder;
                    /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light);
                    } else {
                        builder = new AlertDialog.Builder(context);
                    }*/
                    builder = new AlertDialog.Builder(context);
                    builder.setMessage("We have found new Location "+response +" do you want to update this location")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    BeuSalonsSharedPrefrence.saveAddress(ParlorListActivity.this,
                                            response,response,String.valueOf(lat),String.valueOf(lon));

                                    PAGE_SIZE = 1;
                                    updateList();

                                    dialog.dismiss();

                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                }else{

//                    fetchLoyaltyData(String.valueOf(lat),String.valueOf(lon));
                }

            }
        }
    }
}
