package com.beusalons.android.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Adapter.FreeBies.FreebiesCategoryAdapter;
import com.beusalons.android.Adapter.FreeBies.FreebiesCategoryAdapterBackUp;
import com.beusalons.android.Adapter.FreebieCorporate;
import com.beusalons.android.BuildConfig;
import com.beusalons.android.Event.FreebieUpgradeEvent;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Helper.CheckConnection;
import com.beusalons.android.Model.BillSummery.CouponData;
import com.beusalons.android.Model.Loyalty.FreeCorporateService;
import com.beusalons.android.Model.Loyalty.LoyaltyPointsRequest;
import com.beusalons.android.Model.Loyalty.LoyaltyPointsResponse;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.MyAppointments_Activity;
import com.beusalons.android.ParlorListActivity;
import com.beusalons.android.R;
import com.beusalons.android.RegisterCorporateActivity;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Task.UserCartTask;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.WebViewActivity;
import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;



public class UserLoyalty extends Fragment {

    private static final String TAG= UserLoyalty.class.getSimpleName();

    private int retry=0;

    private View mContentView, mLoadingView, view_, view_cor;

    private TextView txt_points, txt_freebie_name,
            txt_code, txt_code_corporate, txt_free_click, txt_points_corporate, txt_description, txt_top_info,
                txt_expiry, txt_freebie_expiry;
    //            txt_coupon_description, txt_coupon_code;
    private LinearLayout linear_, linear_free_service, linear_corporate, linear_coupon;

    private boolean firstLoginTest= false;

    private RecyclerView recycler_view,rec_new_freeby;

    private String share_txt="", cor_share_txt="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_user_loyalty, container, false);
        mContentView= view.findViewById(R.id.linear_loyalty);
        mLoadingView= view.findViewById(R.id.loading_spinner_loyalty);
        view_= view.findViewById(R.id.view_);
        view_cor= view.findViewById(R.id.view_cor);

        recycler_view= (RecyclerView) view.findViewById(R.id.rec_);
        rec_new_freeby= (RecyclerView) view.findViewById(R.id.rec_new_freeby);

        txt_points= (TextView)view.findViewById(R.id.txt_points);
        txt_freebie_name= (TextView)view.findViewById(R.id.txt_freebie_name);
        txt_description= (TextView)view.findViewById(R.id.txt_description);

        txt_expiry= view.findViewById(R.id.txt_expiry);
        txt_expiry.setTypeface(null, Typeface.ITALIC);
        txt_freebie_expiry= view.findViewById(R.id.txt_freebie_expiry);
        txt_freebie_expiry.setTypeface(null, Typeface.ITALIC);

        txt_code= (TextView)view.findViewById(R.id.txt_code);
        txt_code_corporate= (TextView)view.findViewById(R.id.txt_code_corporate);
        txt_free_click= (TextView)view.findViewById(R.id.txt_free_click);
        txt_points_corporate= (TextView)view.findViewById(R.id.txt_points_corporate);
        txt_top_info= (TextView)view.findViewById(R.id.txt_top_info);
//        txt_coupon_description= (TextView)view.findViewById(R.id.txt_coupon_description);
//        txt_coupon_code= (TextView)view.findViewById(R.id.txt_coupon_code);

        linear_= (LinearLayout)view.findViewById(R.id.linear_);
        linear_free_service= (LinearLayout)view.findViewById(R.id.linear_free_service);
        linear_corporate= (LinearLayout)view.findViewById(R.id.linear_corporate);
//        linear_coupon= (LinearLayout)view.findViewById(R.id.linear_coupon);

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rec_new_freeby.setLayoutManager(linearLayoutManager);

        txt_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent sendIntent= new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                //have to set title and content
                sendIntent.putExtra(Intent.EXTRA_TEXT , share_txt);
//                sendIntent.putExtra(Intent.EXTRA_TEXT, "Share this link with your friends. Avail yourself a free haircut at any of the Be U salons by downloading Be U app.");
                startActivity(Intent.createChooser(sendIntent, "Share Code:"));
            }
        });


        if(CheckConnection.isConnected(getActivity())){
            if(retry==0){
                fetchLoyaltyData();

            }else
                Log.i("nodata", "come'on no data what the fuck is wrong");
        }


        return view;
    }

    public void fetchLoyaltyData(){

        mContentView.setVisibility(View.GONE);
        mLoadingView.setVisibility(View.VISIBLE);

        final LoyaltyPointsRequest loyaltyPointsRequest= new LoyaltyPointsRequest();
        int firstLogin=0;           //0 matalb user ka first login nahi hai

        try{
            SharedPreferences preferences= getActivity().getSharedPreferences("globalPreference", Context.MODE_PRIVATE);
            if(preferences.getBoolean("firstLogin", false)){

                firstLogin=1;

                firstLoginTest= true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        loyaltyPointsRequest.setUserId(BeuSalonsSharedPrefrence.getUserId());
        loyaltyPointsRequest.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        loyaltyPointsRequest.setLatitude(BeuSalonsSharedPrefrence.getLatitude());
        loyaltyPointsRequest.setLongitude(BeuSalonsSharedPrefrence.getLongitude());
        loyaltyPointsRequest.setVersionAndroid(BuildConfig.VERSION_NAME);
        loyaltyPointsRequest.setFirstLogin(firstLogin);


//        final SharedPreferences.Editor editor= preferences.edit();

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<LoyaltyPointsResponse> call= apiInterface.loyaltyPoints(loyaltyPointsRequest);
        call.enqueue(new Callback<LoyaltyPointsResponse>() {
            @Override
            public void onResponse(Call<LoyaltyPointsResponse> call, final Response<LoyaltyPointsResponse> response) {

                try{
                    if(response.isSuccessful()){
                        if(response.body().getSuccess()){

                            cor_share_txt=response.body().getData().getCorporateReferalMessage()!=null?
                                    response.body().getData().getCorporateReferalMessage().getMessage():"";
                            share_txt= response.body().getData().getNoramlReferalMessage()!=null?
                                    response.body().getData().getNoramlReferalMessage().getMessage():"";

                            Log.i("theloyalty", "value:" + cor_share_txt+ "  "+ share_txt);

                            txt_points.setText(""+response.body().getData().getPoints());
                            txt_code.setText(""+response.body().getData().getCode());

                            if(response.body().getData().getFreeExpiry()!=null &&
                                    !response.body().getData().getFreeExpiry().equalsIgnoreCase("")){
                                txt_expiry.setVisibility(View.VISIBLE);
                                txt_expiry.setText("Expiry: "+ formatDateTime(response.body().getData().getFreeExpiry()));
                            }else
                                txt_expiry.setVisibility(View.GONE);


                            BeuSalonsSharedPrefrence.setMyLoyaltyPoints(response.body().getData().getPoints());
                            BeuSalonsSharedPrefrence.setReferCode(getActivity() ,response.body().getData().getCode());
                            BeuSalonsSharedPrefrence.setFreeHeircutBar(response.body().getData().getFreeHairCutBar());
                            BeuSalonsSharedPrefrence.setNormalMessage(response.body().getData().getNoramlReferalMessage().getMessage());

                            Float amt;
                            String name="";
                            if(response.body().getData().getActiveMemberships()!=null &&
                                    response.body().getData().getActiveMemberships().size()>0){

                                amt= (float)response.body().getData().getActiveMemberships().get(0).getCredits();
                                name= response.body().getData().getActiveMemberships().get(0).getName();
                            }
                            else
                                amt=(float)0;

                            BeuSalonsSharedPrefrence.setMembershipName(name);
                            BeuSalonsSharedPrefrence.setMembershipPoints(amt);

                            if (response.body().getData().isSubscribed()){
                                BeuSalonsSharedPrefrence.setisSubscribe(true);
                                BeuSalonsSharedPrefrence.setSubsReferMsg(response.body().getData().getSubscriptionReferMessage());
                                BeuSalonsSharedPrefrence.setSubsBalance(response.body().getData().getRemainingSubscriptionAmount());

                            }

                            int freebee_size;
                            if(response.body().getData().getTnc()!=null){
                                freebee_size= response.body().getData().getFreebies().size()+1;
                            }else{
                                freebee_size= response.body().getData().getFreebies().size();
                            }

                            if(response.body().getData().getCorporateEmailId()!=null &&
                                    !response.body().getData().getCorporateEmailId().equalsIgnoreCase("") &&
                                    !response.body().getData().getCorporateEmailId().equalsIgnoreCase(" ")){

                                BeuSalonsSharedPrefrence.setCorporateReferCode(
                                        response.body().getData().getCorporateReferalMessage().getMessage());
                                BeuSalonsSharedPrefrence.setVerifyCorporate(true);
                                BeuSalonsSharedPrefrence.setCorporateId(response.body().getData().getCorporateEmailId());
                            }


                            if(response.body().getData().getFreeServices()!=null &&
                                    response.body().getData().getFreeServices().size()>0){

                                txt_freebie_name.setText(response.body().getData().getFreeServices().get(0).getName());
                                txt_description.setText(response.body().getData().getFreeServices().get(0).getDescription());

                                if(response.body().getData().getFreeServices().get(0).getExpires_at()!=null &&
                                        !response.body().getData().getFreeServices().get(0).getExpires_at().equalsIgnoreCase("")){
                                    txt_freebie_expiry.setVisibility(View.VISIBLE);
                                    txt_freebie_expiry.setText("Expiry: "+formatDateTime(response.body().getData().getFreeServices().get(0).getExpires_at()));
                                }else
                                    txt_freebie_expiry.setVisibility(View.GONE);


                                txt_free_click.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        UserServices services= new UserServices();
                                        services.setName(response.body().getData().getFreeServices().get(0).getName());

                                        services.setDealId(response.body().getData().getFreeServices().get(0).getDealId());
                                        services.setService_deal_id(response.body().getData().getFreeServices().get(0).getServiceId());

                                        services.setService_code(response.body().getData().getFreeServices().get(0).getCode());
                                        services.setPrice_id(response.body().getData().getFreeServices().get(0).getPriceId());

                                        String brand_id= response.body().getData().getFreeServices().get(0).getBrandId()!=null
                                                && !response.body().getData().getFreeServices().get(0).getBrandId().equalsIgnoreCase("")?
                                                response.body().getData().getFreeServices().get(0).getBrandId():"";
                                        String product_id= response.body().getData().getFreeServices().get(0).getProductId()!=null
                                                && !response.body().getData().getFreeServices().get(0).getProductId().equalsIgnoreCase("")?
                                                response.body().getData().getFreeServices().get(0).getProductId():"";

                                        services.setDescription(response.body().getData().getFreeServices().get(0).getDescription()!=null?
                                                response.body().getData().getFreeServices().get(0).getDescription():"");

                                        services.setBrand_id(brand_id);
                                        services.setProduct_id(product_id);

                                        services.setType("deal");
                                        services.setFree_service(true);

                                        services.setPrimary_key(services.getService_code()+"_"+services.getService_deal_id()+ brand_id+ product_id);

                                        services.setPrice(response.body().getData().getFreeServices().get(0).getPrice());
                                        services.setMenu_price(response.body().getData().getFreeServices().get(0).getPrice());

                                        UserCart userCart= new UserCart();
                                        userCart.setCartType(AppConstant.DEAL_TYPE);


                                        new Thread(new UserCartTask(getActivity(), userCart, services, false, false)).start();

                                        Intent intent= new Intent(getActivity(), ParlorListActivity.class);
                                        intent.putExtra("isDeal", true);
                                        getActivity().startActivity(intent);
                                    }
                                });

                            }else{

                                view_.setVisibility(View.GONE);
                                txt_top_info.setGravity(Gravity.CENTER);
                                linear_free_service.setVisibility(View.GONE);
                            }

                            if(response.body().getData().getFreeCorporateServices()!=null &&
                                    response.body().getData().getFreeCorporateServices().size()>0){

                                txt_points_corporate.setText(""+response.body().getData().getCorporateReferralCount());
                                txt_code_corporate.setText(""+ response.body().getData().getCode());
                                txt_code_corporate.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        Intent sendIntent= new Intent();
                                        sendIntent.setAction(Intent.ACTION_SEND);
                                        sendIntent.setType("text/plain");

                                        //have to set title and content
                                        sendIntent.putExtra(Intent.EXTRA_TEXT ,
                                                cor_share_txt);
//                sendIntent.putExtra(Intent.EXTRA_TEXT, "Share this link with your friends. Avail yourself a free haircut at any of the Be U salons by downloading Be U app.");
                                        startActivity(Intent.createChooser(sendIntent, "Share Code:"));
                                    }
                                });


                                RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(getContext(),
                                        LinearLayoutManager.HORIZONTAL, false);
                                recycler_view.setLayoutManager(layoutManager);
                                recycler_view.setHasFixedSize(true);
                                FreebieCorporate corporate= new FreebieCorporate( response.body().getData().getFreeCorporateServices());
                                recycler_view.setAdapter(corporate);


                            }else{
                                view_cor.setVisibility(View.GONE);
                                linear_corporate.setVisibility(View.GONE);
                            }



                            for(int i=0; i<freebee_size;i++){

                                View inflatedLayout= getActivity().getLayoutInflater().inflate(R.layout.row_loyalty, null, false);
                                TextView txt_name= (TextView) inflatedLayout.findViewById(R.id.txt_name);
                                TextView txt_info= (TextView) inflatedLayout.findViewById(R.id.txt_info);
                                ImageView img= (ImageView)inflatedLayout.findViewById(R.id.img);
                                TextView txt_book= (TextView)inflatedLayout.findViewById(R.id.txt_book);
                                LinearLayout share= (LinearLayout) inflatedLayout.findViewById(R.id.Linear_share);
                                LinearLayout linear_click= (LinearLayout)inflatedLayout.findViewById(R.id.linear_click);
                                linear_click.setVisibility(View.VISIBLE);

                                Log.i("termsand", "value of i:" + i+ "  "+ freebee_size);
                                if(i== freebee_size-1){
                                    Log.i("termsandCon", "i'm in the if tnc mai");

                                    txt_name.setText(response.body().getData().getTnc().getTitle());

                                    String txt="";
                                    for(int t=0;t<response.body().getData().getTnc().getPoints().size();t++){

                                        txt= txt+ "\u2022 " + response.body().getData().getTnc().getPoints().get(t).toString() + "\n";

                                    }
                                    String view_more= txt+ "\n"+ "View T&C";

                                    txt_info.setText(view_more);
                                    txt_info.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            Intent in = new Intent(getActivity(),WebViewActivity.class);
                                            in.putExtra("url", "http://beusalons.com/appTermsConditions");
                                            in.putExtra("title", "Terms & Conditions");
                                            startActivity(in);
                                        }
                                    });

                                    linear_click.setVisibility(View.GONE);
                                    img.setBackgroundResource(R.drawable.ic_tnc);
                                    share.setClickable(false);
                                }else{

                                    txt_name.setText(response.body().getData().getFreebies().get(i).getTitle());
                                    txt_info.setText(response.body().getData().getFreebies().get(i).getMessage());
                                    Glide.with(getActivity()).load(response.body().getData().getFreebies().get(i).getImage()).into(img);
                                   /* Glide.with(getActivity())
                                            .load(response.body().getData().getFreebies().get(i).getImage())
                                            .fitCenter()
//                                        .placeholder(R.drawable.loading_spinner)
                                            .crossFade()
                                            .into(img);*/

                                    if(response.body().getData().getFreebies().get(i).getAction().equalsIgnoreCase("refer")){

                                        txt_book.setText("Refer");
                                        share.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                Intent sendIntent= new Intent();
                                                sendIntent.setAction(Intent.ACTION_SEND);
                                                sendIntent.setType("text/plain");

                                                //have to set title and content
                                                sendIntent.putExtra(Intent.EXTRA_TEXT ,
                                                        share_txt);
//                sendIntent.putExtra(Intent.EXTRA_TEXT, "Share this link with your friends. Avail yourself a free haircut at any of the Be U salons by downloading Be U app.");
                                                startActivity(Intent.createChooser(sendIntent, "Share Code:"));
                                            }
                                        });

                                    }else if(response.body().getData().getFreebies().get(i).getAction().equalsIgnoreCase("corporateRefer")){

                                        txt_book.setText("Refer");
                                        share.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                Intent sendIntent= new Intent();
                                                sendIntent.setAction(Intent.ACTION_SEND);
                                                sendIntent.setType("text/plain");

                                                //have to set title and content
                                                sendIntent.putExtra(Intent.EXTRA_TEXT ,
                                                        cor_share_txt);
//                sendIntent.putExtra(Intent.EXTRA_TEXT, "Share this link with your friends. Avail yourself a free haircut at any of the Be U salons by downloading Be U app.");
                                                startActivity(Intent.createChooser(sendIntent, "Share Code:"));
                                            }
                                        });

                                    } else if(response.body().getData().getFreebies().get(i).getAction().equalsIgnoreCase("book")){

                                        txt_book.setText("Book Now");
                                        share.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                Intent intent= new Intent(getActivity(), ParlorListActivity.class);
                                                intent.putExtra("isDeal",true);
                                                startActivity(intent);
                                            }
                                        });


                                    }else if(response.body().getData().getFreebies().get(i).getAction().equalsIgnoreCase("review")){

                                        txt_book.setText("Review");
                                        share.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                Intent intent= new Intent(getActivity(), MyAppointments_Activity.class);
                                                intent.putExtra("fromFreebies", true);
                                                startActivity(intent);

                                            }
                                        });


                                    }else if(response.body().getData().getFreebies().get(i).getAction().equalsIgnoreCase("share")){

                                        txt_book.setText("Share");
                                        share.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Intent sendIntent= new Intent();
                                                sendIntent.setAction(Intent.ACTION_SEND);
                                                sendIntent.setType("text/plain");

                                                //have to set title and content
                                                sendIntent.putExtra(Intent.EXTRA_TEXT ,share_txt);
//                sendIntent.putExtra(Intent.EXTRA_TEXT, "Share this link with your friends. Avail yourself a free haircut at any of the Be U salons by downloading Be U app.");
                                                startActivity(Intent.createChooser(sendIntent, "Share code:"));

                                            }
                                        });
                                    }
                                }
                                // linear_.addView(inflatedLayout);
                            }

                            if(firstLoginTest){

                                Log.i(TAG, "first login true mai hoon ");
                                SharedPreferences globalPreference= getActivity().getSharedPreferences
                                        ("globalPreference", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editorr= globalPreference.edit();
                                editorr.putBoolean("firstLogin", false).apply();
                            }else{
                                Log.i(TAG, "first login false mai hoon");
                            }
                            if (response.body().getData().getNewFreebies().size()>0){
                                response.body().getData().getNewFreebies().get(0).setSelected(true);
                                FreebiesCategoryAdapterBackUp adapter=new FreebiesCategoryAdapterBackUp(getActivity(),response.body().getData().getNewFreebies());

                                rec_new_freeby.setAdapter(adapter);
                            }


//                            if(response.body().getData().getCouponCode()!=null &&
//                                    response.body().getData().getCouponCode().getCode()!=null &&
//                                    !response.body().getData().getCouponCode().getCode().equalsIgnoreCase("")){
//
//                                linear_.setVisibility(View.VISIBLE);
//                                txt_coupon_description.setText(response.body().getData().getCouponCode().getCouponDescription());
//                                txt_coupon_code.setText(response.body().getData().getCouponCode().getCode());
//                                txt_coupon_code.setTextIsSelectable(true);
//
//                            }else
//                                linear_coupon.setVisibility(View.GONE);


                            mContentView.setVisibility(View.VISIBLE);
                            mLoadingView.setVisibility(View.GONE);

                        }else{

                            Log.i(TAG, "I'm in loyalty response failure");
                        }
                    }else{

                        Log.i(TAG, "I'm in loyalty retrofit response failure");
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<LoyaltyPointsResponse> call, Throwable t) {

                Log.i(TAG, "Loyalty request failure: "+ t.getCause()+ "  "+t.getMessage()+  " "+ t.getStackTrace()+ " "+
                        t.getLocalizedMessage());
            }
        });
    }

    private String formatDateTime(String str_date){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        Date date_format=null;
        try {
            date_format = sdf.parse(str_date);
            Calendar cal= Calendar.getInstance();       //creating calendar instance
            cal.setTime(date_format);
            cal.add(Calendar.HOUR_OF_DAY, 5);
            cal.add(Calendar.MINUTE, 30);

            date_format= cal.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("dd/MM/yyyy").format(date_format);
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(FreebieUpgradeEvent event) {

        Log.i("freebieupgrade", "i'm in the event");
        fetchLoyaltyData();
    }

}
