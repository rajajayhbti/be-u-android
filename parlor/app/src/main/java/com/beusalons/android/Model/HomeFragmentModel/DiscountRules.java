package com.beusalons.android.Model.HomeFragmentModel;

/**
 * Created by Ajay on 9/21/2017.
 */

public class DiscountRules {
    private Integer min;
    private Integer max;
    private Integer discountPercent;
    private String text;
    private Integer minNoOfService;
    private String validity;
    private  Integer excludeNoOfServiceAmount;


    public Integer getExcludeNoOfServiceAmount() {
        return excludeNoOfServiceAmount;
    }

    public void setExcludeNoOfServiceAmount(Integer excludeNoOfServiceAmount) {
        this.excludeNoOfServiceAmount = excludeNoOfServiceAmount;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Integer getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(Integer discountPercent) {
        this.discountPercent = discountPercent;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getMinNoOfService() {
        return minNoOfService;
    }

    public void setMinNoOfService(Integer minNoOfService) {
        this.minNoOfService = minNoOfService;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }
}
