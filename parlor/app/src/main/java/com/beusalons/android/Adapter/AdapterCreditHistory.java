package com.beusalons.android.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.MymembershipDetails.CreditHistory;
import com.beusalons.android.R;

import java.util.List;

/**
 * Created by Ajay on 11/9/2017.
 */

public class AdapterCreditHistory extends RecyclerView.Adapter<AdapterCreditHistory.MyViewHolder> {
    private Activity activity;
    private List<CreditHistory>  creditHistoryList;
    public AdapterCreditHistory(Activity context, List<CreditHistory> list){

        this.activity= context;
        this.creditHistoryList=list;

    }


    @Override
    public AdapterCreditHistory.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(activity).inflate(R.layout.row_credit_history, parent, false);
        return new AdapterCreditHistory.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(AdapterCreditHistory.MyViewHolder holder, int position) {
                holder.txt_salon_name.setText(creditHistoryList.get(position).getParlorName());
                holder.txt_credit_used.setText("Credit Used: "+ AppConstant.CURRENCY+creditHistoryList.get(position).getCreditUsed());
    }

    @Override
    public int getItemCount() {
        return creditHistoryList.size();
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout linearLayout_description;
        private TextView txt_credit_used,txt_salon_name;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_credit_used=(TextView)itemView.findViewById(R.id.txt_quantity_remaining);
            txt_salon_name=(TextView)itemView.findViewById(R.id.txt_salon_name);
            linearLayout_description=(LinearLayout)itemView.findViewById(R.id.linearLayout_description);

        }
    }
}