package com.beusalons.android.SongsActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;

import com.beusalons.android.Adapter.Songs.AllSongsAdapter;
import com.beusalons.android.Model.Songs.AllSongs;
import com.beusalons.android.R;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AllSongsActivity extends AppCompatActivity{

    public static final String SALON_NAME= "com.beusalons.allsongactivity.salonname";

    private RecyclerView recycler_;
    private List<AllSongs.Song> list= new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_songs);

        String salon_name="";
        Intent intent= getIntent();
        if(intent!=null){
            salon_name= intent.getStringExtra(SALON_NAME);
        }
        setToolBar(salon_name);

        recycler_= findViewById(R.id.recycler_);
        recycler_.setLayoutManager(new LinearLayoutManager(this));
        fetchSongs();


        EditText etxt_= findViewById(R.id.etxt_);
        etxt_.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(charSequence.toString().length()>0 &&
                        !charSequence.toString().equalsIgnoreCase("")){

                    String txt= charSequence.toString().toLowerCase();

                    List<AllSongs.Song> selected= new ArrayList<>();

                    for(int a=0; a<list.size();a++)
                        if(list.get(a).getName().toLowerCase().contains(txt) ||
                                list.get(a).getName().startsWith(txt))
                            selected.add(list.get(a));

                    updateView(selected);

                }else
                    updateView(list);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void fetchSongs(){

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<AllSongs> call= service.getAllSongs();
        call.enqueue(new Callback<AllSongs>() {
            @Override
            public void onResponse(Call<AllSongs> call, Response<AllSongs> response) {

                if(response.isSuccessful()){
                    if(response.body().getSuccess()){
                        Log.i("songsactivity", "in the success pe");

                        list= response.body().getData().getSongs();
                        updateView(response.body().getData().getSongs());


                    }else{
                        Log.i("songsactivity", "in the not success pe");
                    }


                }else{
                    Log.i("songsactivity", "in the else pe");
                }


            }

            @Override
            public void onFailure(Call<AllSongs> call, Throwable t) {
                Log.i("songsactivity", "failure: "+t.getStackTrace()+ " "+t.getCause()+ " "+t.getMessage());
            }
        });



    }

    private void updateView(List<AllSongs.Song> list){

        AllSongsAdapter adapter= new AllSongsAdapter(list);
        adapter.setListener(new AllSongsAdapter.ClickListener() {
            @Override
            public void onClick(int id) {

                Log.i("listener", "in the onclick pe");
                Intent intent= new Intent();
                intent.putExtra("id", id);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
        recycler_.setAdapter(adapter);
    }


    private void setToolBar(String salon_name){

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(salon_name+ " Playlist");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
