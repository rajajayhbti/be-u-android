package com.beusalons.android.Fragment;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.R;

/**
 * Created by myMachine on 19-Dec-17.
 */

public class ServiceDesciptionFragment extends DialogFragment {

    public static final String THIS_FRAGMENT= "com.beusalons."+ServiceDesciptionFragment.class.getSimpleName();
    public static final String NAME= "com.beusalons.service.name";
    public static final String DESCRIPTION= "com.beusalons.service.description";
    public static final String TIME= "com.beusalons.service.time";

    String name="", description= "", time="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle= getArguments();
        if(bundle!=null){
            name= bundle.getString(NAME);
            description= bundle.getString(DESCRIPTION);
            time= bundle.getString(TIME);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);          //no action bar space
        LinearLayout linear_= (LinearLayout)
                inflater.inflate(R.layout.service_detail, container, false);

        TextView txt_name= linear_.findViewById(R.id.txt_name);
        TextView txt_description= linear_.findViewById(R.id.txt_description);
        TextView txt_time= linear_.findViewById(R.id.txt_time);
        TextView txt_ok= linear_.findViewById(R.id.txt_ok);

        txt_name.setText(name);
        txt_description.setText(description);
        txt_time.setText("Time: "+time+"mins");


        txt_ok.setBackgroundResource(R.drawable.txt_border_line_red);
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        ImageView img_cancel= linear_.findViewById(R.id.img_cancel);
        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return linear_;
    }
}
