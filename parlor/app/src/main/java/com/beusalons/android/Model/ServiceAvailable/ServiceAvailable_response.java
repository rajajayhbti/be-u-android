package com.beusalons.android.Model.ServiceAvailable;

import java.util.List;

/**
 * Created by Ajay on 10/9/2017.
 */

public class ServiceAvailable_response {

    private Boolean success;
    private List<ServiceAvailableData> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<ServiceAvailableData> getData() {
        return data;
    }

    public void setData(List<ServiceAvailableData> data) {
        this.data = data;
    }
}
