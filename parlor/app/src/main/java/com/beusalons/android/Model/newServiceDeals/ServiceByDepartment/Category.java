package com.beusalons.android.Model.newServiceDeals.ServiceByDepartment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 5/30/2017.
 */

public class Category {

    private String name;
    private List<Service> services = new ArrayList<>();
    private List<Package> packages = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public List<Package> getPackages() {
        return packages;
    }

    public void setPackages(List<Package> packages) {
        this.packages = packages;
    }
}
