package com.beusalons.android.Model.HomeFragmentModel;

/**
 * Created by Ajay on 3/21/2017.
 */

public class Service {


    private Integer serviceCode;

    private String name;

    private String gender;

    public Integer getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(Integer serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
