package com.beusalons.android.Model;

/**
 * Created by myMachine on 20-Dec-17.
 */

public class ServerTime {

    private boolean success;
    private String data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
