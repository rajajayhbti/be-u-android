package com.beusalons.android.Model.SalonFilter;

import java.util.List;

/**
 * Created by myMachine on 22-Feb-18.
 */

public class FilterResponse {

    private Boolean success;
    private Data data;

    public class Data{

        private List<Category> categories = null;
        private List<List<String>> brands = null;

        public List<Category> getCategories() {
            return categories;
        }

        public void setCategories(List<Category> categories) {
            this.categories = categories;
        }

        public List<List<String>> getBrands() {
            return brands;
        }

        public void setBrands(List<List<String>> brands) {
            this.brands = brands;
        }
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
