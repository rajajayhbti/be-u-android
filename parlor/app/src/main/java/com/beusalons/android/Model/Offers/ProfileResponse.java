package com.beusalons.android.Model.Offers;

/**
 * Created by myMachine on 1/21/2017.
 */

public class ProfileResponse {


    private Boolean success;
    private ProfileData data;


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ProfileData getData() {
        return data;
    }

    public void setData(ProfileData data) {
        this.data = data;
    }



}
