package com.beusalons.android.Model.newServiceDeals.ServiceByDepartment;

/**
 * Created by myMachine on 5/30/2017.
 */

public class ServiceResponse {

    private Boolean success;
    private Data data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
