package com.beusalons.android.Model.Corporate.CorporateDetail;

/**
 * Created by myMachine on 7/31/2017.
 */

public class Company {

    private String name;
    private String companyId;
    private String domain;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
