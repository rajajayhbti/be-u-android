package com.beusalons.android.Model.newServiceDeals.ServiceByDepartment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 5/30/2017.
 */

public class Data {

    private List<Category> categories = new ArrayList<>();
    private Slabs slabs = null;

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Slabs getSlabs() {
        return slabs;
    }

    public void setSlabs(Slabs slabs) {
        this.slabs = slabs;
    }
}
