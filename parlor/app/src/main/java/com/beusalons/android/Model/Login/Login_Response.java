package com.beusalons.android.Model.Login;

import java.io.Serializable;

/**
 * Created by myMachine on 11/1/2016.
 */

public class Login_Response implements Serializable {
    private Boolean success;
    private String message;
    private Login_ResponseData data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Login_ResponseData getData() {
        return data;
    }

    public void setData(Login_ResponseData data) {
        this.data = data;
    }
}
