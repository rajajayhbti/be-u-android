package com.beusalons.android.Adapter.Songs;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Model.Songs.SalonSongs;
import com.beusalons.android.R;
import com.bumptech.glide.Glide;

import java.io.IOException;
import java.util.List;

/**
 * Created by myMachine on 28-Dec-17.
 */

public class SalonSongsAdapter extends RecyclerView.Adapter<SalonSongsAdapter.ViewHolder> {

    private List<SalonSongs.Data> list;

    public SalonSongsAdapter(List<SalonSongs.Data> list){
        this.list= list;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view_= LayoutInflater.from(parent.getContext()).
                inflate(R.layout.adapter_salon_songs, null, false);
        return new ViewHolder(view_);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final int index= position;
        LinearLayout linear_= holder.linear;
        TextView txt_name= linear_.findViewById(R.id.txt_name);
        TextView txt_detail= linear_.findViewById(R.id.txt_detail);

        txt_name.setText(list.get(index).getSongName());
        txt_detail.setText(list.get(index).getRequestedBy());

        View view_= linear_.findViewById(R.id.view_);
        if(list.size()-1==position)
            view_.setVisibility(View.INVISIBLE);
        else
            view_.setVisibility(View.VISIBLE);

        ImageView img_= linear_.findViewById(R.id.img_);

        if(position==0){

            try{
                Glide.with(linear_.getContext()).load(R.drawable.song)
                        .load(list.get(position).getImageUrl())
                 .into(img_);
            }catch (Exception e){
                e.printStackTrace();
            }

        }else{

            try{
                Glide.with(linear_.getContext()).load(R.drawable.song)
                        .load(list.get(position).getImageUrl())
                        .into(img_);
            }catch (Exception e){
                e.printStackTrace();
            }
        }



    }

    @Override
    public int getItemCount() {

        if(list!=null &&
                list.size()>0)
            return list.size();
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout linear;
        public ViewHolder(View itemView) {
            super(itemView);
            linear= (LinearLayout)itemView;
        }
    }


}
