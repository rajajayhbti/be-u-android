package com.beusalons.android.Model.subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ashish Sharma on 1/23/2018.
 */

public class SubscriptionResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private SubsData data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public SubsData getData() {
        return data;
    }

    public void setData(SubsData data) {
        this.data = data;
    }

}
