package com.beusalons.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Dialog.ShowDetailsBillSummary;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Helper.CheckConnection;
import com.beusalons.android.Model.AppointmentDetail.AppointmentDetailPost;
import com.beusalons.android.Model.AppointmentDetail.AppointmentDetailResponse;
import com.beusalons.android.Model.Appointments.AppointmentPost;
import com.beusalons.android.Model.OnlinePay.UseFreeBiesPost;
import com.beusalons.android.Model.PaymentSuccessPost;
import com.beusalons.android.Model.PaymentSuccessResponse;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.google.gson.Gson;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OnlinePaymentActivity extends AppCompatActivity implements PaymentResultListener {

    private static final String TAG= OnlinePaymentActivity.class.getSimpleName();

    private TextView  txtShowDetails,  txt_info,txtViewActionBarName,txt_suggetion;
    private TextView txt_freebee,txt_100, txt_saved, txt_welcome,txt_freeBee_points,txt_grand_total,txt_use_coupon,txt_freebee_se_tnc;
    private int payOption= 2;
    private Button btn_pay;
    private LinearLayout linear_online,ll_freebie_use;
    private ImageView imgViewBack;
    private View mContentView;
    private View mLoadingView;

    private CheckBox cbUsePoints,cbUseThreading,cb_use_membership;
    private static String userId, accessToken, email, mobile;

    private static String appt_id;                  //appiontment from the notification
    private double subtotalTemp=0;
//    private AppointmentDetailResponse paymentResponse= new AppointmentDetailResponse();

    private static int data_retry=0;

    private double payable_amount;
    private AppointmentPost appointmentPost;
    private AppointmentDetailResponse appointmentDetailResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_payment);
        setToolBar();

        Bundle bundle= getIntent().getExtras();
        if(bundle!= null && bundle.containsKey("apptId")){
            Log.e("investiga" , " i'm in the onine paymenbt acitivty"+ bundle.getString("apptId"));
            appt_id= bundle.getString("apptId");
        }

        mContentView= findViewById(R.id.relative_online_payments);
        mLoadingView= findViewById(R.id.loading_spinner);
        mContentView.setVisibility(View.GONE);
        mLoadingView.setVisibility(View.VISIBLE);
        txt_grand_total= (TextView)findViewById(R.id.txt_bill_summary_grand_total);
        txt_100= (TextView)findViewById(R.id.txt_100);
       // txt_welcome= (TextView)findViewById(R.id.txt_welcome);
        txt_suggetion= (TextView)findViewById(R.id.txt_suggetion);
       // txt_welcome.setText(fromHtml(getString(R.string.txt_online_payment)));

        txt_freebee= (TextView)findViewById(R.id.txt_bill_summary_freebee);
        cbUsePoints= (CheckBox) findViewById(R.id.cb_use_points);
        cb_use_membership= (CheckBox) findViewById(R.id.cb_use_membership);
        linear_online= (LinearLayout)findViewById(R.id.linear_online);

      txt_info= (TextView)findViewById(R.id.txt_info);
//        txt_info.setVisibility(View.GONE);*/
         txt_freeBee_points= (TextView)findViewById(R.id.txt_freeBee_points);
        txt_saved= (TextView)findViewById(R.id.txt_bill_summary_saved) ;

        btn_pay= (Button)findViewById(R.id.btn_bill_summary_pay);
        txt_freebee_se_tnc= (TextView)findViewById(R.id.txt_frebie_use_tnc);
        txtShowDetails=(TextView)findViewById(R.id.txt_show_detail_bill);

//        txt_bill_summary_freebee=(TextView)findViewById(R.id.txt_bill_summary_freebee);

         txt_freeBee_points.setText("Total Freebie Points Are "+BeuSalonsSharedPrefrence.getMyLoyaltyPoints());

        cbUsePoints.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){

                   /* payable_amount=   payable_amount-BeuSalonsSharedPrefrence.getMyLoyaltyPoints();
                    txt_grand_total.setText(AppConstant.CURRENCY+ String.valueOf(payable_amount));*/
                    btn_pay.setText("PAY "+AppConstant.CURRENCY+payable_amount);
                    usefreebies(true);

                }else{

                   /* payable_amount=   payable_amount+BeuSalonsSharedPrefrence.getMyLoyaltyPoints();
                    txt_grand_total.setText(AppConstant.CURRENCY+ String.valueOf(payable_amount));*/
                    btn_pay.setText("PAY "+AppConstant.CURRENCY+payable_amount);
                    usefreebies(false);

                }
            }
        });

        cb_use_membership.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            //    createAppointment();
            }
        });

        txtShowDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                logShowDetailEvent();
                if (payOption==2){
                    if (cbUsePoints.isChecked()){
                        new ShowDetailsBillSummary(OnlinePaymentActivity.this,appointmentDetailResponse,true,true,false,false);
                    }else{
                        new ShowDetailsBillSummary(OnlinePaymentActivity.this,appointmentDetailResponse,true,false,false,false);
                    }


                }else{
                    if (cbUsePoints.isChecked()){
                        new ShowDetailsBillSummary(OnlinePaymentActivity.this,appointmentDetailResponse,false,true,false,false);
                    }else{
                        new ShowDetailsBillSummary(OnlinePaymentActivity.this,appointmentDetailResponse,false,false,false,false);
                    }
                }

            }
        });

    }

    private void setToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(getResources().getString(R.string.online_payment));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);

        }



    }




    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_cash_payment:
                payOption=1;
                txt_100.setText(appointmentDetailResponse.getData().getDiscountMessage());
                if(appointmentDetailResponse.getData().isOnlinePaymentDiscountAvailable()){

                  //  txt_100.setVisibility(View.VISIBLE);
                    //  txt_online_discount.setText("(-) "+AppConstant.CURRENCY+0);
                    //    linear_online.setVisibility(View.VISIBLE);
                }else{

                    //   txt_100.setVisibility(View.GONE);
                    //  linear_online.setVisibility(View.GONE);
                }
               // txt_100.setText(paymentResponse.getData().getDiscountMessage());
                Double saved=  appointmentDetailResponse.getData().getTotalSaved() +
                        appointmentDetailResponse.getData().getLoyalityPoints();
                if (saved > 0.0) {
                    txt_saved.setVisibility(View.VISIBLE);
                    txt_saved.setText("Saved "+AppConstant.CURRENCY+saved.intValue());
                    txt_saved.setBackgroundResource(R.drawable.discount_seletor);
                }else {
                    txt_saved.setVisibility(View.INVISIBLE);
                }

                payable_amount= appointmentDetailResponse.getData().getPayableAmount();

              //  txt_grand_total.setText(AppConstant.CURRENCY+ String.valueOf(payable_amount));
                btn_pay.setText("PAY "+AppConstant.CURRENCY+appointmentDetailResponse.getData().getPayableAmount());
               if (cbUsePoints.isChecked()){
                   usefreebies(true);
               }else usefreebies(false);

                break;

            case R.id.radio_razorpay:
                if (checked)
                    payOption= 2;
                Log.i(TAG, "i'm in razorpay");

                txt_100.setText(appointmentDetailResponse.getData().getDiscountMessage());

                if(appointmentDetailResponse.getData().isOnlinePaymentDiscountAvailable()){

                    //   txt_100.setVisibility(View.VISIBLE);
                    // linear_online.setVisibility(View.VISIBLE);
                    int discounted_price= (int)(0.10*appointmentDetailResponse.getData().getSubtotal());
                    payable_amount= appointmentDetailResponse.getData().getPayableAmountForOnlineDiscount();
//                    txt_grand_total.setText(AppConstant.CURRENCY+ String.valueOf(payable_amount));


                    if(appointmentDetailResponse.getData().getPayableAmount()>=200.00){



                        Double saved_online=  appointmentDetailResponse.getData().getTotalSaved()+
                                appointmentDetailResponse.getData().getLoyalityPoints()+appointmentDetailResponse.getData().getOnlineDiscount();

                        Log.i("maintohyahahoon", "value : "+ saved_online+ "  "+ saved_online.intValue()
                                + appointmentDetailResponse.getData().getTotalSaved()
                                +"  "+appointmentDetailResponse.getData().getLoyalityPoints() );

                        txt_saved.setText("Saved "+AppConstant.CURRENCY+saved_online.intValue());
                        txt_saved.setBackgroundResource(R.drawable.discount_seletor);
                        txt_saved.setVisibility(View.VISIBLE);

                        txt_freeBee_points.setText("Your Total freebee points are "+appointmentDetailResponse.getData().getUsableLoyalityPoints());

                        //  txt_online_discount.setText("(-) "+AppConstant.CURRENCY+100);

                        btn_pay.setText("PAY "+AppConstant.CURRENCY+String.valueOf(payable_amount));
                    }else{

                     //   cbUseThreading.setVisibility(View.GONE);

                        Double saved_online=  appointmentDetailResponse.getData().getTotalSaved() +
                                appointmentDetailResponse.getData().getLoyalityPoints();
                        if (saved_online > 0.0) {

                            txt_saved.setText("Saved "+AppConstant.CURRENCY+saved_online.intValue());
                            txt_saved.setBackgroundResource(R.drawable.discount_seletor);
                            txt_saved.setVisibility(View.VISIBLE);
                        }else {

                            txt_saved.setVisibility(View.INVISIBLE);
                        }
                        //  payable_amount= appointmentDetailResponse.getData().getPayableAmount();

//                        txt_grand_total.setText(AppConstant.CURRENCY+String.valueOf(payable_amount));

                        //   txt_online_discount.setText("(-) "+AppConstant.CURRENCY+0);

                        btn_pay.setText("PAY "+AppConstant.CURRENCY+String.valueOf(payable_amount));
                        txt_freeBee_points.setText("Your Total freebee points are "+appointmentDetailResponse.getData().getUsableLoyalityPoints());
                    }

                }else{

                    //  txt_100.setVisibility(View.GONE);
                    //        linear_online.setVisibility(View.GONE);

                    Double saved_online=  appointmentDetailResponse.getData().getTotalSaved() +
                            appointmentDetailResponse.getData().getLoyalityPoints();


                    if (saved_online > 0.0) {

                        txt_saved.setText("Saved "+AppConstant.CURRENCY+saved_online.intValue());
                        txt_saved.setBackgroundResource(R.drawable.discount_seletor);
                        txt_saved.setVisibility(View.VISIBLE);
                    }else {

                        txt_saved.setVisibility(View.INVISIBLE);
                    }
                    txt_freeBee_points.setText("Your Total freebee points are "+appointmentDetailResponse.getData().getUsableLoyalityPoints());
                    payable_amount= appointmentDetailResponse.getData().getPayableAmount();
//                    txt_grand_total.setText(AppConstant.CURRENCY + String.valueOf(payable_amount));
                    btn_pay.setText("PAY "+AppConstant.CURRENCY + String.valueOf(payable_amount));

                    if (cbUsePoints.isChecked()){
                        usefreebies(true);
                    }else usefreebies(false);
                }
                break;
        }
    }


    public void fetchData(){

        SharedPreferences preferences= getSharedPreferences("userDetails", Context.MODE_PRIVATE);
        Log.e("Logging" , " testing AM ");
        if(preferences!=null){

            userId= preferences.getString("userId", null);
            accessToken= preferences.getString("accessToken", null);
            email= preferences.getString("emailId", null);
            mobile= preferences.getString("phoneNumber", null);
            Log.e("Logging" , " testing AM "+userId+" : "+accessToken+" : "+email+"appoint ment ID"+appt_id);
        }

        final AppointmentDetailPost appointmentDetailPost = new AppointmentDetailPost();
        appointmentDetailPost.setUserId(userId);
        appointmentDetailPost.setAccessToken(accessToken);
        appointmentDetailPost.setAppointmentId(appt_id);

        appointmentDetailPost.setPaymentMethod(5);

        Log.i(TAG, "value in the stuff yo uknow it: "+ userId+  "    "+accessToken+   "  "+appt_id);

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<AppointmentDetailResponse> call= apiInterface.apptDetail(appointmentDetailPost);
        call.enqueue(new Callback<AppointmentDetailResponse>() {
            @Override
            public void onResponse(Call<AppointmentDetailResponse> call, Response<AppointmentDetailResponse> response) {


                if(response.isSuccessful()){

                    if(response.body().isSuccess()){

                        Log.i(TAG, "value in the response stuff: "+ response.body().getData().getAppointmentId()+ " latitude: "+
                                response.body().getData().getLatitude()+ "   "+
                                response.body().getData().isOnlinePaymentDiscountAvailable());

                        appointmentDetailResponse = response.body();
                        appt_id= appointmentDetailResponse.getData().getAppointmentId();
                        subtotalTemp=appointmentDetailResponse.getData().getSubtotal();
                        int menu_price= 0;

                        Double sub_total= appointmentDetailResponse.getData().getSubtotal()-
                                appointmentDetailResponse.getData().getLoyalityPoints();
                        //    txt_total.setText(AppConstant.CURRENCY+ sub_total);

                        // txt_tax.setText(AppConstant.CURRENCY+ appointmentDetailResponse.getData().getTax());
                        if (payOption==2){
                            payable_amount=appointmentDetailResponse.getData().getPayableAmountForOnlineDiscount();

                        }else  payable_amount= appointmentDetailResponse.getData().getPayableAmount();

                        txt_grand_total.setText(AppConstant.CURRENCY+ payable_amount);
                        btn_pay.setText("PAY "+AppConstant.CURRENCY+payable_amount);
                      //  txt_100.setText(appointmentDetailResponse.getData().getDiscountMessage());
                        txt_freebee_se_tnc.setText(appointmentDetailResponse.getData().getFreebiesTerms());
                        txt_freeBee_points.setText("Your Total Freebie Points Are "+appointmentDetailResponse.getData().getUsableLoyalityPoints());

                        if(appointmentDetailResponse.getData().getPaid()){

                            Log.i(TAG, "I'm in onResponse paid pe hoon");
                            mContentView.setVisibility(View.VISIBLE);
                            mLoadingView.setVisibility(View.GONE);
                            btn_pay.setText("Ok");
                            btn_pay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(OnlinePaymentActivity.this, MainActivity.class));
                                    finish();
                                }
                            });
                        }else{
                            Log.i(TAG, "I'm in onResponse unpaid pe hoon");
                            mContentView.setVisibility(View.VISIBLE);
                            mLoadingView.setVisibility(View.GONE);
                            btn_pay.setText("PAY "+AppConstant.CURRENCY+ String.valueOf(payable_amount));
                            btn_pay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                   /* if(CheckConnection.isConnected(OnlinePaymentActivity.this)){
                                        mContentView.setVisibility(View.GONE);
                                        mLoadingView.setVisibility(View.VISIBLE);
                                        razorpay();
                                    }else{
                                        mContentView.setVisibility(View.VISIBLE);
                                        mLoadingView.setVisibility(View.GONE);
                                        Toast.makeText(OnlinePaymentActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                                    }*/



                                    if(payOption==1){

                                        //cash payment
                                        if(CheckConnection.isConnected(OnlinePaymentActivity.this)){
                                            mContentView.setVisibility(View.GONE);
                                            mLoadingView.setVisibility(View.VISIBLE);
                                            bookCashPayment("", payable_amount, 1);

                                        }else{
                                            mContentView.setVisibility(View.VISIBLE);
                                            mLoadingView.setVisibility(View.GONE);
                                            Toast.makeText(OnlinePaymentActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                                        }

                                    }else if(payOption==2){

                                        if(CheckConnection.isConnected(OnlinePaymentActivity.this)){
                                            mContentView.setVisibility(View.GONE);
                                            mLoadingView.setVisibility(View.VISIBLE);

                                            if (payable_amount>0)
                                                razorpay();
                                            else  bookCashPayment("", payable_amount, 1);

                                        }else{
                                            mContentView.setVisibility(View.VISIBLE);
                                            mLoadingView.setVisibility(View.GONE);
                                            Toast.makeText(OnlinePaymentActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                                        }

                                    }else {

                                        Log.i(TAG, "I'm in payOption others");
                                    }
                                }
                            });
                        }
                        txt_freeBee_points.setText("Your Total freebee points are "+appointmentDetailResponse.getData().getUsableLoyalityPoints());
                        mContentView.setVisibility(View.VISIBLE);
                        mLoadingView.setVisibility(View.INVISIBLE);

                    }else{
                        //success : false hai toh
                        //// TODO: 3/25/2017 toast dikhao
                        Log.e(TAG,"success fail pe aya hai");
                        finish();
                    }
                }else{
                    //// TODO: 3/8/2017  handle karo isseh
                    Log.e(TAG,"retrofit ke issuccessful fail pe aya hai");
                    finish();
                }




            }

            @Override
            public void onFailure(Call<AppointmentDetailResponse> call, Throwable t) {

                Log.i(TAG, "I'm in onFailure" + t.getMessage()+ "  "+ t.getCause()+ "  "+t.getStackTrace());

                if(data_retry<3){
                    fetchData();
                    data_retry++;
                }else{
                    Toast.makeText(OnlinePaymentActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    finish();
                }

            }
        });

    }

    public void razorpay(){

        Log.i("investigaaaaaaaaa", "value in the stuff: "+ appointmentDetailResponse.getData().getPayableAmount()+
                "      "+ appt_id);

        final Activity activity = this;
        final Checkout checkout = new Checkout();
        checkout.setImage(R.drawable.ic_beu_razorpay);
        try {
            JSONObject options = new JSONObject();


            options.put("name", "Be U Salons");
            options.put("description", "OrderID: "+ appt_id);
            //You can omit the image option to fetch the image from dashboard
//            options.put("image", "https://rzp-mobile.s3.amazonaws.com/images/rzp.png");

            options.put("currency", "INR");
            options.put("appointmentId", appt_id); //parlor apt id is to be shown

            double amount= 100* payable_amount;
            options.put("amount", amount);

            //theme color chnage
            JSONObject theme= new JSONObject();
            theme.put("color", "#d2232a");
            options.put("theme", theme);

            JSONObject notes= new JSONObject();
            notes.put("appointmentId", appt_id);
            options.put("notes", notes);

            //prefill
            JSONObject preFill = new JSONObject();
            preFill.put("email", email);
            preFill.put("contact", mobile);
            options.put("prefill", preFill);

            checkout.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String s) {

        Log.i(TAG, "onPaymentSuccess: "+s);

        Intent intent= new Intent(this, PaymentFailSuccessActivity.class);
        Bundle bundle= new Bundle();
        bundle.putString("appointmentId", appt_id);
        bundle.putString("razorPayKey", s);                 //razorpay key
        bundle.putDouble("amount", payable_amount);
        bundle.putString("salonAppointmentId", String.valueOf(appointmentDetailResponse.getData().getParlorAppointmentId()));
        bundle.putString("salonName", appointmentDetailResponse.getData().getParlorName());
        bundle.putString("salonAddress", appointmentDetailResponse.getData().getParlorAddress());
        bundle.putDouble("latitude", appointmentDetailResponse.getData().getLatitude());
        bundle.putDouble("longitude", appointmentDetailResponse.getData().getLongitude());
        bundle.putString("startsAt", appointmentDetailResponse.getData().getStartsAt());
        bundle.putString("appointment_post", new Gson().toJson(appointmentPost));
        bundle.putString("cashbackmessage", appointmentDetailResponse.getData().getAlertMessage());
        bundle.putBoolean("isOnline", true);
        if (cbUsePoints.isChecked()){
            bundle.putInt("useFreebie",1);
        }else   bundle.putInt("useFreebie",0);
        bundle.putBoolean("isSuccess", true);
        bundle.putBoolean("isNotification", true);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    @Override
    public void onPaymentError(int i, String s) {

        Log.i(TAG, "onPaymentFailure: "+s);

        Intent intent= new Intent(this, PaymentFailSuccessActivity.class);
        Bundle bundle= new Bundle();
        bundle.putString("appointmentId", appt_id);
        bundle.putString("razorPayKey", s);                     //razorpay key
        bundle.putDouble("amount", payable_amount);                 //payable amt
        bundle.putString("salonAppointmentId", String.valueOf(appointmentDetailResponse.getData().getParlorAppointmentId()));
        bundle.putString("salonName", appointmentDetailResponse.getData().getParlorName());
        bundle.putString("salonAddress", appointmentDetailResponse.getData().getParlorAddress());
        bundle.putDouble("latitude", appointmentDetailResponse.getData().getLatitude());
        bundle.putDouble("longitude", appointmentDetailResponse.getData().getLongitude());
        bundle.putString("startsAt", appointmentDetailResponse.getData().getStartsAt());
        bundle.putBoolean("isOnline", true);
        bundle.putBoolean("isSuccess", false);
        bundle.putBoolean("isNotification", true);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

       // mContentView.setVisibility(View.GONE);
      //  mLoadingView.setVisibility(View.VISIBLE);
       // fetchData();
        if (cbUsePoints.isChecked()) {
            usefreebies(true);
        }
        else usefreebies(false);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:     //@Todo: handle isko bhi karo
               /* startActivity(new Intent(OnlinePaymentActivity.this, MainActivity.class));
                finish();*/
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }


    private void usefreebies(boolean isUse){
        mContentView.setVisibility(View.GONE);
        mLoadingView.setVisibility(View.VISIBLE);
        SharedPreferences preferences= getSharedPreferences("userDetails", Context.MODE_PRIVATE);
        Log.e("Logging" , " testing AM ");
        if(preferences!=null){

            userId= preferences.getString("userId", null);
            accessToken= preferences.getString("accessToken", null);
            email= preferences.getString("emailId", null);
            mobile= preferences.getString("phoneNumber", null);
            Log.e("Logging" , " testing AM "+userId+" : "+accessToken+" : "+email+"appoint ment ID"+appt_id);
        }
        UseFreeBiesPost useFreeBiesPost=new UseFreeBiesPost();
        useFreeBiesPost.setAccessToken(accessToken);
        useFreeBiesPost.setUserId(userId);
        useFreeBiesPost.setAppointmentId(appt_id);
useFreeBiesPost.setUseLoyalityPoints(isUse);
        if (payOption==2){
            useFreeBiesPost.setPaymentMethod(5);
        }else useFreeBiesPost.setPaymentMethod(1);

        Retrofit retrofit = ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<AppointmentDetailResponse> call= apiInterface.useLoyaltyPoints(useFreeBiesPost);
        call.enqueue(new Callback<AppointmentDetailResponse>() {
            @Override
            public void onResponse(Call<AppointmentDetailResponse> call, Response<AppointmentDetailResponse> response) {
                appointmentDetailResponse = response.body();
                mContentView.setVisibility(View.VISIBLE);
                mLoadingView.setVisibility(View.GONE);
                appt_id= appointmentDetailResponse.getData().getAppointmentId();
                subtotalTemp=appointmentDetailResponse.getData().getSubtotal();
                int menu_price= 0;

                txt_suggetion.setText(appointmentDetailResponse.getData().getSuggestion());
                txt_100.setText(appointmentDetailResponse.getData().getDiscountMessage());
                Double sub_total= appointmentDetailResponse.getData().getSubtotal()-
                        appointmentDetailResponse.getData().getLoyalityPoints();
                //    txt_total.setText(AppConstant.CURRENCY+ sub_total);
                txt_freeBee_points.setText("Your Total freebee points are "+appointmentDetailResponse.getData().getUsableLoyalityPoints());
                // txt_tax.setText(AppConstant.CURRENCY+ appointmentDetailResponse.getData().getTax());
                if (payOption==2){
                    payable_amount=appointmentDetailResponse.getData().getPayableAmount();

                }else  payable_amount= appointmentDetailResponse.getData().getPayableAmount();
                txt_freeBee_points.setText("Your Total freebee points are "+appointmentDetailResponse.getData().getUsableLoyalityPoints());
                txt_grand_total.setText(AppConstant.CURRENCY+ payable_amount);
                btn_pay.setText("PAY "+AppConstant.CURRENCY+payable_amount);
                //  txt_100.setText(appointmentDetailResponse.getData().getDiscountMessage());
                txt_freebee_se_tnc.setText(appointmentDetailResponse.getData().getFreebiesTerms());


                if(appointmentDetailResponse.getData().getPaid()){
                    mContentView.setVisibility(View.VISIBLE);
                    mLoadingView.setVisibility(View.GONE);
                    Log.i(TAG, "I'm in onResponse paid pe hoon");

                    btn_pay.setText("Ok");
                    btn_pay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            startActivity(new Intent(OnlinePaymentActivity.this, MainActivity.class));
                            finish();
                        }
                    });
                }else {
                    Log.i(TAG, "I'm in onResponse unpaid pe hoon");
                    mContentView.setVisibility(View.VISIBLE);
                    mLoadingView.setVisibility(View.GONE);
                    btn_pay.setText("PAY " + AppConstant.CURRENCY + String.valueOf(payable_amount));
                    btn_pay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                          /*  if (CheckConnection.isConnected(OnlinePaymentActivity.this)) {
                                mContentView.setVisibility(View.GONE);
                                mLoadingView.setVisibility(View.VISIBLE);
                                razorpay();
                            } else {
                                mContentView.setVisibility(View.VISIBLE);
                                mLoadingView.setVisibility(View.GONE);
                                Toast.makeText(OnlinePaymentActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                            }*/
                            if(payOption==1){

                                //cash payment
                                if(CheckConnection.isConnected(OnlinePaymentActivity.this)){
                                    mContentView.setVisibility(View.GONE);
                                    mLoadingView.setVisibility(View.VISIBLE);
                                    bookCashPayment("", payable_amount, 1);

                                }else{
                                    mContentView.setVisibility(View.VISIBLE);
                                    mLoadingView.setVisibility(View.GONE);
                                    Toast.makeText(OnlinePaymentActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                                }

                            }else if(payOption==2){

                                if(CheckConnection.isConnected(OnlinePaymentActivity.this)){
                                    mContentView.setVisibility(View.GONE);
                                    mLoadingView.setVisibility(View.VISIBLE);

                                    if (payable_amount>0)
                                        razorpay();
                                    else  bookCashPayment("", payable_amount, 1);

                                }else{
                                    mContentView.setVisibility(View.VISIBLE);
                                    mLoadingView.setVisibility(View.GONE);
                                    Toast.makeText(OnlinePaymentActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                                }

                            }else {

                                Log.i(TAG, "I'm in payOption others");
                            }
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<AppointmentDetailResponse> call, Throwable t) {

            }
        });


    }


    //cash payment method   --- for razor pay go to order summary activity
    public void bookCashPayment(String payment_method_key,final Double amount, int payment_method_id){

        PaymentSuccessPost post= new PaymentSuccessPost();

        post.setRazorpay_payment_id(payment_method_key);
        post.setAmount(amount);
        post.setPaymentMethod(payment_method_id);

        Log.i("BillSummar", "value in payable amt bookcashpayment api: "+ amount);

        post.setAppointmentId(appt_id);
        post.setAccessToken(accessToken);
        post.setUserId(userId);
        if (cbUsePoints.isChecked()){
            post.setUseLoyalityPoints(1);
        }else  post.setUseLoyalityPoints(0);
        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<PaymentSuccessResponse> call= apiInterface.bookCapturePayment(post);
        call.enqueue(new Callback<PaymentSuccessResponse>() {
            @Override
            public void onResponse(Call<PaymentSuccessResponse> call, Response<PaymentSuccessResponse> response) {

                Log.i(TAG, "i'm in cash payment success response ");
                if(response.isSuccessful()){
                    if(response.body().getSuccess()){
                       /* logCashPaymentEvent();
                        logEventAddedPaymentInfo();
                        logBookingConfirmedEvent();
                        getCartData();*/
                        Log.i(TAG, "value in appt id: "+appointmentDetailResponse.getData().getAppointmentId());
                        Intent intent= new Intent(OnlinePaymentActivity.this, PaymentFailSuccessActivity.class);
                        Bundle bundle= new Bundle();
                        bundle.putString("appointmentId", appt_id);
                        bundle.putString("razorPaykey", "");
                        bundle.putDouble("amount", amount);
                        bundle.putString("salonAppointmentId", String.valueOf(appointmentDetailResponse.getData().getParlorAppointmentId()));
                        bundle.putString("salonName", appointmentDetailResponse.getData().getParlorName());
                        bundle.putString("salonAddress", appointmentDetailResponse.getData().getParlorAddress());
                        bundle.putDouble("latitude", appointmentDetailResponse.getData().getLatitude());
                        bundle.putDouble("longitude", appointmentDetailResponse.getData().getLongitude());
                        bundle.putString("startsAt", appointmentDetailResponse.getData().getStartsAt());
                        bundle.putBoolean("isOnline", false);
                        bundle.putBoolean("isSuccess", true);
                        bundle.putString("cashbackmessage", appointmentDetailResponse.getData().getAlertMessage());
                        bundle.putBoolean("isNotification", false);
                        if (cbUsePoints.isChecked()){
                            bundle.putInt("useFreebie",1);
                        }else   bundle.putInt("useFreebie",0);

                        intent.putExtras(bundle);
                        startActivity(intent);
                        finish();
                    }else{
//                        logBookingCancelledEvent();
                        Log.i(TAG, "I'm in the  success false pe ...");
                        Intent intent= new Intent(OnlinePaymentActivity.this, PaymentFailSuccessActivity.class);Bundle bundle= new Bundle();
                        bundle.putString("appointmentId", appt_id);
                        bundle.putString("razorPaykey", "");
                        bundle.putDouble("amount", amount);
                        bundle.putString("salonAppointmentId", String.valueOf(appointmentDetailResponse.getData().getParlorAppointmentId()));
                        bundle.putString("salonName", appointmentDetailResponse.getData().getParlorName());
                        bundle.putString("salonAddress", appointmentDetailResponse.getData().getParlorAddress());
                        bundle.putDouble("latitude", appointmentDetailResponse.getData().getLatitude());
                        bundle.putDouble("longitude", appointmentDetailResponse.getData().getLongitude());
                        bundle.putString("startsAt", appointmentDetailResponse.getData().getStartsAt());
                        bundle.putBoolean("isOnline", false);
                        bundle.putBoolean("isSuccess", false);
                        if (cbUsePoints.isChecked()){
                            bundle.putInt("useFreebie",1);
                        }else   bundle.putInt("useFreebie",0);
                        bundle.putBoolean("isNotification", false);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        finish();
                    }
                }else{
//                    logBookingCancelledEvent();
                    Log.i(TAG, "I'm in the  success false pe ...");
                    Intent intent= new Intent(OnlinePaymentActivity.this, PaymentFailSuccessActivity.class);Bundle bundle= new Bundle();
                    bundle.putString("appointmentId", appt_id);
                    bundle.putString("razorPaykey", "");
                    bundle.putDouble("amount", amount);
                    bundle.putString("salonAppointmentId", String.valueOf(appointmentDetailResponse.getData().getParlorAppointmentId()));
                    bundle.putString("salonName", appointmentDetailResponse.getData().getParlorName());
                    bundle.putString("salonAddress", appointmentDetailResponse.getData().getParlorAddress());
                    bundle.putDouble("latitude", appointmentDetailResponse.getData().getLatitude());
                    bundle.putDouble("longitude", appointmentDetailResponse.getData().getLongitude());
                    bundle.putString("startsAt", appointmentDetailResponse.getData().getStartsAt());
                    bundle.putBoolean("isOnline", false);
                    bundle.putBoolean("isSuccess", false);
                    bundle.putBoolean("isNotification", false);
                    if (cbUsePoints.isChecked()){
                        bundle.putInt("useFreebie",1);
                    }else   bundle.putInt("useFreebie",0);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<PaymentSuccessResponse> call, Throwable t) {
//                logBookingCancelledEvent();

                //Todo: inform the user
                Toast.makeText(OnlinePaymentActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                mContentView.setVisibility(View.VISIBLE);
                mLoadingView.setVisibility(View.INVISIBLE);
                Log.i(TAG, "i'm in payment failure response ");
            }
        });
    }


}
