package com.beusalons.android.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.MainActivity;
import com.beusalons.android.Model.Coupon.CouponCode;
import com.beusalons.android.R;

import org.greenrobot.eventbus.EventBus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.view.View.GONE;
import static com.beusalons.android.Model.Coupon.CouponCode.COUPON;
import static com.beusalons.android.Model.Coupon.CouponCode.EARN_MORE_COUPON;

/**
 * Created by Ajay on 12/5/2017.
 */

public class AdapterCoupon extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity activity;
    private List<CouponCode> list;
    private boolean fromService;

    public AdapterCoupon(Activity context, List<CouponCode> list, boolean fromService) {
        this.activity=context;
        this.list=list;
        this.fromService= fromService;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType){
            case COUPON:
                view= LayoutInflater.from(activity).inflate(R.layout.row_for_coupon_code, parent, false);
                return new AdapterCoupon.MyViewHolder(view);
            case EARN_MORE_COUPON:
                view= LayoutInflater.from(activity).inflate(R.layout.row_earn_more, parent, false);
                return new AdapterCoupon.FooterViewHolder(view);
            default:
                view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_default, parent, false);
                return new DefaultViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder,  int position) {
        final int pos = position;

        switch (list.get(pos).getType()) {
            case COUPON:

                LinearLayout linear_= ((MyViewHolder)holder).linear_;
                
                TextView txtView_title_name, txtView_description,
                        txtView_code_,txtView_use_nd_save, txt_expiry;
                LinearLayout linear_copy, linear_inactive1, linear_inactive2, linear_active;
                txtView_title_name =  linear_.findViewById(R.id.txtView_title_name);
                txtView_description =  linear_.findViewById(R.id.txtView_description);
                txtView_code_ =  linear_.findViewById(R.id.txtView_code_);
                txtView_use_nd_save= linear_.findViewById(R.id.txtView_use_nd_save);
                txt_expiry= linear_.findViewById(R.id.txt_expiry);

                linear_copy = linear_.findViewById(R.id.linear_copy);
                linear_active= linear_.findViewById(R.id.linear_active);
                linear_inactive1= linear_.findViewById(R.id.linear_inactive1);
                linear_inactive2= linear_.findViewById(R.id.linear_inactive2);

                Log.i("sizeoflist", "in the cupon");
                txtView_title_name.setText(list.get(position).getCouponTitle());
                txtView_description.setText(list.get(position).getCouponDescription());

                if (fromService) {
                    txtView_use_nd_save.setVisibility(View.VISIBLE);
                    String useNdSave = "<font color='#000000'>Use and Save</font>" + AppConstant.CURRENCY + list.get(position).getNewAmount();
                    txtView_use_nd_save.setText(fromHtml(useNdSave));
                } else {
                    txtView_use_nd_save.setVisibility(View.INVISIBLE);
                }

                if(!list.get(position).isActive()){

                    linear_active.setBackgroundColor(Color.parseColor("#59000000"));
                    linear_inactive1.setVisibility(View.VISIBLE);
                    linear_inactive2.setVisibility(View.VISIBLE);
                    linear_inactive2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ((MainActivity)activity).setBottomFromOutSide(2);
                        }
                    });
                }else {
                    linear_active.setBackgroundColor(ContextCompat.getColor(linear_.getContext(),
                            R.color.white));
                    linear_inactive1.setVisibility(View.VISIBLE);
                    linear_inactive2.setVisibility(View.GONE);

                    txtView_code_.setText("Code: " + list.get(position).getCode());
                    linear_copy.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (fromService) {

                                EventBus.getDefault().post(list.get(pos));
                                Toast.makeText(activity, "Coupon Copied Press Apply For Execute", Toast.LENGTH_SHORT).show();

                            } else {

                                int sdk = android.os.Build.VERSION.SDK_INT;
                                if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                                    android.text.ClipboardManager clipboard = (android.text.ClipboardManager) (activity).getSystemService(Context.CLIPBOARD_SERVICE);
                                    clipboard.setText(list.get(pos).getCode());
                                } else {
                                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) (activity).getSystemService(Context.CLIPBOARD_SERVICE);
                                    android.content.ClipData clip = android.content.ClipData.newPlainText("text label",
                                            list.get(pos).getCode());
                                    clipboard.setPrimaryClip(clip);
                                }

                                Toast.makeText(activity, "Coupon Copied", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    txt_expiry.setTypeface(null, Typeface.ITALIC);
                    if (list.get(pos).getExpiry()!=null)
                    txt_expiry.setText("Expiry: " + formatDateTime(list.get(pos).getExpiry()));
                }
                break;
            case EARN_MORE_COUPON:
                Log.i("sizeoflist", "in the earn more");
                ((FooterViewHolder) holder).txt_earn_more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((MainActivity)activity).setBottomFromOutSide(2);
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount() {

        if(list!=null &&
                list.size()>0)
            return list.size();
        return 0;
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    @Override
    public int getItemViewType(int position) {
        if(list!=null &&
                list.size()>0){
            Log.i("sizeoflist", "view type: " + list.get(position).getType());
            return list.get(position).getType();
        }

        return 0;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout linear_;
        public MyViewHolder(View itemView) {
            super(itemView);
            linear_= (LinearLayout)itemView;
        }

    }

    class FooterViewHolder extends RecyclerView.ViewHolder {

        TextView txt_earn_more;
        public FooterViewHolder(View itemView) {
            super(itemView);
            txt_earn_more=(TextView) itemView.findViewById(R.id.txt_earn_more);
        }
    }

    public class DefaultViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout linear;
        public DefaultViewHolder(View itemView) {
            super(itemView);
            linear= (LinearLayout)itemView;
        }
    }

    private String formatDateTime(String str_date){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        Date date_format=null;
        try {
            date_format = sdf.parse(str_date);
            Calendar cal= Calendar.getInstance();       //creating calendar instance
            cal.setTime(date_format);
            cal.add(Calendar.HOUR_OF_DAY, 5);
            cal.add(Calendar.MINUTE, 30);

            date_format= cal.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("dd/MM/yyyy").format(date_format);
    }
}
