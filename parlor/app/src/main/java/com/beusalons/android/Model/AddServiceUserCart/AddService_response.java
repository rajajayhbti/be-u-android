package com.beusalons.android.Model.AddServiceUserCart;

/**
 * Created by Ajay on 2/23/2018.
 */

public class AddService_response {
   private boolean success;
   private String data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}
