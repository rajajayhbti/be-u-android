package com.beusalons.android.Model.Deal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ajay on 12/29/2016.
 */

public class DealsTypes implements Serializable{


    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("loyalityPoints")
    @Expose
    private String loyalityPoints;

    @SerializedName("frequencyFree")
    @Expose
    private Integer frequencyFree;

    @SerializedName("frequencyRequired")
    @Expose
    private String frequencyRequired;

    public String getFrequencyRequired() {
        return frequencyRequired;
    }

    public void setFrequencyRequired(String frequencyRequired) {
        this.frequencyRequired = frequencyRequired;
    }

    public Integer getFrequencyFree() {
        return frequencyFree;
    }

    public void setFrequencyFree(Integer frequencyFree) {
        this.frequencyFree = frequencyFree;
    }

    public String getLoyalityPoints() {
        return loyalityPoints;
    }

    public void setLoyalityPoints(String loyalityPoints) {
        this.loyalityPoints = loyalityPoints;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
