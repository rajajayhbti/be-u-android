package com.beusalons.android.Model;

/**
 * Created by myMachine on 1/19/2017.
 */

public class ParlorImagesModel {

    private String _id;
    private String imageUrl;

    private String appImageUrl;

    public String getAppImageUrl() {
        return appImageUrl;
    }

    public void setAppImageUrl(String appImageUrl) {
        this.appImageUrl = appImageUrl;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}