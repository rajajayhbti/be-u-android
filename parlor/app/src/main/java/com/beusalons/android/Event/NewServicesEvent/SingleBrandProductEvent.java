package com.beusalons.android.Event.NewServicesEvent;

import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Service;

/**
 * Created by myMachine on 7/14/2017.
 */

public class SingleBrandProductEvent {

    private Service service;

    public SingleBrandProductEvent(Service service){
        this.service= service;
    }

    public Service getService() {
        return service;
    }
}
