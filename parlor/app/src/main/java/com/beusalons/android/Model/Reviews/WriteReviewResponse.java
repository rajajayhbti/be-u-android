package com.beusalons.android.Model.Reviews;

/**
 * Created by myMachine on 11/29/2016.
 */

public class WriteReviewResponse {

    private Boolean success;
    private String data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}
