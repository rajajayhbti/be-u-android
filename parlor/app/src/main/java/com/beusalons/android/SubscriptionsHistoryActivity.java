package com.beusalons.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Adapter.AdapterSubscriptionHistory;
import com.beusalons.android.Model.SubscriptionHistory.Month;
import com.beusalons.android.Model.SubscriptionHistory.Subscription_response;
import com.beusalons.android.Model.Subscription_post;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Ajay on 2/26/2018.
 */

public class SubscriptionsHistoryActivity  extends AppCompatActivity{
    RecyclerView recycler_history;

    AdapterSubscriptionHistory adapterSubscriptionHistory;
    ImageView imgSubscriber;
    TextView txt_subs_user,txtamount,txt_subs_name;
    private List<Month> monthList;
    private String annualBalane;
    View progress_;
    LinearLayout ll_container;
    LinearLayout linear_bal;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_history);
        setToolBar();
        recycler_history=findViewById(R.id.recycler_history);
        imgSubscriber=findViewById(R.id.img_subs);
        txt_subs_user=findViewById(R.id.txt_subs_user);
        txt_subs_name=findViewById(R.id.txt_subs_name);
        txtamount=findViewById(R.id.txtamount);
        progress_=findViewById(R.id.progress_);
        ll_container=findViewById(R.id.ll_container);
        monthList=new ArrayList();
        linear_bal=findViewById(R.id.linear_bal);
        LinearLayoutManager layoutManager= new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recycler_history .setLayoutManager(layoutManager);
        adapterSubscriptionHistory = new AdapterSubscriptionHistory(SubscriptionsHistoryActivity.this,monthList);
        recycler_history.setAdapter(adapterSubscriptionHistory);
        inItView();
    }


    private void inItView(){
        //initialize the views with shared preference
        SharedPreferences preferences= getSharedPreferences("userDetails", Context.MODE_PRIVATE);
        if(preferences!=null){
            String name= preferences.getString("name", "Name");
            String profilePic= preferences.getString("profilePic", "");
            txt_subs_user.setText(name);


            try{
                Glide.with(this).load(profilePic).apply(RequestOptions.circleCropTransform()).into(imgSubscriber);

            }catch (Exception e){
                e.printStackTrace();
            }


        }
        fetchData();
    }
    private void fetchData(){
        progress_.setVisibility(View.VISIBLE);
        ll_container.setVisibility(View.GONE);
        linear_bal.setVisibility(View.GONE);
        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface=retrofit.create(ApiInterface.class);
        Subscription_post userCartPost=new Subscription_post();
        userCartPost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        userCartPost.setUserId(BeuSalonsSharedPrefrence.getUserId());
//        userCartPost.setUserId("593a34132555a16d0e2b295a");
//        userCartPost.setAccessToken("04d5ae978cd21d99528cc5ff274da3ba823b2ab2");
        Call<Subscription_response> call=apiInterface.getSubscriptionHistory(userCartPost);
        call.enqueue(new Callback<Subscription_response>() {
            @Override
            public void onResponse(Call<Subscription_response> call, Response<Subscription_response> response) {
                if (response.isSuccessful()){
                    progress_.setVisibility(View.GONE);
                      if (response.body().getSuccess()){

                          ll_container.setVisibility(View.VISIBLE);

                          if (monthList.size()>0){
                              monthList.clear();
                          }
       if ( response.body().getData()!=null  && response.body().getData().size()>0){
                              linear_bal.setVisibility(View.VISIBLE);
                 for (int i=0;i<response.body().getData().get(0).getMonths().size();i++){
                      monthList.add(response.body().getData().get(0).getMonths().get(i));
                  }



                          annualBalane=response.body().getData().get(0).getAnnualBalance();
                            if (annualBalane!=null)
                                txtamount.setText(annualBalane);

                          txt_subs_name.setText("You Are A "+response.body().getData().get(0).getSubscriptionType()+" Subscriber");
                          adapterSubscriptionHistory.notifyDataSetChanged();
       }

                      }else{
                          Log.i("getSuccess", "false: ");

                      }

                }else if (!response.isSuccessful()){


                    Log.i("isSuccessful", "failure: ");

                }
            }

            @Override
            public void onFailure(Call<Subscription_response> call, Throwable t) {

                Log.i("failure", "failure: "+ t.getLocalizedMessage()+ "  "+t.getMessage()+"  "+ t.getCause());
            }
        });



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){

            getSupportActionBar().setTitle(getResources().getString(R.string.subscription_his));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }
    }
}
