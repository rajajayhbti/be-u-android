package com.beusalons.android.Adapter.NewServiceDeals.ServicesAdapter;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.newServiceDeals.NewCombo.Service_;
import com.beusalons.android.R;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;

import java.util.List;

/**
 * Created by myMachine on 11-Dec-17.
 */

public class NewServiceAdapter extends RecyclerView.Adapter<NewServiceAdapter.ViewHolder> {

    private List<Service_> list;
    private boolean has_child;

    private double price, menu_price;
    private int service_code;
    private String service_id, service_name;

    public double getPrice(){
        return price;
    }
    public double getMenu_price(){
        return menu_price;
    }
    public int getService_code(){
        return service_code;
    }
    public String getService_id(){
        return service_id;
    }
    public String getService_name(){
        return service_name;
    }

    public interface ServiceListener{
        void onClick(int index);
    }
    private ServiceListener listener;
    public void setListener(ServiceListener listener){
        this.listener= listener;
    }

    public NewServiceAdapter(){}

    public NewServiceAdapter(List<Service_> list, boolean has_child){
        this.list= list;
        this.has_child= has_child;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view_= LayoutInflater.from(parent.getContext()).
                inflate(R.layout.bottomsheet_items, null, false);
        return new ViewHolder(view_);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final int index= position;

        final LinearLayout linear_= holder.linear;
        LinearLayout linear_click= linear_.findViewById(R.id.linear_click);
        TextView txt_name= linear_.findViewById(R.id.txt_name);
        TextView txt_price= linear_.findViewById(R.id.txt_price);
        TextView txt_menu_price= linear_.findViewById(R.id.txt_menu_price);
        TextView txt_save_per= linear_.findViewById(R.id.txt_save_per);
        TextView txt_max_popular= linear_.findViewById(R.id.txt_max_popular);
        final RadioButton radio_= linear_.findViewById(R.id.radio_);

        txt_name.setText(list.get(position).getName());

        //salon types ke liye
        LinearLayout linear_types= linear_.findViewById(R.id.linear_types);
        TextView txt_red_price= linear_.findViewById(R.id.txt_red_price);
        TextView txt_red_save_per= linear_.findViewById(R.id.txt_red_save_per);
        TextView txt_blue_price= linear_.findViewById(R.id.txt_blue_price);
        TextView txt_blue_save_per= linear_.findViewById(R.id.txt_blue_save_per);
        TextView txt_green_price= linear_.findViewById(R.id.txt_green_price);
        TextView txt_green_save_per= linear_.findViewById(R.id.txt_green_save_per);
        if(list.get(position).getMenuPrice()==0){//that means deal mai hai

            if(has_child){
                linear_types.setVisibility(View.GONE);

                if(list.get(position).getLowest()!=null &&
                        !list.get(position).getLowest().equalsIgnoreCase("")){

                    txt_menu_price.setVisibility(View.GONE);
                    txt_save_per.setVisibility(View.GONE);
                    txt_price.setVisibility(View.VISIBLE);

                    String lowest_price= "<font color='#58595b'>Lowest Price </font>"+
                            "<font color='#3e780a'>₹"+list.get(position).getLowest()+"</font>";

                    Typeface type = Typeface.createFromAsset(linear_.getContext().getAssets(),"fonts/Lato-Regular.ttf");
                    txt_price.setTypeface(type);
                    txt_price.setTextSize(12);
                    txt_price.setText(fromHtml(lowest_price));
                }else{
                    txt_price.setVisibility(View.GONE);
                }


            }else{

                txt_menu_price.setVisibility(View.GONE);
                txt_save_per.setVisibility(View.GONE);
                txt_price.setVisibility(View.GONE);

                if(list.get(position).getParlorTypes()!=null &&
                        list.get(position).getParlorTypes().size()>0){

                    txt_red_price.setText("---");
                    txt_red_price.setTextColor(ContextCompat.getColor(linear_.getContext(), R.color.black_ninety));
                    txt_red_save_per.setVisibility(View.GONE);
                    txt_blue_price.setText("---");
                    txt_blue_price.setTextColor(ContextCompat.getColor(linear_.getContext(), R.color.black_ninety));
                    txt_blue_save_per.setVisibility(View.GONE);
                    txt_green_price.setText("---");
                    txt_green_price.setTextColor(ContextCompat.getColor(linear_.getContext(), R.color.black_ninety));
                    txt_green_save_per.setVisibility(View.GONE);

                    linear_types.setVisibility(View.VISIBLE);

                    for(int i=0;i<list.get(position).getParlorTypes().size();i++){

                        if(list.get(position).getParlorTypes().get(i).getType()==0){


                            if(list.get(position).getParlorTypes().get(i).getStartAt()>0){
                                txt_red_price.setText(AppConstant.CURRENCY+ list.get(position).getParlorTypes().get(i).getStartAt());
                                txt_red_price.setTextColor(ContextCompat.getColor(linear_.getContext(), R.color.colorGreen));

                            }

//                            txt_red_save_per.setVisibility(View.VISIBLE);
//                            txt_red_save_per.setText("Save "+list.get(position).getParlorTypes().get(i).getSave()+"%");


                        }else if(list.get(position).getParlorTypes().get(i).getType()==1){

                            if(list.get(position).getParlorTypes().get(i).getStartAt()>0){
                                txt_blue_price.setText(AppConstant.CURRENCY+list.get(position).getParlorTypes().get(i).getStartAt());
                                txt_blue_price.setTextColor(ContextCompat.getColor(linear_.getContext(), R.color.colorGreen));

                            }

//                            txt_blue_save_per.setVisibility(View.VISIBLE);
//                            txt_blue_save_per.setText("Save "+list.get(position).getParlorTypes().get(i).getSave()+"%");

                        }else if(list.get(position).getParlorTypes().get(i).getType()==2){

                            if(list.get(position).getParlorTypes().get(i).getStartAt()>0){
                                txt_green_price.setText(AppConstant.CURRENCY+list.get(position).getParlorTypes().get(i).getStartAt());
                                txt_green_price.setTextColor(ContextCompat.getColor(linear_.getContext(), R.color.colorGreen));

                            }

//                            txt_green_save_per.setVisibility(View.VISIBLE);
//                            txt_green_save_per.setText("Save "+list.get(position).getParlorTypes().get(i).getSave()+"%");

                        }


                    }


                }else
                    linear_types.setVisibility(View.GONE);


            }

        }else{      //salon mai hai

            linear_types.setVisibility(View.GONE);

            if(has_child){

                if(list.get(position).getLowest()!=null &&
                        !list.get(position).getLowest().equalsIgnoreCase("")){
                    txt_price.setVisibility(View.VISIBLE);
                    txt_menu_price.setVisibility(View.GONE);
                    txt_save_per.setVisibility(View.GONE);

                    String lowest_price= "<font color='#58595b'>Lowest Price </font>"+
                            "<font color='#3e780a'>₹"+list.get(position).getLowest()+"</font>";

                    Typeface type = Typeface.createFromAsset(linear_.getContext().getAssets(),"fonts/Lato-Regular.ttf");
                    txt_price.setTypeface(type);
                    txt_price.setTextSize(12);
                    txt_price.setText(fromHtml(lowest_price));
                }else{

                    txt_price.setVisibility(View.GONE);
                    txt_menu_price.setVisibility(View.GONE);
                    txt_save_per.setVisibility(View.GONE);
                }


            }else{

                txt_price.setVisibility(View.VISIBLE);


                //with tax
                int price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                        list.get(position).getPrice());
                int menu_price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                        list.get(position).getMenuPrice());
                txt_price.setText(AppConstant.CURRENCY+ price_);

                if(menu_price_>price_){
                    txt_menu_price.setVisibility(View.VISIBLE);
                    txt_save_per.setVisibility(View.VISIBLE);
                    txt_menu_price.setText(AppConstant.CURRENCY+ menu_price_);
                    txt_menu_price.setPaintFlags(txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    txt_save_per.setText(AppConstant.SAVE+" "+
                            (int) (100 -(((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                                    price_)*100)/
                                    menu_price_))+"%");
                    txt_save_per.setBackgroundResource(R.drawable.discount_seletor);
                }else{
                    txt_menu_price.setVisibility(View.GONE);
                    txt_save_per.setVisibility(View.GONE);
                }


            }



        }



        if(list.get(position).isPopularChoice()){

            txt_max_popular.setVisibility(View.VISIBLE);
            txt_max_popular.setText("Popular Choice");
            txt_max_popular.setBackgroundResource(R.drawable.shape_popular_choice);
        }else{

            if(list.get(position).isMaxSaving()){

                txt_max_popular.setVisibility(View.VISIBLE);
                txt_max_popular.setText("Max. Saving");
                txt_max_popular.setBackgroundResource(R.drawable.shape_max_saving);
            }else{
                txt_max_popular.setVisibility(View.GONE);
            }
        }

        if(list.get(index).isCheck()){

            radio_.setChecked(true);
            price= list.get(index).getPrice();
            menu_price= list.get(index).getMenuPrice();
            service_code= list.get(index).getServiceCode();
            service_id= list.get(index).getServiceId();
            service_name= list.get(index).getName();
        }else{

            radio_.setChecked(false);
        }

        linear_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(list.get(index).isCheck()){
                    radio_.setChecked(false);
                    list.get(index).setCheck(false);
                }else{

                    radio_.setChecked(true);
                    list.get(index).setCheck(true);

                    price= list.get(index).getPrice();
                    menu_price= list.get(index).getMenuPrice();
                    service_code= list.get(index).getServiceCode();
                    service_id= list.get(index).getServiceId();
                    service_name= list.get(index).getName();

                    if(listener!=null)
                        listener.onClick(index);
                }

                int selected_radio_= 0;
                for(int i=0;i<list.size();i++)
                    if(!list.get(i).isCheck())
                        selected_radio_++;

                if(selected_radio_== list.size()){

                    radio_.setChecked(true);
                    list.get(index).setCheck(true);
                }
                updateView(index);
            }
        });

    }

    private void updateView(int pos){

        for(int i=0; i<list.size();i++)
            if(i!=pos)
                list.get(i).setCheck(false);

        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {

        if(list!=null &&
                list.size()>0)
            return list.size();
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private LinearLayout linear;
        public ViewHolder(View itemView) {
            super(itemView);
            linear= (LinearLayout)itemView;
        }
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }



}
