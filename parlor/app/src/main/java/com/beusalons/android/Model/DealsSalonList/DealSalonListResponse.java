package com.beusalons.android.Model.DealsSalonList;

/**
 * Created by myMachine on 6/28/2017.
 */

public class DealSalonListResponse {

    private Boolean success;
    private Data data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
