package com.beusalons.android;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.beusalons.android.Adapter.NewServiceDeals.ServiceMembershipAdapter;
import com.beusalons.android.Event.NewServicesEvent.AddCartEvent;
import com.beusalons.android.Event.NewServicesEvent.AddServiceEvent;
import com.beusalons.android.Event.NewServicesEvent.PackageListEvent;
import com.beusalons.android.Event.NewServicesEvent.ServiceComboEvent;
import com.beusalons.android.Event.NewServicesEvent.SingleBrandProductEvent;
import com.beusalons.android.Event.NewServicesEvent.UpgradeEvent;
import com.beusalons.android.Fragment.ServiceFragments.ServiceSpecificDialogFragment;
import com.beusalons.android.Fragment.ServiceFragments.ServicesSpecificFragment;
import com.beusalons.android.Fragment.UserCartFragment;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.AddServiceUserCart.AddService_response;
import com.beusalons.android.Model.AddServiceUserCart.UserCart_post;
import com.beusalons.android.Model.HomePage.List_;
import com.beusalons.android.Model.SalonHome.HomeResponse;
import com.beusalons.android.Model.ServiceDialog.CustomAdapter;
import com.beusalons.android.Model.Share.ShareSalonService;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.Model.newServiceDeals.Department;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Category;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Data;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Service;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.ServiceResponse;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Slabs;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Task.MultipleServicesTask;
import com.beusalons.android.Task.UserCartTask;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.snappydb.DB;
import com.snappydb.DBFactory;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ServiceSpecificActivity extends AppCompatActivity {
    protected com.beusalons.android.Application nMyApplication;
    private static final String TAG="service_dialog";

    private CoordinatorLayout coordinator_;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    RelativeLayout relativeLayout_services;
    private WebView web_view;

    private TextView txt_item;

    View mLoadingView;
    private ServicesFragmentPager adapter;

    private List<String> str_tab_name= new ArrayList<>();

    private Data data;              //normal data'
    private Data deal_data;

    private String gender, departmentId, parlorId, department_name;
    private String department_service= "";                          //department_service se viewpager position
    private ToggleButton toggle_menu;

    private RecyclerView rec_memebership;

    private ServiceMembershipAdapter membershipAdapter;
    private HomeResponse homeResponse;
    private UserCart user_cart;
    private Slabs slabs;
    AppEventsLogger logger;
    private Spinner spinnerDepartMent;


    private boolean is_first_time= true;           //set menu for first time

    private FirebaseAnalytics mFirebaseAnalytics;

    private UserCart saved_cart;
    private TextView txt_location_name;
    public  ArrayList<Department> CustomListViewValuesArr = new ArrayList<Department>();
    CustomAdapter Cadapter;
    private boolean isSpinnerTouched = false;
    private int check=0;
    int index=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testr);
        nMyApplication = (com.beusalons.android.Application) getApplication();
        nMyApplication.onActivityCreated(this, savedInstanceState);
        logger = AppEventsLogger.newLogger(ServiceSpecificActivity.this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(ServiceSpecificActivity.this);

        Bundle bundle= getIntent().getExtras();
        if(bundle!=null && bundle.containsKey("department_id")){

            user_cart=  new Gson().fromJson(bundle.getString("user_cart", null), UserCart.class);
            user_cart.setCartType(AppConstant.SERVICE_TYPE);            //setting the usercart to service type

            parlorId= user_cart.getParlorId();
            departmentId= bundle.getString("department_id", null);
            department_name= bundle.getString("department_name", null);
            department_service= bundle.getString("department_service", "");

            gender= bundle.getString("gender", null);
            index=bundle.getInt("index",0);
            homeResponse= bundle.getString("membership", null)==null?
                    null: new Gson().fromJson(bundle.getString("membership"), HomeResponse.class);
        }else  finish();

        web_view= findViewById(R.id.web_view);
//homeResponse.getData().getDepartments().get(0).getDepartments().
//        setToolBar();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar()!=null){
            getSupportActionBar().setTitle(department_name);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        TextView txt_inclusive= findViewById(R.id.txt_inclusive);
        txt_inclusive.setTypeface(null, Typeface.ITALIC);

        txt_location_name= (TextView)findViewById(R.id.txt_service_name);
        spinnerDepartMent= (Spinner) findViewById(R.id.spinner_department);
        txt_location_name.setText(department_name);
        setSpinner();

        txt_location_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerDepartMent.performClick();

            }
        });

//        TextView txt_welcome= (TextView)findViewById(R.id.txt_welcome);
//        WebView webView_welcome= (WebView)findViewById(R.id.webView_welcome);
//        if(homeResponse!=null &&
//                homeResponse.getData().getWelcomeOffer()!=null &&
//                (homeResponse.getData().getWelcomeOffer().getWelcomeOffer()!=null ||
//                !homeResponse.getData().getWelcomeOffer().getWelcomeOffer().equalsIgnoreCase("")) ){
//            webView_welcome.setVisibility(View.VISIBLE);
//            webView_welcome.loadData(homeResponse.getData().getWelcomeOffer().getWelcomeOffer(), "text/html",
//                    "utf-8");
////            txt_welcome.setVisibility(View.VISIBLE);
////            txt_welcome.setText(fromHtml(homeResponse.getData().getWelcomeOffer().getWelcomeOffer()));
//        }else{
//            webView_welcome.setVisibility(View.GONE);
////            txt_welcome.setVisibility(View.GONE);
//
//        }


        mLoadingView=(View)findViewById(R.id.loading_for_services);
        coordinator_= (CoordinatorLayout)findViewById(R.id.coordinator_);

        txt_item= (TextView)findViewById(R.id.txt_item);

        relativeLayout_services=(RelativeLayout) findViewById(R.id.relativeLayout_services);
        //membership
        //    rec_memebership= (RecyclerView)findViewById(R.id.rec_memebership);
        //     rec_memebership.hasFixedSize();
        LinearLayoutManager layoutManager1= new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        //     rec_memebership.setLayoutManager(layoutManager1);
        /*if(homeResponse!=null && (homeResponse.getData().getMemberships()!=null && homeResponse.getData().getMemberships().size()>0)){

            rec_memebership.setVisibility(View.VISIBLE);
            membershipAdapter= new ServiceMembershipAdapter(this, homeResponse.getData().getMemberships());
            rec_memebership.setAdapter(membershipAdapter);
        }else{

            rec_memebership.setVisibility(View.GONE);
        }*/

        if (homeResponse!=null
                &&(homeResponse.getData().getSubscriptions()!=null&&homeResponse.getData().getSubscriptions().getList().size()>0)){
            LinearLayout linear_container=findViewById(R.id.linear_container);
            linear_container.removeAllViews();

            web_view.loadData(homeResponse.getData().getSubscriptions().getSubscriptionCount(),
                    "text/html", "utf-8");
            web_view.setBackgroundColor(Color.TRANSPARENT);

            for(int i=0;i<homeResponse.getData().getSubscriptions().getList().size();i++){

                CardView cardView_= (CardView) LayoutInflater.from(linear_container.getContext())
                        .inflate(R.layout.layout_subscription, linear_container, false);

                TextView txt_heading1= cardView_.findViewById(R.id.txt_heading1);
                TextView txt_heading2= cardView_.findViewById(R.id.txt_heading2);
                TextView txt_more= cardView_.findViewById(R.id.txt_more);
                TextView txt_subscribe= cardView_.findViewById(R.id.txt_subscribe);
                ImageView img_type= cardView_.findViewById(R.id.img_type);
                LinearLayout linear_points= cardView_.findViewById(R.id.linear_points);
                linear_points.removeAllViews();

                txt_heading1.setText(fromHtml(homeResponse.getData().getSubscriptions().getList().get(i).getHeading1HTML()));
                txt_heading2.setText(homeResponse.getData().getSubscriptions().getList().get(i).getHeading2());

                for(int j=0;j<homeResponse.getData().getSubscriptions().getList().get(i).getPoints().size();j++){

                    LinearLayout linear_points_= (LinearLayout) LayoutInflater.from(linear_container.getContext())
                            .inflate(R.layout.layout_points, linear_container, false);

                    TextView txt_name= linear_points_.findViewById(R.id.txt_name);
                    txt_name.setText(homeResponse.getData().getSubscriptions().getList().get(i).getPoints().get(j));

                    linear_points.addView(linear_points_);
                }


                txt_more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        view.getContext().startActivity(new Intent(view.getContext(), SubscriptionActivity.class));
                    }
                });

                final int pos= i;
                txt_subscribe.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        saveDataToCart(homeResponse.getData().getSubscriptions().getList().get(pos));
                    }
                });


                linear_container.addView(cardView_);
            }
        }


        Log.i("specificactiv", "value: "+ departmentId+ "  "+ gender + " "+ department_name);

        tabLayout= (TabLayout)findViewById(R.id.tab_services);
        viewPager= (ViewPager)findViewById(R.id.viewpager_);
        viewPager.setOffscreenPageLimit(0);

        LinearLayout linear_cart= (LinearLayout) findViewById(R.id.linear_cart);
        linear_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logViewCartServiceEvent();
                logViewCartServiceFireBaseEvent();
                UserCartFragment fragment= new UserCartFragment();
                Bundle bundle= new Bundle();
                bundle.putBoolean("has_data", true);
                bundle.putBoolean(UserCartFragment.FROM_SERVICE, true);
                bundle.putString(UserCartFragment.CART_DATA ,new Gson().toJson(user_cart, UserCart.class));
                bundle.putString(UserCartFragment.HOME_RESPONSE_DATA,new Gson().toJson(homeResponse, HomeResponse.class));
                fragment.setArguments(bundle);
                fragment.show(getSupportFragmentManager(), "user_cart");
            }
        });

        TextView txt_proceed= (TextView)findViewById(R.id.txt_proceed);
        txt_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                logProceedServiceEvent();
                logProceedServiceFireBaseEvent();
                Intent intent= new Intent(ServiceSpecificActivity.this, BookingSummaryActivity.class);
                intent.putExtra("user_cart" ,new Gson().toJson(user_cart, UserCart.class));
                intent.putExtra(BookingSummaryActivity.HOME_RESPONSE,new Gson().toJson(homeResponse, HomeResponse.class));
                startActivity(intent);
            }
        });

        toggle_menu= (ToggleButton) findViewById(R.id.toggle_deal_normal);
        toggle_menu.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                try{
                    if(!is_first_time){
                        setMenu(isChecked);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

//        fetchData(departmentId);
    }

    private void saveDataToCart(List_ data) {

        UserServices service = new UserServices();
        service.setName(data.getTitle());
        service.setSubscriptionId(data.getSubscriptionId());
        service.setDescription(data.getHeading2()+ " | Recommended: "+data.getRecommended());
        service.setPrice(data.getAmount());
        service.setType("subscription");
        service.setSubscription(true);
        service.setPrimary_key("subscription_"+data.getSubscriptionId());
        Toast.makeText(getApplicationContext(),"Subscription Added to Cart",Toast.LENGTH_SHORT).show();


        new Thread(new UserCartTask(this, user_cart, service, false, false)).start();


    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShareServiceEvent(ShareSalonService event) {

        Log.i("intheshareser", "in the page" + department_name+ " "+ departmentId + " "+
                viewPager.getCurrentItem());


        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Hey check this out! Services from Be U Salons   https://beusalons.app.link/a/key_live_lftOr7qepHyQDI7uVSmpCkndstebwh0V?page=SelectService&departmentId="+departmentId+"&gender="+gender+"&index="+viewPager.getCurrentItem()+"&name="+department_name+"&parlorId="+parlorId);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share Service"));

    }



    private void setSpinner(){
        Resources res = getResources();
        int selectionPosition = -1;

        // Now i have taken static values by loop.
        // For further inhancement we can take data by webservice / json / xml;
        for (int j=0;j<homeResponse.getData().getDepartments().size();j++){

            if (gender.equalsIgnoreCase(homeResponse.getData().getDepartments().get(j).getGender())){
                for (int i=0;i<homeResponse.getData().getDepartments().get(j).getDepartments().size();i++){
                    final Department sched = new Department();
                    sched.setDepartmentId(homeResponse.getData().getDepartments().get(j).getDepartments().get(i).getDepartmentId());
                    sched.setName(homeResponse.getData().getDepartments().get(j).getDepartments().get(i).getName());
                    sched.setImage(homeResponse.getData().getDepartments().get(j).getDepartments().get(i).getImage());
                    if (department_name.equalsIgnoreCase(homeResponse.getData().getDepartments().get(j).getDepartments().get(i).getName())){
                        spinnerDepartMent.setSelection(i);
                    }
                    CustomListViewValuesArr.add(sched);
                }
            }

        }


        // Create custom adapter object ( see below CustomAdapter.java )
        Cadapter = new CustomAdapter(ServiceSpecificActivity.this, R.layout.spinner_rows, CustomListViewValuesArr,res);

        // Set adapter to spinner
        spinnerDepartMent.setAdapter(Cadapter);
        spinnerDepartMent.setSelection(selectionPosition);



        spinnerDepartMent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                isSpinnerTouched = true;
                return false;
            }
        });

        spinnerDepartMent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
                // your code here

                // Get selected row data to show on screen
                //    String Company    = ((TextView) v.findViewById(R.id.company)).getText().toString();
                //   String CompanyUrl = ((TextView) v.findViewById(R.id.sub)).getText().toString();

                //   String OutputMsg = "Selected Company : \n\n"+Company+"\n"+CompanyUrl;
//               output.setText(OutputMsg);


                //        Toast.makeText(
                //              getApplicationContext(),homeResponse.getData().getDepartments().get(0).getDepartments().get(position).getName(), Toast.LENGTH_LONG).show();
                //
                if (++check > 1)  {
                    if (homeResponse.getData().getDepartments().size()==2) {
                        if (gender.equalsIgnoreCase("M")) {
                            txt_location_name.setText(homeResponse.getData().getDepartments().get(0).getDepartments().get(position).getName());
                            fetchData(homeResponse.getData().getDepartments().get(0).getDepartments().get(position).getDepartmentId());
                        } else {
                            txt_location_name.setText(homeResponse.getData().getDepartments().get(1).getDepartments().get(position).getName());
                            fetchData(homeResponse.getData().getDepartments().get(1).getDepartments().get(position).getDepartmentId());
                        }

                    }else{
                        txt_location_name.setText(homeResponse.getData().getDepartments().get(0).getDepartments().get(position).getName());
                        fetchData(homeResponse.getData().getDepartments().get(0).getDepartments().get(position).getDepartmentId());
                    }}else{
                    txt_location_name.setText(department_name);
                    fetchData(departmentId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    public void openCart(){

        try{
            DB snappyDB= DBFactory.open(ServiceSpecificActivity.this);

            saved_cart= null;
            if(snappyDB.exists(AppConstant.USER_CART_DB)) {
                saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
            }
            snappyDB.close();

            if(saved_cart!=null && saved_cart.getServicesList().size()>0){

                int quantity=0;
                if(saved_cart.getServicesList().size()==1){

                    quantity= saved_cart.getServicesList().get(0).getQuantity();
                    if(quantity==1)
                        txt_item.setText(quantity+ " Item In Cart");
                    else
                        txt_item.setText(quantity+ " Items In Cart");
                }
                else if(saved_cart.getServicesList().size()>1){

                    for(int i=0;i<saved_cart.getServicesList().size();i++){

                        quantity+= saved_cart.getServicesList().get(i).getQuantity();
                    }

                    txt_item.setText(quantity+ " Items In Cart");
                }else
                    txt_item.setText("0 Item In Cart");
            }else
                txt_item.setText("0 Item In Cart");

        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void fetchData(String departmentId){

        mLoadingView.setVisibility(View.VISIBLE);
        relativeLayout_services.setVisibility(View.GONE);
        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface= retrofit.create(ApiInterface.class);

        Call<ServiceResponse> call= apiInterface.getService(gender, departmentId, parlorId,BeuSalonsSharedPrefrence.getLatitude(),BeuSalonsSharedPrefrence.getLongitude());
        call.enqueue(new Callback<ServiceResponse>() {
            @Override
            public void onResponse(Call<ServiceResponse> call, Response<ServiceResponse> response) {

                if(response.isSuccessful()){

                    if(response.body().getSuccess()){
                        Log.i("stuffIdont", "i'm onResponse success");

                        data=null;
                        slabs= null;



                        data= response.body().getData();
                        slabs= response.body().getData().getSlabs();

                        //subtitle ka case handle kara
                        String first_subtitle="";
                        int first_subtitle_count=0;
                        for(int i=0; i<data.getCategories().size();i++){
                            for(int j=0; j<data.getCategories().get(i).getServices().size();j++){
                                if(data.getCategories().get(i).getServices().get(j).getSubTitle()!=null){

                                    if(first_subtitle_count==0){

                                        first_subtitle= data.getCategories().get(i).getServices().get(j).getSubTitle();

                                        if(!first_subtitle.equalsIgnoreCase("")){

                                            data.getCategories().get(i).getServices().get(j).setFirstSubtitleElement(true);
                                        }
                                        first_subtitle_count++;
                                    }

                                    if(!first_subtitle.equalsIgnoreCase(
                                            data.getCategories().get(i).getServices().get(j).getSubTitle())){

                                        first_subtitle= data.getCategories().get(i).getServices().get(j).getSubTitle();
                                        if(!first_subtitle.equalsIgnoreCase("")){

                                            data.getCategories().get(i).getServices().get(j).setFirstSubtitleElement(true);
                                        }
                                    }
                                }
                            }
                        }

                        //adding the deal data
                        deal_data= new Data();
                        for(int a =0; a<data.getCategories().size();a++){
                            Category category= new Category();
                            category.setName(data.getCategories().get(a).getName());
                            for(int b=0; b<data.getCategories().get(a).getServices().size();b++){

                                if(data.getCategories().get(a).getServices().get(b).getDealId()!=null){

                                    category.getServices().add(data.getCategories().get(a).getServices().get(b));
                                }
                            }
                            if(category.getServices().size()>0){
                                deal_data.getCategories().add(category);
                            }
                        }

                        //adding packages stuff to services list
                        for(int j=0;j<data.getCategories().size();j++){
                            Category category= new Category();
                            category.setName(data.getCategories().get(j).getName());
                            for(int i=0;i<data.getCategories().get(j).getPackages().size();i++){

                                Service services= new Service();

                                services.setName(data.getCategories().get(j).getPackages().get(i).getName());
                                services.setDealId(""+data.getCategories().get(j).getPackages().get(i).getDealId());
                                services.setParlorDealId(data.getCategories().get(j).getPackages().get(i).getParlorDealId());
                                services.setCategory(data.getCategories().get(j).getPackages().get(i).getDealType());  //package aa raha hai abhi ya aur koi bhi
                                services.setDescription(data.getCategories().get(j).getPackages().get(i).getDescription());
                                services.setShortDescription(data.getCategories().get(j).getPackages().get(i).getShortDescription());
                                services.setPrice(data.getCategories().get(j).getPackages().get(i).getPrice());
                                services.setMenuPrice(data.getCategories().get(j).getPackages().get(i).getMenuPrice());
                                services.setDealPrice(data.getCategories().get(j).getPackages().get(i).getDealPrice());
                                services.setDealType(data.getCategories().get(j).getPackages().get(i).getDealType());
                                services.setSelectors(data.getCategories().get(j).getPackages().get(i).getSelectors());
                                services.setNewCombo(data.getCategories().get(j).getPackages().get(i).getNewCombo());
                                services.setSlabId(data.getCategories().get(j).getPackages().get(i).getSlabId());
                                services.setFirstPackageElement(i==0);

                                data.getCategories().get(j).getServices().add(services);

                                if(deal_data.getCategories().size()>j){

                                    deal_data.getCategories().get(j).getServices().add(services);
                                }else{

                                    category.getServices().add(services);
                                }
                            }
                            if(category.getServices().size()>0){

                                deal_data.getCategories().add(category);
                            }
                        }


                        /*
                        * comment for set filter as per deal menu  default selection
                        * */

                        try{

                            setStartViewPager();
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        //initially jo user select karega woh tab khulega
//                        str_tab_name.clear();
//                        for(int i=0; i<data.getCategories().size();i++){
//
//                            str_tab_name.add(data.getCategories().get(i).getName());
//                        }
//
//                        adapter= new ServicesFragmentPager(getSupportFragmentManager(),
//                                ServiceSpecificActivity.this, data, str_tab_name);
//                        viewPager.setAdapter(adapter);
//                        viewPager.setCurrentItem(vpager_position);
//                        tabLayout.setupWithViewPager(viewPager);
//
//                        for (int i = 0; i < tabLayout.getTabCount(); i++) {
//                            TabLayout.Tab tab = tabLayout.getTabAt(i);
//                            tab.setCustomView(adapter.getTabView(i));
//                        }

//                        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//                            @Override
//                            public void onTabSelected(TabLayout.Tab tab) {
//
//                                TextView txt= (TextView) tab.getCustomView().findViewById(R.id.txt_tab_name);
//                                txt.setTextColor(ContextCompat
//                                        .getColor(ServiceSpecificActivity.this, R.color.colorPrimary));
//
//
//                            }
//
//                            @Override
//                            public void onTabUnselected(TabLayout.Tab tab) {
//
//
//                                TextView txt= (TextView) tab.getCustomView().findViewById(R.id.txt_tab_name);
//                                txt.setTextColor(ContextCompat
//                                        .getColor(ServiceSpecificActivity.this, R.color.colorPrimaryText));
//
//
//                            }
//
//                            @Override
//                            public void onTabReselected(TabLayout.Tab tab) {
//
//                            }
//                        });

                        relativeLayout_services.setVisibility(View.VISIBLE);
                        mLoadingView.setVisibility(View.GONE);


                    }else{
                        Log.i("stuffIdont", "i'm onResponse failure");
                    }

                }else{

                    Log.i("stuffIdont", "i'm retrofit failure :(");
                }


            }

            @Override
            public void onFailure(Call<ServiceResponse> call, Throwable t) {

                Log.i("stuffIdont", "i'm in failure: "+ t.getMessage()+ "   "+
                        t.getStackTrace()+ t.getCause()+ " "+ t.getLocalizedMessage());
            }
        });
    }

    public void setMenu(boolean isChecked){

        try {
            DB snappyDB = DBFactory.open(ServiceSpecificActivity.this);

            saved_cart = null;
            if (snappyDB.exists(AppConstant.USER_CART_DB)) {
                saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
            }
            snappyDB.close();

        }catch (Exception e){
            e.printStackTrace();
        }
        Log.i("abtohdekhnaparega", "Value in viewpager: "+ viewPager.getCurrentItem());
        String position_text="";
        if(str_tab_name.size()>0)
            position_text=str_tab_name.get(viewPager.getCurrentItem());

        if(isChecked){     //deal-->service
            logNormalMenuSwitchEvent();
            logNormalMenuSwitchFirebaseEvent();
            int position= 0;
            str_tab_name.clear();

            for(int i=0; i<data.getCategories().size();i++){
                str_tab_name.add(data.getCategories().get(i).getName());

                if(data.getCategories().get(i).getName().equalsIgnoreCase(position_text)){
                    position= i;
                }
            }
            adapter= new ServicesFragmentPager(getSupportFragmentManager(),
                    ServiceSpecificActivity.this, data, str_tab_name);
            viewPager.setAdapter(adapter);
            viewPager.setCurrentItem(position);
            tabLayout.setupWithViewPager(viewPager);
            for (int i = 0; i < tabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = tabLayout.getTabAt(i);
                tab.setCustomView(adapter.getTabView(i));
            }

        }else{          //service-->deal
            logDealMenuSwitchEvent();
            logDealMenuSwitchFirebaseEvent();

            int position= 0;
            str_tab_name.clear();
            for(int i=0; i<deal_data.getCategories().size();i++){
                str_tab_name.add(deal_data.getCategories().get(i).getName());

                if(deal_data.getCategories().get(i).getName().equalsIgnoreCase(position_text)){
                    position= i;
                }
            }
            adapter= new ServicesFragmentPager(getSupportFragmentManager(),
                    ServiceSpecificActivity.this, deal_data, str_tab_name);
            viewPager.setAdapter(adapter);
            viewPager.setCurrentItem(position);
            tabLayout.setupWithViewPager(viewPager);
            for (int i = 0; i < tabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = tabLayout.getTabAt(i);
                tab.setCustomView(adapter.getTabView(i));
            }

        }

    }

    public void setStartViewPager(){

        int position= 0;
        boolean is_deal = false;

        //deal menu
        str_tab_name.clear();           //clearing the tab name
        for(int i=0; i<deal_data.getCategories().size();i++){
            str_tab_name.add(deal_data.getCategories().get(i).getName());

            if(deal_data.getCategories().get(i).getName().equalsIgnoreCase(department_service)){

                position= i;
                is_deal= true;
            }
        }

        if(is_deal){        //deal kholo kyuki yeh department hai deal mai
            Log.i("viewpagerr", "in deal");
            toggle_menu.setChecked(false);

            adapter= new ServicesFragmentPager(getSupportFragmentManager(),
                    ServiceSpecificActivity.this, deal_data, str_tab_name);
            viewPager.setAdapter(adapter);
            viewPager.setCurrentItem(position);
            tabLayout.setupWithViewPager(viewPager);
            for (int i = 0; i < tabLayout.getTabCount(); i++) {

                TabLayout.Tab tab = tabLayout.getTabAt(i);
                tab.setCustomView(adapter.getTabView(i));
            }
        }else{          //normal menu kholo kyunki deal nai hai selected department mai

            Log.i("viewpagerr", "in service");
            toggle_menu.setChecked(true);

            str_tab_name.clear();           //clearing the tab name
            if (department_service.length()>0){
                for(int i=0; i<data.getCategories().size();i++){
                    str_tab_name.add(data.getCategories().get(i).getName());
                    if(data.getCategories().get(i).getName().equalsIgnoreCase(department_service)){
                        position= i;

                        Log.i("viewpagerr", "values: "+ data.getCategories().get(i).getName()+ " "+
                                department_service);
                    }
                }}else{
                position=index;
                for(int i=0; i<data.getCategories().size();i++){
                    str_tab_name.add(data.getCategories().get(i).getName());

                }
            }
            adapter= new ServicesFragmentPager(getSupportFragmentManager(),
                    ServiceSpecificActivity.this, data, str_tab_name);
            viewPager.setAdapter(adapter);
            viewPager.setCurrentItem(position);
            tabLayout.setupWithViewPager(viewPager);
            for (int i = 0; i < tabLayout.getTabCount(); i++) {

                TabLayout.Tab tab = tabLayout.getTabAt(i);
                tab.setCustomView(adapter.getTabView(i));
            }
        }

        is_first_time= false;           //ab set menu chala hai

    }

    private class ServicesFragmentPager extends FragmentStatePagerAdapter {

        private Context context;
        private Data data;
        private List<String> str_tab_name;

        private ServicesFragmentPager(FragmentManager fm, Context context, Data data, List<String> str_tab_name){
            super(fm);
            this.context= context;
            this.data= data;
            this.str_tab_name= str_tab_name;
        }

        @Override
        public Fragment getItem(int position) {

            try{

                return ServicesSpecificFragment.newInstance(
                        data.getCategories().get(position), slabs, user_cart, saved_cart,homeResponse);
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;


        }

        @Override
        public int getCount() {
            return data.getCategories().size();
        }

        private View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(context).inflate(R.layout.activity_services_custom_tab, null);

            TextView textView= (TextView)v.findViewById(R.id.txt_tab_name);
            textView.setText(str_tab_name.get(position));

            return v;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onComboEvent(final ServiceComboEvent event) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                UserServices services= new UserServices();

                services.setName(event.getService_name());

                services.setService_deal_id(event.getService_deal_id());               //service ya deal id hogi isme

                services.setDealId(event.getDealId());

                services.setType(event.getType());

                //with tax
                int price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                        event.getPrice());
                int menu_price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                        event.getMenu_price());

                services.setPrice(price_);
                services.setMenu_price(menu_price_);

                services.setPackageServices(event.getPackage_services_list());          //isme hi service code hai

                services.setDescription(event.getDescription());
                services.setPrimary_key(event.getPrimary_key());

                Log.i("primary_key", "value in the service activity: " + event.getService_name()+ "  "+ event.getService_code()
                        + "  "+event.getService_deal_id()
                        + "  "+ event.getType()+ "  "+event.getPrice()+ "  "+event.getMenu_price()+ "  "+
                        event.getDescription()+ "  "+ event.getPrimary_key() + "  "+ user_cart.getCartType());

                new Thread(new UserCartTask(ServiceSpecificActivity.this, user_cart, services, false, false)).start();
                addServiceToCart(user_cart.getParlorId(),event.getService_code());

                showSnackbar(event.getService_name());
            }
        }, 250);
    }


    //normal service ke case mai yeh hai bhai
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void addServiceEvent(AddServiceEvent event) {

        Log.i("i'amwhere", "hey hell yea single service");

        UserServices services= new UserServices();
        services.setName(event.getName());

        services.setService_deal_id(event.getService_deal_id());
        services.setService_id(event.getService_id());              //quantity ke liye

        services.setService_code(event.getService_code());
        services.setPrice_id(event.getPrice_id());

        services.setBrand_id(event.getBrand_id());
        services.setProduct_id(event.getProduct_id());

        //service ya deal id hogi isme
        services.setType(event.getType());

        //no tax
        int price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                event.getPrice());
        int menu_price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                event.getMenu_price());

        services.setPrice(price_);
        services.setMenu_price(menu_price_);

        services.setDescription(event.getDescription());
        services.setPrimary_key(event.getPrimary_key());

        Log.i("primary_key", "value in the service activity: " + event.getName()+ "  "+ event.getService_code()
                + "  "+event.getService_deal_id()
                + "  "+ event.getType()+ "  "+event.getPrice()+ "  "+event.getMenu_price()+ "  "+
                event.getDescription()+ "  "+ event.getPrimary_key());

        logAddedToCartEvent(event.getName(),"INR",price_);

        new Thread(new UserCartTask(ServiceSpecificActivity.this, user_cart, services, false, false)).start();
        addServiceToCart(user_cart.getParlorId(),event.getService_code());
        showSnackbar(event.getName());
    }

    //normal service ke case mai yeh hai bhai
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void addServiceEvent(AddCartEvent event) {

        showSnackbar(event.getName());
    }



    public void showSnackbar(String service_name){
        Snackbar snackbar = Snackbar.make( coordinator_,
                service_name+" Service Added To Cart!", 1000);

        //setting the snackbar action button text size
        View view = snackbar.getView();
        TextView txt_action = (TextView) view.findViewById(android.support.design.R.id.snackbar_action);
        TextView txt_text = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txt_action.setTextSize(13);
        txt_action.setAllCaps(false);
        txt_text.setTextSize(13);
        snackbar.setActionTextColor(ContextCompat.getColor(ServiceSpecificActivity.this, R.color.snackbar));

        snackbar.setAction("View Cart", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logViewBlackViewCartPopUpEvent();
                logViewBlackViewCartPopUpFireBaseEvent();
                UserCartFragment fragment= new UserCartFragment();
                Bundle bundle= new Bundle();
                bundle.putBoolean("has_data", true);
                bundle.putBoolean(UserCartFragment.FROM_SERVICE, true);
                bundle.putString(UserCartFragment.CART_DATA ,new Gson().toJson(user_cart, UserCart.class));
                bundle.putString(UserCartFragment.HOME_RESPONSE_DATA,new Gson().toJson(homeResponse, HomeResponse.class));
                fragment.setArguments(bundle);
                fragment.show(getSupportFragmentManager(), "user_cart");
            }
        });
        snackbar.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void upgradeEvent(final UpgradeEvent event) {

        Log.i("upgradeevent", "i'm in the upgrade event: "+ event.getServiceId());

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                String category_index= null, service_index= null;
                for(int i=0; i<data.getCategories().size(); i++){

                    for(int j=0; j<data.getCategories().get(i).getServices().size(); j++){

                        if(data.getCategories().get(i).getServices().get(j).getServiceId()!=null &&
                                data.getCategories().get(i).getServices().get(j)
                                        .getServiceId().equalsIgnoreCase(event.getServiceId())) {

                            category_index= String.valueOf(i);
                            service_index= String.valueOf(j);
                        }
                    }
                }

                if(category_index!=null){

                    Service service= data.getCategories()
                            .get(Integer.parseInt(category_index))
                            .getServices().get(Integer.parseInt(service_index));

                    if((service.getPrices()!=null && service.getPrices().size()>0) &&
                            (service.getPrices().get(0).getBrand()==null ||
                                    service.getPrices().get(0).getBrand().getBrands().size()<1) &&
                            (service.getPrices().get(0).getAdditions() ==null ||
                                    service.getPrices().get(0).getAdditions().size()<1) &&
                            (service.getUpgrades()==null || service.getUpgrades().size()<1) &&
                            (service.getPackages() ==null || service.getPackages().size()<1)){

                        Log.i("ispeha", "i'm in 1");
                        //add normal service here
                        addService(service);

                    }else if((service.getPrices()!=null && service.getPrices().size()>0) &&
                            ((service.getPrices().get(0).getBrand()!= null &&
                                    service.getPrices().get(0).getBrand().getBrands()!=null &&
                                    service.getPrices().get(0).getBrand().getBrands().size()>1) ||
                                    (service.getPrices().get(0).getAdditions()!=null &&
                                            service.getPrices().get(0).getAdditions().size()>0))){

                        //yaha i'm not checking upgrade or package, because on need motherfucker
                        //open window don't add anything,
                        Log.i("ispeha", "i'm in 2");

                        ServiceSpecificDialogFragment fragment= new ServiceSpecificDialogFragment();
                        Bundle bundle= new Bundle();
                        bundle.putString("service", new Gson().toJson(service, Service.class));
                        bundle.putString("user_cart", new Gson().toJson(user_cart, UserCart.class));
                        bundle.putString("slabs", new Gson().toJson(slabs, Slabs.class));
                        bundle.putBoolean("add_service", false);
                        bundle.putBoolean("add_", false);

                        fragment.setArguments(bundle);
                        fragment.show(getSupportFragmentManager(), TAG);

                    }else if((service.getPrices()!=null && service.getPrices().size()>0) &&
                            (service.getPrices().get(0).getBrand()== null ||
                                    service.getPrices().get(0).getBrand().getBrands()==null ||
                                    service.getPrices().get(0).getBrand().getBrands().size()<1) &&
                            (service.getPrices().get(0).getAdditions()==null ||
                                    service.getPrices().get(0).getAdditions().size()<1) &&
                            (service.getUpgrades()!=null && service.getUpgrades().size()>0 ||
                                    service.getPackages()!=null && service.getPackages().size()>0)){

                        //no brand  aur addition but package or upgrade, so add simple service inside

                        Log.i("ispeha", "i'm in 3");

                        ServiceSpecificDialogFragment fragment= new ServiceSpecificDialogFragment();
                        Bundle bundle= new Bundle();
                        bundle.putString("service", new Gson().toJson(service, Service.class));
                        bundle.putString("user_cart", new Gson().toJson(user_cart, UserCart.class));
                        bundle.putString("slabs", new Gson().toJson(slabs, Slabs.class));
                        bundle.putBoolean("add_service", true);
                        bundle.putBoolean("add_", false);

                        fragment.setArguments(bundle);
                        fragment.show(getSupportFragmentManager(), TAG);

                    }else if((service.getPrices()!=null && service.getPrices().size()>0) &&
                            (service.getPrices().get(0).getBrand()!= null ||
                                    service.getPrices().get(0).getBrand().getBrands()!=null ||
                                    service.getPrices().get(0).getBrand().getBrands().size()==1) &&
                            (service.getPrices().get(0).getBrand().getBrands().get(0).getProducts()==null ||
                                    (service.getPrices().get(0).getBrand().getBrands().get(0).getProducts()!=null &&
                                            service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().size()<=1)) &&
                            (service.getPrices().get(0).getAdditions() ==null ||
                                    service.getPrices().get(0).getAdditions().size()<1) &&
                            (service.getUpgrades() ==null || service.getUpgrades().size() ==0) &&
                            (service.getPackages() ==null || service.getPackages().size() ==0)){

                        //add brand service here
                        Log.i("ispeha", "i'm in 4");
                        singleBrandProduct(service);

                    }else if((service.getPrices()!=null && service.getPrices().size()>0) &&
                            (service.getPrices().get(0).getBrand()!= null ||
                                    service.getPrices().get(0).getBrand().getBrands()!=null ||
                                    service.getPrices().get(0).getBrand().getBrands().size()==1) &&
                            (service.getPrices().get(0).getBrand().getBrands().get(0).getProducts()==null ||
                                    (service.getPrices().get(0).getBrand().getBrands().get(0).getProducts()!=null &&
                                            service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().size()<=1)) &&
                            (service.getPrices().get(0).getAdditions() ==null ||
                                    service.getPrices().get(0).getAdditions().size()<1) &&
                            (service.getUpgrades() !=null && service.getUpgrades().size()>0) ||
                            (service.getPackages() !=null && service.getPackages().size()>0)){

                        Log.i("ispeha", "i'm in 5: "+ service.getPrices().get(0).getBrand().getBrands().size());

                        // add brand wali service inside
                        ServiceSpecificDialogFragment fragment= new ServiceSpecificDialogFragment();
                        Bundle bundle= new Bundle();
                        bundle.putString("service", new Gson().toJson(service, Service.class));
                        bundle.putString("user_cart", new Gson().toJson(user_cart, UserCart.class));
                        bundle.putString("slabs", new Gson().toJson(slabs, Slabs.class));
                        bundle.putBoolean("add_service", false);
                        bundle.putBoolean("add_", true);

                        fragment.setArguments(bundle);
                        fragment.show(getSupportFragmentManager(), TAG);
                    }else{

                        Log.i("ispeha", "i'm in 6, kisi mai nai... handle kar bsdk");
                    }
                }
            }
        }, 250);
    }

    private void addService(Service service){

        UserServices services= new UserServices();

        services.setName(service.getName());

        String service_deal_id= service.getDealId()==null? service.getServiceId():service.getDealId();
        services.setService_deal_id(service_deal_id);       //service ya deal id hogi isme

        services.setType(service.getDealId()==null?"service":service.getDealType());

        services.setService_code(service.getServiceCode());
        services.setPrice_id(service.getPrices().get(0).getPriceId());

        int price= service.getDealId()==null?service.getPrices().get(0).getPrice():service.getDealPrice();
        int menu_price= service.getDealId()==null?0:service.getMenuPrice();

        //with tax
        int price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*price);
        int menu_price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*menu_price);

        services.setPrice(price_);
        services.setMenu_price(menu_price_);

        services.setDescription(service.getDescription()==null?
                "":service.getDescription());

        String primary_key= ""+service.getServiceCode()+service_deal_id;
        services.setPrimary_key(primary_key);

        Log.i("primary_key", "value to be sent from adapter: " + services.getName()+ "  "+ services.getService_code()
                + "  "+services.getService_deal_id()+ "  "+services.getPrice_id()
                + "  "+ services.getType()+ "  "+services.getPrice()+ "  "+services.getMenu_price()+ "  "+
                services.getDescription()+ "  "+ services.getPrimary_key());

        new Thread(new UserCartTask(ServiceSpecificActivity.this, user_cart, services, false, false)).start();
        addServiceToCart(user_cart.getParlorId(),service.getServiceCode());
        showSnackbar(services.getName());
    }


    private void addServiceToCart(String parlorId,int serviceCode  ){
        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface= retrofit.create(ApiInterface.class);
        UserCart_post userCartPost=new UserCart_post();
        userCartPost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        userCartPost.setUserId(BeuSalonsSharedPrefrence.getUserId());
        userCartPost.setParlorId(parlorId);
        userCartPost.setServiceCode(serviceCode);
        userCartPost.setQuantity(1);
        Call<AddService_response> call=apiInterface.addServicetoCart(userCartPost);
        call.enqueue(new Callback<AddService_response>() {
            @Override
            public void onResponse(Call<AddService_response> call, Response<AddService_response> response) {
                if (response.isSuccessful()){
                    if (response.body().isSuccess()){
                        Log.e("stuff add service cart", "i'm retrofit getStatus true :(");

                    }else{
                        Log.e("stuff add service cart", "i'm retrofit getStatus false:(");

                    }
                }else{
                    Log.e("stuff add service cart", "i'm retrofit failure :(");

                }
            }

            @Override
            public void onFailure(Call<AddService_response> call, Throwable t) {
                Log.e("stuff add service cart", "i'm in failure: "+ t.getMessage()+ "   "+
                        t.getStackTrace()+ t.getCause()+ " "+ t.getLocalizedMessage());
            }
        });

    }


    private void singleBrandProduct(Service service){

        Log.i("i'amwhere", "hey hell yea single brand product");

        UserServices services= new UserServices();

        services.setName(service.getName());
        services.setService_id(service.getServiceId());
        services.setService_code(service.getServiceCode());
        services.setPrice_id(service.getPrices().get(0).getPriceId());

        String description= "";

        Boolean has_product= false;
        if( service.getPrices()!=null &&
                service.getPrices().size()>0 &&
                service.getPrices().get(0).getBrand()!=null &&
                service.getPrices().get(0).getBrand().getBrands().size()>0) {

            has_product = false;
            if (service.getPrices().get(0).getBrand().getBrands().get(0).getProducts() != null &&
                    service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().size() > 0) {

                has_product = true;
            }
        }

        String brand_name="",brand_id="",
                product_name="", product_id="";

        if(has_product){

            description= service.getPrices().get(0).getBrand().getBrands().get(0).getName()+ ", "+
                    service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().get(0).getName();

            brand_name=service.getPrices().get(0).getBrand().getBrands().get(0).getName();
            brand_id= service.getPrices().get(0).getBrand().getBrands().get(0).getBrandId();

            product_name= service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().get(0).getName();
            product_id= service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().get(0).getProductId();

            if(service.getPrices().get(0).getBrand().getBrands().get(0).getProducts().get(0).getDealRatio()!=null){

                services.setPrice((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                        (int)(service.getPrices().get(0).getBrand().getBrands().get(0)
                                .getProducts().get(0).getDealRatio() * service.getDealPrice())));
                services.setMenu_price((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                        service.getMenuPrice()));

                services.setService_deal_id(service.getDealId());
                services.setType(service.getDealType());
            }else{

                services.setPrice((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                        (int)(service.getPrices().get(0).getBrand().getBrands().get(0)
                                .getProducts().get(0).getRatio() *
                                service.getPrices().get(0).getPrice())));

                services.setService_deal_id(service.getServiceId());
                services.setType("service");
            }
        }else {

            description= service.getPrices().get(0).getBrand().getBrands().get(0).getName();

            brand_name= service.getPrices().get(0).getBrand().getBrands().get(0).getName();
            brand_id= service.getPrices().get(0).getBrand().getBrands().get(0).getBrandId();

            if(service.getPrices().get(0).getBrand().getBrands().get(0).getDealRatio()!=null){

                services.setPrice((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                        (int)(service.getPrices().get(0).getBrand().getBrands()
                                .get(0).getDealRatio()*service.getDealPrice())));
                services.setMenu_price((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*service.getMenuPrice()));

                services.setService_deal_id(service.getDealId());
                services.setType(service.getDealType());

            }else{

                services.setPrice((int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*
                        (int)service.getPrices().get(0).getBrand().getBrands().get(0).getRatio()
                        *service.getPrices().get(0).getPrice()));

                services.setService_deal_id(service.getServiceId());
                services.setType("service");
            }
        }
        Log.i("singleservice", "value: "+ service.getServiceCode()+ " "+ services.getService_deal_id()+ " "+
                brand_id+ " "+ product_id);

        services.setBrand_name(brand_name);
        services.setBrand_id(brand_id);

        services.setProduct_name(product_name);
        services.setProduct_id(product_id);

        services.setPrimary_key(""+service.getServiceCode()+services.getService_deal_id()+brand_id+product_id);
        services.setDescription(description);
        logAddedToCartEvent(services.getName(),"INR",services.getPrice());

        new Thread(new UserCartTask(ServiceSpecificActivity.this, user_cart, services, false, false)).start();
        addServiceToCart(user_cart.getParlorId(),service.getServiceCode());

        showSnackbar(services.getName());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(UserCart saved_cart) {

        if(saved_cart!=null && saved_cart.getServicesList().size()>0){

            int quantity=0;
            if(saved_cart.getServicesList().size()==1){

                quantity= saved_cart.getServicesList().get(0).getQuantity();
                if(quantity==1)
                    txt_item.setText(quantity+ " Item In Cart");
                else
                    txt_item.setText(quantity+ " Items In Cart");
            } else if(saved_cart.getServicesList().size()>1){

                for(int i=0;i<saved_cart.getServicesList().size();i++){

                    quantity+= saved_cart.getServicesList().get(i).getQuantity();
                }

                txt_item.setText(quantity+ " Items In Cart");
            }else
                txt_item.setText("0 Item In Cart");
        }else
            txt_item.setText("0 Item In Cart");

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSingleBrandProductEvent(SingleBrandProductEvent event) {

        Log.i("i'minsinglebran", "i'm here now baby");
        singleBrandProduct(event.getService());

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void packageListEvent(final PackageListEvent event) {

        showSnackbar(event.getName());

        Handler handler = new Handler();
        for (int i=0;i<event.getList().size();i++) {
            final int finalI = i;
            handler.postDelayed(new Runnable() {
                public void run() {

                    addServiceToCart(user_cart.getParlorId(), event.getList().get(finalI).getService_code());
                }

            }, 2000);

        }


        new Thread(new MultipleServicesTask(this, user_cart, event.getList())).start();

        if(event.isAlter())
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    UserCartFragment fragment= new UserCartFragment();
                    Bundle bundle= new Bundle();
                    bundle.putBoolean("has_data", true);
                    bundle.putBoolean(UserCartFragment.FROM_SERVICE, true);
                    bundle.putString(UserCartFragment.CART_DATA ,new Gson().toJson(user_cart, UserCart.class));
                    bundle.putString(UserCartFragment.HOME_RESPONSE_DATA,new Gson().toJson(homeResponse, HomeResponse.class));
                    fragment.setArguments(bundle);
                    fragment.show(getSupportFragmentManager(), "user_cart");
                }
            }, 400);
    }

    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(department_name);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }




    @Override
    protected void onResume() {
        super.onResume();
        openCart();
    }



    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */

    public void logAddedToCartEvent (String contentType, String currency, double price) {
        Log.e("prefine","add  type"+contentType+ " price"+price);

        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        logger.logEvent(AppEventsConstants.EVENT_NAME_ADDED_TO_CART, price, params);
    }


    public void logNormalMenuSwitchEvent () {
        Log.e("toggleMaleFemalenormal","fine");
        logger.logEvent(AppConstant.NormalMenuSwitch);
    }
    public void logServicePageBackButton () {
        Log.e("ServicePageBackButton","fine");
        logger.logEvent(AppConstant.ServicePageBackButton);
    }
    public void logServicePageBackButtonFirebaseEvent () {
        Log.e("ServicePageBackButton","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.ServicePageBackButton,bundle);
    }

    public void logNormalMenuSwitchFirebaseEvent () {
        Log.e("toggleMaleFefirebasnor","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.NormalMenuSwitch,bundle);
    }
    public void logDealMenuSwitchEvent () {
        Log.e("toggleMaleFemaledeal","fine");
        logger.logEvent(AppConstant.DealMenuSwitch);
    }

    public void logDealMenuSwitchFirebaseEvent () {
        Log.e("toggleMalefirebasdeal","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.DealMenuSwitch,bundle);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logProceedServiceEvent () {
        Log.e("ProceedService","fine");

        logger.logEvent(AppConstant.ProceedService);
    }

    public void logProceedServiceFireBaseEvent () {
        Log.e("ProceedfirebaseService","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.ProceedService,bundle);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */


    public void logViewBlackViewCartPopUpEvent () {
        Log.e("BlackViewCartPopUp","fine");

        logger.logEvent(AppConstant.BlackViewCartPopUp);
    }
    public void logViewBlackViewCartPopUpFireBaseEvent () {
        Log.e("BlackViewCartPopUpfire","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.BlackViewCartPopUp,bundle);
    }
    public void logViewCartServiceEvent () {
        Log.e("ViewCartService","fine");

        logger.logEvent(AppConstant.ViewCartService);
    }
    public void logViewCartServiceFireBaseEvent () {
        Log.e("ViewCartfirebaseService","fine");
        Bundle bundle=new Bundle();
        mFirebaseAnalytics.logEvent(AppConstant.ViewCartService,bundle);
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        nMyApplication.onActivityPaused(this);

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        nMyApplication.onActivityDestroyed(this);
        logServicePageBackButtonFirebaseEvent();
        logServicePageBackButton();
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        nMyApplication.onActivityStarted(this);
        EventBus.getDefault().register(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        nMyApplication.onActivityStopped(this);
        EventBus.getDefault().unregister(this);

        super.onStop();
    }
}
