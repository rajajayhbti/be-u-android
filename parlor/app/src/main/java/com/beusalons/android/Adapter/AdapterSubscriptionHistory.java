package com.beusalons.android.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.SubscriptionHistory.Month;
import com.beusalons.android.R;

import java.util.List;

/**
 * Created by Ajay on 2/26/2018.
 */

public class AdapterSubscriptionHistory extends RecyclerView.Adapter<AdapterSubscriptionHistory.MyViewHolder> {
   Activity activity ;
   List<Month> list;

    public AdapterSubscriptionHistory(Activity activity, List<Month> list) {
        this.activity = activity;
        this.list = list;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(activity).inflate(R.layout.row_subscription_history, parent, false);
        return new AdapterSubscriptionHistory.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.linear_his.removeAllViews();

        holder.txt_month_name.setText(list.get(position).getMonth()+"- History");

        int size=list.get(position).getAppointments().size();

        for (int i=0;i<size;i++){
            View view = activity.getLayoutInflater().inflate(R.layout.row_subscription_item, null);
            TextView txt_date = (TextView) view.findViewById(R.id.txt_date);
            TextView txt_salon_name=(TextView)view.findViewById(R.id.txt_salon_name);
            TextView txt_amt_used=(TextView)view.findViewById(R.id.txt_amt_used);
            TextView txt_amt_remaining=(TextView)view.findViewById(R.id.txt_amt_remaining);
            View viewdevider=view.findViewById(R.id.view);



            txt_date.setText(fromHtml("Date: "+"<b><font size=16 color=#414042>"+list.get(position).getAppointments()
                    .get(i).getDate()+"</font></b>"));
            txt_salon_name.setText(fromHtml("Salon Name: "+"<b><font size=16 color=#414042>"+list.get(position).getAppointments()
                    .get(i).getParlorName()+"</font></b>"));
            txt_amt_used.setText(fromHtml("Amount Used: "+"<b><font size=16 color=#414042>"+ AppConstant.CURRENCY+list.get(position).getAppointments()
                    .get(i).getLoyalitySubscription()+"</font></b>"));
            txt_amt_remaining.setText(fromHtml("Payable Amount :  "+"<b><font size=16 color=#414042>"+AppConstant.CURRENCY+list.get(position).getAppointments()
                    .get(i).getPayableAmount()+"</font></b>"));
            if (i==size-1){
                viewdevider.setVisibility(View.GONE);
            }
             holder.linear_his.addView(view);

        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView txt_month_name;
        private LinearLayout linear_his;
        public MyViewHolder(View itemView) {
            super(itemView);
            txt_month_name=itemView.findViewById(R.id.txt_month_name);
            linear_his=itemView.findViewById(R.id.linear_his);
        }
    }


    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

}
