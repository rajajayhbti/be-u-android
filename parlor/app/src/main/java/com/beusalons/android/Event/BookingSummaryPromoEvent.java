package com.beusalons.android.Event;

/**
 * Created by Ashish Sharma on 4/20/2017.
 */

public class BookingSummaryPromoEvent {

    private Integer loyaltyPoints;

    public BookingSummaryPromoEvent(Integer loyaltyPoints){

        this.loyaltyPoints= loyaltyPoints;
    }

    public Integer getLoyaltyPoints() {
        return loyaltyPoints;
    }

    public void setLoyaltyPoints(Integer loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }
}
