package com.beusalons.android.Model;

/**
 * Created by myMachine on 11/21/2016.
 */

public class DateTimeModel {

    public static final int IMAGE_TYPE = 0;
    public static final int TIME_TYPE = 1;

    private String display_time;
    private String date;
    private String time;
    private int image_id;
    private int type;
    private String txt_image;


    public String getTxt_image() {
        return txt_image;
    }

    public void setTxt_image(String txt_image) {
        this.txt_image = txt_image;
    }

    public String getDisplay_time() {
        return display_time;
    }

    public void setDisplay_time(String display_time) {
        this.display_time = display_time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getImage_id() {
        return image_id;
    }

    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
