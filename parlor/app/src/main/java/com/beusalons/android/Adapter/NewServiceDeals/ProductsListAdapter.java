package com.beusalons.android.Adapter.NewServiceDeals;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.beusalons.android.Event.NewServicesEvent.ProductEvent;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.Model.newServiceDeals.ServiceByDepartment.Product;
import com.beusalons.android.R;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by myMachine on 6/1/2017.
 */

public class ProductsListAdapter extends RecyclerView.Adapter<ProductsListAdapter.ViewHolder>{

    private Context context;
    private List<Product> list;
    private String productId, productName, service_deal_id;
    private int price, brand_index, menu_price;
    private boolean hasTypes;
    AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;


    public ProductsListAdapter(Context context, List<Product> list, int brand_index, boolean hasTypes){

        this.context= context;
        this.list= list;
        this.brand_index= brand_index;
        this.hasTypes= hasTypes;
        logger = AppEventsLogger.newLogger(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

    }

    public void setProductList(List<Product> list, int brand_index){

        this.list= list;
        this.brand_index= brand_index;
        notifyDataSetChanged();
    }

    public int getMenu_price(){
        return menu_price;
    }

    public String getProductId(){
        return productId;
    }

    public String getProductName(){
        return productName;
    }

    public String getService_deal_id(){
        return service_deal_id;
    }

    public int getPrice(){
        return price;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Product product= list.get(position);
        holder.txt_brand_name.setText(product.getName());

        holder.txt_save_per.setVisibility(View.GONE);

        /*if(hasTypes){           //don't show price

            holder.txt_menu_price.setVisibility(View.GONE);

            holder.txt_price.setText(AppConstant.CURRENCY+(int)product.getPrice());
        }else{          //show karo price
*/

        //price with tax
        int price_=  (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*product.getPrice());

        holder.txt_price.setText(AppConstant.CURRENCY+price_);

        if(product.getType().equalsIgnoreCase("service")){

            holder.txt_menu_price.setVisibility(View.GONE);

        }else{
            holder.txt_menu_price.setVisibility(View.VISIBLE);

            //menu price with tax
            int menu_price_= (int)Math.round(BeuSalonsSharedPrefrence.getServiceTax()*product.getMenu_price());

            holder.txt_menu_price.setText(AppConstant.CURRENCY+menu_price_);
            holder.txt_menu_price.setTextColor(Color.parseColor("#808285"));
            holder.txt_menu_price.setPaintFlags(  holder.txt_menu_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        if (position==list.size()-1){

            holder.viewDivider.setVisibility(View.GONE);
        }
        boolean isCheck= false;                     //this is main check boolean
        if(product.isCheck()){
            //yaha pahle wale ki value by default a jayegi :D
            productId= product.getProductId();
            productName= product.getName();
            price=(int) product.getPrice();
            service_deal_id= product.getService_deal_id();
            menu_price= (int) product.getMenu_price();

            isCheck= true;
            holder.radio_.setChecked(true);
        }else{
            isCheck= false;
            holder.radio_.setChecked(false);
        }
        final boolean setCheck= isCheck;                        //checking radio check
        holder.linear_radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(setCheck){

                    holder.radio_.setChecked(false);
                    list.get(position).setCheck(false);
                }else{

                    logEditServiceProductChangeEvent(product.getName());
                    logEditServiceProductChangeFireBaseEvent(product.getName());
                    productId= product.getProductId();
                    productName= product.getName();
                    price=(int) product.getPrice();
                    service_deal_id= product.getService_deal_id();
                    menu_price= product.getMenu_price();

                    holder.radio_.setChecked(true);
                    list.get(position).setCheck(true);
                    EventBus.getDefault().post(new ProductEvent(position, brand_index));
                }
                int selected_radio_= 0;
                for(int i=0;i<list.size();i++){

                    if(!list.get(i).isCheck()){

                        Log.i("i'mhere", "count: ++");
                        selected_radio_++;
                    }
                }

                Log.i("i'mhere", "selected size: "+ selected_radio_+ " " + list.size());
                if(selected_radio_== list.size()){

                    holder.radio_.setChecked(true);
                    list.get(position).setCheck(true);
                }

                updateView(position);
            }
        });
    }


    private void updateView(int pos){

        for(int i=0; i<list.size();i++){
            if(i!=pos){
                list.get(i).setCheck(false);
            }
        }
        notifyDataSetChanged();
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logEditServiceProductChangeEvent (String name) {
        Log.e("EditSerProductChange","fine");
        Bundle params = new Bundle();
        params.putString("Name", name);
        logger.logEvent(AppConstant.EditServiceProductChange, params);
    }
    public void logEditServiceProductChangeFireBaseEvent (String name) {
        Log.e("EditProductFirebasech","fine");
        Bundle params = new Bundle();
        params.putString("Name", name);
        mFirebaseAnalytics.logEvent(AppConstant.EditServiceProductChange, params);
    }
    public class ViewHolder extends RecyclerView.ViewHolder{

        RadioButton radio_;
        private TextView txt_price, txt_save_per, txt_menu_price, txt_brand_name;
        private LinearLayout linear_radio;
        private View viewDivider;
        public ViewHolder(View itemView) {
            super(itemView);
            viewDivider=(View)itemView.findViewById(R.id.view_for_divider);
            radio_= (RadioButton)itemView.findViewById(R.id.radio_);
            txt_price= (TextView)itemView.findViewById(R.id.txt_price);
            txt_save_per= (TextView)itemView.findViewById(R.id.txt_save_per);
            txt_menu_price= (TextView)itemView.findViewById(R.id.txt_menu_price);
            txt_brand_name= (TextView)itemView.findViewById(R.id.txt_brand_name);
            linear_radio= (LinearLayout)itemView.findViewById(R.id.linear_radio);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.fragment_services_specific_dialog_radio, parent, false);
        return new ViewHolder(view);
    }
}
