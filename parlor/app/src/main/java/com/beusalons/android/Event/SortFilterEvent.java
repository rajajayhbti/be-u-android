package com.beusalons.android.Event;

import java.util.List;

/**
 * Created by myMachine on 26-Feb-18.
 */

public class SortFilterEvent {

    private String sort;
    private String rating;
    private String category;
    private List<String> brands;

    public SortFilterEvent(String sort, String rating, String category, List<String> brands ){
        this.sort= sort;
        this.rating= rating;
        this.category= category;
        this.brands= brands;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<String> getBrands() {
        return brands;
    }

    public void setBrands(List<String> brands) {
        this.brands = brands;
    }
}
