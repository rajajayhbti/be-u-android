package com.beusalons.android.Model;

/**
 * Created by myMachine on 20-Feb-18.
 */

public class HomePageBottomSheetModel {

    private boolean success;
    private Datum data;


    public class Datum{

        private String type;
        private String heading1;
        private String description;
        private String url;
        private String buttonText;
        private String parlorId;
        private String appointmentId;

        public String getParlorId() {
            return parlorId;
        }

        public void setParlorId(String parlorId) {
            this.parlorId = parlorId;
        }

        public String getAppointmentId() {
            return appointmentId;
        }

        public void setAppointmentId(String appointmentId) {
            this.appointmentId = appointmentId;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getHeading1() {
            return heading1;
        }

        public void setHeading1(String heading1) {
            this.heading1 = heading1;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getButtonText() {
            return buttonText;
        }

        public void setButtonText(String buttonText) {
            this.buttonText = buttonText;
        }
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Datum getData() {
        return data;
    }

    public void setData(Datum data) {
        this.data = data;
    }
}
