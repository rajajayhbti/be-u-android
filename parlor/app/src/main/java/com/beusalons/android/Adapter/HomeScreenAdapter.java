package com.beusalons.android.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.beusalons.android.BillSummaryActivity;
import com.beusalons.android.CorporateLoginActivity;
import com.beusalons.android.DateTimeActivity;
import com.beusalons.android.Event.CartNotificationEvent;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.MainActivity;
import com.beusalons.android.MemberShipCardAcitvity;
import com.beusalons.android.Model.Appointments.AppointmentPost;
import com.beusalons.android.Model.HomePage.ApiResponse;
import com.beusalons.android.Model.HomePage.HomeDatum;
import com.beusalons.android.Model.HomePage.List_;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.MyMembershipDetails;
import com.beusalons.android.ParlorListActivity;
import com.beusalons.android.R;
import com.beusalons.android.SalonPageActivity;
import com.beusalons.android.SubscriptionActivity;
import com.beusalons.android.Task.UserCartTask;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.snappydb.DB;
import com.snappydb.DBFactory;

import org.greenrobot.eventbus.EventBus;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.beusalons.android.Model.HomePage.HomeDatum.BANNER;
import static com.beusalons.android.Model.HomePage.HomeDatum.FREEBIES;
import static com.beusalons.android.Model.HomePage.HomeDatum.PARLOR;
import static com.beusalons.android.Model.HomePage.HomeDatum.PROGRESS;
import static com.beusalons.android.Model.HomePage.HomeDatum.SEARCH;
import static com.beusalons.android.Model.HomePage.HomeDatum.SUBSCRIPTION;

/**
 * Created by myMachine on 22-Jan-18.
 */

public class HomeScreenAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String CAROUSEL_OPEN_PARLOR= "parlors";
    private final String CAROUSEL_OPEN_DEALS= "deals";
    private final String CAROUSEL_OPEN_DEAL_PAGE= "dealPage";
    private final String CAROUSEL_OPEN_FREEBIES= "freebies";
    private final String CAROUSEL_OPEN_CORPORATE= "corporate";
    private final String CAROUSEL_OPEN_MEMBERSHIP= "membership";
    private final String CAROUSEL_OPEN_SUBSCRIPTION= "subscription";

    private List<HomeDatum> list;
    private String current_time;

    public HomeScreenAdapter(List<HomeDatum> list){
        this.list= list;
    }

    public void setList(List<HomeDatum> list, String current_time){
        this.list= list;
        Log.i("sizeing", "initial size: "+ this.list.size());
        this.current_time= current_time;
        notifyDataSetChanged();
    }

    public void addData(List<HomeDatum> list){
        int size= this.list.size();
        Log.i("sizeing", "size of new: "+ list.size()+ " "+ "old: "+ size);
        this.list.addAll(list);             //add kara
        Log.i("sizeing", "size: "+ this.list.size());
        notifyItemRangeInserted(size, this.list.size());        //existing size, aur new size
    }

    public void addProgress(){
        if(list!=null &&
                list.size()>0){
            Log.i("sizeing", "remove progress size: "+ list.size());
            list.add(new HomeDatum("progress", 100));
            notifyItemInserted(list.size()-1);
        }
    }

    public void removeProgress(){
        int size= list!=null &&
                list.size()>0?list.size():0;
        Log.i("sizeing", "remove progress size: "+ size);
        if(size>0){
            list.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        switch (viewType){
            case PARLOR:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_parlor, parent, false);
                return new ParlorViewHolder(view);
            case SUBSCRIPTION:
                view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_subscription, parent, false);
                return new SubscriptionViewHolder(view);
            case BANNER:
                view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_banner, parent, false);
                return new BannerViewHolder(view);
            case FREEBIES:
                view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_header, parent, false);
                return new FreebiesViewHolder(view);
            case SEARCH:
                view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_salon, parent, false);
                return new SearchViewHolder(view);
            case PROGRESS:
                view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_progress, parent, false);
                return new ProgressViewHolder(view);
            default:
                view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_default, parent, false);
                return new DefaultViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final int index= position;

        switch (list.get(index).getType_index()){

            case PARLOR:{

                final LinearLayout linear_= ((ParlorViewHolder)holder).linear;
                ImageView img_salon= linear_.findViewById(R.id.img_salon);
                ImageView img_type= linear_.findViewById(R.id.img_type);
                ImageView img_fav= linear_.findViewById(R.id.img_fav);
                TextView txt_name=  linear_.findViewById(R.id.txt_name);
                TextView txt_address= linear_.findViewById(R.id.txt_address);
                TextView txt_cost= linear_.findViewById(R.id.txt_cost);
                TextView txt_rating= linear_.findViewById(R.id.txt_rating);
                TextView txt_distance= linear_.findViewById(R.id.txt_distance);
                final TextView txt_detail= linear_.findViewById(R.id.txt_detail);

                txt_name.setText(list.get(index).getName());
                Glide.with(linear_.getContext())
                        .load(list.get(index).getImage())
                        .into(img_salon);
                if(list.get(index).getParlorType()==0)
                    img_type.setImageResource(R.drawable.ic_premium_badge);
                else if(list.get(index).getParlorType()==1)
                    img_type.setImageResource(R.drawable.ic_standard_badge);
                else
                    img_type.setImageResource(R.drawable.ic_budget_badge);

                if(list.get(index).getDistance()>1)
                    txt_distance.setText(list.get(index).getDistance()+"Kms");
                else
                    txt_distance.setText(list.get(index).getDistance()+"Km");

                txt_rating.setText(""+list.get(index).getRating());

                int price= list.get(index).getPrice();
                String cost="";
                if(price==1)
                    cost= "<font color='#3e780a'>₹</font>"+" "+"₹"+" "+"₹"+" "+"₹"+" "+"₹";
                else if(price==2)
                    cost= "<font color='#3e780a'>₹ ₹</font>"+" "+"₹"+" "+"₹"+" "+"₹";
                else if(price==3)
                    cost= "<font color='#3e780a'>₹ ₹ ₹</font>"+" "+"₹"+" "+"₹";
                else if(price==4)
                    cost= "<font color='#3e780a'>₹ ₹ ₹ ₹</font>"+" "+"₹";
                else
                    cost= "<font color='#3e780a'>₹ ₹ ₹ ₹ ₹</font>";
                txt_cost.setText(fromHtml(cost));

                if(list.get(index).getFavourite())
                    img_fav.setVisibility(View.VISIBLE);
                else
                    img_fav.setVisibility(View.GONE);

                txt_address.setText(list.get(index).getAddress1()+ " "+list.get(index).getAddress2());

                linear_.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        openSalon(view.getContext(), list.get(index).getParlorId(),
                                list.get(index).getName());


                    }
                });

                LinearLayout linear_show= linear_.findViewById(R.id.linear_show);
              //  linear_show.setVisibility(View.GONE);
              if(list.get(index).getAppointments()!=null &&
                        list.get(index).getAppointments().size()>0){

                    linear_show.setVisibility(View.VISIBLE);
                    final LinearLayout linear_container= linear_.findViewById(R.id.linear_container);
                    linear_container.removeAllViews();
                    for(int i=0;i<list.get(index).getAppointments().size();i++){
                        LinearLayout linear_appt= (LinearLayout) LayoutInflater.from(linear_.getContext())
                                .inflate(R.layout.layout_salon_book, linear_container, false);

                        TextView txt_amt, txt_book, txt_date;

                        txt_amt= linear_appt.findViewById(R.id.txt_amt);
                        txt_amt.setText(AppConstant.CURRENCY +(int)list.get(index).getAppointments().get(i).getAmount());


                        txt_date= linear_appt.findViewById(R.id.txt_date);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                        Date date_format=null;
                        try {
                            date_format = sdf.parse(list.get(index).getAppointments().get(i).getDate());
                            Calendar cal= Calendar.getInstance();       //creating calendar instance
                            cal.setTime(date_format);
                            cal.add(Calendar.HOUR_OF_DAY, 5);
                            cal.add(Calendar.MINUTE, 30);

                            txt_date.setText(""+cal.get(Calendar.DAY_OF_MONTH)+"/"+
                                    (1+cal.get(Calendar.MONTH))+"/"+cal.get(Calendar.YEAR));

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        final int pos= i;
                        txt_book= linear_appt.findViewById(R.id.txt_book);
                        txt_book.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent intent= new Intent(view.getContext(), DateTimeActivity.class);
                                Bundle bundle= new Bundle();
                                bundle.putBoolean(DateTimeActivity.REORDER, true);
                                bundle.putString(DateTimeActivity.APPOINTMENT_ID, list.get(index).getAppointments()
                                        .get(pos).getAppointmentId());
                                bundle.putString(DateTimeActivity.CURRENT_TIME, current_time);
                                bundle.putString(DateTimeActivity.OPENING_TIME,list.get(index).getOpeningTime());
                                bundle.putString(DateTimeActivity.CLOSING_TIME,list.get(index).getClosingTime());
                                bundle.putInt(DateTimeActivity.DAY_CLOSED, list.get(index).getDayClosed());
                                intent.putExtras(bundle);
                                view.getContext().startActivity(intent);
                            }
                        });
                        View view_= linear_appt.findViewById(R.id.view_);
                        if(i==list.get(index).getAppointments().size()-1)
                            view_.setVisibility(View.GONE);
                        else
                            view_.setVisibility(View.VISIBLE);

                        linear_container.addView(linear_appt);
                    }

                    if(list.get(index).isSelected()){

                        linear_container.setVisibility(View.VISIBLE);
                        txt_detail.setText("Details ˄");
                    } else{

                        linear_container.setVisibility(View.GONE);
                        txt_detail.setText("Details ˃");
                    }

                    LinearLayout linear_detail= linear_.findViewById(R.id.linear_detail);
                    linear_detail.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if(list.get(index).isSelected()){
                                linear_container.setVisibility(View.GONE);
                                list.get(index).setSelected(false);
                                txt_detail.setText("Details ˃");
                            } else{
                                linear_container.setVisibility(View.VISIBLE);
                                list.get(index).setSelected(true);
                                txt_detail.setText("Details ˄");
                            }

                        }
                    });

                }else
                    linear_show.setVisibility(View.GONE);





            }break;
            case SUBSCRIPTION:{
                LinearLayout linear_= ((SubscriptionViewHolder)holder).linear;

                LinearLayout linear_container= linear_.findViewById(R.id.linear_container);
                linear_container.removeAllViews();

                WebView txt_left= linear_.findViewById(R.id.txt_left);
                if(list.get(index).getSubscriptionCount()!=null &&
                        !list.get(index).getSubscriptionCount().equalsIgnoreCase("")){
                    txt_left.setVisibility(View.VISIBLE);
                    txt_left.loadData(list.get(index).getSubscriptionCount(), "text/html", "utf-8");
                    txt_left.setBackgroundColor(Color.TRANSPARENT);
                }else
                    txt_left.setVisibility(View.GONE);

                for(int i=0;i<list.get(index).getList().size();i++){

                    CardView cardView_= (CardView) LayoutInflater.from(linear_container.getContext())
                            .inflate(R.layout.layout_subscription, linear_container, false);

                    HtmlTextView txt_heading1= cardView_.findViewById(R.id.txt_heading1);
                    HtmlTextView txt_heading2= cardView_.findViewById(R.id.txt_heading2);
                    TextView txt_more= cardView_.findViewById(R.id.txt_more);
                    TextView txt_subscribe= cardView_.findViewById(R.id.txt_subscribe);
                    ImageView img_type= cardView_.findViewById(R.id.img_type);
                    LinearLayout linear_points= cardView_.findViewById(R.id.linear_points);
                    linear_points.removeAllViews();

                    txt_heading1.setHtml(list.get(index).getList().get(i).getHeading1HTML());
                    txt_heading2.setHtml(list.get(index).getList().get(i).getHeading2HTML());

                    if(list.get(index).getList().get(i).getTitle().equalsIgnoreCase("GOLD"))
                        img_type.setImageResource(R.drawable.ic_gold);
                    else
                        img_type.setImageResource(R.drawable.ic_silver);

                    for(int j=0;j<list.get(index).getList().get(i).getPoints().size();j++){

                        LinearLayout linear_points_= (LinearLayout) LayoutInflater.from(linear_container.getContext())
                                .inflate(R.layout.layout_points, linear_container, false);

                        TextView txt_name= linear_points_.findViewById(R.id.txt_name);
                        txt_name.setText(list.get(index).getList().get(i).getPoints().get(j));

                        linear_points.addView(linear_points_);
                    }


                    txt_more.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            view.getContext().startActivity(new Intent(view.getContext(), SubscriptionActivity.class));
                        }
                    });

                    final int pos= i;
                    txt_subscribe.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            saveDataToCart(view.getContext(), list.get(index).getList().get(pos));
                        }
                    });


                    linear_container.addView(cardView_);
                }



            }break;
            case BANNER:{
                final LinearLayout linear_= ((BannerViewHolder)holder).linear;

                LinearLayout linear_c= linear_.findViewById(R.id.linear_c);
                linear_c.removeAllViews();
                int size= list.get(index).getList().size();
                for(int i=0; i<size;i++){
                    final int pos= i;

                    CardView cardView= (CardView) LayoutInflater.from(linear_.getContext()).inflate(R.layout.img_carousel, null, false);
                    final ImageView img_= cardView.findViewById(R.id.img_);
                    Glide.with(linear_.getContext())
                            .load(list.get(index).getList().get(pos).getImageUrl())
                            .into(img_);

                    img_.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if(list.get(index).getList().get(pos).getAction()
                                    .equalsIgnoreCase(CAROUSEL_OPEN_PARLOR)){

                                if(linear_.getContext()!=null){

                                    Intent intent= new Intent(linear_.getContext(), ParlorListActivity.class);
                                    intent.putExtra("isDeal", true);
                                    linear_.getContext().startActivity(intent);
                                }

                            }else if(list.get(index).getList().get(pos).getAction()
                                    .equalsIgnoreCase(CAROUSEL_OPEN_DEALS)){


//                                                addDeal(response.body().getData().getBanner().getList().get(pos));

                            }else if(list.get(index).getList().get(pos).getAction()
                                    .equalsIgnoreCase(CAROUSEL_OPEN_FREEBIES)){

                                if(linear_.getContext()!=null){
                                    MainActivity.isFreebieHome = true;
                                    AHBottomNavigation bottomNavigation= MainActivity.getBottomNav();
                                    bottomNavigation.setCurrentItem(2);
                                }

                            }else if(list.get(index).getList().get(pos).getAction()
                                    .equalsIgnoreCase(CAROUSEL_OPEN_CORPORATE)){

                                linear_.getContext().startActivity(new Intent(linear_.getContext(), CorporateLoginActivity.class));

                            }else if(list.get(index).getList().get(pos).getAction()
                                    .equalsIgnoreCase(CAROUSEL_OPEN_MEMBERSHIP)){

                                Intent in=new Intent(linear_.getContext(), MemberShipCardAcitvity.class);
                                in.putExtra("from_home", true);
                                linear_.getContext().startActivity(in);

                            }else if(list.get(index).getList().get(pos).getAction()
                                    .equalsIgnoreCase(CAROUSEL_OPEN_SUBSCRIPTION)){

                                Intent in=new Intent(linear_.getContext(), SubscriptionActivity.class);
                                in.putExtra("from_home", true);
                                linear_.getContext().startActivity(in);

                            }else if(list.get(index).getList().get(pos).getAction()
                                    .equalsIgnoreCase(CAROUSEL_OPEN_DEAL_PAGE)){

                                if(linear_.getContext()!=null){
                                    MainActivity.isFreebieHome = true;
                                    AHBottomNavigation bottomNavigation= MainActivity.getBottomNav();
                                    bottomNavigation.setCurrentItem(1);
                                }
                            }


                        }
                    });

                    linear_c.addView(cardView);

                }

            }break;
            case FREEBIES:{
                LinearLayout linear_= ((FreebiesViewHolder)holder).linear;

                TextView txt_free_service, txt_cash, txt_subscription, txt_coupons,
                        txt_expiry1, txt_expiry2, txt_expiry3, txt_expiry4;
                txt_free_service = linear_.findViewById(R.id.txt_free_service);
                txt_cash = linear_.findViewById(R.id.txt_cash);
                txt_subscription = linear_.findViewById(R.id.txt_subscription);
                txt_coupons = linear_.findViewById(R.id.txt_coupons);

//                ImageView img_= linear_.findViewById(R.id.img_);
//                img_.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        AHBottomNavigation bottomNavigation= MainActivity.getBottomNav();
//                        bottomNavigation.setCurrentItem(2);
//                    }
//                });

                txt_expiry1 = linear_.findViewById(R.id.txt_expiry1);
                txt_expiry1.setTypeface(null, Typeface.ITALIC);
                txt_expiry2 = linear_.findViewById(R.id.txt_expiry2);
                txt_expiry2.setTypeface(null, Typeface.ITALIC);
                txt_expiry3 = linear_.findViewById(R.id.txt_expiry3);
                txt_expiry3.setTypeface(null, Typeface.ITALIC);
                txt_expiry4 = linear_.findViewById(R.id.txt_expiry4);
                txt_expiry4.setTypeface(null, Typeface.ITALIC);

                if(list.get(index).getScorecard().getList().get(0).getFreebieExpiry()!=null &&
                        !list.get(index).getScorecard().getList().get(0).getFreebieExpiry().equalsIgnoreCase(""))
                    txt_expiry1.setText("Expiry: "+formatDateTime(list.get(index).getScorecard().getList().get(0).getFreebieExpiry()));
                if(list.get(index).getScorecard().getList().get(0).getFreeServiceExpiry()!=null &&
                        !list.get(index).getScorecard().getList().get(0).getFreeServiceExpiry().equalsIgnoreCase(""))
                    txt_expiry2.setText("Expiry: "+formatDateTime(list.get(index).getScorecard().getList().get(0).getFreeServiceExpiry()));
                if(list.get(index).getScorecard().getList().get(0).getSubscriptionExpiry()!=null &&
                        !list.get(index).getScorecard().getList().get(0).getSubscriptionExpiry().equalsIgnoreCase(""))
                    txt_expiry3.setText("Expiry: "+formatDateTime(list.get(index).getScorecard().getList().get(0).getSubscriptionExpiry()));
                if(list.get(index).getScorecard().getList().get(0).getCouponExpiry()!=null &&
                        !list.get(index).getScorecard().getList().get(0).getCouponExpiry().equalsIgnoreCase(""))
                    txt_expiry4.setText("Expiry: "+formatDateTime(list.get(index).getScorecard().getList().get(0).getCouponExpiry()));

                if(list.get(index).getScorecard().getList().get(0).getFreeServices()>0)
                    txt_free_service.setText(""+(int)list.get(index).getScorecard().getList().get(0).getFreeServices());
                else
                    txt_free_service.setText("-");
                if(list.get(index).getScorecard().getList().get(0).getCoupons()>0)
                    txt_coupons.setText(""+(int)list.get(index).getScorecard().getList().get(0).getCoupons());
                else
                    txt_coupons.setText("-");
                if(list.get(index).getScorecard().getList().get(0).getLoyalityPoints()>0)
                    txt_cash.setText(""+(int)list.get(index).getScorecard().getList().get(0).getLoyalityPoints());
                else
                    txt_cash.setText("-");
                if(list.get(index).getScorecard().getList().get(0).getSubscription()>0)
                    txt_subscription.setText(""+(int)list.get(index).getScorecard().getList().get(0).getSubscription());
                else
                    txt_subscription.setText("-");


                LinearLayout linear_cash= linear_.findViewById(R.id.linear_cash);
                linear_cash.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        AHBottomNavigation bottomNavigation= MainActivity.getBottomNav();
                        bottomNavigation.setCurrentItem(2);
                    }
                });

                LinearLayout linear_subs= linear_.findViewById(R.id.linear_subs);
                linear_subs.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        view.getContext().startActivity(new Intent(view.getContext(), SubscriptionActivity.class));
                    }
                });

                LinearLayout linear_service= linear_.findViewById(R.id.linear_service);
                linear_service.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        AHBottomNavigation bottomNavigation= MainActivity.getBottomNav();
                        bottomNavigation.setCurrentItem(2);
                    }
                });

                LinearLayout linear_coupon= linear_.findViewById(R.id.linear_coupon);
                linear_coupon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        AHBottomNavigation bottomNavigation= MainActivity.getBottomNav();
                        bottomNavigation.setCurrentItem(3);
                    }
                });


            }break;
            case SEARCH:{
                LinearLayout linear_= ((SearchViewHolder)holder).linear;
                LinearLayout linear_click= linear_.findViewById(R.id.linear_);
                linear_click.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent= new Intent(view.getContext(), ParlorListActivity.class);
                        intent.putExtra("isService",true);
                        view.getContext().startActivity(intent);
                    }
                });

            }break;
            case PROGRESS:{
                LinearLayout linear_= ((ProgressViewHolder)holder).linear;
            }break;
            default:
                LinearLayout linear_= ((DefaultViewHolder)holder).linear;

        }

    }

    private void openSalon(final Context context, final String salon_id, String salon_name){


        boolean show_dialog= false;
        UserCart saved_cart = null;
        //opening cart, checking cart type equal to service type, and parlor id are same, if not same show dialog
        try {

            DB snappyDB = DBFactory.open(context);
            //db mai jo saved cart hai
            if (snappyDB.exists(AppConstant.USER_CART_DB))
                saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);


            if(saved_cart!=null){

                if(saved_cart.getCartType()!=null &&
                        saved_cart.getCartType().equalsIgnoreCase(AppConstant.SERVICE_TYPE)){

                    if(saved_cart.getParlorId().equalsIgnoreCase(salon_id))
                        show_dialog= false;
                    else{

                        if(saved_cart.getServicesList().size()>0)
                            show_dialog= true;
                        else
                            show_dialog= false;
                    }
                }else{
                    if(snappyDB.exists(AppConstant.USER_CART_DB))
                        snappyDB.del(AppConstant.USER_CART_DB);
                }
            }else
                show_dialog= false;

            snappyDB.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        if(show_dialog){

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Replace cart item?");
            builder.setMessage("Your cart contains services from "+ saved_cart.getParlorName()+". Do you " +
                    "wish to discard the selection and add services from "+ salon_name+"?");
            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(final DialogInterface dialog, int id) {

                    try {

                        DB snappyDB = DBFactory.open(context);
                        snappyDB.destroy();
                        snappyDB.close();

                        EventBus.getDefault().post(new CartNotificationEvent());
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    Intent intent = new Intent(context, SalonPageActivity.class);
                    intent.putExtra("parlorId", salon_id);
                    context.startActivity(intent);

                }
            });
            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    dialog.dismiss();
                }
            }).show();

        }else{
            Intent intent = new Intent(context, SalonPageActivity.class);
            intent.putExtra("parlorId", salon_id);
            context.startActivity(intent);
        }

    }


    public void saveDataToCart(Context context, List_ data){

        Toast.makeText(context, data.getTitle()+ " Added!", Toast.LENGTH_SHORT).show();
        final AppointmentPost appointmentPost= new AppointmentPost();

        appointmentPost.setUserId(BeuSalonsSharedPrefrence.getUserId());
        appointmentPost.setAccessToken(BeuSalonsSharedPrefrence.getAccessToken());
        appointmentPost.setLatitude(BeuSalonsSharedPrefrence.getLatitude());
        appointmentPost.setLongitude(BeuSalonsSharedPrefrence.getLongitude());
        Calendar calendar= Calendar.getInstance();
        Date date= calendar.getTime();
        DateFormat date_format= new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss");
        appointmentPost.setDatetime(date_format.format(date)+" GMT+0530 (India Standard Time)");
        appointmentPost.setParlorId("594a359d9856d3158171ea4f");
        appointmentPost.setMode(1);                 //mode 1 for android, 2 for ios
        appointmentPost.setPaymentMethod(5);
        appointmentPost.setSubscriptionId(data.getSubscriptionId());
        Intent intent= new Intent(context, BillSummaryActivity.class);
        intent.putExtra("appointment_post", new Gson().toJson(appointmentPost));
        context.startActivity(intent);
//        try {
//            DB snappyDB = DBFactory.open(context);
//            UserCart saved_cart= null;               //db mai jo saved cart hai
//            if (snappyDB.exists(AppConstant.USER_CART_DB)) {
//                saved_cart = snappyDB.getObject(AppConstant.USER_CART_DB, UserCart.class);
//                if(saved_cart.getCartType().equalsIgnoreCase(AppConstant.SERVICE_TYPE)||            //deleting cart
//                        saved_cart.getCartType().equalsIgnoreCase(AppConstant.DEAL_TYPE))
//                    snappyDB.del(AppConstant.USER_CART_DB);
//            }
//
//            UserCart cart= new UserCart();
//            cart.setCartType(AppConstant.OTHER_TYPE);
//
//            UserServices service = new UserServices();
//            service.setName(data.getTitle());
//            service.setSubscriptionId(data.getSubscriptionId());
//            service.setDescription(data.getHeading2()+ " | Recommended: "+data.getRecommended());
//            service.setPrice(data.getAmount());
//            service.setType("subscription");
//            service.setSubscription(true);
//            service.setPrimary_key("subscription_"+data.getSubscriptionId());
//
//            new Thread(new UserCartTask(context, cart, service, false, false)).start();
//
//        }catch (Exception e){
//            e.printStackTrace();
//        }


    }



    @Override
    public int getItemCount() {

        if(list!=null &&
                list.size()>0)
            return list.size();
        return 0;
    }

    @Override
    public int getItemViewType(int position) {

        if(list!=null &&
                list.size()>0)
            return list.get(position).getType_index();
        return 0;
    }

    public class ParlorViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout linear;
        public ParlorViewHolder(View itemView) {
            super(itemView);
            linear= (LinearLayout)itemView;
        }
    }

    public class SubscriptionViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout linear;
        public SubscriptionViewHolder(View itemView) {
            super(itemView);
            linear= (LinearLayout)itemView;
        }
    }

    public class BannerViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout linear;
        public BannerViewHolder(View itemView) {
            super(itemView);
            linear= (LinearLayout)itemView;
        }
    }

    public class DefaultViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout linear;
        public DefaultViewHolder(View itemView) {
            super(itemView);
            linear= (LinearLayout)itemView;
        }
    }

    public class FreebiesViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout linear;
        public FreebiesViewHolder(View itemView) {
            super(itemView);
            linear= (LinearLayout)itemView;
        }
    }

    public class SearchViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout linear;
        public SearchViewHolder(View itemView) {
            super(itemView);
            linear= (LinearLayout)itemView;
        }
    }


    public class ProgressViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout linear;
        public ProgressViewHolder(View itemView) {
            super(itemView);
            linear= (LinearLayout)itemView;
        }
    }

    private String formatDateTime(String str_date){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        Date date_format=null;
        try {
            date_format = sdf.parse(str_date);
            Calendar cal= Calendar.getInstance();       //creating calendar instance
            cal.setTime(date_format);
            cal.add(Calendar.HOUR_OF_DAY, 5);
            cal.add(Calendar.MINUTE, 30);

            date_format= cal.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("dd/MM/yyyy").format(date_format);
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }
}
