package com.beusalons.android.Adapter.Songs;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Event.MembershipEvent.Event;
import com.beusalons.android.Event.SongEvent;
import com.beusalons.android.Model.Songs.AllSongs;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.R;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by myMachine on 27-Dec-17.
 */

public class AllSongsAdapter extends RecyclerView.Adapter<AllSongsAdapter.ViewHolder> {

    private List<AllSongs.Song> list;

    public AllSongsAdapter(List<AllSongs.Song> list){
        this.list= list;
    }

    public interface ClickListener{
        void onClick(int id);
    }
    private ClickListener listener;

    public void setListener(ClickListener listener){
        this.listener= listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view_= LayoutInflater.from(parent.getContext()).
                inflate(R.layout.song_tile, null, false);
        return new ViewHolder(view_);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final int index= position;
        final LinearLayout linear_= holder.linear;
        TextView txt_name= linear_.findViewById(R.id.txt_name);
        txt_name.setText(list.get(index).getName());
        linear_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(listener!=null)
                    listener.onClick(list.get(index).getSongId());
//                EventBus.getDefault().post(new SongEvent(list.get(index).getSongId()));
            }
        });

        View view_= linear_.findViewById(R.id.view_);
        if(list.size()-1==position)
            view_.setVisibility(View.INVISIBLE);
        else
            view_.setVisibility(View.VISIBLE);

    }

    @Override
    public int getItemCount() {

        if(list!=null &&
                list.size()>0)
            return list.size();
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private LinearLayout linear;
        public ViewHolder(View itemView) {
            super(itemView);
            linear= (LinearLayout)itemView;
        }
    }

}
