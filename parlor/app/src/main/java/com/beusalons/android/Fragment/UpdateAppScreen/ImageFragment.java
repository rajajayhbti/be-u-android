package com.beusalons.android.Fragment.UpdateAppScreen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Model.UpdateAppResponse;
import com.beusalons.android.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

/**
 * Created by myMachine on 19-Feb-18.
 */

public class ImageFragment extends Fragment {

    private String url;

    public static ImageFragment newInstance(String url){

        ImageFragment fragment= new ImageFragment();
        fragment.url= url;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ImageView img_= (ImageView) inflater.inflate(R.layout.fragment_image, container, false);

        Glide.with(img_.getContext())
                .load(url)
                .apply( new RequestOptions().centerCrop())
                .into(img_);

        return img_;
    }
}
