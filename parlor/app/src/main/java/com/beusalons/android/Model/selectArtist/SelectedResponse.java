package com.beusalons.android.Model.selectArtist;

/**
 * Created by myMachine on 15-Mar-18.
 */

public class SelectedResponse {

    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
