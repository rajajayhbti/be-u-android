package com.beusalons.android.Model.OnlinePay;

/**
 * Created by Ashish Sharma on 5/30/2017.
 */

public class UseFreeBiesPost {

    private String accessToken;
    private String userId;
    private String appointmentId;
    private boolean useLoyalityPoints;
    private Integer paymentMethod;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public boolean isUseLoyalityPoints() {
        return useLoyalityPoints;
    }

    public void setUseLoyalityPoints(boolean useLoyalityPoints) {
        this.useLoyalityPoints = useLoyalityPoints;
    }

    public Integer getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Integer paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}
