package com.beusalons.android.Model.AllDeals;

import java.util.List;

/**
 * Created by myMachine on 4/28/2017.
 */

public class Service {

    public String dealId;
    public String name;
    public Integer sort;
    public String category;
    public String gender;
    public Integer dealIdParlor;
    public String description;
    public Integer menuPrice;
    public Integer dealPrice;
    public double tax;
    public double dealPercentage;
    public Integer dealSort;
    public Integer weekDay;
    public DealType dealType;
    public List<Service_> services = null;
    public Object couponCode;
    public Integer type;

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getDealIdParlor() {
        return dealIdParlor;
    }

    public void setDealIdParlor(Integer dealIdParlor) {
        this.dealIdParlor = dealIdParlor;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(Integer menuPrice) {
        this.menuPrice = menuPrice;
    }

    public Integer getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(Integer dealPrice) {
        this.dealPrice = dealPrice;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getDealPercentage() {
        return dealPercentage;
    }

    public void setDealPercentage(double dealPercentage) {
        this.dealPercentage = dealPercentage;
    }

    public Integer getDealSort() {
        return dealSort;
    }

    public void setDealSort(Integer dealSort) {
        this.dealSort = dealSort;
    }

    public Integer getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(Integer weekDay) {
        this.weekDay = weekDay;
    }

    public DealType getDealType() {
        return dealType;
    }

    public void setDealType(DealType dealType) {
        this.dealType = dealType;
    }

    public List<Service_> getServices() {
        return services;
    }

    public void setServices(List<Service_> services) {
        this.services = services;
    }

    public Object getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(Object couponCode) {
        this.couponCode = couponCode;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
