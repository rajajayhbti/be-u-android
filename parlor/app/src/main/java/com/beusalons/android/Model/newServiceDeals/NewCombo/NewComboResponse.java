package com.beusalons.android.Model.newServiceDeals.NewCombo;

/**
 * Created by myMachine on 6/17/2017.
 */

public class NewComboResponse {

    private Boolean success;
    private Data data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
