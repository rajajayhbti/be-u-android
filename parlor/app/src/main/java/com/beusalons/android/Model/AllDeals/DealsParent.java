package com.beusalons.android.Model.AllDeals;

import java.util.List;

/**
 * Created by myMachine on 4/28/2017.
 */

public class DealsParent {


    private String name;
    private List<DealsChild> list;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DealsChild> getList() {
        return list;
    }

    public void setList(List<DealsChild> list) {
        this.list = list;
    }
}
