package com.beusalons.android.Model.Reviews;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 11/19/2016.
 */

public class ParlorReviewResponse {


    private Boolean success;
    private String message;
    private List<ParlorReviewData> data = new ArrayList<>();

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ParlorReviewData> getData() {
        return data;
    }

    public void setData(List<ParlorReviewData> data) {
        this.data = data;
    }
}
