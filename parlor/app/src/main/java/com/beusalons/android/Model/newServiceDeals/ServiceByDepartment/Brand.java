package com.beusalons.android.Model.newServiceDeals.ServiceByDepartment;

import java.util.List;

/**
 * Created by myMachine on 5/30/2017.
 */

public class Brand {

    private List<Brand_> brands = null;
    private String title;

    public List<Brand_> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand_> brands) {
        this.brands = brands;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
