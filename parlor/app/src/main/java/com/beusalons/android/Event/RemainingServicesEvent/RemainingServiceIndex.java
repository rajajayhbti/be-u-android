package com.beusalons.android.Event.RemainingServicesEvent;

/**
 * Created by myMachine on 10/11/2017.
 */

public class RemainingServiceIndex {

    private int parlor_position;
    private int service_position;

    public RemainingServiceIndex(int parlor_position, int service_position){
        this.parlor_position= parlor_position;
        this.service_position= service_position;
    }

    public int getParlor_position() {
        return parlor_position;
    }

    public int getService_position() {
        return service_position;
    }
}
