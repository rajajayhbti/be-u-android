package com.beusalons.android.Model.newServiceDeals.ServiceByDepartment;

import java.util.List;

/**
 * Created by myMachine on 5/30/2017.
 */

public class Brand_ {

    private String productTitle;
    private double ratio;
    private List<Product> products = null;
    private String name;
    private String brandId;
    private Double dealRatio;

    private boolean isCheck= false;         //radio button check not check ke liye

    private boolean isDeal;                 //ager deal applied hai toh isseh check karunga


    private int price;
    private int menu_price;
    private int save_per;

    private String service_name;
    private String service_deal_id;
    private String type;                        //service or deal type
    private int service_code;
    private int price_id;

    private double brand_ratio;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getBrand_ratio() {
        return brand_ratio;
    }

    public void setBrand_ratio(double brand_ratio) {
        this.brand_ratio = brand_ratio;
    }

    public String getService_deal_id() {
        return service_deal_id;
    }

    public void setService_deal_id(String service_deal_id) {
        this.service_deal_id = service_deal_id;
    }

    public int getService_code() {
        return service_code;
    }

    public void setService_code(int service_code) {
        this.service_code = service_code;
    }

    public int getPrice_id() {
        return price_id;
    }

    public void setPrice_id(int price_id) {
        this.price_id = price_id;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getMenu_price() {
        return menu_price;
    }

    public void setMenu_price(int menu_price) {
        this.menu_price = menu_price;
    }

    public int getSave_per() {
        return save_per;
    }

    public void setSave_per(int save_per) {
        this.save_per = save_per;
    }

    public boolean isDeal() {
        return isDeal;
    }

    public void setDeal(boolean deal) {
        isDeal = deal;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public double getRatio() {
        return ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public Double getDealRatio() {
        return dealRatio;
    }

    public void setDealRatio(Double dealRatio) {
        this.dealRatio = dealRatio;
    }
}
