package com.beusalons.android.Model.subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ashish Sharma on 1/23/2018.
 */

public class SubsData {
    @SerializedName("detail")
    @Expose
    private SubsDetail detail;
    @SerializedName("selectSubscription")
    @Expose
    private SelectSubscription selectSubscription;
    @SerializedName("availSteps")
    @Expose
    private AvailSteps availSteps;
    @SerializedName("faq")
    @Expose
    private Faq faq;

    public SubsDetail getDetail() {
        return detail;
    }

    public void setDetail(SubsDetail detail) {
        this.detail = detail;
    }

    public SelectSubscription getSelectSubscription() {
        return selectSubscription;
    }

    public void setSelectSubscription(SelectSubscription selectSubscription) {
        this.selectSubscription = selectSubscription;
    }

    public AvailSteps getAvailSteps() {
        return availSteps;
    }

    public void setAvailSteps(AvailSteps availSteps) {
        this.availSteps = availSteps;
    }

    public Faq getFaq() {
        return faq;
    }

    public void setFaq(Faq faq) {
        this.faq = faq;
    }
}
