package com.beusalons.android.Model.AboutUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myMachine on 17-Feb-18.
 */

public class QuestionAns{

    private String questionId;
    private List<String> answers= new ArrayList<>();

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }
}