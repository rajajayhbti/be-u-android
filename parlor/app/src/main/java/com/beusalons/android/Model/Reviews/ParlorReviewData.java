package com.beusalons.android.Model.Reviews;

/**
 * Created by myMachine on 1/16/2017.
 */

public class ParlorReviewData {

    public static final int DATA_TYPE = 0;            //default yeh hai
    public static final int PROGRESS_TYPE = 1;

    private String userName;
    private String userImage;
    private String text;
    private double rating;
    private String time;
    private String createAt;
    private  String appointmentId;


    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    private int type= 0;

    public ParlorReviewData(int type){
        this.type= type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
