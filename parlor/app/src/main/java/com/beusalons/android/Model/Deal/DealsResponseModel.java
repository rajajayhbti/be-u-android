package com.beusalons.android.Model.Deal;

import com.beusalons.android.Model.StatusModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ajay on 12/29/2016.
 */

public class DealsResponseModel implements Serializable{

    @SerializedName("data")
    @Expose
    private List<DealsDataModel> data;
    private boolean success;
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DealsDataModel> getData() {
        return data;
    }

    public void setData(List<DealsDataModel> data) {
        this.data = data;
    }



}
