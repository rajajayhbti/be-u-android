package com.beusalons.android.Model.DealsSalonList;

import java.util.List;

/**
 * Created by myMachine on 6/28/2017.
 */

public class  Data {

    private List<Parlor> parlors = null;

    public List<Parlor> getParlors() {
        return parlors;
    }

    public void setParlors(List<Parlor> parlors) {
        this.parlors = parlors;
    }
}
