package com.beusalons.android.Model.MemberShip;

import com.beusalons.android.Model.SalonHome.WelcomeOffer;

import java.util.List;

/**
 * Created by Ajay on 5/26/2017.
 */

public class MemberShipData {

    private List<Membership> memberships = null;
    private WelcomeOffer welcomeOffer;

    public List<Membership> getMemberships() {
        return memberships;
    }

    public void setMemberships(List<Membership> memberships) {
        this.memberships = memberships;
    }

    public WelcomeOffer getWelcomeOffer() {
        return welcomeOffer;
    }

    public void setWelcomeOffer(WelcomeOffer welcomeOffer) {
        this.welcomeOffer = welcomeOffer;
    }
}
