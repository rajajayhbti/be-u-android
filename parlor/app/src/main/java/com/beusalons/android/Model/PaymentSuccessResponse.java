package com.beusalons.android.Model;

/**
 * Created by myMachine on 1/21/2017.
 */

public class PaymentSuccessResponse {

    private Boolean success;
    private String data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
