package com.beusalons.android.Model.Corporate.CorporateVerifyOtp;

/**
 * Created by myMachine on 7/31/2017.
 */

public class CorporateVerifyPost {

    private String userId;
    private String accessToken;
    private String emailId;
    private String otp;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
