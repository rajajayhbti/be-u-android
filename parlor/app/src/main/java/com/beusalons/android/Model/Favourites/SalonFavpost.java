package com.beusalons.android.Model.Favourites;

/**
 * Created by myMachine on 3/31/2017.
 */

public class SalonFavpost {


    private String userId;
    private String accessToken;
    private String parlorId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getParlorId() {
        return parlorId;
    }

    public void setParlorId(String parlorId) {
        this.parlorId = parlorId;
    }
}
