package com.beusalons.android.Model.AppointmentDetail;

/**
 * Created by myMachine on 7/12/2017.
 */

public class UsableMembership {

    private String name;
    private double credits;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCredits() {
        return credits;
    }

    public void setCredits(double credits) {
        this.credits = credits;
    }
}
