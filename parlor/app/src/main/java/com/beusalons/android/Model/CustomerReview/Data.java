package com.beusalons.android.Model.CustomerReview;

import java.util.List;

/**
 * Created by myMachine on 9/13/2017.
 */

public class Data {

    private List<Employee> employees = null;

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}
