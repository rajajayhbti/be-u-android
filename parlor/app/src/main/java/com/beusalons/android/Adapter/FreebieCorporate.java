package com.beusalons.android.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beusalons.android.Fragment.FreebiesUpgradeFragment;
import com.beusalons.android.Helper.AppConstant;
import com.beusalons.android.MainActivity;
import com.beusalons.android.Model.Loyalty.FreeCorporateService;
import com.beusalons.android.Model.UserCart.UserCart;
import com.beusalons.android.Model.UserCart.UserServices;
import com.beusalons.android.ParlorListActivity;
import com.beusalons.android.R;
import com.beusalons.android.Task.UserCartTask;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by myMachine on 8/22/2017.
 */

public class FreebieCorporate extends RecyclerView.Adapter<FreebieCorporate.ViewHolder> {

    private List<FreeCorporateService> list;

    public FreebieCorporate(List<FreeCorporateService> list){

        this.list= list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LinearLayout linear_= (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.freebie_rec, parent, false);
        return new ViewHolder(linear_);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final LinearLayout linear_= holder.linear_;
        TextView txt_name= (TextView)linear_.findViewById(R.id.txt_name);
        TextView txt_avail= (TextView)linear_.findViewById(R.id.txt_avail);
        TextView txt_upgrade= (TextView)linear_.findViewById(R.id.txt_upgrade);
        TextView txt_description= (TextView)linear_.findViewById(R.id.txt_description);

        LinearLayout linear_upgrade= (LinearLayout)linear_.findViewById(R.id.linear_upgrade);
        LinearLayout linear_avail= (LinearLayout)linear_.findViewById(R.id.linear_avail);
        View view_= linear_.findViewById(R.id.view_);

        if(position== list.size()-1)
            view_.setVisibility(View.GONE);
        else
            view_.setVisibility(View.VISIBLE);


        txt_name.setText(list.get(position).getName());
        txt_description.setText(list.get(position).getDescription());

        txt_upgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(list.get(position).getUpgrade()!=null &&
                        list.get(position).getUpgrade().size()>0){
                    FreebiesUpgradeFragment bottomSheet= new FreebiesUpgradeFragment();
                    Bundle bundle= new Bundle();
                    bundle.putString("upgrade",
                            new Gson().toJson(list.get(position),
                                    FreeCorporateService.class));
                    bottomSheet.setArguments(bundle);
                    bottomSheet.show(((MainActivity)linear_.getContext()).getSupportFragmentManager(), "upgrade_freebie");
                }
            }
        });

        txt_avail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                availService(linear_.getContext(), position);
            }
        });

        if(list.get(position).isAvailed()){
            txt_avail.setEnabled(true);
            txt_name.setTextColor(ContextCompat.getColor(linear_.getContext(), R.color.black));
            txt_avail.setTextColor(ContextCompat.getColor(linear_.getContext(), R.color.colorPrimary));
            linear_avail.setBackground(ContextCompat.getDrawable(linear_.getContext(),
                    R.drawable.shape_freebies_red));
        }else{
            txt_avail.setEnabled(false);
            txt_name.setTextColor(ContextCompat.getColor(linear_.getContext(), R.color.textLight));
            txt_avail.setTextColor(ContextCompat.getColor(linear_.getContext(), R.color.textLight));
            linear_avail.setBackground(ContextCompat.getDrawable(linear_.getContext(),
                    R.drawable.shape_freebie_disabled));
        }

        if(list.get(position).getEnableUpgrade() && list.get(position).getUpgrade()!=null &&
                list.get(position).getUpgrade().size()>0)
            linear_upgrade.setVisibility(View.VISIBLE);
        else
            linear_upgrade.setVisibility(View.INVISIBLE);
    }

    private void availService(Context context, int pos){

        UserServices services= new UserServices();
        services.setName(list.get(pos).getName());
        services.setDescription(list.get(pos).getDescription());

        services.setDealId(list.get(pos).getDealId());
        services.setService_deal_id(list.get(pos).getServiceId());

        services.setService_code(list.get(pos).getServiceCode());
        services.setPrice_id(list.get(pos).getPriceId());

        String brandId= list.get(pos).getBrandId()!=null &&
                !list.get(pos).getBrandId().equalsIgnoreCase("")?list.get(pos).getBrandId():"";
        String productId= list.get(pos).getProductId()!=null && !list.get(pos).getProductId().equalsIgnoreCase("")?
                list.get(pos).getProductId():"";

        services.setBrand_id(brandId);
        services.setProduct_id(productId);

        services.setType("deal");
        services.setFree_service(true);

        services.setPrimary_key(services.getService_code()+"_"+services.getService_deal_id()+brandId+productId);

        services.setPrice(list.get(pos).getPrice());
        services.setMenu_price(list.get(pos).getPrice());

        UserCart userCart= new UserCart();
        userCart.setCartType(AppConstant.DEAL_TYPE);

        new Thread(new UserCartTask(context, userCart, services, false, false)).start();

        Intent intent= new Intent(context, ParlorListActivity.class);
        intent.putExtra("isService", true);
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private LinearLayout linear_;
        public ViewHolder(LinearLayout itemView) {
            super(itemView);
            linear_= itemView;
        }
    }


}
