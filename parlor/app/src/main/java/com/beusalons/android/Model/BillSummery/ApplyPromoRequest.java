package com.beusalons.android.Model.BillSummery;

/**
 * Created by Ashish Sharma on 4/19/2017.
 */

public class ApplyPromoRequest {

    private String userId;
    private String accessToken;
    private String couponCode;
    private String appointmentId;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getApppointment() {
        return appointmentId;
    }

    public void setApppointment(String apppointment) {
        this.appointmentId = apppointment;
    }
}
