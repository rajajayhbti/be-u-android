package com.beusalons.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beusalons.android.Model.Send_Otp.Send_Otp_Post;
import com.beusalons.android.Model.Send_Otp.Send_Otp_Response;
import com.beusalons.android.Model.SocialLogin.LoginResponse;
import com.beusalons.android.Model.Verify_Otp.Verify_Otp_Post;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.BeuSalonsSharedPrefrence;
import com.beusalons.android.Utility.Utility;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class OtpVerificationActivity extends AppCompatActivity {

    private TextView txt_text, txt_timer,txtViewActionBarName,txt_send_otp,txt_send_otpByCall;
    private ProgressBar progressBar, progress_verify;
    private ImageView imageView,imgViewBack;
    private EditText etxt_one, etxt_two, etxt_three, etxt_four;
    private Button btn_continue_active, btn_continue_inactive;
    private View relative, linear;
    private LinearLayout ll_txt_otp_by_cal,ll_txt_otp_by_sms;
    private RelativeLayout linearLayout;
    AppEventsLogger logger;
    private static String string_phone, str_token, str_gender;
    private static int int_socialType;


    final private int SMS_PERMISSION = 5;


    private int send_retry= 0, otp_retry=0;

   /* BroadcastReceiver broadcastReceiver= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            readOtp();
        }
    };*/

    final IntentFilter intentFilter= new IntentFilter();
    private int OtpSendingMode=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        //new ToolbarHelper(getSupportActionBar(), getLayoutInflater(), true, this);
        setToolBar();
        Intent intent= getIntent();
        if(intent!=null){
            string_phone= intent.getStringExtra("phoneNumber");
            str_gender= intent.getStringExtra("gender");
            str_token= intent.getStringExtra("socialToken").equalsIgnoreCase("")?null: intent.getStringExtra("socialToken");
            int_socialType= intent.getIntExtra("socialType", 0);
            Log.i("otpverifica", "value in phone number: "+ string_phone);
        }
        logger = AppEventsLogger.newLogger(this);
        relative= findViewById(R.id.relative);
        linear= findViewById(R.id.linear);
        linearLayout=(RelativeLayout)findViewById(R.id.linearLayout_resend_otp);
        ll_txt_otp_by_cal=(LinearLayout)findViewById(R.id.ll_txt_otp_by_cal);
        ll_txt_otp_by_sms=(LinearLayout)findViewById(R.id.ll_txt_otp_by_sms);
        txt_text= (TextView)findViewById(R.id.txt_text);
        txt_send_otp= (TextView)findViewById(R.id.txt_send_otp);
        txt_send_otpByCall=(TextView)findViewById(R.id.txt_send_otp_bycall);
        txt_timer=(TextView)findViewById(R.id.txt_timer);
        progressBar= (ProgressBar)findViewById(R.id.progress_bar);
        progress_verify= (ProgressBar)findViewById(R.id.progress_verify);
        progress_verify.setVisibility(View.GONE);

        imageView= (ImageView)findViewById(R.id.img);

        linearLayout.setVisibility(View.VISIBLE);

//        txt_send_otp.setVisibility(View.VISIBLE);
//        txt_send_otp.setClickable(true);
//        txt_send_otp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                relative.setVisibility(View.GONE);
////                linear.setVisibility(View.VISIBLE);
////                txt_text.setText("Sending your OTP");
//                txt_send_otp.setClickable(false);
//                sendOtp(string_phone);
//            }
//        });
        //initialize the appsflyers
   //     AppsFlyerLib.getInstance().startTracking(this.getApplication(), getResources().getString(R.string.devKey));
        btn_continue_inactive= (Button)findViewById(R.id.btn_continue_inactive);
        btn_continue_active= (Button)findViewById(R.id.btn_continue_active);

        btn_continue_inactive.setVisibility(View.VISIBLE);
        btn_continue_inactive.setClickable(false);

        btn_continue_active.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String otp= ""+etxt_one.getText()+etxt_two.getText()+etxt_three.getText()+etxt_four.getText();
                Log.i("otpverificati", "value in the otp: "+otp);
                verifyOtp(string_phone, otp );
            }
        });
        String txtSMS="Sent OTP by"+"<b><font color='#00B8CA' >"+" SMS"+"</font></b>";
        txt_send_otp.setText(Html.fromHtml(txtSMS), TextView.BufferType.SPANNABLE);
        String txtCall="Sent OTP by"+"<b><font color='#00B8CA' >"+" CALL"+"</font></b>";
        txt_send_otpByCall.setText(Html.fromHtml(txtCall), TextView.BufferType.SPANNABLE);
        etxt_one= (EditText)findViewById(R.id.etxt_one);
        etxt_two= (EditText)findViewById(R.id.etxt_two);
        etxt_three= (EditText)findViewById(R.id.etxt_three);
        etxt_four= (EditText)findViewById(R.id.etxt_four);
        relative.setVisibility(View.GONE);
        linear.setVisibility(View.VISIBLE);
        sendOtp(string_phone);
        /*if(Build.VERSION.SDK_INT < 23){
            intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
            registerReceiver(broadcastReceiver, intentFilter);
            sendOtp(string_phone);                                              //send the otp

        }else {
            requestContactPermission();
        }*/

        etxt_one.setTransformationMethod(new NumericKeyBoardTransformationMethod());
        etxt_two.setTransformationMethod(new NumericKeyBoardTransformationMethod());
        etxt_three.setTransformationMethod(new NumericKeyBoardTransformationMethod());
        etxt_four.setTransformationMethod(new NumericKeyBoardTransformationMethod());


        etxt_one.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.length()==1){

                    etxt_two.requestFocus();
                }else{
                    btn_continue_active.setVisibility(View.GONE);
                    btn_continue_inactive.setVisibility(View.VISIBLE);
                    btn_continue_inactive.setClickable(false);
                }

            }
        });

        etxt_two.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.length()==1){
                    etxt_three.requestFocus();
                } else if(s.length()==0){
                    etxt_one.requestFocus();
                    btn_continue_active.setVisibility(View.GONE);
                    btn_continue_inactive.setVisibility(View.VISIBLE);
                    btn_continue_inactive.setClickable(false);
                }
            }
        });

        etxt_three.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()==1){
                    etxt_four.requestFocus();
                }else if(s.length()==0){
                    etxt_two.requestFocus();
                    btn_continue_active.setVisibility(View.GONE);
                    btn_continue_inactive.setVisibility(View.VISIBLE);
                    btn_continue_inactive.setClickable(false);
                }
            }
        });

        etxt_four.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.length()==1 && !etxt_one.getText().equals("") && !etxt_two.getText().equals("")
                         && !etxt_three.getText().equals("")){

                    btn_continue_active.setVisibility(View.VISIBLE);
                    btn_continue_inactive.setVisibility(View.GONE);

                }else if(s.length()==0){
                    etxt_three.requestFocus();
                    btn_continue_active.setVisibility(View.GONE);
                    btn_continue_inactive.setVisibility(View.VISIBLE);
                    btn_continue_inactive.setClickable(false);

                }

            }
        });

        ll_txt_otp_by_cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OtpSendingMode=1;
                sendOtp(string_phone);
            }
        });
        ll_txt_otp_by_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OtpSendingMode=2;
                sendOtp(string_phone);
            }
        });
    }

    private void setToolBar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(getResources().getString(R.string.otp_verification));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);

        }
    }

    private void sendOtp(final String phone_number){

        relative.setVisibility(View.GONE);
        linear.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        txt_text.setText("Sending your OTP");

        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        final Send_Otp_Post send_otp_post= new Send_Otp_Post();
        send_otp_post.setPhoneNumber(phone_number);
        send_otp_post.setResetPassword(false);
        send_otp_post.setRetry(OtpSendingMode);
        Log.i("otpverificat", "i'm in onResponse send otp"+ send_otp_post.getPhoneNumber()+ "  "+ send_otp_post.getResetPassword());
        //send otp
        Retrofit retrofit= ServiceGenerator.getClient();
        ApiInterface apiInterface= retrofit.create(ApiInterface.class);
        Call<Send_Otp_Response> call= apiInterface.sendOtpRequest(send_otp_post);
        call.enqueue(new Callback<Send_Otp_Response>() {
            @Override
            public void onResponse(Call<Send_Otp_Response> call, Response<Send_Otp_Response> response) {

                try{
                    if(response.isSuccessful()){

                        if(response.body().getSuccess()){
                            Log.i("otpverificat", "i'm in onResponse send otp");
                            txt_text.setText("Sent!");
                            progressBar.setVisibility(View.GONE);
                            imageView.setVisibility(View.VISIBLE);
                            imageView.setImageResource(R.drawable.ic_otp_sent);

                            Runnable runnable= new Runnable() {
                                @Override
                                public void run() {

                                    relative.setVisibility(View.VISIBLE);
                                    linear.setVisibility(View.GONE);

                                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                                    etxt_one.requestFocus();
                                    imm.showSoftInput(etxt_one, InputMethodManager.SHOW_FORCED);
                                    countDown();
                                }
                            };

                            new Handler().postDelayed(runnable, 1000);


                        }else{
                            //Todo: handle karo iseh
                            Log.i("otpverificat", "i'm in onResponse send otp else case"+ response.body().getSuccess());

                            txt_text.setText("Already Sent!");
                            progressBar.setVisibility(View.GONE);
                            imageView.setVisibility(View.VISIBLE);
                            imageView.setImageResource(R.drawable.ic_otp_sent);

                            Runnable runnable= new Runnable() {
                                @Override
                                public void run() {

                                    relative.setVisibility(View.VISIBLE);
                                    linear.setVisibility(View.GONE);

                                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                                    etxt_one.requestFocus();
                                    imm.showSoftInput(etxt_one, InputMethodManager.SHOW_FORCED);
                                    countDown();
                                }
                            };

                            new Handler().postDelayed(runnable, 1000);


//                            if(send_retry<3){
//                                txt_text.setText("Sending failed, Retrying...");
//                                sendOtp(string_phone);
//                                send_retry++;
//                            }else{
//                                txt_text.setText("Failed!");
//                                imageView.setImageResource(R.drawable.ic_otp_failed);
//                                imageView.setVisibility(View.VISIBLE);
//                                progressBar.setVisibility(View.GONE);
//
//                                Runnable runnable= new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        finish();
//                                    }
//                                };
//                                new Handler().postDelayed(runnable, 1000);
//                                Toast.makeText(OtpVerificationActivity.this, "Please try again after 45 seconds", Toast.LENGTH_SHORT).show();
//                            }
                        }
                    }else{
                        Log.i("otpverificat", "i'm in onResponse send otp not successful");

                        txt_text.setText("Failed!");
                        imageView.setImageResource(R.drawable.ic_otp_failed);
                        imageView.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);

                        Runnable runnable= new Runnable() {
                            @Override
                            public void run() {
                                finish();
                            }
                        };
                        new Handler().postDelayed(runnable, 1000);


                        //Todo: handle karo iseh
                    }


                }catch (Exception e){
                    Log.i("otpverificat", "i'm in onResponse send otp exception");
                }
            }

            @Override
            public void onFailure(Call<Send_Otp_Response> call, Throwable t) {
                //Todo: handle karo iseh

                Log.i("otpverificat", "failure send otp");
                if(send_retry<3){
                    txt_text.setText("Sending failed, Retrying...");
                    sendOtp(string_phone);
                    send_retry++;

                }else{
                    txt_text.setText("Failed!");

                    imageView.setImageResource(R.drawable.ic_otp_failed);
                    imageView.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);

                    Runnable runnable= new Runnable() {
                        @Override
                        public void run() {

                            finish();
                        }
                    };
                    new Handler().postDelayed(runnable, 1000);
                }
            }
        });
    }

    private void verifyOtp(String phone_number, final String otp){

        btn_continue_active.setClickable(false);
       // txt_send_otp.setClickable(false);
        etxt_one.setClickable(false);
        etxt_two.setClickable(false);
        etxt_three.setClickable(false);
        etxt_four.setClickable(false);

        progress_verify.setVisibility(View.VISIBLE);
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        Verify_Otp_Post verify_otp_post= new Verify_Otp_Post();
        verify_otp_post.setPhoneNumber(phone_number);
        verify_otp_post.setOtp(otp);
        verify_otp_post.setNewPassword("");
        verify_otp_post.setGender(str_gender);
        verify_otp_post.setAccessToken(str_token);
        verify_otp_post.setSocialLoginType(int_socialType);

        Retrofit retrofit1= ServiceGenerator.getClient();
        ApiInterface apiInterface= retrofit1.create(ApiInterface.class);

        Call<LoginResponse> call=  apiInterface.verifyOtpRequest(verify_otp_post);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if(response.isSuccessful()){
                    if(response.body().isSuccess()){
                        Log.i("otpverifica", "i'm in onResponse");


                        logCompletedRegistrationEvent();

                        SharedPreferences globalPreference= getSharedPreferences
                                ("globalPreference", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editorr= globalPreference.edit();
                        editorr.putBoolean("firstLogin", false).apply();

                        SharedPreferences sharedPref= getSharedPreferences
                                ("userDetails", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor= sharedPref.edit();
                        editor.putString("name", response.body().getData().getName());
                        editor.putString("gender", response.body().getData().getGender());
                        editor.putString("emailId", response.body().getData().getEmailId());
                        editor.putString("phoneNumber", response.body().getData().getPhoneNumber());
                        editor.putString("userId", response.body().getData().getUserId());
                        editor.putString("accessToken", response.body().getData().getAccessToken());
                        editor.putString("profilePic", response.body().getData().getProfilePic());
                        editor.putBoolean("isLoggedIn", true);
                        editor.putInt("loyaltyPoints", 0);
                        editor.putBoolean("offerDialog", true);
                        editor.putString("promoCode", "BEU");
                        editor.putBoolean("userType", response.body().getData().isUserType());  //yeh apne logo ke liye hai
                        editor.putBoolean("firebase_", false);

                        editor.putBoolean(BeuSalonsSharedPrefrence.VERIFY_CORPORATE, false);           //corporate walo ka hai yeh

                        editor.apply();
                        BeuSalonsSharedPrefrence.setLogin(true);
//                        BeuSalonsSharedPrefrence.
                        progress_verify.setVisibility(View.GONE);
                        Intent intent= new Intent(OtpVerificationActivity.this, FetchingLocationActivity.class);
                        startActivity(intent);
                        finishAffinity();
                    }else{
                        //Todo: handle karo iseh
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                        etxt_four.requestFocus();
                        imm.showSoftInput(etxt_four, InputMethodManager.SHOW_FORCED);
                        Toast.makeText(OtpVerificationActivity.this, "Invalid code", Toast.LENGTH_SHORT).show();
                        btn_continue_active.setClickable(true);
                        //txt_send_otp.setClickable(true);
                        etxt_one.setClickable(true);
                        etxt_two.setClickable(true);
                        etxt_three.setClickable(true);
                        etxt_four.setClickable(true);
                        progress_verify.setVisibility(View.GONE);

                        Log.i("otpverifica", "i'm in onResponse else");
                    }
                }else{
                    //Todo: handle karo iseh
                    finish();
                    //// TODO: 3/25/2017 toast dikhao F
                    Log.i("otpverifica", "i'm in onResponse else else");
                }


            }
            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                if(otp_retry<2){
                    verifyOtp(string_phone, otp);
                    otp_retry++;
                }else{
                    btn_continue_active.setClickable(true);
                    //txt_send_otp.setClickable(true);
                    etxt_one.setClickable(true);
                    etxt_two.setClickable(true);
                    etxt_three.setClickable(true);
                    etxt_four.setClickable(true);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                    etxt_one.requestFocus();
                    imm.showSoftInput(etxt_one, InputMethodManager.SHOW_FORCED);
                    Toast.makeText(OtpVerificationActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    progress_verify.setVisibility(View.GONE);
                }
                Log.i("otpverifica", "verfication failure"+ t.getCause()+ "   "+t.getMessage());
            }
        });
    }
    public void logCompletedRegistrationEvent ( ) {
        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, "registratedUser");
        logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, params);
    }
//    private void logEventForAppsFlyers(){
//        Log.e("logEvent","test123");
//        Map<String,Object> eventData = new HashMap<>();
//        eventData.put(AFInAppEventParameterName.REGSITRATION_METHOD,"login");
//        AppsFlyerLib.getInstance().trackEvent(OtpVerificationActivity.this, AFInAppEventType.COMPLETE_REGISTRATION,eventData);
//    }
    private void requestContactPermission() {

        int hasContactPermission = ActivityCompat.checkSelfPermission(this , android.Manifest.permission.RECEIVE_SMS);

        if(hasContactPermission != PackageManager.PERMISSION_GRANTED ) {
            Log.i("permissions", "i'm in not onReqest permis granterd");
            ActivityCompat.requestPermissions(this, new String[]   {android.Manifest.permission.RECEIVE_SMS}, SMS_PERMISSION);
        }else {
            Log.i("permissions", "i'm in already onReqest permis granterd");
            intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
//            registerReceiver(broadcastReceiver, intentFilter);
            sendOtp(string_phone);                           //send the otp
//            Toast.makeText(this, "Contact Permission is already granted", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case SMS_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("permissions", "i'm in onReqest permis granterd");
                    intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
//                    registerReceiver(broadcastReceiver, intentFilter);
                    sendOtp(string_phone);          //send the otp


                } else {
                    Log.i("permissions", "i'm in permission denied onReqest permis granterd");
                    sendOtp(string_phone);          //send the otp

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;

            }
        }

    }

    private void readOtp(){


        Cursor cur = getContentResolver().query(Uri.parse("content://sms/"), null, null, null, null);
        String otp="";
        try{
            if(cur.moveToFirst()){

                String getAddressValue= cur.getString(cur.getColumnIndex("address"));

                if(getAddressValue.endsWith("-BEUSLN")){
                    String getMessageValue= cur.getString(cur.getColumnIndex("body"));
                    otp= getMessageValue.substring(0, getMessageValue.indexOf(" "));
                }
//                if(getAddressValue.equals("MD-BEUSLN")){
//                    String getMessageValue= cur.getString(cur.getColumnIndex("body"));
//                    otp= getMessageValue.substring(0, getMessageValue.indexOf(" "));
//                }
                cur.close();
            }

            Log.e("otpverifica", "value in the klsdajf read otp: "+ otp);

            if (isInteger(otp)){

                etxt_one.setText(otp.charAt(0)+"");
                etxt_two.setText(otp.charAt(1)+"");
                etxt_three.setText(otp.charAt(2)+"");
                etxt_four.setText(otp.charAt(3)+"");
            }else{
                Log.e("otpverifica", "value in the klsdajf read otp: cant read it sfkldajlsldak");
            }



        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    private void countDown(){
        new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                txt_timer.setVisibility(View.VISIBLE);
                txt_timer.setText("Resend in: " + millisUntilFinished / 1000);
                linearLayout.setVisibility(View.GONE);
            }

            public void onFinish() {

                txt_timer.setVisibility(View.GONE);
                linearLayout.setVisibility(View.VISIBLE);
                txt_send_otp.setPaintFlags(txt_send_otp.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
                txt_send_otp.setClickable(true);
                txt_send_otpByCall.setPaintFlags(txt_send_otpByCall.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
                txt_send_otpByCall.setClickable(true);
            }
        }.start();
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try{
            Log.i("otpverifica", "yeh toh hona hi tha");        //// TODO: 3/7/2017: you, yes you handle this receiver

//            unregisterReceiver(broadcastReceiver);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private class NumericKeyBoardTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return source;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
