package com.beusalons.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.beusalons.android.Adapter.ParlorReviewListAdapter;
import com.beusalons.android.Model.Reviews.ParlorReviewData;
import com.beusalons.android.Model.Reviews.ParlorReviewResponse;
import com.beusalons.android.Retrofit.ApiInterface;
import com.beusalons.android.Retrofit.ServiceGenerator;
import com.beusalons.android.Utility.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ParlorReviewActivity extends AppCompatActivity {


    public static final String TAG= ParlorReviewActivity.class.getSimpleName();

    private TextView txt_salonName;


    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private Retrofit retrofit;

    String parlorId;
    String parlorImage;

    private View mContentView;
    private View mLoadingView;

    TextView txtViewActionBarName;
    ImageView imgViewBack;
    private List<ParlorReviewData> reviewsList= new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon_review_);
        //new ToolbarHelper(getSupportActionBar(), getLayoutInflater(), true, this);
        setToolBar();
//
//        mContentView= findViewById(R.id.relative_bill_summary);
//        mLoadingView= findViewById(R.id.loading_spinner_bill_summary);
//        mContentView.setVisibility(View.GONE);
//        mLoadingView.setVisibility(View.VISIBLE);



        txt_salonName= (TextView)findViewById(R.id.txt_salonReview_salonName);

        Intent intent= getIntent();

        if(intent!=null){
            parlorId= intent.getStringExtra("parlorId");
            parlorImage= intent.getStringExtra("parlorImage");
            txt_salonName.setText(intent.getStringExtra("parlorName"));
            Log.i(TAG, "parlor id: "+parlorId + " parlor name: "+intent.getStringExtra("parlorName")+
                    "parlor image: "+parlorImage);
        }

        recyclerView= (RecyclerView)findViewById(R.id.rcyView_salonReview);
        layoutManager= new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter= new ParlorReviewListAdapter(reviewsList, parlorImage, this);

        recyclerView.setAdapter(adapter);

        fetchData();
    }

    private void setToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(getResources().getString(R.string.parlor_review));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Utility.applyFontForToolbarTitle(this);

        }
    }

    private void fetchData(){

//        retrofit = ServiceGenerator.getClient();
//        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
//        Log.i("investi", "in parlor id: "+ parlorId);
//        Call<ParlorReviewResponse> call= apiInterface.getParlorReviews(parlorId, 1);
//        call.enqueue(new Callback<ParlorReviewResponse>() {
//            @Override
//            public void onResponse(Call<ParlorReviewResponse> call, SelectEmployeeResponse<ParlorReviewResponse> response) {
//
//                if(reviewsList!=null){
//                    reviewsList.clear();
//                }
//
//                if(response.body()!=null){
//
//                    Log.i("investi", "response data: "+ response.body().getData().size());
//
//                    for(int i=0;i<response.body().getData().size();i++){
//
//                        ParlorReviewData data= new ParlorReviewData(0);
//                        data.setUserName(response.body().getData().get(i).getUserName());
//                        data.setText(response.body().getData().get(i).getText());
//                        data.setRating(response.body().getData().get(i).getRating());
//                        data.setTime(response.body().getData().get(i).getTime());
//                        reviewsList.add(data);
//                        Log.i("investi", "response data: "+ reviewsList.size());
//                    }
//                    adapter.notifyDataSetChanged();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ParlorReviewResponse> call, Throwable t) {
//                Log.i(TAG, "I'm in on failure"+ t.getMessage()+ "  "+ t.getStackTrace()+ "  "+ t.getCause());
//            }
//        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
